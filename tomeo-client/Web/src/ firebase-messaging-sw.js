// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/9.12.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('../src/firebase-messaging-sw.js')
    .then(function(registration) {
      console.log('Registration successful, scope is:', registration.scope);
    }).catch(function(err) {
    console.log('Service worker registration failed, error:', err);
  });
}

firebase.initializeApp({
  apiKey: "AIzaSyAys1FDAalh5kpgyol-N0iPVnK1AupSVPM",
  authDomain: "myproject-433ea.firebaseapp.com",
  projectId: "myproject-433ea",
  storageBucket: "myproject-433ea.appspot.com",
  messagingSenderId: '639389982950',
  appId: "1:639389982950:web:f2c45957f2b47d160d1b27",
  measurementId: "G-KX8TPBGH9X"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  if (payload.notification) {
    const notificationTitle = payload.notification.title;
    const notificationOptions = {
      body: payload.notification.body,
      icon: '/logo.png',
      vibrate: [200, 100, 200, 100, 200, 100, 200],
    };
    return self.registration.showNotification(notificationTitle, notificationOptions);
  }
});

import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {en_US, NZ_I18N} from 'ng-zorro-antd/i18n';
import {registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutComponent} from './layout/layout.component';
import {LayoutModule} from "./layout/layout.module";
import {NzAffixModule} from "ng-zorro-antd/affix";
import {AuthenticationModule} from "./module/authentication/authentication.module";
import {NzStepsModule} from "ng-zorro-antd/steps";

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    NzAffixModule,
    AuthenticationModule,
    NzStepsModule,
    // AngularFireModule.initializeApp({
    //   apiKey: "AIzaSyAys1FDAalh5kpgyol-N0iPVnK1AupSVPM",
    //   authDomain: "myproject-433ea.firebaseapp.com",
    //   projectId: "myproject-433ea",
    //   storageBucket: "myproject-433ea.appspot.com",
    //   messagingSenderId: "639389982950",
    //   appId: "1:639389982950:web:f2c45957f2b47d160d1b27",
    //   measurementId: "G-KX8TPBGH9X",
    //   databaseURL: 'https://myproject-433ea.firebaseio.com',
    // }),
    // AngularFireMessagingModule,
  ],
  providers: [{provide: NZ_I18N, useValue: en_US}],
  bootstrap: [AppComponent]
})
export class AppModule {
}

function initializeApp(arg0: any): import("@firebase/app").FirebaseApp {
  throw new Error('Function not implemented.');
}


import {Component, OnInit} from '@angular/core';
import {LayoutService} from "../layout.service";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  visible = false
  constructor(
    private layoutService: LayoutService
  ) {
  }

  ngOnInit(): void {
    this.layoutService.categoryMenuVisible.subscribe((value) =>{
      this.visible = value
    })
  }

}

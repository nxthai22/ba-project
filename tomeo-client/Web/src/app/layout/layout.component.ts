import {Component, OnInit} from '@angular/core';
import {LayoutService} from "./layout.service";

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  constructor(
    private layoutService: LayoutService
  ) {
  }

  ngOnInit(): void {
  }

  onScrollPage(event: boolean): void {
    if (event)
      this.layoutService.categoryMenuVisible.next(false)
  }

  onActive(event: any) {
    if (event.constructor?.name === 'HomeComponent')
      this.layoutService.categoryMenuVisible.next(true)
    else
      this.layoutService.categoryMenuVisible.next(false)
  }
}

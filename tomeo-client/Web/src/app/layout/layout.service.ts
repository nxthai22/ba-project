import { Injectable } from "@angular/core";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: "root",
})
export class LayoutService {
  categoryMenuVisible = new BehaviorSubject<boolean>(false)
  constructor() {
  }
}

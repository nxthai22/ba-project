import {NgModule} from "@angular/core";
import {FooterComponent} from "./footer/footer.component";
import {HeaderComponent} from "./header/header.component";
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {NzNotificationModule} from "ng-zorro-antd/notification";
import {NzAffixModule} from "ng-zorro-antd/affix";
import {FormsModule} from "@angular/forms";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzBadgeModule} from "ng-zorro-antd/badge";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzSelectModule} from "ng-zorro-antd/select";
import { MenuComponent } from './menu/menu.component';
import {NzButtonModule} from "ng-zorro-antd/button";
import {AuthenticationModule} from "../module/authentication/authentication.module";
import {NzDrawerModule} from "ng-zorro-antd/drawer";

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    MenuComponent
  ],
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        NzAffixModule,
        NzNotificationModule,
        NzDropDownModule,
        NzIconModule,
        NzDividerModule,
        NzBadgeModule,
        NzInputModule,
        NzSelectModule,
        NzButtonModule,
        AuthenticationModule,
        NzDrawerModule
    ],
  exports: [
    HeaderComponent,
    FooterComponent,
    MenuComponent
  ]
})
export class LayoutModule {
}

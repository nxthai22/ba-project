import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLogin: boolean = false
  isCart: boolean = false
  products = []
  constructor() {
  }

  ngOnInit(): void {
  }

}

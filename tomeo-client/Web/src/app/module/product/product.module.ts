import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from '../product/product.component';
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzRateModule} from "ng-zorro-antd/rate";
import {FormsModule} from "@angular/forms";
import {ProductRoutingModule} from "./product-routing.module";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzRadioModule} from "ng-zorro-antd/radio";
import {SlickCarouselModule} from "ngx-slick-carousel";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {ComponentModule} from "../../share/component/component.module";
import {NzAffixModule} from "ng-zorro-antd/affix";

@NgModule({
  declarations: [
    ProductComponent
  ],
    imports: [
        CommonModule,
        ProductRoutingModule,
        NzDividerModule,
        NzRateModule,
        FormsModule,
        NzIconModule,
        NzButtonModule,
        NzRadioModule,
        SlickCarouselModule,
        NzTabsModule,
        ComponentModule,
        NzAffixModule
    ]
})
export class ProductModule { }

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  tabs = [
    {
      title: 'Description',
      description:'',
      active: true
    },
    {
      title: 'Specification',
      description: '',
      active: false
    },
    {
      title: 'Vendor Info',
      description: '',
      active: false
    },
    {
      title: 'Customer Reviews',
      description: '',
      active: false
    },
  ]
  productConfig = {
    slidesToShow: 4,
    slidesToScroll: 4,
    dots: false,
    infinite: false,
    autoplay: true,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  products = [
    {
      id: 1,
      url: "/assets/images/products/product-1.jpg",
      name: 'Sản phẩm 1',
      rateView: 4,
      price: 150000,
      priceDiscount: 120000
    },
    {
      id: 2,
      url: "/assets/images/products/product-2.jpg",
      name: 'Sản phẩm 2',
      rateView: 4.5,
      price: 150000,
      priceDiscount: 120000
    },
    {
      id: 3,
      url: "/assets/images/products/product-3.jpg",
      name: 'Sản phẩm 3',
      rateView: 5,
      price: 150000,
      priceDiscount: 120000
    },
    {
      id: 4,
      url: "/assets/images/products/product-4.jpg",
      name: 'Sản phẩm 4',
      rateView: 4.5,
      price: 150000,
      priceDiscount: 120000
    },
    {
      id: 5,
      url: "/assets/images/products/product-5.jpg",
      name: 'Sản phẩm 5',
      rateView: 4,
      price: 150000,
      priceDiscount: 120000
    },
  ]

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    const goodId = this.route.snapshot.paramMap.get('id');
  }

  redirectOtherProduct(prodId: any): void {
    this.router
      .navigateByUrl('/', {skipLocationChange: true})
      .then(() => this.router.navigate([`/product-detail/${Number(prodId)}`]));
  }
}

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {HomeComponent} from "./home.component";
import {NzGridModule} from "ng-zorro-antd/grid";
import {HomeRoutingModule} from "./home-routing.module";
import {FormsModule} from "@angular/forms";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzCarouselModule} from "ng-zorro-antd/carousel";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzCardModule} from "ng-zorro-antd/card";
import {SlickCarouselModule} from "ngx-slick-carousel";
import {NzRateModule} from "ng-zorro-antd/rate";
import {NzPopoverModule} from "ng-zorro-antd/popover";
import {NzRadioModule} from "ng-zorro-antd/radio";

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HomeRoutingModule,
    NzGridModule,
    NzMenuModule,
    NzIconModule,
    NzCarouselModule,
    NzDividerModule,
    NzCardModule,
    SlickCarouselModule,
    NzRateModule,
    NzPopoverModule,
    NzRadioModule,
  ]
})
export class HomeModule {
}

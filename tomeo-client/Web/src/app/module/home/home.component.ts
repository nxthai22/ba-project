import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  sliders = [
    {
      url: "/assets/images/sliders/image-1.jpg",
      value: 1
    },
    {
      url: "/assets/images/sliders/image-2.jpg",
      value: 2
    },
    {
      url: "/assets/images/sliders/image-3.jpg",
      value: 3
    },
    {
      url: "/assets/images/sliders/image-4.jpg",
      value: 4
    },
    {
      url: "/assets/images/sliders/image-5.jpg",
      value: 5
    },
    {
      url: "/assets/images/sliders/image-6.jpg",
      value: 6
    },
  ]
  products = [
    {
      url: "/assets/images/products/product-1.jpg",
      name: 'Sản phẩm 1',
      rateView: 4,
      price: 150000,
      priceDiscount: 120000
    },
    {
      url: "/assets/images/products/product-2.jpg",
      name: 'Sản phẩm 2',
      rateView: 4.5,
      price: 150000,
      priceDiscount: 120000
    },
    {
      url: "/assets/images/products/product-3.jpg",
      name: 'Sản phẩm 3',
      rateView: 5,
      price: 150000,
      priceDiscount: 120000
    },
    {
      url: "/assets/images/products/product-4.jpg",
      name: 'Sản phẩm 4',
      rateView: 4.5,
      price: 150000,
      priceDiscount: 120000
    },
    {
      url: "/assets/images/products/product-5.jpg",
      name: 'Sản phẩm 5',
      rateView: 4,
      price: 150000,
      priceDiscount: 120000
    },
  ]
  categories = [
    {
      url: '/assets/images/categories/heart-outline.png',
      value: 'Category 8'
    },
    {
      url: '/assets/images/categories/heart-outline.png',
      value: 'Category 8'
    },
    {
      url: '/assets/images/categories/heart-outline.png',
      value: 'Category 8'
    },
    {
      url: '/assets/images/categories/heart-outline.png',
      value: 'Category 8'
    },
    {
      url: '/assets/images/categories/heart-outline.png',
      value: 'Category 8'
    },
    {
      url: '/assets/images/categories/heart-outline.png',
      value: 'Category 8'
    },
    {
      url: '/assets/images/categories/heart-outline.png',
      value: 'Category 8'
    },
    {
      url: '/assets/images/categories/heart-outline.png',
      value: 'Category 8'
    },
  ]
  weeklyVendors = [
    {
      url: '/assets/images/products/product-4.jpg',
      name: 'Apple',
      value: 150,
      brandUrl: '/assets/images/categories/heart-outline.png',
      isHover: false
    },
    {
      url: '/assets/images/products/product-3.jpg',
      name: 'Apple',
      value: 150,
      brandUrl: '/assets/images/categories/heart-outline.png',
      isHover: false
    },
    {
      url: '/assets/images/products/product-2.jpg',
      name: 'Apple',
      value: 150,
      brandUrl: '/assets/images/categories/heart-outline.png',
      isHover: false
    },
    {
      url: '/assets/images/products/product-1.jpg',
      name: 'Apple',
      value: 150,
      brandUrl: '/assets/images/categories/heart-outline.png',
      isHover: false
    },
  ]
  options = ['Small','Medium','Large','Extra large']
  slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    infinite: true,
    autoplay: true,
    arrows: false,
  };
  productConfig = {
    slidesToShow: 5,
    slidesToScroll: 5,
    dots: false,
    infinite: false,
    autoplay: true,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  categoryConfig = {
    slidesToShow: 8,
    slidesToScroll: 8,
    dots: false,
    infinite: false,
    autoplay: true,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  weeklyVendorConfig = {
    slidesToShow: 4,
    slidesToScroll: 4,
    dots: false,
    infinite: false,
    autoplay: true,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  constructor() {
  }

  ngOnInit(): void {
  }

}

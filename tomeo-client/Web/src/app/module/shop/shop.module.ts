import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ShopComponent} from '../shop/shop.component';
import {ShopRoutingModule} from "./shop-routing.module";
import {SlickCarouselModule} from "ngx-slick-carousel";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzCollapseModule} from "ng-zorro-antd/collapse";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzRateModule} from "ng-zorro-antd/rate";
import {NzIconModule} from "ng-zorro-antd/icon";
import {ComponentModule} from "../../share/component/component.module";


@NgModule({
  declarations: [
    ShopComponent
  ],
  imports: [
    CommonModule,
    ShopRoutingModule,
    ComponentModule,
    SlickCarouselModule,
    NzDividerModule,
    ReactiveFormsModule,
    NzSelectModule,
    NzCollapseModule,
    NzInputModule,
    NzCheckboxModule,
    NzGridModule,
    NzRateModule,
    FormsModule,
    NzIconModule
  ]
})
export class ShopModule {
}

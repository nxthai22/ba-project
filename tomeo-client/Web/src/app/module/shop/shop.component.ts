import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  categories = [
    {
      url: '/assets/images/products/product-2.jpg',
      value: 'Category 8'
    },
    {
      url: '/assets/images/products/product-2.jpg',
      value: 'Category 8'
    },
    {
      url: '/assets/images/products/product-2.jpg',
      value: 'Category 8'
    },
    {
      url: '/assets/images/products/product-2.jpg',
      value: 'Category 8'
    },
    {
      url: '/assets/images/products/product-2.jpg',
      value: 'Category 8'
    },
    {
      url: '/assets/images/products/product-2.jpg',
      value: 'Category 8'
    },
    {
      url: '/assets/images/products/product-2.jpg',
      value: 'Category 8'
    },
    {
      url: '/assets/images/products/product-2.jpg',
      value: 'Category 8'
    },
  ]
  products = [
    {
      id: 1,
      url: "/assets/images/products/product-1.jpg",
      name: 'Sản phẩm 1',
      rateView: 4,
      price: 150000,
      priceDiscount: 120000
    },
    {
      id: 2,
      url: "/assets/images/products/product-2.jpg",
      name: 'Sản phẩm 2',
      rateView: 4.5,
      price: 150000,
      priceDiscount: 120000
    },
    {
      id: 3,
      url: "/assets/images/products/product-3.jpg",
      name: 'Sản phẩm 3',
      rateView: 5,
      price: 150000,
      priceDiscount: 120000
    },
    {
      id: 4,
      url: "/assets/images/products/product-4.jpg",
      name: 'Sản phẩm 4',
      rateView: 4.5,
      price: 150000,
      priceDiscount: 120000
    },
    {
      id: 5,
      url: "/assets/images/products/product-5.jpg",
      name: 'Sản phẩm 5',
      rateView: 4,
      price: 150000,
      priceDiscount: 120000
    },
  ]
  categoryConfig = {
    slidesToShow: 8,
    slidesToScroll: 8,
    dots: false,
    infinite: false,
    autoplay: true,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  }
  sortForm!: FormGroup

  constructor() {
  }

  ngOnInit(): void {
    this.sortForm = new FormGroup({
      sortBy: new FormControl(''),
      size: new FormControl('')
    })
  }

}

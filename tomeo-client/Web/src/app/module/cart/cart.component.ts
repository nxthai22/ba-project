import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  currIndex = 1
  products = [
    {
      id: 1,
      url: "/assets/images/products/product-1.jpg",
      name: 'Sản phẩm 1',
      rateView: 4,
      price: 150000,
      priceDiscount: 120000
    },
    {
      id: 2,
      url: "/assets/images/products/product-2.jpg",
      name: 'Sản phẩm 2',
      rateView: 4.5,
      price: 150000,
      priceDiscount: 120000
    },
    {
      id: 3,
      url: "/assets/images/products/product-3.jpg",
      name: 'Sản phẩm 3',
      rateView: 5,
      price: 150000,
      priceDiscount: 120000
    },
    {
      id: 4,
      url: "/assets/images/products/product-4.jpg",
      name: 'Sản phẩm 4',
      rateView: 4.5,
      price: 150000,
      priceDiscount: 120000
    },
    {
      id: 5,
      url: "/assets/images/products/product-5.jpg",
      name: 'Sản phẩm 5',
      rateView: 4,
      price: 150000,
      priceDiscount: 120000
    },
  ]
  checkoutForm!: FormGroup

  constructor() {
  }

  ngOnInit(): void {
    this.checkoutForm = new FormGroup({
      fullName: new FormControl(null, [Validators.required]),
      phone: new FormControl(null),
      email: new FormControl(null, [Validators.required]),
      provinceId: new FormControl(null, [Validators.required]),
      districtId: new FormControl(null, [Validators.required]),
      wardId: new FormControl(null, [Validators.required]),
      ZIP: new FormControl(null, [Validators.required]),
      address: new FormControl(null, [Validators.required]),
      note: new FormControl(null),
    })
  }

  onIndexChange(event: number) {
    this.currIndex = event
  }
}

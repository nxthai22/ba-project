import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {CartComponent} from "./cart.component";
import {CartRoutingModule} from "./cart-routing.module";
import {NzStepsModule} from "ng-zorro-antd/steps";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NzFormModule} from "ng-zorro-antd/form";
import {ReactiveFormsModule} from "@angular/forms";
import {NzResultModule} from "ng-zorro-antd/result";

@NgModule({
  declarations: [
    CartComponent
  ],
  imports: [
    CommonModule,
    CartRoutingModule,
    NzStepsModule,
    NzIconModule,
    NzButtonModule,
    NzInputModule,
    NzCardModule,
    NzSelectModule,
    NzDividerModule,
    NzFormModule,
    ReactiveFormsModule,
    NzResultModule
  ]
})
export class CartModule {
}

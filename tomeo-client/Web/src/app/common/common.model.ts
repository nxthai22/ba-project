export class Paginator {
  total?: number = 0
  page?: number = 1
  pageSize?: number = 10
}

export class PageParam {
  page?: number = 1
  pageSize?: number = 10
  keyword?: string
  startDate?: number
  toDate?: number
}


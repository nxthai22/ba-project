// import {Injectable} from "@angular/core";
// import {AngularFireMessaging} from "@angular/fire/compat/messaging";
//
// @Injectable({
//   providedIn: 'root'
// })
// export class CommonService {
//   constructor(
//     private angularFireMessaging: AngularFireMessaging
//   ) {
//     this.angularFireMessaging.messages.subscribe((message) => {
//       console.log('message', message)
//     })
//   }
//
//   getToken() {
//     this.angularFireMessaging.getToken.subscribe((token) => {
//       console.log('getToken', token)
//     })
//   }
//
//   requestPermission(userId?: string) {
//     this.angularFireMessaging.requestToken.subscribe(
//       (token) => {
//         console.log('requestToken', token);
//       },
//       (err) => {
//         console.error('Unable to get permission to notify.', err);
//       }
//     );
//   }
//
//   receiveMessage() {
//     this.angularFireMessaging.messages.subscribe(
//       (payload) => {
//         console.log('new message received: ', payload);
//         // We decide not to do anything yet.
//       });
//   }
// }

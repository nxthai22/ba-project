import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {LayoutComponent} from "./layout/layout.component";
import {LoginComponent} from "./module/authentication/login/login.component";

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'home',
        loadChildren: () =>
          import('./module/home/home.module').then((m) => m.HomeModule),
        // canActivate: [AuthGuard],
      },
      {
        path: 'shop',
        loadChildren: () =>
          import('./module/shop/shop.module').then((m) => m.ShopModule),
        // canActivate: [AuthGuard],
      },
      {
        path: 'product',
        loadChildren: () =>
          import('./module/product/product.module').then((m) => m.ProductModule),
        // canActivate: [AuthGuard],
      },
      {
        path: 'cart',
        loadChildren: () =>
          import('./module/cart/cart.module').then((m) => m.CartModule),
        // canActivate: [AuthGuard],
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

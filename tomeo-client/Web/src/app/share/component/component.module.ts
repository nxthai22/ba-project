import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductBoxComponent} from './product-box/product-box.component';
import {NzRateModule} from "ng-zorro-antd/rate";
import {FormsModule} from "@angular/forms";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzButtonModule} from "ng-zorro-antd/button";


@NgModule({
  declarations: [
    ProductBoxComponent
  ],
  imports: [
    CommonModule,
    NzRateModule,
    FormsModule,
    NzIconModule,
    NzButtonModule
  ],
  exports: [ProductBoxComponent]
})
export class ComponentModule {
}

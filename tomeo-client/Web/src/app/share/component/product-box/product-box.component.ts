import {Component, Input, OnInit, TemplateRef} from '@angular/core';
import {NzNotificationService} from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-product-box',
  templateUrl: './product-box.component.html',
  styleUrls: ['./product-box.component.css']
})
export class ProductBoxComponent implements OnInit {
  @Input() product: any
  @Input() styleClass!: string
  @Input() disableQuickAction: boolean = false
  isHover = false

  constructor(
    private notification: NzNotificationService
  ) {
  }

  ngOnInit(): void {
  }

  createBasicNotification(template: TemplateRef<any>) {
    this.notification.template(template, {nzPlacement: 'bottomLeft'})
  }

}

module.exports = {
  important: true,
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {
      textColor: {
        primary: '#223263',
        secondary: '#797E8E',
        price: '#F4447F',
      },
      backgroundColor: {
        primary: '#223263',
        secondary: '#797E8E',
        price: '#F4447F',
      },
      borderColor: {
        primary: '#223263',
        secondary: '#797E8E',
        price: '#F4447F',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/line-clamp')],
}

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
})

module.exports = withBundleAnalyzer({
  swcMinify: false,
  images: {
    domains: [
      'cdn.ushare.com.vn',
      'hasuta.com.vn',
      'demo.hasuta.com.vn',
      'hasutashop.com',
      'hasuta.com',
      '192.168.2.229',
      'cdn.ushare.hoangnq.com',
      'ushare.hn.ss.bfcplatform.vn',
      'ushare_dev.hn.ss.bfcplatform.vn',
      'ushare-dev.hn.ss.bfcplatform.vn'
    ],
    imageSizes: [180],
  },
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // your project has type errors.
    //
    // This option is rarely needed, and should be reserved for advanced
    // setups. You may be looking for `ignoreDevErrors` instead.
    // !! WARN !!
    ignoreBuildErrors: true,
  },
  eslint: {
    // Warning: Dangerously allow production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
})

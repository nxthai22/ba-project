import { useRequest } from "ahooks";
import { atom, useRecoilState } from "recoil";
import { ProductCategoriesService, ProductCategoryEntity } from "services";

export const categoriesState = atom<ProductCategoryEntity[] | []>({
  key: 'categoriesState',
  default: [],
});

export const useCategory = () => {
  const [categories, setCategories] = useRecoilState(categoriesState);
  const getCategoryReq = useRequest(ProductCategoriesService.getManyBase, { manual: true });

  const getCategory = () => {
    getCategoryReq.runAsync({
      limit: 100,
      filter: ['parentId||isnull'],
      join: ['childrens'],
      sort: ['createdAt,DESC'],
    }).then(res => {
      setCategories(res.data);
    })
  }

  return {
    categories,
    getCategory
  }
};

import { useRequest } from 'ahooks'
import { atom, useRecoilState } from 'recoil'
import { CollaboratorStatsService, CollaboratorChartStatsDto } from 'services'

export interface CollaboratorStatsParams {
  merchantId?: number
  timePeriod?: string
  fromDate?: string
  toDate?: string
}

export const CollaboratorStatsState = atom<CollaboratorChartStatsDto>({
  key: 'CollaboratorStatsState',
  default: {
    revenueAvg: 0,
    charts: [],
  },
})

export const useCollaboratorStats = () => {
  const [collaboratorStats, setCollaboratorStats] = useRecoilState(CollaboratorStatsState)
  const getCollaboratorStatsReq = useRequest(CollaboratorStatsService.collaboratorStatsControllerCollaboratorStats, { manual: true })

  const getCollaboratorStats = (params: CollaboratorStatsParams) => {
    getCollaboratorStatsReq.runAsync(params).then(res => {
      setCollaboratorStats(res)
    })
  }

  return {
    collaboratorStats,
    getCollaboratorStats
  }
}

import { useRequest, useUpdateEffect } from 'ahooks'
import { useState } from 'react'
import { useRecoilValue } from 'recoil'
import { authState } from 'recoil/Atoms'
import { ProductsService } from 'services'

export interface GetManyBaseParams {
  fields?: any | null[]
  s?: string
  filter?: any | null[]
  or?: any | null[]
  sort?: any | null[]
  join?: any | null[]
  limit?: number
  offset?: number
  page?: number
  cache?: number
}

export const useGetProducts = () => {
  const user = useRecoilValue(authState)

  const [filterParams, setFilterParams] = useState<GetManyBaseParams>({
    sort: ['createdAt,DESC'],
    filter: [`merchantId||$eq||${user.merchantId}`],
    join: ['variants'],
    limit: 10,
    page: 1,
  })

  const getProductsReq = useRequest(ProductsService.getManyBase, {
    manual: true,
  })

  const getProducts = (params: GetManyBaseParams) => {
    getProductsReq.runAsync(params)
  }

  const setFilter = (params: GetManyBaseParams) => {
    setFilterParams({ ...filterParams, ...params })
  }
  useUpdateEffect(() => {
    getProducts(filterParams)
  }, [filterParams])

  return {
    filterParams,
    setFilter,
    products: getProductsReq.data?.data || [],
    loading: getProductsReq.loading,
    metadata: {
      total: getProductsReq.data?.total || 0,
      pageCount: getProductsReq.data?.pageCount || 0,
    },
  }
}

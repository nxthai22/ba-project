import { useRequest } from 'ahooks'
import { atom, useRecoilState } from 'recoil'
import { OrderStatsService, OrderRevenueStatsDto } from 'services'

export interface OrderStatsRevenueParams {
  /**  */
  merchantId?: number
  /** Khoảng thời gian */
  timePeriod?: string
  /** Ngày bắt đầu */
  fromDate?: string
  /** Ngày kết thúc */
  toDate?: string
}

export const OrderStatsRevenueState = atom<OrderRevenueStatsDto[] | []>({
  key: 'OrderStatsRevenueState',
  default: [],
})

export const useOrderStatsRevenue = () => {
  const [orderStatsRevenue, setOrderStatsRevenue] = useRecoilState(OrderStatsRevenueState)
  const getOrderStatsRevenueReq = useRequest(OrderStatsService.orderStatsControllerOrderRevenueStats, { manual: true })

  const getOrderStatsRevenue = (params: OrderStatsRevenueParams) => {
    getOrderStatsRevenueReq.runAsync(params).then(res => {
      setOrderStatsRevenue(res)
    })
  }

  return {
    orderStatsRevenue,
    getOrderStatsRevenue
  }
}

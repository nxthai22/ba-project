import { useRequest } from 'ahooks'
import { atom, useRecoilState } from 'recoil'
import { OrderStatsService, OrderStatusStatDto } from 'services'

export interface OrderStatsStatusParams {
  merchantId?: number
  timePeriod?: string
  fromDate?: string
  toDate?: string
}

export const OrderStatsStatusState = atom<OrderStatusStatDto[] | []>({
  key: 'OrderStatsStatusState',
  default: [],
})

export const useOrderStatsStatus = () => {
  const [orderStatsStatus, setOrderStatsStatus] = useRecoilState(OrderStatsStatusState)
  const getOrderStatsStatusReq = useRequest(OrderStatsService.orderStatsControllerOrderStatusStats, { manual: true })

  const getOrderStatsStatus = (params: OrderStatsStatusParams) => {
    getOrderStatsStatusReq.runAsync(params).then(res => {
      setOrderStatsStatus(res)
    })
  }

  return {
    orderStatsStatus,
    getOrderStatsStatus
  }
}

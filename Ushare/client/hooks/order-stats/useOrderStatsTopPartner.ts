import { useRequest } from 'ahooks'
import { atom, useRecoilState } from 'recoil'
import { OrderStatsService, OrderTopPartnerDto } from 'services'

export interface OrderStatsTopPartnerParams {
  /**  */
  merchantId?: number
  /** Sắp xếp theo đơn hàng, doanh thu, hoa hồng */
  sort?: string
  /** Khoảng thời gian */
  timePeriod?: string
  /** Ngày bắt đầu */
  fromDate?: string
  /** Ngày kết thúc */
  toDate?: string
}

export const OrderStatsTopPartnerState = atom<OrderTopPartnerDto[] | []>({
  key: 'OrderStatsTopPartnerState',
  default: [],
})

export const useOrderStatsTopPartner = () => {
  const [orderStatsTopPartner, setOrderStatsTopPartner] = useRecoilState(OrderStatsTopPartnerState)
  const getOrderStatsTopPartnerReq = useRequest(OrderStatsService.orderStatsControllerOrderRevenueTopPartner, { manual: true })

  const getOrderStatsTopPartner = (params: OrderStatsTopPartnerParams) => {
    getOrderStatsTopPartnerReq.runAsync(params).then(res => {
      setOrderStatsTopPartner(res)
    })
  }

  return {
    orderStatsTopPartner,
    getOrderStatsTopPartner
  }
}

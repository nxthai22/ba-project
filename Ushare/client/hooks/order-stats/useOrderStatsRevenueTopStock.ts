import { useRequest } from 'ahooks'
import { atom, useRecoilState } from 'recoil'
import { OrderStatsService, OrderRevenueTopStockDto } from 'services'

export interface OrderStatsRevenueTopStockParams {
  merchantId?: number
  timePeriod?: string
  fromDate?: string
  toDate?: string
}

export const OrderStatsRevenueTopStockState = atom<OrderRevenueTopStockDto[] | []>({
  key: 'OrderStatsRevenueTopStockState',
  default: [],
})

export const useOrderStatsRevenueTopStock = () => {
  const [orderStatsRevenueTopStock, setOrderStatsRevenueTopStock] = useRecoilState(OrderStatsRevenueTopStockState)
  const getOrderStatsRevenueTopStockReq = useRequest(OrderStatsService.orderStatsControllerOrderRevenueTopStock, { manual: true })

  const getOrderStatsRevenueTopStock = (params: OrderStatsRevenueTopStockParams) => {
    getOrderStatsRevenueTopStockReq.runAsync(params).then(res => {
      setOrderStatsRevenueTopStock(res)
    })
  }

  return {
    orderStatsRevenueTopStock,
    getOrderStatsRevenueTopStock
  }
}

import { alertError } from 'utils'
import { useRequest } from 'ahooks'
import { CustomerStatsService } from 'services'

export interface CustomerStatsParams {
  merchantId?: number
  timePeriod?: string
  fromDate?: string
  toDate?: string
}

export const useCustomerStats = () => {
  const getCustomerStatsReq = useRequest(
    CustomerStatsService.customerStatsControllerCustomerStats,
    { manual: true }
  )

  const getCustomerStats = (params: CustomerStatsParams) => {
    return getCustomerStatsReq
      .runAsync(params)
      .catch((error) => alertError(error))
  }

  return {
    customerStats: getCustomerStatsReq.data || { revenueAvg: 0, charts: [] },
    getCustomerStats,
  }
}

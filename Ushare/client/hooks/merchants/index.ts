
import { useRequest } from 'ahooks'
import { atom, useRecoilState } from 'recoil'
import { MerchantsService, MerchantEntity } from 'services'

export const merchantsState = atom<MerchantEntity[] | []>({
  key: 'merchantsState',
  default: [],
})

export const useMerchant = () => {
  const [merchants, setMerchants] = useRecoilState(merchantsState)
  const getMerchantsReq = useRequest(MerchantsService.getManyBase, { manual: true })

  const getMerchants = () => {
    getMerchantsReq
      .runAsync({
        limit: 99,
        sort: ['name,ASC'],
      })
      .then(res => {
        setMerchants(res.data)
      })
  }

  return {
    merchants,
    getMerchants
  }
}

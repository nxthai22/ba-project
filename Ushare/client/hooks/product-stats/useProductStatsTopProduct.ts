import { useRequest } from 'ahooks'
import { ProductStatsService } from 'services'
import { alertError } from 'utils'

export interface ProductStatsTopProductParams {
  /**  */
  merchantId?: number
  /** Sắp xếp theo đơn hàng, doanh thu, hoa hồng */
  sort?: string
  /** Khoảng thời gian */
  timePeriod?: string
  /** Ngày bắt đầu */
  fromDate?: string
  /** Ngày kết thúc */
  toDate?: string
}

export const useProductStatsTopProduct = () => {
  const getProductStatsTopProductReq = useRequest(
    ProductStatsService.productStatsControllerOrderRevenueTopPartner,
    { manual: true }
  )

  const getProductStatsTopProduct = (params: ProductStatsTopProductParams) => {
    return getProductStatsTopProductReq
      .runAsync(params)
      .catch((error) => alertError(error))
  }

  return {
    productStatsTopProduct: getProductStatsTopProductReq.data || [],
    getProductStatsTopProduct,
  }
}

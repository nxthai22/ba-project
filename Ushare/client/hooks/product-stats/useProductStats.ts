import { alertError } from 'utils'
import { useRequest } from 'ahooks'
import { ProductStatsService } from 'services'

export interface ProductStatsParams {
  /**  */
  merchantId?: number
  /** Khoảng thời gian */
  timePeriod?: string
  /** Ngày bắt đầu */
  fromDate?: string
  /** Ngày kết thúc */
  toDate?: string
}

export const useProductStats = () => {
  const getProductStatsReq = useRequest(
    ProductStatsService.productStatsControllerCollaboratorStats,
    { manual: true }
  )

  const getProductStats = (params: ProductStatsParams) => {
    return getProductStatsReq
      .runAsync(params)
      .catch((error) => alertError(error))
  }

  return {
    productStats: getProductStatsReq.data?.charts || [],
    getProductStats,
  }
}

import { useRequest } from 'ahooks'
import { GroupBuyingService } from 'services'
import { alertError } from 'utils'

export interface GetOneBaseParams {
  id: number
  fields?: any | null[]
  join?: any | null[]
  cache?: number
}

export const useGetOneGroupBuying = () => {
  const getOneGroupBuyingReq = useRequest(GroupBuyingService.getOneBase, {
    manual: true,
  })

  const getOneGroupBuying = (params: GetOneBaseParams) => {
    getOneGroupBuyingReq.runAsync(params).catch(error => alertError(error))
  }

  return {
    getOneGroupBuying,
    data: getOneGroupBuyingReq.data,
    loading: getOneGroupBuyingReq.loading,
  }
}

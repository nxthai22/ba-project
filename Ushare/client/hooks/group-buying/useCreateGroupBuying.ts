import { useRequest } from 'ahooks'
import { GroupBuyingService, CreateGroupBuyingDto } from 'services'
import { alertError } from 'utils'

export const useCreateGroupBuying = () => {
  const createGroupBuyingReq = useRequest(GroupBuyingService.createOneBase, {
    manual: true,
  })

  const createGroupBuying = (data: CreateGroupBuyingDto) => {
    return createGroupBuyingReq
      .runAsync({ body: data })
      .catch((error) => alertError(error))
  }

  return {
    createGroupBuying,
    data: createGroupBuyingReq.data,
    loading: createGroupBuyingReq.loading,
  }
}

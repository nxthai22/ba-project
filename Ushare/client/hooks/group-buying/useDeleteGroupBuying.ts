import { useRequest } from 'ahooks'
import { GroupBuyingService } from 'services'
import { alertError } from 'utils'

export const useDeleteGroupBuying = () => {
  const deleteGroupBuyingReq = useRequest(
    GroupBuyingService.deleteOneBase,
    { manual: true }
  )

  const deleteGroupBuying = async (id: number) => {
    return deleteGroupBuyingReq
      .runAsync({ id })
      .then(() => true)
      .catch((error) => alertError(error))
  }

  return {
    deleteGroupBuying,
    loading: deleteGroupBuyingReq.loading,
  }
}

import { useRequest } from 'ahooks'
import { GroupBuyingService } from 'services'
import { alertError } from 'utils'

export interface EndOneBaseParams {
  id: number
}

export const useEndGroupBuying = () => {
  const endGroupBuyingReq = useRequest(
    GroupBuyingService.groupBuyingControllerEndCampaign,
    { manual: true }
  )

  const endGroupBuying = async (id: number) => {
    return endGroupBuyingReq
      .runAsync({ id })
      .then(() => true)
      .catch((error) => alertError(error))
  }

  return {
    endGroupBuying,
    loading: endGroupBuyingReq.loading,
  }
}

import { useRequest, useUpdateEffect } from 'ahooks'
import { useState } from 'react'
import { GroupBuyingService } from 'services'
import { alertError } from 'utils'

export interface GetManyBaseParams {
  fields?: any | null[]
  s?: string
  filter?: any | null[]
  or?: any | null[]
  sort?: any | null[]
  join?: any | null[]
  limit?: number
  offset?: number
  page?: number
  cache?: number
}

export const useGetListGroupBuying = () => {
  const [filterParams, setFilterParams] = useState<GetManyBaseParams>({
    sort: ['createdAt,DESC'],
    limit: 10,
    page: 1,
  })

  const getGroupBuyingReq = useRequest(GroupBuyingService.getManyBase, {
    manual: true,
  })

  const getListGroupBuying = (params: GetManyBaseParams) => {
    getGroupBuyingReq.runAsync(params).catch(error => alertError(error))
  }

  const setFilter = (params: GetManyBaseParams) => {
    setFilterParams({ ...filterParams, ...params })
  }
  useUpdateEffect(() => {
    getListGroupBuying(filterParams)
  }, [filterParams])

  return {
    filterParams,
    setFilter,
    listGroupBuying: getGroupBuyingReq.data?.data || [],
    loading: getGroupBuyingReq.loading,
    metadata: {
      total: getGroupBuyingReq.data?.total,
      pageCount: getGroupBuyingReq.data?.pageCount,
    },
  }
}

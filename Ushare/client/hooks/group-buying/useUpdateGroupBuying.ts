import { useRequest } from 'ahooks'
import { GroupBuyingService, UpdateGroupBuyingDto } from 'services'
import { alertError } from 'utils'

export interface UpdateOneBaseParams {
  id: number
  body?: UpdateGroupBuyingDto
}

export const useUpdateGroupBuying = () => {
  const updateGroupBuyingReq = useRequest(GroupBuyingService.updateOneBase, { manual: true })

  const updateGroupBuying = (params: UpdateOneBaseParams) => {
    updateGroupBuyingReq.runAsync(params).catch(error => alertError(error))
  }

  return {
    updateGroupBuying,
    data: updateGroupBuyingReq.data,
    loading: updateGroupBuyingReq.loading,
  }
}

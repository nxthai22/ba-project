import { useRequest } from "ahooks";
import { useMemo } from "react";
import { WardsService } from "services";


export const useWardInfo = (id: number) => {

  const getWardRequest = useRequest(WardsService.getOneBase, { manual: true });

  useMemo(() => {
    if (!id) {
      return;
    }

    getWardRequest.runAsync({ id, join: ['district', 'district.province'], })
  }, [id])

  return {
    ward: id ? getWardRequest.data : undefined
  }
};

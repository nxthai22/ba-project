import { useRequest } from "ahooks";
import { useMemo } from "react";
import { WardsService } from "services";


export const useWard = (districtId: number) => {
  const getWardsRequest = useRequest(WardsService.getManyBase, { manual: true });


  useMemo(() => {
    if (!districtId) {
      return;
    }

    getWardsRequest.runAsync({
      filter: `districtId||$eq||${districtId}`,
      sort: 'name,ASC',
      limit: 100
    })
  }, [districtId]);

  return {
    wards: districtId ? getWardsRequest.data?.data || [] : []
  }
};

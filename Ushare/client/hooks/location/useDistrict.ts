import { useRequest } from "ahooks";
import { useMemo } from "react";
import { DistrictsService } from "services";


export const useDistrict = (provinceId: number) => {
  const getDistrictsRequest = useRequest(DistrictsService.getManyBase, { manual: true });


  useMemo(() => {
    if (!provinceId) {
      return;
    }

    getDistrictsRequest.runAsync({
      filter: `provinceId||$eq||${provinceId}`,
      sort: 'name,ASC',
      limit: 100
    })
  }, [provinceId]);

  return { 
    districts: provinceId ? getDistrictsRequest.data?.data || [] : []
  }
};

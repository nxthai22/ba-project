import { useMount, useRequest } from "ahooks";
import { ProvincesService } from "services";


export const useProvince = () => {
  const getProvincesRequest = useRequest(ProvincesService.getManyBase, { manual: true });

  useMount(() => {
    getProvincesRequest.runAsync({limit: 100});
  });

  return {
    provinces: getProvincesRequest.data?.data || []
  }
};

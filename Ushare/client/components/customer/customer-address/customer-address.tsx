import { yupResolver } from '@hookform/resolvers/yup'
import { usePrevious } from 'ahooks'
import { Col, Form, Input, Row } from 'antd'
import { DistrictSelect, ProvinceSelect, WardSelect } from 'components/location'
import { useWardInfo } from 'hooks/location'
import { useEffect } from 'react'
import { Controller, useForm, useWatch } from 'react-hook-form'
import { WardEntity } from 'services'
import * as yup from 'yup'

const phoneRegExp = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/
const schema = yup.object().shape({
  fullname: yup.string().trim().required('Vui lòng nhập họ tên.'),
  tel: yup.string().trim().required('Vui lòng nhập số điện thoại.').matches(phoneRegExp, 'Số điện thoại không hợp lệ'),
  provinceId: yup.string().trim().required('Vui lòng chọn tỉnh/thành phố.'),
  districtId: yup.string().trim().required('Vui lòng chọn quận/huyện.'),
  wardId: yup.string().trim().required('Vui lòng chọn phường/xã.'),
  shippingAddress: yup.string().trim().required('Vui lòng nhập địa chỉ.'),
})

interface ICustomerAddress {
  fullname: string
  tel: string
  provinceId: number
  districtId: number
  wardId: number
  shippingAddress: string
  ward: WardEntity
}

// eslint-disable-next-line @typescript-eslint/ban-types
type Props = {
  value?: ICustomerAddress
  onChange?: (value: ICustomerAddress) => void
}

const CustomerAddress = (props: Props) => {
  const form = useForm<ICustomerAddress>({
    mode: 'onSubmit',
    resolver: yupResolver(schema),
    defaultValues: props.value,
  })

  const handleForm = async (data) => {
    props.onChange && props.onChange({
      ...data,
      ward
    })
  }

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = form

  const provinceId = useWatch({
    control,
    name: 'provinceId',
  });
  const oldProvinceId = usePrevious(provinceId);

  const districtId = useWatch({
    control,
    name: 'districtId',
  });
  const oldDistrictId = usePrevious(districtId);

  const wardId = useWatch({
    control,
    name: 'wardId',
  });

  const {ward} = useWardInfo(wardId);

  useEffect(() => {
    if (oldProvinceId && provinceId && provinceId !== oldProvinceId) {
      form.setValue('districtId', null)
      form.setValue('wardId', null)
    }
  }, [provinceId]);

  useEffect(() => {
    if (oldDistrictId && districtId && oldDistrictId !== districtId) {
      form.setValue('wardId', null)
    }
  }, [districtId])

  return (
    <Form
      id="customer-address-form"
      layout="vertical"
      onFinish={handleSubmit(handleForm)}
    >
      <Form.Item
        label={'Họ và tên'}
        required
        validateStatus={errors.fullname && 'error'}
        help={errors.fullname?.message}
      >
        <Controller
          control={control}
          name={'fullname'}
          render={({ field }) => <Input {...field} maxLength={200} />}
        />
      </Form.Item>
      <Form.Item
        label={'SĐT'}
        required
        validateStatus={errors.tel && 'error'}
        help={errors.tel?.message}
      >
        <Controller
          control={control}
          name={'tel'}
          render={({ field }) => <Input {...field} />}
        />
      </Form.Item>
      <Row gutter={[8, 0]}>
        <Col xs={24} sm={24} md={8} lg={8}>
          <Form.Item
            label={'Tỉnh/Thành phố'}
            required
            validateStatus={errors.provinceId && 'error'}
          >
            <Controller
              control={control}
              name={'provinceId'}
              render={({ field }) => (
                <ProvinceSelect {...field}></ProvinceSelect>
              )}
            />
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={8} lg={8}>
          <Form.Item
            label={'Quận/Huyện'}
            required
            validateStatus={errors.districtId && 'error'}
          >
            <Controller
              control={control}
              name={'districtId'}
              render={({ field }) => (
                <DistrictSelect
                  provinceId={provinceId}
                  {...field}
                ></DistrictSelect>
              )}
            />
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={8} lg={8}>
          <Form.Item
            label={'Phường/Xã'}
            required
            validateStatus={errors.wardId && 'error'}
          >
            <Controller
              control={control}
              name={'wardId'}
              render={({ field }) => (
                <WardSelect districtId={districtId} {...field}></WardSelect>
              )}
            />
          </Form.Item>
        </Col>
      </Row>
      <Form.Item
        label={'Số nhà, tên đường'}
        required
        validateStatus={errors.shippingAddress && 'error'}
        help={errors.shippingAddress?.message}
      >
        <Controller
          control={control}
          name={'shippingAddress'}
          render={({ field }) => <Input {...field} maxLength={200} />}
        />
      </Form.Item>
    </Form>
  )
}

export default CustomerAddress

import Link from 'components/common/Link'
import { Rate } from 'antd'
import { compactNumber, formatCurrency } from 'utils'
import React, { FC } from 'react'
import { EnumProductEntityStatus, ProductEntity } from 'services'

interface IProps {
  product: ProductEntity
  pub_id?: number
  index?: number
  className?: string
  isFlashsale?: boolean
  salePercent?: number
}

const HomeProductBlock: FC<IProps> = ({
  product,
  pub_id,
  index,
  className,
  isFlashsale,
  salePercent,
}) => {
  let dropPercent = 0
  if (
    product.priceBeforeDiscount &&
    product.priceBeforeDiscount != product.price
  ) {
    dropPercent =
      100 - Math.round((product.price / product.priceBeforeDiscount) * 100)
  }
  return (
    <div className={className + ' h-full'}>
      <div
        key={`top-product-sales-${product.id}`}
        className={'relative h-full'}
      >
        {product.status === EnumProductEntityStatus.inactive && (
          <Link
            href={{
              pathname: `/san-pham/${product.slug}_${product.id}`,
              query: {
                ...(pub_id && { pub_id: Number(pub_id) }),
              },
            }}
          >
            <div
              className={
                'absolute h-full w-full rounded-lg z-20 top-0 left-0 opacity-80 flex justify-center items-center'
              }
              style={{
                backgroundColor: 'white',
              }}
            >
              <p
                className={'m-0 py-1.5 px-5 opacity-50 font-bold rounded-lg'}
                style={{ backgroundColor: 'black', color: 'white' }}
              >
                Tạm hết hàng
              </p>
            </div>
          </Link>
        )}
        <Link
          href={{
            pathname: `/san-pham/${product.slug}_${product.id}`,
            query: {
              ...(pub_id && { pub_id: Number(pub_id) }),
            },
          }}
        >
          <div className={'flex justify-between absolute w-full z-10'}>
            <div>
              {!isNaN(Number(index)) && (
                <div
                  className={'text-white py-1 px-2 md:px-6 rounded-tl-lg rounded-br-lg'}
                  style={{
                    backgroundColor: '#80B4FB',
                  }}
                >
                  {index + 1}
                </div>
              )}
            </div>
            <div className={'flex justify-between'}>
              <div className={'mr-1'}>
                {dropPercent >= 40 && (
                  <div
                    className={
                      'text-white py-[6px] md:py-1 px-1 md:px-3 rounded-br-lg rounded-bl-lg right-1 md:right-2 bg-price text-xs md:text-base'
                    }
                  >
                    Giảm sốc
                  </div>
                )}
              </div>
              <div>
                {dropPercent > 0 && (
                  <div
                    className={`text-white py-1 px-2 md:px-3 rounded-tr-lg rounded-bl-lg right-2 ${
                      !isFlashsale ? 'bg-price' : 'bg-amber-500'
                    } md:text-base text-sm`}
                  >
                    -{dropPercent}%
                  </div>
                )}
              </div>
            </div>
          </div>
          <div
            className={
              'bg-white rounded-lg p-3 flex flex-col h-full justify-end'
            }
          >
            <div className={'my-0'}>
              <div className={'flex justify-center'}>
                <div
                  className={'image-square'}
                  style={{
                    backgroundImage: `url("${
                      product?.images?.[0] || '/logo.svg'
                    }")`,
                  }}
                >
                  {/*<img*/}
                  {/*  className={'rounded-t-lg image'}*/}
                  {/*  src={product?.images?.[0] || '/logo.svg'}*/}
                  {/*  alt={product.name}*/}
                  {/*/>*/}
                </div>
              </div>
              <div
                className={
                  'font-medium md:text-lg text-sm text-primary uppercase line-clamp-2 mt-2'
                }
              >
                {product.name}
              </div>
            </div>
            <div className={'flex flex-col mt-auto mb-0'}>
              <Rate allowHalf value={5} disabled className={'mt-2'} />
              <div className={'flex items-center flex-wrap mt-2'}>
                {product.priceBeforeDiscount !== product.price && (
                  <div
                    className={
                      'text-secondary md:text-lg text-sm font-medium mr-3 line-through'
                    }
                  >
                    {formatCurrency(product.priceBeforeDiscount)}
                  </div>
                )}
                <div className={'text-price md:text-xl text-base font-medium'}>
                  {formatCurrency(product.price)}
                </div>
              </div>
              <div className={'flex flex-col'}>
                {isFlashsale ? (
                  <>
                    <div
                      style={{
                        width: '75%',
                        backgroundColor: '#F4F4F4',
                        height: 6,
                        borderRadius: 3,
                        marginTop: 14,
                        marginBottom: 10,
                      }}
                    >
                      <div
                        className={'bg-price'}
                        style={{
                          width: `${Math.min(salePercent, 100)}%`,
                          height: 6,
                          borderRadius: 3,
                        }}
                      />
                    </div>
                    <div className={'flex items-center text-price'}>
                      <div className={'mr-1'}>
                        Đã bán {compactNumber(product?.saleAmount || 0)}
                      </div>
                      <svg
                        width="9"
                        height="14"
                        viewBox="0 0 9 14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M1.81991 6.4027C1.86173 6.3557 1.90608 6.3108 1.94409 6.261C2.1861 5.94531 2.33815 5.57772 2.44839 5.18628C2.60234 4.6384 2.66442 4.07438 2.66506 3.50265C2.66632 2.89935 2.74108 2.31218 2.96535 1.75659C3.2967 0.935817 3.85358 0.372503 4.59102 0.0161348C4.59989 0.0112242 4.60939 0.00841814 4.62966 0C4.61382 0.0441953 4.60179 0.0799724 4.58912 0.115048C4.39145 0.670646 4.41616 1.22905 4.57898 1.78675C4.75194 2.37813 5.03956 2.90216 5.37597 3.39392C5.66804 3.82114 5.97404 4.23573 6.2718 4.65874C6.73365 5.31536 7.03268 6.05194 7.13721 6.87973C7.16382 7.09088 7.1822 7.30274 7.20374 7.5146C7.20817 7.55809 7.21324 7.60158 7.22084 7.67033C7.26772 7.59878 7.30447 7.54967 7.33488 7.49636C7.54205 7.13508 7.65165 6.73452 7.70993 6.31571C7.78976 5.74188 7.76759 5.16944 7.68586 4.59841C7.68269 4.57666 7.68206 4.55492 7.696 4.52195C7.8037 4.6868 7.91647 4.84745 8.01784 5.01651C8.51643 5.85131 8.81483 6.76889 8.939 7.75872C9.00109 8.25539 9.01756 8.75347 8.98018 9.25294C8.91176 10.1775 8.70649 11.0551 8.22057 11.8275C7.49643 12.978 6.51951 13.7132 5.27397 13.9994C5.267 14.0008 5.25877 13.9994 5.24546 13.9994C5.27904 13.9615 5.30882 13.9264 5.33923 13.892C5.77447 13.4143 6.02218 12.8342 6.09884 12.1642C6.16156 11.6198 6.13369 11.0839 5.962 10.5641C5.84099 10.1972 5.63509 9.88922 5.41462 9.59037C5.2594 9.37992 5.10038 9.17087 4.96354 8.94568C4.69112 8.49671 4.60115 7.98952 4.61763 7.45567C4.62206 7.32168 4.63537 7.18839 4.64614 7.04038C4.61699 7.05791 4.59102 7.07054 4.56694 7.08808C3.98535 7.50899 3.59446 8.09545 3.35878 8.81169C3.14655 9.45498 3.09586 10.1214 3.15288 10.8005C3.16619 10.959 3.18393 11.1176 3.20103 11.288C3.00337 11.1547 2.85955 10.9766 2.74615 10.7654C2.63401 10.5571 2.56622 10.3291 2.5035 10.0884C2.47373 10.1642 2.43952 10.2379 2.41417 10.315C2.27733 10.7352 2.25009 11.1688 2.2938 11.6093C2.35019 12.1825 2.4997 12.7226 2.78226 13.2116C2.94065 13.4852 3.13324 13.7258 3.36068 13.932C3.37589 13.9461 3.39046 13.9608 3.4196 13.9882C3.12374 13.9271 2.85195 13.845 2.5884 13.7335C1.75719 13.382 1.07614 12.8208 0.599083 11.9776C0.310822 11.4683 0.148002 10.9099 0.0751451 10.3157C0.0231948 9.88992 -0.00594799 9.4627 0.00102095 9.03267C0.0117911 8.39499 0.104922 7.77135 0.265841 7.16104C0.407754 6.62368 0.567406 6.09123 0.703617 5.55177C0.828424 5.0572 0.843629 4.55632 0.702983 4.05895C0.701083 4.05193 0.702983 4.04281 0.702983 4.01826C1.0641 4.29606 1.34983 4.63138 1.55003 5.05299C1.74959 5.4739 1.83512 5.92287 1.80091 6.3999C1.80724 6.39919 1.81358 6.4013 1.81991 6.4027Z"
                          fill="#F4447F"
                        />
                      </svg>
                    </div>
                  </>
                ) : (
                  <div className={'flex items-center text-secondary'}>
                    <svg
                      width="11"
                      height="12"
                      viewBox="0 0 11 12"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M10.6613 11.9329C10.799 11.851 10.8465 11.7351 10.8283 11.5727C10.7329 10.7281 10.645 9.88261 10.5542 9.03756C10.4733 8.28138 10.3923 7.52473 10.3113 6.76856C10.2294 6.00447 10.1471 5.24038 10.0647 4.47629C10.0252 4.1096 9.98839 3.74291 9.94511 3.37669C9.92092 3.17147 9.82831 3.09283 9.62077 3.09236C9.1196 3.09097 8.61889 3.0919 8.11772 3.0919C8.07584 3.0919 8.03396 3.0919 7.98045 3.0919C7.98045 2.92903 7.98231 2.77826 7.97998 2.62795C7.96416 1.63818 7.54256 0.874087 6.69704 0.355232C5.7026 -0.254829 4.33357 -0.0598517 3.53365 0.792187C3.04784 1.30965 2.81144 1.9225 2.80726 2.62842C2.80632 2.77873 2.80726 2.92903 2.80726 3.09143C2.75235 3.09143 2.71046 3.09143 2.66905 3.09143C2.19114 3.09143 1.71371 3.09097 1.2358 3.09143C0.998477 3.0919 0.911459 3.16263 0.884934 3.39949C0.807688 4.08587 0.736025 4.77317 0.662036 5.46002C0.580136 6.22411 0.497771 6.9882 0.415871 7.75229C0.334902 8.50846 0.253467 9.26511 0.172498 10.0213C0.117122 10.5383 0.0664005 11.0562 0.00497532 11.5723C-0.0141039 11.7347 0.0342913 11.8505 0.172032 11.9324C3.6686 11.9329 7.1647 11.9329 10.6613 11.9329ZM9.43743 3.61727C9.71477 6.20549 9.99118 8.78813 10.2681 11.3717C7.00835 11.3717 3.76725 11.3717 0.520573 11.3717C0.798381 8.78022 1.07479 6.19898 1.3512 3.6196C1.86261 3.6196 2.36146 3.6196 2.86961 3.6196C2.86961 3.74756 2.86496 3.86762 2.87101 3.98721C2.8752 4.06725 2.84914 4.10774 2.77468 4.14636C2.51549 4.27945 2.37402 4.49955 2.36565 4.79086C2.35727 5.07658 2.48105 5.30459 2.72443 5.4549C3.09344 5.68292 3.56623 5.5596 3.77423 5.18919C3.99201 4.80156 3.85195 4.34273 3.45454 4.13612C3.42337 4.11984 3.3773 4.09099 3.3759 4.06586C3.36846 3.91648 3.37218 3.76664 3.37218 3.61913C4.74866 3.61913 6.10652 3.61913 7.47276 3.61913C7.47276 3.74245 7.46764 3.85878 7.47462 3.97465C7.47974 4.06213 7.45415 4.10867 7.37038 4.15241C6.90877 4.39485 6.83199 5.03423 7.22101 5.373C7.45926 5.58054 7.73288 5.63219 8.02326 5.50748C8.31782 5.38091 8.4765 5.14172 8.48162 4.82297C8.48627 4.51351 8.33643 4.28317 8.06141 4.13612C8.0307 4.11984 7.98603 4.09052 7.9851 4.06493C7.97765 3.91555 7.98138 3.76571 7.98138 3.61773C8.4751 3.61727 8.95161 3.61727 9.43743 3.61727ZM3.32751 3.08585C3.34472 2.78803 3.33402 2.50138 3.38242 2.2245C3.58298 1.08349 4.67839 0.338945 5.80638 0.580457C6.91993 0.818711 7.59933 1.8043 7.42855 3.08585C6.07162 3.08585 4.71422 3.08585 3.32751 3.08585ZM7.7152 5.06262C7.58304 5.06355 7.47695 4.95885 7.47741 4.82808C7.47788 4.69919 7.58863 4.5903 7.7166 4.59262C7.84038 4.59495 7.94555 4.69965 7.9488 4.8239C7.95206 4.95326 7.84596 5.06169 7.7152 5.06262ZM3.07901 5.06215C2.94639 5.06355 2.84076 4.96071 2.84123 4.82902C2.84123 4.69919 2.95012 4.59076 3.07901 4.59262C3.2028 4.59448 3.30843 4.69872 3.31215 4.82297C3.31634 4.95187 3.21024 5.06076 3.07901 5.06215Z"
                        fill="#797E8E"
                      />
                    </svg>
                    <div className={'ml-2'}>
                      {compactNumber(product?.saleAmount || 0)}
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </Link>
      </div>
    </div>
  )
}

export default HomeProductBlock

import React, { FC, useEffect, useState } from 'react'
import { Controller, useFormContext, useWatch } from 'react-hook-form'
import {
  DistrictEntity,
  DistrictsService,
  EnumCreateOrderDtoPaymentType,
  EnumVoucherEntityDiscountType,
  EnumVoucherEntityType,
  OrdersService,
  ProductsService,
  ProvinceEntity,
  ProvincesService,
  VoucherEntity,
  VouchersService,
  WardEntity,
  WardsService,
} from 'services'
import {
  alertError,
  filterOption,
  formatCurrency,
  getDistrictName,
  getFullAddress,
  getProvinceName,
  getWardName,
  modifyEntity,
} from 'utils'
import {
  Button,
  Card,
  Descriptions,
  Form,
  Input,
  Modal,
  Radio,
  Select,
} from 'antd'
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import {
  buyNowCartState,
  cartState,
  childSelectedRowKeysState,
  loadingState,
  orderInfoState,
} from 'recoil/Atoms'
import { destroyCookie, parseCookies, setCookie } from 'nookies'
import { useRouter } from 'next/router'
import { groupBy } from 'lodash'
import { useAddCartProducts } from 'recoil/Hooks'
import { format, formatDistance } from 'date-fns'
import { vi } from 'date-fns/locale'
import { Voucher } from '../../constants/icons'
import { ModalOrderSuccess } from 'components/guest/common/Modal/ModalOrderSuccess'
import { ModalOrderError } from 'components/guest/common/Modal/ModalOrderError'

const OrderCheckout: FC = () => {
  const addProduct = useAddCartProducts()
  const [orderInfo, setOrderInfo] = useRecoilState(orderInfoState)
  const childSelectedRowKeys = useRecoilValue(childSelectedRowKeysState)
  const {
    control,
    setValue,
    getValues,
    formState: { errors },
    handleSubmit,
  } = useFormContext()

  const setIsLoading = useSetRecoilState(loadingState)
  const [provinces, setProvinces] = useState<ProvinceEntity[]>([])
  const [districts, setDistricts] = useState<DistrictEntity[]>([])
  const [wards, setWards] = useState<WardEntity[]>([])
  const [ward, setWard] = useState<WardEntity>()
  const cart = useRecoilValue(cartState)
  const buyNow = useRecoilValue(buyNowCartState)
  const { customer } = parseCookies(null)
  const router = useRouter()
  const { pub_id = 0 } = router.query
  const [totalPriceBeforeDiscount, setTotalPriceBeforeDiscount] = useState(0)
  const [totalPrice, setTotalPrice] = useState(0)
  const [total, setTotal] = useState(0)
  const [totalVoucherDiscount, setTotalVoucherDiscount] = useState(0)
  const [orderIds, setOrderIds] = useState([])
  const [vouchers, setVouchers] = useState<VoucherEntity[]>([])
  const [isShowVoucherUshareModal, setIsShowVoucherUshareModal] =
    useState<boolean>(false)
  const [selectedVoucherUshare, setSelectedVoucherUshare] =
    useState<VoucherEntity>(null)
  const [isShowErrorProduct, setIsShowErrorProduct] = useState<boolean>(false)
  const [messageErrorProduct, setMessageErrorProduct] = useState<string>('')
  const watchProvinceId = useWatch({
    control,
    name: 'provinceId',
    defaultValue: 0,
  })
  const watchDistrictId = useWatch({
    control,
    name: 'districtId',
    defaultValue: 0,
  })
  const watchWardId = useWatch({
    control,
    name: 'wardId',
    defaultValue: 0,
  })

  const watchPaymentType = useWatch({
    control,
    name: 'paymentType',
  })
  useEffect(() => {
    if (watchWardId && watchWardId !== 0) {
      WardsService.getOneBase({
        id: watchWardId,
        join: ['district', 'district.province'],
      })
        .then((wardResponse) => setWard(wardResponse))
        .catch((e) => alertError(e))
    }
  }, [])
  useEffect(() => {
    if (watchWardId && watchWardId !== 0) {
      WardsService.getOneBase({
        id: watchWardId,
        join: ['district', 'district.province'],
      })
        .then((wardResponse) => setWard(wardResponse))
        .catch((e) => alertError(e))
    }
  }, [watchWardId])
  useEffect(() => {
    if (watchDistrictId && watchDistrictId != 0)
      WardsService.getManyBase({
        filter: `districtId||eq||${watchDistrictId}`,
        limit: 100,
        sort: ['name,ASC'],
      })
        .then((response) => {
          setWards(response.data)
        })
        .catch((error) => alertError(error))
  }, [watchDistrictId])
  useEffect(() => {
    if (watchProvinceId && watchProvinceId != 0)
      DistrictsService.getManyBase({
        filter: `provinceId||eq||${watchProvinceId}`,
        limit: 100,
        sort: ['name,ASC'],
      })
        .then((response) => {
          setDistricts(response.data)
        })
        .catch((error) => alertError(error))
  }, [watchProvinceId])
  useEffect(() => {
    ProvincesService.getManyBase({
      limit: 100,
      sort: ['position,ASC'],
    })
      .then((response) => setProvinces(response.data))
      .catch((error) => alertError(error))
  }, [])
  useEffect(() => {
    if (customer) {
      const customerDecoded = JSON.parse(customer)

      setValue('fullname', customerDecoded.fullname)
      setValue('tel', customerDecoded.tel)
      setValue('shippingAddress', customerDecoded.shippingAddress)
      setValue('wardId', customerDecoded.wardId)
      setValue('note', customerDecoded.note)

      if (customerDecoded.wardId)
        WardsService.getOneBase({
          id: customerDecoded.wardId,
          join: ['district', 'district.province'],
        })
          .then((wardResponse) => {
            setWard(wardResponse)
            setValue('districtId', wardResponse.districtId)
            setValue('provinceId', wardResponse.district.provinceId)
          })
          .catch((e) => alertError(e))
    }
  }, [customer])

  useEffect(() => {
    VouchersService.voucherControllerGetManyVoucherForGuest()
      .then((response) => {
        setVouchers(response.data)
      })
      .catch((e) => {
        alertError(e)
      })
  }, [])
  useEffect(() => {
    const merchantAddresses = getValues('merchantAddresses')
    let tmpCart
    const selectedProducts = []
    if (buyNow?.[Number(pub_id)]) tmpCart = { ...buyNow }
    else tmpCart = { ...cart }
    console.log('tmpCart', tmpCart)
    if (tmpCart?.[Number(pub_id)]) {
      Object.entries(tmpCart?.[Number(pub_id)]).map((merchantAdress) => {
        merchantAdress?.[1]?.map((product) => {
          const childRowKey = `${product?.merchantAddressId}-${product?.id}-${product?.productVariantId}`
          if (childSelectedRowKeys.includes(childRowKey)) {
            selectedProducts.push(product)
          }
        })
      })
      const productIds = Object.keys(groupBy(selectedProducts, 'id')).map(
        (productId) => Number(productId)
      )
      if (productIds.length > 0) {
        ProductsService.getManyBase({
          filter: [`id||in||${productIds.join(',')}`],
        }).then((response) => {
          let totalPrice = 0
          let tmpTotalPriceBeforeDiscount = 0
          let tmpTotalVoucherDiscount = 0
          merchantAddresses?.map((merchantAddress) => {
            let merchantAddressPriceBeforeDiscount = 0
            let merchantAddressPrice = 0
            let merchantAddressVoucherDiscount = 0
            merchantAddress?.products?.map((product) => {
              if (productIds.includes(product?.id)) {
                merchantAddressPriceBeforeDiscount +=
                  product.priceBeforeDiscount * product.quantity
                merchantAddressPrice += product.price * product.quantity
                if (
                  merchantAddress?.voucher?.quantityUsed <
                  merchantAddress?.voucher?.quantity
                ) {
                  let discount = 0
                  switch (merchantAddress?.voucher?.type) {
                    case EnumVoucherEntityType.shop:
                      discount =
                        (merchantAddress?.voucher?.discountType ===
                        EnumVoucherEntityDiscountType.amount
                          ? Number(merchantAddress?.voucher?.discountValue || 0)
                          : (Number(product.price || 0) *
                              Number(
                                merchantAddress?.voucher?.discountValue || 0
                              )) /
                            100) * product.quantity
                      break
                    case EnumVoucherEntityType.product:
                      if (
                        merchantAddress?.voucher?.productIds?.includes(
                          product.id
                        )
                      ) {
                        discount =
                          (merchantAddress?.voucher?.discountType ===
                          EnumVoucherEntityDiscountType.amount
                            ? Number(
                                merchantAddress?.voucher?.discountValue || 0
                              )
                            : (Number(product.price || 0) *
                                Number(
                                  merchantAddress?.voucher?.discountValue || 0
                                )) /
                              100) * product.quantity
                      }
                      break
                  }
                  merchantAddressVoucherDiscount += discount
                  product.priceAfterDiscount =
                    product.price * product.quantity - discount
                }
              }
            })
            merchantAddress.totalPriceAfterDiscount =
              merchantAddressPrice - merchantAddressVoucherDiscount

            totalPrice += merchantAddressPrice
            tmpTotalPriceBeforeDiscount += merchantAddressPriceBeforeDiscount
            tmpTotalVoucherDiscount += merchantAddressVoucherDiscount
          })
          // const totalPrice = selectedProducts.reduce(
          //   (totalPrice, selectedProduct) => {
          //     const merchantAddress = merchantAddresses?.find(
          //       (tmpMerchantAddress) =>
          //         Number(tmpMerchantAddress.merchantAddressId) ===
          //         selectedProduct.merchantAddressId
          //     )
          //     const productData = response.data.find(
          //       (product) => product.id === selectedProduct.id
          //     )
          //     if (
          //       merchantAddress?.voucher?.quantityUsed <
          //       merchantAddress?.voucher?.quantity
          //     )
          //       switch (merchantAddress?.voucher?.type) {
          //         case EnumVoucherEntityType.shop:
          //           tmpTotalVoucherDiscount +=
          //             (merchantAddress?.voucher?.discountType ===
          //             EnumVoucherEntityDiscountType.amount
          //               ? Number(merchantAddress?.voucher?.discountValue || 0)
          //               : (Number(productData.price || 0) *
          //                   Number(
          //                     merchantAddress?.voucher?.discountValue || 0
          //                   )) /
          //                 100) * selectedProduct.quantity
          //           break
          //         case EnumVoucherEntityType.product:
          //           if (
          //             merchantAddress?.voucher?.productIds?.includes(
          //               productData.id
          //             )
          //           ) {
          //             tmpTotalVoucherDiscount +=
          //               (merchantAddress?.voucher?.discountType ===
          //               EnumVoucherEntityDiscountType.amount
          //                 ? Number(merchantAddress?.voucher?.discountValue || 0)
          //                 : (Number(productData.price || 0) *
          //                     Number(
          //                       merchantAddress?.voucher?.discountValue || 0
          //                     )) /
          //                   100) * selectedProduct.quantity
          //           }
          //           break
          //       }
          //     // if (
          //     //   merchantAddress?.voucher?.type === 'product' &&
          //     //   merchantAddress?.voucher?.productIds?.includes(productData.id)
          //     // ) {
          //     //   if (merchantAddress?.voucher?.discountType === 'amount') {
          //     //     tmpTotalVoucherDiscount +=
          //     //       merchantAddress?.voucher?.discountValue
          //     //   } else {
          //     //     tmpTotalVoucherDiscount += Math.round(
          //     //       (productData?.price *
          //     //         selectedProduct.quantity *
          //     //         merchantAddress?.voucher?.discountValue) /
          //     //         100
          //     //     )
          //     //   }
          //     // } else if (
          //     //   merchantAddress?.voucher?.type === 'shop' &&
          //     //   merchantAddress?.voucher?.merchantId === productData.merchantId
          //     // ) {
          //     //   if (merchantAddress?.voucher?.discountType === 'amount') {
          //     //     tmpTotalVoucherDiscount +=
          //     //       merchantAddress?.voucher?.discountValue
          //     //   } else {
          //     //     tmpTotalVoucherDiscount += Math.round(
          //     //       (productData?.price *
          //     //         selectedProduct.quantity *
          //     //         merchantAddress?.voucher?.discountValue) /
          //     //         100
          //     //     )
          //     //   }
          //     // }
          //
          //     tmpTotalPriceBeforeDiscount +=
          //       productData.priceBeforeDiscount * selectedProduct.quantity
          //     return totalPrice + productData.price * selectedProduct.quantity
          //   },
          //   0
          // )
          setTotalPrice(tmpTotalPriceBeforeDiscount - totalPrice)
          setTotalPriceBeforeDiscount(tmpTotalPriceBeforeDiscount)
          setTotal(totalPrice - tmpTotalVoucherDiscount)
          setTotalVoucherDiscount(tmpTotalVoucherDiscount)
        })
      } else {
        setTotalPrice(0)
        setTotalPriceBeforeDiscount(0)
        setTotal(0)
        setTotalVoucherDiscount(0)
      }
    }
  }, [pub_id, childSelectedRowKeys, cart, getValues('merchantAddresses')])

  const onSubmit = (data) => {
    if (childSelectedRowKeys.length > 0) {
      const order = {
        fullname: data.fullname,
        tel: data.tel,
        wardId: data.wardId,
        note: data.note,
        shippingAddress: data.shippingAddress,
        paymentType: data.paymentType,
      }
      setCookie(null, 'customer', JSON.stringify(order), {
        path: '/',
      })
      setOrderInfo(order)
    } else {
      alertError('Vui lòng tick chọn sản phẩm')
    }
  }
  const submitOrder = () => {
    if (orderInfo) {
      const ushareVoucher = getValues('voucherUshareCode')
      const orderProducts = []
      const orderInfoData = []
      getValues('merchantAddresses').map((merchantAddress) => {
        const voucherCodes = []
        if (merchantAddress?.voucher?.code)
          voucherCodes.push(merchantAddress?.voucher?.code)
        if (ushareVoucher) voucherCodes.push(ushareVoucher)
        const tmpOrderInfo = {
          ...orderInfo,
          pubId: Number(pub_id),
          merchantAddressId: Number(merchantAddress.merchantAddressId),
          voucherCodes,
          products: [],
        }
        merchantAddress.products.map((product) => {
          if (childSelectedRowKeys.includes(product.rowKey)) {
            orderProducts.push({
              quantity: 0,
              id: product.id,
              productVariantId: product.productVariantId,
              merchantAddressId: Number(merchantAddress.merchantAddressId),
            })
            tmpOrderInfo.products.push({
              quantity: product.quantity,
              id: product.id,
              variantId: product.productVariantId,
            })
          }
        })
        if (tmpOrderInfo.products.length > 0) {
          orderInfoData.push(tmpOrderInfo)
        }
      })
      new Promise<any>((resolve) => {
        if (Number(orderInfoData.id))
          return resolve(
            OrdersService.updateOneBase({
              id: orderInfoData.id,
              body: orderInfoData,
            })
          )
        else
          return resolve(
            OrdersService.createOneBase({
              body: orderInfoData,
            })
          )
      })
        .then((response) => {
          setOrderIds(response.map((order) => order.code))
          addProduct(orderProducts, false, Number(pub_id))
          destroyCookie(null, 'customer', { path: '/' })
          setOrderInfo(null)
        })
        .catch((error) => {
          setIsShowErrorProduct(true)
          setMessageErrorProduct(
            error?.response?.data?.message?.message ||
              error?.response?.data?.message
          )
        })
      // modifyEntity(OrdersService, orderInfoData, 'Tạo đơn hàng', (response) => {
      //   setOrderIds(response.map((order) => order.code))
      //   addProduct(orderProducts, false, Number(pub_id))
      //   destroyCookie(null, 'customer', {
      //     path: '/',
      //   })
      //   setOrderInfo(null)
      // })
      //   .then((response) => {
      //     console.log('response', response)
      //   })
      //   .catch((err) => {
      //     console.log('err', err)
      //     setIsShowErrorProduct(true)
      //     setMessageErrorProduct(err)
      //   })
    } else {
      alertError('Dữ liệu đơn hàng lỗi! Vui lòng liên hệ CSKH')
    }
  }
  const applyVoucher = (value) => {
    setIsLoading(true)
    const merchantAddresses = getValues('merchantAddresses')
    if (!merchantAddresses || merchantAddresses?.length < 1)
      alertError('Chưa chọn sản phẩm để áp dụng mã')
    const selectedProducts = []
    const tmpCart = { ...cart }
    if (tmpCart?.[Number(pub_id)]) {
      Object.entries(tmpCart?.[Number(pub_id)]).map((merchantAdress) => {
        merchantAdress[1]?.map((product) => {
          const childRowKey = `${product.merchantAddressId}-${product.id}-${product.productVariantId}`
          if (childSelectedRowKeys.includes(childRowKey)) {
            selectedProducts.push(product)
          }
        })
      })
      const productIds = Object.keys(groupBy(selectedProducts, 'id')).map(
        (productId) => Number(productId)
      )
      if (productIds?.length > 0) {
        VouchersService.voucherControllerGetOneByCode({
          code: value,
        })
          .then((voucher) => {
            setIsLoading(false)
            setSelectedVoucherUshare(voucher)
            setIsShowVoucherUshareModal(false)
            let tmpTotalVoucherDiscount = totalVoucherDiscount
            merchantAddresses.map((merchantAddress) => {
              let ushareVoucherDiscount = 0
              merchantAddress?.products?.map((product) => {
                if (productIds.includes(product.id)) {
                  if (voucher?.quantityUsed < voucher?.quantity) {
                    let discount = 0
                    switch (voucher?.type) {
                      case EnumVoucherEntityType.shop:
                        discount =
                          (voucher?.discountType ===
                          EnumVoucherEntityDiscountType.amount
                            ? Number(voucher?.discountValue || 0)
                            : (Number(
                                product.priceAfterDiscount || product.price || 0
                              ) *
                                Number(voucher?.discountValue || 0)) /
                              100) * product.quantity
                        break
                      case EnumVoucherEntityType.product:
                        if (voucher?.productIds?.includes(product.id)) {
                          discount =
                            (voucher?.discountType ===
                            EnumVoucherEntityDiscountType.amount
                              ? Number(voucher?.discountValue || 0)
                              : (Number(
                                  product.priceAfterDiscount ||
                                    product.price ||
                                    0
                                ) *
                                  Number(voucher?.discountValue || 0)) /
                                100) * product.quantity
                        }
                        break
                    }
                    ushareVoucherDiscount += discount
                  }
                }
              })
              tmpTotalVoucherDiscount += ushareVoucherDiscount
            })
            setTotalVoucherDiscount(tmpTotalVoucherDiscount)
            setTotal(
              total - tmpTotalVoucherDiscount < 0
                ? 0
                : total - tmpTotalVoucherDiscount
            )
          })
          .catch((e) => {
            alertError(e)
            setIsLoading(false)
            setSelectedVoucherUshare(null)
            setIsShowVoucherUshareModal(false)
            setTotalPrice(0)
            setTotalPriceBeforeDiscount(0)
            setTotal(0)
            setTotalVoucherDiscount(0)
          })
      } else {
        alertError('Chưa chọn sản phẩm để áp dụng mã')
        setTotalPrice(0)
        setTotalPriceBeforeDiscount(0)
        setTotal(0)
        setTotalVoucherDiscount(0)
      }
    }
  }
  return (
    <Card
      title={'Thông tin người mua'}
      extra={
        orderInfo && (
          <Button
            type={'text'}
            size={'small'}
            onClick={() => setOrderInfo(null)}
          >
            Sửa
          </Button>
        )
      }
    >
      {orderInfo ? (
        <>
          <Descriptions column={1}>
            <Descriptions.Item label="Họ và tên">
              <strong>{orderInfo?.fullname}</strong>
            </Descriptions.Item>
            <Descriptions.Item label="Số điện thoại">
              <strong>{orderInfo?.tel}</strong>
            </Descriptions.Item>
            <Descriptions.Item label="Địa chỉ">
              <strong>
                {orderInfo?.shippingAddress}, {getFullAddress(ward)}
              </strong>
            </Descriptions.Item>
            <Descriptions.Item label="Ghi chú">
              <strong> {orderInfo?.note}</strong>
            </Descriptions.Item>
          </Descriptions>
        </>
      ) : (
        <>
          <Form.Item
            label={
              <>
                <span>Người mua</span>
                <span style={{ color: '#F4447F' }} className={'ml-1'}>
                  (*)
                </span>
              </>
            }
            labelCol={{ md: 24 }}
            labelAlign={'left'}
            validateStatus={errors.fullname && 'error'}
            help={errors.fullname?.message}
          >
            <Controller
              control={control}
              name={'fullname'}
              render={({ field }) => (
                <Input {...field} className={'rounded-md'} />
              )}
            />
          </Form.Item>
          <Form.Item
            label={
              <>
                <span>Số điện thoại</span>
                <span style={{ color: '#F4447F' }} className={'ml-1'}>
                  (*)
                </span>
              </>
            }
            labelCol={{ md: 24 }}
            labelAlign={'left'}
            validateStatus={errors.tel && 'error'}
            help={errors.tel?.message}
          >
            <Controller
              control={control}
              name={'tel'}
              render={({ field }) => (
                <Input
                  {...field}
                  className={'rounded-md'}
                  placeholder={'Nhập số điện thoại'}
                />
              )}
            />
          </Form.Item>
          <Form.Item
            label={
              <>
                <span>Địa chỉ nhận hàng</span>
                <span style={{ color: '#F4447F' }} className={'ml-1'}>
                  (*)
                </span>
              </>
            }
            labelCol={{ md: 24 }}
            labelAlign={'left'}
            validateStatus={errors.shippingAddress && 'error'}
            help={errors.shippingAddress?.message}
          >
            <Controller
              control={control}
              name={'shippingAddress'}
              render={({ field }) => (
                <Input
                  {...field}
                  className={'rounded-md'}
                  placeholder={'Nhập số nhà/địa chỉ cụ thể'}
                />
              )}
            />
          </Form.Item>
          <Form.Item
            label={
              <>
                <span>Tỉnh/Thành phố</span>
                <span style={{ color: '#F4447F' }} className={'ml-1'}>
                  (*)
                </span>
              </>
            }
            labelCol={{ md: 24 }}
            labelAlign={'left'}
            validateStatus={errors.provinceId && 'error'}
            help={errors.provinceId?.message}
          >
            <Controller
              control={control}
              name={'provinceId'}
              render={({ field }) => (
                <Select
                  {...field}
                  filterOption={filterOption}
                  showSearch
                  placeholder={'Chọn Tỉnh/Thành phố'}
                  dropdownClassName={'rounded-md'}
                  className={'rounded-md'}
                >
                  {provinces.map((province) => (
                    <Select.Option
                      value={province.id}
                      key={`province-${province.id}`}
                    >
                      {getProvinceName(province)}
                    </Select.Option>
                  ))}
                </Select>
              )}
            />
          </Form.Item>
          <Form.Item
            label={
              <>
                <span>Quận/Huyện</span>
                <span style={{ color: '#F4447F' }} className={'ml-1'}>
                  (*)
                </span>
              </>
            }
            labelCol={{ md: 24 }}
            labelAlign={'left'}
            validateStatus={errors.districtId && 'error'}
            help={errors.districtId?.message}
          >
            <Controller
              control={control}
              name={'districtId'}
              render={({ field }) => (
                <Select
                  disabled={districts.length === 0}
                  filterOption={filterOption}
                  showSearch
                  {...field}
                  placeholder={'Chọn Quận/Huyện'}
                >
                  {districts.map((district) => (
                    <Select.Option
                      value={district.id}
                      key={`district-${district.id}`}
                    >
                      {getDistrictName(district)}
                    </Select.Option>
                  ))}
                </Select>
              )}
            />
          </Form.Item>
          <Form.Item
            label={
              <>
                <span>Phường/Xã</span>
                <span style={{ color: '#F4447F' }} className={'ml-1'}>
                  (*)
                </span>
              </>
            }
            labelCol={{ md: 24 }}
            labelAlign={'left'}
            validateStatus={errors.wardId && 'error'}
            help={errors.wardId?.message}
          >
            <Controller
              control={control}
              name={'wardId'}
              render={({ field }) => (
                <Select
                  disabled={wards.length === 0}
                  filterOption={filterOption}
                  showSearch
                  {...field}
                  placeholder={'Chọn Phường/Xã'}
                >
                  {wards.map((ward) => (
                    <Select.Option value={ward.id} key={`ward-${ward.id}`}>
                      {getWardName(ward)}
                    </Select.Option>
                  ))}
                </Select>
              )}
            />
          </Form.Item>
          <Form.Item
            label={'Ghi chú'}
            labelCol={{ md: 24 }}
            labelAlign={'left'}
            validateStatus={errors.note && 'error'}
            help={errors.note?.message}
          >
            <Controller
              control={control}
              name={'note'}
              render={({ field }) => (
                <Input.TextArea
                  {...field}
                  rows={2}
                  className={'rounded-md'}
                  placeholder={
                    'Nhập mã hóa đơn thanh toán trả trước hoặc lưu ý cho đơn hàng'
                  }
                />
              )}
            />
          </Form.Item>
          <div className={'flex mb-6'}>
            <div
              className={'flex-1 text-secondary'}
              style={{ fontWeight: 'bold' }}
            >
              Ưu đãi Ushare
            </div>
            {selectedVoucherUshare ? (
              <>
                <div
                  className={'border rounded text-price p-1'}
                  style={{ borderColor: '#F4447F', cursor: 'pointer' }}
                  onClick={() => {
                    setIsShowVoucherUshareModal(true)
                  }}
                >
                  {selectedVoucherUshare?.subTitle}
                </div>
              </>
            ) : (
              <div
                className={'font-normal cursor-pointer'}
                style={{
                  color: '#80B4FB',
                }}
                onClick={() => {
                  if (!(totalPrice > 0) || !(totalPriceBeforeDiscount > 0))
                    alertError('Chưa chọn sản phẩm để áp dụng mã')
                  setIsShowVoucherUshareModal(
                    totalPrice > 0 || totalPriceBeforeDiscount > 0
                  )
                }}
              >
                Chọn hoặc nhập mã
              </div>
            )}
          </div>
        </>
      )}
      <Form.Item>
        <hr />
        <div className={'my-3 text-primary font-medium'}>
          Thông tin thanh toán
        </div>
        <div
          className={
            'border rounded-md px-2 md:px-5 py-3 md:text-base text-secondary'
          }
        >
          <div
            className={
              'flex items-center justify-between border-b border-gray-200 py-2'
            }
          >
            <span className={'font-medium'}>Tổng tiền hàng</span>
            <div>
              <span className={'md:text-lg'}>
                {formatCurrency(totalPriceBeforeDiscount)}
              </span>
            </div>
          </div>
          <div
            className={
              'flex items-center justify-between border-b border-gray-200 py-2'
            }
          >
            <span className={'font-medium text-secondary'}>
              Giảm giá tiền hàng
            </span>
            <div>
              <span className={'md:text-lg'}>
                {totalPrice
                  ? `-${formatCurrency(totalPrice)}`
                  : `${formatCurrency(totalPrice)}`}
              </span>
            </div>
          </div>
          <div
            className={
              'flex items-center justify-between border-b border-gray-200 py-2'
            }
          >
            <span className={'font-medium'}>Giảm giá từ mã ưu đãi</span>
            <div>
              <span className={'md:text-lg'}>
                {totalVoucherDiscount
                  ? `-${formatCurrency(totalVoucherDiscount)}`
                  : `${formatCurrency(totalVoucherDiscount)}`}
              </span>
            </div>
          </div>
          <div
            className={
              'flex items-center justify-between border-b border-gray-200 py-2'
            }
          >
            <span className={'font-medium'}>Phí vận chuyển</span>
            <div>
              <span className={'md:text-lg'}>{formatCurrency(0)}</span>
            </div>
          </div>
          <div className={'flex items-center justify-between pt-2'}>
            <span className={'font-medium'}>Tổng thanh toán</span>
            <div>
              <span className={'text-price md:text-lg'}>
                {formatCurrency(total)}
              </span>
            </div>
          </div>
        </div>
        <div className={'md:text-base font-normal text-secondary mt-3'}>
          Bạn đã tiết kiệm được{' '}
          <span className={'text-price'}>
            {formatCurrency(totalPriceBeforeDiscount - total)}
          </span>{' '}
          từ đơn hàng này
        </div>
        <div className={'text-primary font-medium mt-3'}>
          Chọn phương thức thanh toán
        </div>
        <div className={'payment-type'}>
          <Controller
            name={'paymentType'}
            control={control}
            render={({ field }) => (
              <Radio.Group
                {...field}
                defaultValue={1}
                className={'mt-2 w-full'}
                disabled={Boolean(orderInfo)}
              >
                <Radio value={'cod'}>
                  <span className={'leading-9'}>Thanh toán khi nhận hàng</span>
                </Radio>
                <Radio value={'bacs'}>
                  <span className={'leading-9'}>Thanh toán trả trước</span>
                  {watchPaymentType === EnumCreateOrderDtoPaymentType.bacs && (
                    <div className={'text-secondary -ml-9'}>
                      Vui lòng nhập Số điện thoại của bạn trong phần Nội dung
                      chuyển khoản. Đơn hàng sẽ đươc chúng tôi giao sau khi tiền
                      đã chuyển.
                      <br />
                      <strong>
                        NGÂN HÀNG QUỐC TẾ VIB
                        <br />
                        Số tài khoản: 051704060048573
                        <br />
                        Chủ tài khoản: Trương Văn Hạnh
                      </strong>
                    </div>
                  )}
                </Radio>
              </Radio.Group>
            )}
          />
        </div>
      </Form.Item>
      <Form.Item>
        {!orderInfo ? (
          <Button
            size={'large'}
            style={{
              backgroundColor: '#F4447F',
              color: '#FFFFFF',
              width: '100%',
              border: 0,
              borderRadius: '4px',
            }}
            onClick={handleSubmit(onSubmit)}
          >
            Đặt hàng ngay
          </Button>
        ) : (
          <Button
            size={'large'}
            style={{
              backgroundColor: '#F4447F',
              color: '#FFFFFF',
              width: '100%',
              border: 0,
            }}
            onClick={handleSubmit(submitOrder)}
          >
            Hoàn tất đơn hàng
          </Button>
        )}
      </Form.Item>
      <Modal
        className={'rounded-modal'}
        title={<label className={'text-primary'}>Ưu đãi từ Ushare</label>}
        visible={isShowVoucherUshareModal}
        footer={false}
        onCancel={() => {
          setIsShowVoucherUshareModal(false)
        }}
        destroyOnClose={true}
      >
        <div className={'flex gap-2'}>
          <Controller
            control={control}
            render={({ field }) => (
              <Input
                {...field}
                className={
                  'border-price rounded-sm h-12 bg-gray-50 font-semibold text-secondary px-5'
                }
                placeholder={'Nhập mã Voucher của Ushare'}
              />
            )}
            name={'voucherUshareCode'}
          />
          <Button
            className={
              'h-12 rounded-sm bg-price text-white border-0 px-6 text-base'
            }
            onClick={() => applyVoucher(getValues('voucherUshareCode'))}
          >
            Áp dụng
          </Button>
        </div>
        {vouchers?.length > 0 ? (
          <>
            <div
              className={'border border-gray-200 p-2 overflow-y-scroll'}
              style={{
                maxHeight: 308,
              }}
            >
              {vouchers?.map((voucher: VoucherEntity) => (
                <div
                  key={`voucher-${voucher.id}`}
                  className={'py-1 flex flex-row'}
                >
                  <div
                    className={'flex-none'}
                    style={{
                      width: 4,
                      height: 72,
                      background:
                        'url("/images/voucher-bg-right.svg") no-repeat',
                    }}
                  />
                  <div
                    className={'grow flex flex-row p-2'}
                    style={{
                      background:
                        'linear-gradient(90deg, #F54C84 -1.6%, rgba(251, 20, 96, 0.69) 118.95%)',
                    }}
                  >
                    <div className="flex-none">
                      <img src="/images/voucher.svg" alt="Voucher" />
                    </div>
                    <div className={'grow text-white ml-2'}>
                      <div className={'font-bold text-sm'}>{voucher.name}</div>
                      <div className={'flex flex-row justify-between'}>
                        <div className={'text-xs'}>
                          <div>
                            {format(new Date(voucher.endDate), 'd/M/y', {
                              locale: vi,
                            })}
                          </div>
                          <div>
                            {formatDistance(
                              new Date(voucher.endDate),
                              new Date(),
                              { locale: vi }
                            )}
                          </div>
                        </div>
                        <div className={'flex flex-row items-end'}>
                          <div className="items-center flex">
                            <Button
                              className={'border-0 text-price'}
                              onClick={() => applyVoucher(voucher.code)}
                              disabled={
                                selectedVoucherUshare?.id === voucher.id
                              }
                            >
                              {selectedVoucherUshare?.id === voucher.id
                                ? 'Đã áp dụng'
                                : 'Áp dụng'}
                            </Button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className={'flex-none'}
                    style={{
                      width: 4,
                      height: 72,
                      background:
                        'url("/images/voucher-bg-left.svg") no-repeat',
                    }}
                  />
                </div>
              ))}
            </div>
          </>
        ) : (
          <div className={'mt-5 flex justify-center flex-col items-center'}>
            <div className={'text-secondary'}>
              <Voucher />
            </div>
            <div className={'text-primary'}>
              Chưa có mã giảm giá nào của Ushare
            </div>
            <div className={'text-secondary'}>
              Nhập mã giảm giá có thể sử dụng vào thanh bên trên
            </div>
          </div>
        )}
      </Modal>
      <ModalOrderSuccess
        visible={orderIds.length > 0}
        orderIds={orderIds}
        setOrderIds={setOrderIds}
      />
      <ModalOrderError
        visible={isShowErrorProduct}
        messageError={messageErrorProduct}
        onShowError={setIsShowErrorProduct}
      />
    </Card>
  )
}

export default OrderCheckout

import React, { FC, useEffect, useState } from 'react'
import { Modal, Upload } from 'antd'
import { UploadOutlined } from '@ant-design/icons'
import ReactPlayer from 'react-player'
import { UploadRequestOption as RcCustomRequestOptions } from 'rc-upload/lib/interface'
import { RcFile } from 'antd/lib/upload'
import { alertError, getCdnFile } from 'utils'
import { UploadFile } from 'antd/es/upload/interface'
import { useSetRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'
import mime from 'mime-types'
import { CdnService } from 'services'

interface IProps {
  files?: string[]
  onImageChange?: React.Dispatch<React.SetStateAction<string[]>>
  onUploaded?: (url: string) => void
  onRemoved?: () => void
  multiple?: boolean
  disableUpload?: boolean
  disabled?: boolean
}

const listAllowedUpload = ['image/png', 'image/jpg', 'image/jpeg', 'video/mp4', 'video/webm']
const listImages = ['image/png', 'image/jpg', 'image/jpeg']
const listVideos = ['video/mp4', 'video/webm']

const UploadMedia: FC<IProps> = (props) => {
  const {
    files,
    onImageChange,
    multiple = true,
    onUploaded,
    disableUpload,
    disabled,
  } = props
  const setIsLoading = useSetRecoilState(loadingState)
  const [fileList, setFileList] = useState<UploadFile[]>([])
  const [previewVisible, setPreviewVisible] = useState<boolean>(false)
  const [previewFile, setPreviewFile] = useState<UploadFile>()

  useEffect(() => {
    if (files?.length) {
      setFileList(() => {
        return files.map((file) => {
          const fileParts = file.split('/')
          const fileName = fileParts[fileParts.length - 1]
          return {
            uid: file,
            size: 0,
            name: file,
            url: getCdnFile(file),
            status: 'done',
            originFileObj: {} as RcFile,
            type: mime.lookup(fileName.split('?')[0]) as string,
          }
        })
      })
    } else {
      setFileList([])
    }
  }, [files])

  const handlePreview = (file: UploadFile) => {
    setPreviewVisible(!previewVisible)
    setPreviewFile(file)
  }

  const handleMediaUpload = (file) => {
    if (!listImages.includes(file.type)) {
      alertError(`Chỉ được upload file ảnh(.PNG, .JPG, .JPEG)`)
      // alertError(`Chỉ được upload file ảnh(.PNG, .JPG, .JPEG, .MP4, .WEBM)`)
      return Upload.LIST_IGNORE
    }
    return true;
  }
  const uploadImage = (options: RcCustomRequestOptions) => {
    setIsLoading(true)
    const { onSuccess, onError, file } = options

    CdnService.cdnControllerUploadFile({ files: file })
      .then((response) => {
        onSuccess('Ok')
        if (!multiple && onUploaded) {
          onUploaded(response.files[0])
        } else {
          onImageChange((prevState) => {
            if (prevState) return [...prevState, response.files[0]]
            else return [response.files[0]]
          })
        }
        setIsLoading(false)
      })
      .catch((error) => {
        alertError(error)
        onError(error)
        setIsLoading(false)
      })
  }

  return (
    <>
      <Upload
        disabled={disabled}
        customRequest={uploadImage}
        listType="picture-card"
        fileList={fileList}
        maxCount={1}
        onPreview={handlePreview}
        multiple={multiple}
        beforeUpload={handleMediaUpload}
        onRemove={(file) => {
          if (!onUploaded) {
            onImageChange((prevState) => {
              const tmpFiles = [...prevState]
              const existIndex = tmpFiles.findIndex(
                (tmpFile) => tmpFile === file.name
              )
              if (existIndex > -1) tmpFiles.splice(existIndex, 1)
              if (tmpFiles.length === 0) setFileList(tmpFiles)
              return tmpFiles
            })
          } else {
            onUploaded('')
          }
          return true
        }}
      >
        {!disableUpload && (multiple || (!multiple && fileList?.length === 0)) && (
          <div>
            <UploadOutlined />
            <div style={{ marginTop: 8 }}>Tải lên</div>
          </div>
        )}
      </Upload>
      <Modal
        visible={previewVisible}
        title={previewFile?.name}
        footer={null}
        onCancel={() => {
          setPreviewVisible(!previewVisible)
        }}
      >
        <ReactPlayer
          width={'100%'}
          url={getCdnFile(previewFile?.url)}
          playing={true}
          controls={true}
        />
      </Modal>
    </>
  )
}
export default UploadMedia

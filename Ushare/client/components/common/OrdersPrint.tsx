import React, { FC } from 'react'
import { BankInfo, EnumOrderHistoryEntityStatus, OrderEntity } from 'services'
import { format } from 'date-fns'
import Barcode from 'react-barcode'
import { Table } from 'antd'
import {
  capitalizeFirstLetter,
  formatNumber,
  getFullAddress,
  num2Word2,
} from 'utils'

interface Props {
  orders: OrderEntity[]
  bankInfo: BankInfo
}

const OrdersPrint: FC<Props> = React.forwardRef((props, ref) => {
  return (
    <div ref={ref}>
      {props?.orders?.map((order) => {
        const confirmedHistory = order?.histories?.find(
          (history) =>
            history?.status === EnumOrderHistoryEntityStatus.confirmed
        )
        return (
          <div
            key={`order-print-${order.id}`}
            className={'bg-white p-5'}
            style={{
              pageBreakAfter: 'always',
            }}
          >
            {/*<div className={'flex justify-between text-xs mb-3'}>*/}
            {/*  <div>{format(new Date(), 'hh:mm, dd/MM/yyy')}</div>*/}
            {/*  <div>{order?.merchant?.name} / Ushare / Thông tin giao hàng</div>*/}
            {/*</div>*/}
            <div className={'flex gap-4'}>
              <div className={'w-20 flex-none'}>
                <img src={'/logo.png'} width={70} />
              </div>
              <div>
                <div className={'text-center font-bold uppercase'}>
                  {order?.merchantAddress?.name}
                </div>
                <div className={'text-center'}>
                  {order?.merchantAddress?.address},{' '}
                  {getFullAddress(order?.merchantAddress?.ward)}
                </div>
                <div className={'text-center'}>
                  {order?.merchantAddress?.tel}
                </div>
              </div>
            </div>

            <div
              className={
                'flex-grow text-center uppercase font-semibold mt-2 text-xl'
              }
            >
              Phiếu giao hàng
            </div>
            <div className={'flex flex-col'}>
              {order?.shippingInfos?.map((shippingInfo, shippingInfoIndex) => {
                if (
                  shippingInfo?.partner !== 'other' &&
                  shippingInfo?.partner_id
                ) {
                  let code = shippingInfo?.partner_id
                  const codes = shippingInfo?.partner_id.split('.')
                  switch (shippingInfo?.partner) {
                    case 'ghtk':
                      code = codes[codes.length - 1]
                      break
                  }
                  return (
                    <div
                      key={`shipping-info-${shippingInfoIndex}`}
                      className={'flex justify-center'}
                    >
                      <Barcode
                        value={code}
                        height={30}
                        text={shippingInfo?.partner_id}
                      />
                    </div>
                  )
                }
              })}
            </div>
            <div className={'flex mt-4'}>
              <div className={'w-1/2'}>
                <div>
                  <strong>Khách hàng:</strong> {order?.fullname}
                </div>
                <div>
                  <strong>Số điện thoại:</strong> {order?.tel}
                </div>
                <div>
                  <strong>Địa chỉ:</strong> {order?.shippingAddress},{' '}
                  {/*{order?.ward?.name}, {order?.ward?.district?.name},{' '}*/}
                  {/*{order?.ward?.district?.province?.name}*/}
                  {getFullAddress(order?.ward)}
                </div>
              </div>
              <div className={'w-1/2'}>
                <div>
                  <strong>Ngày:</strong>{' '}
                  {format(
                    new Date(order?.createdAt),
                    "'Ngày' dd 'tháng' MM 'năm' yyyy"
                  )}
                </div>
                <div>
                  <strong>Số: {order?.code}</strong>
                </div>
                <div>
                  <strong>Thu ngân:</strong> {confirmedHistory?.user?.fullName}{' '}
                  - {confirmedHistory?.user?.tel}
                </div>
              </div>
            </div>
            <Table
              className={'mt-2 order-print'}
              size={'small'}
              bordered={true}
              pagination={false}
              dataSource={order?.orderProducts}
              rowClassName={'p-0 m-0 border-black'}
              columns={[
                {
                  dataIndex: 'id',
                  title: 'STT',
                  render: (value, record, index) => index + 1,
                  className: 'px-1 py-0 border-black',
                  align: 'center',
                },
                {
                  dataIndex: ['product', 'name'],
                  title: 'Tên hàng hoá',
                  align: 'center',
                  className: 'px-1 py-0 border-black',
                  render: (value) => <div className={'text-left'}>{value}</div>,
                },
                {
                  dataIndex: 'variantId',
                  title: 'Phân loại',
                  className: 'px-1 py-0 border-black',
                  render: (value, record) => (
                    <div className={'text-left'}>{record?.variant?.name}</div>
                  ),
                  align: 'center',
                },
                {
                  dataIndex: 'quantity',
                  title: 'SL',
                  align: 'center',
                  className: 'px-1 py-0 border-black',
                },
                {
                  dataIndex: 'price',
                  title: 'Đơn giá',
                  render: (value) => (
                    <div className={'text-right'}>{formatNumber(value)}</div>
                  ),
                  className: 'px-1 py-0 border-black',
                  align: 'center',
                },
                {
                  dataIndex: 'price',
                  title: 'Thành tiền',
                  render: (value, record) => (
                    <div className={'text-right'}>
                      {formatNumber(value * record.quantity)}
                    </div>
                  ),
                  className: 'px-1 py-0 border-black',
                  align: 'center',
                },
              ]}
              summary={() => {
                return (
                  <>
                    <Table.Summary.Row>
                      <Table.Summary.Cell
                        index={1}
                        colSpan={3}
                        rowSpan={6}
                        className={'px-1 py-0 border-black'}
                      >
                        {order?.note && `Ghi chú: ${order?.note}`}
                      </Table.Summary.Cell>
                      <Table.Summary.Cell
                        index={2}
                        colSpan={2}
                        className={'px-1 py-0 border-black'}
                      >
                        <strong>Tiền hàng</strong>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell
                        index={2}
                        align={'right'}
                        className={'px-1 py-0 border-black'}
                      >
                        <strong>
                          {formatNumber(order?.total + order?.discount)}
                        </strong>
                      </Table.Summary.Cell>
                    </Table.Summary.Row>
                    <Table.Summary.Row>
                      <Table.Summary.Cell
                        index={2}
                        colSpan={2}
                        className={'px-1 py-0 border-black'}
                      >
                        Khuyến mãi
                      </Table.Summary.Cell>
                      <Table.Summary.Cell
                        index={2}
                        align={'right'}
                        className={'px-1 py-0 border-black'}
                      >
                        {formatNumber(order?.discount)}
                      </Table.Summary.Cell>
                    </Table.Summary.Row>
                    <Table.Summary.Row>
                      <Table.Summary.Cell
                        index={2}
                        colSpan={2}
                        className={'px-1 py-0 border-black'}
                      >
                        Phí giao hàng
                      </Table.Summary.Cell>
                      <Table.Summary.Cell
                        index={2}
                        align={'right'}
                        className={'px-1 py-0 border-black'}
                      >
                        {/*{formatNumber(*/}
                        {/*  order?.shippingInfos?.reduce((total, curr) => {*/}
                        {/*    return total + Number(curr?.fee || 0)*/}
                        {/*  }, 0)*/}
                        {/*)}*/}0
                      </Table.Summary.Cell>
                    </Table.Summary.Row>
                    <Table.Summary.Row>
                      <Table.Summary.Cell
                        index={2}
                        colSpan={2}
                        className={'px-1 py-0 border-black'}
                      >
                        <strong>Tổng thanh toán</strong>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell
                        index={2}
                        align={'right'}
                        className={'px-1 py-0 border-black'}
                      >
                        <strong>{formatNumber(order?.total)}</strong>
                      </Table.Summary.Cell>
                    </Table.Summary.Row>
                    <Table.Summary.Row>
                      <Table.Summary.Cell
                        index={2}
                        colSpan={2}
                        className={'px-1 py-0 border-black'}
                      >
                        Đã cọc
                      </Table.Summary.Cell>
                      <Table.Summary.Cell
                        index={2}
                        align={'right'}
                        className={'px-1 py-0 border-black'}
                      >
                        {formatNumber(order?.deposit)}
                      </Table.Summary.Cell>
                    </Table.Summary.Row>
                    <Table.Summary.Row>
                      <Table.Summary.Cell
                        index={2}
                        colSpan={2}
                        className={'px-1 py-0 border-black'}
                      >
                        <strong>Còn phải thu</strong>
                      </Table.Summary.Cell>
                      <Table.Summary.Cell
                        index={2}
                        align={'right'}
                        className={'px-1 py-0 border-black'}
                      >
                        <strong>
                          {formatNumber(
                            Number(order?.total || 0) -
                              Number(order?.deposit || 0)
                          )}
                        </strong>
                      </Table.Summary.Cell>
                    </Table.Summary.Row>
                  </>
                )
              }}
            />
            <div className={'mt-2'}>
              Số tiền viết bằng chữ:{' '}
              {capitalizeFirstLetter(num2Word2.convert(order.total))}
            </div>
            <div className={'text-center mt-2'}>
              Cảm ơn quý khách đã tin tưởng lựa chọn sản phẩm của cửa hàng!
              <br />
              Thông tin chuyển khoản:{' '}
              {props?.bankInfo?.bankNumber &&
                `STK ${props?.bankInfo?.bankNumber}`}{' '}
              {props?.bankInfo?.bank?.shortName &&
                `- Ngân hàng ${props?.bankInfo?.bank?.shortName}`}{' '}
              {props?.bankInfo?.bankAccount &&
                ` - ${props?.bankInfo?.bankAccount}`}{' '}
              {props?.bankInfo?.bankBranch &&
                `- Chi nhánh ${props?.bankInfo?.bankBranch}`}
            </div>
            <div className={'text-center mt-4'}>
              Mời quý khách ghé thăm Website:
              <br />
              https://ushare.com.vn để cập nhật những sản phẩm mới nhất của công
              ty chúng tôi
            </div>
            <div className={'grid grid-cols-4 mt-4'}>
              <div className={'text-center'}>
                <strong>Người lập</strong>
                <br />
                <span className={'italic'}>(Ký, họ tên)</span>
              </div>
              <div className={'text-center'}>
                <strong>Khách hàng</strong>
                <br />
                <span className={'italic'}>(Ký, họ tên)</span>
              </div>
              <div className={'text-center'}>
                <strong>Thủ kho</strong>
                <br />
                <span className={'italic'}>(Ký, họ tên)</span>
              </div>
              <div className={'text-center'}>
                <strong>Vận chuyển</strong>
                <br />
                <span className={'italic'}>(Ký, họ tên)</span>
              </div>
            </div>
          </div>
        )
      })}
    </div>
  )
})
export default OrdersPrint

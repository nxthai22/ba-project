import {Menu} from 'antd'
import React from 'react'
import {MenuItemProps} from 'antd/lib/menu/MenuItem'
import Icon from '@ant-design/icons'
import {IconType} from 'react-icons/lib'
import {NotificationDto} from '../../../services'
import {format} from 'date-fns'
import viLocale from 'date-fns/locale/vi'

interface ExtendedMenuItemProps extends MenuItemProps {
  notification?: NotificationDto
  icon?: IconType
  isBlank?: boolean
}

const MenuItem: React.FC<ExtendedMenuItemProps> = (props) => {
  return (
    <Menu.Item
      {...props}
      style={{ width: 360, paddingRight: 1 }}
      className={
        `border-solid border-b border-b-[#e5e5e5] last:border-b-transparent 
        ${props?.notification?.read === false ? 'bg-[#FEF0F5] hover:bg-white' : ''}`
      }
    >
      {props.children}
      {!props.isBlank ? (
        <div className='py-2 flex items-center'>
          <Icon
            component={props.icon}
            className='bg-white rounded-full border text-[22px] text-[#F4447F] p-1'
          />
          <div
            className='ml-2 whitespace-normal'
            style={{wordBreak: 'break-word'}}
          >
            <span className='font-medium'>
              {props.notification?.title}
            </span>
            <div className='text-xs mt-1'>
              {props.notification?.body}
              <div className='text-[#80B4FB]'>
                {props.notification?.createdAt ? format(new Date(props.notification.createdAt), 'HH:mm dd-MM-yyyy', {locale: viLocale}) : ''}
              </div>
            </div>
          </div>
        </div>
        ) : 'Không có thông báo'
      }
    </Menu.Item>
  )
}

export default MenuItem

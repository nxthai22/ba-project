import React, { FC, useEffect, useState } from 'react'
import {
  Button,
  Col,
  Form,
  Input,
  InputNumber,
  Row,
  Select,
  Space,
  Table,
} from 'antd'
import { Controller, SubmitHandler, useForm, useWatch } from 'react-hook-form'
import {
  EnumShipmentRequestDtoPartner,
  OrderEntity,
  OrderProductEntity,
  OrdersService,
  ShipmentRequestDto,
} from 'services'
import { useSetRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'
import {
  alertError,
  alertSuccess,
  formatCurrency,
  getCdnFile,
  getFullAddress,
} from 'utils'
import { ColumnsType } from 'antd/es/table'
import { SHIPPING_PARTNER } from '@/constants'
import { TableRowSelection } from 'antd/es/table/interface'
import * as yup from 'yup'
import { SchemaOf } from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'

interface IProps {
  orderIds: number[]
  onSuccess: () => void
  onCancel?: () => void
}

interface Inputs {
  shippingPartner?: EnumShipmentRequestDtoPartner
  createShipment?: ShipmentRequestDto[]
}

interface ShippingPartner {
  partner: string
  orderIndex: number
  orderProducts: OrderProductEntity[]
  fee?: number
}

interface NewOrderEntity extends OrderEntity {
  shippingPartners: ShippingPartner[]
}

const phoneRegExp = /([84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/
const schema: SchemaOf<Inputs> = yup.object().shape({
  createShipment: yup.array().of(
    yup.object().shape({
      orderId: yup.number(),
      height: yup.number(),
      length: yup.number(),
      width: yup.number(),
      note: yup.string(),
      merchantAddressId: yup.number(),
      partner: yup
        .mixed<keyof typeof EnumShipmentRequestDtoPartner>()
        .oneOf(Object.values(EnumShipmentRequestDtoPartner))
        .required('Chưa chọn đối tác vận chuyển'),
      otherInfo: yup.object().shape({
        tel: yup.string().matches(phoneRegExp, 'Số điện thoại không hợp lệ'),
        fullName: yup.string(),
        partnerName: yup.string(),
      }),
    })
  ),
})

const BatchDelivery: FC<IProps> = (props) => {
  const { orderIds, onSuccess, onCancel } = props
  const setIsLoading = useSetRecoilState(loadingState)
  const {
    control,
    setValue,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
  })
  const [orders, setOrders] = useState<NewOrderEntity[]>([])
  const [selectedRowKeys, setSelectedRowKeys] = useState<number[]>([])
  const watchShippingPartner: EnumShipmentRequestDtoPartner = useWatch({
    control,
    name: 'shippingPartner',
  })

  const watchCreateShipment = useWatch({
    control,
    name: 'createShipment',
  })
  const [isDisableSelectPartner, setIsDisableSelectPartner] = useState(true)

  const columns: ColumnsType<NewOrderEntity> = [
    {
      dataIndex: 'code',
      title: 'Mã ĐH',
      render: (value, record, index) => {
        return (
          <>
            {value}
            <Controller
              control={control}
              name={`createShipment.${index}.orderId`}
              render={({ field }) => (
                <Input
                  {...field}
                  defaultValue={value}
                  style={{
                    display: 'none',
                  }}
                />
              )}
            />
          </>
        )
      },
    },
    {
      dataIndex: 'shippingAddress',
      title: 'Địa chỉ nhận',
      width: 250,
      render: (value, record) => value + ', ' + getFullAddress(record.ward),
    },
    {
      dataIndex: 'orderProducts',
      title: 'Trọng lượng (g)',
      align: 'center',
      render: (value, record) => {
        return (
          record?.orderProducts?.reduce((weight, orderProduct) => {
            return (weight += orderProduct.product.weight)
          }, 0) || 0
        )
      },
    },
    {
      dataIndex: 'shippingInfo',
      title: 'Đối tác vận chuyển',
      width: 150,
      align: 'center',
      render: (value, record, index) => {
        return (
          <>
            <Controller
              control={control}
              name={`createShipment.${index}.partner`}
              render={({ field }) => (
                <Select
                  {...field}
                  allowClear
                  style={{ width: '100%' }}
                  placeholder={'Chọn đối tác vận chuyển'}
                >
                  {Object.keys(SHIPPING_PARTNER).map((partner) => (
                    <Select.Option
                      value={partner}
                      key={`tbl-sl-shipping-${index}-partner-${partner}`}
                    >
                      {SHIPPING_PARTNER[partner]}
                    </Select.Option>
                  ))}
                </Select>
              )}
            />
            {errors?.createShipment?.[index]?.partner && (
              <div role={'alert'} className={'ant-form-item-explain-error'}>
                {errors?.createShipment?.[index]?.partner?.message}
              </div>
            )}
          </>
        )
      },
    },
    {
      dataIndex: 'shippingInfo',
      title: 'Phí ship',
      align: 'center',
      // render: (value) => {
      //   return value?.fee ? formatCurrency(value?.fee) : null
      // },
      render: (value, record, index) => {
        return (
          <>
            {watchCreateShipment?.[index]?.partner === 'other' ? (
              <Controller
                control={control}
                name={`createShipment.${index}.otherFee`}
                render={({ field }) =>
                  <InputNumber
                    {...field}
                    formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  />
                }
              />
            ) : (
              value?.fee ? formatCurrency(value?.fee) : null
            )}
          </>
        )
      },
    },
    {
      dataIndex: '',
      title: 'Ghi chú',
      render: (value, record, index) => {
        return (
          <>
            {watchCreateShipment?.[index]?.partner === 'other' ? (
              <>
                <div className={'text-left mb-4'}>
                  <div>Tên đối tác:</div>
                  <Controller
                    control={control}
                    render={({ field }) => <Input {...field} />}
                    name={`createShipment.${index}.otherInfo.partnerName`}
                  />
                  <div>Tên người vận chuyển:</div>
                  <Controller
                    control={control}
                    render={({ field }) => <Input {...field} />}
                    name={`createShipment.${index}.otherInfo.fullName`}
                  />
                  <div>Số điện thoại người vận chuyển:</div>
                  <Controller
                    control={control}
                    render={({ field }) => <Input {...field} />}
                    name={`createShipment.${index}.otherInfo.tel`}
                  />
                  {errors?.createShipment?.[index]?.otherInfo?.tel && (
                    <div
                      role={'alert'}
                      className={'ant-form-item-explain-error'}
                    >
                      {errors?.createShipment?.[index]?.otherInfo?.tel?.message}
                    </div>
                  )}
                </div>
              </>
            ) : null}
            <Controller
              control={control}
              name={`createShipment.${index}.note`}
              render={({ field }) => (
                <Input.TextArea
                  placeholder={'Nhập ghi chú (Tối đa 120 ký tự)'}
                  rows={2}
                  {...field}
                />
              )}
            />
          </>
        )
      },
    },
  ]

  useEffect(() => {
    setIsLoading(true)
    OrdersService.getManyBase({
      filter: `id||in||${orderIds.join(',')}`,
      join: [
        'ward',
        'ward.district',
        'ward.district.province',
        'merchant',
        'merchant.addresses',
        'orderProducts',
        'orderProducts.product',
      ],
    })
      .then((response) => {
        setIsLoading(false)
        setOrders(
          response.data.map((order, orderIndex) => ({
            ...order,
            shippingPartners: [
              {
                partner: undefined,
                orderIndex,
                orderProducts: order.orderProducts,
              },
            ],
          }))
        )
        setValue(
          'createShipment',
          response.data.map((orderItem) => {
            return {
              orderId: orderItem.id,
              height: 0,
              length: 0,
              width: 0,
              note: '',
              partner: null,
              merchantAddressId: orderItem.merchantAddressId,
              // orderProductIds: [],
            }
          })
        )
      })
      .catch((error) => {
        setIsLoading(false)
        alertError(error)
      })
  }, [])

  useEffect(() => {
    if (watchCreateShipment) {
      const bodyData: ShipmentRequestDto[] = []
      watchCreateShipment.map((item) => {
        if (item.partner && item.partner !== 'other') bodyData.push(item)
      })
      if (bodyData?.length > 0) {
        setIsLoading(true)
        OrdersService.orderControllerCalculateFee({
          body: bodyData as unknown as ShipmentRequestDto[],
        })
          .then((response) => {
            setIsLoading(false)
            setOrders((prevState) => {
              const tmpOrders = [...prevState]
              tmpOrders.map((order, index) => {
                const shippingInfoData = response.find(
                  (shippingFeeResponse) =>
                    shippingFeeResponse.orderId === order.id
                )
                order.shippingInfo = shippingInfoData?.shippingInfo
                if (shippingInfoData) {
                  if (!shippingInfoData?.partner) {
                    tmpOrders[index].shippingPartners.map(
                      (shippingPartner, shippingPartnerIndex) => {
                        if (
                          shippingInfoData.shippingInfo.partner ===
                          shippingPartner.partner
                        ) {
                          tmpOrders[index].shippingPartners[
                            shippingPartnerIndex
                          ].fee = shippingInfoData.shippingInfo?.fee || 0
                        }
                      }
                    )
                  }
                }
              })
              return tmpOrders
            })
          })
          .catch((error) => {
            setIsLoading(false)
            alertError(error)
          })
      }
    }
  }, [watchCreateShipment])

  const caculateFeeShip = (
    orderIds,
    partner: EnumShipmentRequestDtoPartner
  ) => {
    const tmpCreateShipment: ShipmentRequestDto[] = getValues('createShipment')
    orderIds.map((orderId) => {
      const createShipmentIndex = tmpCreateShipment.findIndex(
        (createShipmentDto) => createShipmentDto.orderId === orderId
      )
      setValue(`createShipment.${createShipmentIndex}.partner`, partner)
    })
    setValue('shippingPartner', null)
  }

  useEffect(() => {
    if (watchShippingPartner) {
      let tmpOrderIds = [...orderIds]
      if (selectedRowKeys.length > 0) {
        tmpOrderIds = [...selectedRowKeys]
      }
      caculateFeeShip(
        tmpOrderIds,
        watchShippingPartner as unknown as EnumShipmentRequestDtoPartner
      )
    }
  }, [watchShippingPartner])

  const rowSelection: TableRowSelection<OrderEntity> = {
    onChange: (selectedRowKeys: React.Key[]) => {
      setSelectedRowKeys(selectedRowKeys as number[])
      setIsDisableSelectPartner(selectedRowKeys.length < 0)
    },
    getCheckboxProps: (record: OrderEntity) => ({
      id: String(record.id),
    }),
  }

  const expandedProductRowRender = (
    shippingPartner: ShippingPartner,
    parentIndex: number
  ) => {
    const columns: ColumnsType<OrderProductEntity> = [
      {
        dataIndex: 'product',
        title: 'Sản phẩm',
        width: 365,
        render: (value, record, index) => {
          return (
            <Row className={'items-center'}>
              <Col xs={4} sm={4} md={4} xl={4} lg={4}>
                {value?.images && value?.images.length > 0 && (
                  <img
                    src={getCdnFile(value?.images?.[0])}
                    width={'60px'}
                    alt={value?.name}
                  />
                )}
              </Col>
              <Col xs={20} sm={20} md={20} xl={20} lg={20}>
                {value?.name}
              </Col>
              <div className={'hidden'}>
                <Controller
                  control={control}
                  defaultValue={record.id}
                  name={`createShipment.${shippingPartner.orderIndex}.shippingPartners.${parentIndex}.${index}.orderProductId`}
                  render={({ field }) => (
                    <Input {...field} className={'w-full'} />
                  )}
                />
              </div>
            </Row>
          )
        },
      },
      {
        dataIndex: 'weight',
        title: 'Trọng lượng (g)',
        align: 'center',
        width: 150,
        render: (value) => (value ? value : null),
      },
      {
        dataIndex: 'shippingInfo',
        title: 'Đối tác vận chuyển',
        width: 150,
        align: 'center',
        render: (value, record, index) => {
          return (
            <Controller
              control={control}
              name={`createShipment.${shippingPartner.orderIndex}.shippingPartners.${parentIndex}.${index}.partner`}
              render={({ field }) => (
                <Select
                  {...field}
                  allowClear
                  style={{ width: '100%' }}
                  placeholder={'Chọn đối tác vận chuyển'}
                  onChange={(value) => {
                    setOrders((prevState) => {
                      const tmpOrders = [...prevState]

                      const existPartnerIndex = tmpOrders[
                        shippingPartner.orderIndex
                      ].shippingPartners.findIndex((shippingPartner) => {
                        if (value) return shippingPartner.partner === value
                        else return shippingPartner.partner === undefined
                      })
                      tmpOrders[shippingPartner.orderIndex].shippingPartners[
                        parentIndex
                      ].orderProducts.splice(index, 1)
                      if (existPartnerIndex >= 0) {
                        tmpOrders[shippingPartner.orderIndex].shippingPartners[
                          existPartnerIndex
                        ].orderProducts.push(record)
                      } else {
                        tmpOrders[
                          shippingPartner.orderIndex
                        ].shippingPartners.push({
                          partner: value,
                          orderIndex: shippingPartner.orderIndex,
                          orderProducts: [record],
                        })
                      }
                      tmpOrders.map((order, orderIndex) => {
                        setValue(
                          `createShipment.${orderIndex}.shippingPartners`,
                          []
                        )
                        order.shippingPartners.map(
                          (shippingPartner, shippingPartnerIndex) => {
                            shippingPartner.orderProducts.map(
                              (orderProduct, orderProductIndex) => {
                                setValue(
                                  `createShipment.${orderIndex}.shippingPartners.${shippingPartnerIndex}.${orderProductIndex}.partner`,
                                  shippingPartner.partner
                                )
                              }
                            )
                          }
                        )
                      })
                      return tmpOrders
                    })
                  }}
                >
                  {Object.keys(SHIPPING_PARTNER).map((partner) => (
                    <Select.Option
                      value={partner}
                      key={`tbl-sl-shipping-${parentIndex}-partner-${partner}`}
                    >
                      {SHIPPING_PARTNER[partner]}
                    </Select.Option>
                  ))}
                </Select>
              )}
            />
          )
        },
      },
    ]

    return (
      <Table
        rowKey={'id'}
        showHeader={false}
        columns={columns}
        dataSource={[...shippingPartner.orderProducts]}
        pagination={false}
      />
    )
  }

  const expandedRowRender = (
    recordOrderEntity: NewOrderEntity,
    parentIndex: number
  ) => {
    const columns: ColumnsType<ShippingPartner> = [
      {
        dataIndex: 'partner',
        title: 'Đối tác vận chuyển',
        width: 351,
        render: (value) => {
          if (value !== undefined) return SHIPPING_PARTNER[value]
          return '-'
        },
      },
      {
        dataIndex: 'weight',
        title: 'Trọng lượng (g)',
        align: 'center',
        width: 150,
      },
      {
        dataIndex: 'shippingInfo',
        title: 'Phí ship',
        align: 'center',
        render: (value, record) =>
          record?.partner !== 'other' ? record.fee : null,
        // render: (value, record, index) => {
        //   return (
        //     <>
        //       {record.partner === 'other' ? (
        //         <Controller
        //           control={control}
        //           name={`createShipment.${parentIndex}.${index}.otherFee`}
        //           render={({ field }) => <InputNumber {...field} />}
        //         />
        //       ) : (
        //         record.fee
        //       )}
        //     </>
        //   )
        // },
      },
      {
        dataIndex: '',
        title: 'Ghi chú',
        render: (value, record, index) => {
          return (
            <>
              {record.partner === 'other' ? (
                <>
                  <div className={'text-left mb-4'}>
                    <div>Tên đối tác:</div>
                    <Controller
                      control={control}
                      render={({ field }) => <Input {...field} />}
                      name={`createShipment.${parentIndex}.${index}.otherInfo.partnerName`}
                    />
                    <div>Tên người vận chuyển:</div>
                    <Controller
                      control={control}
                      render={({ field }) => <Input {...field} />}
                      name={`createShipment.${parentIndex}.${index}.otherInfo.fullName`}
                    />
                    <div>Số điện thoại người vận chuyển:</div>
                    <Controller
                      control={control}
                      render={({ field }) => <Input {...field} />}
                      name={`createShipment.${parentIndex}.${index}.otherInfo.tel`}
                    />
                  </div>
                </>
              ) : null}
              <Controller
                control={control}
                name={`createShipment.${parentIndex}.${index}.note`}
                render={({ field }) => (
                  <Input.TextArea
                    placeholder={'Nhập ghi chú (Tối đa 120 ký tự)'}
                    rows={2}
                    {...field}
                  />
                )}
              />
            </>
          )
        },
      },
    ]
    return (
      <Table
        showHeader={false}
        columns={columns}
        dataSource={[...recordOrderEntity.shippingPartners]}
        expandable={{
          expandedRowRender: expandedProductRowRender,
          rowExpandable: (record) => record.orderProducts.length > 0,
        }}
        pagination={false}
      />
    )
  }

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    let canSubmit = 1
    const body = data.createShipment.map((tmpCreateShipment, index) => {
      if (!tmpCreateShipment.partner) {
        canSubmit = 100
      }
      if (
        tmpCreateShipment[index]?.height == 0 ||
        tmpCreateShipment[index]?.width == 0 ||
        tmpCreateShipment[index]?.length == 0
      ) {
        canSubmit = 200
      }
      if (tmpCreateShipment[index]?.note?.length > 120) {
        canSubmit = 300
      }
      return tmpCreateShipment
    })
    if (canSubmit == 1) {
      setIsLoading(true)
      OrdersService.orderControllerCreateShipment({
        body,
      })
        .then(() => {
          setIsLoading(false)
          alertSuccess('Tạo lệnh Vận chuyển sang Đối tác vận chuyển thành công')
          onSuccess()
        })
        .catch((error) => {
          setIsLoading(false)
          alertError(error)
        })
    } else if (canSubmit == 100)
      alertError('Có đơn hàng chưa chọn Đối tác vận chuyển!')
    else if (canSubmit == 200)
      alertError('Có đơn hàng nhập đầy đủ thông tin kích thước đơn hàng!')
    // else if (canSubmit == 200)
    //   alertError('Ghi chú chỉ được nhập tối đa 120 ký tự!')
  }
  return (
    <Form onFinish={handleSubmit(onSubmit)}>
      <Row justify={'end'} gutter={[16, 16]}>
        <Col>
          <Form.Item label={'Đối tác vận chuyển'}>
            <Controller
              control={control}
              name={'shippingPartner'}
              render={({ field }) => (
                <Select
                  {...field}
                  placeholder={'Chọn 1 đối tác'}
                  disabled={isDisableSelectPartner}
                  allowClear
                >
                  {Object.keys(SHIPPING_PARTNER).map((partner) => (
                    <Select.Option
                      value={partner}
                      key={`sl-shipping-partner-${partner}`}
                    >
                      {SHIPPING_PARTNER[partner]}
                    </Select.Option>
                  ))}
                </Select>
              )}
            />
          </Form.Item>
        </Col>
        <Col xs={24}>
          <Table
            dataSource={[...orders]}
            rowKey={'id'}
            columns={columns}
            rowSelection={rowSelection}
            pagination={false}
            scroll={{
              x: 1500,
            }}
            expandable={{ expandedRowRender }}
          />
        </Col>
        <Col>
          <Space>
            <Button onClick={onCancel}>Huỷ</Button>
            <Button type={'primary'} htmlType={'submit'}>
              Xác nhận
            </Button>
          </Space>
        </Col>
      </Row>
    </Form>
  )
}
export default BatchDelivery

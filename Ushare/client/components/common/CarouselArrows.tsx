import React from 'react'

export const NextArrow = (props) => {
  const { className, style, onClick } = props
  return (
    <div className={className} style={{ ...style }} onClick={onClick}>
      <svg
        width="36"
        height="36"
        viewBox="0 0 36 36"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g clipPath="url(#clip0_6278_27404)">
          <path
            d="M27.3097 17.0121L10.6586 0.4071C10.113 -0.136624 9.22978 -0.13571 8.68514 0.409913C8.14092 0.955465 8.14233 1.83922 8.68795 2.38337L24.3479 18.0001L8.68739 33.6167C8.14184 34.1609 8.14043 35.0441 8.68458 35.5897C8.9576 35.8632 9.31528 36 9.67296 36C10.0297 36 10.386 35.8641 10.6585 35.5925L27.3097 18.9879C27.5724 18.7265 27.7199 18.3707 27.7199 18.0001C27.7199 17.6294 27.572 17.274 27.3097 17.0121Z"
            fill="currentColor"
          />
        </g>
        <defs>
          <clipPath id="clip0_6278_27404">
            <rect width="36" height="36" fill="white" />
          </clipPath>
        </defs>
      </svg>
    </div>
  )
}

export const PrevArrow = (props) => {
  const { className, style, onClick } = props
  return (
    <div className={className} style={{ ...style }} onClick={onClick}>
      <svg
        width="36"
        height="36"
        viewBox="0 0 36 36"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g clipPath="url(#clip0_6278_27410)">
          <path
            d="M8.69034 17.0121L25.3414 0.4071C25.887 -0.136624 26.7702 -0.13571 27.3149 0.409913C27.8591 0.955465 27.8577 1.83922 27.312 2.38337L11.6521 18.0001L27.3126 33.6167C27.8582 34.1609 27.8596 35.0441 27.3154 35.5897C27.0424 35.8632 26.6847 36 26.327 36C25.9703 36 25.614 35.8641 25.3415 35.5925L8.69034 18.9879C8.42758 18.7265 8.28014 18.3707 8.28014 18.0001C8.28014 17.6294 8.42801 17.274 8.69034 17.0121Z"
            fill="currentColor"
          />
        </g>
        <defs>
          <clipPath id="clip0_6278_27410">
            <rect
              width="36"
              height="36"
              fill="white"
              transform="matrix(-1 0 0 1 36 0)"
            />
          </clipPath>
        </defs>
      </svg>
    </div>
  )
}

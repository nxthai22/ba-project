import React, {FC, useEffect} from 'react'
import {app} from '../../utils/firebase'
import firebase from "firebase/app";
import {notification} from 'antd';
import {NotificationsService} from "../../services";
import {v4 as uuidv4} from 'uuid';
import localforage from 'localforage';

const FCM_TOKEN = 'fcm_token';

interface IProps {
  updateUnreadBell: Function;
  onNotificationClick?: Function;
}


const FirebaseCloudMessage: FC<IProps> = (props) => {

  useEffect(() => {
    // Initialize Firebase Cloud Messaging and get a reference to the service
    if(!firebase.messaging.isSupported()){
      return;
    }
    const messaging = firebase.messaging(app);
    //checking whether token is available in indexed DB
    localforage.getItem(FCM_TOKEN).then((tokenInLocalForage) => {
      // Get registration token. Initially this makes a network call, once retrieved
      // subsequent calls to getToken will return from cache.
      messaging.getToken({vapidKey: process.env.NEXT_PUBLIC_FCM_VAPID_KEY})
        .then((currentToken) => {
          if (currentToken && currentToken !== tokenInLocalForage) {
            // Send the token to your server and update the UI if necessary
            //setting FCM token in indexed db using localforage
            localforage.setItem(FCM_TOKEN, currentToken);
            // Track the token -> client mapping, by sending to backend server
            NotificationsService.notificationControllerRegisterFcmToken({
              body: {
                fcmToken: currentToken
              },
            })
              .then(() => {
                console.log('Update notification token success')
              })
              .catch((error) => {
              })
          } else {
            console.log('No need to register fcm token');
          }
        }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
      });
    });

    messaging.onMessage((payload) => {
      console.log(payload);
      const key = uuidv4();
      if (payload?.notification) {
        notification.open({
          key: key,
          message: payload?.notification.title,
          description: payload?.notification.body,
          duration: 5,
          onClick: () => {
            console.log('Notification Clicked!');
            try {
              const data = payload?.data?.data;
              const notiId = JSON.parse(data).notiId;
              props.onNotificationClick(notiId)
            } catch (e) {
            }
            // close this notification
            notification.close(key)
          },
        });
      }
      // do reload Notification Bell
      props.updateUnreadBell();
    });
  }, [])
  return (<></>);
}
export default FirebaseCloudMessage

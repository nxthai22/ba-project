import { Input } from 'antd'
import InputNumber from 'components/common/InputNumber'
import React, { FC, useEffect } from 'react'
import { MerchantAddressEntity } from 'services'
import { Controller, useFormContext, useWatch } from 'react-hook-form'

interface IProps {
  inputPrefixName: string
  merchantAddresses: MerchantAddressEntity[]
}

const MerchantAddress: FC<IProps> = ({
  inputPrefixName,
  merchantAddresses,
}) => {
  const { control, setValue } = useFormContext()
  return (
    <>
      {merchantAddresses.map((address, index) => {
        // setValue(`${inputPrefixName}.${index}.merchantAddressId`, address.id)
        return (
          <div
            key={`productGroupTypeItemValue-merchantAddress-${inputPrefixName}-${index}-${address.id}`}
            className={
              'border-b border-r flex items-center justify-center flex-1'
            }
          >
            <div className={'w-full border-r h-full items-center'}>
              <div className={'flex px-5 h-full items-center truncate w-40'}>
                {address.name}
              </div>
              <Controller
                control={control}
                name={`${inputPrefixName}.${index}.merchantAddressId`}
                render={({ field }) => <Input {...field} type={'hidden'} />}
              />
            </div>
            <div className={'w-full'}>
              <Controller
                control={control}
                name={`${inputPrefixName}.${index}.quantity`}
                render={({ field }) => (
                  <InputNumber
                    {...field}
                    defaultValue={0}
                    placeholder="Số lượng"
                    className={'border-none w-full'}
                  />
                )}
              />
            </div>
          </div>
        )
      })}
    </>
  )
}
export default MerchantAddress

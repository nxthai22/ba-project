import { Form, FormItemProps } from 'antd'
import React from 'react'

const FormItem: React.FC<FormItemProps> = (props) => {
  return (
    <Form.Item
      labelCol={{
        sm: { span: 12 },
        md: { span: 8 },
      }}
      {...props}
    >
      {props.children}
    </Form.Item>
  )
}

export default FormItem

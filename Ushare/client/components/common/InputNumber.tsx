import { Input, InputNumber as AntdInputNumber } from 'antd'
import { InputNumberProps as AntdInputNumberProps } from 'antd/lib/input-number'
import { FC } from 'react'

interface InputNumberProps extends AntdInputNumberProps {
  addonAfter?: any
  addonBefore?: any
}

const InputNumber: FC<AntdInputNumberProps> = (props) => {
  return (
    <AntdInputNumber
      {...props}
      formatter={(value) =>
        value
          ? `${value}`.replace('.', ',').replace(/\B(?=(\d{3})+(?!\d))/g, '.')
          : '0'
      }
      parser={(value) => value.replace(/\$\s?|(\.*)/g, '').replace(',', '.')}
    />
  )
}

export default InputNumber

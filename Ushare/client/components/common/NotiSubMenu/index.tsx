import {Badge, Menu} from 'antd'
import React from 'react'
import {SubMenuProps} from 'antd/lib/menu/SubMenu'
import Icon from '@ant-design/icons'
import {IconType} from 'react-icons/lib'

interface ExtendedSubMenuProps extends SubMenuProps {
  body: string
  type: string
  icon: IconType
  unreadCount: number
}

const NotiSubMenu: React.FC<ExtendedSubMenuProps> = (props) => {
  return (
    <Menu.SubMenu
      {...props}
      icon={''}
      className='border-solid border-b border-b-[#e5e5e5] last:border-b-transparent max-h-screen overflow-auto'
      title={
        <div className='py-2 flex items-center'>
          <Icon
            component={props.icon}
            className='bg-white rounded-full border text-[22px] text-[#F4447F] p-1'
          />
          <div
            className='ml-2 whitespace-normal'
            style={{wordBreak: 'break-word'}}
          >
            <span className='font-medium'>
              {props.title}{' '}
            </span>
            <Badge
              count={props.unreadCount}
              overflowCount={99}
              className='relative -top-[2px]'
            />
            <div className='text-xs mt-1'>
              {props.body}
            </div>
          </div>
        </div>
      }
    >
      {props.children}
    </Menu.SubMenu>
  )
}

export default NotiSubMenu

import React from 'react'
import { Col, Row, Select } from 'antd'
import { filterOption } from 'utils'
import { MerchantEntity } from 'services'
import { useMerchant } from 'hooks/merchants'
import { useMount } from 'ahooks'
import { SearchIcon } from 'constants/icons'

interface IProps {
  onChangeMerchantId: (id: null | number) => void
}

const SelectMerchant = (props: IProps) => {
  const {merchants, getMerchants} = useMerchant()
  
  useMount(() => {
    getMerchants()
  })

  return (
    <Row>
      <Col sm={{span: 12, offset: 6}}>
        <div className='relative admin-dashboard-select-merchant'>
          <Select
            placeholder='Chọn nhà cung cấp'
            className='w-full text-base'
            size='large'
            defaultValue={null}
            filterOption={filterOption}
            onChange={(id: null | number) => props.onChangeMerchantId(id)}
          >
            <Select.Option value={null}>
              Tất cả nhà cung cấp
            </Select.Option>
            {merchants && merchants.map((item: MerchantEntity) => (
              <Select.Option
                key={item.id}
                value={item.id}
              >
                {item.name}
              </Select.Option>
            ))}
          </Select>
          <span className='absolute admin-dashboard-select-merchant-icon'>
            <SearchIcon />
          </span>
        </div>
      </Col>
    </Row>
  )
}

export default SelectMerchant

import {
  BankOutlined,
  BarChartOutlined,
  ContainerOutlined,
  DashboardFilled,
  DiffOutlined,
  DollarCircleOutlined,
  HistoryOutlined,
  HomeOutlined,
  NotificationOutlined,
  SettingOutlined,
  ShoppingCartOutlined,
  TagOutlined,
  TagsOutlined,
  TeamOutlined,
  UserOutlined,
} from '@ant-design/icons'
import Sider from 'antd/lib/layout/Sider'
import Menu from 'antd/lib/menu'
import { useRouter } from 'next/router'
import React, { FC, useEffect, useState } from 'react'
import { useRecoilState, useRecoilValue } from 'recoil'
import { authState, sidebarCollapsedState } from 'recoil/Atoms'
import { parseCookies } from 'nookies'
import { _findInTree, hasPermission } from 'utils'
import Link from 'components/common/Link'
import { MdHouseSiding, MdMapsHomeWork } from 'react-icons/md'
import { EnumRoleEntityType } from '../../../../services'

export interface SidebarItem {
  title: string
  icon?: JSX.Element
  visible?: boolean
  path?: string
  key?: string
  children?: SidebarItem[]
  permissions?: string[]
}

interface ISideBar {
  drawerVisible?: boolean
}

const Sidebar: FC<ISideBar> = () => {
  const router = useRouter()
  const user = useRecoilValue(authState)
  const { isSidebarCollapsed } = parseCookies(null)
  const [openKeys, setOpenKeys] = useState<string[] | undefined>([])
  const [routes, setRoutes] = useState<SidebarItem[]>([])
  const [sidebarCollapsed, setSidebarCollapsed] = useRecoilState(
    sidebarCollapsedState
  )
  useEffect(() => {
    setRoutes([
      {
        title: 'Trang bán hàng',
        icon: <HomeOutlined />,
        visible: true,
        key: 'home',
        path: process.env.NEXT_PUBLIC_CUSTOMER_WEB_URL,
      },
      {
        title: 'Tổng quan',
        icon: <DashboardFilled />,
        path: '/dashboard',
        visible: hasPermission(user, [
          'revenue_RevenueController_calculateRevenue',
          'revenue_RevenueController_calculateCommission',
          'revenue_RevenueController_getTopProductSale',
        ]),
      },
      {
        title: 'Thống kê',
        icon: <BarChartOutlined />,
        visible: hasPermission(user, [
          'revenue_RevenueController_calculateReferralCommissionReport',
          'revenue_RevenueController_calculateRevenueStatisticDetail',
        ]),
        key: 'statistics_key',
        children: [
          {
            title: 'Doanh thu CTV',
            path: '/statistics/revenue-detail',
            visible: hasPermission(user, [
              'revenue_RevenueController_calculateRevenueStatisticDetail',
            ]),
          },
          {
            title: 'Hoa hồng TV',
            path: '/statistics',
            visible: hasPermission(user, [
              'revenue_RevenueController_calculateReferralCommissionReport',
            ]),
          },
        ],
      },
      {
        title: 'Sản phẩm',
        icon: <ShoppingCartOutlined />,
        key: 'products',
        visible: hasPermission(user, [
          'products_createOneBase',
          'products_deleteOneBase',
          'products_updateOneBase',
        ]),
        children: [
          {
            title: 'Danh sách',
            path: '/product',
            visible: hasPermission(user, [
              'products_createOneBase',
              'products_deleteOneBase',
              'products_updateOneBase',
            ]),
          },
          {
            title: 'Danh mục',
            path: '/product-category',
            visible: hasPermission(user, ['product-categories_deleteOneBase']),
          },
        ],
      },
      {
        title: 'Lịch sử giao dịch',
        icon: <HistoryOutlined />,
        visible: hasPermission(user, [
          'user-transaction_createOneBase',
          'user-transaction_deleteOneBase',
          'user-transaction_getManyBase',
          'user-transaction_getOneBase',
          'user-transaction_createOneBase',
        ]),
        key: 'hitory',
        children: [
          {
            title: 'Nạp tiền',
            path: '/transaction/deposit',
            visible: hasPermission(user),
          },
          {
            title: 'Rút tiền',
            path: '/transaction/withdraw',
            visible: hasPermission(user),
          },
        ],
      },
      {
        title: 'Đơn hàng',
        icon: <ContainerOutlined />,
        visible: hasPermission(user, [
          'OrderController_FilterOrder',
          'OrderController_calculateFee',
          'OrderController_changeStatus',
          'OrderController_createShipment',
          'OrderController_getStatusCount',
          'orders_deleteOneBase',
          'orders_getManyBase',
          'orders_updateOneBase',
        ]),
        path: '/order',
      },
      {
        title: 'Nhà cung cấp',
        icon: <MdHouseSiding />,
        path: '/merchant',
      },
      {
        title: 'Kho hàng',
        icon: <MdMapsHomeWork />,
        visible: hasPermission(user, [
          'merchant-addresses_createOneBase',
          'merchant-addresses_deleteOneBase',
          'merchant-addresses_getManyBase',
          'merchant-addresses_getOneBase',
          'merchant-addresses_updateOneBase',
        ]),
        path: '/merchant-address',
      },
      {
        title: 'Bài đăng',
        icon: <DiffOutlined />,
        path: '/product-post',
        visible: hasPermission(user, [
          'product-post_createOneBase',
          'product-post_deleteOneBase',
          'product-post_getManyBase',
          'product-post_getOneBase',
          'product-post_updateOneBase',
        ]),
      },
      {
        title: 'Thành viên',
        icon: <UserOutlined />,
        visible: hasPermission(user, [
          'users_getManyBase',
          'users_createOneBase',
          'users_deleteOneBase',
          'users_updateOneBase',
        ]),
        path: '/user',
      },
      {
        title: 'Flash sale',
        icon: <TagsOutlined />,
        key: 'flashsale',
        visible: hasPermission(user, [
          'flash-sale_FlashSaleController_getFlashSaleMerchant',
          'flash-sale_FlashSaleController_addProductToFlashSale',
          'flash-sale_FlashSaleController_updateProductInFlashSaleByMerchantId',
          'flash-sale_FlashSaleController_getNow',
          'flash-sale_FlashSaleController_getProductInFlashSale',
          'flash-sale_FlashSaleController_getFlashSales',
          // 'FlashSaleController_addProductToFlashSale',
          // 'FlashSaleController_getProductInFlashSaleByMerchantId',
          // 'FlashSaleController_updateProductInFlashSaleByMerchantId',
          'flash-sale_createOneBase',
          'flash-sale_deleteOneBase',
          'flash-sale_getManyBase',
          'flash-sale_getOneBase',
          'flash-sale_updateOneBase',
        ]),
        children: [
          {
            title: 'Danh sách',
            visible: hasPermission(user, [
              'flash-sale_createOneBase',
              'flash-sale_deleteOneBase',
              'flash-sale_createOneBase',
            ]),
            path: '/flash-sale',
          },
          {
            title: 'Sản phẩm',
            visible: hasPermission(user, [
              'flash-sale_FlashSaleController_getProductInFlashSaleByMerchantId',
              'flash-sale_FlashSaleController_addProductToFlashSale',
              'flash-sale_FlashSaleController_updateProductInFlashSaleByMerchantId',
            ]),
            path: '/flash-sale/product',
          },
        ],
      },
      {
        title: 'Marketing',
        icon: <TagOutlined />,
        visible: hasPermission(user, [
          'vouchers_createOneBase',
          'vouchers_deleteOneBase',
          'vouchers_updateOneBase',
        ]),
        key: '/marketing',
        children: [
          {
            title: 'Mã giảm giá',
            path: '/marketing/voucher',
            visible: true,
          },
          // {
          //   title: 'Mua kèm Deal sốc',
          //   path: '/marketing/combo',
          //   visible: true,
          // },
        ],
      },
      {
        title: 'Thông báo',
        icon: <NotificationOutlined />,
        visible: [
          EnumRoleEntityType.merchant,
          EnumRoleEntityType.admin,
        ].includes(user.type),
        path: '/notification',
      },
      {
        title: 'Cài đặt',
        icon: <SettingOutlined />,
        visible: hasPermission(user),
        key: 'settings_key',
        children: [
          {
            title: 'Chung',
            path: '/settings',
            visible: true,
            icon: <SettingOutlined />,
          },
          {
            title: 'Ngân hàng',
            path: '/bank',
            visible: true,
            icon: <BankOutlined />,
          },
          {
            title: 'Quyền',
            key: '/settings/role/root',
            visible: true,
            icon: <UserOutlined />,
            children: [
              {
                title: 'Nhóm quyền',
                path: '/settings/role',
                visible: true,
              },
              {
                title: 'Phân quyền',
                path: '/settings/role/permission',
                visible: true,
              },
            ],
          },
        ],
      },
      {
        title: 'Cấu hình',
        icon: <SettingOutlined />,
        visible: [
          EnumRoleEntityType.merchant,
          EnumRoleEntityType.admin,
        ].includes(user.type),
        key: 'config',
        children: [
          {
            title: 'Thưởng NCC',
            path: '/merchant/config',
            visible: user.type === EnumRoleEntityType.merchant,
            icon: <DollarCircleOutlined />,
          },
          {
            title: 'Mua chung',
            path: '/merchant/group-buying',
            visible: user.type === EnumRoleEntityType.merchant,
            icon: <TeamOutlined />,
          },
          {
            title: 'Ushare',
            path: '/config',
            visible: user.type === EnumRoleEntityType.admin,
            icon: <DollarCircleOutlined />,
          },
        ],
      },
    ])
  }, [user])

  useEffect(() => {
    routes.map((route) => {
      const tmpOpenKeys = _findInTree(route, router.asPath)
      if (tmpOpenKeys) {
        setOpenKeys(tmpOpenKeys)
      }
    })
  }, [router.asPath])

  useEffect(() => {
    setSidebarCollapsed(
      isSidebarCollapsed === undefined ? false : isSidebarCollapsed !== '1'
    )
  }, [isSidebarCollapsed])

  const submenuChild = (obj: SidebarItem) => {
    obj?.permissions?.map((permission) => {
      if (user?.permissions?.includes(permission)) obj.visible = true
    })

    if (obj.children && obj.children.length > 0) {
      const cHtml = obj.children.map((item) => {
        return submenuChild(item)
      })
      return (
        obj.visible && (
          <Menu.SubMenu
            key={obj.path || obj.key}
            title={obj.title}
            icon={obj.icon}
          >
            {cHtml}
          </Menu.SubMenu>
        )
      )
    } else {
      return (
        obj.visible && (
          <Menu.Item key={obj.path || obj.key} icon={obj.icon}>
            <Link href={obj?.path || '#'}>{obj.title}</Link>
          </Menu.Item>
        )
      )
    }
  }

  const menu = routes.map((obj) => {
    obj?.permissions?.map((permission) => {
      if (user?.permissions?.includes(permission)) obj.visible = true
    })
    if (user?.roleId == 1) obj.visible = true
    if (obj.children && obj.children.length > 0) {
      return submenuChild(obj)
    } else {
      return (
        obj.visible && (
          <Menu.Item key={obj.path || obj.key} icon={obj.icon}>
            <Link href={obj?.path || '#'}>{obj.title}</Link>
          </Menu.Item>
        )
      )
    }
  })

  const handleOpenChangeMenu = (openKeys: React.Key[]) => {
    setOpenKeys(openKeys as string[])
  }

  return (
    <>
      <Sider
        trigger={null}
        breakpoint="lg"
        collapsible
        collapsed={sidebarCollapsed}
        style={{
          overflow: 'auto',
          height: '100vh',
          position: 'fixed',
          left: 0,
          zIndex: 100,
        }}
      >
        <Link
          href={{
            pathname: process.env.NEXT_PUBLIC_CUSTOMER_WEB_URL,
          }}
        >
          <div className="slider-logo block m-3 text-center">
            <img src="/logo.png" alt="Logo" width={80} className={'mx-auto'} />
          </div>
        </Link>
        <Menu
          theme="dark"
          mode="inline"
          selectedKeys={[router.asPath]}
          openKeys={openKeys}
          onOpenChange={handleOpenChangeMenu}
        >
          {menu}
        </Menu>
      </Sider>
    </>
  )
}
export default Sidebar

import React from 'react'
import Head from 'next/head'
import { Col, PageHeader, Row, Button, ButtonProps, DatePicker } from 'antd'
import { SITE_NAME } from '@/constants'
import Link from 'components/common/Link'
import s from './index.module.css'

interface ContentButtonProps extends ButtonProps {
  text?: string
  visible?: boolean
  icon?: JSX.Element
}

interface Props {
  title?: string
  onBack?: (e?: React.MouseEvent<HTMLDivElement>) => void
  subTitle?: string
  buttons?: ContentButtonProps[]
  endButtons?: ContentButtonProps[]
  endElement?: JSX.Element
  children?: any
  className?: string
  footer?: JSX.Element
}

const Content = ({
  title,
  onBack,
  buttons,
  children,
  subTitle,
  endButtons,
  endElement,
  footer,
}: Props) => {
  let extraButtons: ContentButtonProps[] = []
  if (buttons) {
    extraButtons = [...extraButtons, ...buttons]
  }
  if (endButtons) {
    extraButtons = [...extraButtons, ...endButtons]
  }
  return (
    <>
      {title ? (
        <Head>
          <title>{title}</title>
        </Head>
      ) : null}
      {(title || onBack || subTitle || extraButtons.length > 0) && (
        <PageHeader
          onBack={onBack}
          ghost={false}
          title={title}
          subTitle={subTitle}
          extra={
            <>
              <Row gutter={16}>
                <Col>{endElement}</Col>
                <Col>
                  {extraButtons.map((button, index) => {
                    if (button.visible) {
                      if (button.href)
                        return (
                          <Link
                            href={button.href}
                            key={`end_button_${button.href}`}
                          >
                            <Button
                              key={`content-button-${index}`}
                              type={button.type}
                              icon={button.icon}
                              className={button.className}
                            >
                              {button.text}
                            </Button>
                          </Link>
                        )
                      return (
                        <Button
                          key={`content-button-${index}`}
                          type={button.type}
                          onClick={button.onClick}
                          icon={button.icon}
                          className={button.className}
                        >
                          {button.text}
                        </Button>
                      )
                    }
                  })}
                </Col>
              </Row>
            </>
          }
          footer={footer}
        />
      )}
      <div className={'m-auto mt-6'}>{children}</div>
    </>
  )
}

export default Content

import React, { FC } from 'react'

interface Props {
  left?: JSX.Element
  right?: JSX.Element
}

const FooterBar: FC<Props> = ({ left, right }) => {
  return (
    <div className={'ant-pro-footer-bar-product flex items-center justify-between bg-white fixed bottom-0 left-0 z-[99] w-full px-7'}>
      <div className={'ant-pro-footer-bar-left'}>{left}</div>
      <div className={'ant-pro-footer-bar-right'}>{right}</div>
    </div>
  )
}
export default FooterBar

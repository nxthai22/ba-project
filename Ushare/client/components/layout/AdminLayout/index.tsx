/* eslint-disable @typescript-eslint/ban-ts-comment */
import {
  BellOutlined,
  LogoutOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined
} from '@ant-design/icons'
import { useLocalStorageState } from 'ahooks'
import {
  Badge,
  Col,
  Divider,
  Dropdown,
  Image,
  Layout,
  Menu,
  Modal,
  Row,
  Space,
  Spin
} from 'antd'
import Sidebar from 'components/layout/AdminLayout/Sidebar'
import { format } from 'date-fns'
import viLocale from 'date-fns/locale/vi'
import { withAuth } from 'hooks'
import localforage from 'localforage'
import _ from 'lodash'
import { useRouter } from 'next/router'
import { setCookie } from 'nookies'
import React, { useEffect, useState } from 'react'
import { HiOutlineHashtag } from 'react-icons/hi'
import {
  MdOutlineCardGiftcard,
  MdOutlineLocalOffer,
  MdOutlineShoppingCart,
  MdSpeakerPhone
} from 'react-icons/md'
import { SiHackthebox } from 'react-icons/si'
import { useRecoilState, useRecoilValue } from 'recoil'
import { authState, loadingState, sidebarCollapsedState } from 'recoil/Atoms'
import {
  EnumNotificationEntityType,
  EnumRoleEntityType,
  NotificationEntity,
  NotificationsService
} from '../../../services'
import { alertError } from '../../../utils'
import FirebaseCloudMessage from '../../common/FirebaseCloudMessage'
import MenuItem from '../../common/NotiMenuItem'
import NotiSubMenu from '../../common/NotiSubMenu'

const { Header, Content, Footer } = Layout
const Index: React.FC = ({ children }) => {
  const setToken = useLocalStorageState('token')[1]
  const router = useRouter()
  const user = useRecoilValue(authState)
  const isLoading = useRecoilValue(loadingState)
  const [sidebarCollapsed, setsidebarCollapsed] = useRecoilState(
    sidebarCollapsedState
  )
  const [notiMap, setNotiMap] = useState<any>([])
  const [notiData, setNotiData] = useState<any>({})
  const [notiCounter, setNotiCounter] = useState<any>({})
  const [unreadCount, setUnreadCount] = useState<number>(0)
  const [notificationDetail, setNotificationDetail] =
    useState<NotificationEntity>(null)
  const [isShowModalNotiDetail, setIsShowModalNotiDetail] =
    useState<boolean>(false)

  const MENU_MAP = {
    order: { title: 'Đơn hàng', icon: MdOutlineShoppingCart },
    offer: { title: 'Ưu đãi', icon: MdOutlineLocalOffer },
    bonus: { title: 'Thưởng', icon: MdOutlineCardGiftcard },
    product: { title: 'Sản phẩm', icon: SiHackthebox },
    highlight: { title: 'Nổi bật', icon: MdSpeakerPhone },
    other: { title: 'Khác', icon: HiOutlineHashtag },
  }
  useEffect(() => {
    switch (user?.type) {
      case EnumRoleEntityType.admin:
        setNotiMap(['order', 'other'])
        break
      case EnumRoleEntityType.merchant:
        setNotiMap(['order', 'offer', 'other'])
        break
      case EnumRoleEntityType.accountant:
        setNotiMap(['order', 'offer', 'other'])
        break
      default:
        setNotiMap(Object.keys(MENU_MAP))
        break
    }
  }, [])

  const logout = () => {
    // unregister service worker
    navigator.serviceWorker.getRegistrations().then(function (registrations) {
      for (const registration of registrations) {
        registration.unregister()
      }
    })
    localforage.removeItem('fcm_token')
    setToken(null)
    window.location.href = '/auth/login'
  }
  const handleCollapse = () => {
    setCookie(null, 'isSidebarCollapsed', sidebarCollapsed ? '1' : '0')
    setsidebarCollapsed(!sidebarCollapsed)
  }
  const onMenuOpenChange = (openKeys: string[]) => {
    if (openKeys.length > 0) {
      const currentType = openKeys[0].replaceAll('noti-sub-menu-', '')
      notiData[currentType] = null
      setNotiData(notiData)
      NotificationsService.notificationControllerGetNotificationByType({
        limit: 100,
        page: 1,
        type: currentType,
      }).then((res) => {
        const data = {}
        data[currentType] = res.data
        setNotiData(data)

        // remove unread count
        if (notiCounter[currentType]) {
          const data = {
            ...notiCounter,
          }
          data[currentType].unreadCount = 0
          setNotiCounter(data)

          // set unread count for the Bell
          const count = _.sumBy(data, 'unreadCount')
          setUnreadCount(count || 0)
        }
      })
    }
  }
  const onMenuItemClick = (notificationId) => {
    if (notificationId) {
      // Get detail
      NotificationsService.notificationControllerGetDetail({
        id: notificationId,
      })
        .then((res) => {
          if (res.type === EnumNotificationEntityType.order) {
            // @ts-ignore
            if (res.data?.orderId) {
              // @ts-ignore
              router.push(`/order/${res.data?.orderId}`)
            }
          }
          if (res.type === EnumNotificationEntityType.other) {
            // @ts-ignore
            if (res.data?.notiConfigId) {
              // @ts-ignore
              router.push(`/notification/${res.data?.notiConfigId}`)
            }
          } else {
            setNotificationDetail(res)
            setIsShowModalNotiDetail(true)
          }
        })
        .catch((error) => {
          alertError(error)
        })
    }
  }

  useEffect(() => {
    getUnreadCount()
  }, [])

  const onVisibleChange = (visible: boolean) => {
    if (visible) {
      getUnreadCount()
    }
  }
  const getUnreadCount = () => {
    NotificationsService.notificationControllerCountUnread({}).then((res) => {
      if (res && res.length > 0) {
        const count = _.sumBy(res, 'unreadCount')
        setUnreadCount(count || 0)
        const counter = {}
        for (let i = 0; i < res.length; i++) {
          const item = res[i]
          counter[item.type] = item
        }
        setNotiCounter(counter)
      }
    })
  }

  // @ts-ignore
  return (
    <Spin spinning={isLoading} tip="Đang tải...">
      <FirebaseCloudMessage
        updateUnreadBell={getUnreadCount}
        onNotificationClick={onMenuItemClick}
      />
      <Layout className={'antd-pro-layout'}>
        <Sidebar />
        <Layout
          style={{
            marginLeft: sidebarCollapsed ? 80 : 200,
            transition: 'all 0.2s',
          }}
        >
          <Header className="header">
            <Row
              justify="space-between"
              style={{
                boxShadow: '0 1px 4px rgb(0 21 41 / 8%)',
                padding: '0 28px',
              }}
            >
              <Col>
                {React.createElement(
                  sidebarCollapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                  {
                    className: 'trigger',
                    onClick: handleCollapse,
                  }
                )}
              </Col>
              <Col>
                <Space direction="horizontal" size={[24, 12]} wrap>
                  <Dropdown
                    trigger={['click']}
                    onVisibleChange={onVisibleChange}
                    overlay={
                      <Menu
                        style={{ width: 360 }}
                        mode="vertical"
                        onOpenChange={onMenuOpenChange}
                        forceSubMenuRender={true}
                        triggerSubMenuAction={'click'}
                      >
                        {notiMap?.map((key) => (
                          <NotiSubMenu
                            key={'noti-sub-menu-' + key}
                            icon={MENU_MAP[key].icon}
                            title={MENU_MAP[key].title}
                            body={notiCounter[key]?.subTitle || ''}
                            unreadCount={notiCounter[key]?.unreadCount || 0}
                            type={key}
                          >
                            {notiData[key] && notiData[key].length > 0 ? (
                              notiData[key]?.map((noti) => (
                                <MenuItem
                                  key={noti?.id}
                                  icon={MENU_MAP[key].icon}
                                  notification={noti}
                                  onClick={() => onMenuItemClick(noti.id)}
                                />
                              ))
                            ) : notiData[key]?.length === 0 ? (
                              <MenuItem isBlank={true} />
                            ) : (
                              <div style={{ width: 360 }}></div>
                            )}
                          </NotiSubMenu>
                        ))}
                      </Menu>
                    }
                    placement="bottomRight"
                  >
                    <Badge
                      count={unreadCount}
                      dot={true}
                      className="cursor-pointer"
                    >
                      <BellOutlined style={{ fontSize: 20 }} />
                    </Badge>
                  </Dropdown>

                  <Dropdown
                    overlay={
                      <Menu>
                        <Menu.Item key="setting:2">
                          <a href={'#'} onClick={logout}>
                            <LogoutOutlined /> Đăng xuất
                          </a>
                        </Menu.Item>
                      </Menu>
                    }
                    placement="bottomCenter"
                  >
                    <span>
                      <UserOutlined /> {user?.fullName}
                    </span>
                  </Dropdown>
                </Space>
              </Col>
            </Row>
          </Header>
          <Content>
            <div className="site-layout-background">{children}</div>
          </Content>
          <Footer className="text-center">UShare ©2021</Footer>
        </Layout>
      </Layout>
      <Modal
        visible={isShowModalNotiDetail}
        onCancel={() => {
          setIsShowModalNotiDetail(false)
        }}
        destroyOnClose={true}
        width={600}
        footer={false}
      >
        <Image
          src={notificationDetail?.image}
          height={'100%'}
          preview={false}
        />
        <div style={{ fontWeight: '600', fontSize: 22 }}>
          {notificationDetail?.title}
        </div>
        <div style={{ fontWeight: '400', fontSize: 16 }}>
          <span>Thời gian: </span>
          <span>
            {notificationDetail?.createdAt
              ? format(
                  new Date(notificationDetail.createdAt),
                  'HH:mm dd-MM-yyyy',
                  { locale: viLocale }
                )
              : ''}
          </span>
        </div>
        <Divider />
        <p style={{ fontWeight: '600', fontSize: 18 }}>
          Nội dung chương trình:
        </p>
        <div
          dangerouslySetInnerHTML={{
            __html: notificationDetail?.content,
          }}
          className={'leading-8 text-lg'}
        />
      </Modal>
    </Spin>
  )
}

export default withAuth(Index)
// export default Id

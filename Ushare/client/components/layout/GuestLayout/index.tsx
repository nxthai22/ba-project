import { useMount } from 'ahooks'
import { AutoComplete, Button, Input, SelectProps, Spin } from 'antd'
import axios, { AxiosRequestConfig } from 'axios'
import Link from 'components/common/Link'
import FooterSupport from 'components/guest/common/FooterSupport'
import { FreeShip, Genuine, Secure, Time } from 'constants/icons'
import { useCategory } from 'hooks/category'
import { useRouter } from 'next/router'
import { parseCookies } from 'nookies'
import { stringify } from 'qs'
import { useEffect, useState } from 'react'
import Slider from 'react-slick'
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import {
  CartState,
  cartState,
  loadingState,
  orderInfoState,
} from 'recoil/Atoms'
import { ProductsService } from 'services'
import { serviceOptions } from 'services/serviceOptions'
import ToggleCategory from './ToggleCategory/ToggleCategory'

const GuestLayout = ({ children }) => {
  const router = useRouter()
  const [cartData, setCartData] = useRecoilState(cartState)
  const isLoading = useRecoilValue(loadingState)
  const setOrderInfo = useSetRecoilState(orderInfoState)
  const { cart } = parseCookies(null)
  const { pub_id = 0 } = router.query
  const [cartQuantity, setCartQuantity] = useState<number>(0)
  const [keyword, setKeyword] = useState('')
  const [keywords, setKeywords] = useState<SelectProps<object>['options']>([])
  const axiosConfig: AxiosRequestConfig = {
    baseURL: process.env.NEXT_PUBLIC_API_URL || 'http://localhost:4000/',
    timeout: 60000, // 1 phút
    paramsSerializer: (params) => stringify(params, { arrayFormat: 'repeat' }),
  }

  const { categories, getCategory } = useCategory()

  serviceOptions.axios = axios.create(axiosConfig)

  useMount(() => {
    getCategory()
  })

  useEffect(() => {
    if (cart) {
      const tmpCart: CartState = JSON.parse(cart)
      setCartData(tmpCart)
    }
  }, [cart, pub_id])

  useEffect(() => {
    if (cartData?.[Number(pub_id)]) {
      setCartQuantity(
        Object.entries(cartData?.[Number(pub_id)]).reduce((total, item) => {
          return total + item?.[1]?.length
        }, 0)
      )
    } else {
      setCartQuantity(0)
    }
  }, [cartData])

  const onSearch = (value) => {
    if (value !== '')
      ProductsService.getManyBase({
        filter: [`name||$contL||${value}`],
        limit: 10,
      }).then((products) =>
        setKeywords(
          products.data.map((product) => ({
            value: `/san-pham/${product.slug}_${product.id}`,
            label: product.name,
          }))
        )
      )
  }

  const onSearchItemSelect = (product) => {
    if (product !== '') {
      router
        .push({
          pathname: product,
          query: {
            ...(pub_id && { pub_id: Number(pub_id) }),
          },
        })
        .then(() => {
          setKeyword('')
          setKeywords([])
        })
    }
  }

  return (
    <Spin spinning={isLoading} tip="Đang tải...">
      <div className={'guest-layout'}>
        <div className={'bg-white'}>
          <div className="header max-w-6xl m-auto h-20 items-center content-center hidden md:flex">
            <div className={'flex w-full items-center h-11'}>
              <div>
                <Link
                  href={{
                    pathname: `/`,
                    query: {
                      ...(pub_id && { pub_id: Number(pub_id) }),
                    },
                  }}
                >
                  <img src={'/logo.svg'} alt={'Ushare'} />
                </Link>
              </div>
              <div className={'grow pl-5 pr-10 relative'}>
                <AutoComplete
                  className={'w-full'}
                  options={keywords}
                  onSearch={onSearch}
                  onSelect={onSearchItemSelect}
                  dropdownClassName={'search-result'}
                  value={keyword}
                  onChange={setKeyword}
                >
                  <Input
                    className={'h-10 rounded-md'}
                    placeholder="Nhập để tìm kiếm"
                    size="large"
                    suffix={<img src={'/images/search.svg'} />}
                  />
                </AutoComplete>
              </div>
              <div className={'flex items-center'}>
                {/*<Link*/}
                {/*  href={{*/}
                {/*    pathname: `/checkout/cart`,*/}
                {/*    query: {*/}
                {/*      ...(pub_id && { pub_id: Number(pub_id) }),*/}
                {/*    },*/}
                {/*  }}*/}
                {/*>*/}
                <Button
                  type={'link'}
                  size={'small'}
                  className={'mb-3'}
                  onClick={() => {
                    router.push({
                      pathname: `/checkout/cart`,
                      query: {
                        ...(pub_id && { pub_id: Number(pub_id) }),
                      },
                    })
                    setOrderInfo(null)
                  }}
                >
                  <span
                    className={
                      'inline-block text-white text-xs text-center w-8 pt-4'
                    }
                    style={{
                      background: 'url("/images/cart.svg") no-repeat center',
                    }}
                  >
                    {cartQuantity}
                  </span>
                </Button>
                {/*</Link>*/}
              </div>
            </div>
          </div>
        </div>
        <div className={'top-menu md:h-11 px-4 md:px-0 md:bg-price bg-white '}>
          <div className={'flex max-w-6xl m-auto h-full justify-between'}>
            <div className={'top-menu-left flex items-center gap-1'}>
              <ToggleCategory></ToggleCategory>
            </div>
            <div className={'items-center flex md:hidden'}>
              <div className={'flex-none w-40 py-2'}>
                <Link
                  href={{
                    pathname: `/`,
                    query: {
                      ...(pub_id && { pub_id: Number(pub_id) }),
                    },
                  }}
                >
                  <img src={'/logo.svg'} alt={'Ushare'} />
                </Link>
              </div>
            </div>
            <div className={'items-center flex md:hidden'}>
              <div className={'flex h-full items-center'}>
                <div>
                  <Link
                    href={{
                      pathname: `/checkout/cart`,
                      query: {
                        ...(pub_id && { pub_id: Number(pub_id) }),
                      },
                    }}
                  >
                    <div
                      className={'items-center w-8 h-8 '}
                      style={{
                        background: 'url("/images/cart.svg") no-repeat center',
                      }}
                    >
                      <div
                        className={'text-white text-xs text-center w-8 pt-4'}
                      >
                        {cartQuantity}
                      </div>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
            <div className={'top-menu-right items-center hidden md:flex'}>
              {/*<Link*/}
              {/*  href={{*/}
              {/*    pathname: `/huong-dan-mua-hang`,*/}
              {/*    query: {*/}
              {/*      ...(pub_id && { pub_id: Number(pub_id) }),*/}
              {/*    },*/}
              {/*  }}*/}
              {/*>*/}
              {/*  <div className={'text-white mr-1'}>Hướng dẫn mua hàng</div>*/}
              {/*</Link>*/}
              {/*<span className={'text-white mr-1'}>|</span>*/}
              {/*<Link*/}
              {/*  href={{*/}
              {/*    pathname: `/cam-ket`,*/}
              {/*    query: {*/}
              {/*      ...(pub_id && { pub_id: Number(pub_id) }),*/}
              {/*    },*/}
              {/*  }}*/}
              {/*>*/}
              {/*  <div className={'text-white mr-1'}>Cam kết của Ushare</div>*/}
              {/*</Link>*/}
              {/*<span className={'text-white mr-1'}>|</span>*/}
              <a href={'tel:0904113364'}>
                <div className={'text-white'}>Liên Hệ</div>
              </a>
            </div>
          </div>
        </div>
        <div className={'flex md:hidden px-4 relative bg-white py-3'}>
          <AutoComplete
            className={'w-full'}
            options={keywords}
            onSearch={onSearch}
            onSelect={onSearchItemSelect}
            dropdownClassName={'search-result'}
            value={keyword}
            onChange={setKeyword}
          >
            <Input
              className={'h-11 rounded-md'}
              placeholder="Nhập để tìm kiếm"
              size="large"
              suffix={<img src={'/images/search.svg'} />}
            />
          </AutoComplete>
        </div>
        <div>{children}</div>
        <div className="footer bg-white">
          <div className={'bg-price'}>
            <div className="flex max-w-6xl m-auto gap-5">
              <div className={'w-full my-4 gap-2 p-4 text-base'}>
                <Slider
                  arrows={false}
                  slidesToShow={4}
                  slidesToScroll={4}
                  infinite={false}
                  responsive={[
                    {
                      breakpoint: 600,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        arrows: false,
                      },
                    },
                    {
                      breakpoint: 480,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        arrows: false,
                      },
                    },
                  ]}
                >
                  <div className={'flex flex-col border-r'}>
                    <div className={'flex justify-center py-5 text-white'}>
                      <Genuine />
                    </div>
                    <div
                      className={
                        'uppercase text-center px-1 text-white font-semibold'
                      }
                    >
                      Hàng chính hãng
                    </div>
                    <div className={'text-center px-1 text-white mt-2'}>
                      Đền 200% giá trị nếu sai sự thật
                    </div>
                  </div>
                  <div className={'flex flex-col border-r'}>
                    <div className={'flex justify-center py-5 text-white'}>
                      <FreeShip />
                    </div>
                    <div
                      className={
                        'uppercase text-center px-1 text-white font-semibold'
                      }
                    >
                      Miễn phí vận chuyển
                    </div>
                    <div className={'text-center px-1 text-white mt-2'}>
                      Áp dụng trên toàn quốc
                    </div>
                  </div>
                  <div className={'flex flex-col border-r'}>
                    <div className={'flex justify-center py-5 text-white'}>
                      <Time />
                    </div>
                    <div
                      className={
                        'uppercase text-center px-1 text-white font-semibold'
                      }
                    >
                      Đổi mới tại nhà
                    </div>
                    <div className={'text-center px-1 text-white mt-2'}>
                      Trong 7 ngày đầu nếu có lỗi
                    </div>
                  </div>
                  <div className={'flex flex-col'}>
                    <div className={'flex justify-center py-5 text-white'}>
                      <Secure />
                    </div>
                    <div
                      className={
                        'uppercase text-center px-1 text-white font-semibold'
                      }
                    >
                      Được kiểm tra hàng
                    </div>
                    <div className={'text-center px-1 text-white mt-2'}>
                      Kiểm tra hàng trước khi thanh toán
                    </div>
                  </div>
                </Slider>
              </div>
            </div>
          </div>
          <div>
            <div className="flex max-w-6xl m-auto gap-5 grid md:grid-cols-4 py-12 md:px-0 px-4">
              <div className={'text-secondary'}>
                <img src={'/logo.svg'} className={'mb-4'} />
                <div className={'flex mb-5'}>
                  <img src={'/images/location.svg'} className={'mr-4'} />
                  V6 A08 KĐT Văn Phú, Hà Đông, Hà Nội
                </div>
                <div className={'flex mb-5'}>
                  <img src={'/images/phone.svg'} className={'mr-4'} />
                  0904 11 3364
                </div>
                <div className={'flex mb-5'}>
                  <img src={'/images/mail.svg'} className={'mr-4'} />
                  <a
                    href={'mailto:cskh.ushare@gmail.com'}
                    target={'_blank'}
                    rel={'noreferrer'}
                  >
                    <span className={'text-secondary hover:text-price'}>
                      cskh.ushare@gmail.com
                    </span>
                  </a>
                </div>
              </div>
              <div className={'h-px bg-gray-200 md:hidden'} />
              <div className={'text-secondary'}>
                <div
                  className={'flex mb-3 mt-6 font-semibold uppercase'}
                  style={{
                    color: '#FB81AA',
                  }}
                >
                  Ushare
                </div>
                <div className={'flex mb-3'}>Giới thiệu</div>
                <div className={'flex mb-3'}>Chính sách đổi trả</div>
                <div className={'flex mb-3'}>Chính sách giao hàng</div>
                <div className={'flex'}>Chính sách bảo mật</div>
              </div>
              <div className={'h-px bg-gray-200 md:hidden'} />
              <div className={'text-secondary'}>
                <div
                  className={'flex mb-3 mt-6 font-semibold uppercase'}
                  style={{
                    color: '#FB81AA',
                  }}
                >
                  Danh mục sản phẩm
                </div>
                {categories?.map((category) => (
                  <div
                    key={`footer-category-${category.id}`}
                    className={'flex mb-3'}
                  >
                    <Link
                      href={{
                        pathname: `/danh-muc/${category.slug}_${category.id}`,
                        query: {
                          ...(pub_id && { pub_id: Number(pub_id) }),
                        },
                      }}
                    >
                      <span className={'text-secondary hover:text-price'}>
                        {category.name}
                      </span>
                    </Link>
                  </div>
                ))}
              </div>
              <div className={'h-px bg-gray-200 md:hidden'} />
              <div className={'text-secondary'}>
                <div
                  className={'flex mb-3 mt-6 font-semibold uppercase'}
                  style={{
                    color: '#FB81AA',
                  }}
                >
                  Kết nối với chúng tôi
                </div>
                <div className={'flex gap-3'}>
                  <a
                    href={'https://www.facebook.com/ushare.com.vn'}
                    target={'_blank'}
                    rel={'noreferrer'}
                    className={'pr-2'}
                  >
                    <img src={'/images/facebook.svg'} />
                  </a>
                  <a
                    href={'https://www.facebook.com/groups/569695171038971 '}
                    target={'_blank'}
                    rel={'noreferrer'}
                    className={'pr-2'}
                  >
                    <img src={'/images/google.svg'} />
                  </a>
                  <a
                    href={
                      'https://www.youtube.com/channel/UCpwqRSeQCsQnF4kCerVTySg'
                    }
                    target={'_blank'}
                    rel={'noreferrer'}
                    className={'pr-2'}
                  >
                    <img src={'/images/youtube.svg'} />
                  </a>
                </div>
              </div>
            </div>
            <div className={'h-5 bg-price'} />
          </div>
        </div>
        <FooterSupport />
      </div>
    </Spin>
  )
}
export default GuestLayout

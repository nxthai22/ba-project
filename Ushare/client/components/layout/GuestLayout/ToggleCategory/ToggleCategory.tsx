import { Dropdown, Menu } from 'antd'
import { MenuIcon } from 'constants/icons'
import { useCategory } from 'hooks/category'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { ProductCategoryEntity } from 'services';
import * as _ from 'lodash';
import clxs from 'clsx';

const ToggleCategory = () => {
  const router = useRouter()
  const { pub_id = 0 } = router.query
  const { categories } = useCategory();
  const [colapses, setColapses] = useState<Record<string, boolean>>({});

  const getCategoryTreeEle = (categories: ProductCategoryEntity[], level = 0) => {
    const children = _.sortBy(categories, (item) => item.name);
    return _.map(children, (item) => {
      const isColapse = !!colapses[item.id];

      return (
        <>
           <Menu.Item
            style={{ paddingLeft: 15 * level + 'px' }}
            key={item.id}
            className={`border-0 pr-0 category-level-${level}`}
          >
            {item.childrens?.length ? (
              <span
                className={clxs('absolute -right-2 w-10 text-center z-10')}
                onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  setColapses({ ...colapses, [item.id]: !isColapse });
                }}
              >
                <span
                  className={clxs(
                    'fa-solid fa-angle-right inline-block transition-transform duration-900 easy text-white md:text-secondary',
                    { 'rotate-0': isColapse, 'rotate-90': !isColapse }
                  )}
                ></span>
              </span>
            ) : null}
            <Link href={`/danh-muc/${item.slug}_${item.id}`}>
              <div className={`text-white md:text-secondary`}>
              {item.name}

                </div>
            </Link>
          </Menu.Item>
          {item.childrens?.length && !isColapse
            ? getCategoryTreeEle(item.childrens, level + 1)
            : null}
        </>
      );
    });
  };

  return (
    <Dropdown
      overlayClassName={'top-menu-dropdown md:w-96 w-full'}
      overlay={
        <Menu
          className={
            'top-menu-dropdown-menu md:w-90 w-full md:bg-white bg-price text-white md:rounded-b-2xl rounded-t-2xl px-5'
          }
        >
          <Menu.Item key="title" className='p-0'>
            <div className={'flex items-center justify-between'}>
              <div
                className={
                  'text-white md:text-price uppercase text-lg font-semibold'
                }
              >
                Danh mục
              </div>
              <span className={'text-white md:text-price'}>
                <svg
                  width="12"
                  height="13"
                  viewBox="0 0 12 13"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M1.16525 13C0.756388 13 0.456559 12.9754 0.265758 12.9261C0.088586 12.8646 0 12.7784 0 12.6676C0 12.5568 0.0613288 12.4152 0.183986 12.2429L4.53833 6.22301L0.674617 0.757101C0.53833 0.572442 0.470187 0.43087 0.470187 0.332386C0.470187 0.233901 0.565588 0.153881 0.756388 0.0923278C0.947189 0.0307748 1.24702 0 1.65588 0H2.31005C2.58262 0 2.8075 0.0246197 2.98467 0.0738622C3.17547 0.110794 3.31857 0.196969 3.41397 0.332386L6.07155 4.1179L8.85179 0.313918C8.93356 0.178502 9.06985 0.0923287 9.26065 0.0553966C9.46508 0.0184653 9.69676 0 9.95571 0H10.5281C10.9233 0 11.2164 0.0307748 11.4072 0.0923278C11.598 0.14157 11.6934 0.22159 11.6934 0.332386C11.6934 0.381628 11.6729 0.443182 11.632 0.517045C11.6048 0.590909 11.5639 0.670927 11.5094 0.757101L7.56388 6.22301L11.8365 12.2429C11.9455 12.4152 12 12.5568 12 12.6676C12 12.7661 11.9114 12.8461 11.7342 12.9077C11.5571 12.9692 11.2572 13 10.8348 13H10.201C9.92845 13 9.69676 12.9815 9.50596 12.9446C9.32879 12.8954 9.1925 12.803 9.0971 12.6676L6.01022 8.32813L2.86201 12.6861C2.75298 12.8215 2.60307 12.9077 2.41227 12.9446C2.22146 12.9815 1.99659 13 1.73765 13H1.16525Z"
                    fill="currentColor"
                  />
                </svg>
              </span>
            </div>
          </Menu.Item>
          <Menu.Divider className={'bg-white md:bg-price p-0 mb-1'} />
          {getCategoryTreeEle(categories)}
          <Menu.Divider className={'bg-white md:bg-price px-5 md:hidden'} />
          <Menu.Item className={'md:hidden border-0 p-0'}>
            <Link
              href={{
                pathname: `/huong-dan-mua-hang`,
                query: {
                  ...(pub_id && { pub_id: Number(pub_id) }),
                },
              }}
            >
              <div className={'text-white mr-1'}>Hướng dẫn mua hàng</div>
            </Link>
          </Menu.Item>
          <Menu.Item className={'md:hidden border-0 p-0'}>
            <Link
              href={{
                pathname: `/cam-ket`,
                query: {
                  ...(pub_id && { pub_id: Number(pub_id) }),
                },
              }}
            >
              <div className={'text-white mr-1'}>Cam kết của Ushare</div>
            </Link>
          </Menu.Item>
          <Menu.Item className={'md:hidden border-0 p-0'}>
            <Link
              href={{
                pathname: `/lien-he`,
                query: {
                  ...(pub_id && { pub_id: Number(pub_id) }),
                },
              }}
            >
              <div className={'text-white'}>Liên Hệ</div>
            </Link>
          </Menu.Item>
        </Menu>
      }
    >
      <div
        className={
          'cursor-pointer h-11 flex items-center text-price md:text-white'
        }
      >
        <MenuIcon />
      </div>
    </Dropdown>
  )
}

export default ToggleCategory

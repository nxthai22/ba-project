import { DetailProudct, ProductVariantEntity } from 'services'
import { compactNumber, formatCurrency } from 'utils'

interface IProps {
  product: DetailProudct
  selectedVariant: ProductVariantEntity
}

const Price = ({ product, selectedVariant }: IProps) => {
  return (
    <>
      <div className='flex flex-col md:flex-row md:justify-between md:items-center'>
        <div>
          {product?.priceBeforeDiscount > 0 && (
            <span className='text-primary line-through mr-3 text-lg md:text-2xl'>
              {formatCurrency(product?.priceBeforeDiscount)}
            </span>
          )}
          <span className='font-bold text-price text-[22px] md:text-[28px]'>
            {formatCurrency(
              selectedVariant
                ? selectedVariant?.price
                : product?.price
            )}
          </span>
        </div>
        {product?.saleAmount > 0 && (
          <div className='text-secondary text-sm md:text-base'>
            Lượt bán: {compactNumber(product?.saleAmount)}
          </div>
        )}
      </div>
    </>
  )
}

export default Price

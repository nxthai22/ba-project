import { FreeShip, Genuine, Secure, Time } from 'constants/icons'

const Warranty = () => {
  return (
    <div className='w-full grid grid-cols-4 mt-8 gap-2'>
      <div className='flex flex-col'>
        <div className='border rounded-md flex justify-center md:py-5 text-price py-1 px-3'>
          <Genuine />
        </div>
        <div className='uppercase text-center md:px-1 text-secondary mt-5 text-xs md:font-bold'>
          Hàng chính hãng
        </div>
        <div className='text-center md:px-1 text-secondary mt-2 text-xs'>
          Đền 200% giá trị nếu sai sự thật
        </div>
      </div>
      <div className='flex flex-col'>
        <div className='border rounded-md flex justify-center md:py-5 py-1 px-3 text-price'>
          <FreeShip />
        </div>
        <div className='uppercase text-center md:px-1 text-secondary mt-5 text-xs md:font-bold'>
          Miễn phí vận chuyển
        </div>
        <div className='text-center md:px-1 text-secondary mt-2 text-xs'>
          Áp dụng trên toàn quốc
        </div>
      </div>
      <div className='flex flex-col'>
        <div className='border rounded-md flex justify-center md:py-5 py-1 px-3 text-price'>
          <Time />
        </div>
        <div className='uppercase text-center md:px-1 text-secondary mt-5 text-xs md:font-bold'>
          Đổi mới tại nhà
        </div>
        <div className='text-center md:px-1 text-secondary mt-2 text-xs'>
          Trong 7 ngày đầu nếu có lỗi
        </div>
      </div>
      <div className='flex flex-col'>
        <div className='border rounded-md flex justify-center md:py-5 py-1 px-3 text-price'>
          <Secure />
        </div>
        <div className='uppercase text-center md:px-1 text-secondary mt-5 text-xs md:font-bold'>
          Được kiểm tra hàng
        </div>
        <div className='text-center md:px-1 text-secondary mt-2 text-xs'>
          Kiểm tra hàng trước khi thanh toán
        </div>
      </div>
    </div>
  )
}

export default Warranty

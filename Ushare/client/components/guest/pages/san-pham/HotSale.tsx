import Countdown from 'react-countdown'

interface IProps {
  hotSaleExpire: number
}

const HotSale = ({ hotSaleExpire }: IProps) => {
  if (!hotSaleExpire) {
    return null
  }

  return (
    <div className='flash-sale relative pt-5 mb-2'>
      <div className='flex border-price border rounded-lg items-center py-2 md:py-3 justify-center relative'>
        <div className='text-primary absolute left-4 md:text-2xl text-lg font-semibold -md:top-6 -top-4 uppercase bg-white px-2'>
          Hotsale
        </div>
        <div className='text-price md:mr-10 mr-3 md:text-xl text-base'>
          Kết thúc trong
        </div>
        {hotSaleExpire && (
          <Countdown
            date={hotSaleExpire}
            renderer={({
              hours,
              minutes,
              seconds,
              completed,
            }) => {
              if (!completed) {
                return (
                  <div className='text-price text-2xl font-bold flex flex-row items-center'>
                    <div className='bg-price flex items-center justify-center rounded-[4px] md:rounded-full text-white text-lg md:text-2xl font-medium mr-2 w-8 md:w-11 h-11'>
                      {hours}
                    </div>
                    :
                    <div className='bg-price flex items-center justify-center rounded-[4px] md:rounded-full text-white text-lg md:text-2xl font-medium mx-2 w-8 md:w-11 h-11'>
                      {minutes}
                    </div>
                    :
                    <div className='bg-price flex items-center justify-center rounded-[4px] md:rounded-full text-white text-lg md:text-2xl font-medium ml-2 w-8 md:w-11 h-11'>
                      {seconds}
                    </div>
                  </div>
                )
              }
            }}
          />
        )}
      </div>
    </div>
  )
}

export default HotSale

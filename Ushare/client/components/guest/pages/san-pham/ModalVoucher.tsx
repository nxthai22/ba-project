import { Modal } from 'antd'
import { VoucherEntity } from 'services'
import { formatDate } from 'utils'
import { format, formatDistance } from 'date-fns'
import { vi } from 'date-fns/locale'

interface IProps {
  currentVoucher: VoucherEntity
  setCurrentVoucher: (id: any) => void
  setCurrentVoucherId: (id: any) => void
}

const ModalVoucher = ({
  currentVoucher,
  setCurrentVoucher,
  setCurrentVoucherId
 }: IProps) => {

  return (
    <Modal
      visible={Boolean(currentVoucher)}
      footer={false}
      onCancel={() => {
        setCurrentVoucher(null)
        setCurrentVoucherId(null)
      }}
    >
      <div className='text-lg text-primary font-semibold'>
        Chi tiết mã giảm giá
      </div>
      {currentVoucher && (
        <>
          <div className='py-1 flex flex-row'>
            <img src='/images/voucher-bg-right.svg' alt='Voucher' />
            <div
              className='grow flex flex-row p-2'
              style={{
                background: 'linear-gradient(90deg, #F54C84 -1.6%, rgba(251, 20, 96, 0.69) 118.95%)'
              }}
            >
              <div className='flex-none'>
                <img src='/images/voucher.svg' alt={'Voucher'} />
              </div>
              <div className='grow text-white ml-2'>
                <div className='font-bold text-sm'>
                  {currentVoucher.name}
                </div>
                <div className='flex flex-row justify-between'>
                  <div className='text-xs'>
                    <div>
                      {format(
                        new Date(currentVoucher.endDate),
                        'd/M/y',
                        { locale: vi }
                      )}
                    </div>
                    <div>
                      {formatDistance(
                        new Date(currentVoucher.endDate),
                        new Date(),
                        { locale: vi }
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <img src='/images/voucher-bg-left.svg' alt='Voucher' />
          </div>
          <div className='text-secondary text-sm my-2'>
            {currentVoucher?.description}
          </div>
          <div className='border-t' />
          <div className='my-2'>
            <div className='font-semibold text-primary mb-2'>
              Có hiệu lực
            </div>
            <div className='text-secondary text-sm'>
              {formatDate(currentVoucher?.startDate, 'HH:mm dd/MM/y')} -{' '}
              {formatDate(currentVoucher?.endDate, 'HH:mm dd/MM/y')}
            </div>
          </div>
          <div className='border-t' />
          <div className='my-2'>
            <div className='font-semibold text-primary mb-2'>
              Sản phẩm
            </div>
            <div className='text-secondary text-sm'>
              {currentVoucher?.products.map(product => (
                <div key={`voucher-product-${product.id}`}>
                  {product.name}
                </div>
              ))}
            </div>
          </div>
          <div className='border-t' />
          <div className='my-2'>
            <div className='font-semibold text-primary mb-2'>
              Phương thức thanh toán
            </div>
            <div className='text-secondary text-sm'>
              Mọi hình thức thanh toán
            </div>
          </div>
        </>
      )}
    </Modal>
  )
}

export default ModalVoucher

import { Rate } from 'antd'

const Ratings = () => {
  return (
    <>
      <div className={'mb-4'}>
        <div className={'uppercase text-primary font-bold mb-1'}>
          Đánh giá sản phẩm
        </div>
        <div className={'flex items-center'}>
          <Rate allowHalf value={5} disabled />
          <span
            className={'ml-2 text-sm font-bold'}
            style={{ color: '#9098B1' }}
          >
            5
          </span>
        </div>
      </div>
      <div className={'text-secondary text-sm'}>
        <span className={'text-price'}>Miễn phí</span> vận chuyển
        | Giao hàng{' '}
        <span className={'text-price'}>toàn quốc</span>
      </div>
    </>
  )
}

export default Ratings

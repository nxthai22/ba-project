import { DetailProudct, VoucherEntity } from 'services'
import { format, formatDistance } from 'date-fns'
import { vi } from 'date-fns/locale'

interface IProps {
  product: DetailProudct
  setCurrentVoucherId: (id: number) => void
}

const Vouchers = ({ product, setCurrentVoucherId }: IProps) => {
  if (!product?.vouchers.length) {
    return null
  }

  return (
    <>
      <div className='uppercase text-primary font-bold'>
        Ưu đãi cho khách hàng
      </div>
      <div
        className='border border-gray-200 p-2 overflow-y-scroll'
        style={{ maxHeight: 168 }}
      >
        {product?.vouchers.map((voucher: VoucherEntity) => (
          <div
            key={voucher.id}
            className='py-1 flex flex-row cursor-pointer'
            onClick={() => setCurrentVoucherId(voucher.id)}
          >
            <img src='/images/voucher-bg-right.svg' alt='Voucher' />
            <div
              className='grow flex flex-row p-2'
              style={{
                background: 'linear-gradient(90deg, #F54C84 -1.6%, rgba(251, 20, 96, 0.69) 118.95%)',
              }}
            >
              <div>
                <img src='/images/voucher.svg' alt='Voucher' />
              </div>
              <div className='grow text-white ml-2'>
                <div className='font-bold text-sm'>
                  {voucher.name}
                </div>
                <div className='flex flex-row justify-between'>
                  <div className='text-xs'>
                    <div>
                      {format(
                        new Date(voucher.endDate),
                        'd/M/y',
                        { locale: vi }
                      )}
                    </div>
                    <div>
                      {formatDistance(
                        new Date(voucher.endDate),
                        new Date(),
                        { locale: vi }
                      )}
                    </div>
                  </div>
                  <div className='flex flex-row items-end'>
                    <div className='items-center flex'>
                      <span className='mr-1 text-xs'>
                        Xem chi tiết
                      </span>
                      <img
                        src='/images/arrow/right.svg'
                        className='w-1'
                        alt='Right Arrow'
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <img src='/images/voucher-bg-left.svg' alt='Voucher' />            
          </div>
        ))}
      </div>
    </>
  )
}

export default Vouchers

import Image from 'next/image'

export default function NoProduct () {
  return (
    <div className='flex flex-col justify-center items-center mt-16'>
      <div className='relative'>
        <Image
          src='/images/no-product.svg'
          alt='No product'
          width={369}
          height={389}
        />
      </div>
      <div className='uppercase text-lg text-price font-medium my-10'>
        Không có sản phẩm
      </div>
    </div>
  )
}

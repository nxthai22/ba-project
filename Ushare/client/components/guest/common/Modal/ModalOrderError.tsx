import { Button, Modal } from 'antd'

export interface IProps {
  visible: boolean
  messageError: string
  onShowError: (param: boolean) => void
}

export function ModalOrderError ({ visible, messageError, onShowError }: IProps) {
  return (
    <Modal
      className={'rounded-modal'}
      centered={true}
      visible={visible}
      footer={null}
      title={null}
      width={350}
      destroyOnClose={true}
      closable={false}
    >
      <div className={'w-full flex flex-col justify-center items-center'}>
        <p className={'m-0 font-bold text-center text-[#F4447F]'}>
          {messageError}
        </p>
        <Button
          className={'rounded-md mt-4 text-[#F4447F]'}
          onClick={() => {onShowError(false)}}
        >
          Đóng
        </Button>
      </div>
    </Modal>
  )
}

import { Button, Modal } from 'antd'
import { useRouter } from 'next/router'
import Image from 'next/image'

export interface IProps {
  visible: boolean
  orderIds: any[]
  setOrderIds: (param: any[]) => void
}

export function ModalOrderSuccess ({ visible, orderIds, setOrderIds }: IProps) {
  const router = useRouter()

  return (
    <Modal
      visible={visible}
      footer={false}
      className='order-success'
      width={780}
      onCancel={() => setOrderIds([])}
    >
      <div className='flex justify-center flex-col md:px-12'>
        <div className='text-center'>
          <div className='mx-auto w-48'>
            <Image
              src='/images/checkout-success.png'
              alt='Checkout success'
              width={362}
              height={230}
            />
          </div>
          <div className='text-lg text-price font-bold'>
            Tạo đơn hàng thành công
          </div>
          <div className='text-primary mt-3'>
            Cảm ơn bạn đã mua hàng tại Ushare! Chúng tôi sẽ gọi điện trong 24h
            để xác nhận đơn hàng.
          </div>
          <div className='text-primary mt-3 text-base'>
            Mã đơn hàng:
            <br />
            {orderIds.map(orderId => 
              <div key={`order-${orderId}`} className='text-price'>
                {orderId}
              </div>
            )}
          </div>
        </div>
        <div className='flex justify-center mt-8 gap-2 md:gap-4'>
          <Button
            className='h-9 md:h-12 md:px-10 md:text-base text-white bg-price border-price hover:bg-white hover:text-price'
            onClick={() => router.push('/')}
          >
            Quay lại trang chủ
          </Button>
          <Button
            className='h-9 md:h-12 md:px-10 md:text-base text-price border-price hover:bg-price hover:text-white'
            onClick={() => router.reload()}
          >
            Xem giỏ hàng
          </Button>
        </div>
      </div>
    </Modal>
  )
}

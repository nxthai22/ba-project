const FooterSupport = () => {
  return (
    <div className='fixed right-3 md:right-7 bottom-5'>
      <a
        href='tel:0904113364'
        target='_blank'
        rel='noreferrer'
        className='block w-12 md:w-14 hover:opacity-80'
      >
        <img
          src='/images/phone-float.png'
          alt='Gọi điện cho Ushare'
        />
      </a>
      <a
        href='https://m.me/158714087586067'
        target='_blank'
        rel='noreferrer'
        className='block w-12 md:w-14 hover:opacity-80'
      >
        <img
          src='/images/chat-float.png'
          alt='Chat với Ushare'
        />
      </a>
    </div>
  )
}

export default FooterSupport

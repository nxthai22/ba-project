import { Calendar, Col, Modal, Radio, Row, Typography } from 'antd'
import { isToday } from 'date-fns'
import { FlashSaleEntity } from 'services'
import { formatDate } from 'utils'

export interface IProps {
  visible: boolean
  onOk?: (flashSale: FlashSaleEntity) => void
  onCancel?: () => void
  callback: () => void
  fetchFlashSales: (time?: Date) => void
  flashSales: FlashSaleEntity[]
  onSelectFlashSale: (value: number) => void
  selectedFlashSaleId: number
}

export default function ModalFlashSale (props: IProps) {
  const {
    visible,
    onOk,
    onCancel,
    callback,
    fetchFlashSales,
    flashSales,
    onSelectFlashSale,
    selectedFlashSaleId,
  } = props
  return (
    <Modal
      title='Chọn khung giờ Flash sale'
      width={'960px'}
      visible={visible}
      onOk={() => onOk && callback()}
      onCancel={() => onCancel && onCancel()}
    >
      <Row>
        <Col xs={24} sm={24} md={24} lg={12} className='border'>
          <Typography.Title
            level={5}
            className='px-6 h-12 flex items-center border-solid border-b'
          >
            Ngày
          </Typography.Title>
          <Calendar
            fullscreen={false}
            disabledDate={date => date.isBefore(new Date(), 'D')}
            className='p-2'
            onChange={date => {
              if (isToday(date.toDate())) {
                return fetchFlashSales(date.toDate())
              }

              fetchFlashSales(new Date(date.toDate().setHours(0, 0, 0, 0)))
            }}
          />
        </Col>
        <Col xs={24} sm={24} md={24} lg={12} className='border'>
          <Typography.Title
            level={5}
            className='px-6 h-12 flex items-center border-solid border-b mb-0'
          >
            Khung giờ
          </Typography.Title>
          <Row className='h-10 bg-[#f6f6f6] pl-6 flex items-center'>
            <Col xs={12} sm={12} md={12} lg={12}>
              <Typography.Text>Khung giờ</Typography.Text>
            </Col>
            <Col xs={12} sm={12} md={12} lg={12}>
              <Typography.Text>Sản Phẩm</Typography.Text>
            </Col>
          </Row>
          <div className='max-h-[310px] overflow-y-auto'>
            {flashSales.map(flashSale => (
              <Row
                key={flashSale.id}
                className='border-solid border-t py-3'
              >
                <Col xs={12} sm={12} md={12} lg={12} className='pl-4'>
                  <Radio.Group
                    onChange={(e) => onSelectFlashSale(e.target.value)}
                    value={selectedFlashSaleId}
                    className='w-full'
                  >
                    <Radio
                      key={flashSale.id}
                      value={flashSale.id}
                      className='w-full'
                    >
                      <span>
                        {formatDate(flashSale.startTime, 'HH:mm:ss')} -{' '}
                        {formatDate(flashSale.endTime, 'HH:mm:ss')}
                      </span>
                    </Radio>
                  </Radio.Group>
                </Col>
                <Col xs={12} sm={12} md={12} lg={12} className='pl-3'>
                  <Typography.Text>
                    Số sản phẩm tham gia {flashSale.maxQuantity}
                  </Typography.Text>
                </Col>
              </Row>
            ))}
          </div>
        </Col>
      </Row>
    </Modal>
  )
}



      
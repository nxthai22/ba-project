import { CalendarOutlined } from '@ant-design/icons'
import { Button, Card, Col, Form, Row, Typography } from 'antd'
import { FlashSaleEntity } from 'services'
import { formatDate } from 'utils'

export interface IProps {
  canEdit: boolean
  selectedFlashSale: FlashSaleEntity
  onShowModalFlashSale: (value: boolean) => void
}

export default function BasicInfo ({ canEdit, selectedFlashSale, onShowModalFlashSale }: IProps) {
  return (
    <Card title={'Thông tin cơ bản'}>
      <Form.Item label={'Khung thời gian'}>
        {canEdit ? (
          <Button
            type={selectedFlashSale ? 'default' : 'primary'}
            icon={<CalendarOutlined />}
            onClick={() => onShowModalFlashSale(true)}
          >
            {selectedFlashSale
              ? `${formatDate(selectedFlashSale.startTime, 'HH:mm dd-MM-yyyy')} - 
                 ${formatDate(selectedFlashSale.endTime, 'HH:mm dd-MM-yyyy')}`
              : `Lựa chọn khung giờ`}
          </Button>
        ) : (
          <>
            {selectedFlashSale &&
              `${formatDate(selectedFlashSale.startTime, 'HH:mm dd-MM-yyyy')} - 
               ${formatDate(selectedFlashSale.endTime, 'HH:mm dd-MM-yyyy')}`}
          </>
        )}
      </Form.Item>
      <Form.Item label={'Tiêu chí sản phẩm'}>
        <Card>
          <Typography.Text>Tất cả tiêu chí sản phẩm</Typography.Text>
          <Row gutter={20} className='mt-3'>
            <Col xs={24} sm={24} md={24} lg={12}>
              <ul className='list-disc leading-7 px-5'>
                <li>
                  Số lượng khuyến mãi: 1~1000
                </li>
                <li>
                  Giá khuyến mãi: Giá sau khuyến mãi là giá thấp nhất trong 7 ngày qua (không tính giá chạy Flash Sale)
                </li>
                <li>
                  Số lượng đơn hàng trong vòng 30 ngày qua: Không giới hạn
                </li>
                <li>
                  Thời gian tham gia chương trình tiếp theo: &gt;= 1 ngày (Cùng một sản phẩm không thể đăng ký Flash Sale trong 1 ngày liên tiếp)
                </li>
              </ul>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12}>
              <ul className='list-disc leading-7'>
                <li>
                  Mức khuyến mãi: 5% ~ 90%
                </li>
                <li>
                  Hàng đặt trước: Không chấp nhận hàng đặt trước
                </li>
                <li>
                  Thời gian chuẩn bị hàng: Không giới hạn ngày
                </li>
              </ul>
            </Col>
          </Row>
        </Card>
      </Form.Item>
    </Card>
  )
}

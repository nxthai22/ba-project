import Icon, { UserOutlined } from '@ant-design/icons'
import { Steps } from 'antd'
import { MdOutlineLocalShipping } from 'react-icons/md'
import { EnumOrderHistoryEntityType, OrderEntity } from 'services'
import { formatDate, getOrderStatusDesc } from 'utils'

export interface IProps {
  order: OrderEntity
}

export function OrderStepHistory ({ order }: IProps) {
  return (
    <Steps
      direction='vertical'
      status='process'
      size='small'
      responsive={true}
      current={order.histories.length - 1}
    >
      {order.histories.map((history, index) =>
        <Steps.Step
          key={`step-${index}`}
          title={
            <>
              {history.type === EnumOrderHistoryEntityType.shipment &&
                <>
                  <Icon
                    component={MdOutlineLocalShipping}
                    className='text-gray-500'
                    style={{ verticalAlign: 1 }}
                  />{' '}
                </>
              }
              {getOrderStatusDesc(history)}
            </>
          }
          description={
            <>
              {formatDate(history?.createdAt)}
              {history?.userId &&
                <>
                  <br />
                  <div className='text-gray-400'>
                    <UserOutlined
                      style={{
                        color: 'rgba(156, 163, 175, var(--tw-text-opacity))',
                        verticalAlign: 2,
                      }}
                    />{' '}
                    {history?.user?.roleId === 1
                      ? order?.tel
                      : history?.user?.fullName}
                  </div>
                </>
              }
            </>
          }
        />
      )}
    </Steps>
  )
}

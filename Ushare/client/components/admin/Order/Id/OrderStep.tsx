import { Card, Col, Steps } from 'antd'
import { EnumOrderHistoryEntityStatus, OrderEntity } from 'services'
import { getOrderStatusDesc } from 'utils'

export interface IProps {
  currentStep: number
  FAILED_STATUS: EnumOrderHistoryEntityStatus[]
  order: OrderEntity
}

export default function OrderStep({
  currentStep,
  FAILED_STATUS,
  order,
}: IProps) {
  return (
    <Card>
      <Col xs={24} sm={24} md={24} lg={24}>
        <Steps
          labelPlacement="vertical"
          responsive={true}
          current={currentStep}
        >
          {FAILED_STATUS.includes(
            order?.histories?.[order?.histories?.length - 1]?.status
          ) ? (
            <>
              {order?.histories.map((history, index) => (
                <Steps.Step
                  key={`history-${index}`}
                  title={getOrderStatusDesc(history.status)}
                />
              ))}
            </>
          ) : (
            <>
              {order.groupId && <Steps.Step title="Chờ ghép đơn" />}
              <Steps.Step title="Chờ xác nhận" />
              <Steps.Step title="Đã xác nhận" />
              <Steps.Step title="Đang giao" />
              <Steps.Step title="Đã giao, chờ đối soát" />
              <Steps.Step title="Đã đối soát" />
            </>
          )}
        </Steps>
      </Col>
    </Card>
  )
}

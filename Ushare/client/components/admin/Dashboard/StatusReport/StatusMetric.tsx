import { formatCurrency } from 'utils'

export interface IProps {
  title: string
  backgroundColor: string
  data: {
    count: number,
    revenue: number,
    countIncrease: number,
    countProduct: number,
    countProductIncrease: number,
  }
}

export default function StatusMetric ({ title, backgroundColor = '#D6D9E5', data }: IProps) {
  return (
    <div className='rounded-md md:rounded-lg overflow-hidden bg-white font-bold'>
      <div style={{ backgroundColor }}>
        <div className='flex items-center py-2 md:py-3 px-3 text-[#333] md:text-base'>
          {title}
        </div>
      </div>
      <ul className='px-3 py-2 md:py-3 mb-0 text-[#888]'>
        <li className='flex justify-between mb-2'>
          <div>
            <span className='font-normal w-20 inline-block'>Đơn hàng</span>
            <span className='text-[#333]'>{data?.count || 0}</span>
          </div>
          {
            data?.countIncrease ? 
              <span className={data.countIncrease > 0 ? 'text-[#2ACC39]' : 'text-red-500'}>
                {data.countIncrease > 0 ? `+${data.countIncrease.toFixed(1)}` : data.countIncrease.toFixed(1)}%
              </span>
            : <span>0%</span>
          }
        </li>
        <li className='flex justify-between mb-2'>
          <div>
            <span className='font-normal w-20 inline-block'>Sản phẩm</span>
            <span className='text-[#333]'>{data?.countProduct || 0}</span>
          </div>
          <span className='text-red-500'>&nbsp;</span>
          {
            data?.countProductIncrease ? 
              <span className={data.countProductIncrease > 0 ? 'text-[#2ACC39]' : 'text-red-500'}>
                {data.countProductIncrease > 0 ? `+${data.countProductIncrease.toFixed(1)}` : data.countProductIncrease.toFixed(1)}%
              </span>
            : <span>0%</span>
          }
        </li>
        <li className='flex justify-between mb-2'>
          <div>
            <span className='font-normal w-20 inline-block'>Tổng tiền</span>
            <span className='text-[#333]'>{formatCurrency(data?.revenue || 0)}</span>
          </div>
          <span>&nbsp;</span>
        </li>
      </ul>
    </div>
  )
}

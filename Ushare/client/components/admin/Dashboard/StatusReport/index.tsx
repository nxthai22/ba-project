import { Col, Row } from 'antd'
import DatePicker from 'components/common/DatePicker'
import { endOfDay, startOfDay } from 'date-fns'
import { OrderStatsStatusParams, useOrderStatsStatus } from 'hooks/order-stats'
import React, { useEffect, useState } from 'react'
import { EnumOrderStatusStatDtoStatus, OrderStatusStatDto } from 'services'
import { getTime } from 'utils/timer'
import StatusMetric from './StatusMetric'
import { calculateStatusTotal, mergeTwoStatus } from './StatusHelper'

const {
  today,
  yesterday,
  thisWeekStart,
  thisWeekEnd,
  lastWeekStart,
  lastWeekEnd,
  thisMonthStart,
  thisMonthEnd,
  lastMonthStart,
  lastMonthEnd,
  thisQuarterStart,
  thisQuarterEnd,
  lastQuarterStart,
  lastQuarterEnd,
  thisYearStart,
  thisYearEnd,
  lastYearStart,
  lastYearEnd,
} = getTime

interface IProps {
  merchantId: number | null
}

export default function StatusReport({ merchantId }: IProps) {
  const { orderStatsStatus, getOrderStatsStatus } = useOrderStatsStatus()
  const [rangePicker, setRangePicker] = useState({
    fromDate: startOfDay(today),
    toDate: endOfDay(today),
  })

  useEffect(() => {
    const params: OrderStatsStatusParams = {
      fromDate: startOfDay(rangePicker.fromDate).toISOString(),
      toDate: endOfDay(rangePicker.toDate).toISOString(),
    }
    if (merchantId) {
      params.merchantId = merchantId
    }
    getOrderStatsStatus(params)
  }, [merchantId, rangePicker])

  const statusTotal = calculateStatusTotal(orderStatsStatus)
  const statusWaitingForGrouping = orderStatsStatus.find(
    (order: OrderStatusStatDto) =>
      order.status === EnumOrderStatusStatDtoStatus.waiting_for_grouping
  )
  const statusPending = orderStatsStatus.find(
    (order: OrderStatusStatDto) =>
      order.status === EnumOrderStatusStatDtoStatus.pending
  )
  const statusCreated = orderStatsStatus.find(
    (order: OrderStatusStatDto) =>
      order.status === EnumOrderStatusStatDtoStatus.created
  )
  const statusConfirmed = orderStatsStatus.find(
    (order: OrderStatusStatDto) =>
      order.status === EnumOrderStatusStatDtoStatus.confirmed
  )
  const statusProcessing = orderStatsStatus.find(
    (order: OrderStatusStatDto) =>
      order.status === EnumOrderStatusStatDtoStatus.processing
  )
  const statusShipping = orderStatsStatus.find(
    (order: OrderStatusStatDto) =>
      order.status === EnumOrderStatusStatDtoStatus.shipping
  )
  const statusShipped = orderStatsStatus.find(
    (order: OrderStatusStatDto) =>
      order.status === EnumOrderStatusStatDtoStatus.shipped
  )
  const statusCompleted = orderStatsStatus.find(
    (order: OrderStatusStatDto) =>
      order.status === EnumOrderStatusStatDtoStatus.completed
  )
  const statusCancelled = orderStatsStatus.find(
    (order: OrderStatusStatDto) =>
      order.status === EnumOrderStatusStatDtoStatus.cancelled
  )
  const statusMerchantCancelled = orderStatsStatus.find(
    (order: OrderStatusStatDto) =>
      order.status === EnumOrderStatusStatDtoStatus.merchant_cancelled
  )
  const statusReturn = orderStatsStatus.find(
    (order: OrderStatusStatDto) =>
      order.status === EnumOrderStatusStatDtoStatus.return
  )

  const mergeCancelAndMerchantCancel = mergeTwoStatus(
    statusCancelled,
    statusMerchantCancelled
  )
  const mergePendingAndCompleted = mergeTwoStatus(statusPending, statusCreated)
  const mergeConfirmAndProcess = mergeTwoStatus(
    statusConfirmed,
    statusProcessing
  )

  return (
    <div>
      <div className="flex flex-col md:flex-row justify-between md:items-center my-5">
        <h5 className="font-bold text-lg mb-0 uppercase">Báo cáo đơn hàng</h5>
        <div className="flex flex-col md:flex-row md:items-center">
          <span className="font-medium text-[#333] mr-2">Ngày tạo đơn</span>
          <DatePicker.RangePicker
            format={'dd-MM-yyyy'}
            allowClear={false}
            ranges={{
              'Hôm nay': [today, today],
              'Hôm qua': [yesterday, yesterday],
              'Tuần này': [thisWeekStart, thisWeekEnd],
              'Tuần trước': [lastWeekStart, lastWeekEnd],
              'Tháng này': [thisMonthStart, thisMonthEnd],
              'Tháng trước': [lastMonthStart, lastMonthEnd],
              'Quý này': [thisQuarterStart, thisQuarterEnd],
              'Quý trước': [lastQuarterStart, lastQuarterEnd],
              'Năm này': [thisYearStart, thisYearEnd],
              'Năm trước': [lastYearStart, lastYearEnd],
            }}
            defaultValue={[rangePicker.fromDate, rangePicker.toDate]}
            onChange={(date: Date[]) =>
              setRangePicker({
                fromDate: startOfDay(date[0]),
                toDate: endOfDay(date[1]),
              })
            }
          />
        </div>
      </div>
      <div className="mb-6">
        <Row gutter={[24, 24]}>
          <Col xs={24} md={12} lg={8} xl={4}>
            <StatusMetric
              title="Phát sinh"
              backgroundColor="#FFC087"
              data={statusTotal}
            />
          </Col>
          <Col xs={24} md={12} lg={8} xl={4}>
            <StatusMetric
              title="Chờ ghép đơn"
              backgroundColor="#E4C1FF"
              data={statusWaitingForGrouping}
            />
          </Col>
          <Col xs={24} md={12} lg={8} xl={4}>
            <StatusMetric
              title="Chờ xác nhận"
              backgroundColor="#7CDBA3"
              data={mergePendingAndCompleted}
            />
          </Col>
          <Col xs={24} md={12} lg={8} xl={4}>
            <StatusMetric
              title="Đã xác nhận"
              backgroundColor="#FFD770"
              data={mergeConfirmAndProcess}
            />
          </Col>
          <Col xs={24} md={12} lg={8} xl={4}>
            <StatusMetric
              title="Đang giao"
              backgroundColor="#D2CFFF"
              data={statusShipping}
            />
          </Col>
          <Col xs={24} md={12} lg={8} xl={4}>
            <StatusMetric
              title="Đã giao, chờ đối soát"
              backgroundColor="#90D8CA"
              data={statusShipped}
            />
          </Col>
          <Col xs={24} md={12} lg={8} xl={4}>
            <StatusMetric
              title="Đã đối soát"
              backgroundColor="#B3E0FF"
              data={statusCompleted}
            />
          </Col>
          <Col xs={24} md={12} lg={8} xl={4}>
            <StatusMetric
              title="Đã hủy"
              backgroundColor="#F1B0BC"
              data={mergeCancelAndMerchantCancel}
            />
          </Col>
          <Col xs={24} md={12} lg={8} xl={4}>
            <StatusMetric
              title="Hoàn hàng"
              backgroundColor="#D6D9E5"
              data={statusReturn}
            />
          </Col>
        </Row>
      </div>
    </div>
  )
}

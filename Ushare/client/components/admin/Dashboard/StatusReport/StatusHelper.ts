import percentage from 'calculate-percentages'
import { OrderStatusStatDto } from 'services'

const statusDto = {
  count: 0,
  prevCount: 0,
  revenue: 0,
  countIncrease: 0,
  countProduct: 0,
  prevCountProduct: 0,
  countProductIncrease: 0,
}

const mergePropertyStatus = (statusTotal, statusWantToMerge) => {
  const totalCount = statusTotal.count + statusWantToMerge.count
  statusTotal.count = totalCount

  const totalRevenue = statusTotal.revenue + statusWantToMerge.revenue
  statusTotal.revenue = totalRevenue

  const totalPrevCount = statusTotal.prevCount + statusWantToMerge.prevCount
  statusTotal.prevCount = totalPrevCount
  
  let percent = percentage.differenceBetween(totalPrevCount, totalCount)
  if (percent === Infinity) percent = 100
  statusTotal.countIncrease = percent

  const totalProduct = statusTotal.countProduct + statusWantToMerge.countProduct
  statusTotal.countProduct = totalProduct

  const totalPrevProduct = statusTotal.prevCountProduct + statusWantToMerge.prevCountProduct
  statusTotal.prevCountProduct = totalPrevProduct

  let percentProduct = percentage.differenceBetween(totalPrevProduct, totalProduct)
  if (percentProduct === Infinity) percentProduct = 100
  statusTotal.countProductIncrease = percentProduct
}

export const calculateStatusTotal = (listOrderStatus: OrderStatusStatDto[]) => {
  const statusTotal = {...statusDto}

  if (listOrderStatus.length) { 
    listOrderStatus.map((order: OrderStatusStatDto) => {
      mergePropertyStatus(statusTotal, order)
    })
  }

  return statusTotal
}

export const mergeTwoStatus = (statusOne: OrderStatusStatDto, statusTwo: OrderStatusStatDto) => {
  let mergeStatus = {...statusDto}

  if (statusOne) mergeStatus = {...statusOne}

  if (statusTwo) mergePropertyStatus(mergeStatus, statusTwo)

  return mergeStatus
}

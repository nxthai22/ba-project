import { ArrowRightIcon } from 'constants/icons'
import { LineChart } from '@opd/g2plot-react'
import { ProductStatsParams, useProductStats } from 'hooks/product-stats'
import { useEffect } from 'react'
import { ProductStatsDto } from 'services'
import { format } from 'date-fns'

interface IProps {
  merchantId: number
  rangePicker: {
    fromDate: Date
    toDate: Date
  }
}

export default function ProductReport({ merchantId, rangePicker }: IProps) {
  const { productStats, getProductStats } = useProductStats()

  useEffect(() => {
    const params: ProductStatsParams = {
      fromDate: rangePicker.fromDate.toISOString(),
      toDate: rangePicker.toDate.toISOString(),
    }
    if (merchantId) params.merchantId = merchantId

    getProductStats(params)
  }, [merchantId, rangePicker])

  const data = []
  productStats.map((chart: ProductStatsDto) => {
    const info1 = {
      time: format(new Date(chart.time), 'dd/MM/yyyy'),
      name: 'Tổng số SP đang hoạt động',
      count: Number(chart.totalProduct),
    }
    const info2 = {
      time: format(new Date(chart.time), 'dd/MM/yyyy'),
      name: 'Phát sinh doanh thu',
      count: Number(chart.countProductHaveRevenue),
    }
    const info3 = {
      time: format(new Date(chart.time), 'dd/MM/yyyy'),
      name: 'Hoàn về',
      count: Number(chart.countProductReturn),
    }
    data.push(info1, info2, info3)
  })

  return (
    <div className="rounded-lg overflow-hidden bg-white">
      <div className="flex justify-between items-center p-3 md:p-5 bg-[#97E0FF]">
        <h5 className="font-bold md:text-lg mb-0 uppercase">Sản phẩm</h5>
        <ArrowRightIcon />
      </div>
      <div className="h-full p-3 md:p-5 mb-8">
        <LineChart
          data={data}
          xField="time"
          yField="count"
          seriesField="name"
          height={250}
          yAxis={{
            label: {
              formatter: (v: any) => `${v}`,
            },
          }}
          legend={{
            layout: 'horizontal',
            position: 'bottom',
            offsetY: 10,
            flipPage: false,
          }}
          color={['#FF643A', '#74CBED', '#0040BB']}
          point={{
            shape: 'circle',
            size: 3,
          }}
        />
      </div>
    </div>
  )
}

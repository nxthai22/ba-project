import { Select, Table } from 'antd'
import {
  ProductStatsTopProductParams,
  useProductStatsTopProduct,
} from 'hooks/product-stats/useProductStatsTopProduct'
import { useEffect, useState } from 'react'
import { TopProductDto } from 'services'
import { formatCurrency } from 'utils'

interface IProps {
  merchantId: number
  rangePicker: {
    fromDate: Date
    toDate: Date
  }
}

export default function TopProductReport({ merchantId, rangePicker }: IProps) {
  const [sort, setSort] = useState('sale')
  const { productStatsTopProduct, getProductStatsTopProduct } =
    useProductStatsTopProduct()

  useEffect(() => {
    const params: ProductStatsTopProductParams = {
      sort,
      fromDate: rangePicker.fromDate.toISOString(),
      toDate: rangePicker.toDate.toISOString(),
    }
    if (merchantId) {
      params.merchantId = merchantId
    }
    getProductStatsTopProduct(params)
  }, [merchantId, rangePicker, sort])

  const columns = [
    {
      title: <span className="font-bold">STT</span>,
      dataIndex: 'key',
      key: 'key',
      width: 50,
    },
    {
      title: <span className="font-bold">Sản phẩm</span>,
      dataIndex: 'name',
      key: 'name',
      width: 400,
    },
    {
      title: <span className="font-bold">Đã bán</span>,
      dataIndex: 'saleQuantity',
      key: 'saleQuantity',
      width: 100,
      render: (data: number) => <span className="font-bold">{data}</span>,
    },
    {
      title: <span className="font-bold">Doanh thu (VNĐ)</span>,
      dataIndex: 'saleRevenue',
      key: 'saleRevenue',
      width: 150,
      render: (data: number) => <span className="font-bold">{data}</span>,
    },
  ]

  const dataSource = productStatsTopProduct.map(
    (product: TopProductDto, index: number) => {
      return {
        key: index + 1,
        name: product.product.name,
        saleQuantity: Number(product.saleQuantity),
        saleRevenue: formatCurrency(product.saleRevenue),
      }
    }
  )

  return (
    <div className="rounded-lg overflow-hidden bg-white p-5">
      <div className="flex flex-col md:flex-row justify-between md:items-center mb-7">
        <h5 className="font-bold md:text-lg mb-2 md:mb-0 uppercase">
          Top sản phẩm
        </h5>
        <div>
          <Select
            defaultValue={sort}
            onChange={(value: string) => setSort(value)}
            className="antd-custom-select-font-bold w-44"
          >
            <Select.Option value="sale">Bán chạy nhất</Select.Option>
            <Select.Option value="revenue">Doanh thu cao nhất</Select.Option>
          </Select>
        </div>
      </div>
      <div>
        <Table
          size="small"
          className="pb-5"
          columns={columns}
          dataSource={dataSource}
          pagination={false}
          scroll={{
            x: true,
          }}
        />
      </div>
    </div>
  )
}

import { ArrowRightIcon } from 'constants/icons'
import { LineChart } from '@opd/g2plot-react'
import {
  useCollaboratorStats,
  CollaboratorStatsParams,
} from 'hooks/collaborator-stats/useCollaboratorStats'
import { useEffect } from 'react'
import { formatCurrency } from 'utils'
import { CollaboratorStatsDto } from 'services'
import { format } from 'date-fns'

interface IProps {
  merchantId: number
  rangePicker: {
    fromDate: Date
    toDate: Date
  }
}

export default function CollaboratorReport({
  merchantId,
  rangePicker,
}: IProps) {
  const { collaboratorStats, getCollaboratorStats } = useCollaboratorStats()
  const { revenueAvg, charts } = collaboratorStats

  useEffect(() => {
    const params: CollaboratorStatsParams = {
      fromDate: rangePicker.fromDate.toISOString(),
      toDate: rangePicker.toDate.toISOString(),
    }
    if (merchantId) params.merchantId = merchantId

    getCollaboratorStats(params)
  }, [merchantId, rangePicker])

  const data = []
  charts.map((chart: CollaboratorStatsDto) => {
    const info1 = {
      time: format(new Date(chart.time), 'dd/MM/yyyy'),
      name: 'Tổng số CTV',
      count: chart.totalCollaborator,
    }
    const info2 = {
      time: format(new Date(chart.time), 'dd/MM/yyyy'),
      name: 'CTV phát sinh doanh thu',
      count: chart.countCollaboratorHaveRevenue,
    }
    data.push(info1, info2)
  })

  return (
    <div className="rounded-lg overflow-hidden bg-white">
      <div className="flex justify-between items-center p-3 md:p-5 bg-[#E4D2FF]">
        <h5 className="font-bold md:text-lg mb-0 uppercase">
          Thống kê cộng tác viên
        </h5>
        <ArrowRightIcon />
      </div>
      <div className="p-3 md:p-5">
        <div className="w-full flex flex-col md:flex-row justify-between">
          {/*<div className='rounded-md p-3 text-center text-white md:font-medium text-sm md:text-base bg-[#8335F3] md:w-[120px] mb-3'>*/}
          {/*  <div>Doanh thu trung bình</div>*/}
          {/*  <div className='border-solid border-b border-white my-2' />*/}
          {/*  <div>{formatCurrency(revenueAvg)}</div>*/}
          {/*</div>*/}
          <div className={'w-full'}>
            <LineChart
              data={data}
              xField="time"
              yField="count"
              seriesField="name"
              height={250}
              yAxis={{
                label: {
                  formatter: (v: any) => `${v}`,
                },
              }}
              legend={{
                layout: 'horizontal',
                position: 'bottom',
                offsetY: 10,
                flipPage: false,
              }}
              color={['#5465FF', '#FFCF23']}
            />
          </div>
        </div>
        <div className={'pt-3'}>
          Doanh thu trung bình{' '}
          <strong className={'pl-4 text-sm'}>
            {formatCurrency(revenueAvg)}
          </strong>
        </div>
      </div>
    </div>
  )
}

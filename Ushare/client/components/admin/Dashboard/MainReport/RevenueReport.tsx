/* eslint-disable no-prototype-builtins */
import { DualAxesChart } from '@opd/g2plot-react'
import { format } from 'date-fns'
import { OrderStatsRevenueParams, useOrderStatsRevenue } from 'hooks/order-stats'
import { useEffect } from 'react'
import { OrderRevenueStatsDto } from 'services'
import { compactNumber, formatCurrency } from 'utils'

interface IProps {
  merchantId: number
  rangePicker: {
    fromDate: Date
    toDate: Date
  }
}

export default function RevenueReport ({ merchantId, rangePicker }: IProps) {
  const {orderStatsRevenue, getOrderStatsRevenue} = useOrderStatsRevenue()

  useEffect(() => {
    const params: OrderStatsRevenueParams = {
      fromDate: rangePicker.fromDate.toISOString(),
      toDate: rangePicker.toDate.toISOString()
    }
    if (merchantId) {
      params.merchantId = merchantId
    }
    getOrderStatsRevenue(params)
  }, [merchantId, rangePicker])

  let totalRevenue = 0
  let totalCount = 0

  const data = orderStatsRevenue.map((order: OrderRevenueStatsDto) => {
    totalRevenue += order.revenue ? Number(order.revenue) : 0
    totalCount += order.count ? Number(order.count) : 0

    return {
      'date': format(new Date(order.time), 'dd/MM/yyyy'),
      'doanh thu': order.revenue ? Number(order.revenue) : 0,
      'đơn hàng': order.count ? Number(order.count) : 0
    }
  })

  return (
    <div className='bg-white rounded-lg p-3 md:p-5'>
      <div className='flex flex-col md:flex-row justify-between mb-5'>
        <h5 className='font-bold text-lg mb-2 md:mb-0 uppercase'>
          Biểu đồ doanh thu bán hàng
        </h5>
        <div className='md:text-base'>
          <div className='flex justify-between'>
            <span className='mr-1'>Tổng doanh thu: </span>
            <span>{formatCurrency(totalRevenue)}</span>
          </div>
          <div className='flex justify-between'>
            <span className='mr-1'>Tổng đơn hàng: </span>
            <span>{totalCount}</span>
          </div>
        </div>
      </div>
      <div>
        <DualAxesChart
          data={[data, data]}
          xField={'date'}
          yField={['doanh thu', 'đơn hàng']}
          yAxis={
            {
              'doanh thu': {
                label: {
                  formatter: (text) => compactNumber(Number(text)),
                },
                min: 0,
                tickCount: 5,
                maxTickCount: 5,
                tickMethod: 'd3-linear'
              },
              'đơn hàng': {
                label: {
                  formatter: (text) => compactNumber(Number(text)),
                },
                min: 0,
                tickCount: 5,
                maxTickCount: 5,
                tickMethod: 'd3-linear'
              },
            }
          }
          tooltip={{
            formatter: (datum) => {
              const name = datum.hasOwnProperty('doanh thu') ? 'doanh thu' : 'đơn hàng'
              const value = datum.hasOwnProperty('doanh thu') ? formatCurrency(datum['doanh thu']) : datum['đơn hàng']
              return {
                name,
                value,
              }
            },
          }}
          geometryOptions={[
            {
              geometry: 'column',
              color: '#5465FF',
              columnWidthRatio: 0.4,
            },
            {
              geometry: 'line',
              smooth: true,
              color: '#08AA58',
            },
          ]}
          legend={{
            layout: 'horizontal',
            position: 'bottom',
            offsetY: 10
          }}
        />
      </div>
    </div>
  )
}

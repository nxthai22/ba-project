import { PieChart } from '@opd/g2plot-react'
import { OrderStatsRevenueParams } from 'hooks/order-stats'
import { useOrderStatsRevenueTopStock } from 'hooks/order-stats/useOrderStatsRevenueTopStock'
import { useEffect } from 'react'
import { OrderRevenueTopStockDto } from 'services'
import { formatNumber } from 'utils'

// const data = [
//   {
//     name: 'Hà Nội',
//     value: 36,
//   },
//   {
//     name: 'Hải Phòng',
//     value: 33,
//   },
//   {
//     name: 'Hưng Yên',
//     value: 10,
//   },
//   {
//     name: 'Thái Bình',
//     value: 4,
//   },
//   {
//     name: 'Lào Cai',
//     value: 4,
//   },
//   {
//     name: 'Hà Giang',
//     value: 3,
//   },
//   {
//     name: 'Phú Thọ',
//     value: 2,
//   },
//   {
//     name: 'Bắc Giang',
//     value: 2,
//   },
//   {
//     name: 'Yên Bái',
//     value: 1,
//   },
//   {
//     name: 'HCM',
//     value: 5,
//   },
//   {
//     name: 'Khác',
//     value: 50,
//   }
// ]

interface IProps {
  merchantId: number
  rangePicker: {
    fromDate: Date
    toDate: Date
  }
}

export default function ProvinceReport ({ merchantId, rangePicker }: IProps) {
  const {orderStatsRevenueTopStock, getOrderStatsRevenueTopStock} = useOrderStatsRevenueTopStock()

  useEffect(() => {
    const params: OrderStatsRevenueParams = {
      fromDate: rangePicker.fromDate.toISOString(),
      toDate: rangePicker.toDate.toISOString()
    }
    if (merchantId) {
      params.merchantId = merchantId
    }
    getOrderStatsRevenueTopStock(params)
  }, [merchantId, rangePicker])

  const data = orderStatsRevenueTopStock.map((order: OrderRevenueTopStockDto) => {
    return {
      name: order.merchantAddress.name,
      value: Number(order.revenue)
    }
  })

  return (
    <div className='bg-white rounded-lg overflow-hidden p-5 mb-4 pb-10'>
      <h5 className='font-bold text-lg mb-5 uppercase'>
        Top 10 tỉnh thành có doanh thu cao nhất
      </h5>
      <div>
      {orderStatsRevenueTopStock.length ?
        <PieChart
          autoFit={true}
          appendPadding={0}
          radius={0.8}
          angleField={'value'}
          colorField={'name'}
          data={data}
          label={{
            content: '{value}',
          }}
          statistic={{
            title: false,
            content: false,
          }}
          interactions={[
            {
              type: 'element-selected',
            },
            {
              type: 'element-active',
            },
          ]}
          tooltip={{
            formatter: (datum) => {
              return {
                name: datum.name,
                value: formatNumber(datum.value),
              }
            },
          }}
          legend={{
            layout: 'horizontal',
            position: 'bottom',
            flipPage: false,
            itemWidth: 150
          }}
        />
      : 
        <div className='flex items-center justify-center'>Chưa có dữ liệu</div>
      }
      </div>
    </div>
  )
}

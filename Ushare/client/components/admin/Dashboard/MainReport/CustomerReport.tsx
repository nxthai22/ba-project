import { ArrowRightIcon } from 'constants/icons'
import { ColumnChart } from '@opd/g2plot-react'
import { formatCurrency } from 'utils'
import { CustomerStatsParams, useCustomerStats } from 'hooks/customer-stats'
import { useEffect } from 'react'
import { format } from 'date-fns'
import { CustomerStatsDto } from 'services'

interface IProps {
  merchantId: number
  rangePicker: {
    fromDate: Date
    toDate: Date
  }
}

export default function CustomerReport({ merchantId, rangePicker }: IProps) {
  const { customerStats, getCustomerStats } = useCustomerStats()
  const { revenueAvg, charts } = customerStats

  useEffect(() => {
    const params: CustomerStatsParams = {
      fromDate: rangePicker.fromDate.toISOString(),
      toDate: rangePicker.toDate.toISOString(),
    }
    if (merchantId) params.merchantId = merchantId

    getCustomerStats(params)
  }, [merchantId, rangePicker])

  const data = []
  charts.map((chart: CustomerStatsDto) => {
    const info1 = {
      time: format(new Date(chart.time), 'dd/MM/yyyy'),
      name: 'Tổng số khách hàng',
      count: Number(chart.totalCustomer),
    }
    const info2 = {
      time: format(new Date(chart.time), 'dd/MM/yyyy'),
      name: 'Số khách hàng cũ',
      count: Number(chart.countOldCustomer),
    }
    data.push(info1, info2)
  })

  return (
    <div className="rounded-lg overflow-hidden bg-white">
      <div className="flex justify-between items-center p-3 md:p-5 bg-[#FFCCBE]">
        <h5 className="font-bold md:text-lg mb-0 uppercase">
          Thống kê khách hàng
        </h5>
        <ArrowRightIcon />
      </div>
      <div className="p-3 md:p-5">
        <div className="w-full flex flex-col md:flex-row justify-between">
          {/*<div className="rounded-md p-3 text-center text-white md:font-medium text-sm md:text-base bg-[#FF6136] md:w-[120px] mb-3">*/}
          {/*  <div>Doanh thu trung bình</div>*/}
          {/*  <div className="border-solid border-b border-white my-2" />*/}
          {/*  <div>{formatCurrency(revenueAvg)}</div>*/}
          {/*</div>*/}
          <div className={'w-full'}>
            <ColumnChart
              data={data}
              isStack={false}
              xField="time"
              yField="count"
              seriesField="name"
              isGroup={true}
              height={250}
              legend={{
                layout: 'horizontal',
                position: 'bottom',
                offsetY: 10,
                flipPage: false,
                marker: {
                  symbol: 'circle',
                },
              }}
              color={['#FF643A', '#991C1C']}
              columnWidthRatio={0.4}
            />
          </div>
        </div>
        <div className={'pt-3'}>
          Doanh thu trung bình{' '}
          <strong className={'pl-4 text-sm'}>
            {formatCurrency(revenueAvg)}
          </strong>
        </div>
      </div>
    </div>
  )
}

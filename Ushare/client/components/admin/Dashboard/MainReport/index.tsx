import { Col, Row } from 'antd'
import DatePicker from 'components/common/DatePicker'
import { getTime } from 'utils/timer'
import RevenueReport from './RevenueReport'
import ProvinceReport from './ProvinceReport'
import CollaboratorReport from './CollaboratorReport'
import CustomerReport from './CustomerReport'
import ProductReport from './ProductReport'
import { useState } from 'react'
import TopPartnerReport from './TopPartnerReport'
import TopProductReport from './TopProductReport'
import { endOfDay, startOfDay } from 'date-fns'

const {
  today,
  yesterday,
  thisWeekStart,
  thisWeekEnd,
  lastWeekStart,
  lastWeekEnd,
  thisMonthStart,
  thisMonthEnd,
  lastMonthStart,
  lastMonthEnd,
  thisQuarterStart,
  thisQuarterEnd,
  lastQuarterStart,
  lastQuarterEnd,
  thisYearStart,
  thisYearEnd,
  lastYearStart,
  lastYearEnd,
} = getTime

interface IProps {
  merchantId: number | null
}

export default function SaleAndProvince({ merchantId }: IProps) {
  const [rangePicker, setRangePicker] = useState({
    fromDate: startOfDay(thisMonthStart),
    toDate: endOfDay(thisMonthEnd),
  })

  return (
    <>
      <div className="flex justify-between items-center my-5">
        <h5 className="font-medium text-lg mb-0">&nbsp;</h5>
        <div className="flex flex-col md:flex-row md:items-center">
          <span className="font-medium text-[#333] mr-2">Ngày hoàn thành</span>
          <DatePicker.RangePicker
            format={'dd-MM-yyyy'}
            allowClear={false}
            ranges={{
              'Hôm nay': [today, today],
              'Hôm qua': [yesterday, yesterday],
              'Tuần này': [thisWeekStart, thisWeekEnd],
              'Tuần trước': [lastWeekStart, lastWeekEnd],
              'Tháng này': [thisMonthStart, thisMonthEnd],
              'Tháng trước': [lastMonthStart, lastMonthEnd],
              'Quý này': [thisQuarterStart, thisQuarterEnd],
              'Quý trước': [lastQuarterStart, lastQuarterEnd],
              'Năm này': [thisYearStart, thisYearEnd],
              'Năm trước': [lastYearStart, lastYearEnd],
            }}
            defaultValue={[rangePicker.fromDate, rangePicker.toDate]}
            onChange={(date: Date[]) =>
              setRangePicker({
                fromDate: startOfDay(date[0]),
                toDate: endOfDay(date[1]),
              })
            }
          />
        </div>
      </div>
      <div className="mb-10">
        <Row gutter={[24, 24]}>
          <Col xs={24} md={24}>
            <RevenueReport merchantId={merchantId} rangePicker={rangePicker} />
          </Col>
          {/* Tạm thời ẩn vì sai dữ liệu
          <Col xs={24} md={6}>
            <ProvinceReport
              merchantId={merchantId}
              rangePicker={rangePicker}
            />
          </Col>
          */}
        </Row>
      </div>
      <div className="mb-10">
        <Row gutter={[24, 24]}>
          <Col xs={24} md={24} lg={8}>
            <CollaboratorReport
              merchantId={merchantId}
              rangePicker={rangePicker}
            />
          </Col>
          <Col xs={24} md={24} lg={8}>
            <CustomerReport merchantId={merchantId} rangePicker={rangePicker} />
          </Col>
          <Col xs={24} md={24} lg={8}>
            <ProductReport merchantId={merchantId} rangePicker={rangePicker} />
          </Col>
        </Row>
      </div>
      <div className="mb-10">
        <Row gutter={[24, 24]}>
          <Col xs={24} md={12}>
            <TopPartnerReport
              merchantId={merchantId}
              rangePicker={rangePicker}
            />
          </Col>
          <Col xs={24} md={12}>
            <TopProductReport
              merchantId={merchantId}
              rangePicker={rangePicker}
            />
          </Col>
        </Row>
      </div>
    </>
  )
}

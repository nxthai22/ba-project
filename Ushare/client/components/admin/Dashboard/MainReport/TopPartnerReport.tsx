import { Select, Table } from 'antd'
import { OrderStatsTopPartnerParams, useOrderStatsTopPartner } from 'hooks/order-stats/useOrderStatsTopPartner'
import { useEffect, useState } from 'react'
import { OrderTopPartnerDto } from 'services'
import { formatCurrency } from 'utils'

interface IProps {
  merchantId: number
  rangePicker: {
    fromDate: Date
    toDate: Date
  }
}

export default function TopPartnerReport ({ merchantId, rangePicker }: IProps) {
  const [sort, setSort] = useState('order')
  const { orderStatsTopPartner, getOrderStatsTopPartner } = useOrderStatsTopPartner()

  const columns = [
    {
      title: <span className='font-bold'>STT</span>,
      dataIndex: 'key',
      key: 'key',
      width: 50
    },
    {
      title: <span className='font-bold'>Tên</span>,
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: <span className='font-bold'>Đơn hàng</span>,
      dataIndex: 'count',
      key: 'count',
      width: 100,
      render: (data: number) => <span className='font-bold'>{data}</span>
    },
    {
      title: <span className='font-bold'>{sort === 'commission' ? 'Hoa hồng (VNĐ)' : 'Doanh thu (VNĐ)'}</span>,
      dataIndex: sort === 'commission' ? 'commission' : 'revenue',
      key: sort === 'commission' ? 'commission' : 'revenue',
      width: 150,
      render: (data: number) => <span className='font-bold'>{data}</span>
    },
  ]

  useEffect(() => {
    const params: OrderStatsTopPartnerParams = {
      sort,
      fromDate: rangePicker.fromDate.toISOString(),
      toDate: rangePicker.toDate.toISOString()
    }
    if (merchantId) {
      params.merchantId = merchantId
    }
    getOrderStatsTopPartner(params)
  }, [merchantId, rangePicker, sort])

  const dataSource = orderStatsTopPartner.map((order: OrderTopPartnerDto, index: number) => {
    return {
      key: index + 1,
      name: order.user.fullName,
      count: Number(order.count),
      commission: formatCurrency(order.commission),
      revenue: formatCurrency(order.revenue)
    }
  })

  return (
    <div className='rounded-lg overflow-hidden bg-white p-5'>
      <div className='flex flex-col md:flex-row justify-between md:items-center mb-7'>
        <h5 className='font-bold md:text-lg mb-2 md:mb-0 uppercase'>
          Top cộng tác viên
        </h5>
        <div>
          <Select
            defaultValue={sort}
            onChange={(value: string) => setSort(value)}
            className='antd-custom-select-font-bold'
          >
            <Select.Option value='order'>
              Đơn hàng nhiều nhất
            </Select.Option>
            <Select.Option value='revenue'>
              Doanh thu cao nhất
            </Select.Option>
            <Select.Option value='commission'>
              Hoa hồng cao nhất
            </Select.Option>
          </Select>
        </div>
      </div>
      <div>
        <Table
          size='small'
          className='pb-5'
          columns={columns}
          dataSource={dataSource}
          pagination={false}
          scroll={{
            x: true
          }}
        />
      </div>
    </div>
  )
}

import { Button, Card, Col, Form, Input, Modal, Row, Space, Table } from 'antd'
import { ColumnsType } from 'antd/es/table'
import { useGetProducts } from 'hooks/products'
import { useEffect, useState } from 'react'
import { useRecoilValue } from 'recoil'
import { authState } from 'recoil/Atoms'
import { ProductEntity } from 'services'
import { getCdnFile } from 'utils'
import { IProductInputs } from './MainTables'

interface IProps {
  visible: boolean
  onOk?: (selectedProducts: IProductInputs[]) => void
  onCancel?: () => void
}

const ModalSelectProducts = ({ visible, onOk, onCancel }: IProps) => {
  const user = useRecoilValue(authState)
  const [form] = Form.useForm()
  const [selectedProducts, setSelectedProducts] = useState<IProductInputs[]>([])
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([])
  const { filterParams, setFilter, products, metadata, loading } =
    useGetProducts()

  const columns: ColumnsType<ProductEntity> = [
    {
      dataIndex: 'name',
      title: 'Tên sản phẩm',
      width: 300,
      render: (name, record) => {
        return (
          <span className="text-sm inline-flex items-center">
            <span className="flex-grow-0 flex-shrink-0 basis-auto mr-2">
              {record?.images && record?.images.length > 0 && (
                <img
                  src={getCdnFile(record?.images?.[0])}
                  width={'50px'}
                  alt={name}
                />
              )}
            </span>
            {name}
          </span>
        )
      },
    },
  ]
  const onSearch = (value: any) => {
    setSelectedRowKeys([])
    setSelectedProducts([])
    const filter = [`merchantId||$eq||${user.merchantId}`]
    if (value.searchText) {
      filter.push(`name||$contL||${value.searchText}`)
    }
    setFilter({
      page: 1,
      filter,
    })
  }
  const onReset = () => {
    form.resetFields()
    setFilter({
      page: 1,
      filter: [`merchantId||$eq||${user.merchantId}`],
    })
    setSelectedRowKeys([])
    setSelectedProducts([])
  }

  const rowSelection = {
    selectedRowKeys,
    preserveSelectedRowKeys: true,
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRowKeys(selectedRowKeys)
      setSelectedProducts(selectedRows)
    },
  }

  useEffect(() => {
    if (visible) {
      setFilter({
        page: 1,
        filter: [`merchantId||$eq||${user.merchantId}`],
      })
    }
  }, [visible])

  return (
    <>
      <Modal
        title="Chọn Sản Phẩm"
        width="960px"
        visible={visible}
        maskClosable={false}
        onOk={() => onOk && onOk(selectedProducts)}
        onCancel={() => onCancel && onCancel()}
      >
        <Form
          form={form}
          autoComplete="off"
          onFinish={onSearch}
          onReset={onReset}
          colon={false}
          initialValues={{ searchText: '' }}
        >
          <Row gutter={[16, 16]}>
            <Col xs={24} sm={24} md={24} lg={12}>
              <Form.Item label="Nhập tên" name="searchText">
                <Input placeholder="Nhập tên sản phẩm" />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} className={'text-right'}>
              <Form.Item>
                <Space>
                  <Button htmlType="button" onClick={onReset}>
                    Đặt lại
                  </Button>
                  <Button type="primary" htmlType="submit">
                    Tìm kiếm
                  </Button>
                </Space>
              </Form.Item>
            </Col>
          </Row>
        </Form>

        {user.merchantId && (
          <Card
            title={`${metadata.total} sản phẩm`}
            extra={`Đã chọn ${selectedRowKeys?.length} sản phẩm`}
          >
            <Table
              rowSelection={rowSelection}
              rowKey="id"
              size="small"
              columns={columns}
              dataSource={products}
              loading={loading}
              pagination={{
                current: filterParams.page,
                pageSize: filterParams.limit,
                total: metadata.total,
                onChange: (page) => setFilter({ page }),
              }}
            />
          </Card>
        )}
      </Modal>
    </>
  )
}

export default ModalSelectProducts

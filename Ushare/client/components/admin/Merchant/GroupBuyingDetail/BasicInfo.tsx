import { yupResolver } from '@hookform/resolvers/yup'
import { Card, Form, Input } from 'antd'
import React, { forwardRef, useEffect, useImperativeHandle } from 'react'
import { Controller, useForm } from 'react-hook-form'
import * as yup from 'yup'
import { isBefore, isFuture } from 'date-fns'
import DatePicker from 'components/common/DatePicker'
import { GroupBuyingCampaignEntity } from 'services'
import _ from 'lodash'

interface Inputs {
  name: string
  rangeTime?: [Date, Date]
}

interface IProps {
  canEdit: boolean
  data: GroupBuyingCampaignEntity
}

const schema = yup.object().shape({
  name: yup.string().trim().required('Nhập tên chương trình'),
  rangeTime: yup
    .array()
    .of(yup.string())
    .typeError('Chưa chọn thời gian')
    .required('Chưa chọn thời gian'),
})

const BasicInfo = ({ canEdit, data }: IProps, ref) => {
  const currentTime = new Date()
  const {
    control,
    setValue,
    getValues,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
  })

  useEffect(() => {
    if (data) {
      setValue('name', data.name)
      setValue('rangeTime', [new Date(data.startTime), new Date(data.endTime)])
    }
  }, [data])

  useImperativeHandle(ref, () => ({
    value: async () => {
      await handleSubmit(() => {}, () => {})()

      if (!isValid) {
        return { name: '', rangeTime: [] }
      }
      return getValues()
    },
  }))

  return (
    <Card title="Thông tin cơ bản">
      <Form labelCol={{ span: 3 }} wrapperCol={{ span: 12 }} colon={false}>
        <Form.Item
          label="Tên chương trình"
          required
          validateStatus={errors.name && 'error'}
          help={errors.name?.message}
        >
          <Controller
            control={control}
            name="name"
            render={({ field }) => (
              <Input
                {...field}
                placeholder="Nhập tên chương trình"
                minLength={1}
                maxLength={100}
                disabled={!canEdit}
              />
            )}
          />
        </Form.Item>
        <Form.Item
          label="Thời gian áp dụng"
          required
          validateStatus={errors.rangeTime && 'error'}
          help={errors?.rangeTime?.message}
        >
          <Controller
            name="rangeTime"
            control={control}
            render={({ field }) => (
              <DatePicker.RangePicker
                {...field}
                showTime
                allowClear={false}
                format="HH:mm DD-MM-YYYY"
                disabled={!canEdit}
                disabledDate={(date) => isBefore(date, currentTime)}
                disabledTime={(date) => ({
                  disabledHours: () => {
                    if (isFuture(date)) return []
                    return _.times(24).filter(
                      (h) => currentTime.getHours() + 1 > h
                    )
                  },
                })}
              />
            )}
          />
        </Form.Item>
      </Form>
    </Card>
  )
}

export default forwardRef(BasicInfo)

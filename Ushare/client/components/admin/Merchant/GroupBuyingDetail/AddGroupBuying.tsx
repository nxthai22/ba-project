import { DeleteOutlined } from '@ant-design/icons'
import { yupResolver } from '@hookform/resolvers/yup'
import { Button, Checkbox, Form, Input, Table, Tooltip } from 'antd'
import { ColumnsType } from 'antd/es/table'
import InputNumber from 'components/common/InputNumber'
import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from 'react'
import { Controller, useFieldArray, useForm } from 'react-hook-form'
import { formatCurrency, getCdnFile } from 'utils'
import * as yup from 'yup'

interface Inputs {
  name: string
  dates: Date[]
  groupBuyingProducts: IProductInputs[]
}

interface IVariantInputs {
  id: number
  name: string
  price: number
  stockNumber: number

  // Id sản phẩm thêm vào chương trình
  productId: number

  // Id biến thể thêm vào chương trình
  productVariantId?: number

  // Gía mua chung
  groupPrice: number

  // Số lượng sản phẩm được dùng cho chương trình
  maxQuantity: number
}

export interface IProductInputs {
  id: number
  price?: number
  name: string
  images: string[]
  stockNumber: number
  variantInputs: IVariantInputs[]
  minMemberNumber: number
  maxQuantityPerUser: number
  timeToTeamUp: number
  groupPrice: number
  maxQuantity: number
  maxGroupPerUser: number
}

const schema = yup.object().shape({
  groupBuyingProducts: yup.array(),
})

interface IProps {
  data: any[]
}

const AddGroupBuying = ({ data }: IProps, ref) => {
  // console.log('AddGroupBuying data', data);
  const {
    control,
    setValue,
    handleSubmit,
    setError,
    clearErrors,
    getValues,
    formState: { errors },
  } = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
  })
  const {
    fields: productFields,
    remove: removeProductField,
    append: appendProductField,
  } = useFieldArray({
    control,
    name: `groupBuyingProducts`,
  })

  const [productViews, setProductViews] = useState<IProductInputs[]>(data)
  const productColumns = [
    {
      title: 'Sản phẩm',
      dataIndex: 'name',
      key: 'name',
      width: 250,
      render: (name, record) => (
        <span className="text-sm inline-flex items-center">
          <span className="flex-grow-0 flex-shrink-0 basis-auto mr-1">
            {record?.images && record?.images.length > 0 && (
              <img
                src={getCdnFile(record?.images?.[0])}
                width={'50px'}
                alt={name}
              />
            )}
          </span>
          <span>
            {name}
          </span>
        </span>
      ),
    },
    { title: 'Giá bán lẻ', width: 100 },
    { title: 'Giá mua chung *', width: 100 },
    {
      title: 'CTV tối thiểu / nhóm *',
      dataIndex: 'minMemberNumber',
      key: 'minMemberNumber',
      width: 100,
      render: (_value, record, index) => {
        return (
          <Controller
            name={`groupBuyingProducts.${index}.minMemberNumber`}
            control={control}
            defaultValue={0}
            render={({ field }) => (
              <InputNumber {...field} min={0} addonAfter="người" />
            )}
          />
        )
      },
    },
    {
      title: 'SP tối đa / CTV',
      dataIndex: 'maxQuantityPerUser',
      key: 'maxQuantityPerUser',
      width: 100,
      render: (_value, record, index) => {
        return (
          <Controller
            name={`groupBuyingProducts.${index}.maxQuantityPerUser`}
            control={control}
            defaultValue={0}
            render={({ field }) => (
              <InputNumber {...field} min={0} addonAfter="sp" />
            )}
          />
        )
      },
    },
    {
      title: 'Thời gian ghép đơn',
      dataIndex: 'timeToTeamUp',
      key: 'timeToTeamUp',
      width: 100,
      render: (_value, record, index) => {
        return (
          <Controller
            name={`groupBuyingProducts.${index}.timeToTeamUp`}
            control={control}
            defaultValue={0}
            render={({ field }) => (
              <InputNumber {...field} min={0} addonAfter="giờ" />
            )}
          />
        )
      },
    },
    {
      title: 'Số nhóm chờ ghép tối đa / CTV',
      dataIndex: 'maxGroupPerUser',
      key: 'maxGroupPerUser',
      width: 150,
      render: (_value, record, index) => {
        return (
          <Controller
            name={`groupBuyingProducts.${index}.maxGroupPerUser`}
            control={control}
            defaultValue={0}
            render={({ field }) => <InputNumber {...field} min={0} />}
          />
        )
      },
    },
    {
      title: () => (
        <>
          <div>Số SP áp dụng</div>
          <div className="text-xs text-gray-400">
            Nếu không nhập, mặc định = SL tồn kho
          </div>
        </>
      ),
      dataIndex: 'maxQuantity',
      key: 'maxQuantity',
      width: 150,
    },
    {
      title: 'SL tồn kho',
      dataIndex: 'stockNumber',
      key: 'stockNumber',
      width: 100,
    },
    {
      title: 'Hành động',
      key: 'actions',
      width: 100,
      render: (_value, record, index) => {
        return (
          <Button
            shape="circle"
            className="mx-1"
            icon={
              <Tooltip title="Xóa">
                <DeleteOutlined />
              </Tooltip>
            }
            onClick={() => {
              const filterProduct = productViews.filter(product => product.id !== record.id)
              setProductViews(filterProduct)
              removeProductField(index)
            }}
          />
        )
      },
    },
  ]
  const expandedRowRender = (product: IProductInputs, parentIndex: number) => {
    const variantColumns: ColumnsType<IVariantInputs> = [
      {
        dataIndex: 'variantName',
        width: 250,
        render: (name, record, index) => {
          return (
            <>
              <Controller
                name={`groupBuyingProducts.${parentIndex}.hasVariants`}
                control={control}
                defaultValue={record.productId !== record.variantId}
                render={({ field }) => (
                  <Checkbox {...field} className="hidden" />
                )}
              />
              <Controller
                name={`groupBuyingProducts.${parentIndex}.productId`}
                control={control}
                defaultValue={record.productId}
                render={({ field }) => <Input {...field} className="hidden" />}
              />
              <Controller
                name={`groupBuyingProducts.${parentIndex}.groupBuyingVariants.${index}.price`}
                control={control}
                defaultValue={record.variantPrice}
                render={({ field }) => <Input {...field} className="hidden" />}
              />
              <Controller
                name={`groupBuyingProducts.${parentIndex}.groupBuyingVariants.${index}.stock`}
                control={control}
                defaultValue={record.stockNumber}
                render={({ field }) => <Input {...field} className="hidden" />}
              />
              <Controller
                name={`groupBuyingProducts.${parentIndex}.groupBuyingVariants.${index}.productVariantId`}
                control={control}
                defaultValue={record.variantId}
                render={({ field }) => <Input {...field} className="hidden" />}
              />
              <div className="flex items-center">
                <Controller
                  name={`groupBuyingProducts.${parentIndex}.groupBuyingVariants.${index}.selected`}
                  control={control}
                  render={({ field }) => (
                    <Checkbox {...field} className="mr-2" />
                  )}
                />
                {name}
              </div>
            </>
          )
        },
      },
      {
        dataIndex: 'variantPrice',
        key: 'variantPrice',
        width: 100,
        render: (price) => formatCurrency(price),
      },
      {
        dataIndex: 'variantGroupPrice',
        key: 'variantGroupPrice',
        width: 100,
        render: (_value, record, index) => {
          return (
            <Controller
              name={`groupBuyingProducts.${parentIndex}.groupBuyingVariants.${index}.groupPrice`}
              control={control}
              defaultValue={0}
              render={({ field }) => <InputNumber {...field} min={0} />}
            />
          )
        },
      },
      {
        title: 'CTV tối thiểu / nhóm',
        width: 100,
      },
      {
        title: 'SP tối đa / CTV',
        width: 100,
      },
      {
        title: 'Thời gian ghép đơn',
        width: 100,
      },
      {
        title: 'Số nhóm chờ ghép tối đa / CTV',
        width: 150,
      },
      {
        title: 'Số SP áp dụng',
        dataIndex: 'maxQuantity',
        key: 'maxQuantity',
        width: 150,
        render: (_value, record, index) => {
          return (
            <Controller
              name={`groupBuyingProducts.${parentIndex}.groupBuyingVariants.${index}.maxQuantity`}
              control={control}
              defaultValue={0}
              render={({ field }) => <InputNumber {...field} min={0} />}
            />
          )
        },
      },
      {
        title: 'SL tồn kho',
        dataIndex: 'stockNumber',
        key: 'stockNumber',
        width: 100,
      },
      {
        title: 'Hành động',
        key: 'actions',
        width: 100,
      },
    ]

    return (
      <Table
        rowKey={(record) => record.variantId}
        showHeader={false}
        columns={variantColumns}
        dataSource={product.variantInputs}
        pagination={false}
      />
    )
  }
  const formatVariants = (product) => {
    const variants = []
    if (product.variants.length) {
      product.variants.map((variant) => {
        variants.push({
          productId: product.id,
          variantId: variant.id,
          variantName: variant.name,
          variantPrice: variant.price,
          variantGroupPrice: 0,
          stockNumber:
            variant.stock?.reduce(
              (total, { quantity }) => total + (quantity ? quantity : 0),
              0
            ) || 0,
        })
      })
    } else {
      // product has no variants
      variants.push({
        productId: product.id,
        variantId: product.id,
        variantName: product.name,
        variantPrice: product.price,
        variantGroupPrice: 0,
        stockNumber:
          product.stock?.reduce(
            (total, { quantity }) => total + (quantity ? quantity : 0),
            0
          ) || 0,
      })
    }

    return variants
  }
  useEffect(() => {
    if (data) {
      const formatProduct = data.map((product) => {
        return {
          id: product.id,
          images: product.images,
          name: product.name,
          price: 0,
          variantInputs: formatVariants(product),
          stockNumber: null,
          timeToTeamUp: 0,
          groupPrice: 0,
          minMemberNumber: 0,
          maxQuantityPerUser: 0,
          maxQuantity: null,
          maxGroupPerUser: 0,
        }
      })
      setProductViews(formatProduct)
    }
  }, [data])

  useImperativeHandle(ref, () => ({
    value: () => {
      return getValues()
    },
  }))

  return (
    <>
      <Form id="form-add-group-buying">
        <Table
          rowKey="id"
          dataSource={productViews}
          columns={productColumns}
          sticky
          size="small"
          expandable={{
            expandedRowRender: expandedRowRender,
          }}
          scroll={{
            x: 1500,
          }}
        />
      </Form>
    </>
  )
}

export default forwardRef(AddGroupBuying)

import { Table } from 'antd'
import { ColumnsType } from 'antd/es/table'
import { useEffect, useState } from 'react'
import { GroupBuyingCampaignEntity, ProductsService } from 'services'
import { formatCurrency, getCdnFile } from 'utils'
import { getTotalStock } from './GroupBuyingHelper'

interface IVariantInputs {
  id: number
  name: string
  price: number
  stockNumber: number
  productId: number
  productVariantId?: number
  groupPrice: number
  maxQuantity: number
}

interface IProductInputs {
  id: number
  productId: number
  price?: number
  name: string
  images: string[]
  stockNumber: number
  variantInputs: IVariantInputs[]
  minMemberNumber: number
  maxQuantityPerUser: number
  timeToTeamUp: number
  groupPrice: number
  maxQuantity: number
  maxGroupPerUser: number
}

interface IProps {
  data: GroupBuyingCampaignEntity
}

const ViewGroupBuying = ({ data }: IProps) => {
  const [productViews, setProductViews] = useState<IProductInputs[]>([])

  const formatVariants = (groupBuyingVariants, product) => {
    const variants = []
    if (groupBuyingVariants.length) {
      product.variants.map((variant) => {
        const existVariant = groupBuyingVariants.find(
          (item) => item.productVariantId === variant.id
        )
        if (existVariant) {
          variants.push({
            variantName: variant.name,
            variantPrice: variant.price,
            variantGroupPrice: existVariant.groupPrice,
            maxQuantity: existVariant.maxQuantity,
            stockNumber:
              variant.stock?.reduce(
                (total, { quantity }) => total + (quantity ? quantity : 0),
                0
              ) || 0,
          })
        }
      })
    }
    return variants
  }

  useEffect(() => {
    if (data) {
      const { groupBuyingProducts } = data
      const productIds = groupBuyingProducts.map((product) => product.productId)
      if (productIds.length > 0)
        ProductsService.getManyBase({
          filter: [`id||$in||${productIds.join()}`],
          join: 'variants',
        }).then((response) => {
          const formatProduct = response.data.map((product) => {
            const buyingProduct = groupBuyingProducts.find(
              (item) => item.productId === product.id
            )
            const { groupBuyingVariants } = buyingProduct
            return {
              id: product.id,
              productId: product.id,
              images: product.images,
              name: product.name,
              price: groupBuyingVariants.length ? null : product.price,
              variantInputs: formatVariants(groupBuyingVariants, product),
              stockNumber: getTotalStock(product.variants),
              timeToTeamUp: buyingProduct.timeToTeamUp,
              groupPrice: buyingProduct.groupPrice,
              minMemberNumber: buyingProduct.minMemberNumber,
              maxQuantityPerUser: buyingProduct.maxQuantityPerUser,
              maxQuantity: groupBuyingVariants.length
                ? null
                : buyingProduct.maxQuantity,
              maxGroupPerUser: buyingProduct.maxGroupPerUser,
            }
          })
          setProductViews(formatProduct)
        })
    }
  }, [data])

  const columns: ColumnsType<IProductInputs> = [
    {
      title: 'Sản phẩm',
      dataIndex: 'name',
      key: 'name',
      render: (name, record) => (
        <a
          href={`/product/edit/${record.id}`}
          target="_blank"
          rel="noreferrer"
          className="text-sm inline-flex items-center"
        >
          <span className="flex-grow-0 flex-shrink-0 basis-auto mr-1">
            {record?.images && record?.images.length > 0 && (
              <img
                src={getCdnFile(record?.images?.[0])}
                width={'50px'}
                alt={name}
              />
            )}
          </span>
          <span>{name}</span>
        </a>
      ),
    },
    {
      title: 'Giá bán lẻ',
      dataIndex: 'price',
      key: 'price',
      width: 150,
      render: (price) => (price ? formatCurrency(price) : null),
    },
    {
      title: 'Giá mua chung',
      dataIndex: 'groupPrice',
      key: 'groupPrice',
      width: 150,
      render: (price) => formatCurrency(price),
    },
    {
      title: 'CTV tối thiểu / nhóm',
      dataIndex: 'minMemberNumber',
      key: 'minMemberNumber',
      width: 170,
    },
    {
      title: 'SP tối đa / CTV',
      dataIndex: 'maxQuantityPerUser',
      key: 'maxQuantityPerUser',
      width: 150,
    },
    {
      title: 'Thời gian ghép đơn',
      dataIndex: 'timeToTeamUp',
      key: 'timeToTeamUp',
      width: 170,
      render: (time) => `${time} giờ`,
    },
    {
      title: 'Số nhóm chờ ghép tối đa / CTV',
      dataIndex: 'maxGroupPerUser',
      key: 'maxGroupPerUser',
      width: 150,
    },
    {
      title: 'Số SP áp dụng',
      dataIndex: 'maxQuantity',
      key: 'maxQuantity',
      width: 150,
    },
    {
      title: 'SL tồn kho',
      dataIndex: 'stockNumber',
      key: 'stockNumber',
      width: 150,
    },
  ]

  const expandedRowRender = (product: IProductInputs) => {
    const columns: ColumnsType<IVariantInputs> = [
      { dataIndex: 'variantName' },
      {
        dataIndex: 'variantPrice',
        key: 'variantPrice',
        width: 150,
        render: (price) => formatCurrency(price),
      },
      {
        dataIndex: 'variantGroupPrice',
        key: 'variantGroupPrice',
        width: 150,
        render: (groupPrice) => formatCurrency(groupPrice),
      },
      {
        title: 'CTV tối thiểu / nhóm',
        dataIndex: 'minMemberNumber',
        key: 'minMemberNumber',
        width: 170,
      },
      {
        title: 'SP tối đa / CTV',
        dataIndex: 'maxQuantityPerUser',
        key: 'maxQuantityPerUser',
        width: 150,
      },
      {
        title: 'Thời gian ghép đơn',
        dataIndex: 'timeToTeamUp',
        key: 'timeToTeamUp',
        width: 170,
      },
      {
        title: 'Số nhóm chờ ghép tối đa / CTV',
        width: 150,
      },
      {
        title: 'Số SP áp dụng',
        dataIndex: 'maxQuantity',
        key: 'maxQuantity',
        width: 150,
      },
      {
        title: 'SL tồn kho',
        dataIndex: 'stockNumber',
        key: 'stockNumber',
        width: 150,
      },
    ]

    return (
      <Table
        rowKey={(record) =>
          record.id
            ? `product-variant-${record.id}`
            : `product-${record.productId}`
        }
        showHeader={false}
        columns={columns}
        dataSource={product.variantInputs}
        pagination={false}
      />
    )
  }

  return (
    <Table
      rowKey={(record) =>
        record.id
          ? `product-variant-${record.id}`
          : `product-${record.productId}`
      }
      dataSource={productViews}
      columns={columns}
      pagination={false}
      sticky={true}
      expandable={{
        expandedRowRender: expandedRowRender,
        defaultExpandAllRows: true,
        rowExpandable: (product) => {
          const buyingProduct = data.groupBuyingProducts.find(
            (item) => item.productId === product.id
          )
          const { groupBuyingVariants } = buyingProduct
          return groupBuyingVariants.length > 0
        },
      }}
      scroll={{
        x: 1200,
      }}
    />
  )
}

export default ViewGroupBuying

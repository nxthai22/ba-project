export const getTotalStock = (productVariants) => {
  let totalStock = 0
  if (Array.isArray(productVariants) && productVariants.length) {
    productVariants.map((variant) => {
      const totalStockEachVariant =
        variant.stock?.reduce(
          (total: number, { quantity }) => total + (quantity ? quantity : 0),
          0
        ) || 0
      totalStock += totalStockEachVariant
    })
  }
  return totalStock
}

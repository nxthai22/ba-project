import { PlusOutlined } from '@ant-design/icons'
import { yupResolver } from '@hookform/resolvers/yup'
import { Button, Card } from 'antd'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import { StatusLabelByTime } from 'enums'
import { uniqBy, has } from 'lodash'
import { useRouter } from 'next/router'
import React, { useEffect, useRef, useState } from 'react'
import { useFieldArray, useForm } from 'react-hook-form'
import { alertError, alertSuccess, getStatusLabelByTime } from 'utils'
import * as yup from 'yup'
import BasicInfo from './BasicInfo'
import { useGetOneGroupBuying } from 'hooks/group-buying/useGetOneGroupBuying'
import { useCreateGroupBuying } from 'hooks/group-buying/useCreateGroupBuying'
import AddGroupBuying from './AddGroupBuying'
import ViewGroupBuying from './ViewGroupBuying'
import ModalSelectProducts from './ModalSelectProduct'

interface Inputs {
  name: string
  dates: Date[]
  groupBuyingProducts: IProductInputs[]
}

interface IVariantInputs {
  id: number
  name: string
  price: number
  stockNumber: number

  // Id sản phẩm thêm vào chương trình
  productId: number

  // Id biến thể thêm vào chương trình
  productVariantId?: number

  // Gía mua chung
  groupPrice: number

  // Số lượng sản phẩm được dùng cho chương trình
  maxQuantity: number
}

interface IProductInputs {
  id: number
  productId: number
  price?: number
  name: string
  images: string[]
  variantInputs: IVariantInputs[]

  // SL CTV tối thiểu/nhóm
  minMemberNumber: number
  // SP tối đa/CTV
  maxQuantityPerUser: number
  // Tgian ghép đơn
  timeToTeamUp: number
}

const schema = yup.object().shape({
  groupBuyingProducts: yup.array().of(
    yup.object().shape({
      variantInputs: yup.array().of(
        yup.object().shape({
          discountValue: yup
            .number()
            .min(0, 'Số tiền giảm giá tham gia khuyến mãi phải lớn hơn 0'),
          qty: yup.number().nullable(),
          // .min(1, 'Số lượng sản phẩm tham gia khuyến mãi phải lớn hơn 0'),
        })
      ),
    })
  ),
})

const GroupBuyingDetail = () => {
  const router = useRouter()
  const { id } = router.query
  const [canEdit, setCanEdit] = useState(false)
  const basicInfoRef = useRef<any>()
  const productInfoRef = useRef<any>()
  const {
    data,
    getOneGroupBuying: getOneGroupBuyingAPI,
    loading,
  } = useGetOneGroupBuying()
  const { createGroupBuying: createGroupBuyingAPI } = useCreateGroupBuying()
  const [showModalSelectProducts, setShowModalSelectProduct] =
    useState<boolean>(false)
  const [listProducts, setListProducts] = useState<IProductInputs[]>([])

  const {
    control,
    setValue,
    handleSubmit,
    setError,
    clearErrors,
    formState: { errors },
  } = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
    defaultValues: {
      groupBuyingProducts: [],
    },
  })
  const {
    fields: productFields,
    remove: removeProductField,
    append: appendProductField,
  } = useFieldArray({
    control,
    name: `groupBuyingProducts`,
  })

  useEffect(() => {
    if (Number(id) && Number(id) !== 0) {
      getOneGroupBuyingAPI({ id: Number(id) })
    }
    if (id === 'create') {
      setCanEdit(true)
    }
  }, [id])

  useEffect(() => {
    if (data) {
      const statusValue = getStatusLabelByTime(data.startTime, data.endTime)
      const inProgress = statusValue === StatusLabelByTime.INPROGRESS
      const isComing = statusValue === StatusLabelByTime.COMMING
      if (inProgress || isComing) setCanEdit(true)
    }
  }, [data])

  const onClickSave = () => {
    const { name, rangeTime } = basicInfoRef.current.value()
    if (!name || !rangeTime.length) {
      alertError('Vui lòng nhập thông tin cơ bản')
      return
    }
    const { groupBuyingProducts } = productInfoRef.current.value()
    if (!groupBuyingProducts.length) {
      alertError('Không có sản phẩm tham gia')
      return
    }
    const listGroupBuyingProducts = []
    const errors = []
    console.log('groupBuyingProducts', groupBuyingProducts)
    groupBuyingProducts.map((product) => {
      const hasGroupBuyingVariants = has(product, 'groupBuyingVariants')
      if (hasGroupBuyingVariants) {
        const selectedVariants = product.groupBuyingVariants.filter(
          (variant) => variant.selected === true
        )
        if (selectedVariants.length) {
          if (!product.minMemberNumber) {
            alertError('Vui lòng nhập CTV tối thiểu / nhóm')
            errors.push('minMemberNumber')
          } else if (!product.maxQuantityPerUser) {
            alertError('Vui lòng nhập số sản phẩm tối đa / CTV')
            errors.push('maxQuantityPerUser')
          } else if (!product.timeToTeamUp) {
            alertError('Vui lòng nhập thời gian ghép đơn')
            errors.push('timeToTeamUp')
          } else {
            listGroupBuyingProducts.push({
              productId: product.productId,
              minMemberNumber: product.minMemberNumber,
              maxQuantityPerUser: product.maxQuantityPerUser,
              timeToTeamUp: product.timeToTeamUp,
              groupPrice: product.hasVariants
                ? 0
                : selectedVariants[0].groupPrice,
              maxQuantity: product.hasVariants
                ? 0
                : selectedVariants[0].maxQuantity,
              groupBuyingVariants: product.hasVariants ? selectedVariants : [],
            })
            errors.length = 0
          }
        }
      }
    })
    if (errors.length) {
      return
    }
    if (!listGroupBuyingProducts.length) {
      alertError('Vui lòng chọn biến thể sản phẩm tham gia')
      return
    }
    const params = {
      name,
      startTime: rangeTime[0].toISOString(),
      endTime: rangeTime[1].toISOString(),
      // name: 'Mua chung by kentrung 2',
      // startTime: '2022-05-20T04:00:05.466Z',
      // endTime: '2022-05-21T05:00:12.466Z',
      groupBuyingProducts: listGroupBuyingProducts,
    }
    // console.log('params', params)
    createGroupBuyingAPI(params).then(() => {
      alertSuccess('Tạo mới mua chung thành công!')
      router.push('/merchant/group-buying')
    })
  }

  return (
    <>
      <BasicInfo canEdit={canEdit} data={data} ref={basicInfoRef} />
      <Card
        title="Sản phẩm tham gia"
        extra={
          canEdit ? (
            <Button
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => setShowModalSelectProduct(true)}
            >
              Thêm sản phẩm
            </Button>
          ) : null
        }
      >
        {id && id !== 'create' && !canEdit ? (
          <ViewGroupBuying data={data} />
        ) : (
          <>
            <AddGroupBuying data={listProducts} ref={productInfoRef} />
            <ModalSelectProducts
              visible={showModalSelectProducts}
              onOk={(selectedProducts) => {
                setListProducts((prevState) => {
                  return uniqBy([...prevState, ...selectedProducts], 'id')
                })
                setShowModalSelectProduct(false)
              }}
              onCancel={() => setShowModalSelectProduct(false)}
            />
          </>
        )}
      </Card>
      {canEdit && (
        <FooterBar
          right={
            <Button
              type="primary"
              // form="form-add-group-buying"
              // key="submit"
              // htmlType="submit"
              disabled={false}
              onClick={onClickSave}
            >
              Lưu
            </Button>
          }
        />
      )}
    </>
  )
}

export default GroupBuyingDetail

import { Button, Table, Tag, Tooltip } from 'antd'
import { StatusLabelByTime } from 'enums'
import React, { useEffect, useState } from 'react'
import { alertSuccess, getStatusLabelByTime } from 'utils'
import { format } from 'date-fns'
import ModalStop from './ModalStop'
import ModalDelete from './ModalDelete'
import Link from 'next/link'
import {
  CopyOutlined,
  DeleteOutlined,
  EditOutlined,
  EyeOutlined,
  PoweroffOutlined,
} from '@ant-design/icons'
import { useGetListGroupBuying } from 'hooks/group-buying'
import { GroupBuyingCampaignEntity } from 'services'
import { useEndGroupBuying } from 'hooks/group-buying/useEndGroupBuying'
import { useDeleteGroupBuying } from 'hooks/group-buying/useDeleteGroupBuying'

interface IProps {
  status?: StatusLabelByTime
}

const DataTables = ({ status }: IProps) => {
  const [endGroupBuying, setEndGroupBuying] =
    useState<GroupBuyingCampaignEntity>()
  const [idDeleteGroupBuying, setIdDeleteGroupBuying] = useState(0)
  const [showModalStop, setShowModalStop] = useState(false)
  const [showModalDelete, setShowModalDelete] = useState(false)
  const { filterParams, setFilter, listGroupBuying, loading, metadata } =
    useGetListGroupBuying()
  const { endGroupBuying: endGroupBuyingAPI } = useEndGroupBuying()
  const { deleteGroupBuying: deleteGroupBuyingAPI } = useDeleteGroupBuying()

  const columns = [
    {
      title: 'Tên chương trình',
      key: 'name',
      dataIndex: 'name',
      render: (name: string, record) => (
        <Link href={`/merchant/group-buying/${record.id}`}>{name}</Link>
      ),
    },
    {
      title: 'Thời gian áp dụng',
      key: 'startTime',
      dataIndex: 'startTime',
      render: (startTime: string, record) => (
        <>
          {format(new Date(startTime), 'HH:mm d/MM/y')} -{' '}
          {format(new Date(record.endTime), 'HH:mm d/MM/y')}
        </>
      ),
    },
    {
      title: 'Số sản phẩm tham gia',
      key: 'productNumber',
      dataIndex: 'productNumber',
      width: 200,
    },
    {
      title: 'Số người đặt hàng',
      key: 'participantNumber',
      dataIndex: 'participantNumber',
      width: 200,
    },
    {
      title: 'Trạng thái',
      key: 'status',
      width: 160,
      render: (record) => {
        const statusValue = getStatusLabelByTime(
          record.startTime,
          record.endTime
        )
        let color = 'default'
        let label = StatusLabelByTime.OVER
        switch (statusValue) {
          case StatusLabelByTime.INPROGRESS:
            color = 'processing'
            label = StatusLabelByTime.INPROGRESS
            break
          case StatusLabelByTime.COMMING:
            color = 'volcano'
            label = StatusLabelByTime.COMMING
            break
          default:
            break
        }
        return <Tag color={color}>{label}</Tag>
      },
    },
    {
      title: 'Hành động',
      key: 'actions',
      width: 200,
      render: (record: GroupBuyingCampaignEntity) => {
        const statusValue = getStatusLabelByTime(
          record.startTime,
          record.endTime
        )
        const isOver = statusValue === StatusLabelByTime.OVER
        const inProgress = statusValue === StatusLabelByTime.INPROGRESS
        const isComming = statusValue === StatusLabelByTime.COMMING
        return (
          <div className="flex">
            <Link href={`/merchant/group-buying/${record.id}`}>
              <Button
                shape="circle"
                className="mx-1"
                icon={
                  <Tooltip title={isOver ? 'Chi tiết' : 'Chỉnh sửa'}>
                    {isOver ? <EyeOutlined /> : <EditOutlined />}
                  </Tooltip>
                }
              />
            </Link>
            {isComming && (
              <Button
                shape="circle"
                className="mx-1"
                icon={
                  <Tooltip title="Xóa">
                    <DeleteOutlined />
                  </Tooltip>
                }
                onClick={() => {
                  setShowModalDelete(true)
                  setIdDeleteGroupBuying(Number(record.id))
                }}
              />
            )}
            {inProgress && (
              <Button
                shape="circle"
                className="mx-1"
                icon={
                  <Tooltip title="Kết thúc">
                    <PoweroffOutlined />
                  </Tooltip>
                }
                onClick={() => {
                  setShowModalStop(true)
                  setEndGroupBuying(record)
                }}
              />
            )}
            {/* Tạm ẩn vì chưa có thời gian làm
            <Link href={`/merchant/group-buying/`}>
              <Button
                shape="circle"
                className="mx-1"
                icon={
                  <Tooltip title="Sao chép">
                    <CopyOutlined />
                  </Tooltip>
                }
              />
            </Link> 
            */}
          </div>
        )
      },
    },
  ]

  const handleEndGroupBuying = () => {
    endGroupBuyingAPI(Number(endGroupBuying.id)).then((data) => {
      if (!data) return

      setShowModalStop(false)
      alertSuccess('Kết thúc chương trình thành công!')
      setFilter({ ...filterParams })
    })
  }
  const handleDeleteGroupBuying = () => {
    deleteGroupBuyingAPI(Number(idDeleteGroupBuying)).then((data) => {
      if (!data) return

      setShowModalDelete(false)
      alertSuccess('Xóa chương trình thành công!')
      setFilter({ ...filterParams })
    })
  }

  useEffect(() => {
    let filter = []
    if (status) {
      const now = new Date()
      switch (status) {
        case StatusLabelByTime.INPROGRESS:
          filter = [
            `startTime||lt||${now.toISOString()}`,
            `endTime||gt||${now.toISOString()}`,
          ]
          break
        case StatusLabelByTime.COMMING:
          filter = [`startTime||gt||${now.toISOString()}`]
          break
        case StatusLabelByTime.OVER:
          filter = [`endTime||lt||${now.toISOString()}`]
          break
      }
    }
    setFilter({
      ...filterParams,
      filter,
    })
  }, [status])

  return (
    <>
      <Table
        rowKey={'id'}
        columns={columns}
        dataSource={listGroupBuying}
        scroll={{ x: 1000 }}
        loading={loading}
        size="small"
        pagination={{
          pageSize: filterParams.limit,
          total: metadata.total,
          pageSizeOptions: [10, 20, 50, 100],
          onChange: (page) => setFilter({ page }),
        }}
      />
      <ModalStop
        visible={showModalStop}
        onChangeVisible={setShowModalStop}
        endGroupBuying={endGroupBuying}
        onEndGroupBuying={handleEndGroupBuying}
      />
      <ModalDelete
        visible={showModalDelete}
        onChangeVisible={setShowModalDelete}
        onDeleteGroupBuying={handleDeleteGroupBuying}
      />
    </>
  )
}

export default DataTables

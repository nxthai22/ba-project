import { Button, Modal } from 'antd'
import { GroupBuyingCampaignEntity } from 'services'

interface IProps {
  visible: boolean
  onChangeVisible: (visible: boolean) => void
  endGroupBuying: GroupBuyingCampaignEntity
  onEndGroupBuying: () => void
}

const ModalStop = ({
  visible,
  onChangeVisible,
  endGroupBuying,
  onEndGroupBuying,
}: IProps) => {
  return (
    <Modal
      title="Kết thúc chương trình mua chung"
      visible={visible}
      onCancel={() => onChangeVisible(false)}
      width={600}
      footer={[
        <Button key="back" onClick={() => onChangeVisible(false)}>
          Hủy
        </Button>,
        <Button
          key="submit"
          type="primary"
          danger
          onClick={onEndGroupBuying}
        >
          Kết thúc
        </Button>,
      ]}
    >
      <div>
        <p>
          Đã có <b>{endGroupBuying?.participantNumber || 0}</b> người đang ghép
          đơn, nếu kết thúc chương trình, các đơn đang ghép sẽ bị hủy.
        </p>
        <p>Bạn có chắc chắn muốn kết thúc chương trình?</p>
      </div>
    </Modal>
  )
}

export default ModalStop

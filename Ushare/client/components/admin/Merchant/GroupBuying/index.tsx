import { Card, Tabs } from 'antd'
import React from 'react'
import DataTables from './DataTables'
import { StatusLabelByTime } from 'enums'

const GroupBuying = () => {
  return (
    <Card>
      <Tabs defaultActiveKey="All">
        <Tabs.TabPane tab="Tất cả" key="All">
          <DataTables />
        </Tabs.TabPane>
        <Tabs.TabPane tab="Đang diễn ra" key={StatusLabelByTime.INPROGRESS}>
          <DataTables status={StatusLabelByTime.INPROGRESS} />
        </Tabs.TabPane>
        <Tabs.TabPane tab="Sắp diễn ra" key={StatusLabelByTime.COMMING}>
          <DataTables status={StatusLabelByTime.COMMING} />
        </Tabs.TabPane>
        <Tabs.TabPane tab="Đã kết thúc" key={StatusLabelByTime.OVER}>
          <DataTables status={StatusLabelByTime.OVER} />
        </Tabs.TabPane>
      </Tabs>
    </Card>
  )
}

export default GroupBuying

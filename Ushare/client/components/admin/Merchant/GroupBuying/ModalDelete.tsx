import { Button, Modal } from 'antd'

interface IProps {
  visible: boolean
  onChangeVisible: (visible: boolean) => void
  onDeleteGroupBuying: () => void
}

const ModalDelete = ({
  visible,
  onChangeVisible,
  onDeleteGroupBuying,
}: IProps) => {
  return (
    <Modal
      title="Xóa chương trình mua chung"
      visible={visible}
      onCancel={() => onChangeVisible(false)}
      width={600}
      footer={[
        <Button key="back" onClick={() => onChangeVisible(false)}>
          Hủy
        </Button>,
        <Button
          key="submit"
          type="primary"
          danger
          onClick={onDeleteGroupBuying}
        >
          Xóa
        </Button>,
      ]}
    >
      <div>
        <p>Bạn có chắc chắn muốn xóa chương trình?</p>
      </div>
    </Modal>
  )
}

export default ModalDelete

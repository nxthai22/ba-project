import { useProvince } from 'hooks/location';
import { Select, SelectProps } from 'antd';
import * as _ from 'lodash';
import React from 'react';

const { Option } = Select;

/* eslint-disable-next-line */
export interface ProvinceSelectProps extends SelectProps {}

function ProvinceSelect(props: ProvinceSelectProps, ref) {
  const { provinces } = useProvince();
  return (
    <Select
      ref={ref}
      placeholder="Chọn Tỉnh/TP"
      showSearch
      {...props}
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      filterOption={(input, option: any) => {
        return option.children?.toLowerCase().indexOf(input.toLowerCase()) >= 0;
      }}
    >
      {_.map(provinces, (province) => (
        <Option key={province.id} value={province.id}>
          {province.name}
        </Option>
      ))}
    </Select>
  );
}

export default React.memo(React.forwardRef(ProvinceSelect));

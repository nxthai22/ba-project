import { useWard } from 'hooks/location';
import { Select, SelectProps } from 'antd';
import * as _ from 'lodash';
import React from 'react';

const { Option } = Select;

/* eslint-disable-next-line */
export interface WardSelectProps extends SelectProps {
  districtId?: number;
}

function WardSelect({districtId, ...props}: WardSelectProps, ref) {
  const { wards } = useWard(districtId);
  return (
    <Select
      ref={ref}
      placeholder="Chọn Phường/Xã"
      disabled={!districtId }
      showSearch
      {...props}
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      filterOption={(input, option: any) => {
        return option.children?.toLowerCase().indexOf(input.toLowerCase()) >= 0;
      }}
    >
      {_.map(wards, (ward) => (
        <Option key={ward.id} value={ward.id}>
          {ward.name}
        </Option>
      ))}
    </Select>
  );
}

export default React.memo(React.forwardRef(WardSelect));

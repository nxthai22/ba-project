import { default as ProvinceSelect } from './province-select/province-select';
import { default as DistrictSelect } from './district-select/district-select';
import { default as WardSelect } from './ward-select/ward-select';
export {
  ProvinceSelect, DistrictSelect, WardSelect
};


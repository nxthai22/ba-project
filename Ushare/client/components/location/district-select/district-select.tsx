import { useDistrict } from 'hooks/location';
import { Select, SelectProps } from 'antd';
import * as _ from 'lodash';
import React from 'react';

const { Option } = Select;

/* eslint-disable-next-line */
export interface DistrictSelectProps extends SelectProps {
  provinceId?: number;
}

function DistrictSelect({provinceId, ...props}: DistrictSelectProps, ref) {
  const { districts } = useDistrict(provinceId);
  return (
    <Select
      ref={ref}
      placeholder="Chọn Quận/Huyện"
      disabled={!provinceId}
      showSearch
      {...props}
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      filterOption={(input, option: any) => {
        return option.children?.toLowerCase().indexOf(input.toLowerCase()) >= 0;
      }}
    >
      {_.map(districts, (district) => (
        <Option key={district.id} value={district.id}>
          {district.name}
        </Option>
      ))}
    </Select>
  );
}

export default React.memo(React.forwardRef(DistrictSelect));

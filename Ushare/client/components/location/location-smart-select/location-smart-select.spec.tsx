import { render } from '@testing-library/react';
import LocationSmartSelect from './location-smart-select';

describe('LocationSmartSelect', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<LocationSmartSelect />);
    expect(baseElement).toBeTruthy();
  });
});

import { Col, Row } from 'antd';
import { useState } from 'react';
import DistrictSelect from '../district-select/district-select';
import ProvinceSelect from '../province-select/province-select';
import WardSelect from '../ward-select/ward-select';

interface ILocation {
  provinceId?: number;
  districtId?: number;
  wardId?: number;
}

/* eslint-disable-next-line */
export interface LocationSmartSelectProps {
  value?: ILocation;
  onChange?: (value?: ILocation) => void;
}

// REVIEW: Chưa hỗ trợ chỉ truyền 1 trong 3 giá trị
// province, district, ward thì tìm hiển thị ra các giá trị còn lại

export function LocationSmartSelect(props: LocationSmartSelectProps) {
  const [location, setLocation] = useState(props.value);

  const handleProvinceChange = (provinceId: number) => {
    handleChange({ provinceId });
  };

  const handleDistrictChange = (districtId: number) => {
    handleChange({ provinceId: location.provinceId, districtId });
  };

  const handleWardChange = (wardId: number) => {
    handleChange({ ...location, wardId });
  };

  const handleChange = (location) => {
    setLocation(location);
    props.onChange(location);
  };

  return (
    <Row gutter={[7, 7]}>
      <Col className="gutter-row" span={8}>
        <ProvinceSelect
          value={location.provinceId}
          onChange={handleProvinceChange}
        />
      </Col>

      <Col className="gutter-row" span={8}>
        <DistrictSelect
          provinceId={location.provinceId}
          value={location.districtId}
          onChange={handleDistrictChange}
        />
      </Col>

      <Col className="gutter-row" span={8}>
        <WardSelect
          districtId={location.districtId}
          value={location.wardId}
          onChange={handleWardChange}
        />
      </Col>
    </Row>
  );
}

export default LocationSmartSelect;

/** Generate by swagger-axios-codegen */
// @ts-nocheck
/* eslint-disable */

import { IRequestOptions, IRequestConfig, getConfigs, axios } from './serviceOptions';
export const basePath = '';

export interface IList<T> extends Array<T> {}
export interface List<T> extends Array<T> {}
export interface IDictionary<TValue> {
  [key: string]: TValue;
}
export interface Dictionary<TValue> extends IDictionary<TValue> {}

export interface IListResult<T> {
  items?: T[];
}

export class ListResultDto<T> implements IListResult<T> {
  items?: T[];
}

export interface IPagedResult<T> extends IListResult<T> {
  totalCount?: number;
  items?: T[];
}

export class PagedResultDto<T = any> implements IPagedResult<T> {
  totalCount?: number;
  items?: T[];
}

// customer definition
// empty

export class UsersService {
  /**
   * Thêm Thành viên
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: UserEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Thành viên
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyUserEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa thành viên
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: UserEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Thành viên
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách top hoa hồng từ doanh thu F1
   */
  static userControllerTopBonus(
    params: {
      /** Ngày bắt đầu */
      fromDate?: string;
      /** Ngày kết thúc */
      toDate?: string;
      /** Số lượng record/trang */
      limit: number;
      /** Trang */
      page: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserBonus> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users/top-bonus';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fromDate: params['fromDate'],
        toDate: params['toDate'],
        limit: params['limit'],
        page: params['page']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Firebase Messaging token
   */
  static userControllerAddDeviceToken(
    params: {
      /** requestBody */
      body?: AddDeviceTokenDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DeviceTokenEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users/device-token';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thay đổi mật khẩu
   */
  static userControllerChangePassword(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: ChangePasswordDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users/change-password/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Cập nhật avatar
   */
  static userControllerUpdateAvatar(
    params: {
      /** requestBody */
      body?: UpdateAvatarDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users/update-avatar';

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Cập nhật thông tin thanh toán
   */
  static userControllerUpdatePaymentInfo(
    params: {
      /** requestBody */
      body?: UpdatePaymentInfoDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users/update-payment-info';

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ConfigService {
  /**
   * Tạo cấu hình
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: ConfigDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ConfigEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/config';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Cấu hình
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyConfigEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/config';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa cấu hình
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: ConfigDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ConfigEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/config/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Cấu hình
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ConfigEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/config/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class MerchantsService {
  /**
   * Thêm Đối tác
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: CreateMerchantDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MerchantEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/merchants';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Đối tác
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyMerchantEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/merchants';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Đối tác
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: CreateMerchantDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MerchantEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/merchants/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Đối tác
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MerchantEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/merchants/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Đối tác
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MerchantEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/merchants/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class AuthService {
  /**
   * Đăng nhập bằng Email/Password
   */
  static authControllerLogin(
    params: {
      /** requestBody */
      body?: LoginDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<LoginResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/auth/login';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Đăng ký tài khoản
   */
  static authControllerRegister(
    params: {
      /** requestBody */
      body?: RegisterDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/auth/register';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xác thực tài khoản
   */
  static authControllerVerifyAccount(
    params: {
      /** requestBody */
      body?: VerifyOtpDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<LoginResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/auth/verify-account';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Gửi OTP
   */
  static authControllerSendOtpForgotPassword(
    params: {
      /** requestBody */
      body?: SendOtpForgotPasswordDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/auth/send-otp-forgot-password';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xác thực mã OTP đổi mật khẩu
   */
  static authControllerResetPassword(
    params: {
      /** requestBody */
      body?: ResetPasswordDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/auth/reset-password';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class WalletsService {
  /**
   * Nạp tiền vào ví
   */
  static walletControllerDeposit(
    params: {
      /** requestBody */
      body?: DepositWalletDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserTransactionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/wallets/deposit';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Rút tiền từ ví
   */
  static walletControllerWithdraw(
    params: {
      /** requestBody */
      body?: WithdrawWalletDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserTransactionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/wallets/withdraw';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Cập nhật giao dịch
   */
  static walletControllerUpdateTransactionStatus(
    params: {
      /** requestBody */
      body?: UpdateTransactionDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserTransactionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/wallets/transaction/update-status';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Ví
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserWalletEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/wallets/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Ví
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: UserWalletEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserWalletEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/wallets/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Ví
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyUserWalletEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/wallets';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Ví
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: UserWalletEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserWalletEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/wallets';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class WardsService {
  /**
   * Chi tiết Xã/phường
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<WardEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/wards/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Xã/phường
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: WardEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<WardEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/wards/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Xã/phường
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyWardEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/wards';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Xã/phường
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: WardEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<WardEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/wards';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class DistrictsService {
  /**
   * Chi tiết Quận/huyện
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DistrictEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/districts/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Quận/huyện
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: DistrictEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DistrictEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/districts/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Quận/huyện
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyDistrictEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/districts';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Quận/huyện
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: DistrictEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DistrictEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/districts';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class OrdersService {
  /**
   * Chi tiết Đơn hàng
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrderEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/orders/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Đơn hàng
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: UpdateOrderDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrderEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/orders/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Tính phí vận chuyển
   */
  static orderControllerCalculateFee(
    params: {
      /** requestBody */
      body?: ShipmentRequestDto[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ShippingFeeResponseDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/orders/fee';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Tạo đơn hàng Vận chuyển với đối tác
   */
  static orderControllerCreateShipment(
    params: {
      /** requestBody */
      body?: ShipmentRequestDto[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrderEntity[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/orders/create-shipment';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thay đổi trạng thái đơn hàng
   */
  static orderControllerChangeStatus(
    params: {
      /** ID của đơn hàng */
      id: number;
      /** requestBody */
      body?: ChangeStatusDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ChangeStatusDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/orders/change-status/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Đơn hàng
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: CreateOrderDto[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrderEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/orders';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Đơn hàng
   */
  static getManyBase(
    params: {
      /** Trạng thái vận đơn */
      shippingStatus?: string;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyOrderEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/orders';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        shippingStatus: params['shippingStatus'],
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Lấy số lượng đơn hàng theo trạng thái
   */
  static orderControllerGetStatusCount(options: IRequestOptions = {}): Promise<StatusCountResponseDto[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/orders/status-count';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Tìm kiếm nhanh đơn hàng/doanh thu theo CTV
   */
  static orderControllerFilterOrder(
    params: {
      /** requestBody */
      body?: OrderFilterDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<SearchResult> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/orders/filter-order';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thay đổi hình thức thanh toán đơn hàng
   */
  static orderControllerChangePaymentType(
    params: {
      /** requestBody */
      body?: ChangePaymentTypeDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrderEntity[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/orders/change-payment-type';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Kế toán xác nhận tiền đặt cọc
   */
  static orderControllerConfirmDeposit(
    params: {
      /** requestBody */
      body?: ConfirmDepositDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrderEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/orders/confirm-deposit';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Cập nhật ghi chú đơn hàng
   */
  static orderControllerUpdateNote(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: CreateOrderDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrderEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/orders/update-note/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Cập nhật trạng thái vận chuyển đơn hàng
   */
  static orderControllerUpdateShippingStatus(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: ChangeShippingStatusDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrderEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/orders/update-shipping-status/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ShippingService {
  /**
   *
   */
  static shippingControllerUpdateGhtkShipment(
    params: {
      /** requestBody */
      body?: GhtkWebhookDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/shipping/ghtk/update-shipment';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static shippingControllerUpdateGhnShipment(
    params: {
      /** requestBody */
      body?: GhnWebhookDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/shipping/ghn/update-shipment';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static shippingControllerUpdateVtpShipment(
    params: {
      /** requestBody */
      body?: VtpWebhookDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/shipping/vtp/update-shipment';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ProductsService {
  /**
   * Danh sách Sản phẩm
   */
  static getManyBase(
    params: {
      /**  */
      type?: string;
      /**  */
      provinceId?: number;
      /**  */
      attributeFilters?: any | null[];
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyProductEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/products';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        type: params['type'],
        provinceId: params['provinceId'],
        attributeFilters: params['attributeFilters'],
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm sản phẩm và biến thể
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: CreateProductDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/products';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Sản phẩm
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DetailProudct> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/products/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa sản phẩm và biến thể
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: CreateProductDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/products/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xóa sản phẩm
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/products/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Lấy danh sách sản phẩm theo top tìm kiếm/Dành riêng cho bạn (Top tìm kiếm: keywords = [])
   */
  static productControllerRecommend(
    params: {
      /** Trang */
      page: number;
      /** Số bản ghi trên 1 trang */
      limit: number;
      /** Danh sách từ khóa tìm kiếm */
      keywords: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductEntity[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/products/recommend';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { page: params['page'], limit: params['limit'], keywords: params['keywords'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Lấy danh sách sản phẩm cùng danh mục/cùng merchant
   */
  static productControllerGetOrther(
    params: {
      /** Trang */
      page: number;
      /** Số bản ghi trên 1 trang */
      limit: number;
      /** Ký tự tìm kiếm */
      textSearch?: string;
      /** Loại tìm kiếm */
      sortType?: string;
      /** Id khu vực tìm kiếm */
      provinceId?: number;
      /** Id danh mục */
      categoryId?: number;
      /** Id NCC */
      merchantId?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<SearchResult> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/products/orther';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        page: params['page'],
        limit: params['limit'],
        textSearch: params['textSearch'],
        sortType: params['sortType'],
        provinceId: params['provinceId'],
        categoryId: params['categoryId'],
        merchantId: params['merchantId']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Tìm kiếm danh sách sản phẩm (Tên, tag, khu vực, Danh mục): bán chạy, giá thấp nhất, giá cao nhất, hoa hồng
   */
  static productControllerSearchProduct(
    params: {
      /** requestBody */
      body?: ProductFilterDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<SearchResult> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/products/searchProduct';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ProductCategoriesService {
  /**
   * Thêm Danh mục sản phẩm
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: CreateProductCategoryDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductCategoryEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-categories';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Danh mục sản phẩm
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyProductCategoryEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-categories';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Danh mục sản phẩm
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: CreateProductCategoryDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductCategoryEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-categories/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Danh mục sản phẩm
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductCategoryEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-categories/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Danh mục sản phẩm
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductCategoryEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-categories/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class FlashSaleService {
  /**
   * Chi tiết Flash Sale
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FlashSaleEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/flash-sale/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Flash Sale
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: UpdateFlashSaleDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FlashSaleEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/flash-sale/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Flash Sale
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: CreateFlashSaleDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FlashSaleEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/flash-sale';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Flash Sale
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyFlashSaleEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/flash-sale';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Lấy danh sách flash sale theo merchant
   */
  static flashSaleControllerGetFlashSaleMerchant(
    params: {
      /** Tìm kiếm từ ngày */
      fromDate?: string;
      /** Tìm kiếm đến ngày */
      toDate?: string;
      /** Số lượng record/trang */
      limit: number;
      /** Trang */
      page: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FlashSaleMerchant> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/flash-sale/get-flash-sale-by-merchant';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fromDate: params['fromDate'],
        toDate: params['toDate'],
        limit: params['limit'],
        page: params['page']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Lấy danh sách sản phẩm trong chương trình Flash sale theo NCC
   */
  static flashSaleControllerGetProductInFlashSaleByMerchantId(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FlashSaleEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/flash-sale/get-product-flash-sales-by-merchant-id/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm sản phẩm vào chương trình theo từng đối tác
   */
  static flashSaleControllerAddProductToFlashSale(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: FlashSaleDetailDto[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FlashSaleEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/flash-sale/add-product-flash-sales/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Đối tác chỉnh sửa sản phẩm
   */
  static flashSaleControllerUpdateProductInFlashSaleByMerchantId(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: FlashSaleDetailDto[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FlashSaleEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/flash-sale/update-product-flash-sale/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Lấy danh sách sản phẩm flash sale tại thời điểm hiện tại
   */
  static flashSaleControllerGetNow(
    params: {
      /** ID Tỉnh thành */
      provinceId?: number;
      /** ID chuyên mục */
      productCategoryId?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<FlashSaleEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/flash-sale/now';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { provinceId: params['provinceId'], productCategoryId: params['productCategoryId'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class RevenueService {
  /**
   * Hoa hồng
   */
  static revenueControllerCalculateRevenue(
    params: {
      /** requestBody */
      body?: FilterRevenueWithPaging;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RevenueResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/revenue/summary';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Hoa hồng theo khoảng ngày
   */
  static revenueControllerCaculateRevenueStatistic(
    params: {
      /** requestBody */
      body?: FilterRevenueWithPaging;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RevenueStatisticEntity[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/revenue/revenue-month-report';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Doanh thu CTV
   */
  static revenueControllerCalculateRevenueStatisticDetail(
    params: {
      /** Ký tự tìm kiếm */
      textSearch?: string;
      /** Ngày bắt đầu */
      fromDate: string;
      /** Ngày kết thúc */
      toDate: string;
      /** Id đối tác */
      merchantId?: number;
      /** Trường */
      field?: string;
      /** Id người dùng */
      userId?: number;
      /** Số lượng record/trang */
      limit?: number;
      /** Trang */
      page?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RevenueStatisticResponses> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/revenue/revenue-statistic-detail-report';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        textSearch: params['textSearch'],
        fromDate: params['fromDate'],
        toDate: params['toDate'],
        merchantId: params['merchantId'],
        field: params['field'],
        userId: params['userId'],
        limit: params['limit'],
        page: params['page']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Hoa hồng thưởng giới thiệu
   */
  static revenueControllerCalculateReferralCommissionReport(
    params: {
      /** Ký tự tìm kiếm */
      textSearch?: string;
      /** Ngày bắt đầu */
      fromDate: string;
      /** Ngày kết thúc */
      toDate: string;
      /** Id đối tác */
      merchantId?: number;
      /** Trường */
      field?: string;
      /** Id người dùng */
      userId?: number;
      /** Số lượng record/trang */
      limit?: number;
      /** Trang */
      page?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RevenueReferralStatisticResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/revenue/referral-commission-report';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        textSearch: params['textSearch'],
        fromDate: params['fromDate'],
        toDate: params['toDate'],
        merchantId: params['merchantId'],
        field: params['field'],
        userId: params['userId'],
        limit: params['limit'],
        page: params['page']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Hoa hồng thưởng giới thiệu
   */
  static revenueControllerDetailReferralCommissionReport(
    params: {
      /** Ký tự tìm kiếm */
      textSearch?: string;
      /** Ngày bắt đầu */
      fromDate: string;
      /** Ngày kết thúc */
      toDate: string;
      /** Id đối tác */
      merchantId?: number;
      /** Trường */
      field?: string;
      /** Id người dùng */
      userId?: number;
      /** Số lượng record/trang */
      limit?: number;
      /** Trang */
      page?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DetailRefrralStatistic[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/revenue/detail-referral-commission';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        textSearch: params['textSearch'],
        fromDate: params['fromDate'],
        toDate: params['toDate'],
        merchantId: params['merchantId'],
        field: params['field'],
        userId: params['userId'],
        limit: params['limit'],
        page: params['page']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Top Hoa hồng theo sản phẩm + hoa hồng theo doanh thu
   */
  static revenueControllerGetTopUserCommissionReport(
    params: {
      /** requestBody */
      body?: FilterRevenueWithPaging;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RevenueStatisticResponse[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/revenue/top-commission';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Top Hoa hồng theo sản phẩm + hoa hồng theo doanh thu (Mới)
   */
  static revenueControllerGetTopUserCommissionReportNew(
    params: {
      /** requestBody */
      body?: FilterRevenueWithPaging;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<TopUserCommissionResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/revenue/top-commission-new';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thống kê Hoa hồng 7 ngày gần nhất
   */
  static revenueControllerCalculateCommission(
    params: {
      /** Ký tự tìm kiếm */
      textSearch?: string;
      /** Ngày bắt đầu */
      fromDate: string;
      /** Ngày kết thúc */
      toDate: string;
      /** Id đối tác */
      merchantId?: number;
      /** Trường */
      field?: string;
      /** Id người dùng */
      userId?: number;
      /** Số lượng record/trang */
      limit?: number;
      /** Trang */
      page?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/revenue/commission';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        textSearch: params['textSearch'],
        fromDate: params['fromDate'],
        toDate: params['toDate'],
        merchantId: params['merchantId'],
        field: params['field'],
        userId: params['userId'],
        limit: params['limit'],
        page: params['page']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Biểu đồ thống kê Hoa hồng
   */
  static revenueControllerCalculateRevenueChart(
    params: {
      /** Ký tự tìm kiếm */
      textSearch?: string;
      /** Ngày bắt đầu */
      fromDate: string;
      /** Ngày kết thúc */
      toDate: string;
      /** Id đối tác */
      merchantId?: number;
      /** Trường */
      field?: string;
      /** Id người dùng */
      userId?: number;
      /** Số lượng record/trang */
      limit?: number;
      /** Trang */
      page?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/revenue/revenueChart';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        textSearch: params['textSearch'],
        fromDate: params['fromDate'],
        toDate: params['toDate'],
        merchantId: params['merchantId'],
        field: params['field'],
        userId: params['userId'],
        limit: params['limit'],
        page: params['page']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Top 10 sản phẩm bán chạy
   */
  static revenueControllerGetTopProductSale(
    params: {
      /** requestBody */
      body?: FilterRevenueWithPaging;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/revenue/top-sale';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Tỉ trọng Doanh thu hàng hoá
   */
  static revenueControllerGetTopProductRevenueRate(
    params: {
      /** Ký tự tìm kiếm */
      textSearch?: string;
      /** Ngày bắt đầu */
      fromDate: string;
      /** Ngày kết thúc */
      toDate: string;
      /** Id đối tác */
      merchantId?: number;
      /** Trường */
      field?: string;
      /** Id người dùng */
      userId?: number;
      /** Số lượng record/trang */
      limit?: number;
      /** Trang */
      page?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/revenue/top-product-revenue-rate';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        textSearch: params['textSearch'],
        fromDate: params['fromDate'],
        toDate: params['toDate'],
        merchantId: params['merchantId'],
        field: params['field'],
        userId: params['userId'],
        limit: params['limit'],
        page: params['page']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thống kê Hoa hồng theo User
   */
  static revenueControllerRevenueByUser(
    params: {
      /** Tháng bắt đầu thực hiện tìm kiếm */
      fromDate: string;
      /** Tháng kết thúc thực hiện tìm kiếm */
      toDate: string;
      /** Loại tìm kiếm */
      fillterType: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RevenueStatisticByUserResponse[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/revenue/revenue-by-user';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fromDate: params['fromDate'], toDate: params['toDate'], fillterType: params['fillterType'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class VouchersService {
  /**
   * Thêm Mã giảm giá
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: CreateVoucherDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<VoucherEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/vouchers';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Mã giảm giá
   */
  static getManyBase(
    params: {
      /**  */
      productIds: string;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyVoucherEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/vouchers';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        productIds: params['productIds'],
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Mã giảm giá
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: CreateVoucherDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<VoucherEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/vouchers/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Mã giảm giá
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<VoucherEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/vouchers/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Mã giảm giá
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<VoucherEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/vouchers/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách mã giảm giá cho khách vãng lai/CTV chưa đăng nhập
   */
  static voucherControllerGetManyVoucherForGuest(
    params: {
      /** Id NCC */
      merchantId?: number;
      /** Danh sách Id sản phẩm */
      productIds?: any | null[];
      /**  */
      page?: number;
      /**  */
      limit?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<VoucherEntityResponse> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/vouchers/voucher-for-guest';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        merchantId: params['merchantId'],
        productIds: params['productIds'],
        page: params['page'],
        limit: params['limit']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Lấy chi tiết Voucher theo mã
   */
  static voucherControllerGetOneByCode(
    params: {
      /**  */
      code: string;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<VoucherEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/vouchers/byCode/{code}';
      url = url.replace('{code}', params['code'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách mã giảm giá đã lưu
   */
  static voucherControllerGetSaved(options: IRequestOptions = {}): Promise<VoucherHistoryEntity[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/vouchers/saved';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Tính giảm giá cho đơn hàng
   */
  static voucherControllerCalculateDiscountOrder(
    params: {
      /** requestBody */
      body?: CreateOrderDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<number> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/vouchers/calculate-discount';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Lưu mã giảm giá
   */
  static voucherControllerPostSave(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<VoucherHistoryEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/vouchers/save/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class MerchantAddressesService {
  /**
   * Thêm Kho hàng
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: CreateMerchantAddressDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MerchantAddressEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/merchant-addresses';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Kho hàng
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyMerchantAddressEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/merchant-addresses';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Kho hàng
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: CreateMerchantAddressDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MerchantAddressEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/merchant-addresses/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Kho hàng
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<MerchantAddressEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/merchant-addresses/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class CartProductService {
  /**
   * Thêm Sản phẩm trong giỏ hàng
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: CreateUpdateCartProductDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CartProductEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/cart-product';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Sản phẩm trong giỏ hàng
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyCartProductEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/cart-product';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Sản phẩm trong giỏ hàng
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: CreateUpdateCartProductDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CartProductEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/cart-product/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Sản phẩm trong giỏ hàng
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CartProductEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/cart-product/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Sản phẩm trong giỏ hàng
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CartProductEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/cart-product/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Cập nhật nhiều sản phẩm trong giỏ hàng
   */
  static cartProductControllerUpdateMany(
    params: {
      /** requestBody */
      body?: CreateUpdateCartProductDto[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CartProductEntity[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/cart-product/bulk';

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xóa nhiều sản phẩm trong giỏ hàng
   */
  static cartProductControllerDeleteMany(
    params: {
      /** Danh sách id giỏ hàng */
      ids: any | null[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/cart-product/deletes';

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);
      configs.params = { ids: params['ids'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class PermissionsService {
  /**
   * Chi tiết Permissions
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PermissionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/permissions/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Permissions
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: PermissionEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PermissionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/permissions/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Permissions
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyPermissionEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/permissions';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Permissions
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: PermissionEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PermissionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/permissions';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ProductCategoryAttributesService {
  /**
   * Chi tiết Thuộc tính Sản phẩm
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductCategoryAttributeEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-category-attributes/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Thuộc tính Sản phẩm
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: ProductCategoryAttributeEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductCategoryAttributeEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-category-attributes/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Thuộc tính Sản phẩm
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyProductCategoryAttributeEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-category-attributes';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Thuộc tính Sản phẩm
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: ProductCategoryAttributeEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductCategoryAttributeEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-category-attributes';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ProvincesService {
  /**
   * Chi tiết Tỉnh/thành
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProvinceEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/provinces/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Tỉnh/thành
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: ProvinceEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProvinceEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/provinces/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Tỉnh/thành
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyProvinceEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/provinces';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Tỉnh/thành
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: ProvinceEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProvinceEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/provinces';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class RolesService {
  /**
   * Thêm Role
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: RoleEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RoleEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/roles';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Role
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyRoleEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/roles';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Role
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: RoleEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RoleEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/roles/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Role
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RoleEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/roles/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách chức năng
   */
  static roleControllerRoutes(options: IRequestOptions = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/roles/routes';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Cập nhật nhiều nhóm Quyền
   */
  static roleControllerUpdateMany(
    params: {
      /** requestBody */
      body?: RoleEntity[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/roles/bulk';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ShippingAddressesService {
  /**
   * Chi tiết Địa chỉ giao hàng
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ShippingAddressEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/shipping-addresses/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Địa chỉ giao hàng
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: ShippingAddressEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ShippingAddressEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/shipping-addresses/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Địa chỉ giao hàng
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyShippingAddressEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/shipping-addresses';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Địa chỉ giao hàng
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: ShippingAddressEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ShippingAddressEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/shipping-addresses';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class ProductPostService {
  /**
   * Danh sách Bài đăng
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyProductPostEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-post';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Bài đăng
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: CreateProductPostDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductPostEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-post';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Bài đăng
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: UpdateProductPostDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductPostEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-post/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Bài đăng
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductPostEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-post/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Bài đăng
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ProductPostEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-post/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   *
   */
  static productPostControllerAddContent(options: IRequestOptions = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/product-post/add-content';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class CdnService {
  /**
   * Upload media
   */
  static cdnControllerUploadFile(
    params: {
      /**  */
      files: [];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/cdn/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['files']) {
        if (Object.prototype.toString.call(params['files']) === '[object Array]') {
          for (const item of params['files']) {
            data.append('files', item as any);
          }
        } else {
          data.append('files', params['files'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class CustomersService {
  /**
   * Thêm Khách hàng
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: CreateCustomerDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CustomerEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/customers';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Khách hàng
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyCustomerEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/customers';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm địa chỉ nhận hàng
   */
  static customerControllerAddAddress(
    params: {
      /** requestBody */
      body?: CreateCustomerDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CustomerEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/customers/address';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Cập nhật địa chỉ nhận hàng
   */
  static customerControllerUpdateAddress(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: CreateCustomerDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CustomerEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/customers/update-address/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Khách hàng
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CustomerEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/customers/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Khách hàng
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: CustomerEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CustomerEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/customers/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class BankService {
  /**
   * Chi tiết Ngân hàng
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BankEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/bank/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Ngân hàng
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: BankEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BankEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/bank/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Ngân hàng
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyBankEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/bank';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Ngân hàng
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: BankEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BankEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/bank';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class UserTransactionService {
  /**
   * Thêm UserTransaction
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: UserTransactionEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserTransactionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/user-transaction';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách UserTransaction
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyUserTransactionEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/user-transaction';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xác nhận nạp/rút tiền
   */
  static userTransactionControllerVerify(
    params: {
      /** ID topup */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserTransactionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/user-transaction/verify/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Huỷ lệnh nạp tiền
   */
  static userTransactionControllerCancel(
    params: {
      /** ID topup */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserTransactionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/user-transaction/cancel/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết UserTransaction
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserTransactionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/user-transaction/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa UserTransaction
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: UserTransactionEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserTransactionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/user-transaction/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class NotificationService {
  /**
   * Chi tiết Thông báo
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<NotificationEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/notification/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Thông báo
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: NotificationEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<NotificationEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/notification/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Thông báo
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyNotificationEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/notification';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Thông báo
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: NotificationEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<NotificationEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/notification';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class GiỏHàngService {
  /**
   * Giỏ hàng
   */
  static cartControllerGetAll(options: IRequestOptions = {}): Promise<GetAllCartResponse[]> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/cart';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export interface GetManyUserEntityResponseDto {
  /**  */
  data: UserEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface GetManyUserWalletEntityResponseDto {
  /**  */
  data: UserWalletEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface UserWalletEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Id người dùng */
  userId: number;

  /** Loại ví */
  type: EnumUserWalletEntityType;

  /** Số tiền trong ví */
  amount?: number;
}

export interface GetManyMerchantEntityResponseDto {
  /**  */
  data: MerchantEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface MerchantEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tên đối tác */
  name: string;

  /** Mã nhà cung cấp */
  code: string;

  /** Địa chỉ */
  address?: string;

  /** Số điện thoại */
  tel?: string;

  /** ID của Xã\/Phường */
  wardId?: number;

  /** Trạng thái */
  status?: EnumMerchantEntityStatus;

  /** Id ngân hàng */
  bankId?: number;

  /** Chủ TK ngân hàng */
  bankAccount?: string;

  /** STK ngân hàng */
  bankNumber?: string;

  /** Chi nhánh ngân hàng */
  bankBranch?: string;

  /** Website */
  website?: string;

  /** Hoa hồng */
  commission?: string[];

  /** Kho hàng */
  addresses?: string[];

  /** Thống kê doanh thu */
  revenueStatistic?: string[];
}

export interface UserEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Email */
  email?: string;

  /** Mật khẩu */
  password: string;

  /** Lưu các mật khẩu cũ */
  passwordHistory?: string[];

  /** Tên */
  fullName: string;

  /** Số điện thoại */
  tel: string;

  /** Đăng nhập cuối */
  lastLogin?: Date;

  /** Role ID */
  roleId?: number;

  /** ID cha */
  parentId?: number;

  /** Danh sách cấp dưới  */
  referralChildren?: UserEntity[];

  /** Id cấp trên */
  referralParentId?: number;

  /** Session sau khi gửi xác nhận SĐT với Firebase */
  sessionVerifyCode?: string;

  /** ID Nhà cung cấp */
  merchantId?: number;

  /** Số CMTND\/CCCD */
  nationalId?: string;

  /** Số CMTND\/CCCD */
  nationalIssueDate?: Date;

  /** Nơi cấp */
  nationalIssueBy?: string;

  /** Địa chỉ */
  address?: string;

  /** Phường\/Xã */
  wardId?: number;

  /** Số tài khoản */
  bankNumber?: string;

  /** Chủ tài khoản */
  bankAccountName?: string;

  /** Chi nhánh ngân hàng */
  bankBranch?: string;

  /** Ngày sinh */
  dob?: Date;

  /** Giới tính */
  gender: EnumUserEntityGender;

  /** Trạng thái */
  status: EnumUserEntityStatus;

  /** Ngày bắt đầu tính hoa hồng cho người giới thiệu (ngày hoàn thành đơn đầu tiên) */
  referralBonusStart?: Date;

  /** Thời hạn kết thúc nhận Bonus cho người giới thiệu */
  referralBonusExpire?: Date;

  /** Giá trị hoa hồng\/thưởng được áp dụng */
  valueBonus?: number;

  /** Loại giá trị hoa hồng\/thưởng được áp dụng */
  valueBonusType: EnumUserEntityValueBonusType;

  /** Ví của Thành viên */
  wallets: UserWalletEntity[];

  /** Danh sách cấp trên */
  leaders: UserEntity[];

  /** Danh sách NCC */
  merchants: MerchantEntity[];

  /** Id ngân hàng */
  bankId?: number;

  /** Ảnh đại diện */
  avatar?: string;
}

export interface UserBonusResponse {
  /** Thông tin F1 */
  user: CombinedUserTypes;

  /** Hoa hồng từ doanh thu F1 */
  totalReferralBonus: number;

  /** Hoa hồng từ đơn đầu tiên doanh thu F1 */
  totalFirstOrderBonus: number;
}

export interface UserBonus {
  /** Hoa hồng từ doanh thu F1 */
  totalReferralBonus: number;

  /** Hoa hồng từ đơn đầu tiên doanh thu F1 */
  totalFirstOrderBonus: number;

  /** Danh sách hoa hồng từ doanh thu F1 */
  userBonus: UserBonusResponse[];
}

export interface AddDeviceTokenDto {
  /** Firebase Messaging token */
  token: string;
}

export interface DeviceTokenEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Firebase Messaging token */
  token: string;

  /** User ID */
  userId: number;
}

export interface ChangePasswordDto {
  /** Mật khẩu cũ */
  oldPassword: string;

  /** Mật khẩu mới */
  newPassword: string;

  /** Nhập lại mật khẩu mới */
  newPasswordConfirm: string;
}

export interface UpdateAvatarDto {
  /** Ảnh đại diện */
  avatar: string;
}

export interface UpdatePaymentInfoDto {
  /** Số tài khoản */
  bankNumber?: string;

  /** Chủ tài khoản */
  bankAccountName?: string;

  /** Chi nhánh ngân hàng */
  bankBranch?: string;

  /** Id ngân hàng */
  bankId?: number;
}

export interface ReferralCommission {
  /** Tiền thưởng đăng ký thành công cho F0 */
  f0: number;

  /** Tiền thưởng đăng ký thành công cho F1 */
  f1: number;
}

export interface ConfigDto {
  /** Id đối tác (null: Cấu hình cho sàn; #null: Cấu hình cho đối tác) */
  merchantId: number;

  /** Loại hoa hồng\/thưởng */
  key: string;

  /** Ngày bắt đầu được áp dụng */
  startDate: Date;

  /** Ngày kết thúc áp dụng */
  endDate: Date;

  /** Cấu hình thưởng giới thiệu cho sàn */
  referralCommission: CombinedReferralCommissionTypes;

  /** Cấu hình thưởng theo số lượng CTV cho sàn */
  amountCollaboratorCommission: string[];

  /** Cấu hình cho đối tác */
  merchantCommission: string[];

  /** Tên cấu hình */
  name: string;

  /** Các thuộc tính cấu hình */
  value: object;

  /** Ghi chú cấu hình */
  note: string;
}

export interface GetManyConfigEntityResponseDto {
  /**  */
  data: ConfigEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface ConfigEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Id dùng để lấy cấu hình từ phía đối tác */
  merchantId: number;

  /** Kiểu cấu hình */
  key: EnumConfigEntityKey;

  /** Tên cấu hình */
  name?: string;

  /** Các thuộc tính cấu hình */
  value: [];

  /** Ngày bắt đầu được áp dụng */
  startDate: Date;

  /** Ngày kết thúc áp dụng */
  endDate: Date;

  /** Ghi chú */
  note: string;
}

export interface CreateMerchantDto {
  /** Tên đối tác */
  name: string;

  /** Mã nhà cung cấp */
  code: string;

  /** Địa chỉ */
  address?: string;

  /** Số điện thoại */
  tel?: string;

  /** ID của Xã\/Phường */
  wardId?: number;

  /** Trạng thái */
  status?: EnumCreateMerchantDtoStatus;

  /** Id ngân hàng */
  bankId?: number;

  /** Chủ TK ngân hàng */
  bankAccount?: string;

  /** STK ngân hàng */
  bankNumber?: string;

  /** Chi nhánh ngân hàng */
  bankBranch?: string;
}

export interface LoginDto {
  /** Số điện thoại */
  tel: string;

  /** Mật khẩu */
  password: string;
}

export interface LoginResponse {
  /** Access token */
  token: string;
}

export interface RegisterDto {
  /** Email */
  email?: string;

  /** Mật khẩu */
  password: string;

  /** Tên */
  fullName: string;

  /** Số điện thoại */
  tel: string;

  /** Phường\/Xã */
  wardId?: number;

  /** Ngày sinh */
  dob?: Date;

  /** Giới tính */
  gender: EnumRegisterDtoGender;

  /** Mã người giới thiệu */
  referralCode?: string;
}

export interface VerifyOtpDto {
  /** Firebase Token */
  idToken: string;

  /** Số điện thoại */
  tel: string;
}

export interface SendOtpForgotPasswordDto {
  /** Số điện thoại reset mật khẩu */
  tel: string;

  /** Recaptcha Token */
  recaptchaToken: string;
}

export interface ResetPasswordDto {
  /** Mật khẩu */
  password: string;

  /** Số điện thoại */
  tel: string;

  /** Firebase Token */
  idToken: string;
}

export interface DepositWalletDto {
  /** Số tiền giao dịch */
  amount: number;

  /** Firebase Token */
  idToken: string;
}

export interface GetManyUserTransactionEntityResponseDto {
  /**  */
  data: UserTransactionEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface GetManyBankEntityResponseDto {
  /**  */
  data: BankEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface BankEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tên đầy đủ của ngân hàng */
  name: string;

  /** Tên viết tắt */
  shortName: string;
}

export interface UserTransactionEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Id người dùng */
  userId: number;

  /** Id ví người dùng */
  walletId: number;

  /** Id quản trị xác nhận giao dịch */
  confirmId?: number;

  /** Số tiền giao dịch */
  amount: number;

  /** Trạng thái giao dịch */
  status: EnumUserTransactionEntityStatus;

  /** Id ngân hàng */
  bankId?: number;

  /** Số tài khoản */
  bankNumber?: string;

  /** Chủ tài khoản */
  bankAccountName?: string;

  /** Loại ví */
  walletType: EnumUserTransactionEntityWalletType;

  /** Loại giao dịch */
  transactionType: EnumUserTransactionEntityTransactionType;

  /** Ghi chú */
  note?: string;

  /** Thông tin tên ngân hàng */
  bank: CombinedBankTypes;
}

export interface WithdrawWalletDto {
  /** Số tiền giao dịch */
  amount: number;

  /** Loại ví */
  walletType: EnumWithdrawWalletDtoWalletType;

  /** Ghi chú */
  note?: string;

  /** Firebase Token */
  idToken: string;
}

export interface UpdateTransactionDto {
  /** Trạng thái giao dịch */
  status: EnumUpdateTransactionDtoStatus;

  /** Ghi chú */
  note?: string;

  /** Id giao dịch */
  transactionId: string;
}

export interface GetManyWardEntityResponseDto {
  /**  */
  data: WardEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface GetManyDistrictEntityResponseDto {
  /**  */
  data: DistrictEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface GetManyProvinceEntityResponseDto {
  /**  */
  data: ProvinceEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface ProvinceEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tên Tỉnh\/thành */
  name: string;

  /** ID Giao hàng nhanh */
  ghnId?: number;

  /** ID Viettel Post */
  vtpId?: number;

  /** ID eShop */
  eShopId?: string;

  /** ID Viettel Post */
  nameExtensions?: string[];

  /** Loại Tỉnh\/thành */
  type: EnumProvinceEntityType;

  /** Sắp xếp */
  position: EnumProvinceEntityPosition;
}

export interface DistrictEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tên Quận\/huyện */
  name: string;

  /** Loại Quận\/huyện */
  type: EnumDistrictEntityType;

  /** ID Tỉnh\/thành */
  provinceId: number;

  /** Thông tin Tỉnh\/thành */
  province?: CombinedProvinceTypes;

  /** ID Giao hàng nhanh */
  ghnId?: number;

  /** ID Viettel Post */
  vtpId?: number;

  /** ID eShop */
  eShopId?: string;

  /** ID Viettel Post */
  nameExtensions?: string[];
}

export interface WardEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tên Phường\/Xã */
  name: string;

  /** Loại Phường\/Xã */
  type: EnumWardEntityType;

  /** ID của Quận\/Huyện */
  districtId: number;

  /** Thông tin Quận\/huyện */
  district?: CombinedDistrictTypes;

  /** Code Giao hàng nhanh */
  ghnCode?: string;

  /** ID Viettel Post */
  vtpId?: number;

  /** ID eShop */
  eShopId?: string;
}

export interface GetManyOrderEntityResponseDto {
  /**  */
  data: OrderEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface GetManyMerchantAddressEntityResponseDto {
  /**  */
  data: MerchantAddressEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface MerchantAddressEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** ID của NCC */
  merchantId: number;

  /** Nhà cung cấp */
  merchant: CombinedMerchantTypes;

  /** Địa chỉ */
  address: string;

  /** Tên kho hàng */
  name: string;

  /** Mã */
  code: string;

  /** Số điện thoại */
  tel: string;

  /** Trạng thái */
  status: EnumMerchantAddressEntityStatus;

  /** ID của Xã\/Phường */
  wardId: number;

  /** Xã phường */
  ward: CombinedWardTypes;

  /** ShopId địa chỉ của GHN */
  ghnId?: number;

  /** ShopId địa chỉ của GHTK */
  ghtkId?: number;

  /** ShopId địa chỉ của VIETTELPOST */
  vtpId?: number;
}

export interface ShipperInfo {
  /** Tên Đối tác */
  partnerName?: string;

  /** Tên người vận chuyển */
  fullName?: string;

  /** Số điện thoại người vận chuyển */
  tel?: string;
}

export interface ShippingInfo {
  /** Id sản phẩm vận đơn */
  orderProductIds: string[];

  /** Đối tác vận chuyển */
  partner: EnumShippingInfoPartner;

  /** Ghi chú của Merchant cho Đối tác */
  note: string;

  /** Mã đơn hàng */
  id?: number;

  /** Mã đơn hàng đối tác Vận chuyển */
  partner_id?: string;

  /** Phí vận chuyển */
  fee: number;

  /** Phí bảo hiểm */
  insurance_fee?: number;

  /** Thời gian lấy hàng dự kiến */
  estimated_pick_time?: string;

  /** Thời gian giao hàng dự kiến */
  estimated_deliver_time?: string;

  /** Trạng thái giao hàng */
  status?: EnumShippingInfoStatus;

  /** Tên chi tiết trạng thái giao hàng */
  statusName?: string;

  /** Thông tin Người giao hàng */
  shipperInfo?: CombinedShipperInfoTypes;
}

export interface GetManyProductEntityResponseDto {
  /**  */
  data: ProductEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface ProductGroupTypeValue {
  /** Tên biến thể */
  field: string;
}

export interface ProductGroupType {
  /** Tên nhóm Biến thể */
  name: string;

  /** Biến thể */
  value: ProductGroupTypeValue[];
}

export interface StockValue {
  /** Id kho hàng */
  merchantAddressId: number;

  /** Số lượng kho */
  quantity: number;

  /** Thông tin địa chỉ kho hàng */
  merchantAddress?: CombinedMerchantAddressTypes;
}

export interface GetManyProductCategoryEntityResponseDto {
  /**  */
  data: ProductCategoryEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface GetManyProductCategoryAttributeEntityResponseDto {
  /**  */
  data: ProductCategoryAttributeEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface ProductCategoryAttributeValueEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tên giá trị */
  name: string;

  /** Đối tác */
  merchantId?: number;

  /** Id thuộc tính danh mục sản phẩm */
  productCategoryAttributeId?: number;

  /** Thuộc tính danh mục sản phẩm */
  productCategoryAttribute?: CombinedProductCategoryAttributeTypes;
}

export interface ProductCategoryAttributeEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tên thuộc tính */
  name: string;

  /** Loại control */
  controlType: EnumProductCategoryAttributeEntityControlType;

  /** Đối tác có thể tự thêm */
  canAdd: boolean;

  /** Đơn vị */
  units?: string[];

  /** Id danh mục sản phẩm */
  productCategoryId?: number;

  /** Danh sách giá trị của thuộc tính */
  productCategoryAttributeValues: ProductCategoryAttributeValueEntity[];
}

export interface ProductCategoryEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tên */
  name: string;

  /** Đường dẫn danh mục */
  slug?: string;

  /** Mã */
  code: string;

  /** Mô tả */
  description?: string;

  /** Màu sắc */
  color?: string;

  /** Trạng thái */
  state: EnumProductCategoryEntityState;

  /** Ảnh */
  image?: string;

  /** ID danh mục của Merchant */
  merchantCategoryId?: number;

  /** Danh sách thuộc tính */
  productCategoryAttributes: ProductCategoryAttributeEntity[];

  /** Danh sách cấp dưới  */
  childrens?: ProductCategoryEntity[];

  /** Danh mục cha */
  parentId?: number;

  /** Danh mục cha */
  parent?: CombinedParentTypes;
}

export interface ProductAttributeEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Id sản phẩm */
  productId: number;

  /** ID giá trị thuộc tính danh mục */
  productCategoryAttributeValueIds?: number[];

  /** Diá trị thuộc tính danh mục */
  productCategoryAttributeValues?: ProductCategoryAttributeValueEntity[];

  /** ID giá trị thuộc tính danh mục */
  productCategoryAttributeId?: number;

  /** Giá trị thuộc tính danh mục */
  productCategoryAttribute?: CombinedProductCategoryAttributeTypes;

  /** Đơn vị tính */
  productCategoryAttributeUnitIndex?: number;
}

export interface ProductVariantEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tên biến thể "Thuộc tính 1,Thuộc tính 2" */
  name: string;

  /** Index biến thể */
  groupTypeIndex?: number;

  /** Index giá trị biến thể */
  valueTypeIndex?: number;

  /** Id sản phẩm */
  productId?: number;

  /** Sản phẩm */
  product?: CombinedProductTypes;

  /** Hình ảnh biến thể */
  images?: string[];

  /** Giá biến thể sản phẩm */
  price: number;

  /** Hoa hồng biến thể sản phẩm từ NCC */
  commission?: number;

  /** Danh sách tồn kho */
  stock?: StockValue[];

  /** Mã Sku */
  SKU?: string;

  /** Tag biến thể sản phẩm */
  tag?: string;

  /** Flash sale */
  flashSale?: object;
}

export interface ProductStockEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** ID Sản phẩm */
  productId: number;

  /** Số lượng tồn kho */
  quantity: number;

  /** ID Kho */
  merchantAddressId: number;

  /** Kho */
  merchantAddress: CombinedMerchantAddressTypes;
}

export interface ProductEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Id sản phẩm eShop */
  productEShopId?: string;

  /** Tên sản phẩm */
  name: string;

  /** Đường dẫn sản phẩm */
  slug: string;

  /** Mô tả */
  description?: string;

  /** Mô tả ngắn */
  shortDescription?: string;

  /** Danh sách ảnh */
  images?: string[];

  /** Giá sản phẩm\/Giá thấp nhất của sản phẩm */
  price: number;

  /** Giá chưa khuyến mãi */
  rawPrice: number;

  /** Giá cao nhất sản phẩm */
  maxPrice: number;

  /** Giá trước khi giảm */
  priceBeforeDiscount: number;

  /** ID Người đăng */
  userId: number;

  /** Khối lượng (ĐVT: gram) */
  weight: number;

  /** Chiều cao */
  height?: number;

  /** Chiều dài */
  length?: number;

  /** Chiều rộng */
  width?: number;

  /** ID Đối tác */
  merchantId: number;

  /** Video */
  videoUrl: string;

  /** Đối tác */
  merchant: CombinedMerchantTypes;

  /** ID Sản phẩm của Merchant */
  merchantProductId: number;

  /** ID Danh mục sản phẩm */
  productCategoryId: number;

  /** Số lượng sản phẩm đã bán */
  saleAmount: number;

  /** Trạng thái */
  status: EnumProductEntityStatus;

  /** Tên nhóm biến thể */
  groupType?: ProductGroupType[];

  /** Hoa hồng sản phẩm từ NCC */
  commission: number;

  /** Hoa hồng theo % sản phẩm từ NCC */
  commissionPercent: number;

  /** Danh sách tồn kho */
  stock?: StockValue[];

  /** Mã Sku */
  SKU?: string;

  /** Tag sản phẩm */
  tag?: string;

  /** Danh mục */
  productCategory: CombinedProductCategoryTypes;

  /** Danh sách thuộc tính */
  productAttributes: ProductAttributeEntity[];

  /** Biến thể */
  variants?: ProductVariantEntity[];

  /** Kho */
  stocks?: ProductStockEntity[];

  /** Kho */
  cartProducts?: string[];

  /** Flash sale */
  flashSale?: object;
}

export interface OrderProductEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Mã đơn hàng */
  orderId: number;

  /** ID Sản phẩm */
  productId: number;

  /** ID Flash sale */
  flashSaleId?: number;

  /** ID Flash sale detail */
  flashSaleDetailId?: number;

  /** Sản phẩm */
  product?: CombinedProductTypes;

  /** Tên biến thể (Tên các thuộc tính) */
  variantTitle?: string;

  /** ID biến thể */
  variantId: number;

  /** ID biến thể */
  variant: CombinedVariantTypes;

  /** Số lượng */
  quantity?: number;

  /** Giá */
  price?: number;

  /** Giảm giá */
  discount?: number;

  /** Hoa hồng */
  commission?: number;

  /** Danh sách voucher được áp dụng */
  voucherIds?: number[];
}

export interface OrderHistoryEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** ID Người dùng */
  userId?: number;

  /** Người dùng */
  user?: CombinedUserTypes;

  /** ID Đơn hàng */
  orderId: number;

  /** Trạng thái */
  status: EnumOrderHistoryEntityStatus;

  /** Loại */
  type: EnumOrderHistoryEntityType;
}

export interface OrderEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Mã đơn hàng */
  code: string;

  /** Trạng thái đơn hàng */
  status: EnumOrderEntityStatus;

  /** ID Đối tác */
  merchantId?: number;

  /** Đối tác */
  merchant: CombinedMerchantTypes;

  /** ID Địa chỉ lấy hàng Đối tác */
  merchantAddressId?: number;

  /** Địa chỉ lấy hàng Đối tác */
  merchantAddress: CombinedMerchantAddressTypes;

  /** ID Người dùng đặt hàng */
  userId?: number;

  /** Người dùng đặt hàng */
  user: CombinedUserTypes;

  /** Họ tên */
  fullname: string;

  /** Địa chỉ */
  shippingAddress: string;

  /** ID Phường\/Xã */
  wardId?: number;

  /** Phường\/Xã */
  ward: CombinedWardTypes;

  /** Số điện thoại */
  tel: string;

  /** Phí ship */
  shippingFee?: number;

  /** Giảm giá Phí ship */
  shippingDiscount?: number;

  /** Tổng tiền */
  total?: number;

  /** Giảm giá */
  discount?: number;

  /** Phương thức thanh toán */
  paymentType?: EnumOrderEntityPaymentType;

  /** Trạng thái thanh toán */
  paymentStatus?: EnumOrderEntityPaymentStatus;

  /** Đơn vị vận chuyển */
  shippingPartner?: string;

  /** Thông tin vận chuyển */
  shippingInfos?: ShippingInfo[];

  /** Thông tin vận chuyển (view) */
  shippingInfo?: CombinedShippingInfoTypes;

  /** ID Đơn hàng của Merchant  */
  merchantOrderId?: number;

  /** Danh sách sản phẩm */
  orderProducts?: OrderProductEntity[];

  /** Lịch sử đơn hàng */
  histories?: OrderHistoryEntity[];

  /** Chiều cao (ĐVT: cm) */
  height?: number;

  /** Chiều dài (ĐVT: cm) */
  length?: number;

  /** Chiều rộng (ĐVT: cm) */
  width?: number;

  /** Ghi chú của KH\/CTV */
  note?: string;

  /** Lý do huỷ */
  cancel_reason?: string;

  /** Cấu hình */
  configCommissionId?: number;

  /** Tiền đặt cọc\/trả trước */
  deposit?: number;

  /** Hoa hồng */
  commission?: number;

  /** Giảm giá của voucher Shop */
  voucherMerchantDiscount?: number;

  /** Giảm giá của voucher Ushare */
  voucherUshareDiscount?: number;
}

export interface CreateSubShipmentDto {
  /** Chiều cao (ĐVT: cm) */
  height?: number;

  /** Chiều dài (ĐVT: cm) */
  length?: number;

  /** Chiều rộng (ĐVT: cm) */
  width?: number;

  /** Đối tác */
  partner?: EnumCreateSubShipmentDtoPartner;

  /** ID sản phẩm trong đơn */
  orderProductId?: number;

  /** Thông tin Người vận chuyển đối với DVVC là Khác */
  otherInfo?: CombinedOtherInfoTypes;
}

export interface ShipmentRequestDto {
  /** Chiều cao (ĐVT: cm) */
  height?: number;

  /** Chiều dài (ĐVT: cm) */
  length?: number;

  /** Chiều rộng (ĐVT: cm) */
  width?: number;

  /** Ghi chú của KH\/CTV */
  note?: string;

  /** Kho */
  merchantAddressId: number;

  /** Đối tác */
  partner?: EnumShipmentRequestDtoPartner;

  /** ID Đơn hàng */
  orderId: number;

  /** Thông tin giao vận đơn con */
  subShipments?: CreateSubShipmentDto[];

  /** Phí vận chuyển đối tác khác */
  otherFee?: number;

  /** Thông tin Người vận chuyển đối với DVVC là Khác */
  otherInfo?: CombinedOtherInfoTypes;
}

export interface SubShippingFeeResponseDto {
  /** Danh sách orderProductId trong đơn cha */
  orderProductIds: number[];

  /** Thông tin vận chuyển */
  shippingInfo: CombinedShippingInfoTypes;
}

export interface ShippingFeeResponseDto {
  /** Danh sách orderProductId trong đơn cha */
  orderId: number;

  /** Đối tác */
  partner?: string;

  /** Danh sách orderProductId trong đơn cha */
  shippingInfo: CombinedShippingInfoTypes;

  /** Danh sách orderProductId trong đơn cha */
  subShippingInfo?: SubShippingFeeResponseDto[];
}

export interface ChangeStatusDto {
  /** Trạng thái đơn hàng */
  status: EnumChangeStatusDtoStatus;

  /** Lý do huỷ */
  cancel_reason?: string;
}

export interface CreateOrderProduct {
  /** Số lượng */
  quantity: number;

  /** ID Sản phẩm */
  id: number;

  /** ID biến thể Sản phẩm */
  variantId?: number;
}

export interface UpdateOrderDto {
  /** ID */
  id?: number;

  /** Trạng thái đơn hàng */
  status?: EnumUpdateOrderDtoStatus;

  /** Id kho hàng */
  merchantAddressId?: number;

  /** Họ tên */
  fullname: string;

  /** Địa chỉ */
  shippingAddress: string;

  /** ID Phường\/Xã */
  wardId?: number;

  /** Số điện thoại */
  tel: string;

  /** Phương thức thanh toán */
  paymentType?: EnumUpdateOrderDtoPaymentType;

  /** Ghi chú của KH\/CTV */
  note?: string;

  /** Sản phẩm trong đơn hàng */
  products?: CreateOrderProduct[];

  /** ID người giới thiệu\/Id CTV */
  pubId?: number;

  /** Danh sách Id voucher */
  voucherIds?: number[];

  /** Danh sách mã voucher */
  voucherCodes?: string[];
}

export interface CreateOrderDto {
  /** Id kho hàng */
  merchantAddressId?: number;

  /** Họ tên */
  fullname: string;

  /** Địa chỉ */
  shippingAddress: string;

  /** ID Phường\/Xã */
  wardId?: number;

  /** Số điện thoại */
  tel: string;

  /** Phương thức thanh toán */
  paymentType?: EnumCreateOrderDtoPaymentType;

  /** Ghi chú của KH\/CTV */
  note?: string;

  /** Sản phẩm trong đơn hàng */
  products?: CreateOrderProduct[];

  /** ID người giới thiệu\/Id CTV */
  pubId?: number;

  /** Danh sách Id voucher */
  voucherIds?: number[];

  /** Danh sách mã voucher */
  voucherCodes?: string[];
}

export interface StatusCountResponseDto {
  /** Trạng thái */
  status: EnumStatusCountResponseDtoStatus;

  /** Số lượng đơn */
  count: number;
}

export interface OrderFilterDto {
  /** Trang */
  page: number;

  /** Số bản ghi trên 1 trang */
  limit: number;

  /** Ký tự tìm kiếm */
  textSearch?: string;

  /** Ngày bắt đầu */
  fromDate: Date;

  /** Ngày kết thúc */
  toDate: Date;

  /** Id CTV */
  userId: number;
}

export interface SearchResult {
  /** Trang */
  page: number;

  /** Số bản ghi trên 1 trang */
  limit: number;

  /** Tổng số bản ghi */
  totalCount: number;

  /** Tổng số trang */
  totalPage: number;

  /** Dữ liệu trả về */
  data: object;
}

export interface ChangePaymentTypeDto {
  /** Phương thức thanh toán */
  paymentType?: EnumChangePaymentTypeDtoPaymentType;

  /** Tiền đặt cọc\/trả trước */
  deposit?: number;

  /** Danh sách Id đơn hàng */
  orderIds: number[];
}

export interface ConfirmDepositDto {
  /** Id đơn hàng */
  orderId: number;

  /** Tiền cọc */
  deposit: number;
}

export interface ChangeShippingStatusDto {
  /** Trạng thái vận chuyển */
  shippingStatus: EnumChangeShippingStatusDtoShippingStatus;
}

export interface GhtkWebhookDto {}

export interface GhnWebhookDto {}

export interface VtpWebhookDto {}

export interface GetManyVoucherEntityResponseDto {
  /**  */
  data: VoucherEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface VoucherEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Id đối tác */
  merchantId?: number;

  /** Loại voucher */
  type: EnumVoucherEntityType;

  /** Tên voucher */
  name: string;

  /** Mã voucher */
  code: string;

  /** Thời gian bắt đầu áp dụng voucher */
  startDate: Date;

  /** Thời gian kết thúc áp dụng voucher */
  endDate: Date;

  /** Danh sách sản phẩm được áp dụng */
  productIds?: number[];

  /** Danh sách người dùng được áp dụng */
  userIds?: string[];

  /** Số lượng voucher */
  quantity?: number;

  /** Số lượng voucher đã sử dụng */
  quantityUsed?: number;

  /** Giá trị nhỏ nhất đơn hàng */
  minAmount?: number;

  /** Giá trị lớn nhất đơn hàng */
  maxAmount?: number;

  /** Loại giảm */
  discountType: EnumVoucherEntityDiscountType;

  /** Mức giảm */
  discountValue: number;

  /** Mức giảm tối đa */
  discountMaximum?: number;

  /** Mô tả  */
  description?: string;

  /** Danh sách Sản phẩm áp dụng */
  products?: ProductEntity[];

  /** Tiêu đề ngắn mã giảm giá */
  subTitle?: string;

  /** Số lượng sản phẩm tối thiểu cho 1 đơn hàng */
  minProductNumber?: number;

  /** Giá tối thiểu cho mỗi sản phẩm */
  minProductPrice?: number;
}

export interface DetailProudct {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Id sản phẩm eShop */
  productEShopId?: string;

  /** Tên sản phẩm */
  name: string;

  /** Đường dẫn sản phẩm */
  slug: string;

  /** Mô tả */
  description?: string;

  /** Mô tả ngắn */
  shortDescription?: string;

  /** Danh sách ảnh */
  images?: string[];

  /** Giá sản phẩm\/Giá thấp nhất của sản phẩm */
  price: number;

  /** Giá chưa khuyến mãi */
  rawPrice: number;

  /** Giá cao nhất sản phẩm */
  maxPrice: number;

  /** Giá trước khi giảm */
  priceBeforeDiscount: number;

  /** ID Người đăng */
  userId: number;

  /** Khối lượng (ĐVT: gram) */
  weight: number;

  /** Chiều cao */
  height?: number;

  /** Chiều dài */
  length?: number;

  /** Chiều rộng */
  width?: number;

  /** ID Đối tác */
  merchantId: number;

  /** Video */
  videoUrl: string;

  /** Đối tác */
  merchant: CombinedMerchantTypes;

  /** ID Sản phẩm của Merchant */
  merchantProductId: number;

  /** ID Danh mục sản phẩm */
  productCategoryId: number;

  /** Số lượng sản phẩm đã bán */
  saleAmount: number;

  /** Trạng thái */
  status: EnumDetailProudctStatus;

  /** Tên nhóm biến thể */
  groupType?: ProductGroupType[];

  /** Hoa hồng sản phẩm từ NCC */
  commission: number;

  /** Hoa hồng theo % sản phẩm từ NCC */
  commissionPercent: number;

  /** Danh sách tồn kho */
  stock?: StockValue[];

  /** Mã Sku */
  SKU?: string;

  /** Tag sản phẩm */
  tag?: string;

  /** Danh mục */
  productCategory: CombinedProductCategoryTypes;

  /** Danh sách thuộc tính */
  productAttributes: ProductAttributeEntity[];

  /** Biến thể */
  variants?: ProductVariantEntity[];

  /** Kho */
  stocks?: ProductStockEntity[];

  /** Kho */
  cartProducts?: string[];

  /** Flash sale */
  flashSale?: object;

  /** Danh sách mã giảm giá */
  vouchers: VoucherEntity[];

  /** Cấu hình thưởng hoa hồng doanh thu */
  configCommission: CombinedConfigCommissionTypes;
}

export interface CreateProductCategoryAttributeValueDto {
  /** Id thuộc tính danh mục sản phẩm */
  productCategoryAttributeId?: number;

  /** Đơn vị tính */
  productCategoryAttributeUnitIndex?: number;

  /** ID các thuộc tính đã chọn */
  productCategoryAttributeValueIds?: string[];

  /**  Truyền lên nếu NCC tự nhập thuộc tính */
  name?: string;
}

export interface CreateProductVariantDto {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tên biến thể "Thuộc tính 1,Thuộc tính 2" */
  name: string;

  /** Index biến thể */
  groupTypeIndex?: number;

  /** Index giá trị biến thể */
  valueTypeIndex?: number;

  /** Id sản phẩm */
  productId?: number;

  /** Hình ảnh biến thể */
  images?: string[];

  /** Giá biến thể sản phẩm */
  price: number;

  /** Hoa hồng biến thể sản phẩm từ NCC */
  commission?: number;

  /** Danh sách tồn kho */
  stock?: StockValue[];

  /** Mã Sku */
  SKU?: string;

  /** Tag biến thể sản phẩm */
  tag?: string;

  /** Flash sale */
  flashSale?: object;
}

export interface CreateProductDto {
  /** Tên sản phẩm */
  name: string;

  /** Mô tả */
  description?: string;

  /** Danh sách ảnh */
  images?: string[];

  /** Giá sản phẩm\/Giá thấp nhất của sản phẩm */
  price: number;

  /** Giá trước khi giảm */
  priceBeforeDiscount: number;

  /** Khối lượng (ĐVT: gram) */
  weight: number;

  /** Chiều cao */
  height?: number;

  /** Chiều dài */
  length?: number;

  /** Chiều rộng */
  width?: number;

  /** Video */
  videoUrl: string;

  /** ID Danh mục sản phẩm */
  productCategoryId: number;

  /** Tên nhóm biến thể */
  groupType?: ProductGroupType[];

  /** Hoa hồng theo % sản phẩm từ NCC */
  commissionPercent: number;

  /** Danh sách tồn kho */
  stock?: StockValue[];

  /** Mã Sku */
  SKU?: string;

  /** Tag sản phẩm */
  tag?: string;

  /** Danh sách giá trị thuộc tính của danh mục */
  productAttributes?: CreateProductCategoryAttributeValueDto[];

  /** Danh sách biến thể */
  variants?: CreateProductVariantDto[];
}

export interface ProductFilterDto {
  /** Trang */
  page: number;

  /** Số bản ghi trên 1 trang */
  limit: number;

  /** Ký tự tìm kiếm */
  textSearch?: string;

  /** Loại tìm kiếm */
  sortType?: EnumProductFilterDtoSortType;

  /** Id khu vực tìm kiếm */
  provinceId?: number;

  /** Id danh mục */
  categoryId?: number;

  /** Id NCC */
  merchantId?: number;
}

export interface CreateProductCategoryDto {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tên */
  name: string;

  /** Đường dẫn danh mục */
  slug?: string;

  /** Mã */
  code: string;

  /** Mô tả */
  description?: string;

  /** Màu sắc */
  color?: string;

  /** Trạng thái */
  state: EnumCreateProductCategoryDtoState;

  /** Ảnh */
  image?: string;

  /** ID danh mục của Merchant */
  merchantCategoryId?: number;

  /** Danh sách thuộc tính */
  productCategoryAttributes: ProductCategoryAttributeEntity[];

  /** Danh sách cấp dưới  */
  childrens?: ProductCategoryEntity[];

  /** Danh mục cha */
  parentId?: number;

  /** Danh mục cha */
  parent?: CombinedParentTypes;
}

export interface GetManyFlashSaleEntityResponseDto {
  /**  */
  data: FlashSaleEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface FlashSaleDetailEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Id chương trình Flash Sale */
  flashSaleId: number;

  /** Id đối tác tham gia Flash Sale */
  merchantId: number;

  /** Id biến thể sản phẩm trong Flash Sale */
  productVariantId: number;

  /** Thông tin sản phẩm trong Flash Sale */
  productVariant: CombinedProductVariantTypes;

  /** Id sản phẩm trong Flash Sale */
  productId: number;

  /** Thông tin sản phẩm trong Flash Sale */
  product: CombinedProductTypes;

  /** Mức giảm giá */
  discountValue: number;

  /** Loại giảm giá */
  discountValueType: EnumFlashSaleDetailEntityDiscountValueType;

  /** Số lượng sản phẩm cho chương trình */
  quantity: number;

  /** Số sản phẩm đã được bán */
  usedQuantity: number;
}

export interface FlashSaleEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tên chương trình Flash Sale */
  name: string;

  /** Thời gian bắt đầu chương trình Flash Sale */
  startTime: string;

  /** Thời gian kết thúc chương trình Flash Sale */
  endTime: string;

  /** Số lượng khuyến mãi 1 NCC được phép bán ra */
  maxQuantity?: number;

  /** Số lượng sản phẩm tối đa được tham gia của 1 NCC */
  maxProductNumber?: number;

  /** Danh sách sản phẩm trong chương trình */
  flashSaleDetail: FlashSaleDetailEntity[];
}

export interface CreateFlashSaleDto {
  /** Tên chương trình Flash Sale */
  name: string;

  /** Thời gian bắt đầu chương trình Flash Sale */
  startTime: string;

  /** Thời gian kết thúc chương trình Flash Sale */
  endTime: string;

  /** Số lượng khuyến mãi 1 NCC được phép bán ra */
  maxQuantity?: number;

  /** Số lượng sản phẩm tối đa được tham gia của 1 NCC */
  maxProductNumber?: number;
}

export interface UpdateFlashSaleDto {
  /** ID */
  id?: number;

  /** Tên chương trình Flash Sale */
  name: string;

  /** Thời gian bắt đầu chương trình Flash Sale */
  startTime: string;

  /** Thời gian kết thúc chương trình Flash Sale */
  endTime: string;

  /** Số lượng khuyến mãi 1 NCC được phép bán ra */
  maxQuantity?: number;

  /** Số lượng sản phẩm tối đa được tham gia của 1 NCC */
  maxProductNumber?: number;
}

export interface FlashSaleMerchant {
  /** count */
  count: number;

  /** data */
  data: FlashSaleEntity[];

  /** page */
  page: number;

  /** pageCount */
  pageCount: number;

  /** total */
  total: number;
}

export interface FlashSaleDetailDto {
  /** Id chương trình flash sale */
  flashSaleId: number;

  /** Id sản phẩm thêm vào chương trình flash sale */
  productId: number;

  /** Id biến thể thêm vào chương trình flash sale */
  productVariantId?: number;

  /** Mức giảm giá */
  discountValue: number;

  /** Loại giảm giá */
  discountValueType: string;

  /** Số lượng sản phẩm được dùng cho chương trình */
  qty: number;
}

export interface FilterRevenueWithPaging {
  /** Ký tự tìm kiếm */
  textSearch?: string;

  /** Ngày bắt đầu */
  fromDate: Date;

  /** Ngày kết thúc */
  toDate: Date;

  /** Id đối tác */
  merchantId?: number;

  /** Trường */
  field?: EnumFilterRevenueWithPagingField;

  /** Id người dùng */
  userId?: number;

  /** Số lượng record\/trang */
  limit?: number;

  /** Trang */
  page?: number;
}

export interface Revenue {
  /** Tổng */
  sum: number;

  /** Trạng thái */
  status: EnumRevenueStatus;

  /** Số lượng đơn hàng */
  count: number;

  /** Phần trăm */
  rate: number;
}

export interface RevenueResponse {
  /** Doanh thu ước tính */
  revenue: CombinedRevenueTypes;

  /** Doanh thu hoàn thành */
  revenueComplepted: CombinedRevenueCompleptedTypes;

  /** Hoa hồng ước tính */
  commission: CombinedCommissionTypes;

  /** Hoa hồng thực nhận */
  commissionComplelted: CombinedCommissionCompleltedTypes;

  /** Hoa hồng giới thiệu */
  referralCommission: number;

  /** Cấu hình hoa hồng */
  currConfigCommission: string[];
}

export interface RevenueStatisticEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Id người dùng */
  userId: number;

  /** Id đối tác */
  merchantId: number;

  /** Tổng doanh thu hoàn thành trong tháng */
  totalRevenueCompleted: number;

  /** Tổng hoa hồng hoàn thành trong tháng */
  totalCommissionCompleted: number;

  /** Tổng hoa hồng theo sản phẩm trong tháng */
  totalProductCommission: number;

  /** Tháng thống kê */
  date: Date;
}

export interface RevenueStatistic {
  /** Id CTV */
  userId: number;

  /** Id NCC */
  merchantId: number;

  /** Tháng\/Quý tính doanh thu\/thưởng */
  date: Date;

  /** Tổng doanh thu hoàn thành */
  totalRevenueCompleted: number;

  /** Tổng chiết khấu theo doanh thu hoàn thành */
  totalCommissionCompleted: number;

  /** Tổng chiết khấu theo doanh thu */
  totalCommission: number;

  /** Thưởng giới thiệu */
  totalReferralCommission: number;

  /** Hoa hồng\/thưởng được áp dụng */
  commissionValue: number;

  /** Loại giá trị của hoa hồng\/thưởng (tiền\/%) */
  commissionValueType: string;

  /** Khoảng thời gian thưởng (Tháng, quý, năm) */
  commissionTimeType: string;
}

export interface RevenueStatisticResponse {
  /** Id CTV */
  userId: number;

  /** Thông tin CTV */
  user: CombinedUserTypes;

  /** Id NCC */
  merchantId: number;

  /** Thông tin NCC */
  merchant: CombinedMerchantTypes;

  /** Thống kê toàn bộ doanh thu */
  revenueStatistic?: RevenueStatistic[];

  /** Thống kê doanh thu theo tháng */
  revenueStatisticByMonth?: RevenueStatistic[];

  /** Thống kê doanh thu theo quý */
  revenueStatisticByQuarter?: RevenueStatistic[];

  /** Tổng chiết khấu theo doanh thu */
  totalCommissionRevenue?: number;
}

export interface RevenueStatisticResponses {
  /** count */
  count: number;

  /** data */
  data: RevenueStatisticResponse[];

  /** page */
  page: number;

  /** pageCount */
  pageCount: number;

  /** total */
  total: number;
}

export interface RevenueReferralStatistic {
  /** Thông tin người giới thiệu */
  referral: CombinedReferralTypes;

  /** Số lượng thành viên được giới thiêu */
  members: number;

  /** Thống kê hoa hồng giới thiệu */
  statistic: RevenueStatistic[];

  /** Thống kê hoa hồng giới thiệu */
  totalCommission: number;
}

export interface RevenueReferralStatisticResponse {
  /** count */
  count: number;

  /** data */
  data: RevenueReferralStatistic[];

  /** page */
  page: number;

  /** pageCount */
  pageCount: number;

  /** total */
  total: number;
}

export interface DetailRefrralStatistic {
  /** Thông tin thành viên được giới thiệu */
  user: CombinedUserTypes;

  /** Thưởng đơn đầu tiên */
  firstOrderCommission: number;

  /** Thưởng doanh thu trong tháng */
  revenueCommission: number;

  /** Tổng hoa hồng */
  totalCommission: number;
}

export interface TopUserCommissionResponse {
  /** Hoa hồng sản phẩm + hoa hồng doanh thu */
  revenues: RevenueStatisticResponse[];

  /** Cấu hình thưởng doanh thu theo NCC */
  configRevenue: CombinedConfigRevenueTypes;

  /** Doanh thu tháng hiện tại của CTV */
  revenueCurrentMonth: CombinedRevenueCurrentMonthTypes;
}

export interface RevenueStatisticByUserResponse {
  /** Ngày */
  date: Date;

  /** Doanh thu */
  revenue: CombinedRevenueTypes;
}

export interface CreateVoucherDto {
  /** ID */
  id?: number;

  /** Loại voucher */
  type: EnumCreateVoucherDtoType;

  /** Tên voucher */
  name: string;

  /** Mã voucher */
  code: string;

  /** Thời gian bắt đầu áp dụng voucher */
  startDate: Date;

  /** Thời gian kết thúc áp dụng voucher */
  endDate: Date;

  /** Danh sách sản phẩm được áp dụng */
  productIds?: number[];

  /** Số lượng voucher */
  quantity?: number;

  /** Giá trị nhỏ nhất đơn hàng */
  minAmount?: number;

  /** Giá trị lớn nhất đơn hàng */
  maxAmount?: number;

  /** Loại giảm */
  discountType: EnumCreateVoucherDtoDiscountType;

  /** Mức giảm */
  discountValue: number;

  /** Mức giảm tối đa */
  discountMaximum?: number;

  /** Mô tả  */
  description?: string;
}

export interface VoucherEntityResponse {
  /** count */
  count: number;

  /** data */
  data: VoucherEntity[];

  /** page */
  page: number;

  /** pageCount */
  pageCount: number;

  /** total */
  total: number;
}

export interface VoucherHistoryEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** SĐT Người đặt hàng */
  tel: string;

  /** Id voucher */
  voucherId: number;

  /** Id đơn hàng */
  orderId: number;
}

export interface CreateMerchantAddressDto {
  /** Địa chỉ */
  address: string;

  /** Tên kho hàng */
  name: string;

  /** Mã */
  code: string;

  /** Số điện thoại */
  tel: string;

  /** Trạng thái */
  status: EnumCreateMerchantAddressDtoStatus;

  /** ID của Xã\/Phường */
  wardId: number;
}

export interface CreateUpdateCartProductDto {
  /** ID Sản phẩm */
  productId: number;

  /** ID Biến thể của Sản phẩm */
  productVariantId?: number;

  /** ID Kho hàng */
  merchantAddressId: number;

  /** Số lượng */
  quantity: number;

  /** Sản phẩm được chọn */
  checked: boolean;

  /** ID giỏ hàng */
  id?: number;

  /** Mã ưu đãi của shop */
  voucherCode?: string;

  /** Mã ưu đãi của Ushare */
  voucherUshareCode?: string;
}

export interface GetManyCartProductEntityResponseDto {
  /**  */
  data: CartProductEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface CartProductEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Người dùng */
  userId: number;

  /** ID Sản phẩm */
  productId: number;

  /** Sản phẩm */
  product?: CombinedProductTypes;

  /** ID Biến thể của Sản phẩm */
  productVariantId?: number;

  /** Biến thể sản phẩm */
  productVariant?: CombinedProductVariantTypes;

  /** ID Kho hàng */
  merchantAddressId: number;

  /** Số lượng */
  quantity: number;

  /** Sản phẩm được chọn */
  checked: boolean;

  /** Giá sau khi áp dụng mã giảm giá thành công */
  priceDiscount: number;

  /** Mã ưu đãi của shop */
  voucherCode?: string;

  /** Mã ưu đãi của Ushare */
  voucherUshareCode?: string;

  /** Hoa hồng cho CTV */
  commission?: number;
}

export interface GetManyPermissionEntityResponseDto {
  /**  */
  data: PermissionEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface PermissionEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Hành động */
  action: string;
}

export interface GetManyRoleEntityResponseDto {
  /**  */
  data: RoleEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface RoleEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tên nhóm */
  name: string;

  /** Quyền */
  permissions: string[];

  /** Loại quyền */
  type: EnumRoleEntityType;
}

export interface GetManyShippingAddressEntityResponseDto {
  /**  */
  data: ShippingAddressEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface ShippingAddressEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;
}

export interface GetManyProductPostEntityResponseDto {
  /**  */
  data: ProductPostEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface ProductPostEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Người tạo */
  createdById?: number;

  /** Người sửa cuối */
  modifiedById?: number;

  /** ID Sản phẩm */
  productId?: number;

  /** Thông tin Sản phẩm */
  product?: CombinedProductTypes;

  /** Tiêu đề */
  title?: string;

  /** Nội dung */
  content: string;

  /** Ảnh\/video */
  media?: string[];

  /** Trạng thái */
  status?: EnumProductPostEntityStatus;
}

export interface CreateProductPostDto {
  /** ID Sản phẩm */
  productId?: number;

  /** Tiêu đề */
  title?: string;

  /** Nội dung */
  content: string;

  /** Ảnh\/video */
  media?: string[];

  /** Trạng thái */
  status?: EnumCreateProductPostDtoStatus;
}

export interface UpdateProductPostDto {
  /** ID */
  id?: number;

  /** ID Sản phẩm */
  productId?: number;

  /** Tiêu đề */
  title?: string;

  /** Nội dung */
  content: string;

  /** Ảnh\/video */
  media?: string[];

  /** Trạng thái */
  status?: EnumUpdateProductPostDtoStatus;
}

export interface CreateCustomerDto {
  /** Id người dùng */
  userId?: number;

  /** Tên */
  fullName: string;

  /** Số điện thoại */
  tel: string;

  /** Id xã phường */
  wardId: number;

  /** Địa chỉ cụ thể */
  address?: string;

  /** Id khách hàng */
  customerId?: number;
}

export interface GetManyCustomerEntityResponseDto {
  /**  */
  data: CustomerEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface CustomerEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Id người dùng */
  userId?: number;

  /** Tên */
  fullName: string;

  /** Số điện thoại */
  tel: string;
}

export interface GetManyNotificationEntityResponseDto {
  /**  */
  data: NotificationEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface NotificationData {
  /** Mã đơn hàng */
  orderId?: number;

  /** Đường dẫn */
  url?: string;
}

export interface NotificationEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Tiêu đề */
  title: string;

  /** Nội dung */
  content?: string;

  /** Dữ liệu đi kèm */
  data?: CombinedDataTypes;

  /** Nhà cung cấp */
  merchantId?: string;

  /** Danh sách User nhận riêng */
  userIds?: string;

  /** Loại */
  type: EnumNotificationEntityType;
}

export interface GetAllCartResponse {
  /** Kho hàng */
  merchantAddress: CombinedMerchantAddressTypes;

  /** Sản phẩm */
  products: CartProductEntity[];

  /** Tổng tiền hàng */
  totalPriceBeforeDiscount: number;

  /** Giảm giá tiền hàng */
  totalDiscountPrice: number;

  /** Tổng tiền thanh toán */
  totalPrice: number;

  /** Giảm giá từ mã ưu đãi */
  totalVoucherDiscount: number;

  /** Tổng hoa hồng sản phẩm */
  totalCommission: number;

  /** Mã giảm giá */
  voucher: CombinedVoucherTypes;
}

export interface ConfigBanner {
  /** Đường dẫn ảnh */
  image?: string;

  /** Đường dẫn khi bấm vào Banner. Nếu có giá trị thì mở luôn đường dẫn này */
  url?: string;

  /** Nội dung khi bấm vào Banner */
  content?: string;

  /** Loại */
  type?: EnumConfigBannerType;
}

export interface BankInfo {
  /** ID Ngân hàng */
  bankId: number;

  /** Tên Chủ tài khoản */
  bankAccount: string;

  /** Số tài khoản */
  bankNumber: string;

  /** Chi nhánh */
  bankBranch?: string;

  /** Email Kế toán */
  accountantEmails?: string[];
}
export enum EnumUserWalletEntityType {
  'consumption' = 'consumption',
  'bonus' = 'bonus'
}
export enum EnumMerchantEntityStatus {
  'inactive' = 'inactive',
  'active' = 'active'
}
export enum EnumUserEntityGender {
  'male' = 'male',
  'female' = 'female',
  'other' = 'other'
}
export enum EnumUserEntityStatus {
  'active' = 'active',
  'inactive' = 'inactive',
  'banned' = 'banned',
  'deleted' = 'deleted'
}
export enum EnumUserEntityValueBonusType {
  'amount' = 'amount',
  'percent' = 'percent'
}
export type CombinedUserTypes = UserEntity;
export type CombinedReferralCommissionTypes = ReferralCommission;
export enum EnumConfigEntityKey {
  'home' = 'home',
  'make_money' = 'make_money',
  'join_community' = 'join_community',
  'flash_deal' = 'flash_deal',
  'top_commission' = 'top_commission',
  'community' = 'community',
  'good_start' = 'good_start',
  'free_course' = 'free_course',
  'current_offers' = 'current_offers',
  'flash_sale' = 'flash_sale',
  'FAQ' = 'FAQ',
  'blog' = 'blog',
  'introduce' = 'introduce',
  'revenue' = 'revenue',
  'revenue_quarter' = 'revenue_quarter',
  'product_quantity' = 'product_quantity',
  'first_order' = 'first_order',
  'register' = 'register',
  'amount_collaborator' = 'amount_collaborator',
  'product' = 'product',
  'shipping_info' = 'shipping_info',
  'bank_info' = 'bank_info'
}
export enum EnumCreateMerchantDtoStatus {
  'inactive' = 'inactive',
  'active' = 'active'
}
export enum EnumRegisterDtoGender {
  'male' = 'male',
  'female' = 'female',
  'other' = 'other'
}
export enum EnumUserTransactionEntityStatus {
  'created' = 'created',
  'completed' = 'completed',
  'cancelled' = 'cancelled'
}
export enum EnumUserTransactionEntityWalletType {
  'consumption' = 'consumption',
  'bonus' = 'bonus'
}
export enum EnumUserTransactionEntityTransactionType {
  'deposit' = 'deposit',
  'withdrawal' = 'withdrawal',
  'bonus' = 'bonus',
  'consumption' = 'consumption'
}
export type CombinedBankTypes = BankEntity;
export enum EnumWithdrawWalletDtoWalletType {
  'consumption' = 'consumption',
  'bonus' = 'bonus'
}
export enum EnumUpdateTransactionDtoStatus {
  'created' = 'created',
  'completed' = 'completed',
  'cancelled' = 'cancelled'
}
export enum EnumProvinceEntityType {
  'city' = 'city',
  'province' = 'province'
}
export enum EnumProvinceEntityPosition {
  'city' = 'city',
  'province' = 'province'
}
export enum EnumDistrictEntityType {
  'district' = 'district',
  'town' = 'town',
  'township' = 'township',
  'city' = 'city'
}
export type CombinedProvinceTypes = ProvinceEntity;
export enum EnumWardEntityType {
  'ward' = 'ward',
  'commune' = 'commune',
  'town' = 'town'
}
export type CombinedDistrictTypes = DistrictEntity;
export type CombinedMerchantTypes = MerchantEntity;
export enum EnumMerchantAddressEntityStatus {
  'inactive' = 'inactive',
  'active' = 'active'
}
export type CombinedWardTypes = WardEntity;
export enum EnumShippingInfoPartner {
  'ghtk' = 'ghtk',
  'ghn' = 'ghn',
  'viettelpost' = 'viettelpost',
  'other' = 'other'
}
export enum EnumShippingInfoStatus {
  'ready_to_pick' = 'ready_to_pick',
  'fail' = 'fail',
  'shipping' = 'shipping',
  'shipped' = 'shipped',
  'return' = 'return',
  'completed' = 'completed',
  'cancelled' = 'cancelled'
}
export type CombinedShipperInfoTypes = ShipperInfo;
export type CombinedMerchantAddressTypes = MerchantAddressEntity;
export type CombinedProductCategoryAttributeTypes = ProductCategoryAttributeEntity;
export enum EnumProductCategoryAttributeEntityControlType {
  'text' = 'text',
  'multi_select' = 'multi_select',
  'select' = 'select'
}
export enum EnumProductCategoryEntityState {
  'active' = 'active',
  'inActive' = 'inActive'
}
export type CombinedParentTypes = ProductCategoryEntity;
export type CombinedProductTypes = ProductEntity;
export enum EnumProductEntityStatus {
  'active' = 'active',
  'inactive' = 'inactive',
  'deleted' = 'deleted'
}
export type CombinedProductCategoryTypes = ProductCategoryEntity;
export type CombinedVariantTypes = ProductVariantEntity;
export enum EnumOrderHistoryEntityStatus {
  'created' = 'created',
  'confirmed' = 'confirmed',
  'processing' = 'processing',
  'pending' = 'pending',
  'onhold' = 'onhold',
  'completed' = 'completed',
  'cancelled' = 'cancelled',
  'merchant_cancelled' = 'merchant_cancelled',
  'refunded' = 'refunded',
  'failed' = 'failed',
  'shipping' = 'shipping',
  'shipped' = 'shipped',
  'ready_to_pick' = 'ready_to_pick',
  'picking' = 'picking',
  'return' = 'return'
}
export enum EnumOrderHistoryEntityType {
  'order' = 'order',
  'shipment' = 'shipment'
}
export enum EnumOrderEntityStatus {
  'created' = 'created',
  'confirmed' = 'confirmed',
  'processing' = 'processing',
  'pending' = 'pending',
  'onhold' = 'onhold',
  'completed' = 'completed',
  'cancelled' = 'cancelled',
  'merchant_cancelled' = 'merchant_cancelled',
  'refunded' = 'refunded',
  'failed' = 'failed',
  'shipping' = 'shipping',
  'shipped' = 'shipped',
  'ready_to_pick' = 'ready_to_pick',
  'picking' = 'picking',
  'return' = 'return'
}
export enum EnumOrderEntityPaymentType {
  'cod' = 'cod',
  'bacs' = 'bacs'
}
export enum EnumOrderEntityPaymentStatus {
  'confirm_deposit' = 'confirm_deposit',
  'deposited' = 'deposited'
}
export type CombinedShippingInfoTypes = ShippingInfo;
export enum EnumCreateSubShipmentDtoPartner {
  'ghtk' = 'ghtk',
  'ghn' = 'ghn',
  'viettelpost' = 'viettelpost',
  'other' = 'other'
}
export type CombinedOtherInfoTypes = ShipperInfo;
export enum EnumShipmentRequestDtoPartner {
  'ghtk' = 'ghtk',
  'ghn' = 'ghn',
  'viettelpost' = 'viettelpost',
  'other' = 'other'
}
export enum EnumChangeStatusDtoStatus {
  'created' = 'created',
  'confirmed' = 'confirmed',
  'processing' = 'processing',
  'pending' = 'pending',
  'onhold' = 'onhold',
  'completed' = 'completed',
  'cancelled' = 'cancelled',
  'merchant_cancelled' = 'merchant_cancelled',
  'refunded' = 'refunded',
  'failed' = 'failed',
  'shipping' = 'shipping',
  'shipped' = 'shipped',
  'ready_to_pick' = 'ready_to_pick',
  'picking' = 'picking',
  'return' = 'return'
}
export enum EnumUpdateOrderDtoStatus {
  'created' = 'created',
  'confirmed' = 'confirmed',
  'processing' = 'processing',
  'pending' = 'pending',
  'onhold' = 'onhold',
  'completed' = 'completed',
  'cancelled' = 'cancelled',
  'merchant_cancelled' = 'merchant_cancelled',
  'refunded' = 'refunded',
  'failed' = 'failed',
  'shipping' = 'shipping',
  'shipped' = 'shipped',
  'ready_to_pick' = 'ready_to_pick',
  'picking' = 'picking',
  'return' = 'return'
}
export enum EnumUpdateOrderDtoPaymentType {
  'cod' = 'cod',
  'bacs' = 'bacs'
}
export enum EnumCreateOrderDtoPaymentType {
  'cod' = 'cod',
  'bacs' = 'bacs'
}
export enum EnumStatusCountResponseDtoStatus {
  'created' = 'created',
  'confirmed' = 'confirmed',
  'processing' = 'processing',
  'pending' = 'pending',
  'onhold' = 'onhold',
  'completed' = 'completed',
  'cancelled' = 'cancelled',
  'merchant_cancelled' = 'merchant_cancelled',
  'refunded' = 'refunded',
  'failed' = 'failed',
  'shipping' = 'shipping',
  'shipped' = 'shipped',
  'ready_to_pick' = 'ready_to_pick',
  'picking' = 'picking',
  'return' = 'return'
}
export enum EnumChangePaymentTypeDtoPaymentType {
  'cod' = 'cod',
  'bacs' = 'bacs'
}
export enum EnumChangeShippingStatusDtoShippingStatus {
  'ready_to_pick' = 'ready_to_pick',
  'fail' = 'fail',
  'shipping' = 'shipping',
  'shipped' = 'shipped',
  'return' = 'return',
  'completed' = 'completed',
  'cancelled' = 'cancelled'
}
export enum EnumVoucherEntityType {
  'shop' = 'shop',
  'product' = 'product'
}
export enum EnumVoucherEntityDiscountType {
  'percentage' = 'percentage',
  'amount' = 'amount'
}
export enum EnumDetailProudctStatus {
  'active' = 'active',
  'inactive' = 'inactive',
  'deleted' = 'deleted'
}
export type CombinedConfigCommissionTypes = ConfigEntity;
export enum EnumProductFilterDtoSortType {
  'top_sell' = 'top_sell',
  'max_price' = 'max_price',
  'min_price' = 'min_price',
  'top_commission' = 'top_commission'
}
export enum EnumCreateProductCategoryDtoState {
  'active' = 'active',
  'inActive' = 'inActive'
}
export type CombinedProductVariantTypes = ProductVariantEntity;
export enum EnumFlashSaleDetailEntityDiscountValueType {
  'amount' = 'amount',
  'percent' = 'percent'
}
export enum EnumFilterRevenueWithPagingField {
  'fullName' = 'fullName',
  'tel' = 'tel'
}
export enum EnumRevenueStatus {
  'created' = 'created',
  'confirmed' = 'confirmed',
  'processing' = 'processing',
  'pending' = 'pending',
  'onhold' = 'onhold',
  'completed' = 'completed',
  'cancelled' = 'cancelled',
  'merchant_cancelled' = 'merchant_cancelled',
  'refunded' = 'refunded',
  'failed' = 'failed',
  'shipping' = 'shipping',
  'shipped' = 'shipped',
  'ready_to_pick' = 'ready_to_pick',
  'picking' = 'picking',
  'return' = 'return'
}
export type CombinedRevenueTypes = Revenue;
export type CombinedRevenueCompleptedTypes = Revenue;
export type CombinedCommissionTypes = Revenue;
export type CombinedCommissionCompleltedTypes = Revenue;
export type CombinedReferralTypes = UserEntity;
export type CombinedConfigRevenueTypes = ConfigEntity;
export type CombinedRevenueCurrentMonthTypes = RevenueStatisticResponse;
export enum EnumCreateVoucherDtoType {
  'shop' = 'shop',
  'product' = 'product'
}
export enum EnumCreateVoucherDtoDiscountType {
  'percentage' = 'percentage',
  'amount' = 'amount'
}
export enum EnumCreateMerchantAddressDtoStatus {
  'inactive' = 'inactive',
  'active' = 'active'
}
export enum EnumRoleEntityType {
  'admin' = 'admin',
  'merchant' = 'merchant',
  'leader' = 'leader',
  'other' = 'other',
  'partner' = 'partner',
  'accountant' = 'accountant',
  'merchant_accountant' = 'merchant_accountant',
  'management_partner' = 'management_partner'
}
export enum EnumProductPostEntityStatus {
  'published' = 'published',
  'draf' = 'draf'
}
export enum EnumCreateProductPostDtoStatus {
  'published' = 'published',
  'draf' = 'draf'
}
export enum EnumUpdateProductPostDtoStatus {
  'published' = 'published',
  'draf' = 'draf'
}
export type CombinedDataTypes = NotificationData;
export enum EnumNotificationEntityType {
  'order' = 'order',
  'offer' = 'offer',
  'bonus' = 'bonus',
  'highlight' = 'highlight'
}
export type CombinedVoucherTypes = VoucherEntity;
export enum EnumConfigBannerType {
  'image' = 'image',
  'video' = 'video'
}

# Production image, copy all the files and run next
FROM node:16.11-alpine AS runner
WORKDIR /app

ENV NODE_ENV development

RUN addgroup -g 1001 -S nodejs
RUN adduser -S nextjs -u 1001
USER nextjs

# You only need to copy next.config.js if you are NOT using the default configuration
# COPY --from=builder /app/next.config.js ./
COPY --chown=nextjs:nodejs ./public ./public
COPY --chown=nextjs:nodejs ./.next ./.next
COPY --chown=nextjs:nodejs ./node_modules ./node_modules
COPY --chown=nextjs:nodejs ./package.json ./package.json
COPY --chown=nextjs:nodejs ./next.config.js ./next.config.js

USER nextjs

EXPOSE 3000

# Next.js collects completely anonymous telemetry data about general usage.
# Learn more here: https://nextjs.org/telemetry
# Uncomment the following line in case you want to disable telemetry.
# ENV NEXT_TELEMETRY_DISABLED 1

CMD ["yarn", "start"]

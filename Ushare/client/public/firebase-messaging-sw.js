// Scripts for firebase and firebase messaging
importScripts('https://www.gstatic.com/firebasejs/8.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.9.0/firebase-analytics.js');
importScripts('https://www.gstatic.com/firebasejs/8.9.0/firebase-messaging.js')


if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('../firebase-messaging-sw.js')
        .then(function (registration) {
            console.log('Registration successful, scope is:', registration.scope);
        }).catch(function (err) {
        console.log('Service worker registration failed, error:', err);
    });
}

function notifyMe() {
    // Let's check whether notification permissions have already been granted
    if (Notification.permission === "granted") {
        // If it's okay let's create a notification
        return true;
    }
    // Otherwise, we need to ask the user for permission
    else if (Notification.permission !== "denied") {
        Notification.requestPermission().then(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                return true;
            } else {
                return false;
            }
        });
    }
    // At last, if the user has denied notifications, and you
    // want to be respectful there is no need to bother them any more.
    return false;
}

notifyMe();

// Initialize the Firebase app in the service worker by passing the generated config
const firebaseConfig = {
    apiKey: "AIzaSyArgUgqSWe0_MOZr9gAA0yhXu0Tv5kVcRQ",
    authDomain: "ushare-dev.firebaseapp.com",
    projectId: "ushare-dev",
    storageBucket: "ushare-dev.appspot.com",
    messagingSenderId: "543626051699",
    appId: "1:543626051699:web:c078e45dee9bab66ba2a06",
    measurementId: "G-JHBGZ505S9"
};
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    if (payload.notification) {
        const notificationTitle = payload.notification.title;
        const notificationOptions = {
            body: payload.notification.body,
            icon: '/logo.png',
            vibrate: [200, 100, 200, 100, 200, 100, 200],
        };
        return self.registration.showNotification(notificationTitle, notificationOptions);
    }
});

self.addEventListener('notificationclick', function (event) {
    console.log("notificationclick", event);
    event.notification.close();
    event.waitUntil(self.clients.openWindow("/dashboard"));
});


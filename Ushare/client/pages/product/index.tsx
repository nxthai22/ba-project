import Content from 'components/layout/AdminLayout/Content'
import DataTable from 'components/common/DataTable'
import { Button, Card, Col, Form, Input, Row, Select, Space } from 'antd'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import React, { FC, useEffect, useState } from 'react'
import { ServiceParams } from 'utils/interfaces'
import { ColumnsType } from 'antd/es/table'
import {
  EnumProductEntityStatus,
  ProductCategoriesService,
  ProductCategoryEntity,
  ProductEntity,
  ProductsService,
} from 'services'
import { alertError, filterOption, formatCurrency, hasPermission } from 'utils'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { authState, countDataTable, loadingState } from 'recoil/Atoms'
import { useRouter } from 'next/router'

interface Inputs {
  name?: string
  productCateId?: string
}

const Index: FC = () => {
  const router = useRouter()
  const title = 'Danh sách sản phẩm'
  const user = useRecoilValue(authState)
  const [productCategoies, setProductCategoies] =
    useState<ProductCategoryEntity[]>()
  const setIsLoading = useSetRecoilState(loadingState)
  const countDatatable = useRecoilValue(countDataTable)
  const { control, handleSubmit, reset } = useForm<Inputs>({
    defaultValues: {
      name: '',
      productCateId: '',
    },
  })
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['createdAt,DESC'],
    join: ['productCategory', 'merchant'],
    filter: [user.merchantId && `merchantId||eq||${user.merchantId}`],
    userId: Number(user.sub),
  })
  const columns: ColumnsType<ProductEntity> = [
    {
      dataIndex: 'name',
      title: 'Tên Sản phẩm',
      width: 200,
      fixed: 'left',
      render: (value, record) => {
        return (
          <div className={'flex'}>
            <div className={'flex-none w-15 mr-3'}>
              {record?.images && record?.images.length > 0 && (
                <img
                  src={record?.images?.[0]}
                  width={'60px'}
                  alt={record.name}
                />
              )}
            </div>
            <div className={'flex-grow'}>{record.name}</div>
          </div>
        )
      },
    },
    {
      dataIndex: 'productCategory',
      title: 'Danh mục',
      render: (value) => value?.name,
    },
    {
      dataIndex: 'priceBeforeDiscount',
      title: 'Giá gốc',
      render: (value) => formatCurrency(value),
    },
    {
      dataIndex: 'price',
      title: 'Giá bán',
      render: (value) => formatCurrency(value),
    },
    {
      dataIndex: 'commission',
      title: 'Hoa hồng',
      render: (value) => formatCurrency(value),
    },
    {
      dataIndex: 'status',
      title: 'Trạng thái',
      render: (value) => {
        switch (value) {
          case EnumProductEntityStatus.active:
            return 'Đang kinh doanh'
          case EnumProductEntityStatus.inactive:
            return 'Tạm hết hàng'
          case EnumProductEntityStatus.deleted:
            return 'Ngừng kinh doanh'
        }
      },
    },
  ]
  if ([1].includes(user?.roleId)) {
    columns.push({
      dataIndex: 'merchant',
      title: 'Đối tác',
      render: (value) => value?.name,
    })
  }

  useEffect(() => {
    setIsLoading(true)
    ProductCategoriesService.getManyBase()
      .then((response) => {
        setProductCategoies(response.data)
        setIsLoading(false)
      })
      .catch((e) => {
        alertError(e)
        setIsLoading(false)
      })
  }, [])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const filter = []
    if (data?.name) filter.push(`name||$contL||${data?.name}`)
    if (data?.productCateId)
      filter.push(`productCategoryId||eq||${data?.productCateId}`)
    if (user?.merchantId) filter.push(`merchantId||eq||${user.merchantId}`)
    setTableServiceParams((prevState) => ({ ...prevState, filter: filter }))
  }

  const onReset = () => {
    reset()
    setTableServiceParams({
      sort: ['createdAt,DESC'],
      join: ['productCategory', 'merchant'],
      filter: [user.merchantId && `merchantId||eq||${user.merchantId}`],
      userId: Number(user.sub),
    })
  }
  return (
    <Content
      title={title}
      endButtons={[
        {
          text: 'Thêm mới',
          href: '/product/category',
          type: 'primary',
          visible:
            user.merchantId &&
            hasPermission(user, [
              'products_createOneBase',
              'products_updateOneBase',
            ]),
        },
      ]}
    >
      <Card>
        <Form
          autoComplete={'off'}
          onFinish={handleSubmit(onSubmit)}
          onReset={onReset}
          colon={false}
        >
          <Row gutter={[16, 16]}>
            <Col xs={24} sm={24} md={24} lg={8}>
              <Form.Item label={'Nhập Tên sản phẩm'}>
                <Controller
                  control={control}
                  name={'name'}
                  render={({ field }) => (
                    <Input {...field} placeholder={'Nhập Tên sản phẩm'} />
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={8}>
              <Form.Item label={'Danh mục'}>
                <Controller
                  name={'productCateId'}
                  control={control}
                  render={({ field }) => (
                    <Select
                      showSearch
                      filterOption={filterOption}
                      placeholder="Vui lòng chọn Danh mục"
                      {...field}
                    >
                      {' '}
                      <Select.Option value={''}>Tất cả</Select.Option>
                      {productCategoies &&
                        productCategoies.map((c) => (
                          <Select.Option key={`category_${c.id}`} value={c.id}>
                            {c.name}
                          </Select.Option>
                        ))}
                    </Select>
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={8} className={'text-right'}>
              <Form.Item>
                <Space>
                  <Button htmlType={'reset'}>Đặt lại</Button>
                  <Button type={'primary'} htmlType={'submit'}>
                    Tìm kiếm
                  </Button>
                </Space>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>
      <Card
        title={<span className={'text-2xl'}>{countDatatable} Sản phẩm</span>}
      >
        <DataTable
          service={ProductsService.getManyBase}
          serviceParams={tableServiceParams}
          deleteService={ProductsService.deleteOneBase}
          columns={columns}
          action={[
            ...(user?.roleId === 1
              ? [
                  {
                    type: 'view',
                    onClick: (id) => router.push(`/product/edit/${id}`),
                  },
                ]
              : hasPermission(user, [
                  'products_createOneBase',
                  'products_updateOneBase',
                ]) && [
                  {
                    type: 'edit',
                    onClick: (id) => router.push(`/product/edit/${id}`),
                  },
                ]),
          ]}
          path={'/product'}
        />
      </Card>
    </Content>
  )
}
export default Index

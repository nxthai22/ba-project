import {
  CloseOutlined,
  DeleteOutlined,
  PlusCircleOutlined,
} from '@ant-design/icons'
import * as yup from 'yup'
import React, { FC, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import {
  Controller,
  FormProvider,
  SubmitHandler,
  useFieldArray,
  useForm,
  useWatch,
} from 'react-hook-form'
import {
  Button,
  Card,
  Cascader,
  Col,
  Form,
  Input,
  Radio,
  Row,
  Select,
} from 'antd'
import {
  alertError,
  filterOption,
  flattenCategoryTree,
  modifyEntity,
} from 'utils'
import Content from 'components/layout/AdminLayout/Content'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { authState, loadingState } from 'recoil/Atoms'
import {
  CreateProductDto,
  CreateProductVariantDto,
  EnumCreateProductDtoStatus,
  EnumMerchantAddressEntityStatus,
  EnumProductCategoryAttributeEntityControlType,
  EnumRoleEntityType,
  MerchantAddressEntity,
  MerchantAddressesService,
  ProductCategoriesService,
  ProductCategoryAttributeEntity,
  ProductCategoryEntity,
  ProductsService,
} from 'services'
import FormItem from 'components/common/FormItem'
import InputNumber from 'components/common/InputNumber'
import UploadMedia from 'components/common/UploadMedia'
import VariantTableBody from 'components/common/Variant/TableBody'
import { yupResolver } from '@hookform/resolvers/yup'
import RichTextEditor from 'components/common/RichTextEditor'
import { groupBy, sortBy } from 'lodash'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import UploadVideo from '../../../components/common/UploadVideo'

interface Inputs {
  product: CreateProductDto
  price: number
  quantity: number
  sku: string
  variants: any[]
}

yup.addMethod(yup.array, 'unique', function (message, mapper = (a) => a) {
  return this.test('unique', message, function (list) {
    return list.length === new Set(list.map(mapper)).size
  })
})

const schema = yup.object().shape({
  product: yup.object().shape({
    name: yup
      .string()
      .min(10, 'Tên sản phẩm của bạn quá ngắn. Vui lòng nhập ít nhất 10 ký tự.')
      .max(120)
      .required('Không được để ô trống'),
    productCategoryId: yup
      .number()
      .positive()
      .integer()
      .required('Danh mục không được để trống')
      .typeError('Danh mục không được để trống'),
    description: yup
      .string()
      .test('description', 'Mô tả không được để trống', (value) => {
        const regex = /(<([^>]+)>)/gi
        return value && !!value.replace(regex, '').length
      }),
    priceBeforeDiscount: yup
      .number()
      .positive()
      .integer()
      .min(4, 'Giá niêm yết phải ít nhất 1.000')
      .required('Vui lòng nhập giá niêm yết')
      .typeError('Giá niêm yết phải ít nhất 1.000.'),
    commissionPercent: yup
      .number()
      .positive()
      .integer()
      .min(0, 'Hoa hồng phải lớn hơn 0%')
      .max(100, 'Hoa hồng phải nhỏ hơn 100%')
      .required('Vui lòng nhập hoa hồng')
      .typeError('Hoa hồng phải lớn hơn 0%'),
    price: yup.number().when('groupType', {
      is: (value) => value.length === 0,
      then: yup
        .number()
        // .positive()
        .integer()
        // .min(1000, 'Giá trị thấp nhất nên là 1.000đ')
        // .typeError('Giá trị thấp nhất nên là 1.000đ')
        .lessThan(
          yup.ref('priceBeforeDiscount'),
          'Giá bán phải nhỏ hơn hoặc bằng giá niêm yết'
        ),
    }),
    stock: yup.array().when('groupType', {
      is: (value) => value.length === 0,
      then: yup
        .array()
        .of(
          yup.object().shape({
            merchantAddressId: yup.number().required('Vui long chọn 1 kho'),
            quantity: yup
              .number()
              .positive()
              .integer()
              .min(1, 'Số lượng trong kho phải lớn hơn 0')
              .required('Vui long Nhập số lượng'),
          })
        )
        .unique('Kho không được trùng nhau', (stock) => stock.merchantAddressId)
        .min(1, 'Phải chọn ít nhất 1 kho có hàng'),
    }),
    groupType: yup
      .array()
      .of(
        yup.object().shape({
          name: yup.string().required('Vui lòng nhập tên nhóm'),
          value: yup.array().of(
            yup.object().shape({
              field: yup.string().required('Vui lòng nhập phân loại hàng'),
            })
          ),
        })
      )
      .nullable(),
    weight: yup
      .number()
      .required('Vui lòng nhập Cân nặng')
      .typeError('Chỉ được nhập kiểu số.'),
    height: yup.number().typeError('Chỉ được nhập kiểu số.'),
    length: yup.number().typeError('Chỉ được nhập kiểu số.'),
    width: yup.number().typeError('Chỉ được nhập kiểu số.'),
  }),
  // variants: yup.array().when('product.groupType', (e, schema) => {
  //   if (e.length === 1) {
  //     return yup.array().of(
  //       yup.object().shape({
  //         price: yup
  //           .number()
  //           .positive()
  //           .integer()
  //           .test('passwords-match', 'Passwords must match', function (value) {
  //             console.log(this.options.context.dateTo)
  //             return this.parent.product.priceBeforeDiscount > value
  //           }),
  //         quantity: yup.number().positive().integer(),
  //       })
  //     )
  //   } else if (e.length === 2) {
  //     return yup.array().of(
  //       yup.object().shape({
  //         price: yup
  //           .number()
  //           .positive()
  //           .integer()
  //           .min(1000, 'Giá trị thấp nhất nên là 1.000đ')
  //           .max(
  //             yup.ref('price.priceBeforeDiscount'),
  //             'Giá bán phải nhỏ hơn hoặc bằng giá niêm yết'
  //           ),
  //         quantity: yup.number().positive().integer(),
  //       })
  //     )
  //   } else {
  //     return yup.array().nullable()
  //   }
  // }),
})
const ProductDetail: FC = () => {
  const router = useRouter()
  const { productName, categoryId, id } = router.query
  const user = useRecoilValue(authState)
  const setIsLoading = useSetRecoilState(loadingState)
  const [title, setTitle] = useState('Thêm 1 sản phẩm mới')
  const [fileList, setFileList] = useState<string[]>([])
  const [thumbnail, setThumbnail] = useState<string[]>([])
  const [videoList, setVideoList] = useState<UploadFile[]>([])
  const [productCategoryAttributes, setProductCategoryAttributes] = useState<
    ProductCategoryAttributeEntity[]
  >([])
  const [productCategories, setProductCategories] = useState<
    ProductCategoryEntity[]
  >([])
  const [productCategory, setProductCategory] =
    useState<ProductCategoryEntity[]>(null)
  const [merchantAddress, setMerchantAddress] = useState<
    MerchantAddressEntity[]
  >([])
  const [variantTableHeads, setVariantTableHeads] = useState([
    'Tên',
    'SKU',
    'Giá',
    'Kho',
    'Số lượng',
  ])
  const formMethods = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
    defaultValues: {
      product: {
        status: EnumCreateProductDtoStatus.active,
      },
    },
  })
  const {
    control,
    setValue,
    handleSubmit,
    getValues,
    reset,
    formState: { errors },
  } = formMethods

  const {
    fields: stockFields,
    append: stockAppend,
    remove: stockRemove,
  } = useFieldArray({
    control,
    name: 'product.stock',
  })
  const {
    fields: groupTypeFields,
    append: groupTypeAppend,
    remove: groupTypeRemove,
  } = useFieldArray({
    control,
    name: 'product.groupType',
  })
  const { replace: productAttributesReplace } = useFieldArray({
    control,
    name: 'product.productAttributes',
  })

  const watchProductName = useWatch({
    control,
    name: 'product.name',
  })
  const watchProductGroupType = useWatch({
    control,
    name: 'product.groupType',
  })

  const watchPrice = useWatch({
    control,
    name: 'price',
  })

  const watchQuantity = useWatch({
    control,
    name: 'quantity',
  })

  const watchProductCategoryId = useWatch({
    control,
    name: 'product.productCategoryId',
  })

  useEffect(() => {
    setIsLoading(true)
    if (productName && categoryId) {
      Promise.all([
        ProductCategoriesService.getOneBase({
          id: Number(categoryId),
          join: ['parent'],
        }),
        MerchantAddressesService.getManyBase({
          sort: ['name,ASC'],
          filter: [
            `status||eq||${EnumMerchantAddressEntityStatus.active}`,
            `merchantId||eq||${Number(user.merchantId)}`,
          ],
          limit: 1000,
        }),
        ProductCategoriesService.getManyBase({
          limit: 100,
          filter: [`parentId||$isnull`],
          join: ['childrens'],
        }),
      ])
        .then(
          ([
            productCategoryDetailResponse,
            merchantAddressResponse,
            categoriesResponse,
          ]) => {
            setIsLoading(false)
            setValue('product.name', String(productName))
            setValue('product.productCategoryId', Number(categoryId))
            setProductCategories(categoriesResponse.data)
            // setProductCategory(productCategoryDetailResponse)
            setProductCategory(
              flattenCategoryTree(productCategoryDetailResponse, [])
                .reverse()
                .map((category) => category.id)
            )
            setMerchantAddress(merchantAddressResponse.data)
          }
        )
        .catch((e) => {
          setIsLoading(false)
          alertError(e)
        })
    }
  }, [productName, categoryId])

  useEffect(() => {
    setIsLoading(true)
    if (id && Number(id) && Number(id) > 0) {
      setTitle('Cập nhật sản phẩm')
      setValue('product.name', String(productName))
      const productId = Number(id)
      Promise.all([
        productId &&
          ProductsService.getOneBase({
            id: productId,
            join: [
              `productCategory`,
              'productCategory.productCategoryAttributes',
              'productCategory.productCategoryAttributes.productCategoryAttributeValues',
              'productAttributes',
              'productAttributes.productCategoryAttribute',
              'variants',
              'productCategory.parent',
            ],
          }),
        ProductCategoriesService.getManyBase({
          limit: 100,
          filter: [`parentId||$isnull`],
          join: ['childrens'],
        }),
      ])
        .then(([productResponse, categoriesResponse]) => {
          const categoryAttributes =
            productResponse.productCategory?.productCategoryAttributes?.reduce(
              (arr, curr) => {
                if (curr.productCategoryAttributeValues?.length > 0)
                  arr.push(
                    ...curr.productCategoryAttributeValues.map(
                      (value) => value.id
                    )
                  )
                return arr
              },
              []
            )
          productResponse.productAttributes?.map((prodAttribute) => {
            if (
              !prodAttribute.productCategoryAttributeValueIds.every((att) =>
                categoryAttributes.includes(att)
              )
            ) {
              prodAttribute.productCategoryAttributeValueIds = []
            }
            if (
              prodAttribute.productCategoryAttribute?.controlType ===
              EnumProductCategoryAttributeEntityControlType.text
            ) {
              prodAttribute.name =
                prodAttribute.productCategoryAttributeValues?.[0]?.name
            }
          })
          Promise.all([
            MerchantAddressesService.getManyBase({
              sort: ['name,ASC'],
              filter: [
                `status||eq||${EnumMerchantAddressEntityStatus.active}`,
                `merchantId||eq||${Number(productResponse.merchantId)}`,
              ],
              limit: 100,
            }),
          ])
            .then(([merchantAddressResponse]) => {
              setIsLoading(false)
              reset({
                product: {
                  ...productResponse,
                  productAttributes: sortBy(
                    productResponse.productAttributes,
                    'id'
                  ),
                },
              })
              setProductCategories(categoriesResponse.data)
              setMerchantAddress(merchantAddressResponse.data)
              if (
                productResponse?.productCategory?.id &&
                categoriesResponse?.data?.find(
                  (category) =>
                    category.id === productResponse.productCategory.id
                )
              ) {
                setProductCategory(
                  flattenCategoryTree(productResponse.productCategory, [])
                    .reverse()
                    .map((category) => category.id)
                )
              }
              setFileList(productResponse.images)
              setThumbnail(productResponse.thumbnail)
              if (productResponse.videoProductUrl) {
                setVideoList([
                  {
                    url: productResponse.videoProductUrl,
                    thumbnail: productResponse.videoProductThumbnailUrl,
                  },
                ])
              }
              const variants = productResponse?.variants
              const groupTypes = productResponse?.groupType
              if (groupTypes?.length === 1) {
                setValue(`variants`, variants)
              } else {
                let variantIndex = 0
                groupTypes?.[0]?.value?.map(
                  (productGroupTypeItem, productGroupTypeIndex) => {
                    groupTypes?.[1]?.value?.map(
                      (
                        productSecondGroupTypeItem,
                        productSecondGroupTypeIndex
                      ) => {
                        variantIndex += 1
                        setValue(
                          `variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.name`,
                          variants[variantIndex - 1]?.name
                        )
                        const variant = variants?.find(
                          (variant) =>
                            variant.groupTypeIndex === productGroupTypeIndex &&
                            variant.valueTypeIndex ===
                              productSecondGroupTypeIndex
                        )
                        if (variant) {
                          setValue(
                            `variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.id`,
                            variant.id
                          )
                          setValue(
                            `variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.price`,
                            variant?.price
                          )
                          setValue(
                            `variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.SKU`,
                            variant?.SKU
                          )
                          setValue(
                            `variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.stock`,
                            variant?.stock
                          )
                        } else {
                          setValue(
                            `variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.price`,
                            variants[variantIndex - 1]?.price
                          )
                          setValue(
                            `variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.SKU`,
                            variants[variantIndex - 1]?.SKU
                          )
                          setValue(
                            `variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.stock`,
                            variants[variantIndex - 1]?.stock
                          )
                        }
                        return
                      }
                    )
                  }
                )
              }
            })
            .catch((error) => {
              alertError(error)
              setIsLoading(false)
            })
        })
        .catch((error) => {
          alertError(error)
          setIsLoading(false)
        })
    }
  }, [id])

  useEffect(() => {
    const tmpArrColunm = []
    if (watchProductGroupType) {
      watchProductGroupType?.map((watchProductGroupTypeItem) => {
        tmpArrColunm.push(watchProductGroupTypeItem.name || 'Tên')
      })
      setVariantTableHeads(
        tmpArrColunm.concat(['SKU', 'Giá', 'Kho', 'Số lượng'])
      )
    } else {
      setVariantTableHeads(['Tên', 'SKU', 'Giá', 'Kho', 'Số lượng'])
    }
  }, [watchProductGroupType])

  useEffect(() => {
    if (watchProductCategoryId)
      ProductCategoriesService.getOneBase({
        id: Number(watchProductCategoryId),
      })
        .then((productCategory) => {
          setProductCategoryAttributes(
            productCategory?.productCategoryAttributes
          )
        })
        .catch()
    else setProductCategoryAttributes(null)
  }, [watchProductCategoryId])

  const applyAll = () => {
    const groupTypes = getValues('product.groupType')
    const price = getValues('price')
    const quantity = getValues('quantity')
    if (groupTypes.length === 1) {
      groupTypes[0].value.map((groupType, groupIndex) => {
        if (price != 0) setValue(`variants.${groupIndex}.price`, price)
        if (quantity != 0)
          merchantAddress.map((address, addressIndex) => {
            setValue(
              `variants.${groupIndex}.stock.${addressIndex}.quantity`,
              quantity
            )
          })
      })
    } else {
      groupTypes.map(() => {
        groupTypes[0].value.map((value, valueIndex) => {
          groupTypes[1].value.map((secondValue, secondValueIndex) => {
            if (price != 0)
              setValue(
                `variants.${valueIndex}.${secondValueIndex}.price`,
                price
              )
            if (quantity != 0)
              merchantAddress.map((address, addressIndex) => {
                setValue(
                  `variants.${valueIndex}.${secondValueIndex}.stock.${addressIndex}.quantity`,
                  quantity
                )
              })
          })
        })
      })
    }
  }

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    if (data.product && user?.roleId !== 1) {
      if (fileList && fileList.length > 0) {
        data.product.images = fileList
      } else {
        data.product.images = null
        throw alertError(`Sản phẩm cần tối thiểu 1 ảnh`)
      }
      if (thumbnail && thumbnail?.length > 0) data.product.thumbnail = thumbnail
      else throw alertError(`Sản phẩm cần tối thiểu 1 ảnh bìa`)
      if (videoList && videoList.length > 0) {
        data.product.videoProductUrl = videoList[0].url
        data.product.videoProductThumbnailUrl = videoList[0].thumbnail
      } else {
        data.product.videoProductUrl = null
        data.product.videoProductThumbnailUrl = null
      }
      data.product.price = Number(data.product.price)
      data.product.commission = Number(data.product.commission)
      if (!isNaN(Number(categoryId)))
        data.product.productCategoryId = Number(categoryId)
      delete data.product.productCategory
      if (data?.product?.groupType.length > 0) {
        if (data?.product?.groupType.length > 1) {
          const tmpValue: CreateProductVariantDto[] = []
          data?.variants?.map((variant) => {
            Object.keys(variant).map((key) => {
              if (!isNaN(parseInt(key))) {
                tmpValue.push(variant[key])
              }
            })
          })
          data.product.variants = tmpValue
        } else {
          data.product.variants = data?.variants?.map((variant) => {
            const tmpStock = []
            const stockGrouped = groupBy(variant.stock, 'merchantAddressId')
            Object.keys(stockGrouped).map((merchantAddressId) => {
              const stocks = stockGrouped[merchantAddressId]
              if (
                merchantAddress.find(
                  (address) => address.id === stocks[0].merchantAddressId
                )
              ) {
                tmpStock.push({
                  quantity: stocks[0].quantity,
                  merchantAddressId: stocks[0].merchantAddressId,
                })
              }
            })
            variant.stock = tmpStock
            return variant
          })
        }
        data.product.variants?.map((variant) => {
          variant.SKU = variant.SKU?.trim()
          if (variant.SKU?.includes(' '))
            throw alertError(
              `Mã sku ${variant.SKU} không được chứa khoảng trắng`
            )
        })
      } else {
        data.product.variants = null
      }
      if (
        data.product?.SKU?.length > 0 &&
        data.product?.SKU?.trim()?.includes(' ')
      )
        return alertError(
          `Mã sku ${data.product?.SKU} không được chứa khoảng trắng`
        )
      data?.product?.productAttributes?.map((attr) => {
        if (typeof attr.productCategoryAttributeValueIds === 'number') {
          attr.productCategoryAttributeValueIds = [
            attr.productCategoryAttributeValueIds,
          ]
        }
      })
      modifyEntity(ProductsService, data.product, title, () => {
        return router.push(`/product`)
      }).then()
    } else alertError('Có lỗi xảy ra!')
  }

  return (
    <Content title={title} onBack={() => router.push('/product')}>
      <FormProvider {...formMethods}>
        <Form
          labelCol={{
            md: { span: 4 },
            sm: {
              span: 12,
            },
          }}
        >
          <Card title={<div className={'text-xl'}>Thông tin cơ bản</div>}>
            <div className={'p-4'}>
              <Form.Item label={'Ảnh bìa sản phẩm'} required={true} er>
                <Row gutter={[40, 0]}>
                  <Col xs={24} sm={24} md={24} xl={24} lg={24}>
                    <UploadMedia
                      multiple={false}
                      files={thumbnail}
                      onImageChange={setThumbnail}
                      disableUpload={user?.type === EnumRoleEntityType.admin}
                    />
                  </Col>
                </Row>
              </Form.Item>
              <Form.Item label={'Ảnh sản phẩm'} required={true} er>
                <Row gutter={[40, 0]}>
                  <Col xs={24} sm={24} md={24} xl={24} lg={24}>
                    <UploadMedia
                      multiple={true}
                      files={fileList}
                      onImageChange={setFileList}
                      disableUpload={user?.type === EnumRoleEntityType.admin}
                    />
                  </Col>
                </Row>
              </Form.Item>
              <Form.Item label={'Video sản phẩm'}>
                <Row gutter={[40, 0]}>
                  <Col xs={24} sm={24} md={24} xl={24} lg={24}>
                    <UploadVideo
                      multiple={false}
                      files={videoList}
                      onVideoChange={setVideoList}
                      disableUpload={user?.type === EnumRoleEntityType.admin}
                    />
                  </Col>
                </Row>
              </Form.Item>
              <Form.Item
                label={'Tên sản phẩm'}
                required={true}
                validateStatus={errors?.product?.name && 'error'}
                help={errors?.product?.name?.message}
              >
                <Controller
                  control={control}
                  name="product.name"
                  render={({ field }) => (
                    <Input
                      {...field}
                      minLength={10}
                      maxLength={120}
                      addonAfter={`${watchProductName?.length}/120`}
                      disabled={user?.roleId === 1}
                    />
                  )}
                />
              </Form.Item>
              <Form.Item
                label={'Mô tả sản phẩm'}
                required={true}
                validateStatus={errors.product?.description && 'error'}
                help={errors.product?.description?.message}
              >
                <Controller
                  control={control}
                  name="product.description"
                  render={({ field }) => <RichTextEditor {...field} />}
                />
              </Form.Item>
              <Form.Item
                label={'Đường dẫn Video'}
                validateStatus={errors.product?.videoUrl && 'error'}
                help={errors.product?.videoUrl?.message}
                extra="Chỉ hỗ trợ link Youtube"
              >
                <Controller
                  control={control}
                  name="product.videoUrl"
                  render={({ field }) => (
                    <Input {...field} disabled={user?.roleId === 1} />
                  )}
                />
              </Form.Item>
              <Form.Item
                label={'Danh mục'}
                required={true}
                validateStatus={errors.product?.productCategoryId && 'error'}
                help={errors.product?.productCategoryId?.message}
              >
                {productCategories && (
                  <Controller
                    control={control}
                    name="product.productCategoryId"
                    render={({ field }) => (
                      <Cascader
                        {...field}
                        disabled={user?.roleId === 1}
                        placeholder="Chọn danh mục"
                        fieldNames={{
                          label: 'name',
                          value: 'id',
                          children: 'childrens',
                        }}
                        value={productCategory}
                        onChange={(value) => {
                          if (value) {
                            setValue(
                              'product.productCategoryId',
                              value[value?.length - 1]
                            )
                          } else {
                            setValue('product.productCategoryId', null)
                          }
                          productAttributesReplace([
                            {
                              productCategoryAttributeValueIds: [],
                            },
                          ])
                          setProductCategory(value)
                        }}
                        options={productCategories}
                      />
                    )}
                  />
                )}
              </Form.Item>
              <Form.Item label={'Trạng thái kinh doanh'} required={true}>
                <Controller
                  control={control}
                  name="product.status"
                  render={({ field }) => (
                    <Radio.Group {...field}>
                      <Radio value={EnumCreateProductDtoStatus.active}>
                        Đang kinh doanh
                      </Radio>
                      <Radio value={EnumCreateProductDtoStatus.deleted}>
                        Ngừng kinh doanh
                      </Radio>
                      <Radio value={EnumCreateProductDtoStatus.inactive}>
                        Tạm hết hàng
                      </Radio>
                    </Radio.Group>
                  )}
                />
              </Form.Item>
            </div>
          </Card>
          {productCategory && productCategoryAttributes?.length > 0 && (
            <Card
              title={
                <>
                  <div className={'text-xl'}>Thuộc tính sản phẩm</div>
                </>
              }
            >
              <Row gutter={[8, 8]} className={'p-4'}>
                {productCategoryAttributes?.map((item, index) => {
                  setValue(
                    `product.productAttributes.${index}.productCategoryAttributeId`,
                    item.id
                  )
                  return (
                    <Col
                      key={`category-id-${index}`}
                      xs={24}
                      sm={24}
                      md={24}
                      xl={12}
                      lg={12}
                    >
                      <FormItem label={item?.name}>
                        <Input.Group compact>
                          {item.controlType ===
                            EnumProductCategoryAttributeEntityControlType.text && (
                            <Controller
                              control={control}
                              name={`product.productAttributes.${index}.name`}
                              render={({ field }) => (
                                <Input
                                  {...field}
                                  disabled={user?.roleId === 1}
                                  placeholder={`Nhập tên ${item?.name}`}
                                  className={`${
                                    item?.units?.length > 0 ? 'w-8/12' : ''
                                  }`}
                                />
                              )}
                            />
                          )}
                          {(item.controlType ===
                            EnumProductCategoryAttributeEntityControlType.select ||
                            item.controlType ===
                              EnumProductCategoryAttributeEntityControlType.multi_select) && (
                            <Controller
                              control={control}
                              name={`product.productAttributes.${index}.productCategoryAttributeValueIds`}
                              render={({ field }) => (
                                <Select
                                  {...field}
                                  disabled={user?.roleId === 1}
                                  filterOption={filterOption}
                                  showSearch
                                  allowClear
                                  placeholder={'Chọn giá trị'}
                                  mode={
                                    item.controlType ===
                                    EnumProductCategoryAttributeEntityControlType.multi_select
                                      ? 'multiple'
                                      : null
                                  }
                                  className={`${
                                    item?.units?.length > 0
                                      ? 'w-8/12'
                                      : 'w-full'
                                  }`}
                                >
                                  {item?.productCategoryAttributeValues.map(
                                    (attrValue) => {
                                      // if (index === 0) console.log(attrValue)
                                      return (
                                        attrValue.name && (
                                          <Select.Option
                                            value={attrValue.id}
                                            key={`productAttributes-${attrValue.id}`}
                                          >
                                            {attrValue.name}
                                          </Select.Option>
                                        )
                                      )
                                    }
                                  )}
                                </Select>
                              )}
                            />
                          )}
                          {item?.units?.length > 0 && (
                            <Controller
                              control={control}
                              name={`product.productAttributes.${index}.productCategoryAttributeUnitIndex`}
                              render={({ field }) => (
                                <Select
                                  filterOption={filterOption}
                                  showSearch
                                  allowClear
                                  {...field}
                                  disabled={user?.roleId === 1}
                                  placeholder={'Chọn đơn vị'}
                                  className={'w-4/12'}
                                >
                                  {item?.units.map(
                                    (attrValue, attrValueIndex) => (
                                      <Select.Option
                                        value={attrValueIndex}
                                        key={`productCategoryAttributeUnitIndex-${attrValueIndex}`}
                                      >
                                        {attrValue}
                                      </Select.Option>
                                    )
                                  )}
                                </Select>
                              )}
                            />
                          )}
                        </Input.Group>
                      </FormItem>
                    </Col>
                  )
                })}
              </Row>
            </Card>
          )}
          <Card title={<div className={'text-xl'}>Thông tin bán hàng</div>}>
            <div className={'p-4'}>
              <Form.Item label={'Mã SKU'}>
                <Controller
                  control={control}
                  name={`product.SKU`}
                  render={({ field }) => (
                    <Input
                      {...field}
                      disabled={user?.roleId === 1}
                      className={'w-full'}
                    />
                  )}
                />
              </Form.Item>
              <Form.Item
                label={'Giá niêm yết'}
                required={true}
                validateStatus={errors.product?.priceBeforeDiscount && 'error'}
                help={errors.product?.priceBeforeDiscount?.message}
              >
                <Controller
                  control={control}
                  name={`product.priceBeforeDiscount`}
                  render={({ field }) => (
                    <InputNumber
                      {...field}
                      disabled={user?.roleId === 1}
                      addonAfter="đ"
                      className={'w-full'}
                    />
                  )}
                />
              </Form.Item>
              <Form.Item
                label={'Hoa hồng'}
                required={true}
                validateStatus={errors.product?.commissionPercent && 'error'}
                help={errors.product?.commissionPercent?.message}
              >
                <Controller
                  control={control}
                  name={`product.commissionPercent`}
                  render={({ field }) => (
                    <InputNumber
                      {...field}
                      disabled={user?.roleId === 1}
                      addonAfter="%"
                      className={'w-full'}
                      min={0}
                      max={100}
                    />
                  )}
                />
              </Form.Item>
              {groupTypeFields.length === 0 ? (
                <Form.Item label="Phân loại hàng">
                  <Button
                    disabled={user?.roleId === 1}
                    type="dashed"
                    className={'w-full'}
                    onClick={() => groupTypeAppend({ name: '', value: [''] })}
                    style={{
                      color: '#5863dc',
                      borderColor: '#5863dc',
                    }}
                    icon={<PlusCircleOutlined />}
                  >
                    Thêm phân loại hàng
                  </Button>
                </Form.Item>
              ) : (
                <>
                  {groupTypeFields.map((groupTypeField, groupTypeIndex) => (
                    <Form.Item
                      label={`Nhóm phân loại ${groupTypeIndex + 1}`}
                      key={groupTypeField.id}
                    >
                      <div
                        style={{
                          backgroundColor: '#fafafa',
                        }}
                        className="relative p-4"
                      >
                        <CloseOutlined
                          style={{
                            top: '10px',
                            right: '10px',
                            color: '#c4c4c4',
                          }}
                          className="absolute cursor-pointer"
                          onClick={() => groupTypeRemove(groupTypeIndex)}
                        />
                        <Form.Item
                          label="Tên nhóm"
                          labelCol={{ span: 3 }}
                          wrapperCol={{ span: 21 }}
                          validateStatus={
                            errors.product?.groupType?.[`${groupTypeIndex}`]
                              ?.name && 'error'
                          }
                          help={
                            errors.product?.groupType?.[`${groupTypeIndex}`]
                              ?.name?.message
                          }
                        >
                          <Controller
                            control={control}
                            name={`product.groupType.${groupTypeIndex}.name`}
                            render={({ field }) => (
                              <Input
                                disabled={user?.roleId === 1}
                                style={{ width: '90%' }}
                                {...field}
                                placeholder={
                                  groupTypeIndex === 0
                                    ? 'Nhập tên Nhóm phân loại hàng, ví dụ: màu sắc, kích thước v.v'
                                    : 'Nhập tên phân loại, ví dụ: Size, v.v'
                                }
                              />
                            )}
                          />
                        </Form.Item>
                        <Form.Item
                          label="Phân loại hàng"
                          labelCol={{ span: 3 }}
                          wrapperCol={{ span: 21 }}
                        >
                          <NestedFieldArray
                            nestIndex={groupTypeIndex}
                            control={control}
                          />
                        </Form.Item>
                      </div>
                    </Form.Item>
                  ))}
                  {groupTypeFields.length === 1 && (
                    <Form.Item label="Nhóm phân loại 2">
                      <Button
                        disabled={user?.roleId === 1}
                        type="dashed"
                        onClick={() =>
                          groupTypeAppend({ name: '', value: [''] })
                        }
                        className={'w-full'}
                        style={{
                          color: '#5863dc',
                          borderColor: '#5863dc',
                        }}
                        icon={<PlusCircleOutlined />}
                      >
                        Thêm
                      </Button>
                    </Form.Item>
                  )}
                </>
              )}
              {groupTypeFields.length === 0 ? (
                <>
                  <Form.Item
                    label={'Giá'}
                    required={true}
                    validateStatus={errors.product?.price && 'error'}
                    help={errors.product?.price?.message}
                  >
                    <Controller
                      control={control}
                      name={`product.price`}
                      render={({ field }) => (
                        <InputNumber
                          {...field}
                          disabled={user?.roleId === 1}
                          addonAfter="đ"
                          className={'w-full'}
                        />
                      )}
                    />
                  </Form.Item>
                  <Form.Item
                    label={'Nhập số lượng'}
                    required={true}
                    validateStatus={errors.product?.stock && 'error'}
                    help={errors.product?.stock?.message}
                  >
                    <div
                      style={{
                        backgroundColor: '#fafafa',
                      }}
                      className="relative p-4"
                    >
                      {stockFields?.map((stockItem, stockIndex) => (
                        <Row gutter={[8, 8]} key={`stock-${stockItem.id}`}>
                          <Col xs={12} sm={12} md={12} xl={12} lg={12}>
                            <Form.Item
                              label={`Kho: ${stockIndex + 1}`}
                              required={true}
                              validateStatus={
                                errors.product?.stock?.[stockIndex]
                                  ?.merchantAddressId && 'error'
                              }
                              help={
                                errors.product?.stock?.[stockIndex]
                                  ?.merchantAddressId?.message
                              }
                            >
                              <Controller
                                control={control}
                                name={`product.stock.${stockIndex}.merchantAddressId`}
                                render={({ field }) => (
                                  <Select
                                    {...field}
                                    disabled={user?.roleId === 1}
                                    filterOption={filterOption}
                                    showSearch
                                    allowClear
                                  >
                                    {merchantAddress?.map(
                                      (merchantAddressItem) => (
                                        <Select.Option
                                          value={merchantAddressItem.id}
                                          key={`merchantAddress-${merchantAddressItem.id}`}
                                        >
                                          {merchantAddressItem.name}
                                        </Select.Option>
                                      )
                                    )}
                                  </Select>
                                )}
                              />
                            </Form.Item>
                          </Col>
                          <Col xs={1} sm={11} md={11} xl={11} lg={11}>
                            <Form.Item
                              label="Số lượng"
                              required={true}
                              validateStatus={
                                errors.product?.stock?.[stockIndex]?.quantity &&
                                'error'
                              }
                              help={
                                errors.product?.stock?.[stockIndex]?.quantity
                                  ?.message
                              }
                            >
                              <Controller
                                control={control}
                                name={`product.stock.${stockIndex}.quantity`}
                                render={({ field }) => (
                                  <InputNumber
                                    {...field}
                                    disabled={user?.roleId === 1}
                                    min={1}
                                    className={'w-full'}
                                  />
                                )}
                              />
                            </Form.Item>
                          </Col>
                          <Col xs={1} sm={1} md={1} xl={1} lg={1}>
                            {stockFields.length > 1 ? (
                              <Button
                                disabled={user?.roleId === 1}
                                type={'text'}
                                className={'text-gray-400'}
                                onClick={() => stockRemove(stockIndex)}
                                icon={<DeleteOutlined />}
                              />
                            ) : null}
                          </Col>
                        </Row>
                      ))}
                      <FormItem>
                        <Button
                          disabled={user?.roleId === 1}
                          type="dashed"
                          className={'w-full'}
                          onClick={() => stockAppend({})}
                          style={{
                            color: '#5863dc',
                            borderColor: '#5863dc',
                          }}
                          icon={<PlusCircleOutlined />}
                        >
                          Thêm 1 kho
                        </Button>
                      </FormItem>
                    </div>
                  </Form.Item>
                </>
              ) : (
                <>
                  <Form.Item label="Mẹo thiết lập phân loại hàng">
                    <Row gutter={[16, 16]}>
                      <Col xs={24} sm={12} md={12} lg={8}>
                        <Controller
                          control={control}
                          name="price"
                          render={({ field }) => (
                            <InputNumber
                              {...field}
                              disabled={user?.roleId === 1}
                              addonAfter="đ"
                              className={'w-full'}
                              placeholder="Giá"
                            />
                          )}
                        />
                      </Col>
                      <Col xs={24} sm={12} md={12} lg={8}>
                        <Form.Item label="Số lượng">
                          <Controller
                            control={control}
                            name="quantity"
                            render={({ field }) => (
                              <InputNumber
                                {...field}
                                disabled={user?.roleId === 1}
                                className={'w-full'}
                                placeholder="Kho hàng"
                              />
                            )}
                          />
                        </Form.Item>
                      </Col>
                      <Col xs={24} sm={12} md={12} xl={8} lg={8}>
                        <Button
                          type={'primary'}
                          className={'w-full'}
                          onClick={applyAll}
                          disabled={!watchPrice && !watchQuantity}
                        >
                          Áp dụng cho tất cả phân loại
                        </Button>
                      </Col>
                    </Row>
                  </Form.Item>
                  <Form.Item label="Phân loại hàng">
                    <Row className={'text-center border'}>
                      {variantTableHeads?.map(
                        (variantTableHeadsItem, variantTableHeadsIndex) => {
                          return (
                            <Col
                              span={
                                variantTableHeads.length === 5
                                  ? variantTableHeadsIndex < 3
                                    ? 4
                                    : variantTableHeadsIndex < 4
                                    ? 8
                                    : 4
                                  : variantTableHeadsIndex < 4
                                  ? 4
                                  : variantTableHeadsIndex < 5
                                  ? 6
                                  : 2
                                // variantTableHeads.length === 4 ||
                                // variantTableHeadsIndex < 2
                                //   ? 6
                                //   : 4
                              }
                              className={'py-2'}
                              style={{ backgroundColor: '#fafafa' }}
                              key={`variantTableHeads-${variantTableHeadsIndex}`}
                            >
                              {variantTableHeadsItem}
                            </Col>
                          )
                        }
                      )}
                    </Row>
                    <VariantTableBody
                      productGroupType={watchProductGroupType}
                      merchantAddress={merchantAddress}
                    />
                  </Form.Item>
                </>
              )}
            </div>
          </Card>
          <Card title={<div className={'text-xl'}>Kích thước đóng gói</div>}>
            <Row gutter={[8, 8]} className={'p-4'}>
              <Col xs={24}>
                <Form.Item
                  label={<>Cân nặng</>}
                  extra="Sau khi đóng gói"
                  required={true}
                  validateStatus={errors.product?.weight && 'error'}
                  help={errors.product?.weight?.message}
                >
                  <Controller
                    control={control}
                    name={`product.weight`}
                    render={({ field }) => (
                      <Input
                        {...field}
                        disabled={user?.roleId === 1}
                        addonAfter={'gr'}
                        placeholder={'Cân nặng'}
                      />
                    )}
                  />
                </Form.Item>
              </Col>
              <Col xs={24}>
                <Form.Item
                  label={'Kích thước đóng gói'}
                  extra={
                    'Phí vận chuyển thực tế sẽ thay đổi nếu bạn nhập sai kích thước'
                  }
                >
                  <Row gutter={[16, 16]}>
                    <Col xs={24} md={8}>
                      <Controller
                        control={control}
                        name={`product.length`}
                        render={({ field }) => (
                          <Input
                            {...field}
                            disabled={user?.roleId === 1}
                            addonAfter={'cm'}
                            placeholder={'Chiều dài'}
                          />
                        )}
                      />
                    </Col>
                    <Col xs={24} md={8}>
                      <Controller
                        control={control}
                        name={`product.width`}
                        render={({ field }) => (
                          <Input
                            {...field}
                            disabled={user?.roleId === 1}
                            addonAfter={'cm'}
                            placeholder={'Chiều rộng'}
                          />
                        )}
                      />
                    </Col>
                    <Col xs={24} md={8}>
                      <Controller
                        control={control}
                        name={`product.height`}
                        render={({ field }) => (
                          <Input
                            {...field}
                            disabled={user?.roleId === 1}
                            addonAfter={'cm'}
                            placeholder={'Chiều cao'}
                          />
                        )}
                      />
                    </Col>
                  </Row>
                </Form.Item>
              </Col>
            </Row>
          </Card>
          <FooterBar
            right={
              user?.type === EnumRoleEntityType.merchant && (
                <Button type={'primary'} onClick={handleSubmit(onSubmit)}>
                  Lưu
                </Button>
              )
            }
          />
        </Form>
      </FormProvider>
    </Content>
  )
}
export default ProductDetail

const NestedFieldArray: FC<any> = ({ nestIndex, control }) => {
  const { fields, remove, append } = useFieldArray({
    control,
    name: `product.groupType[${nestIndex}].value`,
  })
  return (
    <>
      {fields.map((item, k) => {
        return (
          <Row
            key={item.id}
            gutter={[16, 16]}
            className={'mb-2'}
            style={{
              width: '100%',
            }}
          >
            <Col xs={24} md={22}>
              <Controller
                control={control}
                name={`product.groupType.${nestIndex}.value.${k}.field`}
                render={({ field }) => (
                  <Input {...field} placeholder={`Nhập phân loại hàng`} />
                )}
              />
            </Col>
            <Col xs={24} md={2}>
              {fields.length > 1 ? (
                <Button
                  type={'text'}
                  onClick={() => remove(k)}
                  className={'text-gray-400'}
                  icon={<DeleteOutlined />}
                />
              ) : null}
            </Col>
          </Row>
        )
      })}
      <Row gutter={[16, 16]}>
        <Col xs={24} md={24}>
          <Button
            type="dashed"
            onClick={() => append({})}
            style={{
              width: '90%',
              color: '#5863dc',
              borderColor: '#5863dc',
            }}
            icon={<PlusCircleOutlined />}
          >
            Thêm phân loại hàng
          </Button>
        </Col>
      </Row>
    </>
  )
}

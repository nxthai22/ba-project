import React, { useEffect, useState } from 'react'
import { Alert, Button, Card, Col, Form, Input, Row } from 'antd'
import { LeftOutlined, RightOutlined, SearchOutlined } from '@ant-design/icons'
import Content from 'components/layout/AdminLayout/Content'
import Link from 'components/common/Link'
import { useRouter } from 'next/router'
import { ProductCategoriesService } from 'services'
import { alertError } from 'utils'
import { useSetRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'

const Category = () => {
  const router = useRouter()
  const setIsLoading = useSetRecoilState(loadingState)
  const [title] = useState('Ngành hàng sản phẩm')
  const [productName, setProductName] = useState('')
  const [isScrollRight, setIsScrollRight] = useState(false)
  const [categoryParent, setCategoryParent] = useState([])
  const [selectedCategoryId, setSelectedCategoryId] = useState(0)
  const [focusedCategoryIds, setFocusedCategoryIds] = useState([])
  const [orginalCategoryList, setOrginalCategoryList] = useState([])
  const [firstCategoryChildren, setFirstCategoryChildren] = useState([])
  const [secondCategoryChildren, setSecondCategoryChildren] = useState([])
  const [thirdCategoryChildren, setThirdCategoryChildren] = useState([])
  const [fourthCategoryChildren, setFourthCategoryChildren] = useState([])
  useEffect(() => {
    setIsLoading(true)
    ProductCategoriesService.getManyBase({
      sort: ['id,ASC', 'parentId,DESC', 'name,ASC'],
      limit: 1000,
    })
      .then((productCategoriesResponse) => {
        const categories = productCategoriesResponse?.data?.map((x, i, r) => {
          const hasChildren =
            r.findIndex(({ parentId }) => parentId === x.id) !== -1
          return { ...x, hasChildren }
        })
        const categoryParentList = categories.filter(
          (category) => !category.parentId
        )
        setCategoryParent(categoryParentList)
        setOrginalCategoryList(categories)
        setIsLoading(false)
      })
      .catch((error) => {
        alertError(error)
        setIsLoading(false)
      })
  }, [])

  const generateCategoryChildren = (category, level) => {
    const updatedFocusedCategoryIds = [...focusedCategoryIds].slice(0, level)
    updatedFocusedCategoryIds[level] = category.id
    setFocusedCategoryIds(updatedFocusedCategoryIds)
    const categories = orginalCategoryList.filter(
      ({ parentId }) => parentId === category.id
    )

    switch (level) {
      case 0:
        setSecondCategoryChildren([])
        setThirdCategoryChildren([])
        setFourthCategoryChildren([])
        setFirstCategoryChildren(categories)
        break
      case 1:
        setThirdCategoryChildren([])
        setFourthCategoryChildren([])
        setSecondCategoryChildren(categories)
        break
      case 2:
        setFourthCategoryChildren([])
        setThirdCategoryChildren(categories)
        break
      case 3:
        setFourthCategoryChildren(categories)
        break
      default:
        break
    }

    if (categories.length === 0) {
      setSelectedCategoryId(category.id)
    }
  }

  const findCategories = (name) => {
    setSecondCategoryChildren([])
    setSecondCategoryChildren([])
    const foundCategories = [...orginalCategoryList].filter(
      (category) =>
        category.name.toLowerCase().includes(name.toLowerCase()) &&
        category.parentId === null
    )
    setCategoryParent(foundCategories)
  }

  return (
    <Content
      title={title}
      subTitle={'Vui lòng chọn ngành hàng phù hợp cho sản phẩm của bạn.'}
      onBack={() => router.push('/product')}
    >
      <Card>
        <Form>
          <Row gutter={[8, 8]}>
            <Col xs={24} sm={24} md={24} lg={24}>
              <Form.Item
                label={'Tên sản phẩm'}
                name="productName"
                rules={[
                  {
                    required: true,
                    message: 'Không được để ô trống',
                    transform: (value) => value.trim(),
                    whitespace: false,
                  },
                  {
                    type: 'string',
                    min: 10,
                    message:
                      'Tên sản phẩm của bạn quá ngắn. Vui lòng nhập ít nhất 10 ký tự.',
                    transform: (value) => value.trim(),
                    whitespace: false,
                  },
                  {
                    type: 'string',
                    max: 120,
                    transform: (value) => value.trim(),
                    whitespace: false,
                  },
                ]}
              >
                <Input
                  minLength={10}
                  maxLength={120}
                  addonAfter={`${productName.length}/120`}
                  onChange={(event) =>
                    setProductName(event.target.value.trim())
                  }
                />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>
      <div className={'add-product'}>
        <div className="product-category">
          <div className="category-selector-wrap">
            <div className="category-selector">
              <div className="selector">
                <div className="category-search">
                  <Input
                    placeholder="Tên ngành hàng"
                    prefix={<SearchOutlined className="site-form-item-icon" />}
                    className="rounded-full mb-5"
                    onChange={(event) => findCategories(event.target.value)}
                  />
                </div>
                <div className="category-wrap">
                  <div
                    className="btn-wrap-left"
                    style={{
                      display:
                        window.innerWidth - 48 - 250 < 1500 && isScrollRight
                          ? 'block'
                          : 'none',
                    }}
                  >
                    <Button
                      shape="circle"
                      icon={<LeftOutlined />}
                      size={'large'}
                      onClick={() => setIsScrollRight(false)}
                    />
                  </div>
                  <div
                    className="btn-wrap-right"
                    style={{
                      display:
                        window.innerWidth - 48 - 250 < 1500 &&
                        thirdCategoryChildren.length &&
                        !isScrollRight
                          ? 'block'
                          : 'none',
                    }}
                  >
                    <Button
                      shape="circle"
                      icon={<RightOutlined />}
                      size={'large'}
                      onClick={() => setIsScrollRight(true)}
                    />
                  </div>

                  <div
                    className="category-list"
                    style={isScrollRight ? { left: '-20%' } : { left: '0%' }}
                  >
                    <ul className="scroll-item overflow-y-scroll">
                      {categoryParent.map((category) => (
                        <li
                          key={category.id}
                          className={
                            'category-item ' +
                            (focusedCategoryIds.includes(category.id)
                              ? 'selected'
                              : '')
                          }
                          onClick={() => generateCategoryChildren(category, 0)}
                        >
                          <p className="text-overflow">{category.name}</p>
                          {category.hasChildren && <RightOutlined />}
                        </li>
                      ))}
                    </ul>
                    <ul className="scroll-item overflow-y-scroll">
                      {firstCategoryChildren.map((category) => (
                        <li
                          key={category.id}
                          className={
                            'category-item ' +
                            (focusedCategoryIds.includes(category.id)
                              ? 'selected'
                              : '')
                          }
                          onClick={() => generateCategoryChildren(category, 1)}
                        >
                          <p className="text-overflow">{category.name}</p>
                          {category.hasChildren && <RightOutlined />}
                        </li>
                      ))}
                    </ul>
                    <ul className="scroll-item overflow-y-scroll">
                      {secondCategoryChildren.map((category) => (
                        <li
                          key={category.id}
                          className={
                            'category-item ' +
                            (focusedCategoryIds.includes(category.id)
                              ? 'selected'
                              : '')
                          }
                          onClick={() => generateCategoryChildren(category, 2)}
                        >
                          <p className="text-overflow">{category.name}</p>
                          {category.hasChildren && <RightOutlined />}
                        </li>
                      ))}
                    </ul>
                    <ul className="scroll-item overflow-y-scroll">
                      {thirdCategoryChildren.map((category) => (
                        <li
                          key={category.id}
                          className={
                            'category-item ' +
                            (focusedCategoryIds.includes(category.id)
                              ? 'selected'
                              : '')
                          }
                          onClick={() => generateCategoryChildren(category, 3)}
                        >
                          <p className="text-overflow">{category.name}</p>
                          {category.hasChildren && <RightOutlined />}
                        </li>
                      ))}
                    </ul>
                    <ul className="scroll-item overflow-y-scroll">
                      {fourthCategoryChildren.map((category) => (
                        <li
                          key={category.id}
                          className={
                            'category-item ' +
                            (focusedCategoryIds.includes(category.id)
                              ? 'selected'
                              : '')
                          }
                          onClick={() => generateCategoryChildren(category, 4)}
                        >
                          <p className="text-overflow">{category.name}</p>
                          {category.hasChildren && <RightOutlined />}
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>
              <div className="category-selected">
                <span className="label">Đã chọn :</span>

                {!focusedCategoryIds.length ? (
                  <span className="no-select">Chưa chọn ngành hàng</span>
                ) : (
                  <span className="cat-selected-item">
                    {orginalCategoryList
                      .filter(({ id }) => focusedCategoryIds.includes(id))
                      .map(({ name }) => name)
                      .join(' > ')}
                  </span>
                )}
              </div>
            </div>
            <div className="prohibit-category-policy-alert mt-4 hidden">
              <Alert
                message="Bạn không thể đăng sản phẩm vào ngành hàng này do chính sách hạn chế/cấm đăng bán sản phẩm liên quan đến ngành hàng. "
                type="warning"
                closable
                action={
                  <a href="#" className="underline">
                    Xem chi tiết tại đây
                  </a>
                }
              />
            </div>
          </div>
          <Link
            href={{
              pathname: '/product/edit/0',
              query: {
                productName: productName.trim(),
                categoryId: selectedCategoryId,
              },
            }}
          >
            <Button
              type="primary"
              size={'large'}
              disabled={
                !productName || productName.length < 10 || !selectedCategoryId
              }
            >
              Tiếp theo
            </Button>
          </Link>
        </div>
      </div>
    </Content>
  )
}

export default Category

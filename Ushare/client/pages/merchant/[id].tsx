import React, { FC, useEffect, useState } from 'react'
import Content from 'components/layout/AdminLayout/Content'
import { Button, Card, Form, Input, Select } from 'antd'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import {
  BankEntity,
  BankService,
  MerchantEntity,
  MerchantsService,
} from 'services'
import { alertError, filterOption, modifyEntity } from 'utils'
import { useSetRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'
import { useRouter } from 'next/router'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'

interface Inputs {
  merchant: MerchantEntity
}

const schema = yup.object().shape({
  merchant: yup.object().shape({
    code: yup.string().trim().required('Chưa nhập mã'),
    name: yup.string().trim().required('Chưa nhập tên'),
  }),
})

const MerchantDetail: FC = () => {
  const [title, setTitle] = useState<string>('Tạo mới nhà cung cấp')
  const router = useRouter()
  const setIsLoading = useSetRecoilState(loadingState)
  const [banks, setBanks] = useState<BankEntity[]>()
  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
    defaultValues: {
      merchant: {
        name: '',
        code: '',
      },
    },
  })
  useEffect(() => {
    setIsLoading(true)
    if (Number(router?.query?.id)) {
      setTitle('Cập nhật Đối tác')
      MerchantsService.getOneBase({
        id: Number(router?.query?.id),
      })
        .then((response) => {
          setValue('merchant', response)
          setIsLoading(false)
        })
        .catch((e) => {
          alertError(e)
          setIsLoading(false)
        })
    }
    setIsLoading(false)
  }, [router])
  useEffect(() => {
    setIsLoading(true)
    BankService.getManyBase({
      limit: 1000,
      page: 1,
      sort: ['shortName,ASC'],
    }).then((response) => {
      setBanks(response.data)
    })
    setIsLoading(false)
  }, [])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    data.merchant.name = data.merchant?.name?.trim()
    data.merchant.code = data.merchant?.code?.trim()
    modifyEntity(MerchantsService, data.merchant, title, () =>
      router.push(`/merchant`)
    ).then()
  }
  return (
    <Content title={title} onBack={() => router.push('/merchant')}>
      <Form
        onFinish={handleSubmit(onSubmit)}
        labelCol={{ xs: 24, sm: 24, md: 24, xl: 4, lg: 4 }}
        wrapperCol={{ xs: 24, sm: 24, md: 24, xl: 20, lg: 20 }}
        layout="horizontal"
      >
        <Card>
          <Form.Item
            label={<span>Mã nhà cung cấp</span>}
            required={true}
            validateStatus={errors.merchant?.code && 'error'}
            help={errors.merchant?.code?.message}
          >
            <Controller
              control={control}
              name={'merchant.code'}
              render={({ field }) => (
                <Input {...field} placeholder={'Nhập mã nhà cung cấp'} />
              )}
            />
          </Form.Item>
          <Form.Item
            label={<span>Tên nhà cung cấp</span>}
            required={true}
            validateStatus={errors.merchant?.name && 'error'}
            help={errors.merchant?.name?.message}
          >
            <Controller
              control={control}
              name={'merchant.name'}
              render={({ field }) => (
                <Input {...field} placeholder={'Nhập tên nhà cung cấp'} />
              )}
            />
          </Form.Item>
          <Form.Item label={<span>Ngân hàng</span>} required={false}>
            <Controller
              control={control}
              name={'merchant.bankId'}
              render={({ field }) => (
                <Select
                  showSearch
                  filterOption={filterOption}
                  placeholder="Chọn ngân hàng"
                  {...field}
                >
                  {banks &&
                    banks.map((bank) => (
                      <Select.Option key={bank.id} value={bank.id}>
                        {bank.shortName}
                      </Select.Option>
                    ))}
                </Select>
              )}
            />
          </Form.Item>
          <Form.Item label={<span>Số tài khoản</span>} required={false}>
            <Controller
              control={control}
              name={'merchant.bankNumber'}
              render={({ field }) => (
                <Input {...field} placeholder={'Nhập số tài khoản ngân hàng'} />
              )}
            />
          </Form.Item>
          <Form.Item label={<span>Chủ tài khoản</span>} required={false}>
            <Controller
              control={control}
              name={'merchant.bankAccount'}
              render={({ field }) => (
                <Input
                  {...field}
                  placeholder={'Nhập chủ tài khoản ngân hàng'}
                />
              )}
            />
          </Form.Item>
          <Form.Item label={<span>Chi nhánh</span>} required={false}>
            <Controller
              control={control}
              name={'merchant.bankBranch'}
              render={({ field }) => (
                <Input {...field} placeholder={'Chi nhánh ngân hàng'} />
              )}
            />
          </Form.Item>
        </Card>
        <FooterBar
          right={
            <Button htmlType={'submit'} type={'primary'}>
              Lưu
            </Button>
          }
        />
      </Form>
    </Content>
  )
}
export default MerchantDetail

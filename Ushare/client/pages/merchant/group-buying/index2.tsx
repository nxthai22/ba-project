import {
  DollarCircleOutlined,
  PlusOutlined,
  QuestionCircleOutlined,
} from '@ant-design/icons'
import {
  Button,
  Card,
  Col, Modal,
  Popconfirm,
  Row,
  Statistic,
  Tabs,
  Tag,
  Tooltip,
  Typography,
} from 'antd'
import {ColumnsType} from 'antd/es/table'
import Link from 'components/common/Link'
import Content from 'components/layout/AdminLayout/Content'
import {StatusLabelByTime} from 'enums'
import React, {FC, useEffect, useState} from 'react'
import {
  EnumApproveNotificationConfigDtoAction,
  EnumRoleEntityType,
  GroupBuyingService,
  VoucherEntity,
  VouchersService
} from 'services'
import {
  alertError,
  alertSuccess,
  formatCurrency, formatDate,
  formatNumber,
  getStatusLabelByTime,
} from 'utils'
import {format} from 'date-fns'
import DataTable from 'components/common/DataTable'
import {useRouter} from 'next/router'
import {useRecoilValue} from 'recoil'
import {authState} from '../../../recoil/Atoms'
import {Controller} from "react-hook-form";
import TextArea from "antd/lib/input/TextArea";

const Index: FC = () => {
  const [title] = useState('Danh sách mua chung')
  return (
    <Content
      title={title}
      endButtons={[
        {
          text: 'Thêm mới',
          href: '/merchant/group-buying/create',
          type: 'primary',
          visible: true,
        },
      ]}>
      <Card>
        <Tabs defaultActiveKey="1">
          <Tabs.TabPane tab="Tất cả" key="1">
            <VoucherTable/>
          </Tabs.TabPane>
          <Tabs.TabPane tab="Đang diễn ra" key="2">
            <VoucherTable status={StatusLabelByTime.INPROGRESS}/>
          </Tabs.TabPane>
          <Tabs.TabPane tab="Sắp diễn ra" key="3">
            <VoucherTable status={StatusLabelByTime.COMMING}/>
          </Tabs.TabPane>
          <Tabs.TabPane tab="Đã kết thúc" key="4">
            <VoucherTable status={StatusLabelByTime.OVER}/>
          </Tabs.TabPane>
        </Tabs>
      </Card>
    </Content>
  )
}

interface VoucherTableProps {
  status?: StatusLabelByTime
}

const disabledStatus = [StatusLabelByTime.OVER, StatusLabelByTime.INPROGRESS]
const VoucherTable: FC<VoucherTableProps> = ({status}) => {
  const router = useRouter()
  const user = useRecoilValue(authState)
  const [isShowModalStop, setIsShowModalStop] = useState(false)
  const columns: ColumnsType<VoucherEntity> = [
    {
      dataIndex: 'name',
      title: 'Tên chương trình',
      render: (type, record) => {
        return (
          <Link href={`/merchant/group-buying/${record.id}`}>
            {record.name}
          </Link>
        )
      }
    },
    {
      dataIndex: 'time',
      title: 'Thời gian áp dụng',
      render: (type, record) => {
        return (
          <Typography.Paragraph>
            <Typography.Text className="block">
              {format(new Date(record.startDate), 'HH:mm d/MM/y')}
              <br/>
              {format(new Date(record.endDate), 'HH:mm d/MM/y')}
            </Typography.Text>
          </Typography.Paragraph>
        )
      }
    },
    {
      dataIndex: 'maxProductNumber',
      title: 'Số sản phẩm tham gia',
    },
    {
      dataIndex: 'startTime',
      title: 'Số người đặt hàng',
    },
    {
      title: 'Trạng thái',
      width: 160,
      render: (value, record) => {
        const statusValue = getStatusLabelByTime(
          record.startDate,
          record.endDate
        )
        let color = ''
        let label = ''

        switch (statusValue) {
          case StatusLabelByTime.INPROGRESS:
            color = 'processing'
            label = StatusLabelByTime.INPROGRESS
            break

          case StatusLabelByTime.COMMING:
            color = 'volcano'
            label = StatusLabelByTime.COMMING
            break

          default:
            color = 'default'
            label = StatusLabelByTime.OVER
            break
        }

        return (
          <Typography.Paragraph>
            <Tag color={color}>{label}</Tag>
          </Typography.Paragraph>
        )
      },
    },
    {
      title: 'Hành động',
      width: 160,
      render: (value, record) => {
        const statusValue = getStatusLabelByTime(
          record.startDate,
          record.endDate
        )
        return (
          <div className={'flex flex-col'}>
            {
              !disabledStatus.includes(statusValue) &&
              (<Link href={`/merchant/group-buying/${record.id}`}>
                  Chỉnh sửa
                </Link>
              )
            }
            {
              disabledStatus.includes(statusValue) &&
              (<a  onClick={() => setIsShowModalStop(true)}>
                  Kết thúc
                </a>
              )
            }
          </div>
        )
      },
    },
  ]

  const [serviceParams, setServiceParams] = useState({
    sort: ['createdAt,DESC'],
  })

  useEffect(() => {
    let tmpServiceParams = {
      sort: ['createdAt,DESC'],
      filter: [
        user.type === EnumRoleEntityType.merchant
          ? `merchantId||eq||${user.merchantId}`
          : user.type === EnumRoleEntityType.admin
            ? `merchantId||isnull`
            : [],
      ],
    }
    if (status) {
      const now = new Date()
      switch (status) {
        case StatusLabelByTime.INPROGRESS:
          tmpServiceParams = {
            sort: ['createdAt,DESC'],
            filter: [
              `startDate||lt||${now.toISOString()}`,
              `endDate||gt||${now.toISOString()}`,
              user.type === EnumRoleEntityType.merchant
                ? `merchantId||eq||${user.merchantId}`
                : user.type === EnumRoleEntityType.admin
                  ? `merchantId||isnull`
                  : [],
            ],
          }
          break
        case StatusLabelByTime.COMMING:
          tmpServiceParams = {
            sort: ['createdAt,DESC'],
            filter: [
              `startDate||gt||${now.toISOString()}`,
              user.type === EnumRoleEntityType.merchant
                ? `merchantId||eq||${user.merchantId}`
                : user.type === EnumRoleEntityType.admin
                  ? `merchantId||isnull`
                  : [],
            ],
          }
          break
        case StatusLabelByTime.OVER:
          tmpServiceParams = {
            sort: ['createdAt,DESC'],
            filter: [
              `endDate||lt||${now.toISOString()}`,
              user.type === EnumRoleEntityType.merchant
                ? `merchantId||eq||${user.merchantId}`
                : user.type === EnumRoleEntityType.admin
                  ? `merchantId||isnull`
                  : [],
            ],
          }
          break
      }
    }
    setServiceParams(tmpServiceParams)
  }, [status])

  return (
    <>
      <Modal
        title={`Kết thúc chương trình mua chung`}
        visible={isShowModalStop}
        onCancel={() => {
          setIsShowModalStop(false)
        }}
        width={600}
        closable={false}
        maskClosable={false}
        destroyOnClose={true}
        footer={[
          <Button
            key="back"
            onClick={() => {
              setIsShowModalStop(false)
            }}
          >
            Hủy
          </Button>,
          <>
            <Button
              key="submit"
              type="primary"
              onClick={() => {
                // updateStatus(EnumApproveNotificationConfigDtoAction.rejected)
              }}
            >
              Kết thúc
            </Button>
          </>,
        ]}
      >
        <div>
          <p>
            Đã có {} người đang ghép đơn, nếu kết thúc chương trình, các đơn đang ghép sẽ bị hủy.
          </p>
          <p>
            Bạn có chắc chắn muốn kết thúc chương trình?
          </p>
        </div>
      </Modal>
      <DataTable
        columns={columns}
        service={GroupBuyingService.getManyBase}
        serviceParams={serviceParams}
        scroll={{x: 1000}}
        size={'small'}
        path={'/merchant/group-buying'}
      />
    </>
  )
}

export default Index

import React from 'react'
import { useRouter } from 'next/router'
import GroupBuyingDetail from 'components/admin/Merchant/GroupBuyingDetail'
import Content from 'components/layout/AdminLayout/Content'

const Index = () => {
  const router = useRouter()

  return (
    <Content
      title={router.query.id === 'create' ? 'Thêm chương trình' : 'Thông tin chương trình'}
      onBack={() => router.push('/merchant/group-buying')}
    >
      <GroupBuyingDetail />
    </Content>
  )
}

export default Index

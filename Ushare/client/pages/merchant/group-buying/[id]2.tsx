import {DeleteOutlined, PlusOutlined,} from '@ant-design/icons'
import {yupResolver} from '@hookform/resolvers/yup'
import {Avatar, Button, Card, Col, Form, Input, List, Modal, Row, Space, Table, Tag, Typography,} from 'antd'
import {ColumnsType} from 'antd/es/table'
import {TableRowSelection} from 'antd/es/table/interface'
import ModalFlashSale from 'components/admin/FlashSale/Product/ModalFlashSale'
import DataTable from 'components/common/DataTable'
import InputNumber from 'components/common/InputNumber'
import Content from 'components/layout/AdminLayout/Content'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import {StatusLabelByTime} from 'enums'
import {sortBy, uniq, uniqBy} from 'lodash'
import {useRouter} from 'next/router'
import React, {FC, useEffect, useState} from 'react'
import {Control, Controller, FieldError, SubmitHandler, useFieldArray, useForm,} from 'react-hook-form'
import {useRecoilValue} from 'recoil'
import {authState, countDataTable} from 'recoil/Atoms'
import {FlashSaleDetailDto, FlashSaleEntity, FlashSaleService, ProductEntity, ProductsService,} from 'services'
import {alertError, alertSuccess, formatCurrency, formatNumber, getCdnFile, getStatusLabelByTime,} from 'utils'
import {ServiceParams} from 'utils/interfaces'
import * as yup from 'yup'
import { DatePicker } from 'antd';

enum CommissionValueType {
  AMOUNT = 'amount', // Tính theo số lượng
  PERCENT = 'percent', // Tính theo phần trăm
}

const ErrorMessages = {
  yup: {
    maxQuantity: 'Chưa chọn số lượng sản phẩm được dùng cho chương trình',
  },
  product: {
    stock: 'Số lượng sản phẩm trong kho không còn. Vui lòng kiểm tra lại!',
  },
  variant: {
    stock: 'Số lượng sản phẩm trong kho không còn. Vui lòng kiểm tra lại!',
    discountValue: 'Giá khuyến mãi phải thấp hơn giá trị niêm yết của sản phẩm',
    maxQuantity: 'Chưa chọn số lượng sản phẩm được dùng cho chương trình',
  },
}

interface Inputs {
  name: string
  dates: Date[]
  groupBuyingProducts: IProductInputs[]
}

interface IVariantInputs {
  id: number
  name: string
  price: number
  stockNumber: number

  // Id sản phẩm thêm vào chương trình
  productId: number

  // Id biến thể thêm vào chương trình
  productVariantId?: number

  // Gía mua chung
  groupPrice: number

  // Số lượng sản phẩm được dùng cho chương trình
  maxQuantity: number
}

interface IProductInputs {
  id: number
  productId: number
  price?: number
  name: string
  images: string[]
  variantInputs: IVariantInputs[]

  // SL CTV tối thiểu/nhóm
  minMemberNumber: number
  // SP tối đa/CTV
  maxQuantityPerUser: number
  // Tgian ghép đơn
  timeToTeamUp: number
}

const schema = yup.object().shape({
  groupBuyingProducts: yup.array().of(
    yup.object().shape({
      variantInputs: yup.array().of(
        yup.object().shape({
          discountValue: yup
            .number()
            .min(0, 'Số tiền giảm giá tham gia khuyến mãi phải lớn hơn 0'),
          qty: yup
            .number()
            .nullable()
          // .min(1, 'Số lượng sản phẩm tham gia khuyến mãi phải lớn hơn 0'),
        })
      ),
    })
  ),
})

const Index: FC = () => {
  const router = useRouter()
  const {id} = router.query
  const [title] = useState<string>('Thông tin chương trình')
  const [selectedFlashSale, setSelectedFlashSale] =
    useState<FlashSaleEntity>(undefined)
  const [isChooseTimeFrameVisible, setIsChooseTimeFrameVisible] =
    useState<boolean>(false)
  const [isAddProductVisible, setIsAddProductVisible] = useState<boolean>(false)
  const [canEdit, setCanEdit] = useState<boolean>(false)
  const [productFlashSaleViews, setProductFlashSaleViews] = useState<IProductInputs[]>([])
  const [selectedProductRowKeys, setSelectedProductRowKeys] = useState<string[]>([])
  const [selectedProductVariantRowKeys, setSelectedProductVariantRowKeys] =
    useState<string[]>([])
  const {
    control,
    setValue,
    handleSubmit,
    setError,
    clearErrors,
    formState: {errors},
  } = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
    defaultValues: {
      groupBuyingProducts: [],
    },
  })
  const {
    fields: productFields,
    remove: removeProductField,
    append: appendProductField,
  } = useFieldArray({
    control,
    name: `groupBuyingProducts`,
  })

  useEffect(() => {
    if (Number(id) && Number(id) != 0) {
      FlashSaleService.getOneBase({
        id: Number(id),
        join: ['flashSaleDetail'],
      }).then((response) => {
        setSelectedFlashSale(response)
        if (response?.flashSaleDetail?.length > 0) {
          const tmpSelectedProductRowKeys = []
          const tmpSelectedProductVariantRowKeys = []
          response.flashSaleDetail.map((flashSaleDetail) => {
            tmpSelectedProductRowKeys.push(
              `product-${flashSaleDetail.productId}`
            )
            if (flashSaleDetail?.productVariantId) {
              tmpSelectedProductVariantRowKeys.push(
                `product-variant-${flashSaleDetail?.productVariantId}`
              )
            } else {
              tmpSelectedProductVariantRowKeys.push(
                `product-${flashSaleDetail?.productId}`
              )
            }
          })
          setSelectedProductVariantRowKeys(tmpSelectedProductVariantRowKeys)
          setSelectedProductRowKeys(tmpSelectedProductRowKeys)
        }
        const statusValue = getStatusLabelByTime(
          response.startTime,
          response.endTime
        )

        if (statusValue === StatusLabelByTime.COMMING) {
          setCanEdit(true)
        }

        getProductList(response, statusValue)
      })
    }
    if (id === 'create') {
      setCanEdit(true)
    }
  }, [id])

  const variantsViewFormater = (
    flashSaleDetail: Array<any>,
    product: ProductEntity
  ) => {
    const variants = []
    if (product.variants.length > 0) {
      product.variants.map((variant) => {
        const flashSaleVariant = flashSaleDetail.find(
          (detail) => detail.productVariantId === variant.id
        )
        if ((!canEdit && flashSaleVariant) || canEdit) {
          variants.push({
            ...variant,
            name: variant.name,
            price: variant.price,
            qty: flashSaleVariant?.quantity,
            productVariantId: variant.id,
            discountValue: flashSaleVariant?.discountValue || 0,
            stockNumber:
              variant.stock?.reduce(
                (total, {quantity}) => total + quantity,
                0
              ) || 0,
          })
        }
      })
    } else {
      const flashSaleVariant = flashSaleDetail?.find(
        (flashSaleDetailProduct) =>
          flashSaleDetailProduct.productId === product.id
      )
      if ((!canEdit && flashSaleVariant) || canEdit) {
        variants.push({
          productId: product.id,
          name: product.name,
          price: product?.rawPrice || product?.price || 0,
          qty: flashSaleVariant?.quantity,
          productVariantId: 0,
          discountValue: flashSaleVariant?.discountValue || 0,
          stockNumber:
            product.stock?.reduce(
              (total, {quantity}) => total + quantity,
              0
            ) || 0,
        })
      }
    }
    return variants
  }

  const fillDataIntoProductInputs = (
    formattedProductInputs: IProductInputs[]
  ) => {
    formattedProductInputs.forEach((productInput, productIndex) => {
      productInput.variantInputs.forEach((variantInput: any, variantIndex) => {
        setValue(
          `groupBuyingProducts.${productIndex}.variantInputs.${variantIndex}.discountValue`,
          variantInput.discountValue
        )
        setValue(
          `groupBuyingProducts.${productIndex}.variantInputs.${variantIndex}.discountPercentValue`,
          discountPercentValue
        )
        setValue(
          `groupBuyingProducts.${productIndex}.variantInputs.${variantIndex}.qty`,
          variantInput.qty
        )
        setValue(
          `groupBuyingProducts.${productIndex}.variantInputs.${variantIndex}.productVariantId`,
          variantInput.productVariantId
        )
      })
    })
  }

  const getProductList = (
    selectedFlashSale,
    statusLabelByTime: StatusLabelByTime
  ) => {
    const productIds = selectedFlashSale.flashSaleDetail.map(
      ({productId}) => productId
    )
    if (productIds.length > 0)
      ProductsService.getManyBase({
        filter: [`id||$in||${productIds.join()}`],
        join: 'variants',
      }).then((response) => {
        const formattedProductFields = response.data.map((product) => {
          return {
            id: product.id,
            productId: product.id,
            images: product.images,
            name: product.name,
            price: product?.price,
            variantInputs: variantsViewFormater(
              selectedFlashSale.flashSaleDetail,
              product
            ),
          }
        })
        if (statusLabelByTime === StatusLabelByTime.COMMING) {
          appendProductField(formattedProductFields)
          fillDataIntoProductInputs(formattedProductFields)
        } else {
          setProductFlashSaleViews(formattedProductFields)
        }
      })
  }

  const buildProductListInputs = (products: ProductEntity[]) => {
    const productInputFieldArray: IProductInputs[] = []
    products.map((product) => {
      const existProduct = productFields.find(
        (productField) => productField.productId === product.id
      )
      if (!existProduct) {
        const variants =
          product.variants && product.variants.length
            ? product.variants
            : [
              {
                id: product.id,
                name: product.name,
                price: product.price,
                stock: product.stock,
              },
            ]
        const variantInputs = variants.map((variant) => {
          const variantStockNumber =
            variant.stock?.reduce(
              (total, {quantity}) => total + quantity,
              0
            ) || 0
          return {
            id: variant.id || product.id,
            name: variant.name,
            price: variant.price,
            stockNumber: variantStockNumber,
            // flashSaleId: selectedFlashSale.id,
            productId: product.id,
            productVariantId: product.variants?.length > 0 ? variant.id : null,
            discountValue: variant.price,
            discountValueType: CommissionValueType.AMOUNT,
            discountPercentValue: 0,
            qty: 0,
          }
        })
        productInputFieldArray.push({
          id: product.id,
          productId: product.id,
          name: product.name,
          images: product.images,
          price: product?.price,
          variantInputs,
        })
      }
    })
    if (productInputFieldArray.length > 0)
      appendProductField(productInputFieldArray)
  }

  const checkProductInStock = (): boolean => {
    const isStockEmpty = productFields.some((productField) =>
      productField.variantInputs.some((variantField) => {
        return !variantField.stockNumber
      })
    )

    if (isStockEmpty) {
      alertError(ErrorMessages.product.stock)
    }

    return !isStockEmpty
  }

  const validateForm = (): boolean => {
    let isValid = true

    productFields.forEach((productInput, parentIndex) => {
      productInput.variantInputs.forEach((variantInput, index) => {
        if (
          selectedProductVariantRowKeys.includes(
            variantInput.id
              ? `product-${variantInput.id}`
              : `product-variant-${variantInput.productId}`
          )
        ) {
          if (!variantInput.maxQuantity) {
            setError(`groupBuyingProducts.${parentIndex}.variantInputs.${index}.maxQuantity`, {
              message: 'Số lượng sản phẩm phải lớn hơn 0',
            })
            isValid = false
          } else {
            clearErrors(`groupBuyingProducts.${parentIndex}.variantInputs.${index}.maxQuantity`)
          }
        }
      })
    })

    return isValid
  }

  const formDataFormatter = (
    flashSales: IProductInputs[]
  ): FlashSaleDetailDto[] => {
    const formattedFormData: FlashSaleDetailDto[] = []
    flashSales.forEach((productInput) => {
      productInput.variantInputs.forEach((variantInput) => {
        if (
          variantInput.qty &&
          (selectedProductVariantRowKeys.includes(
              variantInput.productVariantId
                ? `product-variant-${variantInput.productVariantId}`
                : `product-${variantInput.productId}`
            ) ||
            selectedProductVariantRowKeys.includes(
              variantInput.productVariantId
                ? `product-variant-${variantInput.productVariantId}`
                : `product-${variantInput.productId}`
            ))
        ) {
          formattedFormData.push({
            flashSaleId: id === 'create' ? selectedFlashSale.id : Number(id),
            productId: variantInput.productId,
            productVariantId: variantInput.productVariantId,
            discountValue: variantInput.discountValue,
            discountValueType: CommissionValueType.AMOUNT,
            qty: variantInput.qty,
          })
        }
      })
    })

    return formattedFormData
  }

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    // if (!checkProductInStock()) return
    if (!validateForm()) {
      alertError('Kiểm tra khuyến mãi tại các sản phẩm đã chọn')
      return
    }
    const resquestParams = formDataFormatter(data.flashSales)
    FlashSaleService.flashSaleControllerUpdateProductInFlashSaleByMerchantId({
      id: id === 'create' ? selectedFlashSale.id : Number(id),
      body: resquestParams,
    })
      .then(() => {
        alertSuccess(`Cập nhật thông tin chương trình flash sale thành công`)
        router.push(`/flash-sale/product`).then()
      })
      .catch((error) => alertError(error))
  }

  const selectProduct: TableRowSelection<IProductInputs> = {
    onSelectAll: (selected) => {
      if (selected) {
        const tmpSelectedProductRowKeys = []
        const tmpSelectedProductVariantRowKeys = []
        productFields.forEach((row) => {
          tmpSelectedProductRowKeys.push(`product-${row.productId}`)
          row?.variantInputs.forEach((variantInput) => {
            tmpSelectedProductVariantRowKeys.push(
              variantInput?.productVariantId
                ? `product-variant-${variantInput?.productVariantId}`
                : `product-${variantInput?.productId}`
            )
          })
        })
        setSelectedProductRowKeys(tmpSelectedProductRowKeys)
        setSelectedProductVariantRowKeys(tmpSelectedProductVariantRowKeys)
      } else {
        setSelectedProductRowKeys([])
        setSelectedProductVariantRowKeys([])
      }
    },
    onSelect: (record: IProductInputs, selected: boolean) => {
      const variantRowKeys = record.variantInputs.map(
        ({productVariantId, productId}) =>
          productVariantId
            ? `product-variant-${productVariantId}`
            : `product-${productId}`
      )
      if (selected) {
        const selectedRowKeys = uniq([
          ...selectedProductRowKeys,
          `product-${record.productId}`,
        ])
        setSelectedProductRowKeys(selectedRowKeys)
        setSelectedProductVariantRowKeys(
          uniq([...selectedProductVariantRowKeys, ...variantRowKeys])
        )
      } else {
        const selectedRowKeys = [...selectedProductRowKeys].filter(
          (id) => id !== `product-${record.productId}`
        )
        setSelectedProductRowKeys(selectedRowKeys)

        const filteredVariantRowKeys = selectedProductVariantRowKeys.filter(
          (id) => !variantRowKeys.includes(id)
        )
        setSelectedProductVariantRowKeys(filteredVariantRowKeys)
      }
    },
    getCheckboxProps: (record: IProductInputs) => ({
      id: String(record.id),
      indeterminate:
        record?.variantInputs?.some(({productVariantId, productId}) =>
          selectedProductVariantRowKeys.includes(
            productVariantId
              ? `product-variant-${productVariantId}`
              : `product-${productId}`
          )
        ) &&
        !record?.variantInputs?.every(({productVariantId, productId}) =>
          selectedProductVariantRowKeys.includes(
            productVariantId
              ? `product-variant-${productVariantId}`
              : `product-${productId}`
          )
        ),
    }),
    selectedRowKeys: selectedProductRowKeys,
  }

  const selectVariant: TableRowSelection<IVariantInputs> = {
    onSelect: (record: IVariantInputs, selected: boolean) => {
      const variantInputs =
        productFields.find(({productId}) => productId === record.productId)
          ?.variantInputs || []
      if (selected) {
        const selectedRowKeys = uniq([
          ...selectedProductVariantRowKeys,
          record.productVariantId
            ? `product-variant-${record.productVariantId}`
            : `product-${record.productId}`,
        ])
        setSelectedProductVariantRowKeys(selectedRowKeys)
        const isCheckAll = variantInputs.every(
          ({productVariantId, productId}) =>
            selectedRowKeys.includes(
              productVariantId
                ? `product-variant-${productVariantId}`
                : `product-${productId}`
            )
        )
        if (isCheckAll) {
          setSelectedProductRowKeys(
            uniq([...selectedProductRowKeys, `product-${record.productId}`])
          )
        }
      } else {
        const selectedRowKeys = [...selectedProductVariantRowKeys].filter(
          (id) =>
            id !==
            (record.productVariantId
              ? `product-variant-${record.productVariantId}`
              : `product-${record.productId}`)
        )
        setSelectedProductVariantRowKeys(selectedRowKeys)
        setSelectedProductRowKeys(
          [...selectedProductRowKeys].filter(
            (id) => id !== `product-${record.productId}`
          )
        )
      }
    },
    getCheckboxProps: (record: IVariantInputs) => {
      return {
        id: String(
          record.productVariantId
            ? `product-variant-${record.productVariantId}`
            : `product-${record.productId}`
        ),
      }
    },
    selectedRowKeys: selectedProductVariantRowKeys,
  }

  const productColumns: ColumnsType<IProductInputs> = [
    {
      title: 'Phân loại hàng',
      dataIndex: 'name',
      key: 'name',
      width: '20%',
      render: (value, record) => {
        return (
          <div className={'flex'}>
            {record?.images && record?.images.length > 0 && (
              <div className={'mr-3 w-15'}>
                <img
                  src={getCdnFile(record?.images?.[0])}
                  width={'60px'}
                  alt={value}
                />
              </div>
            )}
            <div className="flex items-center font-medium">
              <a
                href={`/product/edit/${record.productId}`}
                target={'_blank'}
                rel={'noreferrer'}
              >
                {value}
              </a>
            </div>
          </div>
        )
      },
    },

    {title: 'Giá bán lẻ', width: '5%'},
    {title: 'Giá mua chung*', width: '10%'},
    {
      title: 'SL CTV tối thiểu/nhóm *',
      width: '15%',
      render: (_value, record, index) => {
        return (
          <ProductInputNumber
            variableName={'minMemberNumber'}
            errors={errors}
            control={control}
            recordProduct={record}
            addonAfter="người"
            productInputIndex={index}
            setValue={setValue}
            clearErrors={clearErrors}
          />)
      }
    },
    {
      title: 'SP tối đa/CTV',
      width: '15%',
      render: (_value, record, index) => {
        return (
          <ProductInputNumber
            variableName={'maxQuantityPerUser'}
            errors={errors}
            control={control}
            recordProduct={record}
            addonAfter="sp"
            productInputIndex={index}
            setValue={setValue}
            clearErrors={clearErrors}
          />)
      }
    },
    {
      title: 'Tgian ghép đơn*',
      width: '15%',
      render: (_value, record, index) => {
        return (
          <ProductInputNumber
            variableName={'timeToTeamUp'}
            errors={errors}
            control={control}
            recordProduct={record}
            addonAfter="giờ"
            productInputIndex={index}
            setValue={setValue}
            clearErrors={clearErrors}
          />)
      }
    },
    {title: 'Số sản phẩm áp dụng', width: '10%'},
    {title: 'Slg tồn kho', width: '5%'},
    {
      title: 'Hành động',
      width: '5%',
      align: 'center',
      render: (_value, _record, index) => (
        <Button
          icon={<DeleteOutlined onClick={() => removeProductField(index)}/>}
        />
      ),
    },
  ]


  const expandedProductRowRender = (
    product: IProductInputs,
    parentIndex: number
  ) => {
    const columns: ColumnsType<IVariantInputs> = [
      {title: 'Phân loại hàng', dataIndex: 'name', width: '20%'},
      {
        title: 'Giá bán lẻ',
        dataIndex: 'price',
        width: '5%',
        render: (value) => formatCurrency(value),
      },
      {
        title: 'Giá mua chung*', width: '10%',
        render: (_value, record, index) => {
          return (
            <ProductVariantInputNumber
              variableName={'groupPrice'}
              errors={errors}
              control={control}
              recordVariant={record}
              productInputIndex={parentIndex}
              variantInputIndex={index}
              setValue={setValue}
              clearErrors={clearErrors}
            />)
        }
      },
      {title: 'SL CTV tối thiểu/nhóm *', width: '15%'},
      {title: 'SP tối đa/CTV', width: '15%'},
      {title: 'Tgian ghép đơn*', width: '15%'},
      {
        title: 'Số sản phẩm áp dụng', width: '10%',
        render: (_value, record, index) => {
          return (
            <ProductVariantInputNumber
              variableName={'maxQuantity'}
              errors={errors}
              control={control}
              recordVariant={record}
              productInputIndex={parentIndex}
              variantInputIndex={index}
              setValue={setValue}
              clearErrors={clearErrors}
            />)
        }
      },
      {
        title: 'Slg tồn kho',
        dataIndex: 'stockNumber',
        width: '5%',
        render: (value) => formatNumber(value),
      },
      {
        title: 'Hành động',
        width: '5%',
        align: 'center',
        render: (_value, _record, index) => (
          <Button
            icon={<DeleteOutlined onClick={() => removeProductField(index)}/>}
          />
        ),
      },
    ]
    return (
      <Table
        rowKey={(record) =>
          record.productVariantId
            ? `product-variant-${record.productVariantId}`
            : `product-${record.productId}`
        }
        showHeader={false}
        columns={columns}
        rowSelection={selectVariant}
        dataSource={product.variantInputs}
        pagination={false}
      />
    )
  }

  const productColumnsViewMode: ColumnsType<IProductInputs> = [
    {
      title: 'Phân loại hàng',
      dataIndex: 'name',
      key: 'name',
      width: '40%',
      render: (_value, record) => (
        <Row>
          <Col className={'mr-3 w-15'}>
            {record?.images && record?.images.length > 0 && (
              <img
                src={getCdnFile(record?.images?.[0])}
                width={'60px'}
                alt={record.name}
              />
            )}
          </Col>
          <Col className="flex items-center font-medium">
            <a
              href={`/product/edit/${record.id}`}
              target={'_blank'}
              rel={'noreferrer'}
            >
              {record.name}
            </a>
          </Col>
        </Row>
      ),
    },
    {
      title: 'Giá gốc',
      width: '10%',
    },
    {title: 'Giá đã giảm', width: '10%'},
    {title: 'Khuyến Mãi', width: '10%'},
    {title: 'SL sản phẩm khuyến mãi', width: '20%'},
    {title: 'Kho hàng', width: '10%'},
  ]

  const expandedRowProductViewRender = (product: IProductInputs) => {
    const columns: ColumnsType<IVariantInputs> = [
      {dataIndex: 'name', width: '40%'},
      {
        dataIndex: 'price',
        width: '10%',
        render: (value) => formatCurrency(value),
      },
      {
        dataIndex: 'discountValue',
        width: '10%',
        render: (value) => formatCurrency(value),
      },
      {
        render: (_value, record) => {
          return (
            <Tag color={'processing'}>
              {Math.abs(
                Math.ceil(
                  ((record.discountValue - record.price) / record.price) * 100
                )
              )}
              %
            </Tag>
          )
        },
        width: '10%',
      },
      {
        dataIndex: 'qty',
        width: '20%',
      },
      {
        dataIndex: 'stockNumber',
        width: '10%',
      },
    ]

    return (
      <Table
        rowKey={(record) =>
          record.id
            ? `product-variant-${record.id}`
            : `product-${record.productId}`
        }
        showHeader={false}
        columns={columns}
        dataSource={product.variantInputs}
        pagination={false}
      />
    )
  }

  const onBulkUpdate = (onlySelected: boolean, data: TableFilterInputs) => {
    console.log(onlySelected, data)
    console.log(productFields)
    console.log(selectedProductRowKeys)
    console.log(selectedProductVariantRowKeys)
    productFields.forEach((productInput, productIndex) => {
      if (!onlySelected || selectedProductRowKeys.includes(`product-${productInput.productId}`)) {
        if (data.minMemberNumber) {
          setValue(
            `groupBuyingProducts.${productIndex}.minMemberNumber`,
            data.minMemberNumber
          )
        }
        if (data.maxQuantityPerUser) {
          setValue(
            `groupBuyingProducts.${productIndex}.maxQuantityPerUser`,
            data.maxQuantityPerUser
          )
        }
        if (data.timeToTeamUp) {
          setValue(
            `groupBuyingProducts.${productIndex}.timeToTeamUp`,
            data.timeToTeamUp
          )
        }
      }
      productInput.variantInputs.forEach((variantInput, variantIndex) => {
        if (!onlySelected || selectedProductVariantRowKeys.includes(
          variantInput.productVariantId
            ? `product-variant-${variantInput.productVariantId}`
            : `product-${variantInput.productId}`)
        ) {
          if (data.groupPrice) {
            setValue(
              `groupBuyingProducts.${productIndex}.variantInputs.${variantIndex}.groupPrice`,
              data.groupPrice
            )
          }
          if (data.maxQuantity) {
            setValue(
              `groupBuyingProducts.${productIndex}.variantInputs.${variantIndex}.maxQuantity`,
              data.maxQuantity
            )
          }
        }
      })
    })
    validateForm()
  }

  return (
    <Content title={title} onBack={() => router.push('/merchant/group-buying')}>
      <Form
        onFinish={handleSubmit(onSubmit)}
        labelCol={{span: 3}}
        wrapperCol={{span: 12}}
      >
        <Card title={'Thông tin cơ bản'}>
          <Form.Item label={'Tên chương trình'}>
            <Controller
              control={control}
              name="name"
              render={({field}) => (
                <Input
                  {...field}
                  placeholder={'Nhập tên chương trình'}
                  minLength={1}
                  maxLength={100}
                />
              )}
            />
          </Form.Item>
          <Form.Item label={'Thời gian áp dụng'}>
            <Controller
              control={control}
              name={'dates'}
              render={({field}) => (
                <DatePicker.RangePicker
                  {...field}
                  showTime
                  format={'d/MM/y'}
                />
              )}
            />
          </Form.Item>
        </Card>
        <Card
          title={<ProductListTitle/>}
          extra={
            canEdit ? (
              <Button
                type="primary"
                icon={<PlusOutlined/>}
                onClick={() => setIsAddProductVisible(true)}
              >
                Thêm sản phẩm
              </Button>
            ) : (
              ''
            )
          }
        >
          {id && id !== 'create' && !canEdit ? (
            <Table
              rowKey={(record) =>
                record.id
                  ? `product-variant-${record.id}`
                  : `product-${record.productId}`
              }
              dataSource={productFlashSaleViews}
              columns={productColumnsViewMode}
              pagination={false}
              sticky={true}
              expandable={{
                expandedRowRender: expandedRowProductViewRender,
                defaultExpandAllRows: true,
              }}
              scroll={{
                x: 1200,
              }}
            />
          ) : (
            <>
              {productFields.length > 0 && (
                <TableFilter
                  selectedRowKeys={selectedProductVariantRowKeys}
                  onUpdate={onBulkUpdate}
                />
              )}
              <Table
                style={{display: productFields.length > 0 ? 'block' : 'none'}}
                rowKey={(record) => `product-${record.productId}`}
                dataSource={productFields}
                columns={productColumns}
                rowSelection={selectProduct}
                pagination={false}
                sticky={true}
                expandable={{
                  expandedRowRender: expandedProductRowRender,
                  defaultExpandAllRows: true,
                }}
                scroll={{
                  x: 1500,
                }}
              />
            </>
          )}
        </Card>

        {canEdit && (
          <FooterBar
            right={
              <Button
                htmlType={'submit'}
                type={'primary'}
                disabled={!selectedFlashSale}
              >
                Lưu
              </Button>
            }
          />
        )}
      </Form>

      <ModalSelectionFlashSale
        visible={isChooseTimeFrameVisible}
        onOk={(flashSale) => {
          setSelectedFlashSale(flashSale)
          setIsChooseTimeFrameVisible(false)
        }}
        onCancel={() => setIsChooseTimeFrameVisible(false)}
      />

      <ModalSelectionProducts
        visible={isAddProductVisible}
        onOk={(products) => {
          buildProductListInputs(products)
          setIsAddProductVisible(false)
        }}
        onCancel={() => setIsAddProductVisible(false)}
        selectedRows={selectedProductRowKeys}
        maxProductNumber={selectedFlashSale?.maxProductNumber}
      />
    </Content>
  )
}

interface TableFilterProps {
  selectedRowKeys?: string[]
  onUpdate?: (onlySelected: boolean, data: TableFilterInputs) => void
  onDelete?: (selectedRowKeys: string[]) => void
}

interface TableFilterInputs {
  groupPrice: number
  minMemberNumber: number
  maxQuantityPerUser: number
  timeToTeamUp: number
  maxQuantity: number
}

interface TableFilterErrors {
  promotionPercent?: {
    message: string
  }
}

const TableFilter: FC<TableFilterProps> = ({
                                             selectedRowKeys = [],
                                             onUpdate,
                                             onDelete,
                                           }) => {
  const [errors, setErrors] = useState<TableFilterErrors>({})
  const {control, setValue, getValues} = useForm<TableFilterInputs>({})

  const onApply = (onlySelected) => {
    if (!onValidateForm()) return
    const groupPrice = getValues('groupPrice')
    const minMemberNumber = getValues('minMemberNumber')
    const maxQuantityPerUser = getValues('maxQuantityPerUser')
    const timeToTeamUp = getValues('timeToTeamUp')
    const maxQuantity = getValues('maxQuantity')
    if (onUpdate) {
      onUpdate(onlySelected, {
        groupPrice,
        minMemberNumber,
        maxQuantityPerUser,
        timeToTeamUp,
        maxQuantity
      })
    }
  }

  const onValidateForm = (): boolean => {
    let isValid = true
    const maxQuantity = getValues('maxQuantity')
    // TODO show error maxQuantity exceeding the stocks number
    return isValid
  }

  return (
    <Card>
      <Form>
        <Row gutter={20} className="mb-3">
          <Col span={4}>
            <Typography.Text strong>Chỉnh sửa hàng loạt</Typography.Text>
          </Col>
          <Col span={2}>
            <Typography.Text strong>Giá mua chung</Typography.Text>
          </Col>
          <Col span={2}>
            <Typography.Text strong>SL CTV tối thiểu/nhóm</Typography.Text>
          </Col>
          <Col span={2}>
            <Typography.Text strong>SP tối đa/CTV</Typography.Text>
          </Col>
          <Col span={2}>
            <Typography.Text strong>Tgian ghép đơn</Typography.Text>
          </Col>
          <Col span={2}>
            <Typography.Text strong>Số sp áp dụng</Typography.Text>
          </Col>
          <Col span={8}/>
        </Row>
        <Row gutter={20}>
          <Col span={4}>
            Phân loại hàng đã chọn:{' '}
            <Typography.Text strong>{selectedRowKeys.length}</Typography.Text>
          </Col>
          <Col span={2}>
            <Controller
              control={control}
              name="groupPrice"
              render={({field}) => (
                <InputNumber
                  {...field}
                  onChange={(value) => {
                    setValue('groupPrice', value as number)
                    onValidateForm()
                  }}
                />
              )}
            />
          </Col>
          <Col span={2}>
            <Form.Item
              validateStatus={errors?.promotionPercent && 'error'}
              help={errors?.promotionPercent?.message}
            >
              <Controller
                control={control}
                name="minMemberNumber"
                render={({field}) => (
                  <InputNumber
                    {...field}
                    min={0}
                    max={100}
                    addonAfter="người"
                    onChange={(value) => {
                      setValue('minMemberNumber', value as number)
                      onValidateForm()
                    }}
                  />
                )}
              />
            </Form.Item>
          </Col>
          <Col span={2}>
            <Controller
              control={control}
              name="maxQuantityPerUser"
              render={({field}) => <InputNumber
                {...field}
                min={0}
                max={100}
                addonAfter="sp"
                onChange={(value) => {
                  setValue('maxQuantityPerUser', value as number)
                  onValidateForm()
                }}
              />}
            />
          </Col>
          <Col span={2}>
            <Controller
              control={control}
              name="timeToTeamUp"
              render={({field}) => <InputNumber
                {...field}
                min={0}
                max={100}
                addonAfter="giờ"
                onChange={(value) => {
                  setValue('timeToTeamUp', value as number)
                  onValidateForm()
                }}
              />}
            />
          </Col>
          <Col span={2}>
            <Controller
              control={control}
              name="maxQuantity"
              render={({field}) => <InputNumber {...field} />}
            />
          </Col>
          <Col span={8} offset={2}>
            <Row gutter={20}>
              <Col>
                <Button
                  type="default"
                  disabled={!selectedRowKeys.length}
                  onClick={() => onApply(true)}
                >
                  Cập nhật theo lựa chọn
                </Button>
              </Col>
              <Col>
                <Button
                  type="default"
                  onClick={() => onApply(false)}
                >
                  Cập nhật tất cả
                </Button>
              </Col>
              <Col>
                <Button
                  danger
                  onClick={() => onDelete && onDelete(selectedRowKeys)}
                >
                  Xóa
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </Card>
  )
}

const ProductListTitle: FC = () => {
  return (
    <>
      <Typography.Text>Sản phẩm tham gia</Typography.Text>
    </>
  )
}

interface IModalSelectionFlashSaleProps {
  visible: boolean
  onOk?: (flashSale: FlashSaleEntity) => void
  onCancel?: () => void
}

/**
 * Selection flash sale modal
 * @param visible Whether the dialog is visible
 * @param onOk Return selected flash sale
 * @param onCancel invisible dialog
 * @returns JSX
 */
const ModalSelectionFlashSale: FC<IModalSelectionFlashSaleProps> = ({
                                                                      visible,
                                                                      onOk,
                                                                      onCancel,
                                                                    }) => {
  const [flashSales, setFlashSales] = useState<FlashSaleEntity[]>([])
  const [selectedFlashSaleId, setSelectedFlashSaleId] = useState<number>(0)

  // Get flash sales
  const fetchFlashSales = (startTime: Date = new Date()) => {
    Promise.all([
      FlashSaleService.getManyBase({
        limit: 100,
        sort: ['name,ASC'],
        filter: [`startTime||$gt||${new Date(startTime).toISOString()}`],
      }),
      FlashSaleService.getManyBase({
        limit: 100,
        sort: ['name,ASC'],
        filter: [`startTime||$gt||${new Date(startTime).toISOString()}`],
        join: ['flashSaleDetail'],
      }),
    ])
      .then(([response, responseCheck]) => {
        const flashSalesJoined = responseCheck.data?.filter(
          (data) => data.flashSaleDetail?.length > 0
        )
        const flashSales = sortBy(
          response.data?.filter(
            (data) =>
              !flashSalesJoined?.find((fl) => fl.id === data.id) &&
              new Date(data.startTime).getDate() ===
              new Date(startTime).getDate() &&
              new Date(data.startTime).getMonth() ===
              new Date(startTime).getMonth() &&
              new Date(data.startTime).getFullYear() ===
              new Date(startTime).getFullYear()
          ),
          'startTime'
        )
        setFlashSales(flashSales)
      })
      .catch((error) => alertError(error))
  }

  const callback = () => {
    if (selectedFlashSaleId) {
      const flashSale = flashSales.find(({id}) => id === selectedFlashSaleId)
      onOk(flashSale)
    }
  }

  useEffect(() => {
    if (visible) fetchFlashSales()
  }, [visible])

  return (
    <>
      <ModalFlashSale
        visible={visible}
        onOk={onOk}
        onCancel={onCancel}
        callback={callback}
        fetchFlashSales={fetchFlashSales}
        flashSales={flashSales}
        onSelectFlashSale={setSelectedFlashSaleId}
        selectedFlashSaleId={selectedFlashSaleId}
      />
    </>
  )
}

enum EnumProductRowRender {
  discount = 'discount',
  promotion = 'promotion',
}

interface IProductRowRenderProps {
  variableName: string
  errors: {
    groupBuyingProducts?: {
      variantInputs?: {
        maxQuantity?: FieldError
        qty?: FieldError
      }[]
    }[]
  }
  control: Control<Inputs>
  recordProduct?: IProductInputs
  recordVariant?: IVariantInputs
  addonAfter?: string
  productInputIndex: number
  variantInputIndex?: number
  setValue,
  clearErrors
}

const ProductInputNumber: FC<IProductRowRenderProps> = ({
                                                          variableName,
                                                          errors,
                                                          control,
                                                          recordProduct,
                                                          addonAfter,
                                                          productInputIndex,
                                                          setValue,
                                                          clearErrors
                                                        }) => {

  const handleOnChange = (value: number) => {
    if (value) {
      // @ts-ignore
      setValue(`groupBuyingProducts.${productInputIndex}.${variableName}`, value)
      // @ts-ignore
      clearErrors(`groupBuyingProducts.${productInputIndex}.${variableName}`)
    }
  }
  return (
    <Form.Item
      required={true}
      validateStatus={
        errors.groupBuyingProducts?.[productInputIndex]?.[variableName]?.message && 'error'
      }
      help={
        errors.groupBuyingProducts?.[productInputIndex]?.[variableName]?.message
      }
      style={{marginBottom: 0}}
    >
      <Controller
        key={`${variableName}-${recordProduct.id}`}
        control={control}
        // @ts-ignore
        name={`groupBuyingProducts.${productInputIndex}.${variableName}`}
        render={({field}) => (
          <InputNumber
            {...field}
            min={0}
            addonAfter={addonAfter}
            onChange={(value) => handleOnChange(Number(value))}
          />
        )}
      />
    </Form.Item>
  )
}


const ProductVariantInputNumber: FC<IProductRowRenderProps> = ({
                                                                 variableName,
                                                                 errors,
                                                                 control,
                                                                 recordVariant,
                                                                 productInputIndex,
                                                                 variantInputIndex,
                                                               }) => {

  const handleOnChange = (value: number) => {
    if (value) {
      // @ts-ignore
      setValue(`groupBuyingProducts.${productInputIndex}.variantInputs.${variantInputIndex}.${variableName}`, value)
      // @ts-ignore
      clearErrors(`groupBuyingProducts.${productInputIndex}.variantInputs.${variantInputIndex}.${variableName}`)
    }
  }
  return (
    <Form.Item
      required={true}
      validateStatus={
        errors.groupBuyingProducts?.[productInputIndex]?.variantInputs?.[variantInputIndex]?.[variableName]?.message && 'error'
      }
      help={
        errors.groupBuyingProducts?.[productInputIndex]?.variantInputs?.[variantInputIndex]?.[variableName]?.message
      }
      style={{marginBottom: 0}}
    >
      <Controller
        key={`${variableName}-${recordVariant.id}`}
        control={control}
        // @ts-ignore
        name={`groupBuyingProducts.${productInputIndex}.variantInputs.${variantInputIndex}.${variableName}`}
        render={({field}) => (
          <InputNumber
            {...field}
            onChange={(value) => handleOnChange(Number(value))}
          />
        )}
      />
    </Form.Item>
  )
}

interface IModalSelectionProductProps {
  visible: boolean
  onOk?: (selectedProducts: ProductEntity[]) => void
  onCancel?: () => void
  selectedRows: string[]
  maxProductNumber: number
}

interface IModalSelectionProductInputs {
  searchText?: string
}

const ModalSelectionProducts: FC<IModalSelectionProductProps> = ({
                                                                   visible,
                                                                   onOk,
                                                                   onCancel,
                                                                   selectedRows,
                                                                   maxProductNumber,
                                                                 }: IModalSelectionProductProps) => {
  const user = useRecoilValue(authState)
  const countDatatable = useRecoilValue(countDataTable)
  const [selectedProducts, setSelectedProducts] = useState<ProductEntity[]>([])
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>()

  const [tableProductServiceParams, setTableProductServiceParams] =
    useState<ServiceParams>({
      sort: ['createdAt,DESC'],
      filter: [`merchantId||$eq||${user.merchantId}`],
      join: ['variants'],
    })

  const tableProductColumns: ColumnsType<ProductEntity> = [
    {
      dataIndex: 'name',
      title: 'Sản phẩm',
      width: 300,
      render: (value, record) => {
        return (
          <List.Item key={record.id}>
            <List.Item.Meta
              avatar={<Avatar src={record.images?.[0]}/>}
              title={<a>{record.name}</a>}
              description={record.shortDescription}
            />
          </List.Item>
        )
      },
    },
  ]

  const {
    control: productControl,
    setValue: setProductValue,
    handleSubmit: handleSubmitSearchProducts,
  } = useForm<IModalSelectionProductInputs>({
    defaultValues: {
      searchText: '',
    },
  })
  const [form] = Form.useForm()

  const onSubmitProductForm: SubmitHandler<IModalSelectionProductInputs> = (
    data
  ) => {
    const filter = []
    if (data?.searchText) {
      filter.push(`name||$contL||${data?.searchText}`)
    }
    filter.push(`merchantId||$eq||${user.merchantId}`)

    setTableProductServiceParams((prevState) => ({
      ...prevState,
      filter: filter,
      join: ['variants'],
    }))
  }

  const onResetSearchProducts = () => {
    setProductValue('searchText', '')
    setTableProductServiceParams({
      filter: [`merchantId||$eq||${user.merchantId}`],
      sort: ['createdAt,DESC'],
      join: ['variants'],
    })
  }

  useEffect(() => {
    if (visible) {
      setSelectedProducts([])
    }
    setSelectedRowKeys(selectedRows)
  }, [visible])

  return (
    <>
      <Modal
        title="Chọn Sản Phẩm"
        width={'960px'}
        visible={visible}
        onOk={() => onOk && onOk(selectedProducts)}
        onCancel={() => onCancel && onCancel()}
      >
        <Card>
          <Form
            form={form}
            autoComplete={'off'}
            onFinish={handleSubmitSearchProducts(onSubmitProductForm)}
            onReset={onResetSearchProducts}
          >
            <Row gutter={[16, 16]}>
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form.Item label={'Nhập tên'}>
                  <Controller
                    control={productControl}
                    name={'searchText'}
                    render={({field}) => (
                      <Input {...field} placeholder="Nhập tên sản phẩm"/>
                    )}
                  />
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} className={'text-right'}>
                <Form.Item>
                  <Space>
                    <Button htmlType={'reset'}>Đặt lại</Button>
                    <Button type={'primary'} htmlType={'submit'}>
                      Tìm kiếm
                    </Button>
                  </Space>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>

        {user.merchantId && (
          <Card
            title={
              <span className={'text-2xl'}>{countDatatable} sản phẩm</span>
            }
            extra={`Đã chọn ${selectedRowKeys?.length} sản phẩm`}
          >
            <DataTable
              service={ProductsService.getManyBase}
              serviceParams={tableProductServiceParams}
              columns={tableProductColumns}
              rowKey={(record) => `product-${record.id}`}
              rowSelection={{
                type: 'checkbox',
                onSelectAll: (selected, selectedRows, changeRows) => {
                  if (selected) {
                    setSelectedProducts((prevState) =>
                      uniqBy([...prevState, ...selectedRows], 'id')
                    )
                    setSelectedRowKeys((prevState) =>
                      uniq([
                        ...prevState,
                        ...selectedRows.map((row) => `product-${row.id}`),
                      ])
                    )
                  } else {
                    setSelectedProducts((prevState) => {
                      changeRows.map((row) => {
                        const index = prevState.findIndex(
                          (product) => product.id === row.id
                        )
                        if (index > -1) {
                          prevState.splice(index, 1)
                        }
                      })

                      return prevState
                    })
                    setSelectedRowKeys((prevState) => {
                      changeRows.map((row) => {
                        const index = prevState.findIndex(
                          (key) => key === `product-${row.id}`
                        )
                        if (index > -1) {
                          prevState.splice(index, 1)
                        }
                      })
                      return prevState
                    })
                  }
                },
                onSelect: (record, selected) => {
                  if (selected) {
                    setSelectedProducts((prevState) =>
                      uniqBy([...prevState, record], 'id')
                    )
                    setSelectedRowKeys((prevState) =>
                      uniq([...prevState, `product-${record.id}`])
                    )
                  } else {
                    setSelectedProducts((prevState) => {
                      const index = prevState.findIndex(
                        (product) => product.id === record.id
                      )
                      if (index > -1) {
                        prevState.splice(index, 1)
                      }
                      return prevState
                    })
                    setSelectedRowKeys((prevState) => {
                      const index = prevState.findIndex(
                        (key) => key === `product-${record.id}`
                      )
                      if (index > -1) {
                        prevState.splice(index, 1)
                      }
                      return prevState
                    })
                  }
                },
                onChange: (
                  selectedRowKeys: React.Key[],
                  selectedRows: ProductEntity[]
                ) => {
                  setSelectedProducts((prevState) =>
                    uniqBy([...prevState, ...selectedRows], 'id')
                  )
                  setSelectedRowKeys((prevState) =>
                    uniq([...prevState, ...selectedRowKeys])
                  )
                },
                selectedRowKeys: selectedRowKeys,
                getCheckboxProps: (record) => ({
                  disabled: selectedRows.find(
                    (key) => Number(key.split('-')[1]) === record.id
                  ),
                }),
              }}
              scroll={{y: 300}}
            />
          </Card>
        )}
      </Modal>
    </>
  )
}

export default Index

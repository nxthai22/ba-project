import Content from 'components/layout/AdminLayout/Content'
import GroupBuying from 'components/admin/Merchant/GroupBuying'

const Index = () => {
  return (
    <Content
      title='Danh sách mua chung'
      endButtons={[
        {
          text: 'Thêm mới',
          href: '/merchant/group-buying/create',
          type: 'primary',
          visible: true,
        },
      ]}
    >
      <GroupBuying />
    </Content>
  )
}

export default Index

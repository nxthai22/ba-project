import Content from 'components/layout/AdminLayout/Content'
import DataTable from 'components/common/DataTable'
import FormItem from 'components/common/FormItem'
import { Button, Card, Col, Form, Input, Row, Space } from 'antd'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import React, { FC, useState } from 'react'
import { ServiceParams } from 'utils/interfaces'
import { ColumnsType } from 'antd/es/table'
import { MerchantsService, ProductEntity } from 'services'
import { formatDate, hasPermission } from 'utils'
import { useRecoilValue } from 'recoil'
import { authState, countDataTable } from 'recoil/Atoms'

interface Inputs {
  name?: string
}

const Index: FC = () => {
  const [title] = useState<string>('Quản lý Nhà cung cấp')
  const user = useRecoilValue(authState)
  const countDatatable = useRecoilValue(countDataTable)
  const { control, handleSubmit, reset } = useForm<Inputs>({
    defaultValues: {
      name: '',
    },
  })
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['createdAt,DESC'],
    // join : ['productCategory']
  })
  const columns: ColumnsType<ProductEntity> = [
    {
      dataIndex: 'code',
      title: 'Mã nhà cung cấp ',
    },
    {
      dataIndex: 'name',
      title: 'Tên nhà cung cấp',
    },
    {
      dataIndex: 'createdAt',
      title: 'Ngày tạo',
      render: (value) => formatDate(value),
    },
  ]

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const filter = []
    const or = []
    if (data?.name)
      filter.push(`name||$contL||${data?.name}`) &&
        or.push(`code||$contL||${data?.name}`)

    setTableServiceParams((prevState) => ({
      ...prevState,
      filter: filter,
      or: or,
    }))
  }

  const onReset = () => {
    reset()
    setTableServiceParams({
      sort: ['createdAt,DESC'],
    })
  }
  return (
    <Content
      title={title}
      endButtons={[
        {
          text: 'Thêm mới',
          href: '/merchant/create',
          type: 'primary',
          visible: hasPermission(user, ['']),
        },
      ]}
    >
      <Card>
        <Form
          autoComplete={'off'}
          onFinish={handleSubmit(onSubmit)}
          onReset={onReset}
          colon={false}
        >
          <Row gutter={[8, 8]}>
            <Col xs={24} sm={24} md={24} lg={12}>
              <Form.Item label={'Tên/mã nhà cung cấp'}>
                <Controller
                  control={control}
                  name={'name'}
                  render={({ field }) => (
                    <Input
                      {...field}
                      placeholder={'Nhập Tên/mã nhà cung cấp để tìm kiếm'}
                    />
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: 'right' }}>
              <FormItem>
                <Space>
                  <Button htmlType={'reset'}>Đặt lại</Button>
                  <Button type={'primary'} htmlType={'submit'}>
                    Tìm kiếm
                  </Button>
                </Space>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Card>
      <Card
        title={<span className={'text-2xl'}>{countDatatable} Đối tác</span>}
      >
        <DataTable
          service={MerchantsService.getManyBase}
          serviceParams={tableServiceParams}
          deleteService={MerchantsService.deleteOneBase}
          columns={columns}
          action={['edit', 'delete']}
          path={'/merchant'}
        />
      </Card>
    </Content>
  )
}
export default Index

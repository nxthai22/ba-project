import React, { FC, useState } from 'react'
import { useRecoilValue } from 'recoil'
import { authState, countDataTable } from '../../../recoil/Atoms'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { ServiceParams } from '../../../utils/interfaces'
import { ColumnsType } from 'antd/es/table'
import {
  ConfigEntity,
  ConfigService,
} from '../../../services'
import { formatDate } from '../../../utils'
import { Button, Card, Col, Form, Input, Row, Space } from 'antd'
import FormItem from '../../../components/common/FormItem'
import DataTable from '../../../components/common/DataTable'
import Content from '../../../components/layout/AdminLayout/Content'

interface Inputs {
  name?: string
}

const Index: FC = () => {
  const [title] = useState<string>('Quản lý cấu hình')
  const user = useRecoilValue(authState)
  const countDatatable = useRecoilValue(countDataTable)
  const { control, handleSubmit, reset } = useForm<Inputs>({
    defaultValues: {
      name: '',
    },
  })
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['createdAt,DESC'],
    filter: [`merchantId||eq||${user.merchantId}`],
  })
  const columns: ColumnsType<ConfigEntity> = [
    {
      dataIndex: 'name',
      title: 'Tên cấu hình',
    },
    {
      dataIndex: 'key',
      title: 'Key cấu hình',
    },
    {
      dataIndex: 'createdAt',
      title: 'Ngày tạo',
      render: (value) => formatDate(value),
    },
    {
      dataIndex: 'startDate',
      title: 'Ngày bắt đầu',
      render: (value) => {
        if (value) return formatDate(value)
        return ''
      },
    },
    {
      dataIndex: 'endDate',
      title: 'Ngày kết thúc',
      render: (value) => {
        if (value) return formatDate(value)
        return ''
      },
    },
    {
      dataIndex: 'note',
      title: 'Ghi chú',
    },
  ]
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const filter = [`merchantId||eq||${user.merchantId}`]
    if (data?.name) filter.push(`name||$contL||${data?.name}`)

    setTableServiceParams((prevState) => ({
      ...prevState,
      filter: filter,
    }))
  }
  const onReset = () => {
    reset()
    setTableServiceParams({
      sort: ['createdAt,DESC'],
      filter: [`merchantId||eq||${user.merchantId}`],
    })
  }
  return (
    <Content
      title={title}
      endButtons={[
        {
          text: 'Thêm mới',
          href: '/merchant/config/create',
          type: 'primary',
          visible: true,
        },
      ]}
    >
      <Card>
        <Form
          autoComplete={'off'}
          onFinish={handleSubmit(onSubmit)}
          onReset={onReset}
          colon={false}
        >
          <Row gutter={[8, 8]}>
            <Col xs={24} sm={24} md={24} lg={12}>
              <Form.Item label={'Tên cấu hình'}>
                <Controller
                  control={control}
                  name={'name'}
                  render={({ field }) => (
                    <Input
                      {...field}
                      placeholder={'Nhập cấu hình để tìm kiếm'}
                    />
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: 'right' }}>
              <FormItem>
                <Space>
                  <Button htmlType={'reset'}>Đặt lại</Button>
                  <Button type={'primary'} htmlType={'submit'}>
                    Tìm kiếm
                  </Button>
                </Space>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Card>
      <Card
        title={<span className={'text-2xl'}>{countDatatable} Cấu hình</span>}
      >
        <DataTable
          service={ConfigService.getManyBase}
          serviceParams={tableServiceParams}
          columns={columns}
          action={['edit', 'delete']}
          path={'/merchant/config'}
        />
      </Card>
    </Content>
  )
}
export default Index

import {
  ConfigEntity,
  ConfigMerchantCommission,
  ConfigService,
  EnumConfigEntityKey,
  EnumRoleEntityType,
  UsersService,
} from '../../../services'
import React, { FC, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { authState, loadingState } from '../../../recoil/Atoms'
import Content from 'components/layout/AdminLayout/Content'
import {
  Controller,
  SubmitHandler,
  useFieldArray,
  useForm,
  useWatch,
} from 'react-hook-form'
import { Button, Card, Col, Form, Input, Row, Select } from 'antd'
import { alertError, modifyEntity } from '../../../utils'
import _ from 'lodash'
import { DeleteOutlined } from '@ant-design/icons'
import InputNumber from '../../../components/common/InputNumber'
import FormItem from '../../../components/common/FormItem'
import DatePicker from 'components/common/DatePicker'
import FooterBar from '../../../components/layout/AdminLayout/FooterBar'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { compareAsc } from 'date-fns'

interface Inputs {
  merchant: ConfigEntity
  configCommissions: MerchantCommission[]
  dates: Date[]
}

interface MerchantCommission {
  index?: number
  key?: EnumConfigEntityKey
  name?: string
  min?: number
  max?: number
  value?: number
  valueType?: string
  startDate?: Date
  endDate?: Date
  note?: string
}

const schema = yup.object().shape({
  dates: yup.array().length(2).required('Chưa chọn khoảng ngày áp dụng'),
})

const MerchantConfigDetail: FC = () => {
  const [title, setTitle] = useState<string>('Tạo mới cấu hình')
  const router = useRouter()
  const user = useRecoilValue(authState)
  const setIsLoading = useSetRecoilState(loadingState)
  const [disabledRangePicker, setDisabledRangePicker] = useState<Boolean[]>([
    false,
    false,
  ])
  const {
    register,
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm<Inputs>({
    resolver: yupResolver(schema),
    defaultValues: {
      merchant: {
        key: EnumConfigEntityKey.revenue,
      },
      dates: [new Date(), new Date()],
      configCommissions: [
        {
          index: 1,
          min: 0,
          max: 0,
          value: 0,
          valueType: 'percent',
          startDate: new Date(),
          endDate: new Date(),
        },
      ],
    },
  })
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'configCommissions',
  })

  useEffect(() => {
    setIsLoading(true)
    if (router?.query?.id && router?.query?.id !== 'create') {
      setTitle('Cập nhật cấu hình thưởng/hoa hồng')
      ConfigService.getOneBase({
        id: Number(router?.query?.id),
      })
        .then((response) => {
          if (response) {
            const values = _.sortBy(response.value, ['min'])
            const merchantConfigs: MerchantCommission[] = []
            values?.map((config, index) => {
              merchantConfigs.push({
                index: index,
                key: response.key,
                name: response.name,
                min: config.min,
                max: config.max,
                startDate: new Date(response.startDate),
                endDate: new Date(response.endDate),
                note: response.note,
                value: config.value,
                valueType: config.valueType,
              })
              setValue(`configCommissions.${index}.key`, response.key)
              setValue(`configCommissions.${index}.name`, response.name)
              setValue(`configCommissions.${index}.min`, config.min)
              setValue(`configCommissions.${index}.max`, config.max)
              setValue(`configCommissions.${index}.note`, response.note)
              setValue(`configCommissions.${index}.value`, config.value)
              setValue(`configCommissions.${index}.valueType`, config.valueType)
            })
            setValue('configCommissions', merchantConfigs)
          }
          setValue('merchant', response)
          setValue('dates', [
            new Date(response.startDate),
            new Date(response.endDate),
          ])
          setIsLoading(false)
          let isDisabled = [false, false]
          if (compareAsc(new Date(response?.startDate), new Date()) <= 0) {
            isDisabled[0] = true
          }
          if (compareAsc(new Date(response?.endDate), new Date()) <= 0) {
            isDisabled[1] = true
          }
          setDisabledRangePicker(isDisabled)
        })
        .catch((e) => {
          alertError(e)
          setIsLoading(false)
        })
    } else {
      setTitle('Thêm mới cấu hình thưởng/hoa hồng')
      setIsLoading(false)
      setDisabledRangePicker([false, false])
    }
  }, [router])
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const startDate = new Date(data.dates?.[0])
    const endDate = new Date(data.dates?.[1])
    setValue('merchant.startDate', startDate)
    setValue('merchant.endDate', endDate)
    const configMerchants = data.configCommissions?.map((config) => {
      return {
        productIds: [],
        min: config.min,
        max: config.max,
        value: config.value,
        valueType: config.valueType,
      }
    })
    setValue('merchant.value', configMerchants)
    setValue('merchant.merchantId', user.merchantId)
    if (!data?.merchant?.id) {
      setValue(
        'merchant.name',
        `Cấu hình thưởng doanh thu tháng ${
          startDate?.getMonth() + 1
        }/${startDate?.getFullYear()} đến ${
          endDate?.getMonth() + 1
        }/${endDate?.getFullYear()}`
      )
    }
    modifyEntity(
      ConfigService,
      data.merchant,
      title + ' thành công',
      (response) => {
        return router.push(`/merchant/config`)
      }
    ).then()
  }
  return (
    <Content title={title} onBack={() => router.push('/merchant/config')}>
      <Form onFinish={handleSubmit(onSubmit)}>
        <Card
          title={'Thưởng doanh số'}
          extra={
            <Row gutter={15}>
              <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                <Button
                  type={'primary'}
                  onClick={() =>
                    append({
                      min: 0,
                      max: 0,
                      value: 0,
                      valueType: 'percent',
                      note: '',
                    })
                  }
                >
                  Thêm
                </Button>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                <Button htmlType={'submit'} type={'primary'}>
                  Lưu
                </Button>
              </Col>
            </Row>
          }
        >
          <Row gutter={[15, 15]}>
            <Col xs={24} sm={24} md={24} lg={12} xl={6}>
              <FormItem required={true}>
                <Controller
                  control={control}
                  name={'merchant.key'}
                  render={({ field }) => (
                    <Select size={'middle'} {...field}>
                      <Select.Option value={EnumConfigEntityKey.revenue}>
                        Thưởng tháng
                      </Select.Option>
                      <Select.Option
                        value={EnumConfigEntityKey.revenue_quarter}
                      >
                        Thưởng quý
                      </Select.Option>
                    </Select>
                  )}
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={18}>
              <FormItem required={true}>
                <Controller
                  control={control}
                  name={'dates'}
                  render={({ field }) => (
                    <DatePicker.RangePicker
                      format={'d/MM/y'}
                      {...field}
                      disabled={disabledRangePicker}
                    />
                  )}
                />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col xs={24} sm={24} md={24} lg={12} xl={2}>
              <h4>Mức</h4>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={8}>
              <h4 style={{ textAlign: 'center' }}>Doanh thu (VNĐ)</h4>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={5}>
              <h4 style={{ textAlign: 'center' }}>Hoa hồng</h4>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={7}>
              <h4 style={{ textAlign: 'center' }}>Ghi chú</h4>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={1}></Col>
          </Row>
          {fields.map((field, index) => (
            <Row gutter={[10, 15]} style={{ marginTop: 15 }} key={field.id}>
              <Col
                xs={24}
                sm={24}
                md={24}
                lg={12}
                xl={1}
                style={{ display: 'flex', alignItems: 'center' }}
              >
                <p style={{ marginBottom: 0 }}>{index + 1}</p>
              </Col>
              <Col style={{ display: 'flex', alignItems: 'center' }}>
                <p style={{ marginBottom: 0 }}>Từ</p>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={4}>
                <Controller
                  control={control}
                  name={`configCommissions.${index}.min`}
                  render={({ field }) => (
                    <InputNumber
                      {...field}
                      size="middle"
                      className={'w-full'}
                    />
                  )}
                />
              </Col>
              <Col style={{ display: 'flex', alignItems: 'center' }}>
                <p style={{ marginBottom: 0 }}>đến</p>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={4}>
                <Controller
                  control={control}
                  name={`configCommissions.${index}.max`}
                  render={({ field }) => (
                    <InputNumber
                      {...field}
                      size="middle"
                      className={'w-full'}
                    />
                  )}
                />
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={5}>
                <Input.Group>
                  <Row>
                    <Col span={16}>
                      <Controller
                        control={control}
                        name={`configCommissions.${index}.value`}
                        render={({ field }) => (
                          <InputNumber
                            {...field}
                            size="middle"
                            className={'w-full'}
                          />
                        )}
                      />
                    </Col>
                    <Col span={8}>
                      <Controller
                        control={control}
                        name={`configCommissions.${index}.valueType`}
                        render={({ field }) => (
                          <Select size={'middle'} {...field}>
                            <Select.Option value={'percent'}>%</Select.Option>
                            <Select.Option value={'amount'}>VNĐ</Select.Option>
                          </Select>
                        )}
                      />
                    </Col>
                  </Row>
                </Input.Group>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={6}>
                <Controller
                  control={control}
                  name={`merchant.note`}
                  render={({ field }) => <Input {...field} size="middle" />}
                />
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={2}>
                <Button>
                  <DeleteOutlined onClick={() => remove(index)} />
                </Button>
              </Col>
            </Row>
          ))}
        </Card>
      </Form>
    </Content>
  )
}
export default MerchantConfigDetail

import React, { FC, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import Content from 'components/layout/AdminLayout/Content'
import { Controller, SubmitHandler, useFieldArray, useForm, useWatch } from 'react-hook-form'
import { Button, Card, Col, Form, Input, InputNumber, Row, Select } from 'antd'
import _ from 'lodash'
import { DeleteOutlined } from '@ant-design/icons'
import DatePicker from 'components/common/DatePicker'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { compareAsc } from 'date-fns'
import { BankEntity, BankService, ConfigEntity, ConfigService, EnumConfigEntityKey } from 'services'
import { authState, loadingState } from '../../recoil/Atoms'
import { alertError, modifyEntity } from '../../utils'
import FormItem from 'components/common/FormItem'

interface Inputs {
  merchant: ConfigEntity
  partnerConfigs: PartnerConfig[]
  dates: Date[]
  registerConfig: RegisterConfig
  bankConfig: BankInfoConfig
}

interface PartnerConfig {
  index?: number
  key?: EnumConfigEntityKey
  min?: number
  max?: number
  value?: number
  valueType?: string
  note?: string
  rangeTime?: number
}

interface RegisterConfig {
  f0?: number
  f1: number
}

interface BankInfoConfig {
  bankId?: number
  bankBranch?: string
  bankNumber?: string
  bankAccount?: string
  accountantEmails?: string
}

const schema = yup.object().shape({
  dates: yup.array().length(2).required('Chưa chọn khoảng ngày áp dụng')
})

const ConfigDetail: FC = () => {
  const [title, setTitle] = useState<string>('Tạo mới cấu hình')
  const router = useRouter()
  const user = useRecoilValue(authState)
  const setIsLoading = useSetRecoilState(loadingState)
  const [disabledRangePicker, setDisabledRangePicker] = useState<boolean[]>([false, false])
  const [isEdit, setIsEdit] = useState<boolean>(false)
  const [banks, setBanks] = useState<BankEntity[]>([])
  const {
    register,
    control,
    handleSubmit,
    setValue,
    formState: { errors }
  } = useForm<Inputs>({
    resolver: yupResolver(schema),
    defaultValues: {
      merchant: {
        key: EnumConfigEntityKey.register
      },
      dates: [new Date(), new Date()],
      partnerConfigs: [
        {
          index: 1,
          min: 0,
          max: 0,
          value: 0,
          valueType: 'percent',
          rangeTime: 1
        }
      ]
    }
  })
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'partnerConfigs'
  })
  const watchConfigKey = useWatch({
    control,
    name: 'merchant.key'
  })
  useEffect(() => {
    watchConfigKey === EnumConfigEntityKey.bank_info &&
    BankService.getManyBase({
      sort: ['shortName,ASC'],
      limit: 100
    }).then(response => {
      if (response) {
        setBanks(response.data)
      }
    })
  })
  useEffect(() => {
    setIsLoading(true)
    if (router?.query?.id && router?.query?.id !== 'create') {
      setTitle('Cập nhật cấu hình Ushare')
      ConfigService.getOneBase({
        id: Number(router?.query?.id)
      })
        .then((response) => {
          if (response) {
            switch (response.key) {
              case EnumConfigEntityKey.amount_collaborator:
                const values = _.sortBy(response.value, ['min'])
                const partnerConfigs: PartnerConfig[] = []
                values?.map((config: PartnerConfig, index) => {
                  partnerConfigs.push({
                    index: index,
                    key: response.key,
                    min: config.min,
                    max: config.max,
                    note: response.note,
                    value: config.value,
                    valueType: config.valueType
                  })
                  setValue(`partnerConfigs.${index}.key`, response.key)
                  setValue(`partnerConfigs.${index}.min`, config.min)
                  setValue(`partnerConfigs.${index}.max`, config.max)
                  setValue(`partnerConfigs.${index}.note`, response.note)
                  setValue(`partnerConfigs.${index}.value`, config.value)
                  setValue(`partnerConfigs.${index}.valueType`, config.valueType)
                  setValue(`partnerConfigs.${index}.rangeTime`, config.rangeTime)
                })
                setValue('partnerConfigs', partnerConfigs)
                break
              case EnumConfigEntityKey.first_order:
              case EnumConfigEntityKey.register:
                setValue('registerConfig', response.value)
                break
              case EnumConfigEntityKey.bank_info:
                setValue('bankConfig', response.value)
                break
            }
            setIsEdit(true)
          }
          setValue('merchant', response)
          setValue('dates', [new Date(response.startDate), new Date(response.endDate)])
          setIsLoading(false)
          let isDisabled = [false, false]
          if (compareAsc(new Date(response?.startDate), new Date()) <= 0) {
            isDisabled[0] = true
          }
          if (compareAsc(new Date(response?.endDate), new Date()) <= 0) {
            isDisabled[1] = true
          }
          setDisabledRangePicker(isDisabled)
        })
        .catch((e) => {
          alertError(e)
          setIsLoading(false)
        })
    } else {
      setTitle('Thêm mới cấu hình Ushare')
      setIsEdit(false)
      setIsLoading(false)
      setDisabledRangePicker([false, false])
    }
  }, [router])
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const startDate = new Date(data.dates?.[0])
    const endDate = new Date(data.dates?.[1])
    setValue('merchant.startDate', startDate)
    setValue('merchant.endDate', endDate)
    let value
    switch (data.merchant.key) {
      case EnumConfigEntityKey.amount_collaborator:
        value = data.partnerConfigs?.map(config => {
          return {
            min: config.min,
            max: config.max,
            value: config.value,
            valueType: config.valueType,
            rangeTime: config.rangeTime
          }
        })
        break
      case EnumConfigEntityKey.register:
      case EnumConfigEntityKey.first_order:
        value = data.registerConfig
        break
      case EnumConfigEntityKey.bank_info:
        value = data.bankConfig
        break
    }
    setValue('merchant.value', value)
    setValue('merchant.merchantId', user.merchantId)
    if (!data?.merchant?.id) {
      let name = ''
      switch (watchConfigKey) {
        case EnumConfigEntityKey.amount_collaborator:
          name = `Cấu hình thưởng doanh thu tháng ${startDate?.getMonth()}/${startDate?.getFullYear()} đến ${endDate?.getMonth()}/${endDate?.getFullYear()}`
          break
        case EnumConfigEntityKey.register:
          name = `Thưởng đăng ký thành công tháng ${(startDate ?? new Date()).getMonth()}/${(startDate ?? new Date()).getFullYear()}`
          break
        case EnumConfigEntityKey.first_order:
          name = `Thưởng phát sinh đơn thành công tháng ${(startDate ?? new Date()).getMonth()}/${(startDate ?? new Date()).getFullYear()}`
          break
        case EnumConfigEntityKey.bank_info:
          name = `Thông tin ngân hàng ${(startDate ?? new Date()).getMonth()}/${(startDate ?? new Date()).getFullYear()}`
          break
      }
      setValue('merchant.name', name)
    }
    modifyEntity(
      ConfigService,
      data.merchant,
      title + ' thành công',
      (response) => {
        return router.push(`/config`)
      }
    ).then()
  }
  return (
    <Content title={title} onBack={() => router.push('/config')}>
      <Form onFinish={handleSubmit(onSubmit)}>
        <Card
          title={title}
          extra={
            <Row gutter={15}>
              {watchConfigKey === EnumConfigEntityKey.amount_collaborator &&
              <Col xs={24} sm={24} md={24} lg={12} xl={14}>
                <Button
                  type={'primary'}
                  onClick={() =>
                    append({
                      min: 0,
                      max: 0,
                      value: 0,
                      valueType: 'percent',
                      note: ''
                    })
                  }
                >Thêm</Button>
              </Col>
              }
              <Col xs={24} sm={24} md={24} lg={12} xl={10}>
                <Button htmlType={'submit'} type={'primary'}>
                  Lưu
                </Button>
              </Col>
            </Row>
          }
        >
          <Row gutter={[15, 15]}>
            <Col xs={24} sm={24} md={24} lg={12} xl={8}>
              <FormItem required={true}>
                <Controller
                  control={control}
                  name={'merchant.key'}
                  render={({ field }) => (
                    <Select
                      size={'middle'}
                      {...field}
                      disabled={isEdit}
                    >
                      <Select.Option value={EnumConfigEntityKey.register}>
                        Thưởng chia sẻ khi đăng ký
                      </Select.Option>
                      <Select.Option value={EnumConfigEntityKey.first_order}>
                        Thưởng chia sẻ khi phát sinh đơn thành công
                      </Select.Option>
                      <Select.Option value={EnumConfigEntityKey.amount_collaborator}>
                        Thưởng chiết khấu theo số lượng CTV
                      </Select.Option>
                      <Select.Option value={EnumConfigEntityKey.bank_info}>
                        Thông tin ngân hàng
                      </Select.Option>
                    </Select>
                  )}
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={8}>
              <FormItem required={true}>
                <Controller
                  control={control}
                  name={'dates'}
                  render={({ field }) => (
                    <DatePicker.RangePicker
                      {...field}
                      format={'d/MM/y'}
                      disabled={disabledRangePicker}
                    />
                  )}
                />
              </FormItem>
            </Col>
          </Row>
          {/*Thưởng đăng ký và đơn đầu tiên đơn đầu tiên*/}
          {[EnumConfigEntityKey.register, EnumConfigEntityKey.first_order].includes(watchConfigKey) &&
          <Row gutter={15}>
            <Col xs={24} sm={24} md={24} lg={12} xl={24}>
              <FormItem
                label={
                  <span>{watchConfigKey === EnumConfigEntityKey.register ? 'Thưởng sau đăng ký (VNĐ)' : 'Thưởng đơn đầu tiên (VNĐ)'}</span>}
                labelCol={{ span: 6 }}
                labelAlign={'left'}
                required={true}
              >
                <Controller
                  control={control}
                  name={'registerConfig.f0'}
                  render={({ field }) => (
                    <InputNumber
                      {...field}
                      size='middle'
                      className={'w-full'}
                    />
                  )}
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={24}>
              <FormItem
                label={
                  <span>{watchConfigKey === EnumConfigEntityKey.register ? 'F1 đăng ký thành công (VNĐ)' : 'F1 phát sinh đơn thành công (VNĐ)'}</span>}
                labelCol={{ span: 6 }}
                labelAlign={'left'}
                required={true}
              >
                <Controller
                  control={control}
                  name={'registerConfig.f1'}
                  render={({ field }) => (
                    <InputNumber
                      {...field}
                      size='middle'
                      className={'w-full'}
                    />
                  )}
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={24}>
              <FormItem
                label={<span>Ghi chú</span>}
                labelCol={{ span: 6 }}
                labelAlign={'left'}
                required={true}
              >
                <Controller
                  control={control}
                  name={'merchant.note'}
                  render={({ field }) => (
                    <Input
                      {...field}
                      size='middle'
                    />
                  )}
                />
              </FormItem>
            </Col>
          </Row>
          }
          {/*Cấu hình thông tin tài khoản ngân hàng*/}
          {watchConfigKey === EnumConfigEntityKey.bank_info &&
          <Row gutter={15}>
            <Col xs={24} sm={24} md={24} lg={12} xl={24}>
              <FormItem
                label={<span>Ngân hàng</span>}
                labelCol={{ span: 3 }}
                labelAlign={'left'}
                required={true}
              >
                <Controller
                  control={control}
                  name={'bankConfig.bankId'}
                  render={({ field }) =>
                    <Select
                      {...field}
                      size={'middle'}
                      className={'w-full'}
                    >
                      {banks && banks.map(bank => (
                        <Select.Option key={bank.id} value={bank.id}>
                          {bank.shortName}
                        </Select.Option>
                      ))}
                    </Select>
                  }
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={24}>
              <FormItem
                label={<span>Chi nhánh</span>}
                required={true}
                labelCol={{ span: 3 }}
                labelAlign={'left'}
              >
                <Controller
                  control={control}
                  name={'bankConfig.bankBranch'}
                  render={({ field }) =>
                    <Input
                      {...field}
                      size={'middle'}
                      className={'w-full'}
                    />
                  }
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={24}>
              <FormItem
                label={<span>Số tài khoản</span>}
                required={true}
                labelCol={{ span: 3 }}
                labelAlign={'left'}
              >
                <Controller
                  control={control}
                  name={'bankConfig.bankNumber'}
                  render={({ field }) =>
                    <Input
                      {...field}
                      size={'middle'}
                      className={'w-full'}
                    />
                  }
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={24}>
              <FormItem
                label={<span>Chủ tài khoản</span>}
                required={true}
                labelCol={{ span: 3 }}
                labelAlign={'left'}
              >
                <Controller
                  control={control}
                  name={'bankConfig.bankAccount'}
                  render={({ field }) =>
                    <Input
                      {...field}
                      size={'middle'}
                      className={'w-full'}
                    />
                  }
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={24}>
              <FormItem
                label={<span>Email</span>}
                labelCol={{ span: 3 }}
                labelAlign={'left'}
              >
                <Controller
                  control={control}
                  name={'bankConfig.accountantEmails'}
                  render={({ field }) =>
                    <Input
                      {...field}
                      size={'middle'}
                      className={'w-full'}
                    />
                  }
                />
              </FormItem>
            </Col>
          </Row>
          }
          {/*Thưởng giới thiệu*/}
          {watchConfigKey === EnumConfigEntityKey.amount_collaborator &&
          <>
            <Row>
              <Col xs={24} sm={24} md={24} lg={12} xl={2}>
                <h4>Mức</h4>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={6}>
                <h4 style={{ textAlign: 'center' }}>Số lượng CTV</h4>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={4}>
                <h4 style={{ textAlign: 'center' }}>Hoa hồng</h4>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={4}>
                <h4 style={{ textAlign: 'center' }}>Số tháng</h4>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={7}>
                <h4 style={{ textAlign: 'center' }}>Ghi chú</h4>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={1}></Col>
            </Row>
            {fields.map((field, index) => (
              <Row gutter={[10, 15]} style={{ marginTop: 15 }} key={field.id}>
                <Col xs={24} sm={24} md={24} lg={12} xl={1} style={{ display: 'flex', alignItems: 'center' }}>
                  <p style={{ marginBottom: 0 }}>{index + 1}</p>
                </Col>
                <Col style={{ display: 'flex', alignItems: 'center' }}>
                  <p style={{ marginBottom: 0 }}>Từ</p>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={3}>
                  <Controller
                    control={control}
                    name={`partnerConfigs.${index}.min`}
                    render={({ field }) => (
                      <InputNumber
                        {...field}
                        size='middle'
                        className={'w-full'}
                      />
                    )}
                  />
                </Col>
                <Col style={{ display: 'flex', alignItems: 'center' }}>
                  <p style={{ marginBottom: 0 }}>đến</p>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={3}>
                  <Controller
                    control={control}
                    name={`partnerConfigs.${index}.max`}
                    render={({ field }) => (
                      <InputNumber
                        {...field}
                        size='middle'
                        className={'w-full'}
                      />
                    )}
                  />
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={4}>
                  <Input.Group>
                    <Row>
                      <Col span={16}>
                        <Controller
                          control={control}
                          name={`partnerConfigs.${index}.value`}
                          render={({ field }) => (
                            <InputNumber
                              {...field}
                              size='middle'
                              className={'w-full'}
                            />
                          )}
                        />
                      </Col>
                      <Col span={8}>
                        <Controller
                          control={control}
                          name={`partnerConfigs.${index}.valueType`}
                          render={({ field }) => (
                            <Select
                              {...field}
                              size={'middle'}
                            >
                              <Select.Option value={'percent'}>
                                %
                              </Select.Option>
                              <Select.Option value={'amount'}>
                                VNĐ
                              </Select.Option>
                            </Select>
                          )}
                        />
                      </Col>
                    </Row>
                  </Input.Group>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={4}>
                  <Controller
                    control={control}
                    name={`partnerConfigs.${index}.rangeTime`}
                    render={({ field }) => (
                      <Input
                        {...field}
                        size='middle'
                      />
                    )}
                  />
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={6}>
                  <Controller
                    control={control}
                    name={`merchant.note`}
                    render={({ field }) => (
                      <Input
                        {...field}
                        size='middle'
                      />
                    )}
                  />
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={1}>
                  <Button>
                    <DeleteOutlined
                      onClick={() =>
                        remove(index)
                      } />
                  </Button>
                </Col>
              </Row>
            ))}
          </>
          }
        </Card>
      </Form>
    </Content>
  )
}
export default ConfigDetail
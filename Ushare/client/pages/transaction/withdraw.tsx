import React, { FC, useState } from 'react'
import { countDataTable } from 'recoil/Atoms'
import { useRecoilValue } from 'recoil'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { ColumnsType } from 'antd/es/table'

import { Button, Card, Col, Form, Input, Row, Space } from 'antd'
import DatePicker from 'components/common/DatePicker'
import { formatCurrency, formatDate, getStatusTransaction } from 'utils'
import FormItem from 'components/common/FormItem'
import Content from 'components/layout/AdminLayout/Content'
import { ServiceParams } from 'utils/interfaces'
import { endOfDay, startOfDay } from 'date-fns'
import TableAndModalTransaction from 'components/common/TableAndModalTransaction'
import { EnumUserTransactionEntityTransactionType, UserTransactionEntity } from '../../services'

interface Inputs {
  searchText?: string
  date?: [Date, Date]
}

const Withdraw: FC = () => {
  const [title] = useState<string>('Lịch sử giao dịch rút tiền')
  const countDatatable = useRecoilValue(countDataTable)
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['createdAt,DESC'],
    filter: [
      `transactionType||eq||${EnumUserTransactionEntityTransactionType.withdrawal}`,
      `createdAt||between||${startOfDay(new Date()).toISOString()},${endOfDay(new Date()).toISOString()}`
    ],
    join: ['user', 'modifiedUser']
  })
  const { control, handleSubmit, reset, setValue } = useForm<Inputs>({
    defaultValues: {
      searchText: '',
      date: [new Date(), new Date()]
    }
  })

  const columns: ColumnsType<UserTransactionEntity> = [
    {
      dataIndex: 'user',
      title: 'Khách hàng',
      render: (value) => value?.fullName
    },
    {
      dataIndex: 'user',
      title: 'Điện thoại',
      render: (value) => value?.tel
    },
    {
      dataIndex: 'bankName',
      title: 'Bank code'
    },
    {
      dataIndex: 'amount',
      title: 'Số tiền',
      align: 'right',
      render: (value) => formatCurrency(value)
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: (value) => getStatusTransaction(value)
    },
    {
      dataIndex: 'createdAt',
      title: 'Ngày tạo',
      align: 'center',
      render: (value) => formatDate(value)
    }
  ]
  const columnTransactionDetail: ColumnsType<UserTransactionEntity> = [
    {
      dataIndex: 'id',
      title: 'Mã giao dịch'
    },
    {
      dataIndex: 'amount',
      title: 'Số tiền',
      align: 'right',
      render: (value) => formatCurrency(value)
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: (value) => getStatusTransaction(value)
    },
    {
      dataIndex: 'createdAt',
      title: 'Ngày tạo',
      align: 'center',
      render: (value) => formatDate(value)
    }
  ]
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    // const filters: any[] = tableServiceParams.filter
    const filters: any[] = [`transactionType||eq||${EnumUserTransactionEntityTransactionType.withdrawal}`]
    const or = []
    Object.keys(data).map((key) => {
      if (data?.[key]) {
        if (key === 'date') {
          if (data?.date) {
            filters.push(
              `createdAt||between||${startOfDay(
                data?.date[0]
              ).toISOString()},${endOfDay(data?.date[1]).toISOString()}`
            )
          }
        } else if (key === 'searchText') {
          filters.push(`user.fullName||$contL||${data?.[key]}`)
          or.push(`user.tel||$contL||${data?.searchText}`)
        }
      }
    })
    setTableServiceParams((prevState) => ({
      ...prevState,
      filter: filters,
      or: or
    }))
  }
  const onReset = () => {
    reset()
    setValue('date',[new Date(),new Date()]);
    setTableServiceParams({
      sort: ['createdAt,DESC'],
      filter: [
        `transactionType||eq||${EnumUserTransactionEntityTransactionType.withdrawal}`,
        `createdAt||between||${startOfDay(new Date()).toISOString()},${endOfDay(new Date()).toISOString()}`
      ],
      join: ['user', 'modifiedUser']
    })
  }
  return (
    <Content title={title}>
      <Card>
        <Form
          autoComplete={'off'}
          onFinish={handleSubmit(onSubmit)}
          onReset={onReset}
          colon={false}
        >
          <Row gutter={[8, 8]}>
            <Col xs={24} sm={24} md={24} xl={8} lg={8}>
              <Form.Item label={'Tên KH/SĐT'}>
                <Controller
                  control={control}
                  name={'searchText'}
                  render={({ field }) => (
                    <Input
                      {...field}
                      placeholder={'Nhập tên khách hàng hoặc SĐT'}
                    />
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} xl={10} lg={10}>
              <Form.Item label={'Từ ngày'}>
                <Controller
                  control={control}
                  name={'date'}
                  render={({ field }) => (
                    <DatePicker.RangePicker
                      format={'d/MM/y'}
                      {...field}
                      style={{
                        width: '100%'
                      }}
                    />
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} xl={6} lg={6} className={'text-right'}>
              <FormItem>
                <Space>
                  <Button htmlType={'reset'}>Đặt lại</Button>
                  <Button type={'primary'} htmlType={'submit'}>
                    Tìm kiếm
                  </Button>
                </Space>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Card>
      <Card
        title={<span className={'text-2xl'}>{countDatatable} Giao dịch</span>}
      >
        <TableAndModalTransaction
          roleUser={'admin'}
          transactionType={EnumUserTransactionEntityTransactionType.withdrawal}
          redirectUrl={'/transaction/withdraw'}
          tableServiceParams={tableServiceParams}
          columns={columns}
          cardTitle={'Lịch sử rút tiền'}
          isDownloadable={true}
          columnTransactionDetail={columnTransactionDetail}
        />
      </Card>
    </Content>
  )
}
export default Withdraw

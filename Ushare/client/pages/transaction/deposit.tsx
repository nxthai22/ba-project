import React, { FC, useState } from 'react'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { ColumnsType } from 'antd/es/table'
import {
  EnumUserTransactionEntityTransactionType,
  UserTransactionEntity,
} from 'services'
import { Button, Card, Col, Form, Input, Row, Space } from 'antd'
import DatePicker from 'components/common/DatePicker'
import { formatCurrency, formatDate, getStatusTransaction } from 'utils'
import FormItem from 'components/common/FormItem'
import Content from 'components/layout/AdminLayout/Content'
import { ServiceParams } from 'utils/interfaces'
import { endOfDay, startOfDay } from 'date-fns'
import TableAndModalTransaction from 'components/common/TableAndModalTransaction'
import { useRecoilValue } from 'recoil'
import { authState, countDataTable } from 'recoil/Atoms'

interface Inputs {
  searchText?: string
  date?: [Date, Date]
}

const Deposit: FC = () => {
  const [title] = useState<string>('Lịch sử giao dịch nạp tiền')
  const countDatatable = useRecoilValue(countDataTable)
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['createdAt,DESC'],
    filter: [
      `transactionType||eq||${EnumUserTransactionEntityTransactionType.deposit}`,
      `createdAt||between||${startOfDay(new Date()).toISOString()},${endOfDay(new Date()).toISOString()}`
    ],
    join: ['user', 'modifiedUser'],
  })
  const { control, handleSubmit, reset, setValue } = useForm<Inputs>({
    defaultValues: {
      searchText: '',
      date: [new Date(), new Date()],
    },
  })
  const columns: ColumnsType<UserTransactionEntity> = [
    {
      dataIndex: 'user',
      title: 'Khách hàng',
      render: (value) => value?.fullName,
    },
    {
      dataIndex: 'user',
      title: 'Điện thoại',
      render: (value) => value?.tel,
    },
    {
      dataIndex: 'amount',
      title: 'Số tiền',
      align: 'right',
      render: (value) => formatCurrency(value),
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: (value) => getStatusTransaction(value),
    },
    {
      dataIndex: 'createdAt',
      title: 'Ngày tạo',
      align: 'center',
      render: (value) => formatDate(value),
    },
  ]
  const columnTransactionDetail: ColumnsType<UserTransactionEntity> = [
    {
      dataIndex: 'id',
      title: 'Mã giao dịch',
    },
    {
      dataIndex: 'amount',
      title: 'Số tiền',
      align: 'right',
      render: (value) => formatCurrency(value),
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: (value) => getStatusTransaction(value),
    },
    {
      dataIndex: 'createdAt',
      title: 'Ngày tạo',
      align: 'center',
      render: (value) => formatDate(value),
    },
  ]
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const filters: any[] = [
      `transactionType||eq||${EnumUserTransactionEntityTransactionType.deposit}`,
    ]
    const or = []
    Object.keys(data).map((key) => {
      if (data?.[key]) {
        if (key === 'date') {
          if (data?.date) {
            filters.push(
              `createdAt||between||${startOfDay(
                data?.date[0]
              ).toISOString()},${endOfDay(data?.date[1]).toISOString()}`
            )
          }
        }
        if (key === 'searchText') {
          filters.push(`user.fullName||$contL||${data?.[key]}`)
          or.push(`user.tel||$contL||${data?.searchText}`)
        }
      }
    })
    setTableServiceParams((prevState) => ({
      ...prevState,
      filter: filters,
      or: or,
      join: ['user', 'modifiedUser'],
    }))
  }
  const onReset = () => {
    reset()
    setValue('date',[new Date(),new Date()]);
    setTableServiceParams({
      sort: ['createdAt,DESC'],
      filter: [
        `transactionType||eq||${EnumUserTransactionEntityTransactionType.deposit}`,
        `createdAt||between||${startOfDay(new Date()).toISOString()},${endOfDay(new Date()).toISOString()}`
      ],
      join: ['user', 'modifiedUser'],
    });
  }
  return (
    <Content
      title={title}
      // endButtons={[
      //   {
      //     text: 'Thêm mới',
      //     href: '/transaction/topup/create',
      //     type: 'primary',
      //     visible: hasPermission(user, [
      //       'user-transaction_createOneBase',
      //       'user-transaction_updateOneBase',
      //     ]),
      //   },
      // ]}
    >
      <Card>
        <Form
          autoComplete={'off'}
          onFinish={handleSubmit(onSubmit)}
          onReset={onReset}
          colon={false}
        >
          <Row gutter={[8, 8]}>
            <Col xs={24} sm={24} md={24} xl={8} lg={8}>
              <Form.Item label={'Tên KH/SĐT'}>
                <Controller
                  control={control}
                  name={'searchText'}
                  render={({ field }) => (
                    <Input
                      {...field}
                      placeholder={'Nhập tên khách hàng hoặc SĐT'}
                    />
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} xl={10} lg={10}>
              <Form.Item label={'Từ ngày'}>
                <Controller
                  control={control}
                  name={'date'}
                  render={({ field }) => (
                    <DatePicker.RangePicker
                      format={'d/MM/y'}
                      {...field}
                      style={{
                        width: '100%',
                      }}
                    />
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} xl={6} lg={6} className={'text-right'}>
              <FormItem>
                <Space>
                  <Button htmlType={'reset'}>Đặt lại</Button>
                  <Button type={'primary'} htmlType={'submit'}>
                    Tìm kiếm
                  </Button>
                </Space>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Card>
      <Card
        title={<span className={'text-2xl'}>{countDatatable} Giao dịch</span>}
      >
        <TableAndModalTransaction
          roleUser={'admin'}
          transactionType={EnumUserTransactionEntityTransactionType.deposit}
          redirectUrl={'/transaction/deposit'}
          tableServiceParams={tableServiceParams}
          columns={columns}
          cardTitle={'Lịch sử nạp tiền'}
          isDownloadable={true}
          columnTransactionDetail={columnTransactionDetail}
        />
      </Card>
    </Content>
  )
}
export default Deposit

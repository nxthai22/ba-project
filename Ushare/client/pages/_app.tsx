import { NextPage } from 'next'
import { AppProps } from 'next/app'
import Head from 'next/head'
import { ConfigProvider } from 'antd'
import viVN from 'antd/lib/locale/vi_VN'
import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import { RecoilRoot } from 'recoil'
import { SITE_NAME, SITE_SLOGAN } from '@/constants'
import AdminLayout from 'components/layout/AdminLayout'
// import 'antd/dist/antd.css'
import 'tailwindcss/tailwind.css'
import '../styles/react-slick.css'
import '../styles/admin.styles.css'
import '../styles/custom-theme.css'
import '../styles/styles.css'
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3'
import { initFacebookSdk } from 'utils'
import GuestLayout from 'components/layout/GuestLayout'

const MyApp: NextPage<AppProps> = ({ Component, pageProps }) => {
  const router = useRouter()
  const authLayout = ['/auth/login', '/auth/register'].includes(router.pathname)
  const guestLayout = [
    '/',
    '/san-pham/[slug]',
    '/danh-muc/[slug]',
    '/xem-them/[slug]',
    '/checkout/cart',
    '/checkout/buy-now',
    '/checkout/success/[id]',
    '/deal-chop-nhoang',
  ].includes(router.pathname)

  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles) {
      jssStyles.parentElement?.removeChild(jssStyles)
    }
    initFacebookSdk().then()
  }, [])

  return (
    <>
      <Head>
        <title>
          {SITE_NAME} - {SITE_SLOGAN}
        </title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, maximum-scale=1"
        />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin={'anonymous'}
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Asap:wght@400;500;600;700&display=swap"
          rel="stylesheet"
        />
      </Head>
      <GoogleReCaptchaProvider
        reCaptchaKey={process.env.NEXT_PUBLIC_GOOGLE_RECAPTCHA_SITE_KEY}
        scriptProps={{ async: true }}
      >
        <RecoilRoot>
          <ConfigProvider locale={viVN}>
            {authLayout ? (
              <Component {...pageProps} />
            ) : guestLayout ? (
              <GuestLayout>
                <Component {...pageProps} />
              </GuestLayout>
            ) : (
              <AdminLayout>
                <Component {...pageProps} />
              </AdminLayout>
            )}
          </ConfigProvider>
        </RecoilRoot>
      </GoogleReCaptchaProvider>
    </>
  )
}

export default MyApp

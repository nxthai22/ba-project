import { useRouter } from 'next/router'
import React, { useEffect, useRef, useState } from 'react'
import {
  GetManyProductEntityResponseDto,
  ProductsService,
  ProvinceEntity,
  ProvincesService,
} from 'services'
import Head from 'next/head'
import { Breadcrumb, Button, Pagination, Select } from 'antd'
import Link from 'components/common/Link'
import { alertError, filterOption } from 'utils'
import HomeProductBlock from 'components/common/HomeProductBlock'
import { LabeledValue } from 'antd/lib/select'
import NoProduct from 'components/guest/common/NoProduct'

const ViewMore = () => {
  const router = useRouter()
  const [title, setTitle] = useState('')
  const { slug, page = 1, pub_id, sort, provinceId } = router.query
  const [products, setProducts] = useState<GetManyProductEntityResponseDto>()
  const [provinces, setProvinces] = useState<ProvinceEntity[]>([])
  const [province, setProvince] = useState<LabeledValue>()
  const selectRef = useRef(null)

  useEffect(() => {
    ProvincesService.getManyBase({
      limit: 100,
    })
      .then((response) => {
        setProvinces(response.data)
      })
      .catch((e) => alertError(e))
  }, [])

  useEffect(() => {
    if (slug) {
      if (provinceId && Number(provinceId) > 0) {
        const tmpProvince = provinces.find(
          (province) => province.id === Number(provinceId)
        )
        if (tmpProvince)
          setProvince({
            label: tmpProvince.name,
            value: tmpProvince.id,
          })
      } else {
        setProvince({
          label: 'Tất cả',
          value: 0,
        })
      }
      let params = {
        limit: 12,
        page: Number(page) || 0,
        ...(sort ? { sort: [sort] } : { sort: ['saleAmount,DESC'] }),
        ...(!isNaN(Number(provinceId)) && {
          provinceId: Number(provinceId),
        }),
        type: '',
      }
      switch (slug) {
        case 'top-tim-kiem':
          params = { ...params, type: 'top_search' }
          setTitle('Top tìm kiếm')
          break
        case 'top-ban-chay':
          params = { ...params, sort: [...params.sort, 'saleAmount,DESC'] }
          setTitle('Top bán chạy')
          break
        case 'san-pham-moi':
          params = {
            ...params,
            ...(!sort && { sort: ['createdAt,DESC'] }),
          }
          setTitle('Sản phẩm mới')
          break
        case 'danh-rieng-cho-ban':
          params = { ...params, type: 'for_you' }
          setTitle('Dành riêng cho bạn')
          break
      }
      ProductsService.getManyBase(params)
        .then((response) => setProducts(response))
        .catch((e) => alertError(e))
    }
  }, [slug, page, sort, provinceId])

  const getQuery = (
    sortOveride = null,
    provinceIdOverride = null,
    pageOverride = null
  ) => {
    return {
      ...(pub_id && { pub_id: Number(pub_id) }),
      ...(page && { page: page }),
      ...(pageOverride && { page: pageOverride }),
      ...(sort && { sort: sort }),
      ...(sortOveride && { sort: sortOveride }),
      ...(provinceId && { provinceId: Number(provinceId) }),
      ...(provinceIdOverride
        ? { provinceId: Number(provinceIdOverride) }
        : { provinceId: 0 }),
    }
  }

  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      <div className={'view-more md:px-0 px-4 product-category'}>
        <div className={'product-detail flex max-w-6xl m-auto flex-col'}>
          <Breadcrumb
            separator=">"
            className={'md:text-base text-sm pb-5 mt-5'}
          >
            <Breadcrumb.Item>
              <Link
                href={{
                  pathname: '/',
                  query: getQuery(),
                }}
              >
                <span
                  style={{
                    color: '#80B4FB',
                  }}
                >
                  Trang chủ
                </span>
              </Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
              <span
                style={{
                  color: '#223263',
                }}
              >
                {title}
              </span>
            </Breadcrumb.Item>
          </Breadcrumb>
          <div
            className={'flex flex-wrap justify-between mb-4 gap-4 top-button'}
          >
            <div className={'flex overflow-x-auto pb-4 md:pb-0'}>
              <>
                <div className={'w-fit mr-2 md:mr-4'}>
                  <Button
                    className={`shadow-sm h-10 md:h-12 px-5 md:px-8 text-sm md:text-lg font-medium rounded-md ${
                      sort === 'saleAmount,DESC' ||
                      (slug === 'top-ban-chay' && sort === 'saleAmount,DESC') ||
                      (!sort && slug !== 'san-pham-moi')
                        ? 'bg-price text-white'
                        : 'text-primary'
                    }`}
                  >
                    <Link
                      href={{
                        pathname: `/xem-them/${slug}`,
                        query: getQuery('saleAmount,DESC'),
                      }}
                    >
                      Bán chạy
                    </Link>
                  </Button>
                </div>
                <div className={'w-fit mr-2 md:mr-4'}>
                  <Button
                    className={`shadow-sm h-10 md:h-12 px-5 md:px-8 text-sm md:text-lg font-medium rounded-md ${
                      sort === 'createdAt,DESC' ||
                      (slug === 'san-pham-moi' && sort !== 'saleAmount,DESC')
                        ? 'bg-price text-white'
                        : 'text-primary'
                    }`}
                  >
                    <Link
                      href={{
                        pathname: `/xem-them/${slug}`,
                        query: getQuery('createdAt,DESC'),
                      }}
                    >
                      Mới nhất
                    </Link>
                  </Button>
                </div>
                <div className={'w-fit mr-2 md:mr-4'}>
                  <Button
                    className={`shadow-sm h-10 md:h-12 px-5 md:px-8 text-sm md:text-lg font-medium rounded-md ${
                      sort === 'price,DESC'
                        ? 'bg-price text-white'
                        : 'text-primary'
                    }`}
                  >
                    <Link
                      href={{
                        pathname: `/xem-them/${slug}`,
                        query: getQuery('price,DESC'),
                      }}
                    >
                      Giá cao nhất
                    </Link>
                  </Button>
                </div>
                <div className={'w-fit mr-2 md:mr-4'}>
                  <Button
                    className={`shadow-sm h-10 md:h-12 px-5 md:px-8 text-sm md:text-lg font-medium rounded-md ${
                      sort === 'price,ASC'
                        ? 'bg-price text-white'
                        : 'text-primary'
                    }`}
                  >
                    <Link
                      href={{
                        pathname: `/xem-them/${slug}`,
                        query: getQuery('price,ASC'),
                      }}
                    >
                      Giá thấp nhất
                    </Link>
                  </Button>
                </div>
              </>
            </div>
            <div className={'flex-1 md:flex-initial md:w-60'}>
              <Select
                ref={selectRef}
                placeholder={'Kho hàng tại'}
                filterOption={filterOption}
                allowClear
                showSearch
                className={'province w-full'}
                value={province}
                onChange={(value) => {
                  router
                    .push({
                      pathname: `/xem-them/${slug}`,
                      query: getQuery(null, value),
                    })
                    .then(() => {
                      selectRef.current.blur()
                    })
                }}
                size="large"
              >
                <Select.Option
                  className={'text-primary text-sm border-t-1 py-3'}
                  key={'province-0'}
                  value={0}
                >
                  Tất cả
                </Select.Option>
                {provinces?.map((province) => (
                  <Select.Option
                    key={`province-${province.id}`}
                    value={province.id}
                    className={'text-primary text-sm border-t-1 py-3'}
                  >
                    {province.name}
                  </Select.Option>
                ))}
              </Select>
            </div>
          </div>
          <div className="mb-8">
            {products?.data?.length > 0 ? (
              <>
                <div
                  className={'grid grid-cols-2 md:grid-cols-4 gap-2 md:gap-4'}
                >
                  {products?.data?.map((product) => (
                    <HomeProductBlock
                      key={`for-you-${product.id}`}
                      product={product}
                      pub_id={Number(pub_id)}
                    />
                  ))}
                </div>
                {products?.pageCount > 1 && (
                  <div className={'mt-12 flex justify-center mb-10'}>
                    <Pagination
                      showSizeChanger={false}
                      defaultCurrent={products.page}
                      defaultPageSize={12}
                      total={products.total}
                      onChange={(page) =>
                        router.push({
                          pathname: `/xem-them/${slug}`,
                          query: getQuery(null, null, page),
                        })
                      }
                    />
                  </div>
                )}
              </>
            ) : (
              <NoProduct />
            )}
          </div>
        </div>
      </div>
    </>
  )
}
export default ViewMore

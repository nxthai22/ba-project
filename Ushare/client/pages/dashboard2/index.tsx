import { loadingState } from 'recoil/Atoms'
import React, { FC, useEffect, useState } from 'react'
import { Controller, useForm, useWatch } from 'react-hook-form'
import { Card, Col, Form, Row, Tooltip } from 'antd'
import { useSetRecoilState } from 'recoil'
import { alertError, formatCurrency } from 'utils'
import { RevenueResponse, RevenueService } from 'services'
import { AreaChart, PieChart } from '@opd/g2plot-react'
import { sum, reduce, chain } from 'lodash'
import { InfoCircleOutlined } from '@ant-design/icons'
import DatePicker from 'components/common/DatePicker'
import { getTime } from 'utils/timer'

interface Inputs {
  dates?: [Date, Date]
  fromDate?: Date
  toDate?: Date
  overviewFilter: Date[]
  commissionFilter: Date[]
  topSaleFilter: Date[]
  topProductRevenueRateFilter: Date[]
}

const {
  today,
  yesterday,
  thisWeekStart,
  thisWeekEnd,
  lastWeekStart,
  lastWeekEnd,
  thisMonthStart,
  thisMonthEnd,
  lastMonthStart,
  lastMonthEnd,
  thisQuarterStart,
  thisQuarterEnd,
  lastQuarterStart,
  lastQuarterEnd,
  thisYearStart,
  thisYearEnd,
  lastYearStart,
  lastYearEnd,
} = getTime

const Dashboard: FC = () => {
  const { control } = useForm<Inputs>({
    defaultValues: {
      overviewFilter: [today, today],
      commissionFilter: [thisMonthStart, thisMonthEnd],
      topSaleFilter: [thisMonthStart, thisMonthEnd],
      topProductRevenueRateFilter: [thisMonthStart, thisMonthEnd],
    },
  })
  const setIsLoading = useSetRecoilState(loadingState)
  const [revenueChart, setRevenueChart] = useState([])
  const [revenueData, setRevenueData] = useState<RevenueResponse>()
  const [topProductSaleData, setTopProductSaleData] = useState([])
  const [topProductRevenueRate, setTopProductRevenueRate] = useState([])

  const watchOverviewFilter = useWatch({
    control,
    name: 'overviewFilter',
  })

  const watchCommissionFilter = useWatch({
    control,
    name: 'commissionFilter',
  })

  const watchTopSaleFilter = useWatch({
    control,
    name: 'topSaleFilter',
  })

  const watchTopProductRevenueRateFilter = useWatch({
    control,
    name: 'topProductRevenueRateFilter',
  })

  const roundedTo100 = (pieData) => {
    const list = pieData.map((i) => i.value)
    var off =
      100 -
      reduce(
        list,
        function (acc, x) {
          return acc + Math.round(x)
        },
        0
      )
    const roundedList = chain(list)
      .map(function (x, i) {
        return Math.round(x) + Number(off > i) - Number(i >= list.length + off)
      })
      .value()
    for (let i = 0; i < list.length; i++) {
      pieData[i].value = roundedList[i]
    }
    return pieData
  }

  useEffect(() => {
    setIsLoading(true)
    RevenueService.revenueControllerCalculateRevenue({
      body: {
        fromDate: watchOverviewFilter[0],
        toDate: watchOverviewFilter[1],
        page: 1,
        limit: 10
      },
    })
      .then((revenueResponse) => {
        setIsLoading(false)
        setRevenueData(revenueResponse)
      })
      .catch((e) => {
        setIsLoading(false)
        alertError(e)
      })
  }, [watchOverviewFilter])

  useEffect(() => {
    setIsLoading(true)
    RevenueService.revenueControllerCalculateRevenueChart({
      fromDate: watchCommissionFilter[0].toISOString(),
      toDate: watchCommissionFilter[1].toISOString(),
      page: 1,
      limit: 10,
    })
      .then((revenueChartResponse) => {
        setIsLoading(false)
        setRevenueChart(revenueChartResponse)
      })
      .catch((e) => {
        setIsLoading(false)
        alertError(e)
      })
  }, [watchCommissionFilter])

  useEffect(() => {
    setIsLoading(true)
    RevenueService.revenueControllerGetTopProductSale({
      body: {
        fromDate: watchTopSaleFilter[0],
        toDate: watchTopSaleFilter[1],
        page: 1,
        limit: 10
      },
    })
      .then((topProductSaleResponse) => {
        setIsLoading(false)
        setTopProductSaleData(topProductSaleResponse)
      })
      .catch((e) => {
        setIsLoading(false)
        alertError(e)
      })
  }, [watchTopSaleFilter])

  useEffect(() => {
    setIsLoading(true)
    RevenueService.revenueControllerGetTopProductRevenueRate({
      fromDate: watchTopProductRevenueRateFilter[0].toISOString(),
      toDate: watchTopProductRevenueRateFilter[1].toISOString(),
    })
      .then((setTopProductRevenueRateResponse) => {
        setIsLoading(false)
        // Calculate percentage
        const total = sum(setTopProductRevenueRateResponse.map((x) => x.value))
        setTopProductRevenueRateResponse.forEach((el) => {
          el.price = el.value
          el.value = (el.value * 100.0) / total
        })

        // Correcting percentage
        setTopProductRevenueRateResponse = roundedTo100(
          setTopProductRevenueRateResponse
        )
        setTopProductRevenueRate(setTopProductRevenueRateResponse)
      })
      .catch((e) => {
        setIsLoading(false)
        alertError(e)
      })
  }, [watchTopProductRevenueRateFilter])
  //
  // useEffect(() => {
  //   setIsLoading(true)
  //   Promise.all([
  //     RevenueService.revenueControllerCalculateCommission(),
  //     RevenueService.revenueControllerGetTopProductSale({
  //       body: { fromDate, toDate, page: 1, limit: 10 },
  //     }),
  //     RevenueService.revenueControllerCalculateRevenue({
  //       body: { fromDate, toDate, page: 1, limit: 10 },
  //     }),
  //   ]).then(([commissionResponse, topProductSaleResponse, revenueResponse]) => {
  //     setIsLoading(false)
  //     setCommissionData(commissionResponse)
  //     setTopProductSaleData(
  //       topProductSaleResponse.labels.map((label, index) => ({
  //         type: label,
  //         value:
  //           topProductSaleResponse?.datasets[0]?.data[index] + Math.random(),
  //       }))
  //     )
  //     setRevenueData(revenueResponse)
  //     setConfigData(
  //       commissionResponse.labels.map((label, index) => ({
  //         date: label,
  //         value: commissionResponse?.datasets?.[0]?.data[index] + Math.random(),
  //       }))
  //     )
  //     const tmpConfig = []
  //     // @ts-ignore
  //     revenueResponse?.currConfigCommission?.[0]?.value?.map((config) => {
  //       tmpConfig.push(config)
  //     })
  //     setCommissionConfig(sortBy(tmpConfig, 'max'))
  //   })
  // }, [fromDate])

  const renderRangePicker = (field: any) => {
    return (
      <DatePicker.RangePicker
        {...field}
        format={'dd-MM-yyyy'}
        allowClear={false}
        ranges={{
          'Hôm nay': [today, today],
          'Hôm qua': [yesterday, yesterday],
          'Tuần này': [thisWeekStart, thisWeekEnd],
          'Tuần trước': [lastWeekStart, lastWeekEnd],
          'Tháng này': [thisMonthStart, thisMonthEnd],
          'Tháng trước': [lastMonthStart, lastMonthEnd],
          'Quý này': [thisQuarterStart, thisQuarterEnd],
          'Quý trước': [lastQuarterStart, lastQuarterEnd],
          'Năm này': [thisYearStart, thisYearEnd],
          'Năm trước': [lastYearStart, lastYearEnd],
        }}
      />
    )
  }

  return (
    <div className={'p-4'}>
      <Form autoComplete={'off'}>
        <Card
          title={'Doanh thu đơn hàng đã xác nhận'}
          extra={
            <div className='flex items-center'>
              <span className='font-medium text-gray-400 mr-2'>
                Thời gian xác nhận
              </span>
              <Controller
                control={control}
                name={'overviewFilter'}
                render={({ field }) => renderRangePicker(field)}
              />
            </div>
          }
        >
          <Row gutter={[24, 24]}>
            <Col xs={24} md={6}>
              <div
                className={'rounded-lg p-4'}
                style={{
                  background: '#CECDFF',
                }}
              >
                <div className={'flex items-center'}>
                  Đơn hàng hoàn thành
                  <div
                    className={'ml-4 px-2 rounded-full text-white font-bold'}
                    style={{
                      background: '#00A389',
                    }}
                  >
                    {revenueData?.revenueComplepted[0].count}
                  </div>
                </div>
                <div
                  style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    color: '#036656',
                  }}
                >
                  {formatCurrency(revenueData?.revenueComplepted[0].sum)}
                </div>
              </div>
            </Col>
            <Col xs={24} md={6}>
              <div
                className={'rounded-lg p-4'}
                style={{
                  background: '#B7F2FF',
                }}
              >
                <div className={'flex items-center'}>
                  Đơn hàng đang xử lý
                  <div
                    className={'ml-4 px-2 rounded-full text-white font-bold'}
                    style={{
                      background: '#FF4A55',
                    }}
                  >
                    {revenueData?.revenue[0].count}
                  </div>
                </div>
                <div
                  style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    color: '#036656',
                  }}
                >
                  {formatCurrency(revenueData?.revenue[0].sum)}
                </div>
              </div>
            </Col>
          </Row>
        </Card>
        <Card
          title={
            <>
              <span>Biểu đồ doanh thu bán hàng </span>
              <span className='relative -top-2'>
                <Tooltip title={<>Doanh thu được tính bằng tổng những đơn hàng đang có trạng thái <b>Đã giao</b>, <b>chờ đối soát</b> và <b>Đã đối soát</b> trong thời gian tìm kiếm</>}>
                  <InfoCircleOutlined />
                </Tooltip>
              </span>
            </>
          }
          extra={
            <div className='flex items-center'>
              <span className='font-medium text-gray-400 mr-2'>
                Tổng doanh thu:{' '}
                <span className='font-bold text-black'>
                  {formatCurrency(revenueChart?.reduce(
                    (total, item) => Number(item.value) + total,
                    0
                  ))}
                </span>
              </span>
              <Controller
                control={control}
                name={'commissionFilter'}
                render={({ field }) => renderRangePicker(field)}
              />
            </div>
          }
        >
          <AreaChart
            xField={'date'}
            yField={'value'}
            data={revenueChart}
            smooth={true}
            yAxis={{
              label: {
                formatter: (text) => {
                  return formatCurrency(Number(text))
                },
              },
            }}
            tooltip={{
              formatter: (datum) => {
                return {
                  name: 'Doanh thu',
                  value: formatCurrency(datum.value),
                }
              },
            }}
          />
        </Card>
        <Row gutter={[24, 24]}>
          <Col md={12} xs={24}>
            <Card
              title={'Biểu đồ tỉ trọng doanh thu hàng hoá'}
              extra={
                <Controller
                  control={control}
                  name={'topProductRevenueRateFilter'}
                  render={({ field }) => renderRangePicker(field)}
                />
              }
            >
              {topProductRevenueRate.length > 0 ? (
                <PieChart
                  autoFit={true}
                  appendPadding={10}
                  radius={0.8}
                  innerRadius={0.6}
                  angleField={'value'}
                  colorField={'label'}
                  data={topProductRevenueRate}
                  label={{
                    content: '{value}%',
                  }}
                  statistic={{
                    title: false,
                    content: false,
                  }}
                  interactions={[
                    {
                      type: 'element-selected',
                    },
                    {
                      type: 'element-active',
                    },
                  ]}
                  tooltip={{
                    formatter: (datum) => {
                      return {
                        name: datum.label,
                        value: datum.value + ' %',
                      }
                    },
                  }}
                  legend={{
                    offsetY: 10,
                    offsetX: -30,
                  }}
                />
              ) : (
                <div>Chưa có dữ liệu</div>
              )}
            </Card>
          </Col>
          <Col md={12} xs={24}>
            <Card
              title={'Top 10 sản phẩm bán chạy'}
              extra={
                <Controller
                  control={control}
                  name={'topSaleFilter'}
                  render={({ field }) => renderRangePicker(field)}
                />
              }
            >
              {topProductSaleData.length > 0 ? (
                <PieChart
                  autoFit={true}
                  appendPadding={10}
                  radius={0.8}
                  innerRadius={0.6}
                  angleField={'value'}
                  colorField={'label'}
                  data={topProductSaleData}
                  statistic={{
                    title: false,
                    content: false,
                  }}
                  interactions={[
                    {
                      type: 'element-selected',
                    },
                    {
                      type: 'element-active',
                    },
                  ]}
                  legend={{
                    offsetY: 10,
                    offsetX: -30,
                  }}
                  tooltip={{
                    formatter: (datum) => {
                      return {
                        name: datum.label,
                        value: datum.value + ' sp',
                      }
                    },
                  }}
                />
              ) : (
                <div>Chưa có dữ liệu</div>
              )}
            </Card>
          </Col>
        </Row>
      </Form>
    </div>
  )
}

export default Dashboard

import { useRouter } from 'next/router'
import { Breadcrumb, Card, Col, Row, Space, Typography } from 'antd'
import React from 'react'
import Head from 'next/head'

const ID = () => {
  const router = useRouter()
  const { id } = router.query
  return (
    <>
      <Head>
        <title>Đặt hàng thành công - Hasuta</title>
      </Head>
      <Row className={'p-6'} gutter={[16, 16]}>
        <Col xs={24}>
          <Breadcrumb
            separator=">"
            style={{
              color: '#3F404D',
            }}
          >
            <Breadcrumb.Item>Tạo đơn</Breadcrumb.Item>
            <Breadcrumb.Item>Kiểm tra</Breadcrumb.Item>
            <Breadcrumb.Item>
              <span
                style={{
                  color: '#1890FE',
                }}
              >
                Hoàn thành
              </span>
            </Breadcrumb.Item>
          </Breadcrumb>
        </Col>
        <Col xs={24}>
          <Card>
            <Row className={'w-full'} gutter={[16, 16]}>
              <Col className={'text-center'} xs={24}>
                <img
                  src={'/images/checkout-success.png'}
                  className={'mx-auto'}
                />
              </Col>
              <Col className={'text-center text-lg font-bold'} xs={24}>
                Tạo đơn hàng thành công
              </Col>
              <Col className={'text-center text-xl'} xs={24}>
                Mã đơn hàng:{' '}
                <strong>#{String(id).split(',').join(' và ')}</strong>
              </Col>
              <Col className={'text-center text-sm'} xs={24}>
                Cảm ơn bạn đã mua hàng tại Ushare! Người mua sẽ được gọi điện
                trong 24h để xác nhận đơn hàng
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </>
  )
}
export default ID

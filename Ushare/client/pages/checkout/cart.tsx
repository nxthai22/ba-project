import React, { FC, useEffect, useState } from 'react'
import {
  Breadcrumb,
  Button,
  Col,
  Form,
  Input,
  List,
  Modal,
  Popconfirm,
  Row,
  Select,
  Table,
} from 'antd'
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import {
  cartState,
  childSelectedRowKeysState,
  loadingState,
  orderInfoState,
} from 'recoil/Atoms'
import { useRouter } from 'next/router'
import {
  CartProductEntity,
  DetailProudct,
  EnumCreateOrderDtoPaymentType,
  EnumProductEntityStatus,
  MerchantAddressesService,
  ProductEntity,
  ProductsService,
  ProductVariantEntity,
  VoucherEntity,
  VouchersService,
} from 'services'
import { alertError, formatCurrency } from 'utils'
import { MinusOutlined, PlusOutlined } from '@ant-design/icons'
import {
  Controller,
  FormProvider,
  useFieldArray,
  useForm,
} from 'react-hook-form'
import * as yup from 'yup'
import { SchemaOf } from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import Head from 'next/head'
import { format, formatDistance } from 'date-fns'
import { vi } from 'date-fns/locale'
import { useAddCartProducts } from 'recoil/Hooks'
import { ColumnsType } from 'antd/es/table'
import { TableRowSelection } from 'antd/es/table/interface'
import OrderCheckout from 'components/common/OrderCheckout'
import Link from 'components/common/Link'
import {
  Voucher,
  HomeIcon,
  EditIcon,
  DeleteIcon,
  PlusIcon,
  MinusIcon,
} from 'constants/icons'
import _ from 'lodash'

interface ICartProduct extends ProductEntity {
  merchantAddressName?: string
  products: [
    {
      name: string
      price: number
      quantity: number
      images: string[]
      priceAfterDiscount: number
    }
  ]
  voucher: VoucherEntity
  voucherUshare: VoucherEntity
  totalPriceAfterDiscount: number
}

interface Inputs {
  provinceId?: number
  districtId?: number
  wardId: number
  paymentType?: EnumCreateOrderDtoPaymentType
  tel: string
  shippingAddress: string
  fullname: string
  note: string
  merchantAddresses: ICartProduct[]
  merchantAddressIds: number[]
  voucherCode: string
  voucherUshareCode: string
}

const phoneRegExp = /(0+(3|5|7|8|9|1[2|6]))+([0-9]{8})\b/
const schema: SchemaOf<Inputs> = yup.object().shape({
  fullname: yup.string().trim().required('Chưa nhập Họ và tên'),
  note: yup.string(),
  tel: yup
    .string()
    .required('Chưa nhập Số điện thoại')
    .matches(phoneRegExp, 'Số điện thoại không hợp lệ'),
  shippingAddress: yup.string().trim().required('Chưa nhập địa chỉ nhận hàng.'),
  wardId: yup
    .number()
    .required('Chưa chọn Phường/Xã nhận hàng.')
    .typeError('Chưa chọn Phường/Xã nhận hàng.'),
  districtId: yup
    .number()
    .required('Chưa chọn Quận/Huyện nhận hàng.')
    .typeError('Chưa chọn Quận/Huyện nhận hàng.'),
  provinceId: yup
    .number()
    .required('Chưa chọn Thành phố/Tỉnh nhận hàng.')
    .typeError('Chưa chọn Thành phố/Tỉnh nhận hàng.'),
  voucherCode: yup.string(),
  merchantAddressIds: yup.array().of(yup.number()),
})

const Cart: FC = () => {
  const cart = useRecoilValue(cartState)
  const [cartLoading, setCartLoading] = useState(false)
  const setIsLoading = useSetRecoilState(loadingState)
  const orderInfo = useRecoilValue(orderInfoState)
  const router = useRouter()
  const { pub_id = 0 } = router.query
  const [voucherMerchantAddressId, setVoucherMerchantAddressId] = useState(0)
  const [product, setProduct] = useState<DetailProudct>()
  const [selectedCartProduct, setSelectedCartProduct] =
    useState<CartProductEntity>()
  const [variant1, setVariant1] = useState<number>(null)
  const [variant2, setVariant2] = useState<number>(null)
  const addProduct = useAddCartProducts()
  const [parentSelectedRowKeys, setParentSelectedRowKeys] = useState([])
  const [childSelectedRowKeys, setChildSelectedRowKeys] = useRecoilState(
    childSelectedRowKeysState
  )
  const [expandedRowKeys, setExpandedRowKeys] = useState([])
  const [selectedVariant, setSelectedVariant] = useState<ProductVariantEntity>()
  const [quantity, setQuantity] = useState<number>(1)
  const [merchantAddressId, setMerchantAddressId] = useState<number>(null)
  const formMethods = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
    defaultValues: {
      paymentType: 'cod',
    },
  })
  const { control, setValue, getValues } = formMethods
  const [merchantChildSelectedRowKeys, setMerchantChildSelectedRowKeys] =
    useState([])
  const [selectedVoucherMerchant, setSelectedVoucherMerchant] =
    useState<VoucherEntity>(null)
  const { fields: merchantAddresses, update: updateMerchantAddress } =
    useFieldArray({
      control,
      name: 'merchantAddresses',
    })

  const [isShowModalEditProduct, setIsShowModalEditProduct] =
    useState<boolean>(false)
  const columns: ColumnsType = [
    {
      dataIndex: 'merchantAddressName',
      render: (value) => (
        <div className={'flex'}>
          <HomeIcon />
          <div className={'ml-2'}>{value}</div>
        </div>
      ),
    },
  ]
  const [vouchers, setVouchers] = useState<VoucherEntity[]>([])

  useEffect(() => {
    const productIds = []
    merchantChildSelectedRowKeys.forEach((merchantChildSelectedRowKey) => {
      const merchantChildSelectedRowKeyParts =
        merchantChildSelectedRowKey.split('-')
      if (merchantChildSelectedRowKeyParts?.[1]) {
        productIds.push(Number(merchantChildSelectedRowKeyParts?.[1]))
      }
    })
    if (productIds?.length > 0) {
      Promise.all(
        productIds.map((productId) => {
          return ProductsService.getOneBase({
            id: productId,
          })
        })
      ).then((products) => {
        setVouchers(products.map((product) => product.vouchers).flat())
      })
    }
  }, [merchantChildSelectedRowKeys])

  useEffect(() => {
    if (product?.groupType?.length > 1)
      setSelectedVariant(
        product?.variants.find(
          (variant) =>
            variant.groupTypeIndex === variant1 &&
            variant.valueTypeIndex === variant2
        )
      )
    else if (product?.groupType?.length === 1)
      setSelectedVariant(
        product?.variants.find(
          (variant) =>
            variant.groupTypeIndex === 0 && variant.valueTypeIndex === variant1
        )
      )
    else {
      if (product)
        setSelectedVariant({
          ...product,
          productId: product?.id,
          id: null,
          stock: product?.stocks,
        })
    }
  }, [variant1, variant2])

  useEffect(() => {
    if (cart?.[Number(pub_id)]) {
      setCartLoading(true)
      const productIds = []
      const productCarts = []
      const merchantAddressIds = []
      Object.entries(cart?.[Number(pub_id)]).forEach((cartItem) => {
        if (cartItem[1].length > 0) {
          merchantAddressIds.push(cartItem[0])
          Object.entries(cartItem[1])?.forEach((productItem) => {
            productCarts.push({
              id: Number(productItem[1]?.id),
              productVariantId:
                Number(productItem[1]?.productVariantId) || null,
              merchantAddressId: Number(cartItem[0]),
            })
            productIds.push(Number(productItem[1]?.id))
          })
        }
      })
      if (productIds.length > 0) {
        Promise.all([
          ProductsService.getManyBase({
            filter: `id||in||${productIds.join(',')}`,
            join: ['variants'],
            sort: ['createdAt,DESC'],
          }),
          MerchantAddressesService.getManyBase({
            filter: `id||in||${merchantAddressIds.join(',')}`,
          }),
        ])
          .then(([productsResponse, merchantAddressResponse]) => {
            setCartLoading(false)
            const tmpCartProduct = []
            const tmpExpandedRowKeys = []
            const tmpCartProductRemoves = []
            merchantAddressIds?.map((merchantAddressId) => {
              const products = cart?.[Number(pub_id)]?.[merchantAddressId]?.map(
                (productItem) => {
                  const product = productsResponse.data.find(
                    (product) => product.id === Number(productItem?.id)
                  )
                  if (product) {
                    let quantity = productItem.quantity
                    //Ép số lượng trong giỏ hàng bằng max số lượng đang có
                    if (product?.variants?.length > 0) {
                      const variant = product?.variants?.find(
                        (variant) =>
                          variant.id === Number(productItem.productVariantId)
                      )
                      const stock = variant?.stock?.find(
                        (stock) =>
                          stock.merchantAddressId === Number(merchantAddressId)
                      )
                      if (stock && stock?.quantity < productItem.quantity) {
                        quantity = stock?.quantity
                      }
                    } else {
                      const stock = product?.stock?.find(
                        (stock) =>
                          stock.merchantAddressId === Number(merchantAddressId)
                      )
                      if (stock && stock?.quantity < productItem.quantity) {
                        quantity = stock?.quantity
                      }
                    }

                    return {
                      ...product,
                      rowKey: `${Number(merchantAddressId)}-${product?.id}-${
                        Number(productItem.productVariantId) || null
                      }`,
                      merchantAddressId: Number(merchantAddressId),
                      productVariantId:
                        Number(productItem.productVariantId) || null,
                      variants: product?.variants?.find(
                        (variant) =>
                          variant.id === Number(productItem.productVariantId)
                      ),
                      quantity: quantity,
                    }
                  } else {
                    tmpCartProductRemoves.push({ ...productItem, quantity: 0 })
                  }
                }
              )
              if (tmpCartProductRemoves?.length > 0) {
                addProduct(tmpCartProductRemoves, false, Number(pub_id))
              }
              if (products?.filter((prod) => prod)?.length > 0) {
                tmpExpandedRowKeys.push(merchantAddressId)
                tmpCartProduct.push({
                  merchantAddressId: merchantAddressId,
                  merchantAddressName: merchantAddressResponse?.data?.find(
                    (m) => m.id === Number(merchantAddressId)
                  ).name,
                  products: _.orderBy(
                    products,
                    ['status', 'createdAt'],
                    ['asc', 'desc']
                  ),
                  productActiveCount:
                    products?.filter(
                      (prod) => prod.status === EnumProductEntityStatus.active
                    )?.length || 0,
                })
              }
            })
            if (tmpCartProduct?.length > 0) {
              setValue(
                'merchantAddresses',
                _.orderBy(tmpCartProduct, ['productActiveCount'], ['desc'])
              )
              setExpandedRowKeys(tmpExpandedRowKeys)
            } else {
              setValue('merchantAddresses', [])
              setExpandedRowKeys([])
            }
          })
          .catch((e) => {
            setCartLoading(false)
            alertError(e)
          })
      } else {
        setValue('merchantAddresses', [])
        setCartLoading(false)
      }
    }
  }, [cart, pub_id])

  const applyVoucher = (value) => {
    if (voucherMerchantAddressId !== 0) {
      setIsLoading(true)
      VouchersService.voucherControllerGetOneByCode({
        code: value,
      })
        .then((voucher) => {
          setIsLoading(false)
          const tmpProducts = [...merchantAddresses]

          const merchantAdressIndex = tmpProducts.findIndex(
            (merchant) =>
              merchant.merchantAddressId === voucherMerchantAddressId
          )
          updateMerchantAddress(merchantAdressIndex, {
            ...merchantAddresses[merchantAdressIndex],
            voucher: voucher,
          })
          setVoucherMerchantAddressId(null)
          setSelectedVoucherMerchant(null)
          setValue('voucherCode', '')
        })
        .catch((e) => {
          alertError(e)
          setIsLoading(false)
        })
    }
  }
  const rowSelection: TableRowSelection<any> = {
    selectedRowKeys: parentSelectedRowKeys,
    hideSelectAll: true,
    onSelect: (record, selected) => {
      const patentArr: any = [...parentSelectedRowKeys]
      let childArr: any = [...childSelectedRowKeys]
      const setChildArr = merchantAddresses
        .find((d: any) => d.merchantAddressId === record.merchantAddressId)
        .products.map((item: any) => item.rowKey)
      //The first step is to determine the selected true: selected, false, unselected
      if (selected) {
        //The second step, the parent Table is selected, and the child Tables are all selected (all integrated together, and then duplicated)
        patentArr.push(record.merchantAddressId)
        childArr = Array.from(new Set([...setChildArr, ...childArr]))
      } else {
        //The second step is to uncheck the parent table and uncheck all the child tables (for childArr, filter out the keys of all child tables under the unchecked parent table)
        patentArr.splice(
          patentArr.findIndex((item: any) => item === record.merchantAddressId),
          1
        )
        childArr = childArr.filter(
          (item: any) => !setChildArr.some((e: any) => e === item)
        )
      }
      //The third step is to set the SelectedRowKeys of the parent and child
      setParentSelectedRowKeys(patentArr)
      setChildSelectedRowKeys(childArr)
    },
    getCheckboxProps: (record) => ({
      disabled:
        Boolean(orderInfo) ||
        [
          EnumProductEntityStatus.inactive,
          EnumProductEntityStatus.deleted,
        ].includes(record.status), // Column configuration not to be checked
    }),
  }
  const childRowSelection: TableRowSelection<any> = {
    selectedRowKeys: childSelectedRowKeys,
    hideSelectAll: true,
    onSelect: (record, selected, selectedRows) => {
      const childArr: any = [...childSelectedRowKeys]
      //The first step is to determine the selected true: select, add the key value to childArr, false: uncheck, remove the key value from childArr
      if (selected) {
        childArr.push(
          `${record.merchantAddressId}-${record?.id}-${
            record.productVariantId || null
          }`
        )
      } else {
        childArr.splice(
          childArr.findIndex((item: any) => item === record?.rowKey),
          1
        )
      }
      //Undefined must be removed, otherwise selectedRows will put the key value selected in other sub-tables into the array, but the value is undefined, such as: [undefined, 1, uundefined]
      selectedRows = selectedRows.filter((a: any) => a !== undefined)
      //The second step is to determine whether the length of selectedRows is the length of the child in the data. If it is equal, the parent table will be selected.
      for (const item of merchantAddresses) {
        if (item.products.find((d: any) => d.rowKey === record?.rowKey)) {
          const parentArr: any = [...parentSelectedRowKeys]
          if (item.products.length === selectedRows.length) {
            parentArr.push(item.merchantAddressId)
          } else {
            if (
              parentArr.length &&
              parentArr.find((d: any) => d === item.merchantAddressId)
            ) {
              parentArr.splice(
                parentArr.findIndex(
                  (item1: any) => item1 === item.merchantAddressId
                ),
                1
              )
            }
          }
          setParentSelectedRowKeys(parentArr)
          break
        }
      }
      setChildSelectedRowKeys(childArr)
    },
    getCheckboxProps: (record) => ({
      disabled:
        Boolean(orderInfo) ||
        [
          EnumProductEntityStatus.inactive,
          EnumProductEntityStatus.deleted,
        ].includes(record?.status), // Column configuration not to be checked
    }),
  }

  useEffect(() => {
    if (selectedCartProduct) {
      setIsLoading(true)
      ProductsService.getOneBase({
        id: selectedCartProduct?.id,
        join: [
          'productCategory',
          'merchant',
          'productCategory.parent',
          'variants',
          'stocks',
          'stocks.merchantAddress',
          'stocks.merchantAddress.ward',
          'stocks.merchantAddress.ward.district',
          'stocks.merchantAddress.ward.district.province',
        ],
      })
        .then((productResponse) => {
          setIsLoading(false)
          setProduct(productResponse)
          if (productResponse?.variants.length > 0)
            setSelectedVariant(
              productResponse?.variants.find(
                (variant) =>
                  variant.groupTypeIndex ===
                    selectedCartProduct?.variants?.groupTypeIndex &&
                  variant.valueTypeIndex ===
                    selectedCartProduct?.variants?.valueTypeIndex
              )
            )
          else {
            setSelectedVariant({
              ...productResponse,
              productId: productResponse?.id,
              id: null,
              stock: productResponse?.stocks,
            })
          }
        })
        .catch((e) => {
          setIsLoading(false)
          alertError(e)
        })
    }
  }, [selectedCartProduct])

  const expandedRowRender = (record) => {
    const columns: ColumnsType = [
      {
        dataIndex: 'id',
        render: (value, record: ProductEntity) => {
          return (
            <>
              {[
                EnumProductEntityStatus.inactive,
                EnumProductEntityStatus.deleted,
              ].includes(record?.status) && (
                <Link
                  href={{
                    pathname: `/san-pham/${record?.slug}_${value}`,
                  }}
                >
                  <div
                    className={
                      'absolute h-full w-full rounded-xl z-20 top-0 left-0 opacity-80 flex justify-center items-center'
                    }
                    style={{
                      backgroundColor: 'white',
                    }}
                  >
                    <p
                      className={
                        'm-0 py-1.5 px-5 opacity-50 font-bold rounded-lg'
                      }
                      style={{ backgroundColor: 'black', color: 'white' }}
                    >
                      {record?.status === EnumProductEntityStatus.inactive
                        ? 'Tạm hết hàng'
                        : 'Ngừng kinh doanh'}
                    </p>
                  </div>
                </Link>
              )}
              <List.Item className={'border-0'}>
                <List.Item.Meta
                  avatar={
                    <div className={'border p-px md:p-6 rounded mr-2 md:mr-4'}>
                      <img
                        src={record?.images?.[0] || '/images/no-image.png'}
                        alt={record?.name}
                        className="w-12 h-12 md:w-[125px] md:h-[125px]"
                      />
                    </div>
                  }
                  title={
                    <Link href={`/san-pham/${record?.slug}_${record?.id}`}>
                      <span
                        className={
                          'font-semibold uppercase text-sm text-primary'
                        }
                      >
                        {record?.name}
                      </span>
                    </Link>
                  }
                  description={
                    <div>
                      <div className={'mb-2'}>
                        <div
                          className={
                            'flex justify-between items-center text-secondary'
                          }
                        >
                          <div className={'text-sm'}>
                            {record?.variants != undefined && (
                              <span>Phân loại: {record?.variants?.name}</span>
                            )}
                          </div>
                        </div>
                        <div
                          className={'flex justify-between items-center mb-2'}
                        >
                          <div className="flex">
                            {record?.priceBeforeDiscount - record?.price >
                              0 && (
                              <div
                                className={'text-secondary mr-2 line-through'}
                              >
                                {formatCurrency(record?.priceBeforeDiscount)}
                              </div>
                            )}
                            <div className={'font-bold text-price'}>
                              {formatCurrency(record?.price)}
                            </div>
                          </div>
                          <Button
                            type={'text'}
                            onClick={() => {
                              setSelectedCartProduct(record)
                              setMerchantAddressId(record?.merchantAddressId)
                              setQuantity(record?.quantity)
                              setIsShowModalEditProduct(true)
                            }}
                            className={
                              'flex items-center text-[#C6C6C6] px-1 md:px-2'
                            }
                          >
                            <span className={'mr-1'}>sửa</span>
                            <EditIcon />
                          </Button>
                        </div>
                        <Row gutter={[4, 4]} className={'mb-2'}>
                          <Col>
                            <Button
                              className={'rounded px-2'}
                              disabled={record?.quantity === 1}
                              onClick={() => {
                                addProduct(
                                  [
                                    {
                                      id: record?.id,
                                      productVariantId:
                                        record?.productVariantId || null,
                                      merchantAddressId:
                                        record?.merchantAddressId,
                                      quantity: -1,
                                    },
                                  ],
                                  false,
                                  Number(pub_id)
                                )
                              }}
                            >
                              <MinusOutlined />
                            </Button>
                          </Col>
                          <Col>
                            <Input
                              className={'w-12 text-center rounded'}
                              value={record?.quantity || 1}
                            />
                          </Col>
                          <Col>
                            <Button
                              className={
                                'rounded bg-price text-white border-0 px-3'
                              }
                              onClick={() => {
                                let stock = null
                                if (record?.variants) {
                                  stock = record?.variants?.stock.find(
                                    (stock) =>
                                      stock.merchantAddressId ===
                                      record?.merchantAddressId
                                  )
                                } else
                                  stock = record?.stock.find(
                                    (stock) =>
                                      stock.merchantAddressId ===
                                      record?.merchantAddressId
                                  )

                                if (stock) {
                                  if (stock.quantity < record?.quantity + 1) {
                                    addProduct(
                                      [
                                        {
                                          id: record?.id,
                                          productVariantId:
                                            record?.productVariantId || null,
                                          merchantAddressId:
                                            record?.merchantAddressId,
                                          quantity: stock.quantity,
                                          replace: true,
                                        },
                                      ],
                                      false,
                                      Number(pub_id)
                                    )
                                  } else if (
                                    stock.quantity >=
                                    record?.quantity + 1
                                  )
                                    addProduct(
                                      [
                                        {
                                          id: record?.id,
                                          productVariantId:
                                            record.productVariantId || null,
                                          merchantAddressId:
                                            record.merchantAddressId,
                                          quantity: 1,
                                        },
                                      ],
                                      false,
                                      Number(pub_id)
                                    )
                                }
                              }}
                            >
                              <PlusOutlined />
                            </Button>
                          </Col>
                        </Row>
                      </div>
                      <div className={'flex mt-2 items-center'}>
                        <div className={'flex-1 text-secondary'}>
                          Tổng giá:{' '}
                          <span className={'font-bold text-base text-price'}>
                            {formatCurrency(record?.price * record?.quantity)}
                          </span>
                        </div>
                        <div>
                          <Popconfirm
                            title="Bạn chắc chắn muốn xoá Sản phẩm này?"
                            okText={'Đồng ý'}
                            onConfirm={() => {
                              addProduct(
                                [
                                  {
                                    id: record?.id,
                                    productVariantId:
                                      record.productVariantId || null,
                                    merchantAddressId: record.merchantAddressId,
                                    quantity: 0,
                                  },
                                ],
                                false,
                                Number(pub_id)
                              )
                            }}
                          >
                            <Button
                              type="text"
                              className="flex items-center text-[#C6C6C6] px-1 md:px-2"
                            >
                              <span className="mr-1">xóa</span>
                              <DeleteIcon />
                            </Button>
                          </Popconfirm>
                        </div>
                      </div>
                    </div>
                  }
                />
              </List.Item>
            </>
          )
        },
      },
    ]

    return (
      <>
        <Table
          key={'product'}
          showHeader={false}
          rowKey={(record) => record?.rowKey}
          columns={columns}
          rowSelection={childRowSelection}
          dataSource={record.products}
          pagination={false}
        />
        {((Boolean(orderInfo) &&
          parentSelectedRowKeys.includes(record.merchantAddressId)) ||
          !orderInfo) && (
          <div className={'flex mb-2'}>
            <div className={'flex-1 text-secondary'}>Ưu đãi từ shop</div>
            {record?.voucher ? (
              <>
                <div
                  className={'border rounded text-price p-1'}
                  style={{ borderColor: '#F4447F', cursor: 'pointer' }}
                  onClick={() => {
                    setVoucherMerchantAddressId(record?.merchantAddressId)
                    const voucherMechant = merchantAddresses?.find(
                      (merchant) =>
                        merchant.merchantAddressId == record?.merchantAddressId
                    )?.voucher
                    setSelectedVoucherMerchant(voucherMechant)
                  }}
                >
                  {record?.voucher?.subTitle}
                </div>
              </>
            ) : (
              <div
                className={'font-normal cursor-pointer'}
                style={{
                  color: '#80B4FB',
                }}
                onClick={() => {
                  const merchantChildSelectedRowKeys = []
                  childSelectedRowKeys.forEach((childSelectedRowKey) => {
                    const childSelectedRowKeyMerchantId =
                      childSelectedRowKey.split('-')?.[0]
                    if (
                      childSelectedRowKeyMerchantId ===
                      record?.merchantAddressId
                    ) {
                      merchantChildSelectedRowKeys.push(childSelectedRowKey)
                    }
                  })
                  if (merchantChildSelectedRowKeys.length === 0) {
                    alertError('Chưa chọn sản phẩm để áp dụng mã')
                  } else {
                    setVoucherMerchantAddressId(record?.merchantAddressId)
                    setMerchantChildSelectedRowKeys(
                      merchantChildSelectedRowKeys
                    )
                  }
                }}
              >
                Chọn hoặc nhập mã
              </div>
            )}
          </div>
        )}
      </>
    )
  }
  return (
    <>
      <Head>
        <title>Giỏ hàng - Ushare</title>
      </Head>
      <FormProvider {...formMethods}>
        <Form>
          <div className={'cart-checkout bg-white md:p-0 p-4'}>
            <div className={'max-w-6xl m-auto justify-between'}>
              <Row gutter={[16, 16]}>
                <Col xs={24}>
                  <Breadcrumb
                    separator=">"
                    className={'text-base border-b pb-5 mt-5'}
                    style={{
                      color: '#80B4FB',
                    }}
                  >
                    <Breadcrumb.Item>
                      <span
                        style={{
                          color: orderInfo ? '#223263' : '#80B4FB',
                        }}
                      >
                        Tạo đơn
                      </span>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                      <a
                        href="#"
                        style={{
                          color: orderInfo ? '#80B4FB' : '#223263',
                        }}
                      >
                        Kiểm tra
                      </a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                      <a href="#">Hoàn thành</a>
                    </Breadcrumb.Item>
                  </Breadcrumb>
                  <hr />
                </Col>
                <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                  <div
                    className={'text-primary text-base font-semibold uppercase'}
                  >
                    Danh sách sản phẩm trong giỏ hàng
                  </div>
                  {merchantAddresses && (
                    <Table
                      loading={cartLoading}
                      className={`cart-products ${
                        Boolean(orderInfo) && 'has-order-info'
                      }`}
                      key={'cart'}
                      rowKey={(record) => record?.merchantAddressId}
                      dataSource={merchantAddresses}
                      columns={columns}
                      expandable={{
                        expandedRowRender,
                        defaultExpandAllRows: true,
                      }}
                      rowSelection={rowSelection}
                      pagination={false}
                      expandedRowKeys={expandedRowKeys}
                      showExpandColumn={false}
                    />
                  )}
                </Col>
                <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                  <OrderCheckout />
                </Col>
              </Row>
            </div>
          </div>
        </Form>
      </FormProvider>
      <Modal
        className={'edit-variant'}
        visible={isShowModalEditProduct}
        onCancel={() => {
          setProduct(null)
          setIsShowModalEditProduct(false)
        }}
        destroyOnClose={true}
        width={600}
        footer={false}
      >
        <div>
          <div className={'uppercase text-primary text-lg font-medium'}>
            {product?.name}
          </div>
          {product?.groupType?.map((group, groupIndex) => {
            return (
              <div
                key={`product-group-type-${groupIndex}`}
                className={'flex py-2 items-center justify-between'}
              >
                <div className={'text-primary text-sm font-medium'}>
                  {group.name}
                </div>
                <div className={'flex flex-row'}>
                  <Select
                    value={
                      variant1 !== null || variant2 !== null
                        ? groupIndex === 0
                          ? variant1
                          : variant2
                        : selectedCartProduct?.variants.valueTypeIndex
                    }
                    className={'w-40'}
                    onChange={(value) => {
                      if (groupIndex === 0) setVariant1(value)
                      else setVariant2(value)
                    }}
                  >
                    {group.value.map((value, valueIndex) => (
                      <Select.Option
                        key={`group-type-value-${valueIndex}`}
                        value={valueIndex}
                      >
                        {value.field}
                      </Select.Option>
                    ))}
                  </Select>
                </div>
              </div>
            )
          })}
          <div className={'flex justify-between items-center mt-3'}>
            <div>Số lượng</div>
            <div
              className={
                'flex-none flex gap-3 items-center w-40 justify-between'
              }
            >
              <Button
                className={
                  'text-primary border-0 w-10 h-10 rounded-full bg-gray-200 justify-center items-center flex cursor-pointer hover:text-white hover:bg-price'
                }
                onClick={() =>
                  setQuantity((quantity) => (quantity > 1 ? quantity - 1 : 1))
                }
              >
                <MinusIcon />
              </Button>
              <div className={'text-price font-medium text-xl'}>{quantity}</div>
              <Button
                className={
                  'text-primary border-0 w-10 h-10 rounded-full bg-gray-200 justify-center items-center flex cursor-pointer hover:text-white hover:bg-price'
                }
                onClick={() => {
                  const stock = selectedVariant?.stock.find(
                    (stock) => stock.merchantAddressId === merchantAddressId
                  )
                  if (stock) {
                    if (stock.quantity < quantity + 1) {
                      setQuantity(stock.quantity)
                    } else setQuantity((quantity) => quantity + 1)
                  } else {
                    setQuantity((quantity) => quantity + 1)
                  }
                }}
              >
                <PlusIcon />
              </Button>
            </div>
          </div>
          <div
            className={'flex justify-between items-center mt-4 border-b pb-4'}
          >
            <div>Kho hàng</div>
            <div className={'w-8/12'}>
              <Select
                placeholder={'Chọn kho hàng'}
                className={'select-merchant-address-cart'}
                value={merchantAddressId}
                onChange={(value) => {
                  const stock = selectedVariant?.stock.find(
                    (stock) => stock.merchantAddressId === value
                  )
                  if (stock) {
                    if (stock.quantity < quantity) {
                      setQuantity(stock.quantity)
                    }
                  }
                  setMerchantAddressId(value)
                }}
              >
                {selectedVariant?.stock &&
                  selectedVariant?.stock?.map((stock) => (
                    <>
                      {stock?.quantity ? (
                        <Select.Option
                          value={stock?.merchantAddressId}
                          className={
                            'bg-white text-primary text-sm border-b mx-5 px-0 border-gray-300 py-3 hover:bg-transperant hover:text-price'
                          }
                        >
                          <div className={'flex justify-between gap-2'}>
                            <div className={'truncate'}>
                              {stock?.merchantAddress?.name}
                            </div>
                            <div>Còn hàng: {stock?.quantity}</div>
                          </div>
                        </Select.Option>
                      ) : null}
                    </>
                  ))}
              </Select>
            </div>
          </div>
          <div
            className={'flex justify-between items-center mt-4 border-b pb-4'}
          >
            <div>Giá bán</div>
            <div className={'text-price text-base'}>
              {formatCurrency(selectedVariant?.price)}
            </div>
          </div>
          <div
            className={'flex justify-between items-center mt-4 border-b pb-4'}
          >
            <div>Tổng giá bán</div>
            <div className={'text-price text-base'}>
              {formatCurrency(selectedVariant?.price * quantity)}
            </div>
          </div>
          <div className={'mt-10'}>
            <Button
              className={
                'w-full h-11 border-0 bg-price text-white font-semibold'
              }
              onClick={() => {
                addProduct(
                  [
                    {
                      id: selectedVariant?.productId,
                      productVariantId:
                        selectedCartProduct?.productVariantId || null,
                      quantity: 0,
                      merchantAddressId: selectedCartProduct?.merchantAddressId,
                    },
                    {
                      id: selectedVariant?.productId,
                      productVariantId: selectedVariant?.id || null,
                      quantity,
                      merchantAddressId,
                      replace: true,
                    },
                  ],
                  false,
                  Number(pub_id)
                )
                setProduct(null)
                setIsShowModalEditProduct(false)
              }}
            >
              Áp dụng
            </Button>
          </div>
        </div>
      </Modal>
      <Modal
        className={'rounded-modal'}
        title={<label className={'text-primary'}>Ưu đãi từ shop</label>}
        visible={Boolean(voucherMerchantAddressId)}
        footer={false}
        onCancel={() => {
          setVoucherMerchantAddressId(null)
        }}
        destroyOnClose={true}
      >
        <div className={'flex gap-2'}>
          <Controller
            control={control}
            render={({ field }) => (
              <Input
                {...field}
                className={
                  'border-price rounded-sm h-12 bg-gray-50 font-semibold text-secondary px-5'
                }
                placeholder={'Nhập mã Voucher của shop'}
              />
            )}
            name={'voucherCode'}
          />
          <Button
            className={
              'h-12 rounded-sm bg-price text-white border-0 px-6 text-base'
            }
            onClick={() => applyVoucher(getValues('voucherCode'))}
          >
            Áp dụng
          </Button>
        </div>
        {vouchers?.length > 0 ? (
          <>
            <div
              className={'border border-gray-200 p-2 overflow-y-scroll'}
              style={{
                maxHeight: 308,
              }}
            >
              {vouchers?.map((voucher: VoucherEntity) => (
                <div
                  key={`voucher-${voucher?.id}`}
                  className={'py-1 flex flex-row'}
                >
                  <div
                    className={'flex-none'}
                    style={{
                      width: 4,
                      height: 72,
                      background:
                        'url("/images/voucher-bg-right.svg") no-repeat',
                    }}
                  />
                  <div
                    className={'grow flex flex-row p-2'}
                    style={{
                      background:
                        'linear-gradient(90deg, #F54C84 -1.6%, rgba(251, 20, 96, 0.69) 118.95%)',
                    }}
                  >
                    <div className="flex-none">
                      <img src="/images/voucher.svg" alt="Voucher" />
                    </div>
                    <div className={'grow text-white ml-2'}>
                      <div className={'font-bold text-sm'}>{voucher.name}</div>
                      <div className={'flex flex-row justify-between'}>
                        <div className={'text-xs'}>
                          <div>
                            {format(new Date(voucher.endDate), 'd/M/y', {
                              locale: vi,
                            })}
                          </div>
                          <div>
                            {formatDistance(
                              new Date(voucher.endDate),
                              new Date(),
                              { locale: vi }
                            )}
                          </div>
                        </div>
                        <div className={'flex flex-row items-end'}>
                          <div className="items-center flex">
                            <Button
                              className={'border-0 text-price'}
                              onClick={() => applyVoucher(voucher.code)}
                              disabled={
                                selectedVoucherMerchant?.id === voucher?.id
                              }
                            >
                              {selectedVoucherMerchant?.id === voucher?.id
                                ? 'Đã áp dụng'
                                : 'Áp dụng'}
                            </Button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className={'flex-none'}
                    style={{
                      width: 4,
                      height: 72,
                      background:
                        'url("/images/voucher-bg-left.svg") no-repeat',
                    }}
                  />
                </div>
              ))}
            </div>
          </>
        ) : (
          <div className={'mt-5 flex justify-center flex-col items-center'}>
            <div className={'text-secondary'}>
              <Voucher />
            </div>
            <div className={'text-primary'}>
              Chưa có mã giảm giá nào của shop
            </div>
            <div className={'text-secondary'}>
              Nhập mã giảm giá có thể sử dụng vào thanh bên trên
            </div>
          </div>
        )}
      </Modal>
    </>
  )
}

export default Cart

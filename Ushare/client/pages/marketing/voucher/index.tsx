import {
  DollarCircleOutlined,
  PlusOutlined,
  QuestionCircleOutlined,
} from '@ant-design/icons'
import {
  Button,
  Card,
  Col,
  Popconfirm,
  Row,
  Statistic,
  Tabs,
  Tag,
  Tooltip,
  Typography,
} from 'antd'
import { ColumnsType } from 'antd/es/table'
import Link from 'components/common/Link'
import Content from 'components/layout/AdminLayout/Content'
import { StatusLabelByTime } from 'enums'
import React, { FC, useEffect, useState } from 'react'
import { EnumRoleEntityType, VoucherEntity, VouchersService } from 'services'
import {
  alertError,
  alertSuccess,
  formatCurrency,
  formatNumber,
  getStatusLabelByTime,
} from 'utils'
import { format } from 'date-fns'
import DataTable from 'components/common/DataTable'
import { useRouter } from 'next/router'
import { useRecoilValue } from 'recoil'
import { authState } from '../../../recoil/Atoms'

const Index: FC = () => {
  const [title] = useState('Mã giảm giá của Shop')
  return (
    <Content title={title}>
      <Card title={'Chỉ số quan trọng'} className="voucher">
        <Row gutter={30}>
          <Col
            xs={24}
            sm={24}
            md={8}
            lg={8}
            style={{ borderRight: '1px solid #c4c4c4' }}
          >
            <Statistic title="Doanh thu" value={128824000} prefix={'₫'} />
          </Col>
          <Col
            xs={24}
            sm={24}
            md={8}
            lg={8}
            style={{ borderRight: '1px solid #c4c4c4' }}
          >
            <Statistic title="Đơn hàng" value={1500} />
          </Col>
          <Col xs={24} sm={24} md={8} lg={8}>
            <Statistic title="Người mua" value={800} />
          </Col>
        </Row>
      </Card>
      <Card>
        <Row>
          <Card.Meta
            title={'Danh sách mã giảm giá'}
            description="Tạo mã giảm giá toàn shop hoặc mã giảm giá sản phẩm ngay bây giờ để thu hút người mua"
            className="flex-1 mb-5"
          />
          <Link href={'voucher/create'}>
            <Button size="large" type="primary" icon={<PlusOutlined />}>
              Tạo mới
            </Button>
          </Link>
        </Row>
        <Tabs defaultActiveKey="1">
          <Tabs.TabPane tab="Tất cả" key="1">
            <VoucherTable />
          </Tabs.TabPane>
          <Tabs.TabPane tab="Đang diễn ra" key="2">
            <VoucherTable status={StatusLabelByTime.INPROGRESS} />
          </Tabs.TabPane>
          <Tabs.TabPane tab="Sắp diễn ra" key="3">
            <VoucherTable status={StatusLabelByTime.COMMING} />
          </Tabs.TabPane>
          <Tabs.TabPane tab="Đã kết thúc" key="4">
            <VoucherTable status={StatusLabelByTime.OVER} />
          </Tabs.TabPane>
        </Tabs>
      </Card>
    </Content>
  )
}

interface VoucherTableProps {
  status?: StatusLabelByTime
}

const disabledStatus = [StatusLabelByTime.OVER, StatusLabelByTime.INPROGRESS]
const VoucherTable: FC<VoucherTableProps> = ({ status }) => {
  const router = useRouter()
  const user = useRecoilValue(authState)
  const columns: ColumnsType<VoucherEntity> = [
    {
      title: 'Mã Voucher | Tên',
      width: 300,
      render: (_value, record) => {
        return (
          <Row>
            <Col className={'mr-3 w-16 text-center'}>
              <DollarCircleOutlined
                style={{
                  fontSize: 40,
                  background: '#fb81aa',
                  padding: 10,
                  color: '#fff',
                }}
              />
            </Col>
            <Col className="flex items-center font-medium flex-1">
              <Typography.Paragraph>
                <Typography.Title level={5} className="mb-0">
                  {record.code}
                </Typography.Title>
                <Typography.Text>{record.name}</Typography.Text>
              </Typography.Paragraph>
            </Col>
          </Row>
        )
      },
    },
    {
      dataIndex: 'type',
      title: 'Loại mã',
      render: (type, record) => {
        const title =
          type === 'product'
            ? 'Mã giảm giá trên sản phẩm'
            : 'Mã giảm giá toàn Shop'
        const text =
          type === 'product'
            ? `(${record.productIds.length} Sản phẩm)`
            : '(Tất cả sản phẩm)'

        return (
          <Typography.Paragraph>
            <Typography.Text className="block">{title}</Typography.Text>
            <Typography.Text type="secondary">{text}</Typography.Text>
          </Typography.Paragraph>
        )
      },
    },
    {
      dataIndex: 'discountValue',
      title: 'Giảm giá',
      render: (value, record) =>
        record.discountType === 'percentage'
          ? value + '%'
          : formatCurrency(value),
    },
    {
      title: 'Tổng số mã giảm giá có thể sử dụng',
      width: 140,
      render: (_value, record) => {
        const availableQuantity = record.quantity - record.quantityUsed
        return formatNumber(availableQuantity)
      },
    },
    {
      dataIndex: 'quantityUsed',
      title: (
        <>
          <Typography.Text>
            Đã dùng{' '}
            <Tooltip
              title={
                'Số lượng mã giảm giá đã sử dụng (không gồm những đơn đã hủy và đơn Trả hàng/Hoàn tiền)'
              }
            >
              <QuestionCircleOutlined />
            </Tooltip>
          </Typography.Text>
        </>
      ),
    },
    {
      title: 'Trạng thái',
      width: 160,
      render: (value, record) => {
        const statusValue = getStatusLabelByTime(
          record.startDate,
          record.endDate
        )
        let color = ''
        let label = ''

        switch (statusValue) {
          case StatusLabelByTime.INPROGRESS:
            color = 'processing'
            label = StatusLabelByTime.INPROGRESS
            break

          case StatusLabelByTime.COMMING:
            color = 'volcano'
            label = StatusLabelByTime.COMMING
            break

          default:
            color = 'default'
            label = StatusLabelByTime.OVER
            break
        }

        return (
          <Typography.Paragraph>
            <Tag color={color}>{label}</Tag>
            <Typography.Text className="block">
              {format(new Date(record.startDate), 'HH:mm d/MM/y')}
              <br />
              {format(new Date(record.endDate), 'HH:mm d/MM/y')}
            </Typography.Text>
          </Typography.Paragraph>
        )
      },
    },
    {
      title: 'Trạng thái',
      width: 160,
      render: (value, record) => {
        const statusValue = getStatusLabelByTime(
          record.startDate,
          record.endDate
        )
        return (
          <div className={'flex flex-col'}>
            <Link href={`/marketing/voucher/${record.id}`}>
              {disabledStatus.includes(statusValue) ? 'Chi tiết' : 'Sửa'}
            </Link>
            <div>
              {!disabledStatus.includes(statusValue) && (
                <Popconfirm
                  title="Bạn chắc chắn muốn xoá mục này?"
                  onConfirm={() => {
                    VouchersService.deleteOneBase({
                      id: record.id,
                    })
                      .then(() => {
                        alertSuccess('Xoá mã giảm giá thành công')
                        router.reload()
                      })
                      .catch((e) => alertError(e))
                  }}
                  disabled={disabledStatus.includes(statusValue)}
                >
                  <Button type={'link'} className={'p-0'}>
                    Xoá
                  </Button>
                </Popconfirm>
              )}
            </div>
          </div>
        )
      },
    },
  ]

  const [serviceParams, setServiceParams] = useState({
    sort: ['createdAt,DESC'],
  })

  useEffect(() => {
    let tmpServiceParams = {
      sort: ['createdAt,DESC'],
      filter: [
        user.type === EnumRoleEntityType.merchant
          ? `merchantId||eq||${user.merchantId}`
          : user.type === EnumRoleEntityType.admin
          ? `merchantId||isnull`
          : [],
      ],
    }
    if (status) {
      const now = new Date()
      switch (status) {
        case StatusLabelByTime.INPROGRESS:
          tmpServiceParams = {
            sort: ['createdAt,DESC'],
            filter: [
              `startDate||lt||${now.toISOString()}`,
              `endDate||gt||${now.toISOString()}`,
              user.type === EnumRoleEntityType.merchant
                ? `merchantId||eq||${user.merchantId}`
                : user.type === EnumRoleEntityType.admin
                ? `merchantId||isnull`
                : [],
            ],
          }
          break
        case StatusLabelByTime.COMMING:
          tmpServiceParams = {
            sort: ['createdAt,DESC'],
            filter: [
              `startDate||gt||${now.toISOString()}`,
              user.type === EnumRoleEntityType.merchant
                ? `merchantId||eq||${user.merchantId}`
                : user.type === EnumRoleEntityType.admin
                ? `merchantId||isnull`
                : [],
            ],
          }
          break
        case StatusLabelByTime.OVER:
          tmpServiceParams = {
            sort: ['createdAt,DESC'],
            filter: [
              `endDate||lt||${now.toISOString()}`,
              user.type === EnumRoleEntityType.merchant
                ? `merchantId||eq||${user.merchantId}`
                : user.type === EnumRoleEntityType.admin
                ? `merchantId||isnull`
                : [],
            ],
          }
          break
      }
    }
    setServiceParams(tmpServiceParams)
  }, [status])

  return (
    <DataTable
      columns={columns}
      service={VouchersService.getManyBase}
      deleteService={VouchersService.deleteOneBase}
      serviceParams={serviceParams}
      scroll={{ x: 1000 }}
      size={'small'}
      path={'/marketing/voucher'}
    />
  )
}

export default Index

import {
  CheckOutlined,
  DeleteOutlined,
  FileImageOutlined,
  PlusOutlined,
  ShopOutlined,
  ShoppingCartOutlined,
} from '@ant-design/icons'
import { yupResolver } from '@hookform/resolvers/yup'
import {
  Badge,
  Button,
  Card,
  Checkbox,
  Col,
  Form,
  Input,
  Modal,
  Popconfirm,
  Radio,
  Row,
  Select,
  Space,
  Table,
  Tooltip,
  Typography,
} from 'antd'
import { ColumnsType } from 'antd/es/table'
import DatePicker from 'components/common/DatePicker'
import InputNumber from 'components/common/InputNumber'
import Content from 'components/layout/AdminLayout/Content'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import { addHours, addMinutes, isBefore, isFuture } from 'date-fns'
import _, { uniq, uniqBy } from 'lodash'
import { useRouter } from 'next/router'
import React, { FC, useEffect, useState } from 'react'
import { Controller, SubmitHandler, useForm, useWatch } from 'react-hook-form'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { authState, loadingState } from 'recoil/Atoms'
import {
  EnumRoleEntityType,
  ProductEntity,
  ProductsService,
  VouchersService,
} from 'services'
import {
  alertError,
  formatCurrency,
  formatNumber,
  getCdnFile,
  getStatusLabelByTime,
  modifyEntity,
} from 'utils'
import { ServiceParams } from 'utils/interfaces'
import * as yup from 'yup'
import { SchemaOf } from 'yup'
import { StatusLabelByTime } from 'enums'
import DataTable from 'components/common/DataTable'

const formLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 12 },
}

interface Inputs {
  id?: number
  type: 'shop' | 'product'
  name: string
  code: string
  startDate: Date
  endDate: Date
  discountType: 'percentage' | 'amount'
  discountValue: number
  discountMaximum: number
  noDiscountMaximum: boolean
  minAmount: number
  quantity: number
  productIds?: number[]
  description?: string
  rangeTimeValue: [Date, Date]
  minProductNumber?: number
  minProductPrice?: number
}

const schema: SchemaOf<Inputs> = yup.object().shape({
  type: yup.mixed().oneOf(['shop', 'product']),
  name: yup.string().required('Không được để trống ô'),
  code: yup
    .string()
    .min(0)
    .max(10)
    .required('Không được để trống mã')
    .matches(
      /^[a-zA-Z0-9\s]+$/,
      'Vui lòng chỉ nhập các kí tự chữ cái (A-Z) số (0-9)'
    ),
  id: yup.number(),
  startDate: yup.date(),
  endDate: yup.date(),
  productIds: yup.array(),
  rangeTimeValue: yup.array(),
  description: yup.string(),
  discountType: yup.mixed().oneOf(['percentage', 'amount']).default('amount'),
  discountValue: yup
    .number()
    .when('discountType', {
      is: (value) => value === 'percentage',
      then: yup
        .number()
        .min(1, 'Mức khuyến mãi nhỏ hơn 1%')
        .max(100, 'Mức khuyến mãi lớn hơn 100%'),
    })
    .when('discountType', {
      is: (value) => value === 'amount',
      then: yup.number().min(1000, 'Giá trị thấp nhất nên là 1.000'),
    }),
  minAmount: yup.number(),
  quantity: yup
    .number()
    .min(1, 'Vui lòng nhập vào giá trị giữa 1 và 200000 .')
    .max(200000, 'Vui lòng nhập vào giá trị giữa 1 và 200000 .')
    .required('Không được để trống ô'),
  noDiscountMaximum: yup.boolean(),
  discountMaximum: yup.number(),
  minProductNumber: yup
    .number()
    .min(1, 'Giá trị nhỏ nhất là 1')
    .required('Không được để trống'),
})

const Index: FC = () => {
  const setIsLoading = useSetRecoilState(loadingState)
  const [cantEdit, setCanEdit] = useState(true)
  const [title, setTitle] = useState<string>('Tạo mới mã giảm giá')
  const router = useRouter()
  const [isAddProductVisible, setIsAddProductVisible] = useState<boolean>(false)
  const [discountValueWarning, setDiscountValueWarning] = useState('')
  const [selectedProducts, setSelectedProducts] = useState<ProductEntity[]>([])
  const {
    control,
    setValue,
    getValues,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
    defaultValues: {
      type: 'shop',
      name: '',
      code: '',
      discountType: 'amount',
      startDate: addMinutes(new Date(), 10),
      endDate: addHours(addMinutes(new Date(), 10), 1),
      rangeTimeValue: [
        addMinutes(new Date(), 10),
        addHours(addMinutes(new Date(), 10), 1),
      ],
      noDiscountMaximum: true,
    },
  })
  const currentTime = new Date()

  const watchType = useWatch({
    control,
    name: 'type',
  })
  const watchNoDiscountMaximum = useWatch({
    control,
    name: 'noDiscountMaximum',
  })
  const watchDiscountType = useWatch({
    control,
    name: 'discountType',
  })
  const watchDiscountValue = useWatch({
    control,
    name: 'discountValue',
  })
  const watchMinAmount = useWatch({
    control,
    name: 'minAmount',
  })

  const columns: ColumnsType<ProductEntity> = [
    {
      dataIndex: 'name',
      title: 'Sản phẩm',
      render: (value, record) => {
        return (
          <Row>
            <Col className={'mr-3 w-16 text-center'}>
              {record?.images && record?.images.length > 0 ? (
                <img
                  src={getCdnFile(record?.images?.[0])}
                  width={'60px'}
                  alt={value}
                />
              ) : (
                <FileImageOutlined style={{ fontSize: 40 }} />
              )}
            </Col>
            <Col className="flex items-center font-medium flex-1">
              <Tooltip title={value}>
                <Typography.Text>{value}</Typography.Text>
              </Tooltip>
            </Col>
          </Row>
        )
      },
      sorter: (a, b) => a.name.length - b.name.length,
    },
    {
      dataIndex: 'price',
      title: 'Giá',
      width: 150,
      render: (value) => formatCurrency(value),
      sorter: (a, b) => a.price - b.price,
    },
    {
      title: 'Kho hàng',
      width: 150,
      render: (_value, record) => {
        const productStockNumber =
          record.stock?.reduce((total, { quantity }) => total + quantity, 0) ||
          0
        return formatNumber(productStockNumber)
      },
      sorter: (a, b) => {
        const productStockNumberA =
          a.stock?.reduce((total, { quantity }) => total + quantity, 0) || 0
        const productStockNumberB =
          b.stock?.reduce((total, { quantity }) => total + quantity, 0) || 0
        return productStockNumberA - productStockNumberB
      },
    },
    {
      dataIndex: '',
      title: 'Xóa',
      width: 80,
      render: (value, record, index) => (
        <Popconfirm
          title="Bạn chắc chắn muốn xoá mục này?"
          onConfirm={() => {
            setSelectedProducts((prevState) => {
              const tmpSelectedProducts = [...prevState]
              tmpSelectedProducts.splice(index, 1)
              return tmpSelectedProducts
            })
          }}
          disabled={!cantEdit}
        >
          <Button
            shape={'circle'}
            key={index}
            icon={<DeleteOutlined />}
            disabled={!cantEdit}
          />
        </Popconfirm>
      ),
    },
  ]

  useEffect(() => {
    if (watchDiscountType === 'percentage') {
      if (watchDiscountValue >= 90) {
        setDiscountValueWarning('Mức khuyến mãi lớn hơn 90% so với giá gốc')
      } else if (watchDiscountValue >= 69) {
        setDiscountValueWarning('Mức khuyến mãi lớn hơn 69% so với giá gốc')
      }
    } else if (watchDiscountType === 'amount') {
      if ((watchDiscountValue / watchMinAmount) * 100 > 90) {
        setDiscountValueWarning(
          'Mức giá khuyến mãi lớn hơn 90 % so với giá trị đơn hàng tối thiểu'
        )
      } else if ((watchDiscountValue / watchMinAmount) * 100 > 69) {
        setDiscountValueWarning(
          'Mức giá khuyến mãi lớn hơn 69 % so với giá trị đơn hàng tối thiểu'
        )
      } else {
        setDiscountValueWarning('')
      }
    } else {
      setDiscountValueWarning('')
    }
  }, [watchMinAmount, watchDiscountType, watchDiscountValue])

  useEffect(() => {
    setValue('discountValue', 0)
  }, [watchDiscountType])

  useEffect(() => {
    setTitle('Tạo mới mã giảm giá')
    if (router?.query?.id && router?.query?.id !== 'create') {
      setTitle('Cập nhật mã giảm giá')
      if (Number(router?.query?.id)) {
        VouchersService.getOneBase({
          id: Number(router?.query?.id),
        })
          .then((response) => {
            if (response) {
              const statusValue = getStatusLabelByTime(
                response.startDate,
                response.endDate
              )
              if (
                [StatusLabelByTime.OVER, StatusLabelByTime.INPROGRESS].includes(
                  statusValue
                )
              ) {
                setCanEdit(false)
              }
              switch (response.type) {
                case 'shop':
                  setValue('type', 'shop')
                  break
                case 'product':
                  setValue('type', 'product')
                  break
              }
              setValue('name', response.name)
              setValue('code', response.code)
              setValue('rangeTimeValue', [
                new Date(response.startDate),
                new Date(response.endDate),
              ])
              switch (response.discountType) {
                case 'percentage':
                  setValue('discountType', 'percentage')
                  break
                case 'amount':
                  setValue('discountType', 'amount')
                  break
              }
              setValue('discountValue', response.discountValue)
              setValue('minAmount', response.minAmount)
              setValue('quantity', response.quantity)
              setValue('productIds', response.productIds)
              setValue('description', response.description)
              setValue('minProductNumber', response.minProductNumber)
              setValue('minProductPrice', response.minProductPrice)
              setValue('discountMaximum', response.discountMaximum)
              setValue('noDiscountMaximum', !(response.discountMaximum > 0))
              setSelectedProducts(response.products)
            }
          })
          .catch((e) => {
            alertError(e)
          })
      }
    }
  }, [router?.query?.id])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    setIsLoading(true)
    data.startDate = getValues('rangeTimeValue')[0]
    data.endDate = getValues('rangeTimeValue')[1]
    data.id = Number(router?.query?.id || 0)
    data.productIds = selectedProducts?.map((product) => product.id)
    modifyEntity(
      VouchersService,
      data,
      title,
      () => {
        setIsLoading(false)
        return router.push(`/marketing/voucher`)
      },
      () => {
        setIsLoading(false)
      }
    ).then()
  }

  return (
    <Content title={title} onBack={() => router.push('/marketing/voucher')}>
      <Form {...formLayout} onFinish={handleSubmit(onSubmit)} colon={false}>
        <Card>
          <Typography.Title level={4}>Thông tin cơ bản</Typography.Title>
          <Form.Item label={'Loại mã'}>
            <Row gutter={30}>
              <Col span={12}>
                <Badge.Ribbon
                  text={<CheckOutlined />}
                  color={watchType === 'shop' ? '' : 'gray'}
                >
                  <Card
                    size="small"
                    className={
                      (cantEdit ? 'cursor-pointer hover:border-blue-400 ' : '') +
                      (watchType === 'shop' ? 'border-blue-400' : '')
                    }
                    onClick={() => cantEdit && setValue('type', 'shop')}
                  >
                    <Row>
                      <Col className={'mr-3 w-15'}>
                        <ShopOutlined style={{ fontSize: 32 }} />
                      </Col>
                      <Col className="flex flex-1 justify-center items-center font-medium">
                        Voucher toàn shop
                      </Col>
                    </Row>
                  </Card>
                </Badge.Ribbon>
              </Col>
              <Col span={12}>
                <Badge.Ribbon
                  text={<CheckOutlined />}
                  color={watchType === 'product' ? '' : 'gray'}
                >
                  <Card
                    size="small"
                    className={
                      (cantEdit ? 'cursor-pointer hover:border-blue-400 ' : '') +
                      (watchType === 'product' ? 'border-blue-400' : '')
                    }
                    onClick={() => cantEdit && setValue('type', 'product')}
                  >
                    <Row>
                      <Col className={'mr-3 w-15'}>
                        <ShoppingCartOutlined style={{ fontSize: 32 }} />
                      </Col>
                      <Col className="flex flex-1 justify-center items-center font-medium">
                        Voucher sản phẩm
                      </Col>
                    </Row>
                  </Card>
                </Badge.Ribbon>
              </Col>
            </Row>
          </Form.Item>
          <Form.Item
            label={'Tên chương trình giảm giá'}
            required={true}
            validateStatus={errors?.name && 'error'}
            help={errors?.name?.message}
          >
            <Controller
              control={control}
              name="name"
              render={({ field }) => (
                <>
                  <Input
                    {...field}
                    placeholder="Nhập vào"
                    disabled={!cantEdit}
                  />
                </>
              )}
            />
          </Form.Item>
          <Form.Item label={'Mô tả'} required={true}>
            <Controller
              control={control}
              name="description"
              render={({ field }) => (
                <>
                  <Input.TextArea
                    rows={3}
                    {...field}
                    placeholder="Nhập vào"
                    disabled={!cantEdit}
                  />
                </>
              )}
            />
          </Form.Item>
          <Form.Item
            label={'Mã voucher'}
            required={true}
            validateStatus={errors?.code && 'error'}
            help={errors?.code?.message}
            className={'mb-3'}
            extra={
              'Vui lòng chỉ nhập các kí tự chữ cái (A-Z) số (0-9): Tối đa 10 kí tự.'
            }
          >
            <Controller
              control={control}
              name="code"
              render={({ field }) => (
                <>
                  <Input
                    {...field}
                    maxLength={10}
                    placeholder="Nhập vào"
                    onChange={(e) => {
                      const code = e.target.value
                        .normalize('NFD')
                        .replace(/[\u0300-\u036f]/g, '')
                      setValue('code', code.toUpperCase())
                    }}
                    disabled={!cantEdit}
                  />
                </>
              )}
            />
          </Form.Item>
          <Form.Item
            label={'Thời gian sử dụng mã'}
            required={true}
            validateStatus={(errors?.startDate || errors?.endDate) && 'error'}
            help={errors?.startDate?.message || errors?.endDate?.message}
          >
            <Controller
              name={'rangeTimeValue'}
              control={control}
              render={({ field }) => (
                <DatePicker.RangePicker
                  {...field}
                  showTime
                  allowClear={true}
                  inputReadOnly
                  format="HH:mm DD-MM-YYYY"
                  disabledDate={(date) => isBefore(date, currentTime)}
                  disabledTime={(date) => ({
                    disabledHours: () => {
                      if (isFuture(date)) return []
                      return _.times(24).filter(
                        (h) => currentTime.getHours() + 1 > h
                      )
                    },
                  })}
                  disabled={!cantEdit}
                />
              )}
            />
            <Typography.Paragraph className="mt-3">
              <Checkbox className="mr-2" disabled={!cantEdit} />
              Cho phép lưu mã trước thời gian sử dụng
            </Typography.Paragraph>
          </Form.Item>
          <Typography.Title level={4}>Thiết lập mã giảm giá</Typography.Title>
          {/*<Form.Item label={'Loại Voucher'}>*/}
          {/*  <Radio.Group style={{ width: '100%' }} disabled={!cantEdit}>*/}
          {/*    <Row>*/}
          {/*      <Col span={6}>*/}
          {/*        <Radio value={1}>Khuyến mãi</Radio>*/}
          {/*      </Col>*/}
          {/*      <Col span={6}>*/}
          {/*        <Radio value={2}>*/}
          {/*          Hoàn Xu{' '}*/}
          {/*          <Tooltip*/}
          {/*            placement="top"*/}
          {/*            title={*/}
          {/*              'Người mua sẽ được nhận lại xu khi sử dụng voucher hoàn xu. Người bán sẽ chịu chi phí này.'*/}
          {/*            }*/}
          {/*          >*/}
          {/*            <QuestionCircleOutlined />*/}
          {/*          </Tooltip>*/}
          {/*        </Radio>*/}
          {/*      </Col>*/}
          {/*    </Row>*/}
          {/*  </Radio.Group>*/}
          {/*</Form.Item>*/}
          <Form.Item
            label="Loại giảm giá | Mức giảm"
            required={true}
            validateStatus={
              errors?.discountType || errors?.discountValue
                ? 'error'
                : discountValueWarning !== '' && 'warning'
            }
            help={
              errors?.discountType?.message ||
              errors?.discountValue?.message ||
              discountValueWarning
            }
          >
            <Row>
              <Col span={10}>
                <Controller
                  control={control}
                  name={'discountType'}
                  render={({ field }) => (
                    <Select showSearch {...field} disabled={!cantEdit}>
                      <Select.Option value="percentage">
                        Theo phần trăm
                      </Select.Option>
                      <Select.Option value="amount">Theo số tiền</Select.Option>
                    </Select>
                  )}
                />
              </Col>
              <Col span={14}>
                <Controller
                  control={control}
                  name={`discountValue`}
                  render={({ field }) => (
                    <>
                      {watchDiscountType === 'amount' ? (
                        <InputNumber
                          {...field}
                          min={0}
                          addonBefore={'₫'}
                          className={'w-full'}
                          disabled={!cantEdit}
                        />
                      ) : (
                        <InputNumber
                          {...field}
                          min={0}
                          max={100}
                          addonAfter={'%GIẢM'}
                          className={'w-full'}
                          disabled={!cantEdit}
                        />
                      )}
                    </>
                  )}
                />
              </Col>
            </Row>
          </Form.Item>
          {watchDiscountType === 'percentage' && (
            <Form.Item
              label={'Mức giảm tối đa'}
              validateStatus={errors.discountMaximum && 'error'}
              help={errors.discountMaximum?.message}
            >
              <Controller
                control={control}
                name="noDiscountMaximum"
                render={({ field }) => (
                  <Radio.Group
                    {...field}
                    className={'w-full'}
                    disabled={!cantEdit}
                  >
                    <Radio value={false}>Giới hạn</Radio>
                    <Radio value={true}>Không giới hạn</Radio>
                  </Radio.Group>
                )}
              />
              {!watchNoDiscountMaximum && (
                <Controller
                  control={control}
                  name="discountMaximum"
                  render={({ field }) => (
                    <InputNumber
                      {...field}
                      min={0}
                      className={'w-full'}
                      addonBefore={'đ'}
                      disabled={!cantEdit}
                    />
                  )}
                />
              )}
            </Form.Item>
          )}
          <Form.Item
            label={'Giá trị đơn hàng tối thiểu'}
            validateStatus={errors?.minAmount && 'error'}
            help={errors?.minAmount?.message}
          >
            <Controller
              control={control}
              name="minAmount"
              render={({ field }) => (
                <InputNumber
                  {...field}
                  min={0}
                  addonBefore={'₫'}
                  className={'w-full'}
                  disabled={!cantEdit}
                />
              )}
            />
          </Form.Item>
          <Form.Item
            label={'Lượt sử dụng tối đa'}
            required={true}
            validateStatus={errors?.quantity && 'error'}
            help={errors?.quantity?.message}
            extra={'Tổng số Mã giảm giá có thể sử dụng'}
          >
            <Controller
              control={control}
              name="quantity"
              render={({ field }) => (
                <InputNumber
                  {...field}
                  min={0}
                  className={'w-full'}
                  disabled={!cantEdit}
                />
              )}
            />
          </Form.Item>
          <Form.Item
            label={'Số sản phẩm tối thiểu/đơn'}
            required={true}
            validateStatus={errors?.minProductNumber && 'error'}
            help={errors?.minProductNumber?.message}
            extra={'Số sản phẩm tối thiểu trong 1 đơn hàng'}
          >
            <Controller
              control={control}
              name="minProductNumber"
              render={({ field }) => (
                <InputNumber
                  {...field}
                  min={0}
                  className={'w-full'}
                  disabled={!cantEdit}
                />
              )}
            />
          </Form.Item>
          <Form.Item
            label={'Giá tối thiểu của sản phẩm'}
            extra={'Giá tối thiểu cho 1 sản phẩm'}
          >
            <Controller
              control={control}
              name="minProductPrice"
              render={({ field }) => (
                <InputNumber
                  {...field}
                  min={0}
                  className={'w-full'}
                  disabled={!cantEdit}
                />
              )}
            />
          </Form.Item>
          <Form.Item label={'Sản phẩm được áp dụng'} wrapperCol={{ span: 18 }}>
            {watchType === 'shop' ? (
              <Typography.Text>Tất cả sản phẩm</Typography.Text>
            ) : (
              <Form.Item
                required={true}
                validateStatus={errors?.productIds && 'error'}
                help={
                  (selectedProducts.length === 0 && (errors?.productIds as any))
                    ?.message
                }
              >
                <Row className={'w-full'}>
                  <Col xs={24} md={12}>
                    {selectedProducts.length > 0 && (
                      <>
                        <Typography.Text strong>
                          {selectedProducts.length}
                        </Typography.Text>{' '}
                        Sản phẩm được chọn
                      </>
                    )}
                  </Col>
                  <Col xs={24} md={12} className={'text-right'}>
                    <Button
                      danger={
                        !!errors.productIds && selectedProducts.length === 0
                      }
                      type="primary"
                      icon={<PlusOutlined />}
                      onClick={() => {
                        setIsAddProductVisible(true)
                      }}
                      disabled={!cantEdit}
                      ghost={true}
                    >
                      Thêm sản phẩm
                    </Button>
                  </Col>
                </Row>
                {selectedProducts.length > 0 && (
                  <Table
                    rowKey={'id'}
                    dataSource={selectedProducts}
                    columns={columns}
                    scroll={{ y: 600 }}
                    className="mt-4"
                  />
                )}
              </Form.Item>
            )}
          </Form.Item>
        </Card>
        {cantEdit && (
          <FooterBar
            right={
              <Button htmlType={'submit'} type={'primary'}>
                Lưu
              </Button>
            }
          />
        )}
      </Form>

      <ModalSelectionProducts
        visible={isAddProductVisible}
        selectedRows={selectedProducts.map(
          (product) => `product-${product.id}`
        )}
        onCancel={() => setIsAddProductVisible(false)}
        onOk={(products) => {
          const productIds = products?.map(({ id }) => id)
          setValue('productIds', productIds)
          const prods = products.reduce((arr, prod) => {
            if (!selectedProducts.find((p) => p.id === prod.id)) arr.push(prod)
            return arr
          }, [])
          setSelectedProducts([...selectedProducts, ...prods])
          setIsAddProductVisible(false)
        }}
      />
    </Content>
  )
}

interface IModalSelectionProductProps {
  visible: boolean
  selectedRows: string[]
  onOk?: (selectedProducts: ProductEntity[]) => void
  onCancel?: () => void
}

interface IModalSelectionProductInputs {
  searchText?: string
}

/**
 * Selection products modal
 * @returns JSX
 */
const ModalSelectionProducts: FC<IModalSelectionProductProps> = ({
  visible,
  selectedRows,
  onOk,
  onCancel,
}: IModalSelectionProductProps) => {
  const user = useRecoilValue(authState)
  const [selectedProducts, setSelectedProducts] = useState<ProductEntity[]>([])
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>()

  const [tableProductServiceParams, setTableProductServiceParams] =
    useState<ServiceParams>({
      sort: ['createdAt,DESC'],
      filter: user.merchantId && [`merchantId||$eq||${user.merchantId}`],
    })
  const columns: ColumnsType<ProductEntity> = [
    {
      dataIndex: 'name',
      title: 'Sản phẩm',
      render: (value, record) => {
        return (
          <Row>
            <Col className={'mr-3 w-16 text-center'}>
              {record?.images && record?.images.length > 0 ? (
                <img
                  src={getCdnFile(record?.images?.[0])}
                  width={'60px'}
                  alt={value}
                />
              ) : (
                <FileImageOutlined style={{ fontSize: 40 }} />
              )}
            </Col>
            <Col className="flex items-center font-medium flex-1">
              <Tooltip title={value}>
                <Typography.Text>{value}</Typography.Text>
              </Tooltip>
            </Col>
          </Row>
        )
      },
      sorter: (a, b) => a.name.length - b.name.length,
    },
    {
      dataIndex: 'price',
      title: 'Giá',
      width: 150,
      render: (value) => formatCurrency(value),
      sorter: (a, b) => a.price - b.price,
    },
    {
      title: 'Kho hàng',
      width: 150,
      render: (_value, record) => {
        const productStockNumber =
          record.stock?.reduce((total, { quantity }) => total + quantity, 0) ||
          0
        return formatNumber(productStockNumber)
      },
      sorter: (a, b) => {
        const productStockNumberA =
          a.stock?.reduce((total, { quantity }) => total + quantity, 0) || 0
        const productStockNumberB =
          b.stock?.reduce((total, { quantity }) => total + quantity, 0) || 0
        return productStockNumberA - productStockNumberB
      },
    },
  ]

  const {
    control: productControl,
    setValue: setProductValue,
    handleSubmit: handleSubmitSearchProducts,
  } = useForm<IModalSelectionProductInputs>({
    defaultValues: {
      searchText: '',
    },
  })
  const [form] = Form.useForm()

  const onSubmitProductForm: SubmitHandler<IModalSelectionProductInputs> = (
    data
  ) => {
    const filter = []
    if (data?.searchText) {
      filter.push(`name||$contL||${data?.searchText}`)
    }
    if (user.type === EnumRoleEntityType.merchant)
      filter.push(`merchantId||eq||${user.merchantId}`)
    setTableProductServiceParams((prevState) => ({
      ...prevState,
      filter: filter,
    }))
  }

  const onResetSearchProducts = () => {
    setProductValue('searchText', '')
    setTableProductServiceParams({
      sort: ['createdAt,DESC'],
      filter: [
        user.type === EnumRoleEntityType.merchant
          ? `merchantId||eq||${user.merchantId}`
          : [],
      ],
    })
  }

  useEffect(() => {
    if (visible) {
      setSelectedProducts([])
    }
    setSelectedRowKeys(selectedRows)
  }, [visible])

  return (
    <>
      <Modal
        title="Chọn Sản Phẩm"
        width={'960px'}
        visible={visible}
        onCancel={onCancel}
        footer={[
          <span key={'count'} className={'mr-3'}>
            Đã chọn {selectedRowKeys?.length} sản phẩm
          </span>,
          ...(onCancel && [
            <Button key={'cancel'} onClick={onCancel}>
              Huỷ
            </Button>,
          ]),
          ...(onOk && [
            <Button
              key={'submit'}
              type="primary"
              onClick={() => onOk(selectedProducts)}
            >
              Xác nhận
            </Button>,
          ]),
        ]}
      >
        <Form
          form={form}
          autoComplete={'off'}
          onFinish={handleSubmitSearchProducts(onSubmitProductForm)}
          onReset={onResetSearchProducts}
        >
          <Row gutter={[16, 16]}>
            <Col xs={24} sm={24} md={24} lg={12}>
              <Form.Item label={'Nhập tên'}>
                <Controller
                  control={productControl}
                  name={'searchText'}
                  render={({ field }) => (
                    <Input {...field} placeholder="Nhập tên sản phẩm" />
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} className={'text-right'}>
              <Form.Item>
                <Space>
                  <Button type={'primary'} htmlType={'submit'}>
                    Tìm
                  </Button>
                  <Button htmlType={'reset'}>Nhập lại</Button>
                </Space>
              </Form.Item>
            </Col>
          </Row>
        </Form>
        <>
          <DataTable
            service={ProductsService.getManyBase}
            serviceParams={tableProductServiceParams}
            columns={columns}
            rowKey={(record) => `product-${record.id}`}
            rowSelection={{
              type: 'checkbox',
              onSelectAll: (selected, selectedRows, changeRows) => {
                if (selected) {
                  setSelectedProducts((prevState) =>
                    uniqBy([...prevState, ...selectedRows], 'id')
                  )
                  setSelectedRowKeys((prevState) =>
                    uniq([
                      ...prevState,
                      ...selectedRows.map((row) => `product-${row.id}`),
                    ])
                  )
                } else {
                  setSelectedProducts((prevState) => {
                    changeRows.map((row) => {
                      const index = prevState.findIndex(
                        (product) => product.id === row.id
                      )
                      if (index > -1) {
                        prevState.splice(index, 1)
                      }
                    })

                    return prevState
                  })
                  setSelectedRowKeys((prevState) => {
                    changeRows.map((row) => {
                      const index = prevState.findIndex(
                        (key) => key === `product-${row.id}`
                      )
                      if (index > -1) {
                        prevState.splice(index, 1)
                      }
                    })
                    return prevState
                  })
                }
              },
              onSelect: (record, selected) => {
                if (selected) {
                  setSelectedProducts((prevState) =>
                    uniqBy([...prevState, record], 'id')
                  )
                  setSelectedRowKeys((prevState) =>
                    uniq([...prevState, `product-${record.id}`])
                  )
                } else {
                  setSelectedProducts((prevState) => {
                    const index = prevState.findIndex(
                      (product) => product.id === record.id
                    )
                    if (index > -1) {
                      prevState.splice(index, 1)
                    }
                    return prevState
                  })
                  setSelectedRowKeys((prevState) => {
                    const index = prevState.findIndex(
                      (key) => key === `product-${record.id}`
                    )
                    if (index > -1) {
                      prevState.splice(index, 1)
                    }
                    return prevState
                  })
                }
              },
              onChange: (
                selectedRowKeys: React.Key[],
                selectedRows: ProductEntity[]
              ) => {
                setSelectedProducts((prevState) =>
                  uniqBy([...prevState, ...selectedRows], 'id')
                )
                setSelectedRowKeys((prevState) =>
                  uniq([...prevState, ...selectedRowKeys])
                )
              },
              selectedRowKeys: selectedRowKeys,
              getCheckboxProps: (record) => ({
                disabled: selectedRows.find(
                  (key) => Number(key.split('-')[1]) === record.id
                ),
              }),
            }}
            scroll={{ y: 300 }}
          />
        </>
      </Modal>
    </>
  )
}

export default Index

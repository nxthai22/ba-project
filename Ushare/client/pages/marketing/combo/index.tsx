import {
  Button,
  Card,
  Col,
  Form,
  Input,
  Row,
  Select,
  Space,
  Statistic,
  Tabs,
  Tag,
} from 'antd'
import { ColumnsType } from 'antd/es/table'
import DataTable from 'components/common/DataTable'
import Content from 'components/layout/AdminLayout/Content'
import React, { FC, useState } from 'react'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { formatDate, getStatusLabelByTime } from 'utils'
import { ServiceParams } from 'utils/interfaces'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { authState, countDataTable, loadingState } from 'recoil/Atoms'
import { TabPane } from 'rc-tabs'
import FormItem from '../../../components/common/FormItem'
import DatePicker from '../../../components/common/DatePicker'
import { format } from 'date-fns'

export enum StatusLabelByTime {
  INPROGRESS = 'Đang diễn ra',
  COMMING = 'Sắp diễn ra',
  OVER = 'Đã kết thúc',
}

export enum EnumComboType {
  DEAL_SHOCK = 'deal_shock',
  GIFT = 'gift',
}

interface Inputs {
  q?: string
  type?: string
  dates?: [Date, Date]
  searchText?: string
}

const Index: FC = () => {
  const user = useRecoilValue(authState)
  const setIsLoading = useSetRecoilState(loadingState)
  const isLoading = useRecoilValue(loadingState)
  const countDatatable = useRecoilValue(countDataTable)
  const [title] = useState<string>('Mua combo')
  const { control, handleSubmit, reset } = useForm<Inputs>({
    defaultValues: {
      searchText: '',
    },
  })
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['updatedAt,DESC'],
  })

  const columns: ColumnsType<any> = [
    {
      dataIndex: 'name',
      title: 'Tên',
      width: 200,
    },
    {
      dataIndex: 'type',
      title: 'Loại deal sốc',
      render: (value) => {
        switch (value) {
          case EnumComboType.GIFT:
            return 'Mua để nhận quà'
          case EnumComboType.DEAL_SHOCK:
            return 'Mua kèm Deal Sốc'
        }
      },
    },
    {
      dataIndex: 'products',
      title: 'Sản phẩm chính',
      width: 250,
    },
    {
      dataIndex: 'productExtras',
      title: 'Sản phẩm mua kèm|Quà tặng',
      width: 250,
    },
    {
      title: 'Trạng thái',
      render: (_value, record) => {
        const statusValue = getStatusLabelByTime(
          record.startTime,
          record.endTime
        )

        if (statusValue === StatusLabelByTime.INPROGRESS) {
          return <Tag color={'processing'}>{StatusLabelByTime.INPROGRESS}</Tag>
        }

        if (statusValue === StatusLabelByTime.COMMING) {
          return <Tag color={'volcano'}>{StatusLabelByTime.COMMING}</Tag>
        }

        return <Tag color={'default'}>{StatusLabelByTime.OVER}</Tag>
      },
    },
    {
      title: 'Thời gian',
      render: (value, record) => {
        return (
          <>
            {formatDate(record.startTime)} - {formatDate(record.endTime)}
          </>
        )
      },
    },
    {
      dataIndex: 'Thao tác',
      title: 'Kết thúc',
      render: (value, record) => {
        return (
          <>
            <Button type={'link'}> Chỉnh sửa</Button>
            <Button type={'link'}> Sao chép</Button>
            <Button type={'link'}> Xóa</Button>
            <Button type={'link'}> Kết thúc</Button>
          </>
        )
      },
    },
  ]

  const removeFilter = (filters: string[], key) => {
    let existIndex = -1
    if (filters.length > 0) {
      filters?.map((filter, index) => {
        if (filter?.includes(key)) {
          existIndex = index
        }
      })
      if (existIndex > -1) filters.splice(existIndex, 1)
    }
    return filters
  }

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    setTableServiceParams((prevState) => {
      if (data.q) {
        removeFilter(prevState.filter, 'code')
        removeFilter(prevState.filter, 'fullname')
        let filterTxt = ''
        switch (data.type) {
          case 'code':
            filterTxt = `code||$contL||${data?.q}`
            removeFilter(prevState.filter, 'fullname')
            removeFilter(prevState.filter, 'tel')
            break
          case 'fullname':
            filterTxt = `fullname||$contL||${data?.q}`
            removeFilter(prevState.filter, 'code')
            removeFilter(prevState.filter, 'tel')
            break
          case 'tel':
            filterTxt = `tel||$contL||${data?.q}`
            removeFilter(prevState.filter, 'code')
            removeFilter(prevState.filter, 'fullname')
            break
        }
        prevState = {
          ...prevState,
          filter: [...removeFilter(prevState.filter, data.type), filterTxt],
        }
      } else {
        removeFilter(prevState.filter, 'code')
        removeFilter(prevState.filter, 'fullname')
        removeFilter(prevState.filter, 'tel')
      }
      if (data?.dates?.length > 0) {
        prevState = {
          ...prevState,
          filter: [
            ...removeFilter(prevState.filter, 'startTime'),
            ...removeFilter(prevState.filter, 'endTime'),
            `startTime||between||${format(
              data.dates[0],
              'yyyy-MM-dd'
            )}  00:00:00,${format(data.dates[1], 'yyyy-MM-dd')} 23:59:59`,
            `endTime||between||${format(
              data.dates[0],
              'yyyy-MM-dd'
            )}  00:00:00,${format(data.dates[1], 'yyyy-MM-dd')} 23:59:59`,
          ],
        }
      } else {
        removeFilter(prevState.filter, 'startTime')
        removeFilter(prevState.filter, 'endTime')
      }
      return { ...prevState }
    })
  }

  const onReset = () => {
    reset()
    setTableServiceParams({
      sort: ['updateAt,DESC'],
    })
  }

  return (
    <Content title={title}>
      <Card title={'Chỉ số quan trọng (7 ngày gần đây)'} className="voucher">
        <Row gutter={30}>
          <Col
            xs={24}
            sm={24}
            md={6}
            lg={6}
            style={{ borderRight: '1px solid #c4c4c4' }}
          >
            <Statistic
              title="Doanh số sản phẩm chính"
              value={128824000}
              prefix={'₫'}
            />
            <p style={{ marginTop: 15 }}>
              <strong>0.00%</strong>
            </p>
          </Col>
          <Col
            xs={24}
            sm={24}
            md={6}
            lg={6}
            style={{ borderRight: '1px solid #c4c4c4' }}
          >
            <Statistic title="Doanh số sản phẩm mua kèm" value={1500} />
            <p style={{ marginTop: 15 }}>
              so với 7 ngày trước <strong>0.00%</strong>
            </p>
          </Col>
          <Col
            xs={24}
            sm={24}
            md={6}
            lg={6}
            style={{ borderRight: '1px solid #c4c4c4' }}
          >
            <Statistic title="Đơn hàng" value={1500} />
            <p style={{ marginTop: 15 }}>
              so với 7 ngày trước <strong>0.00%</strong>
            </p>
          </Col>
          <Col xs={24} sm={24} md={6} lg={6}>
            <Statistic title="Người mua" value={800} />
            <p style={{ marginTop: 15 }}>
              so với 7 ngày trước <strong>0.00%</strong>
            </p>
          </Col>
        </Row>
      </Card>
      <Content>
        <Card
          title={'Danh sách chương trình'}
          extra={
            <Button type={'primary'} href={'/marketing/combo/create'}>
              Thêm mới
            </Button>
          }
        >
          <Tabs defaultActiveKey="1">
            <TabPane tab="Tất cả" key="1">
              <Form className={'mt-6'} onFinish={handleSubmit(onSubmit)}>
                <div
                  className={'flex gap-4'}
                  style={{ justifyContent: 'space-between' }}
                >
                  <Input.Group compact style={{ width: '50%' }}>
                    <Controller
                      control={control}
                      name={'type'}
                      render={({ field }) => (
                        <Select
                          {...field}
                          placeholder="Tìm kiếm theo"
                          style={{ width: '40%' }}
                          defaultValue="name"
                        >
                          <Select.Option value="name">
                            Tên chương trình
                          </Select.Option>
                          <Select.Option value="product_name">
                            Tên sản phẩm
                          </Select.Option>
                          <Select.Option value="product_sku">
                            Mã sản phẩm
                          </Select.Option>
                        </Select>
                      )}
                    />
                    <Controller
                      control={control}
                      name={'q'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          style={{ width: '60%' }}
                          placeholder="Nhập vào"
                        />
                      )}
                    />
                  </Input.Group>
                  <FormItem
                    style={{ width: '50%' }}
                    label={'Thời gian khuyến mại'}
                  >
                    <Controller
                      control={control}
                      name={'dates'}
                      render={({ field }) => (
                        <DatePicker.RangePicker
                          format={'d/MM/y'}
                          {...field}
                          style={{
                            width: '100%',
                          }}
                        />
                      )}
                    />
                  </FormItem>
                  <Form.Item>
                    <Space>
                      <Button htmlType={'reset'}>Đặt lại</Button>
                      <Button type={'primary'} htmlType={'submit'}>
                        Tìm kiếm
                      </Button>
                    </Space>
                  </Form.Item>
                </div>
              </Form>
            </TabPane>
            <TabPane tab="Đang diễn ra" key="2">
              <Form className={'mt-6'} onFinish={handleSubmit(onSubmit)}>
                <div
                  className={'flex gap-4'}
                  style={{ justifyContent: 'space-between' }}
                >
                  <Input.Group compact>
                    <Controller
                      control={control}
                      name={'type'}
                      render={({ field }) => (
                        <Select
                          {...field}
                          placeholder="Tìm kiếm theo"
                          style={{ width: '30%' }}
                          defaultValue="name"
                        >
                          <Select.Option value="name">
                            Tên chương trình
                          </Select.Option>
                          <Select.Option value="product_name">
                            Tên sản phẩm
                          </Select.Option>
                        </Select>
                      )}
                    />
                    <Controller
                      control={control}
                      name={'q'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          style={{ width: '70%' }}
                          placeholder="Nhập vào"
                        />
                      )}
                    />
                  </Input.Group>
                  <Form.Item>
                    <Space>
                      <Button htmlType={'reset'}>Đặt lại</Button>
                      <Button type={'primary'} htmlType={'submit'}>
                        Tìm kiếm
                      </Button>
                    </Space>
                  </Form.Item>
                </div>
              </Form>
            </TabPane>
            <TabPane tab="Sắp diễn ra" key="3">
              <Form className={'mt-6'} onFinish={handleSubmit(onSubmit)}>
                <div
                  className={'flex gap-4'}
                  style={{ justifyContent: 'space-between' }}
                >
                  <Input.Group compact style={{ width: '50%' }}>
                    <Controller
                      control={control}
                      name={'type'}
                      render={({ field }) => (
                        <Select
                          {...field}
                          placeholder="Tìm kiếm theo"
                          style={{ width: '40%' }}
                          defaultValue="name"
                        >
                          <Select.Option value="name">
                            Tên chương trình
                          </Select.Option>
                          <Select.Option value="product_name">
                            Tên sản phẩm
                          </Select.Option>
                        </Select>
                      )}
                    />
                    <Controller
                      control={control}
                      name={'q'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          style={{ width: '60%' }}
                          placeholder="Nhập vào"
                        />
                      )}
                    />
                  </Input.Group>
                  <FormItem
                    style={{ width: '50%' }}
                    label={'Thời gian khuyến mại'}
                  >
                    <Controller
                      control={control}
                      name={'dates'}
                      render={({ field }) => (
                        <DatePicker.RangePicker
                          format={'d/MM/y'}
                          {...field}
                          style={{
                            width: '100%',
                          }}
                        />
                      )}
                    />
                  </FormItem>
                  <Form.Item>
                    <Space>
                      <Button htmlType={'reset'}>Đặt lại</Button>
                      <Button type={'primary'} htmlType={'submit'}>
                        Tìm kiếm
                      </Button>
                    </Space>
                  </Form.Item>
                </div>
              </Form>
            </TabPane>
            <TabPane tab="Đã kết thúc" key="4">
              <Form className={'mt-6'} onFinish={handleSubmit(onSubmit)}>
                <div
                  className={'flex gap-4'}
                  style={{ justifyContent: 'space-between' }}
                >
                  <Input.Group compact style={{ width: '50%' }}>
                    <Controller
                      control={control}
                      name={'type'}
                      render={({ field }) => (
                        <Select
                          {...field}
                          placeholder="Tìm kiếm theo"
                          style={{ width: '40%' }}
                          defaultValue="name"
                        >
                          <Select.Option value="name">
                            Tên chương trình
                          </Select.Option>
                          <Select.Option value="product_name">
                            Tên sản phẩm
                          </Select.Option>
                        </Select>
                      )}
                    />
                    <Controller
                      control={control}
                      name={'q'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          style={{ width: '60%' }}
                          placeholder="Nhập vào"
                        />
                      )}
                    />
                  </Input.Group>
                  <FormItem
                    style={{ width: '50%' }}
                    label={'Thời gian khuyến mại'}
                  >
                    <Controller
                      control={control}
                      name={'dates'}
                      render={({ field }) => (
                        <DatePicker.RangePicker
                          format={'d/MM/y'}
                          {...field}
                          style={{
                            width: '100%',
                          }}
                        />
                      )}
                    />
                  </FormItem>
                  <Form.Item>
                    <Space>
                      <Button htmlType={'reset'}>Đặt lại</Button>
                      <Button type={'primary'} htmlType={'submit'}>
                        Tìm kiếm
                      </Button>
                    </Space>
                  </Form.Item>
                </div>
              </Form>
            </TabPane>
          </Tabs>
          <DataTable
            scroll={{
              x: 1500,
            }}
            columns={columns}
          />
        </Card>
      </Content>
    </Content>
  )
}

export default Index

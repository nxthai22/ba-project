import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import _ from 'lodash'
import { useRouter } from 'next/router'
import { FC, useEffect, useState } from 'react'
import { Button, Card, Form, Input, InputNumber } from 'antd'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { alertError, modifyEntity } from 'utils'
import { CreateFlashSaleDto, FlashSaleService } from 'services'
import Content from 'components/layout/AdminLayout/Content'
import DatePicker from 'components/common/DatePicker'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import { isBefore, isFuture } from 'date-fns'

interface Inputs {
  flashsale: CreateFlashSaleDto
  rangeTimeValue: [Date, Date]
}

const schema = yup.object().shape({
  rangeTimeValue: yup
    .array()
    .of(yup.string())
    .typeError('Chưa chọn thời gian')
    .required('Chưa chọn thời gian'),
  flashsale: yup.object().shape({
    name: yup.string().required('Chưa nhập tên'),
    maxQuantity: yup
      .number()
      .typeError('Chưa nhập số lượng')
      .min(1, 'Số lượng ít nhất phải là 1')
      .required('Chưa nhập số lượng'),
    maxProductNumber: yup
      .number()
      .typeError('Chưa nhập số lượng')
      .min(1, 'Số sản phẩm tham gia ít nhất phải là 1')
      .required('Chưa nhập số lượng'),
  }),
})

const Index: FC = () => {
  const router = useRouter()
  const { id } = router?.query
  const [title] = useState<string>('Thông tin flash sale')
  const currentTime = new Date()

  const {
    control,
    setValue,
    getValues,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>({ mode: 'all', resolver: yupResolver(schema) })

  useEffect(() => {
    if (Number(id) && Number(id) != 0) {
      FlashSaleService.getOneBase({
        id: Number(id),
      })
        .then((response) => {
          setValue('flashsale', response)
          setValue('rangeTimeValue', [
            new Date(response.startTime),
            new Date(response.endTime),
          ])
        })
        .catch((error) => {
          alertError(error)
        })
    }
  }, [id])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    data.flashsale.startTime = new Date(data.rangeTimeValue?.[0]).toISOString()
    data.flashsale.endTime = new Date(data.rangeTimeValue?.[1]).toISOString()

    modifyEntity(
      FlashSaleService,
      data.flashsale,
      'Cập nhật thông tin chương trình flash sale',
      () => router.push(`/flash-sale`)
    ).then()
  }

  return (
    <Content title={title} onBack={() => router.push('/flash-sale')}>
      <Form
        onFinish={handleSubmit(onSubmit)}
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 18 }}
      >
        <Card>
          <Form.Item
            label={'Tên chương trình'}
            required={true}
            validateStatus={errors?.flashsale?.name && 'error'}
            help={errors?.flashsale?.name?.message}
          >
            <Controller
              control={control}
              name="flashsale.name"
              render={({ field }) => (
                <Input {...field} placeholder="Nhập tên chương trình" />
              )}
            />
          </Form.Item>
          <Form.Item
            label={'Thời gian khuyến mãi'}
            required={true}
            validateStatus={errors?.rangeTimeValue && 'error'}
            help={errors?.rangeTimeValue?.[0]?.message}
          >
            <Controller
              name={'rangeTimeValue'}
              control={control}
              render={({ field }) => (
                <DatePicker.RangePicker
                  {...field}
                  showTime
                  allowClear={true}
                  inputReadOnly
                  format="YYYY-MM-DD HH:mm"
                  disabledDate={(date) => isBefore(date, currentTime)}
                  disabledTime={(date) => ({
                    disabledHours: () => {
                      if (isFuture(date)) return []
                      return _.times(24).filter(
                        (h) => currentTime.getHours() + 1 > h
                      )
                    },
                  })}
                />
              )}
            />
          </Form.Item>
          <Form.Item
            label={'Số sản phẩm tham gia'}
            required={true}
            validateStatus={errors?.flashsale?.maxProductNumber && 'error'}
            help={errors?.flashsale?.maxProductNumber?.message}
            extra={'Số lượng sản phẩm tối đa được tham gia của 1 NCC'}
          >
            <Controller
              control={control}
              name="flashsale.maxProductNumber"
              render={({ field }) => (
                <InputNumber {...field} min={1} placeholder="Nhập Số lượng" />
              )}
            />
          </Form.Item>
          <Form.Item
            label={'Số lượng khuyến mãi'}
            required={true}
            validateStatus={errors?.flashsale?.maxQuantity && 'error'}
            help={errors?.flashsale?.maxQuantity?.message}
            extra={'Số lượng khuyến mãi 1 NCC được phép bán ra'}
          >
            <Controller
              control={control}
              name="flashsale.maxQuantity"
              render={({ field }) => (
                <InputNumber {...field} min={1} placeholder="Nhập Số lượng" />
              )}
            />
          </Form.Item>
        </Card>
        <FooterBar
          right={
            <Button htmlType={'submit'} type={'primary'}>
              Lưu
            </Button>
          }
        />
      </Form>
    </Content>
  )
}

export default Index

import {
  Breadcrumb,
  Button,
  Grid,
  Image,
  Modal,
  Rate,
  Select,
  Space,
  Tabs,
  Typography,
} from 'antd'
import Link from 'components/common/Link'
import Head from 'next/head'
import { useRouter } from 'next/router'
import React, { FC, useEffect, useRef, useState } from 'react'
import { Controller, useForm, useWatch } from 'react-hook-form'
import { useSetRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'
import { useAddCartBuyNow, useAddCartProducts } from 'recoil/Hooks'
import {
  DetailProudct,
  EnumDetailProudctStatus,
  ProductCategoryEntity,
  ProductsService,
  ProductVariantEntity,
  VoucherEntity,
  VouchersService,
} from 'services'
import {
  alertError,
  compactNumber,
  flattenCategoryTree,
  formatCurrency,
  formatDate,
  getCdnFile,
  youtubeVIDParser,
} from 'utils'
import Slider from 'react-slick'
import { format, formatDistance } from 'date-fns'
import { vi } from 'date-fns/locale'
import {
  FreeShip,
  Genuine,
  MinusIcon,
  PlusIcon,
  Secure,
  Time,
} from 'constants/icons'
import Countdown from 'react-countdown'
import Warranty from 'components/guest/pages/san-pham/Warranty'
import Price from '../../components/guest/pages/san-pham/Price'
import Ratings from 'components/guest/pages/san-pham/Ratings'
import ModalVoucher from 'components/guest/pages/san-pham/ModalVoucher'
import ReactPlayer from 'react-player'
import { PlayCircleFilled } from '@ant-design/icons'

interface ProductMedia {
  video?: string
  image?: string
}

interface Inputs {
  merchantAddressId: number
}

const ProductDetail: FC = () => {
  const screens = Grid.useBreakpoint()
  const [thumbNav, setThumbNav] = useState<Slider>()
  const [imageNav, setImageNav] = useState<Slider>()
  const addProduct = useAddCartProducts()
  const addProductBuyNow = useAddCartBuyNow()
  const router = useRouter()
  const setIsLoading = useSetRecoilState(loadingState)
  const [product, setProduct] = useState<DetailProudct>()
  const [productMedia, setProductMedia] = useState<ProductMedia[]>()
  const [categories, setCategories] = useState<ProductCategoryEntity[]>([])
  const { slug, pub_id = 0 } = router.query
  const { control, getValues } = useForm<Inputs>({})
  const [variant1, setVariant1] = useState(null)
  const [variant2, setVariant2] = useState(null)
  const [viewMore, setViewMore] = useState(null)
  const [currentVoucherId, setCurrentVoucherId] = useState<number>(null)
  const [currentVoucher, setCurrentVoucher] = useState<VoucherEntity>(null)
  const [quantity, setQuantity] = useState<number>(1)
  const [selectedVariant, setSelectedVariant] = useState<ProductVariantEntity>()
  const [hotSaleExpire, setHotSaleExpire] = useState<number>()
  const descriptionRef = useRef(null)
  const [showSelectStock, setShowSelectStock] = useState(false)
  const [showSelectVariant, setShowSelectVariant] = useState(false)
  const watchMerchantAddressId = useWatch({
    control,
    name: 'merchantAddressId',
  })

  useEffect(() => {
    if (descriptionRef?.current?.clientHeight >= 1200) {
      setViewMore(true)
    }
  })

  useEffect(() => {
    if (slug) {
      const slugs = String(slug).split('_')
      if (Number(slugs[1])) {
        setIsLoading(true)
        Promise.all([
          ProductsService.getOneBase({
            id: Number(slugs[1]),
            join: [
              'productCategory',
              'merchant',
              'productCategory.parent',
              'productCategory.productCategoryAttribute',
              'variants',
              'stocks',
              'productAttributes',
              'stocks.merchantAddress',
              'stocks.merchantAddress.ward',
              'stocks.merchantAddress.ward.district',
              'stocks.merchantAddress.ward.district.province',
            ],
          }),
          // ProductsService.getManyBase({
          //   filter: `id||ne||${Number(slugs[1])}`,
          //   limit: 5,
          // }),
        ])
          .then(([productReponse]) => {
            // set product media
            const mediaList: ProductMedia[] = []
            if (productReponse?.videoProductUrl) {
              mediaList.push({
                video: productReponse?.videoProductUrl,
                image: productReponse?.videoProductThumbnailUrl,
              })
            }
            if (productReponse?.images) {
              for (let i = 0; i < productReponse?.images.length; i++) {
                mediaList.push({ image: productReponse?.images[i] })
              }
            }
            setProductMedia(mediaList)
            // must set productResponse after setting ProductMedia
            setProduct(productReponse)
            setCategories(
              flattenCategoryTree(productReponse.productCategory, []).reverse()
            )
            setHotSaleExpire(Date.now() + 1000 * 90 * 60)
            setIsLoading(false)
          })
          .catch((error) => {
            alertError(error)
            setIsLoading(false)
          })
      }
    }
  }, [slug])

  useEffect(() => {
    if (currentVoucherId)
      VouchersService.getOneBase({ id: currentVoucherId }).then((response) =>
        setCurrentVoucher(response)
      )
  }, [currentVoucherId])

  useEffect(() => {
    setShowSelectStock(false)
    setShowSelectVariant(false)
    if (product?.groupType?.length > 1) {
      setSelectedVariant(
        product?.variants.find(
          (variant) =>
            variant.groupTypeIndex === variant1 &&
            variant.valueTypeIndex === variant2
        )
      )
    } else if (product?.groupType?.length === 1)
      setSelectedVariant(
        product?.variants.find(
          (variant) =>
            variant.groupTypeIndex === 0 && variant.valueTypeIndex === variant1
        )
      )
  }, [variant1, variant2])

  return product ? (
    <>
      <Head>
        <title>{product.name}</title>
      </Head>
      <div className={'bg-white md:px-0 px-4'}>
        <div className={'product-detail flex max-w-6xl m-auto flex-col'}>
          <Breadcrumb
            separator=">"
            className={'md:text-base text-sm md:border-b pb-5 mt-5'}
            style={{
              color: '#80B4FB',
            }}
          >
            <Breadcrumb.Item>DANH MỤC</Breadcrumb.Item>
            {categories &&
              categories.map((category) => (
                <Breadcrumb.Item key={`product-item-${category.id}`}>
                  <Link
                    href={{
                      pathname: `/danh-muc/${category.slug}_${category.id}`,
                      query: {
                        ...(pub_id && { pub_id: Number(pub_id) }),
                      },
                    }}
                  >
                    <span
                      style={{
                        color: '#80B4FB',
                      }}
                    >
                      {category.name}
                    </span>
                  </Link>
                </Breadcrumb.Item>
              ))}
            <Breadcrumb.Item>
              <span
                style={{
                  color: '#223263',
                }}
              >
                {product.name}
              </span>
            </Breadcrumb.Item>
          </Breadcrumb>
          <div>
            <div className={'mt-4 flex md:flex-row flex-col'}>
              <div className={'md:w-7/12 w-full flex flex-col'}>
                <div
                  className={'w-full flex md:flex-row flex-col'}
                  style={{
                    maxHeight: 500,
                  }}
                >
                  {screens.lg && (
                    <div className={'md:w-32 w-full hidden md:block'}>
                      <Slider
                        arrows={false}
                        asNavFor={imageNav}
                        ref={(slider) => setThumbNav(slider)}
                        dots={false}
                        useTransform={false}
                        vertical={true}
                        verticalSwiping={true}
                        swipeToSlide={true}
                        infinite={false}
                        slidesToShow={4}
                        slidesToScroll={1}
                        className={'thumb'}
                      >
                        {productMedia?.map((media, index) => (
                          <div
                            key={`image-icon-${media.image}`}
                            style={{
                              width: 125,
                            }}
                            onClick={() => {
                              imageNav && imageNav.slickGoTo(index)
                            }}
                            className={'relative flex mb-3'}
                          >
                            <Image
                              src={getCdnFile(media.image)}
                              height={'100%'}
                              preview={false}
                              style={{
                                height: 100,
                                width: 125,
                                objectFit: 'fill',
                              }}
                            />
                            {media.video && (
                              <PlayCircleFilled
                                style={{
                                  fontSize: '22px',
                                  top: '50%',
                                  left: '50%',
                                  transform: 'translate(-50%,-50%)',
                                }}
                                className="absolute"
                              />
                            )}
                          </div>
                        ))}
                      </Slider>
                    </div>
                  )}
                  <div className={'overflow-hidden md:pl-3 w-full relative'}>
                    <Slider
                      asNavFor={thumbNav}
                      arrows={false}
                      ref={(slider) => setImageNav(slider)}
                      useTransform={false}
                      slidesToShow={1}
                    >
                      {productMedia?.map((media) => (
                        <div
                          key={`image-content-${media.image}`}
                          className={'px-1 flex justify-center'}
                        >
                          {media.video ? (
                            <ReactPlayer
                              width={'100%'}
                              url={getCdnFile(media.video)}
                              playing={false}
                              controls={true}
                              style={{
                                width: '100%',
                                objectFit: 'fill',
                              }}
                            />
                          ) : (
                            <Image
                              className={'w-full'}
                              src={getCdnFile(media.image)}
                              preview={false}
                              style={{
                                width: '100%',
                                objectFit: 'fill',
                              }}
                            />
                          )}
                        </div>
                      ))}
                    </Slider>
                    {product?.priceBeforeDiscount && (
                      <div
                        className={
                          'absolute right-1 top-0 bg-price py-2 px-5 rounded-bl-md text-lg text-white font-bold'
                        }
                      >
                        -
                        {100 -
                          Math.round(
                            (product?.price / product?.priceBeforeDiscount) *
                              100
                          )}
                        %
                      </div>
                    )}
                  </div>

                  {!screens?.lg && (
                    <div
                      className={
                        'md:w-32 w-full flex-none md:order-1 order-2 md:hidden block'
                      }
                    >
                      <Slider
                        arrows={false}
                        asNavFor={imageNav}
                        ref={(slider) => setThumbNav(slider)}
                        dots={false}
                        useTransform={false}
                        swipeToSlide={true}
                        infinite={false}
                        slidesToShow={4}
                        className={'thumb'}
                      >
                        {productMedia?.map((media, index) => (
                          <div
                            key={`image-${media.image}`}
                            style={{}}
                            onClick={() => {
                              imageNav && imageNav.slickGoTo(index)
                            }}
                            className={'flex mb-3 p-1'}
                          >
                            <Image
                              src={getCdnFile(media.image)}
                              height={'100%'}
                              preview={false}
                            />
                            {media.video && (
                              <PlayCircleFilled
                                style={{
                                  fontSize: '22px',
                                  top: '50%',
                                  left: '50%',
                                  transform: 'translate(-50%,-50%)',
                                }}
                                className="absolute"
                              />
                            )}
                          </div>
                        ))}
                      </Slider>
                    </div>
                  )}
                </div>
                <Warranty />
                <div className={'border-t mt-5'} />
                <div className={'mt-8 hidden md:block'}>
                  <div className={'text-2xl'}>Thông tin sản phẩm</div>
                  <div className={'border rounded-lg py-3 mt-2 pl-4 pr-8'}>
                    <div className={'flex flex-row justify-between'}>
                      <div className={'text-secondary'}>Nhà cung cấp</div>
                      <div className={'text-secondary'}>
                        {product?.merchant.name}
                      </div>
                    </div>
                    <div className={'flex flex-row justify-between mt-2'}>
                      <div className={'text-secondary'}>Danh mục</div>
                      <div className={'text-secondary'}>
                        <Link
                          href={{
                            pathname: `/danh-muc/${product?.productCategory?.slug}_${product?.productCategory?.id}`,
                            query: {
                              ...(pub_id && { pub_id: Number(pub_id) }),
                            },
                          }}
                        >
                          {product?.productCategory?.name}
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className={'md:w-5/12 w-full md:px-8 mt-4 md:mt-0'}>
                <Space
                  direction={'vertical'}
                  className={'w-full'}
                  size={[8, 8]}
                >
                  <Typography className="text-primary font-semibold uppercase text-lg md:text-2xl">
                    {product?.name}
                  </Typography>
                  <Price product={product} selectedVariant={selectedVariant} />
                  <div className={'border-t'} />
                  <Ratings />
                  <div className={'border-t'} />
                  {product?.groupType?.length > 0 ? (
                    <>
                      <div>
                        {product?.groupType?.map((group, groupIndex) => (
                          <div
                            key={`group-${groupIndex}`}
                            className={'flex py-2 items-center'}
                          >
                            <div
                              className={
                                'w-3/12 text-primary text-sm font-medium'
                              }
                            >
                              {group.name}
                            </div>
                            <div className={'flex flex-row'}>
                              {group.value.map((value, valueIndex) => (
                                <div
                                  key={`group-secondary-${valueIndex}`}
                                  className={`px-4 py-2 text-sm mr-2 rounded cursor-pointer ${
                                    (groupIndex === 0 &&
                                      variant1 === valueIndex) ||
                                    (groupIndex === 1 &&
                                      variant2 === valueIndex)
                                      ? 'text-price'
                                      : 'text-secondary'
                                  }`}
                                  style={{
                                    border: `1px solid ${
                                      (groupIndex === 0 &&
                                        variant1 === valueIndex) ||
                                      (groupIndex === 1 &&
                                        variant2 === valueIndex)
                                        ? '#F4447F'
                                        : '#E0E0E0'
                                    }`,
                                  }}
                                  onClick={() => {
                                    if (groupIndex === 0)
                                      setVariant1(valueIndex)
                                    else if (variant1 !== null)
                                      setVariant2(valueIndex)
                                  }}
                                >
                                  {value.field}
                                </div>
                              ))}
                            </div>
                          </div>
                        ))}
                      </div>
                      <div className={'border-t'} />
                      {product?.groupType?.length === 2 ? (
                        <>
                          {variant1 !== null &&
                            variant2 === null &&
                            showSelectVariant && (
                              <div className={'text-red-500 flex'}>
                                Vui lòng chọn phân loại hàng
                              </div>
                            )}
                        </>
                      ) : product?.groupType?.length == 1 &&
                        showSelectVariant ? (
                        <>
                          {variant1 === null && (
                            <div className={'text-red-500 flex'}>
                              Vui lòng chọn phân loại hàng
                            </div>
                          )}
                        </>
                      ) : null}
                      <div className={'flex gap-2 md:gap-6'}>
                        <div
                          className={'flex gap-2 md:gap-3 items-center w-30'}
                        >
                          <Button
                            disabled={
                              (product?.groupType?.length === 2
                                ? variant1 !== null && variant2 === null
                                : product?.groupType?.length == 1
                                ? variant1 === null
                                : null) ||
                              !(
                                EnumDetailProudctStatus.active ===
                                product.status
                              )
                            }
                            className={
                              'text-primary border-0 w-8 md:w-10 h-8 md:h-10 p-0 rounded-full bg-gray-200 justify-center items-center flex cursor-pointer hover:text-white hover:bg-price'
                            }
                            onClick={() =>
                              setQuantity((quantity) =>
                                quantity > 1 ? quantity - 1 : 1
                              )
                            }
                          >
                            <MinusIcon />
                          </Button>
                          <div
                            className={
                              'text-price font-medium text-base md:text-xl'
                            }
                          >
                            {quantity}
                          </div>
                          <Button
                            disabled={
                              (product?.groupType?.length === 2
                                ? variant1 !== null && variant2 === null
                                : product?.groupType?.length == 1
                                ? variant1 === null
                                : null) ||
                              !(
                                EnumDetailProudctStatus.active ===
                                product.status
                              )
                            }
                            className={
                              'text-primary border-0 w-8 md:w-10 h-8 md:h-10 p-0 rounded-full bg-gray-200 justify-center items-center flex cursor-pointer hover:text-white hover:bg-price'
                            }
                            onClick={() => {
                              const stock = selectedVariant?.stock.find(
                                (stock) =>
                                  stock.merchantAddressId ===
                                  getValues('merchantAddressId')
                              )
                              if (stock) {
                                if (stock.quantity < quantity + 1) {
                                  setQuantity(stock.quantity)
                                } else setQuantity((quantity) => quantity + 1)
                              } else {
                                setQuantity((quantity) => quantity + 1)
                              }
                            }}
                          >
                            <PlusIcon />
                          </Button>
                        </div>
                        <div className={'grow'}>
                          <Controller
                            control={control}
                            name={'merchantAddressId'}
                            render={({ field }) => (
                              <Select
                                {...field}
                                disabled={
                                  (product?.groupType?.length === 2
                                    ? variant1 !== null && variant2 === null
                                    : product?.groupType?.length == 1
                                    ? variant1 === null
                                    : null) ||
                                  !(
                                    EnumDetailProudctStatus.active ===
                                    product.status
                                  )
                                }
                                placeholder={'Chọn kho hàng'}
                                className={'select-merchant-address'}
                                onChange={(value) => {
                                  const stock = selectedVariant?.stock.find(
                                    (stock) => stock.merchantAddressId === value
                                  )
                                  if (stock) {
                                    if (stock.quantity < quantity) {
                                      setQuantity(stock.quantity)
                                    }
                                  }
                                  field.onChange(value)
                                }}
                              >
                                {selectedVariant &&
                                  selectedVariant?.stock.map((stock) => {
                                    return (
                                      <React.Fragment
                                        key={`merchant-address-id-${stock.merchantAddressId}`}
                                      >
                                        {stock?.quantity &&
                                        stock?.merchantAddress.status ===
                                          'active' ? (
                                          <Select.Option
                                            key={`merchant-address-id-${stock.merchantAddressId}`}
                                            value={stock?.merchantAddressId}
                                            className={
                                              'bg-white text-primary text-sm border-b mx-5 px-0 border-gray-300 py-3 hover:bg-transperant hover:text-price'
                                            }
                                          >
                                            <div
                                              className={
                                                'flex items-center justify-between gap-1 md:gap-2 text-[13px] md:text-base h-full'
                                              }
                                            >
                                              <div className={'truncate'}>
                                                {stock?.merchantAddress?.name}
                                              </div>
                                              <div className="hidden md:block">
                                                Còn hàng: {stock?.quantity}
                                              </div>
                                              <div className="md:hidden">
                                                Còn: {stock?.quantity}
                                              </div>
                                            </div>
                                          </Select.Option>
                                        ) : null}
                                      </React.Fragment>
                                    )
                                  })}
                              </Select>
                            )}
                          />
                          {selectedVariant &&
                            !watchMerchantAddressId &&
                            showSelectStock && (
                              <div className={'text-red-500 flex'}>
                                Vui lòng chọn kho hàng
                              </div>
                            )}
                        </div>
                      </div>
                    </>
                  ) : (
                    <div
                      className={'flex gap-6'}
                      style={{
                        opacity:
                          EnumDetailProudctStatus.active === product.status
                            ? 1
                            : 0.4,
                      }}
                    >
                      <div className={'flex gap-2 md:gap-3 items-center w-30'}>
                        <Button
                          className={
                            'text-primary border-0 w-8 md:w-10 h-8 md:h-10 p-0 rounded-full bg-gray-200 justify-center items-center flex cursor-pointer hover:text-white hover:bg-price'
                          }
                          disabled={
                            !(EnumDetailProudctStatus.active === product.status)
                          }
                          onClick={() =>
                            setQuantity((quantity) =>
                              quantity > 1 ? quantity - 1 : 1
                            )
                          }
                        >
                          <MinusIcon />
                        </Button>
                        <div
                          className={
                            'text-price font-medium text-base md:text-xl'
                          }
                        >
                          {quantity}
                        </div>
                        <Button
                          className={
                            'text-primary border-0 w-8 md:w-10 h-8 md:h-10 p-0 rounded-full bg-gray-200 justify-center items-center flex cursor-pointer hover:text-white hover:bg-price'
                          }
                          disabled={
                            !(EnumDetailProudctStatus.active === product.status)
                          }
                          onClick={() => {
                            const stock = product?.stocks.find(
                              (stock) =>
                                stock.merchantAddressId ===
                                getValues('merchantAddressId')
                            )
                            if (stock) {
                              if (stock.quantity < quantity + 1) {
                                setQuantity(stock.quantity)
                              } else setQuantity((quantity) => quantity + 1)
                            } else {
                              setQuantity((quantity) => quantity + 1)
                            }
                          }}
                        >
                          <PlusIcon />
                        </Button>
                      </div>
                      <div className={'grow'}>
                        <Controller
                          control={control}
                          name={'merchantAddressId'}
                          render={({ field }) => (
                            <Select
                              {...field}
                              disabled={
                                !(
                                  EnumDetailProudctStatus.active ===
                                  product.status
                                )
                              }
                              placeholder={'Chọn kho hàng'}
                              className={'select-merchant-address'}
                              onChange={(value) => {
                                const stock = product?.stocks.find(
                                  (stock) => stock.merchantAddressId === value
                                )
                                if (stock) {
                                  if (stock.quantity < quantity) {
                                    setQuantity(stock.quantity)
                                  }
                                  setShowSelectStock(false)
                                }
                                field.onChange(value)
                              }}
                            >
                              {product?.stocks &&
                                product?.stocks?.map((stock) => (
                                  <>
                                    {stock?.quantity ? (
                                      <Select.Option
                                        value={stock?.merchantAddressId}
                                        className={
                                          'bg-white text-primary text-sm border-b mx-5 px-0 border-gray-300 py-3 hover:bg-transperant hover:text-price'
                                        }
                                      >
                                        <div
                                          className={
                                            'flex items-center justify-between gap-1 md:gap-2 text-[13px] md:text-base h-full'
                                          }
                                        >
                                          <div className={'truncate'}>
                                            {stock?.merchantAddress?.name}
                                          </div>
                                          <div className="hidden md:block">
                                            Còn hàng: {stock?.quantity}
                                          </div>
                                          <div className="md:hidden">
                                            Còn: {stock?.quantity}
                                          </div>
                                        </div>
                                      </Select.Option>
                                    ) : null}
                                  </>
                                ))}
                            </Select>
                          )}
                        />
                        {showSelectStock && (
                          <div className={'text-red-500 flex'}>
                            Vui lòng chọn kho hàng
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                  {product.status === EnumDetailProudctStatus.active && (
                    <div className={'flash-sale relative pt-5 mb-2'}>
                      <div
                        className={
                          'flex border-price border rounded-lg items-center py-2 md:py-3 justify-center relative'
                        }
                      >
                        <div
                          className={
                            'text-primary absolute left-4 md:text-2xl text-lg font-semibold -md:top-6 -top-4 uppercase bg-white px-2'
                          }
                        >
                          Hotsale
                        </div>
                        <div
                          className={
                            'text-price md:mr-10 mr-3 md:text-xl text-base'
                          }
                        >
                          Kết thúc trong
                        </div>
                        {hotSaleExpire && (
                          <Countdown
                            date={hotSaleExpire}
                            renderer={({
                              hours,
                              minutes,
                              seconds,
                              completed,
                            }) => {
                              if (!completed) {
                                return (
                                  <div
                                    className={
                                      'text-price text-2xl font-bold flex flex-row items-center'
                                    }
                                  >
                                    <div
                                      className={
                                        'bg-price flex items-center justify-center rounded-[4px] md:rounded-full text-white text-lg md:text-2xl font-medium mr-2 w-8 md:w-11 h-11'
                                      }
                                    >
                                      {hours}
                                    </div>
                                    :
                                    <div
                                      className={
                                        'bg-price flex items-center justify-center rounded-[4px] md:rounded-full text-white text-lg md:text-2xl font-medium mx-2 w-8 md:w-11 h-11'
                                      }
                                    >
                                      {minutes}
                                    </div>
                                    :
                                    <div
                                      className={
                                        'bg-price flex items-center justify-center rounded-[4px] md:rounded-full text-white text-lg md:text-2xl font-medium ml-2 w-8 md:w-11 h-11'
                                      }
                                    >
                                      {seconds}
                                    </div>
                                  </div>
                                )
                              }
                            }}
                          />
                        )}
                      </div>
                    </div>
                  )}
                  {product.status === EnumDetailProudctStatus.active &&
                    product?.vouchers.length > 0 && (
                      <>
                        <div className={'uppercase text-primary font-bold'}>
                          Ưu đãi cho khách hàng
                        </div>
                        <div
                          className={
                            'border border-gray-200 p-2 overflow-y-scroll'
                          }
                          style={{
                            maxHeight: 168,
                          }}
                        >
                          {product?.vouchers.map((voucher: VoucherEntity) => (
                            <div
                              key={voucher.id}
                              className={'py-1 flex flex-row cursor-pointer'}
                              onClick={() => setCurrentVoucherId(voucher.id)}
                            >
                              <div
                                className={'flex-none'}
                                style={{
                                  width: 4,
                                  height: 72,
                                  background:
                                    'url("/images/voucher-bg-right.svg") no-repeat',
                                }}
                              />
                              <div
                                className={'grow flex flex-row p-2'}
                                style={{
                                  background:
                                    'linear-gradient(90deg, #F54C84 -1.6%, rgba(251, 20, 96, 0.69) 118.95%)',
                                }}
                              >
                                <div className="flex-none">
                                  <img
                                    src="/images/voucher.svg"
                                    alt="Voucher"
                                  />
                                </div>
                                <div className={'grow text-white ml-2'}>
                                  <div className={'font-bold text-sm'}>
                                    {voucher.name}
                                  </div>
                                  <div
                                    className={'flex flex-row justify-between'}
                                  >
                                    <div className={'text-xs'}>
                                      <div>
                                        {format(
                                          new Date(voucher.endDate),
                                          'd/M/y',
                                          {
                                            locale: vi,
                                          }
                                        )}
                                      </div>
                                      <div>
                                        {formatDistance(
                                          new Date(voucher.endDate),
                                          new Date(),
                                          { locale: vi }
                                        )}
                                      </div>
                                    </div>
                                    <div className={'flex flex-row items-end'}>
                                      <div className="items-center flex">
                                        <span className={'mr-1 text-xs'}>
                                          Xem chi tiết
                                        </span>
                                        <img
                                          src={'/images/arrow/right.svg'}
                                          className={'w-1'}
                                          alt={'Right Arrow'}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div
                                className={'flex-none'}
                                style={{
                                  width: 4,
                                  height: 72,
                                  background:
                                    'url("/images/voucher-bg-left.svg") no-repeat',
                                }}
                              />
                            </div>
                          ))}
                        </div>
                      </>
                    )}
                  {[
                    EnumDetailProudctStatus.inactive,
                    EnumDetailProudctStatus.deleted,
                  ].includes(product.status) && (
                    <div className={'w-full'}>
                      <p
                        className={'my-3 font-semibold'}
                        style={{ color: '#D50000', fontSize: 16 }}
                      >
                        {product.status === EnumDetailProudctStatus.deleted
                          ? 'SẢN PHẨM NGỪNG KINH DOANH'
                          : 'SẢN PHẨM TẠM HẾT HÀNG'}
                      </p>
                    </div>
                  )}
                  <div className={'mt-2 w-full gap-1 flex grid grid-cols-3'}>
                    <div className={'md:grid-cols-3'}>
                      <a
                        href={'https://m.me/158714087586067'}
                        target={'_blank'}
                        rel="noreferrer"
                      >
                        <Button
                          className={
                            'w-full flex items-center text-xs md:text-sm justify-center rounded-md text-white uppercase font-bold py-6 action-button'
                          }
                          size={'large'}
                          style={{
                            backgroundColor: '#80B4FB',
                            height: 56,
                          }}
                        >
                          <img
                            src={'/images/chat.svg'}
                            className={'mr-2'}
                            alt={'Chat'}
                          />{' '}
                          Chat
                        </Button>
                      </a>
                    </div>
                    {product.status === EnumDetailProudctStatus.active && (
                      <>
                        <div className={'md:grid-cols-3'}>
                          <Button
                            className={
                              'w-full flex items-center border-0 text-sm justify-center rounded-md text-white uppercase font-bold py-7 action-button'
                            }
                            style={{
                              backgroundColor: '#5C84FF',
                            }}
                            type={'primary'}
                            size={'large'}
                            onClick={() => {
                              if (!getValues('merchantAddressId')) {
                                setShowSelectVariant(true)
                                setShowSelectStock(true)
                              } else
                                addProduct(
                                  [
                                    {
                                      id: product.id,
                                      quantity: quantity,
                                      merchantAddressId:
                                        getValues('merchantAddressId'),
                                      productVariantId:
                                        selectedVariant?.id || null,
                                    },
                                  ],
                                  true,
                                  Number(pub_id)
                                )
                            }}
                          >
                            <img
                              src={'/images/add-to-cart.svg'}
                              className={'mr-2'}
                              alt={'Add to Cart'}
                            />
                            Giỏ hàng
                          </Button>
                        </div>
                        <div className={'md:grid-cols-3'}>
                          <Button
                            className={
                              'w-full flex items-center border-0 text-sm justify-center rounded-md text-white uppercase font-bold py-7 action-button'
                            }
                            size={'large'}
                            onClick={() => {
                              if (!getValues('merchantAddressId')) {
                                setShowSelectVariant(true)
                                setShowSelectStock(true)
                              } else {
                                addProductBuyNow(
                                  {
                                    id: product.id,
                                    quantity: 1,
                                    merchantAddressId:
                                      getValues('merchantAddressId'),
                                    productVariantId:
                                      selectedVariant?.id || null,
                                  },
                                  Number(pub_id)
                                )
                                router
                                  .push({
                                    pathname: `/checkout/buy-now`,
                                    query: {
                                      ...(pub_id && {
                                        pub_id: Number(pub_id),
                                      }),
                                    },
                                  })
                                  .then()
                              }
                            }}
                            style={{
                              backgroundColor: '#F4447F',
                            }}
                          >
                            <div className={'buyNow flex'}>
                              <img
                                src={'/images/buy-now.svg'}
                                className={'mr-2'}
                                alt={'Buy now'}
                              />
                              Mua ngay
                            </div>
                          </Button>
                        </div>
                      </>
                    )}
                  </div>
                </Space>
              </div>
              <div className={'mt-8 block md:hidden'}>
                <div className={'text-2xl'}>Thông tin sản phẩm</div>
                <div className={'border rounded-lg py-3 mt-2 pl-4 pr-8'}>
                  <div className={'flex flex-row justify-between'}>
                    <div className={'text-secondary'}>Nhà cung cấp</div>
                    <div className={'text-secondary'}>
                      {product?.merchant.name}
                    </div>
                  </div>
                  <div className={'flex flex-row justify-between mt-2'}>
                    <div className={'text-secondary'}>Danh mục</div>
                    <div className={'text-secondary'}>
                      <Link href={`/danh-muc/${product?.productCategory?.id}`}>
                        {product?.productCategory?.name}
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className={'mt-6 text-lg'}>
              <Tabs defaultActiveKey="description">
                <Tabs.TabPane
                  tab="Mô tả sản phẩm"
                  key="description"
                  className={'relative'}
                >
                  <div
                    ref={descriptionRef}
                    className={`transition-max-height duration-500 ease-in-out relative overflow-hidden`}
                    style={{
                      maxHeight: viewMore === true ? 1000 : '100%',
                    }}
                  >
                    <div
                      dangerouslySetInnerHTML={{
                        __html: product?.description.replaceAll('demo.', ''),
                      }}
                      className={'leading-8 text-sm md:text-lg text-secondary'}
                    />
                  </div>
                  {viewMore !== null && (
                    <a
                      className="mt-5 flex items-center"
                      href={'#'}
                      onClick={(e) => {
                        e.preventDefault()
                        setViewMore(!viewMore)
                      }}
                    >
                      {viewMore ? 'Xem thêm' : 'Thu nhỏ'}
                      <svg
                        className={`ml-2 ${!viewMore && 'rotate-180'}`}
                        width="16"
                        height="9"
                        viewBox="0 0 16 9"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M7.57422 8.15039L15.1494 0.575195H-0.000976491L7.57422 8.15039Z"
                          fill="#80B4FB"
                        />
                      </svg>
                    </a>
                  )}
                </Tabs.TabPane>
                {product?.productAttributes?.[0]?.productCategoryAttribute && (
                  <Tabs.TabPane tab="Thông số" key="attributes">
                    <div className={'mb-4'}>
                      {product?.productAttributes.map((productAttribute) => {
                        return (
                          productAttribute?.productCategoryAttribute && (
                            <div
                              key={`attribute-${productAttribute.id}`}
                              className={
                                'flex justify-between text-secondary mt-2'
                              }
                            >
                              <div className={'w-1/2 font-semibold'}>
                                •{' '}
                                {
                                  productAttribute?.productCategoryAttribute
                                    .name
                                }
                              </div>
                              <div className={'w-1/2'}>
                                {productAttribute?.productCategoryAttributeValues.map(
                                  (productCategoryAttributeValue) =>
                                    productCategoryAttributeValue.name
                                )}
                              </div>
                            </div>
                          )
                        )
                      })}
                    </div>
                  </Tabs.TabPane>
                )}
                <Tabs.TabPane tab="Video" key="video">
                  {product?.videoUrl && (
                    <div
                      className={
                        'flex justify-center w-full relative youtube mb-5'
                      }
                    >
                      <iframe
                        className="video"
                        src={`https://www.youtube.com/embed/${youtubeVIDParser(
                          product?.videoUrl
                        )}`}
                        title={product?.name}
                        frameBorder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen
                      />
                    </div>
                  )}
                </Tabs.TabPane>
              </Tabs>
            </div>
          </div>
        </div>
        <ModalVoucher
          currentVoucher={currentVoucher}
          setCurrentVoucher={setCurrentVoucher}
          setCurrentVoucherId={setCurrentVoucherId}
        />
      </div>
    </>
  ) : (
    <></>
  )
}
export default ProductDetail

import React, { useState } from 'react'
import StatusReport from 'components/admin/Dashboard/StatusReport'
import MainReport from 'components/admin/Dashboard/MainReport'
import SelectMerchant from 'components/merchant/select-merchant'
import { useRecoilValue } from 'recoil'
import { authState } from 'recoil/Atoms'

const Dashboard = () => {
  const [merchantId, setMerchantId] = useState<null | number>(null)
  const user = useRecoilValue(authState)
  const isAdminUshare = user.type === 'admin'
  const isAccountantUshare = user.type === 'accountant'

  return (
    <div className='p-4'>
      <div className='mb-10'>
        {(isAdminUshare || isAccountantUshare) &&
          <SelectMerchant
            onChangeMerchantId={setMerchantId}
          />
        }
      </div>
      <div className='mb-10'>
        <StatusReport
          merchantId={merchantId}
        />
      </div>
      <div className='mb-10'>
        <MainReport
          merchantId={merchantId}
        />
      </div>
    </div>
  )
}
export default Dashboard

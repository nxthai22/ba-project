import {
  NOTIFICATION_RECEIVED_TYPE_ADMIN,
  NOTIFICATION_RECEIVED_TYPE_MERCHANT,
  NOTIFICATION_TYPE,
  NOTIFICATION_TYPE_ADMIN
} from '@/constants'
import {yupResolver} from '@hookform/resolvers/yup'
import {Alert, Button, Card, Col, DatePicker, Form, Input, Modal, Radio, Row, Select, Space, Tooltip} from 'antd'
import FormItem from 'components/common/FormItem'
import RichTextEditor from 'components/common/RichTextEditor'
import UploadMedia from 'components/common/UploadMedia'
import Content from 'components/layout/AdminLayout/Content'
import {isBefore, isFuture} from 'date-fns'
import _ from 'lodash'
import moment from 'moment'
import {useRouter} from 'next/router'
import React, {FC, useEffect, useState} from 'react'
import {Controller, FormProvider, SubmitHandler, useForm, useWatch} from 'react-hook-form'
import {useRecoilValue, useSetRecoilState} from 'recoil'
import {authState, loadingState} from 'recoil/Atoms'
import {
  EnumApproveNotificationConfigDtoAction,
  EnumNotificationConfigEntityReceivedType,
  EnumNotificationConfigEntityStatus,
  EnumNotificationConfigEntityType, EnumRoleEntityType,
  NotificationConfigEntity,
  NotificationConfigsService,
} from 'services'
import {alertError, alertSuccess, modifyEntity} from 'utils'
import * as yup from 'yup'
import {SchemaOf} from 'yup'
import TextArea from "antd/lib/input/TextArea";
import {QuestionCircleOutlined} from '@ant-design/icons'


interface Inputs {
  title: string;
  body?: string;
  content?: string;

  /** ID nhà cung cấp */
  merchantId?: number;

  /** Thời gian gửi */
  scheduledTime: Date;

  /** File đính kèm */
  attachedFile?: string;

  /** Image */
  image?: string;

  /** Loại */
  type: EnumNotificationConfigEntityType;

  /** Loại người nhận */
  receivedType: EnumNotificationConfigEntityReceivedType;

  /** Trạng thái */
  status?: EnumNotificationConfigEntityStatus;

  rejected_reason?: string
}

const schema: SchemaOf<Inputs> = yup.object().shape({
  title: yup.string().required('Chưa nhập tiêu đề'),
  body: yup.string().required('Chưa nhập nội dung ngắn'),
  content: yup.string().required('Chưa nhập nội dung'),
  scheduledTime: yup.date(),
  type: yup.mixed().required('Chưa nhập loại thông báo'),
  image: yup.string().nullable(),
  attachedFile: yup.string().nullable(),
  rejected_reason: yup.string().nullable(),
  status: yup.mixed(),
  receivedType: yup.mixed(),
  merchantId: yup.number()
})

const Detail: FC = () => {
  const router = useRouter()
  const {id} = router.query
  const [title, setTitle] = useState('Tạo thông báo')
  const [notification, setNotification] = useState<NotificationConfigEntity>()
  const [imageFiles, setImageFiles] = useState<string[]>([])
  const [attachedFiles, setAttachedFiles] = useState<string[]>([])
  const setIsLoading = useSetRecoilState(loadingState)
  const [isShowModalReject, setIsShowModalReject] = useState(false)
  const user = useRecoilValue(authState)
  const defaultReceivedType = user?.type === EnumRoleEntityType.admin
    ? Object.keys(NOTIFICATION_RECEIVED_TYPE_ADMIN)[0]
    : Object.keys(NOTIFICATION_RECEIVED_TYPE_MERCHANT)[0]
  const [disabledNotify, setDisabledNotify] = useState<boolean>(false)

  const formMethods = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
    defaultValues: {
      receivedType: defaultReceivedType as EnumNotificationConfigEntityReceivedType
    },
  })
  const {
    control,
    setValue,
    handleSubmit,
    getValues,
    formState: {errors},
  } = formMethods

  const currentTime = new Date()

  const watchRejectReason = useWatch({
    control,
    name: 'rejected_reason',
    defaultValue: '',
  })

  useEffect(() => {
    if (Number(id) && Number(id) != 0) {
      setTitle('Chi tiết thông báo')
      setIsLoading(true)
      NotificationConfigsService.getOneBase({
        id: Number(id),
      })
        .then((response) => {
          setNotification(response)
          setValue('title', response.title)
          setValue('body', response.body)
          setValue('content', response.content)
          if (response.scheduledTime) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            setValue('scheduledTime', moment(response.scheduledTime))
          }
          setValue('image', response.image)
          setImageFiles(response.image ? [response.image] : [])
          setValue('attachedFile', response.attachedFile)
          setAttachedFiles(response.attachedFile ? [response.attachedFile] : [])
          setValue('type', response.type)
          setValue('receivedType', response.receivedType)
          setValue('status', response.status)
          setValue('rejected_reason', response.rejecedReason)
          setValue('merchantId', response.merchantId)
          if (response.status !== EnumNotificationConfigEntityStatus.new ||
            (response.merchantId && user?.type === EnumRoleEntityType.admin)){
            setDisabledNotify(true)
          }
          setIsLoading(false)
        })
        .catch((error) => {
          setIsLoading(false)
          alertError(error)
        })
    }
  }, [id])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    setIsLoading(true)
    if (imageFiles && imageFiles.length > 0) {
      data.image = imageFiles[0]
    }
    if (attachedFiles && attachedFiles.length > 0) {
      data.attachedFile = attachedFiles[0]
    }
    let title: string | { message: string } = 'Tạo thông báo'
    if (user?.type === EnumRoleEntityType.merchant) {
      title = {
        message: 'Thêm mới thành công, chúng tôi sẽ xác nhận trong thời gian sớm nhất'
      }
    }
    modifyEntity(
      NotificationConfigsService,
      data,
      String(id) === 'create' ? title : 'Cập nhật thông báo',
      (response) => {
        setIsLoading(false)
        if (response) return router.push(`/notification`)
        else return router.push(`/notification/${id}`)
      }, () => {
        setIsLoading(false)
      }
    ).then()
  }


  const updateStatus = (action: EnumApproveNotificationConfigDtoAction) => {
    setIsLoading(true)
    if (notification?.id) {
      NotificationConfigsService.notificationConfigControllerApproveNotification({
        id: Number(notification?.id),
        body: {
          rejectReason: getValues('rejected_reason'),
          action: action
        },
      })
        .then(() => {
          alertSuccess('Cập nhật thành công')
          setIsShowModalReject(false)
          router.reload()
          setIsLoading(false)
        })
        .catch((error) => {
          setIsLoading(false)
          alertError(error)
        })
    }
  }

  return (
    <>
      <Content title={title} onBack={() => router.push('/notification')}>
        <FormProvider {...formMethods}>
          <Form
            labelCol={{
              sm: {span: 12},
              md: {span: 4},
            }}
            wrapperCol={{
              sm: {span: 12},
              md: {span: 20},
            }}
            colon={false}
          >
            <Card title={<div className={'text-xl'}>Thông tin thông báo</div>}>
              <div className={'p-4'}>
                {
                  String(id) !== 'create' && notification?.status === EnumNotificationConfigEntityStatus.rejected &&
                    <Form.Item
                        label={<></>}
                        required={false}
                    >
                        <Alert
                            message="Lý do từ chối"
                            description={getValues('rejected_reason')}
                            type="warning"
                            showIcon
                            closable={false}
                        />
                    </Form.Item>
                }
                <Form.Item
                  label={'Tiêu đề'}
                  required={true}
                  validateStatus={errors?.title && 'error'}
                  help={errors?.title?.message}
                >
                  <Controller
                    control={control}
                    name="title"
                    render={({field}) => (
                      <Input
                        {...field}
                        placeholder={'Nhập tiêu đề, tối đa 50 ký tự'}
                        minLength={1}
                        maxLength={50}
                        disabled={disabledNotify}
                      />
                    )}
                  />
                </Form.Item>
                <Form.Item
                  label={'Nội dung ngắn'}
                  required={true}
                  validateStatus={errors?.body && 'error'}
                  help={errors?.body?.message}
                >
                  <Controller
                    control={control}
                    name="body"
                    render={({field}) => (
                      <Input
                        {...field}
                        placeholder={'Nhập tiêu nội dung ngắn, tối đa 100 ký tự'}
                        minLength={1}
                        maxLength={100}
                        disabled={disabledNotify}
                      />
                    )}
                  />
                </Form.Item>
                <Form.Item label={'Đối tượng gửi'}>
                  <Controller
                    control={control}
                    name="receivedType"
                    render={({field}) => {
                      if ((String(id) === 'create' && user?.type === EnumRoleEntityType.admin)
                        || (String(id) !== 'create' && !getValues('merchantId')) ) {
                        return (
                          <Radio.Group {...field} disabled={disabledNotify}>
                            {Object.keys(NOTIFICATION_RECEIVED_TYPE_ADMIN).map(type => (
                              <Radio
                                value={type}
                                key={`control-noti-received-type-${type}`}
                              >
                                {NOTIFICATION_RECEIVED_TYPE_ADMIN[type]}
                              </Radio>
                            ))}
                          </Radio.Group>
                        )
                      } else {
                        return (
                          <Radio.Group {...field} disabled={disabledNotify}>
                            {Object.keys(NOTIFICATION_RECEIVED_TYPE_MERCHANT).map(type => (
                              <Radio
                                value={type}
                                key={`control-noti-received-type-${type}`}
                              >
                                {NOTIFICATION_RECEIVED_TYPE_MERCHANT[type]}
                              </Radio>
                            ))}
                          </Radio.Group>
                        )
                      }
                    }
                    }
                  />
                </Form.Item>
                <Form.Item
                  label={'Nội dung'}
                  required={true}
                  validateStatus={errors?.content && 'error'}
                  help={errors?.content?.message}
                >
                  <Controller
                    control={control}
                    name="content"
                    render={({field}) => <RichTextEditor {...field} readOnly={disabledNotify}/>}
                  />
                </Form.Item>
                <Row gutter={[8, 8]}>
                  <Col xs={12} sm={12} md={12} xl={12} lg={12}>
                    <FormItem
                      label={'Loại thông báo'}
                      required={true}
                      validateStatus={errors?.type && 'error'}
                      help={errors?.type?.message}
                    >
                      <Controller
                        control={control}
                        name="type"
                        render={({field}) => (
                          <Select
                            {...field}
                            placeholder={'Chọn loại thông báo'}
                            allowClear
                            disabled={disabledNotify}
                          >
                            {user?.type === EnumRoleEntityType.admin ?
                              Object.keys(NOTIFICATION_TYPE_ADMIN).map((type) => (
                                <Select.Option
                                  value={type}
                                  key={`detail-noti-type-${type}`}
                                >
                                  {NOTIFICATION_TYPE_ADMIN[type]}
                                </Select.Option>
                              )) : Object.keys(NOTIFICATION_TYPE).map((type) => (
                                <Select.Option
                                  value={type}
                                  key={`detail-noti-type-${type}`}
                                >
                                  {NOTIFICATION_TYPE[type]}
                                </Select.Option>
                              ))}
                          </Select>
                        )}
                      />
                    </FormItem>
                  </Col>
                  <Col xs={12} sm={12} md={12} xl={12} lg={12}>
                    <FormItem label={
                      <div>
                        <span>Thời gian gửi </span>
                        {user?.type === EnumRoleEntityType.merchant &&
                            <Tooltip
                                placement="top"
                                title={'Thời gian gửi nên sau giờ hiện tại 2h để admin Ushare duyệt'}
                            >
                                <QuestionCircleOutlined/>
                            </Tooltip>
                        }
                      </div>}>
                      <Controller
                        control={control}
                        name="scheduledTime"
                        render={({field}) => (
                          <DatePicker
                            {...field}
                            format="YYYY-MM-DD HH:mm:ss"
                            disabledDate={(date) => isBefore(date ? date.toDate() : null, currentTime)}
                            disabledTime={(date) => ({
                              disabledHours: () => {
                                if (isFuture(date ? date.toDate() : null)) return []
                                return _.times(24).filter(
                                  (h) => currentTime.getHours() + 1 > h
                                )
                              },
                            })}
                            showTime={{defaultValue: moment('00:00:00', 'HH:mm:ss')}}
                            disabled={disabledNotify}
                          />)}
                      />
                    </FormItem>
                  </Col>
                </Row>
                <Row gutter={[8, 8]}>
                  <Col xs={12} sm={12} md={12} xl={12} lg={12}>
                    <FormItem label={
                      <div>
                        <span>Ảnh sản phẩm </span>
                        <Tooltip
                          placement="top"
                          title={<>
                            <div>Tỉ lệ tiêu chuẩn</div>
                            <div>375 x 208pixel, độ phân giải 300ppi</div>
                          </>}
                        >
                          <QuestionCircleOutlined/>
                        </Tooltip>
                      </div>}
                    >
                      <UploadMedia
                        multiple={false}
                        files={imageFiles}
                        onImageChange={setImageFiles}
                        disabled={disabledNotify}
                      />
                    </FormItem>
                  </Col>
                  <Col xs={12} sm={12} md={12} xl={12} lg={12} className="hidden">
                    <FormItem label={'File đính kèm'}>
                      <UploadMedia
                        multiple={false}
                        files={attachedFiles}
                        onImageChange={setAttachedFiles}
                        disabled={disabledNotify}
                      />
                    </FormItem>
                  </Col>
                </Row>
              </div>
            </Card>

            <div
              className={'ant-pro-footer-bar-product flex items-center justify-between bg-white fixed bottom-0 left-0 z-[99] w-full px-7'}>
              <div/>
              <div>
                {user?.type === EnumRoleEntityType.admin && String(id) !== 'create'
                  && notification?.status === EnumNotificationConfigEntityStatus.new &&
                    <Space>
                        <Button type={'primary'}
                                onClick={() => updateStatus(EnumApproveNotificationConfigDtoAction.approved)}>
                            Duyệt
                        </Button>
                        <Button type={'default'} onClick={() => setIsShowModalReject(true)}>
                            Từ chối
                        </Button>
                    </Space>
                }
                {user?.type === EnumRoleEntityType.admin && String(id) === 'create' &&
                    <Space>
                        <Button type={'primary'} onClick={handleSubmit(onSubmit)}>
                            Lưu tạm
                        </Button>
                        <Button
                            type={'primary'}
                            onClick={() => {
                              setValue('status', EnumNotificationConfigEntityStatus.approved)
                              handleSubmit(onSubmit)()
                            }}
                        >
                            Duyệt
                        </Button>
                    </Space>
                }
                {user?.type !== EnumRoleEntityType.admin && (String(id) === 'create' ||
                    (String(id) !== 'create' && notification?.status === EnumNotificationConfigEntityStatus.new)) &&
                    <Button type={'primary'} onClick={handleSubmit(onSubmit)}>
                        Lưu
                    </Button>
                }

              </div>
            </div>
          </Form>
        </FormProvider>
      </Content>
      <Modal
        title={`Nhập lý do`}
        visible={isShowModalReject}
        onCancel={() => {
          setIsShowModalReject(false)
        }}
        width={600}
        closable={false}
        maskClosable={false}
        destroyOnClose={true}
        footer={[
          <Button
            key="back"
            onClick={() => {
              setIsShowModalReject(false)
            }}
          >
            Đóng
          </Button>,
          <>
            {watchRejectReason?.length > 0 && (
              <Button
                key="submit"
                type="primary"
                onClick={() => {
                  updateStatus(EnumApproveNotificationConfigDtoAction.rejected)
                }}
              >
                Lưu
              </Button>
            )}
          </>,
        ]}
      >
        <Row>
          <Col xs={24}>
            <Controller
              control={control}
              name={'rejected_reason'}
              render={({field}) => (
                <TextArea
                  {...field}
                  rows={4}
                  placeholder={`Nhập lý do`}
                />
              )}
            />
          </Col>
        </Row>
      </Modal>
    </>
  )
}
export default Detail

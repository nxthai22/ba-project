import {NOTIFICATION_RECEIVED_TYPE_ALL, NOTIFICATION_STATUS, NOTIFICATION_TYPE} from '@/constants'
import {Button, Card, Form, Input, Select, Space} from 'antd'
import {ColumnsType} from 'antd/es/table'
import DataTable from 'components/common/DataTable'
import Link from 'components/common/Link'
import Content from 'components/layout/AdminLayout/Content'
import {useRouter} from 'next/router'
import React, {FC, useEffect, useState} from 'react'
import {Controller, SubmitHandler, useForm} from 'react-hook-form'
import {useRecoilValue, useSetRecoilState} from 'recoil'
import {authState, countDataTable, loadingState} from 'recoil/Atoms'
import {
  EnumApproveNotificationConfigDtoAction, EnumNotificationConfigEntityStatus,
  EnumOrderEntityStatus, EnumRoleEntityType,
  MerchantEntity,
  MerchantsService,
  NotificationConfigEntity,
  NotificationConfigsService
} from 'services'
import {alertError, alertSuccess, formatDate} from 'utils'
import {ServiceParams} from 'utils/interfaces'

interface Inputs {
  q?: string
  notiType?: string
  notiStatus?: string
  merchantId?: number
}

const Order: FC = () => {
  const router = useRouter()
  const {status} = router.query
  const [title, setTitle] = useState('Danh sách thông báo')
  const setIsLoading = useSetRecoilState(loadingState)
  const isLoading = useRecoilValue(loadingState)
  const countDatatable = useRecoilValue(countDataTable)
  const {control, handleSubmit, reset, getValues} = useForm<Inputs>({
    defaultValues: {
      q: '',
    },
  })

  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['updatedAt,DESC'],
    join: 'merchant'
  })
  const user = useRecoilValue(authState)
  const [merchants, setMerchants] = useState<MerchantEntity[]>([])

  useEffect(() => {
    if (status && status != '') {
      setTableServiceParams((prevState) => {
        let statusIndex = -1
        prevState.filter?.map((filter, index) => {
          if (filter?.includes('status')) {
            statusIndex = index
          }
        })
        if (statusIndex > -1) prevState.filter.splice(statusIndex, 1)
        if (status != 'all') {
          if (status === 'cancelled')
            return {
              ...prevState,
              filter: [
                ...prevState.filter,
                `status||in||${EnumOrderEntityStatus.cancelled},${EnumOrderEntityStatus.merchant_cancelled}`,
              ],
            }
          else {
            return {
              ...prevState,
              filter: [...prevState.filter, `status||eq||${status}`],
            }
          }
        } else
          return {
            ...prevState,
          }
      })
    }
  }, [status])

  useEffect(() => {
    if (user?.type === EnumRoleEntityType.admin) {
      MerchantsService.getManyBase({}).then((response) => {
        setMerchants(response.data)
      })
    }
  }, [])

  const columns: ColumnsType<NotificationConfigEntity> = [
    {
      dataIndex: 'image',
      title: <div className={'text-center'}>Ảnh</div>,
      width: 100,
      render: (value, record) => {
        return (
          <div className={'text-center w-[50px] h-[50px]'}>
            {record?.image ? (
              <img
                src={record?.image}
                alt={record.title}
                className='w-full h-full object-cover'
              />
            ) : (
              <div className='w-[50px] h-[50px] mx-auto border flex justify-center items-center text-[10px]'>
                No Image
              </div>
            )}
          </div>
        )
      },
    },
    {
      dataIndex: 'type',
      title: 'Loại thông báo',
      render: (_, record) => {
        return (
          <div>
            {NOTIFICATION_TYPE[record.type]}
          </div>
        )
      },
    },
    {
      dataIndex: 'title',
      title: 'Tiêu đề',
      render: (_, record) => {
        return (
          <div>
            {record.title}
          </div>
        )
      },
    },
    {
      dataIndex: 'receivedType',
      title: 'Đối tượng gửi',
      render: (_, record) => {
        return (
          <div>
            {NOTIFICATION_RECEIVED_TYPE_ALL[record.receivedType]}
          </div>
        )
      },
    },
    {
      dataIndex: 'merchant',
      title: 'Nhà cung cấp',
      render: (_, record) => {
        return (
          <div>
            {record.merchant?.name}
          </div>
        )
      },
    },
    {
      dataIndex: 'updatedAt',
      title: 'Ngày chỉnh sửa gần nhất',
      render: (value) => formatDate(value, 'HH:mm dd/MM/y'),
    },
    {
      dataIndex: 'scheduledTime',
      title: 'Ngày gửi',
      render: (value) => formatDate(value, 'HH:mm dd/MM/y'),
    },
    {
      dataIndex: 'status',
      title: 'Trạng thái',
      render: (_, record) => {
        return (
          <div>
            {NOTIFICATION_STATUS[record.status]}
          </div>
        )
      },
    },
    {
      title: 'Hành động',
      key: 'operation',
      align: 'center',
      render: (_, record) => {
        return (
          <Space direction="vertical">
            <Link href={`/notification/${record.id}`}>
              Xem
            </Link>
            {user?.type === EnumRoleEntityType.admin && record.status === EnumNotificationConfigEntityStatus.new &&
                <a onClick={() => updateStatus(record.id, EnumApproveNotificationConfigDtoAction.approved)}>
                    Duyệt
                </a>
            }
          </Space>
        )
      },
    },
  ]

  const updateStatus = (notificationId, action: EnumApproveNotificationConfigDtoAction) => {
    setIsLoading(true)
    if (notificationId) {
      NotificationConfigsService.notificationConfigControllerApproveNotification({
        id: Number(notificationId),
        body: {
          action: action
        },
      })
        .then(() => {
          alertSuccess('Cập nhật thành công')
          router.reload()
          setIsLoading(false)
        })
        .catch((error) => {
          setIsLoading(false)
          alertError(error)
        })
    }
  }

  const removeFilter = (filters: string[], key) => {
    let existIndex = -1
    if (filters.length > 0) {
      filters?.map((filter, index) => {
        if (filter?.includes(key)) {
          existIndex = index
        }
      })
      if (existIndex > -1) filters.splice(existIndex, 1)
    }
    return filters
  }

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    setTableServiceParams((prevState) => {
      if (data.q) {
        prevState = {
          ...prevState,
          s: `{"$or": [{"title": {"$contL": "${data.q}"}},{"body": {"$contL": "${data.q}"}}]}`,
        }
      } else {
        prevState = {
          ...prevState,
          s: undefined,
        }
      }
      if (data.notiType) {
        prevState = {
          ...prevState,
          filter: [...removeFilter(prevState.filter, 'type'), `type||eq||${data?.notiType}`],
        }
      } else {
        prevState = {
          ...prevState,
          filter: [...removeFilter(prevState.filter, 'type')],
        }
      }
      if (data.notiStatus) {
        prevState = {
          ...prevState,
          filter: [...removeFilter(prevState.filter, 'status'), `status||eq||${data?.notiStatus}`],
        }
      } else {
        prevState = {
          ...prevState,
          filter: [...removeFilter(prevState.filter, 'status')],
        }
      }

      if (data.merchantId) {
        prevState = {
          ...prevState,
          filter: [...removeFilter(prevState.filter, 'merchantId'), `merchantId||eq||${data?.merchantId}`],
        }
      } else {
        prevState = {
          ...prevState,
          filter: [...removeFilter(prevState.filter, 'merchantId')],
        }
      }
      return {...prevState}
    })
  }

  const resetForm = () => {
    reset()
    setTableServiceParams({
      sort: ['updatedAt,DESC']
    })
  }

  return (
    <Content
      title={title}
      endButtons={[
        {
          text: 'Thêm mới',
          href: '/notification/create',
          type: 'primary',
          visible: true,
        },
      ]}
    >
      <Card>
        <Form className={'mt-6'} onFinish={handleSubmit(onSubmit)}>
          <div
            className={'flex gap-4'}
            // style={{ justifyContent: 'space-between' }}
          >
            <Input.Group>
              <Controller
                control={control}
                name={'q'}
                render={({field}) => (
                  <Input
                    {...field}
                    // suffix={<SearchOutlined />}
                    placeholder="Nhập tên thông báo hoặc tiêu đề ngắn"
                  />
                )}
              />
            </Input.Group>
            <Input.Group style={{width: '20%'}}
            >
              <Controller
                control={control}
                name={'notiType'}
                render={({field}) => (
                  <Select
                    {...field}
                    placeholder="Loại thông báo"
                    allowClear={true}
                  >
                    {Object.keys(NOTIFICATION_TYPE).map((type) => (
                      <Select.Option
                        value={type}
                        key={`tbl-sl-noti-type-${type}`}
                      >
                        {NOTIFICATION_TYPE[type]}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              /></Input.Group>
            <Input.Group style={{width: '20%'}}
            >
              <Controller
                control={control}
                name={'notiStatus'}
                render={({field}) => (
                  <Select
                    {...field}
                    placeholder="Trạng thái"
                    allowClear={true}
                  >
                    {Object.keys(NOTIFICATION_STATUS).map((status) => (
                      <Select.Option
                        value={status}
                        key={`tbl-sl-noti-status-${status}`}
                      >
                        {NOTIFICATION_STATUS[status]}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              />
            </Input.Group>
            {user?.type === EnumRoleEntityType.admin &&
                <Input.Group>
                    <Controller
                        control={control}
                        name={'merchantId'}
                        render={({field}) => (
                          <Select
                            {...field}
                            placeholder="Nhà cung cấp"
                            allowClear={true}
                          >
                            {merchants.map((merchant) => (
                              <Select.Option
                                value={merchant.id}
                                key={`tbl-sl-merchant-${merchant.id}`}
                              >
                                {merchant.name}
                              </Select.Option>
                            ))}
                          </Select>
                        )}
                    />
                </Input.Group>
            }

            <Form.Item>
              <Space>
                <Button onClick={resetForm}>Đặt lại</Button>
                <Button type={'primary'} htmlType={'submit'}>
                  Tìm kiếm
                </Button>
              </Space>
            </Form.Item>
          </div>
        </Form>
      </Card>
      <Card
        title={<span className={'text-2xl'}>{countDatatable} Thông báo</span>}
      >
        <DataTable
          scroll={{
            x: 1500,
          }}
          service={NotificationConfigsService.getManyBase}
          serviceParams={tableServiceParams}
          columns={columns}
          path={'/notification'}
        />
      </Card>
    </Content>
  )
}
export default Order

import React, { FC, useEffect, useState } from 'react'
import Content from 'components/layout/AdminLayout/Content'
import { useRouter } from 'next/router'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { authState, loadingState } from 'recoil/Atoms'
import {
  ProductCategoriesService,
  ProductCategoryEntity,
  ProductEntity,
  ProductsService,
} from 'services'
import { List, Space } from 'antd'
import Link from 'components/common/Link'
import { formatCurrency } from 'utils'
import Head from 'next/head'

const Category: FC = () => {
  const router = useRouter()
  const { id, page = 1, pub_id } = router.query
  const [title, setTitle] = useState('Danh mục')
  const setIsLoading = useSetRecoilState(loadingState)
  const [category, setCategory] = useState<ProductCategoryEntity>()
  const [products, setProducts] = useState<ProductEntity[]>([])
  const [totalProducts, setTotalProducts] = useState(0)
  const [productsLoading, setProductsLoading] = useState(false)
  const user = useRecoilValue(authState)

  useEffect(() => {
    if (id) {
      setIsLoading(true)
      Promise.all([
        ProductCategoriesService.getOneBase({
          id: Number(id),
        }),
        ProductsService.getManyBase({
          filter: `productCategoryId||eq||${id}`,
          limit: 24,
          page: Number(page),
          sort: ['createdAt,DESC'],
          userId: Number(user.sub),
        }),
      ]).then(([categoryResponse, productsRespose]) => {
        setIsLoading(false)
        setTitle(categoryResponse.name)
        setProducts(productsRespose.data)
        setTotalProducts(productsRespose.total)
      })
    }
  }, [id])
  return (
    <>
      <Head>
        <title>{title}</title>
      </Head>
      <Content>
        <div className={'uppercase font-bold text-lg mb-2 flex items-end'}>
          <img src="/similar.svg" className="mr-4" />
          <div>{title}</div>
        </div>
        <div className={''}>
          <List
            loading={productsLoading}
            dataSource={products}
            renderItem={(product) => (
              <List.Item
                style={{
                  backgroundColor: '#F6F6F6',
                  cursor: 'pointer',
                }}
              >
                <Link
                  href={{
                    pathname: `/product/${product.id}`,
                    query: {
                      ...(pub_id && { pub_id: Number(pub_id) }),
                    },
                  }}
                >
                  <Space
                    direction={'vertical'}
                    style={{
                      width: '100%',
                    }}
                  >
                    <div
                      className={'w-full text-center product-grid-image border'}
                    >
                      <img
                        src={product?.images?.[0] || '/images/no-image.png'}
                        alt={product.name}
                      />
                    </div>
                    <div className={'px-4 pt-2 truncate'}>{product.name}</div>
                    <div className={'font-bold text-center pb-4'}>
                      {formatCurrency(product.price)}
                    </div>
                  </Space>
                </Link>
              </List.Item>
            )}
            itemLayout={'horizontal'}
            grid={{
              column: 4,
              gutter: 16,
              xs: 1,
            }}
            pagination={{
              current: Number(page) || 1,
              pageSize: 24,
              total: totalProducts,
              onChange: (page) => {
                router.push(
                  {
                    pathname: '/',
                    query: {
                      page,
                      ...(pub_id && { pub_id: Number(pub_id) }),
                    },
                  },
                  null,
                  {
                    scroll: false,
                  }
                )
              },
            }}
          />
        </div>
      </Content>
    </>
  )
}
export default Category

import Head from 'next/head'
import { Flash } from 'constants/icons'
import Countdown from 'react-countdown'
import React, { useEffect, useState } from 'react'
import {
  FlashSaleEntity,
  FlashSaleService,
  ProductCategoriesService,
  ProductCategoryEntity,
  ProvinceEntity,
  ProvincesService,
} from 'services'
import { alertError, filterOption } from 'utils'
import HomeProductBlock from 'components/common/HomeProductBlock'
import { Select } from 'antd'
import { Controller, useForm, useWatch } from 'react-hook-form'
import Image from 'next/image'
import NoProduct from 'components/guest/common/NoProduct'

const FlashSale = () => {
  const [flashSale, setFlashSale] = useState<FlashSaleEntity>()
  const [provinces, setProvinces] = useState<ProvinceEntity[]>([])
  const [categories, setCategories] = useState<ProductCategoryEntity[]>([])
  const isSticky = () => {
    const header = document.querySelector('.sticky-section')
    const scrollTop = window.scrollY
    scrollTop >= 120
      ? header?.classList.add('is-sticky')
      : header?.classList.remove('is-sticky')
  }
  const { control } = useForm()
  const watchCategoryId = useWatch({
    control,
    name: 'categoryId',
  })
  const watchProvinceId = useWatch({
    control,
    name: 'provinceId',
  })

  useEffect(() => {
    Promise.all([
      FlashSaleService.flashSaleControllerGetNow(),
      ProvincesService.getManyBase({
        limit: 100,
      }),
      ProductCategoriesService.getManyBase({
        limit: 100,
        filter: [`parentId||isnull`],
      }),
    ])
      .then(([flashSaleResponse, provinceResponse, categoryResponse]) => {
        setFlashSale(flashSaleResponse)
        setProvinces(provinceResponse.data)
        setCategories(categoryResponse.data)
      })
      .catch((e) => alertError(e))
    window.addEventListener('scroll', isSticky)
    return () => {
      window.removeEventListener('scroll', isSticky)
    }
  }, [])

  useEffect(() => {
    FlashSaleService.flashSaleControllerGetNow({
      ...(watchCategoryId && {
        productCategoryId: watchCategoryId,
      }),
      ...(watchProvinceId && {
        provinceId: watchProvinceId,
      }),
    })
      .then((response) => setFlashSale(response))
      .catch((e) => alertError(e))
  }, [watchCategoryId, watchProvinceId])

  return (
    <>
      <Head>
        <title>Deal Chớp Nhoáng</title>
      </Head>
      {flashSale && (
        <div className={'flash-sale'}>
          <div
            className={'sticky-section relative z-[100]'}
            style={{
              boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
            }}
          >
            <div className={'max-w-6xl mx-auto relative'}>
              <div className={'flex px-2 md:px-0 py-4 md:justify-center'}>
                <div className='flex justify-between md:justify-center bg-[#F5F5F7] z-20 md:px-4 w-full md:w-auto'>
                  <div className='flex items-center'>
                    <div className={'blinking scale-50 md:scale-100'}>
                      <Flash />
                    </div>
                    <div className={'text-price uppercase text-sm md:text-3xl font-semibold mx-1 md:mx-4'}>
                      Deal chớp nhoáng
                    </div>
                  </div>
                  <Countdown
                    date={flashSale.endTime}
                    renderer={({ hours, minutes, seconds, completed }) => {
                      if (!completed) {
                        return (
                          <div className='text-price text-base md:text-2xl font-bold flex flex-row items-center'>
                            <div className='bg-price p-2 rounded-full text-white text-base md:text-2xl font-medium mr-1 md:mr-3 w-8 md:w-12 h-8 md:h-12 flex items-center justify-center'>
                              {hours}
                            </div>
                            :
                            <div className='bg-price p-2 rounded-full text-white text-base md:text-2xl font-medium mx-1 md:mx-3 w-8 md:w-12 h-8 md:h-12 flex items-center justify-center'>
                              {minutes}
                            </div>
                            :
                            <div className='bg-price p-2 rounded-full text-white text-base md:text-2xl font-medium ml-1 md:ml-3 w-8 md:w-12 h-8 md:h-12 flex items-center justify-center'>
                              {seconds}
                            </div>
                          </div>
                        )
                      } else {
                        setFlashSale(null)
                        return <></>
                      }
                    }}
                  />
                </div>
              </div>
              <div className={'bg-price absolute h-px w-full top-1/2 z-10 hidden md:block'} />
            </div>
          </div>
          <div className='text-center hidden md:block'>
            <Image
              src='/images/flash-sale-banner.jpg'
              alt='Flash sale banner'
              width={1918}
              height={520}
            />
          </div>
          <div className='text-center md:hidden'>
            <div className='h-52 relative'>
              <Image
                src='/images/flash-sale-banner-mobile.jpg'
                alt='Flash sale banner'
                objectFit='cover'
                layout='fill'
              />
            </div>
          </div>
          <div className={'max-w-6xl mx-auto'}>
            <div className={'my-6 mx-2 flex justify-end'}>
              <Controller
                name={'categoryId'}
                control={control}
                render={({ field }) => (
                  <Select
                    {...field}
                    className={'select mr-4 w-52'}
                    placeholder={'Danh mục'}
                    allowClear
                    dropdownClassName={'flashsale-dropdown'}
                    filterOption={filterOption}
                    showSearch
                  >
                    <Select.Option
                      value={null}
                      className={
                        'mx-2 py-3 border-b border-gray-300 font-medium text-secondary hover:text-price hover:bg-transparent uppercase'
                      }
                    >
                      Tất cả
                    </Select.Option>
                    {categories?.map((category) => (
                      <Select.Option
                        value={category.id}
                        key={`category-${category.id}`}
                        className={
                          'mx-2 py-3 border-b border-gray-300 font-medium text-secondary hover:text-price hover:bg-transparent'
                        }
                      >
                        {category.name}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              />

              <Controller
                name={'provinceId'}
                control={control}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder={'Tỉnh/Thành phố'}
                    className={'select w-52'}
                    allowClear
                    dropdownClassName={'flashsale-dropdown'}
                    filterOption={filterOption}
                    showSearch
                  >
                    <Select.Option
                      value={null}
                      className={
                        'mx-2 py-3 border-b border-gray-300 font-medium text-secondary hover:text-price hover:bg-transparent'
                      }
                    >
                      Tất cả
                    </Select.Option>
                    {provinces?.map((province) => (
                      <Select.Option
                        value={province.id}
                        key={`province-${province.id}`}
                        className={
                          'mx-2 py-3 border-b border-gray-300 font-medium text-secondary hover:text-price hover:bg-transparent'
                        }
                      >
                        {province.name}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              />
            </div>
            {flashSale?.flashSaleDetail?.length > 0 ? (
              <div className={'grid grid-cols-2 md:grid-cols-4 mb-8'}>
                {flashSale?.flashSaleDetail?.map((flashSaleDetail) => {
                  return (
                    <HomeProductBlock
                      key={`flashsale-product-${flashSaleDetail.id}`}
                      product={{
                        ...flashSaleDetail.product,
                        price: flashSaleDetail.discountValue,
                      }}
                      salePercent={
                        flashSaleDetail.usedQuantity / flashSale.maxQuantity
                      }
                      className={'p-2'}
                    />
                  )
                })}
              </div>
            ) : (
              <NoProduct />
            )}
          </div>
        </div>
      )}
    </>
  )
}
export default FlashSale

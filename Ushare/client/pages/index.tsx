import React, { FC, useEffect, useState } from 'react'
import { Col, Row, Tabs } from 'antd'
import {
  ConfigService,
  FlashSaleEntity,
  FlashSaleService,
  ProductCategoriesService,
  ProductCategoryEntity,
  ProductEntity,
  ProductsService,
} from 'services'
import { useRouter } from 'next/router'
import Link from 'components/common/Link'
import Head from 'next/head'
import Slider from 'react-slick'
import Countdown from 'react-countdown'
import { NextArrow, PrevArrow } from 'components/common/CarouselArrows'
import { Flash, FlashWhite, ViewMoreIcon } from 'constants/icons'
import HomeProductBlock from 'components/common/HomeProductBlock'
import { useSetRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'
import NoProduct from 'components/guest/common/NoProduct'

const Index: FC = () => {
  const setIsLoading = useSetRecoilState(loadingState)
  const router = useRouter()
  const { pub_id = 0 } = router.query
  const [productCategories, setProductCategories] = useState<
    ProductCategoryEntity[]
  >([])
  const [topProductSales, setTopProductSales] = useState<ProductEntity[]>([])
  const [topSearch, setTopSearch] = useState<ProductEntity[]>([])
  const [newestProducts, setNewestProducts] = useState<ProductEntity[]>([])
  const [forYou, setForYou] = useState<ProductEntity[]>([])
  const [currentOffers, setCurrentOffers] = useState([])
  const [flashSale, setFlashSale] = useState<FlashSaleEntity>()
  const [activeKey, setActiveKey] = useState('top-search')

  useEffect(() => {
    setIsLoading(true)
    Promise.all([
      ProductCategoriesService.getManyBase({
        limit: 100,
        filter: [`parentId||isnull`],
      }),
      ProductsService.getManyBase({
        limit: 12,
        sort: ['saleAmount,DESC'],
      }),
      ConfigService.getManyBase({
        filter: [`key||in||current_offers,flash_sale`],
      }),
      FlashSaleService.flashSaleControllerGetNow(),
      ProductsService.getManyBase({
        type: 'top_search',
        limit: 12,
      }),
      ProductsService.getManyBase({
        sort: ['createdAt,DESC'],
        limit: 12,
      }),
      ProductsService.getManyBase({
        type: 'for_you',
        limit: 12,
      }),
    ]).then(
      ([
        productCategoriesResponse,
        topProductSaleResponse,
        configResponse,
        flashSaleResponse,
        topSearchResponse,
        newestProductsResponse,
        forYouResponse,
      ]) => {
        configResponse.data.map((config) => {
          switch (config.key) {
            case 'current_offers':
              setCurrentOffers(config.value)
          }
        })
        if (flashSaleResponse?.id) setFlashSale(flashSaleResponse)
        setTopProductSales(topProductSaleResponse.data)
        setProductCategories(productCategoriesResponse.data)
        setTopSearch(topSearchResponse.data)
        setNewestProducts(newestProductsResponse.data)
        setForYou(forYouResponse.data)
        setIsLoading(false)
      }
    )
  }, [])

  return (
    <>
      <Head>
        <title>UShare - Kinh doanh kiểu mới - Bán hàng rảnh tay</title>
      </Head>
      <div className={'index-page'}>
        <div>
          <Slider
            arrows={false}
            dots={false}
            useTransform={false}
            swipeToSlide={true}
            infinite={true}
            slidesToShow={1}
            slidesToScroll={1}
            className={'top-banner'}
            adaptiveHeight={true}
            autoplay={true}
          >
            <div key={`top-banner`}>
              <div
                style={{
                  background: 'url("/images/home_banner_bg.jpg") repeat-x',
                  minHeight: '200px',
                }}
              >
                <img src={'/images/home_banner.jpg'} alt={'TOP Banner'} />
              </div>
            </div>
          </Slider>
        </div>
        <div className={'max-w-6xl mx-auto md:p-6 py-6'}>
          <Row gutter={[24, 24]}>
            <Col xs={24}>
              {productCategories.length > 0 && (
                <Slider
                  dots={false}
                  useTransform={false}
                  swipeToSlide={false}
                  slidesToShow={5}
                  slidesToScroll={5}
                  className={'categories'}
                  adaptiveHeight={true}
                  nextArrow={
                    <div>
                      <div
                        className={'hover:text-price -mt-14'}
                        style={{
                          color: '#999999',
                        }}
                      >
                        <NextArrow />
                      </div>
                    </div>
                  }
                  prevArrow={
                    <div>
                      <div
                        className={'hover:text-price -mt-14'}
                        style={{
                          color: '#999999',
                        }}
                      >
                        <PrevArrow />
                      </div>
                    </div>
                  }
                  responsive={[
                    {
                      breakpoint: 600,
                      settings: {
                        slidesToShow: 2,
                        arrows: false,
                      },
                    },
                    {
                      breakpoint: 480,
                      settings: {
                        slidesToShow: 2,
                        arrows: false,
                      },
                    },
                  ]}
                >
                  {productCategories.map((category) => (
                    <div key={`category-${category.id}`}>
                      <Link
                        href={{
                          pathname: `/danh-muc/${category.slug}_${category.id}`,
                          query: {
                            ...(pub_id && { pub_id: Number(pub_id) }),
                          },
                        }}
                      >
                        <div
                          className={
                            'px-4 flex items-center flex-col text-center hover:drop-shadow-md'
                          }
                        >
                          <div
                            className={
                              'flex items-center justify-center h-28 w-full rounded-xl p-4 category-slider-item'
                            }
                            style={{
                              backgroundColor: category.color,
                            }}
                          >
                            <div
                              className={
                                'w-full h-full category-slider-item-icon'
                              }
                              style={{
                                background: `url("${category.image}") no-repeat center`,
                                backgroundSize: 'contain',
                              }}
                            />
                            <div
                              className={
                                'md:hidden mt-2 font-normal text-white text-xs ml-2'
                              }
                            >
                              {category.name}
                            </div>
                          </div>
                          <div
                            className={
                              'md:block hidden mt-2 font-thin text-primary text-xl'
                            }
                          >
                            {category.name}
                          </div>
                        </div>
                      </Link>
                    </div>
                  ))}
                </Slider>
              )}
            </Col>
          </Row>
        </div>
        {flashSale?.flashSaleDetail.length > 0 && (
          <>
            <div className={'max-w-6xl mx-auto relative hidden md:block'}>
              <div className={'flex py-4 justify-center'}>
                <div
                  className={
                    'flex justify-center items-center px-4 bg-[#F5F5F7]'
                  }
                >
                  <div className={'blinking'}>
                    <Flash />
                  </div>
                  <div
                    className={
                      'text-price uppercase text-3xl font-semibold mx-4'
                    }
                  >
                    Deal chớp nhoáng
                  </div>
                  <Countdown
                    date={flashSale.endTime}
                    renderer={({ hours, minutes, seconds, completed }) => {
                      if (!completed) {
                        return (
                          <div
                            className={
                              'text-price text-2xl font-bold flex flex-row items-center'
                            }
                          >
                            <div
                              className={
                                'bg-price p-2 rounded-full text-white text-2xl font-medium mr-3 w-12 h-12 text-center'
                              }
                            >
                              {hours}
                            </div>
                            :
                            <div
                              className={
                                'bg-price p-2 rounded-full text-white text-2xl font-medium mx-3 w-12 h-12 text-center'
                              }
                            >
                              {minutes}
                            </div>
                            :
                            <div
                              className={
                                'bg-price p-2 rounded-full text-white text-2xl font-medium ml-3 w-12 h-12 text-center'
                              }
                            >
                              {seconds}
                            </div>
                          </div>
                        )
                      } else {
                        setFlashSale(null)
                        return <></>
                      }
                    }}
                  />
                </div>
              </div>
              <div className={'bg-price absolute h-px w-full top-1/2 -z-10'} />
            </div>
            <div
              className={
                'max-w-6xl mx-auto bg-[url("/images/flash-sale-background.jpeg")] bg-no-repeat md:bg-center md:bg-cover'
              }
            >
              <div className={'px-4'}>
                <div
                  className={'flex justify-between items-center pt-4 md:hidden'}
                >
                  <div className="flex items-center">
                    <div className={'blinking'}>
                      <FlashWhite />
                    </div>
                    <div
                      className={
                        'text-white uppercase text-base font-semibold ml-2 mr-1'
                      }
                    >
                      Deal chớp nhoáng
                    </div>
                  </div>
                  <Countdown
                    date={flashSale.endTime}
                    renderer={({ hours, minutes, seconds, completed }) => {
                      if (!completed) {
                        return (
                          <div
                            className={
                              'text-white text-base font-bold flex flex-row items-center'
                            }
                          >
                            <div
                              className={
                                'bg-white p-2 rounded-full text-price text-base font-medium mr-1 w-8 h-8 flex items-center justify-center'
                              }
                            >
                              {hours}
                            </div>
                            :
                            <div
                              className={
                                'bg-white p-2 rounded-full text-price text-base font-medium mx-1 w-8 h-8 flex items-center justify-center'
                              }
                            >
                              {minutes}
                            </div>
                            :
                            <div
                              className={
                                'bg-white p-2 rounded-full text-price text-base font-medium ml-1 w-8 h-8 flex items-center justify-center'
                              }
                            >
                              {seconds}
                            </div>
                          </div>
                        )
                      } else {
                        setFlashSale(null)
                        return <></>
                      }
                    }}
                  />
                </div>
                <div className={'py-4'}>
                  <div className={'grid grid-cols-2 md:grid-cols-4'}>
                    {flashSale?.flashSaleDetail.length > 0 &&
                      flashSale?.flashSaleDetail.map(
                        (flashSaleDetail, flashSaleDetailIndex) => {
                          return (
                            flashSaleDetailIndex <= 3 && (
                              <HomeProductBlock
                                key={`flashsale-product-${flashSaleDetail.id}`}
                                product={{
                                  ...flashSaleDetail.product,
                                  price: flashSaleDetail.discountValue,
                                  saleAmount: flashSaleDetail.usedQuantity,
                                }}
                                salePercent={
                                  flashSaleDetail.usedQuantity /
                                  flashSale.maxQuantity
                                }
                                className={`p-1 md:p-2 ${
                                  flashSaleDetailIndex >= 2
                                    ? 'hidden md:block'
                                    : ''
                                }`}
                                isFlashsale={true}
                              />
                            )
                          )
                        }
                      )}
                  </div>
                  <div className={'text-center'}>
                    <Link
                      href={{
                        pathname: '/deal-chop-nhoang',
                        query: {
                          ...(pub_id && { pub_id: Number(pub_id) }),
                        },
                      }}
                    >
                      <div
                        className={
                          'text-white flex justify-center items-center mt-4'
                        }
                      >
                        <span className={'mr-2'}>Xem thêm</span>
                        <ViewMoreIcon />
                      </div>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
        <div className={'max-w-6xl mx-auto md:p-6'}>
          <Row gutter={[24, 24]}>
            {topProductSales && (
              <Col xs={24}>
                <div className={'relative'}>
                  <div className={'flex py-4 md:px-4 md:justify-center'}>
                    <div
                      className={'flex justify-center items-center px-4'}
                      style={{
                        backgroundColor: '#F5F5F7',
                      }}
                    >
                      <div
                        className={
                          'text-primary uppercase text-3xl font-semibold'
                        }
                      >
                        TOP bán chạy
                      </div>
                    </div>
                  </div>
                  <div
                    className={
                      'hidden md:block absolute h-px w-full top-1/2 -z-10'
                    }
                    style={{
                      backgroundColor: '#C6C6C6',
                    }}
                  />
                </div>
                <div className={'p-4'}>
                  <Slider
                    slidesToShow={4}
                    slidesToScroll={4}
                    autoplay={true}
                    className={'top-product-slider'}
                    nextArrow={
                      <div>
                        <div
                          className={'hover:text-price -mt-14'}
                          style={{
                            color: '#999999',
                          }}
                        >
                          <NextArrow />
                        </div>
                      </div>
                    }
                    prevArrow={
                      <div>
                        <div
                          className={'hover:text-price -mt-14'}
                          style={{
                            color: '#999999',
                          }}
                        >
                          <PrevArrow />
                        </div>
                      </div>
                    }
                    responsive={[
                      {
                        breakpoint: 1024,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 2,
                          arrows: false,
                        },
                      },
                      {
                        breakpoint: 600,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 2,
                          arrows: false,
                        },
                      },
                      {
                        breakpoint: 480,
                        settings: {
                          slidesToShow: 2,
                          slidesToScroll: 2,
                          arrows: false,
                        },
                      },
                    ]}
                  >
                    {topProductSales.length > 0 &&
                      topProductSales.map((product, index) => (
                        <HomeProductBlock
                          key={`top-product-sales-${product.id}`}
                          product={product}
                          pub_id={Number(pub_id)}
                          index={index}
                          className={'px-1'}
                        />
                      ))}
                  </Slider>
                  <div className={'text-center mt-10'}>
                    <Link
                      href={{
                        pathname: '/xem-them/top-ban-chay',
                        query: {
                          ...(pub_id && { pub_id: Number(pub_id) }),
                        },
                      }}
                    >
                      <div
                        className={'flex justify-center items-center'}
                        style={{
                          color: '#33A0FF',
                        }}
                      >
                        <span className={'mr-2'}>Xem thêm</span>
                        <svg
                          width="8"
                          height="13"
                          viewBox="0 0 8 13"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M2.20719 12.2928L7.29289 7.20711C7.68342 6.81658 7.68342 6.18342 7.29289 5.79289L2.20719 0.707193C1.57723 0.0772284 0.500086 0.523398 0.500086 1.4143L0.500086 11.5857C0.500086 12.4766 1.57723 12.9228 2.20719 12.2928Z"
                            fill="currentColor"
                          />
                        </svg>
                      </div>
                    </Link>
                  </div>
                </div>
              </Col>
            )}
          </Row>
        </div>
        {currentOffers && (
          <div className={'bg-white'}>
            <div className={'max-w-6xl mx-auto md:py-6'}>
              <Row gutter={[24, 24]}>
                <Col xs={24}>
                  <div className={'relative'}>
                    <div className={'relative flex p-4 md:justify-center z-10'}>
                      <div
                        className={
                          'bg-white flex justify-center items-center px-4'
                        }
                      >
                        <div
                          className={
                            'text-center text-primary uppercase text-3xl font-semibold'
                          }
                        >
                          Ưu đãi hiện có
                        </div>
                      </div>
                    </div>
                    <div
                      className={'md:block hidden absolute h-px w-full top-1/2'}
                      style={{
                        backgroundColor: '#C6C6C6',
                      }}
                    />
                  </div>
                  <div className={'py-4 grid md:grid-cols-4 gap-4'}>
                    {currentOffers?.length > 0 && (
                      <>
                        <a
                          href={currentOffers[0].url || '/'}
                          target={'_blank'}
                          rel={'noreferrer'}
                          className={'w-full col-span-3'}
                          style={{
                            height: 301,
                          }}
                        >
                          <div
                            className={`col-span-3 rounded-2xl`}
                            style={{
                              background: `url("${currentOffers[0].image}") no-repeat`,
                              backgroundSize: 'cover',
                              backgroundPosition: 'center',
                              height: 301,
                            }}
                          />
                        </a>
                        <a
                          className={'col-span-3 md:col-span-1'}
                          href={currentOffers[1].url || '/'}
                          target={'_blank'}
                          rel={'noreferrer'}
                          style={{
                            height: 301,
                          }}
                        >
                          <div
                            className={`rounded-2xl`}
                            style={{
                              background: `url("${currentOffers[1].image}") no-repeat`,
                              backgroundSize: 'cover',
                              backgroundPosition: 'center',
                              height: 301,
                            }}
                          />
                        </a>
                        <a
                          href={currentOffers[3].url || '/'}
                          target={'_blank'}
                          rel={'noreferrer'}
                          className={'col-span-3'}
                        >
                          <div
                            className={`rounded-2xl`}
                            style={{
                              background: `url("${currentOffers[3].image}") no-repeat`,
                              backgroundSize: 'cover',
                              backgroundPosition: 'center',
                              height: 301,
                            }}
                          />
                        </a>
                        <a
                          className={'col-span-3 md:col-span-1'}
                          href={currentOffers[2].url || '/'}
                          target={'_blank'}
                          rel={'noreferrer'}
                          style={{}}
                        >
                          <div
                            className={`rounded-2xl`}
                            style={{
                              background: `url("${currentOffers[2].image}") no-repeat`,
                              backgroundSize: 'cover',
                              backgroundPosition: 'center',
                              height: 301,
                            }}
                          />
                        </a>
                      </>
                    )}
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        )}
        <div className={'max-w-6xl mx-auto'}>
          <div className={'relative md:mt-6'}>
            <div
              className={'relative flex py-4 md:px-4 md:justify-center z-10'}
            >
              <div
                className={'flex justify-center items-center px-4'}
                style={{
                  backgroundColor: '#F5F5F7',
                }}
              >
                <div
                  className={'text-primary uppercase text-3xl font-semibold'}
                >
                  Gợi ý hôm nay
                </div>
              </div>
            </div>
            <div
              className={'md:block hidden absolute h-px w-full top-1/2'}
              style={{
                backgroundColor: '#C6C6C6',
              }}
            />
          </div>
          <div className="px-4 md:px-0">
            <Tabs
              defaultActiveKey="top-search"
              centered
              className={`today-tabs active-${activeKey}`}
              onChange={(value) => setActiveKey(value)}
            >
              <Tabs.TabPane tab="Top tìm kiếm" key="top-search">
                {topSearch?.length > 0 ? (
                  <>
                    <div
                      className={
                        'grid grid-cols-2 md:grid-cols-4 gap-2 md:gap-4 mt-5'
                      }
                    >
                      {topSearch?.map((product) => (
                        <HomeProductBlock
                          key={`top-search-${product.id}`}
                          product={product}
                          pub_id={Number(pub_id)}
                        />
                      ))}
                    </div>
                    <div className={'text-center my-10'}>
                      <Link
                        href={{
                          pathname: '/xem-them/top-tim-kiem',
                          query: {
                            ...(pub_id && { pub_id: Number(pub_id) }),
                          },
                        }}
                      >
                        <div
                          className={'flex justify-center items-center'}
                          style={{
                            color: '#33A0FF',
                          }}
                        >
                          <span className={'mr-2'}>Xem thêm</span>
                          <ViewMoreIcon />
                        </div>
                      </Link>
                    </div>
                  </>
                ) : (
                  <NoProduct />
                )}
              </Tabs.TabPane>
              <Tabs.TabPane tab="Sản phẩm mới" key="new-product">
                {newestProducts?.length > 0 ? (
                  <>
                    <div
                      className={
                        'grid grid-cols-2 md:grid-cols-4 gap-2 md:gap-4'
                      }
                    >
                      {newestProducts?.map((product) => (
                        <HomeProductBlock
                          key={`newest-${product.id}`}
                          product={product}
                          pub_id={Number(pub_id)}
                        />
                      ))}
                    </div>
                    <div className={'text-center mt-4'}>
                      <Link
                        href={{
                          pathname: '/xem-them/san-pham-moi',
                          query: {
                            ...(pub_id && { pub_id: Number(pub_id) }),
                          },
                        }}
                      >
                        <div
                          className={'flex justify-center items-center'}
                          style={{
                            color: '#33A0FF',
                          }}
                        >
                          <span className={'mr-2'}>Xem thêm</span>
                          <ViewMoreIcon />
                        </div>
                      </Link>
                    </div>
                  </>
                ) : (
                  <NoProduct />
                )}
              </Tabs.TabPane>
              <Tabs.TabPane tab="Dành riêng cho bạn" key="for-you">
                {forYou.length > 0 ? (
                  <>
                    <div
                      className={
                        'grid grid-cols-2 md:grid-cols-4 gap-2 md:gap-4'
                      }
                    >
                      {forYou?.map((product) => (
                        <HomeProductBlock
                          key={`for-you-${product.id}`}
                          product={product}
                          pub_id={Number(pub_id)}
                        />
                      ))}
                    </div>
                    <div className={'text-center mt-4'}>
                      <Link
                        href={{
                          pathname: '/xem-them/danh-rieng-cho-ban',
                          query: {
                            ...(pub_id && { pub_id: Number(pub_id) }),
                          },
                        }}
                      >
                        <div
                          className={'flex justify-center items-center'}
                          style={{
                            color: '#33A0FF',
                          }}
                        >
                          <span className={'mr-2'}>Xem thêm</span>
                          <ViewMoreIcon />
                        </div>
                      </Link>
                    </div>
                  </>
                ) : (
                  <NoProduct />
                )}
              </Tabs.TabPane>
            </Tabs>
          </div>
        </div>
      </div>
    </>
  )
}
export default Index

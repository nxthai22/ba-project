import { useRecoilValue } from 'recoil'
import { authState, countDataTable } from 'recoil/Atoms'
import { ColumnsType } from 'antd/es/table'
import React, { FC, useState } from 'react'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { Button, Card, Col, Form, Input, Row, Select, Space, Tag } from 'antd'
import { ServiceParams } from 'utils/interfaces'
import {
  EnumMerchantAddressEntityStatus, EnumRoleEntityType,
  EnumUserEntityStatus,
  MerchantAddressEntity,
  MerchantAddressesService
} from 'services'
import Content from 'components/layout/AdminLayout/Content'
import DataTable from 'components/common/DataTable'
import { format } from 'date-fns'

interface Inputs {
  name?: string
  status?: EnumMerchantAddressEntityStatus
}

const Index: FC = () => {
  const user = useRecoilValue(authState)
  const [title] = useState<string>('Quản lý Kho hàng')

  const countDatatable = useRecoilValue(countDataTable)
  const { control, handleSubmit, reset } = useForm<Inputs>({
    defaultValues: {
      name: ''
    }
  })
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['createdAt,DESC'],
    filter: [user.type === EnumRoleEntityType.admin ? 'merchantId||isnull' : `merchantId||eq||${user.merchantId}`],
    join: [
      'parent',
      'merchant',
      'ward',
      'ward.district',
      'ward.district.province'
    ]
  })
  const columns: ColumnsType<MerchantAddressEntity> = [
    {
      dataIndex: 'name',
      title: 'Tên kho'
    },
    {
      dataIndex: 'code',
      title: 'Mã'
    },
    {
      dataIndex: 'merchant',
      title: 'Nhà cung cấp',
      render: (value) => value?.name
    },
    {
      dataIndex: 'address',
      title: 'Tỉnh/Thành phố',
      render: (value, record) => record?.ward?.district?.province?.name
    },
    {
      dataIndex: 'tel',
      title: 'SĐT'
    },
    {
      dataIndex: 'status',
      title: 'Trạng thái',
      render: (value) =>
        value === EnumMerchantAddressEntityStatus.active ? (
          <Tag color={'success'}>Hoạt động</Tag>
        ) : value === EnumUserEntityStatus.inactive ? (
          <Tag color={'warning'}>Không hoạt động</Tag>
        ) : null
    },
    {
      dataIndex: 'createdAt',
      title: 'Ngày tạo',
      render: (value) => format(new Date(value), 'd/M/y')
    }
  ]
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const filter = [user.type === EnumRoleEntityType.admin ? 'merchantId||isnull' : `merchantId||eq||${user.merchantId}`]
    const or = []
    if (data?.name)
      filter.push(`name||$contL||${data?.name}`) &&
      or.push(`code||$contL||${data?.name}`)

    if (data?.status) filter.push(`status||$eq||${data?.status}`)

    setTableServiceParams((prevState) => ({
      ...prevState,
      filter: filter,
      or: or
    }))
  }

  const onReset = () => {
    reset()
    setTableServiceParams({
      sort: ['createdAt,DESC'],
      filter: [user.type === EnumRoleEntityType.admin ? 'merchantId||isnull' : `merchantId||eq||${user.merchantId}`],
      join: [
        'parent',
        'merchant',
        'ward',
        'ward.district',
        'ward.district.province'
      ]
    })
  }
  return (
    <Content
      title={title}
      endButtons={[
        {
          text: 'Thêm mới',
          href: '/merchant-address/create',
          type: 'primary',
          visible: true
        }
      ]}
    >
      <Card>
        <Form
          name='search-form'
          autoComplete={'off'}
          onFinish={handleSubmit(onSubmit)}
          onReset={onReset}
          colon={false}
        >
          <Row gutter={[16, 16]}>
            <Col xs={24} sm={24} md={8}>
              <Form.Item label={'Mã kho hoặc tên kho'}>
                <Controller
                  control={control}
                  name={'name'}
                  render={({ field }) => (
                    <Input
                      {...field}
                      placeholder={'Nhập mã kho hoặc tên kho'}
                    />
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item label={'Trạng thái'}>
                <Controller
                  control={control}
                  name={'status'}
                  render={({ field }) => (
                    <Select {...field} placeholder={'Chọn trạng thái kho'}>
                      <Select.Option value={'active'}>Hoạt động</Select.Option>
                      <Select.Option value={'inactive'}>
                        Không hoạt động
                      </Select.Option>
                    </Select>
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8} className={'text-right'}>
              <Form.Item>
                <Space>
                  <Button htmlType={'reset'}>Đặt lại</Button>
                  <Button type={'primary'} htmlType={'submit'}>
                    Tìm kiếm
                  </Button>
                </Space>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>
      <Card
        title={<span className={'text-2xl'}>{countDatatable} Kho hàng</span>}
      >
        <DataTable
          service={MerchantAddressesService.getManyBase}
          serviceParams={tableServiceParams}
          columns={columns}
          action={['edit']}
          path={'/merchant-address'}
        />
      </Card>
    </Content>
  )
}

export default Index

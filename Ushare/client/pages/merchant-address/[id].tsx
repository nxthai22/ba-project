import React, { FC, useEffect, useState } from 'react'
import { Button, Card, Col, Form, Input, Row, Select, Switch } from 'antd'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import { Controller, SubmitHandler, useForm, useWatch } from 'react-hook-form'
import {
  DistrictEntity,
  DistrictsService,
  EnumMerchantAddressEntityStatus,
  MerchantAddressEntity,
  MerchantAddressesService,
  ProvinceEntity,
  ProvincesService,
  WardEntity,
  WardsService,
} from 'services'
import { alertError, filterOption, modifyEntity } from 'utils'
import { useSetRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'
import { useRouter } from 'next/router'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import FormItem from 'components/common/FormItem'
import Content from 'components/layout/AdminLayout/Content'

interface Inputs {
  merchantAddress: MerchantAddressEntity
  provinceId?: number
  districtId?: number
  wardId: number
  isActive: boolean
}

const phoneRegExp = /([+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/
const schema = yup.object().shape({
  merchantAddress: yup.object().shape({
    name: yup.string().required('Chưa nhập Tên'),
    code: yup.string(),
    tel: yup
      .string()
      .required('Chưa nhập số điện thoại')
      .matches(phoneRegExp, 'Số điện thoại không hợp lệ'),
    address: yup.string().required('Chưa nhập địa chỉ kho'),
  }),
  provinceId: yup
    .number()
    .required('Chưa chọn Thành phố/Tỉnh')
    .typeError('Chưa chọn Thành phố/Tỉnh'),
  districtId: yup
    .number()
    .required('Chưa chọn Quận/Huyện')
    .typeError('Chưa chọn Quận/Huyện'),
  wardId: yup
    .number()
    .required('Chưa chọn Xã/Phường')
    .typeError('Chưa chọn Xã/Phường'),
})

const MerchantAddressDetail: FC = () => {
  const [title, setTitle] = useState<string>('Tạo mới kho hàng')
  const router = useRouter()
  const setIsLoading = useSetRecoilState(loadingState)
  const [provinces, setProvinces] = useState<ProvinceEntity[]>([])
  const [districts, setDistricts] = useState<DistrictEntity[]>([])
  const [wards, setWards] = useState<WardEntity[]>([])
  const [merchantAddress, setMerchantAddress] =
    useState<MerchantAddressEntity>()
  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
    defaultValues: {
      merchantAddress: {
        name: '',
        code: '',
      },
      isActive: true,
    },
  })
  const watchWardId = useWatch({
    control,
    name: 'wardId',
    defaultValue: 0,
  })
  const watchProvinceId = useWatch({
    control,
    name: 'provinceId',
    defaultValue: 0,
  })
  const watchDistrictId = useWatch({
    control,
    name: 'districtId',
    defaultValue: 0,
  })
  useEffect(() => {
    setIsLoading(true)
    ProvincesService.getManyBase({
      limit: 100,
      sort: ['name,ASC'],
    })
      .then((response) => {
        setIsLoading(false)
        setProvinces(response.data)
      })
      .catch((error) => {
        setIsLoading(false)
        alertError(error)
      })
  }, [])
  useEffect(() => {
    if (watchWardId && watchWardId != 0) {
      setIsLoading(true)
      WardsService.getOneBase({
        id: watchWardId,
        join: ['district', 'district.province'],
      })
        .then((response) => {
          setIsLoading(false)
          setMerchantAddress((prevState) => {
            return {
              ...prevState,
              ward: response,
            }
          })
        })
        .catch((error) => {
          setIsLoading(false)
          alertError(error)
        })
    }
  }, [watchWardId])

  useEffect(() => {
    setValue('wardId', null)
    setWards([])
    if (watchDistrictId && watchDistrictId != 0) {
      setIsLoading(true)
      WardsService.getManyBase({
        filter: `districtId||eq||${watchDistrictId}`,
        limit: 100,
        sort: ['name,ASC'],
      })
        .then((response) => {
          setIsLoading(false)
          setWards(response.data)
          const existWard = response.data.find(
            (ward) => ward.id === merchantAddress?.ward?.id
          )
          if (existWard) setValue('wardId', merchantAddress?.ward?.id)
        })
        .catch((error) => {
          setIsLoading(false)
          alertError(error)
        })
    }
  }, [watchDistrictId])

  useEffect(() => {
    setValue('districtId', null)
    if (watchProvinceId && watchProvinceId != 0) {
      setIsLoading(true)
      DistrictsService.getManyBase({
        filter: `provinceId||eq||${watchProvinceId}`,
        limit: 100,
        sort: ['name,ASC'],
      })
        .then((response) => {
          setIsLoading(false)
          setDistricts(response.data)
          const existDistrict = response.data.find(
            (district) => district.id === merchantAddress?.ward?.district?.id
          )
          if (existDistrict)
            setValue('districtId', merchantAddress?.ward?.district?.id)
        })
        .catch((error) => {
          setIsLoading(false)
          alertError(error)
        })
    }
  }, [watchProvinceId])

  useEffect(() => {
    if (Number(router?.query?.id)) {
      setTitle('Cập nhật kho hàng')
      setIsLoading(true)
      MerchantAddressesService.getOneBase({
        id: Number(router?.query?.id),
        join: ['ward', 'ward.district', 'ward.district.province'],
      })
        .then((response) => {
          setValue('merchantAddress', response)
          setMerchantAddress(response)
          setValue(
            'isActive',
            response.status === EnumMerchantAddressEntityStatus.active
          )
          setValue('provinceId', response.ward?.district?.provinceId)
          setIsLoading(false)
        })
        .catch((e) => {
          alertError(e)
          setIsLoading(false)
        })
    }
  }, [router])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    data.merchantAddress = {
      ...data.merchantAddress,
      wardId: data.wardId,
      status: data.isActive
        ? EnumMerchantAddressEntityStatus.active
        : EnumMerchantAddressEntityStatus.inactive,
    }
    delete data.merchantAddress.ward
    setIsLoading(false)
    modifyEntity(
      MerchantAddressesService,
      data.merchantAddress,
      title,
      (response) => {
        setIsLoading(false)
        router.push(`/merchant-address`)
      }
    ).then()
  }
  return (
    <Content title={title} onBack={() => router.push('/merchant-address')}>
      <Card>
        <Form onFinish={handleSubmit(onSubmit)}>
          <Row gutter={[16, 16]}>
            <Col xs={24} sm={24} md={24} lg={12} xl={8}>
              <FormItem
                label={<span>Mã kho</span>}
                validateStatus={errors.merchantAddress?.code && 'error'}
                help={errors.merchantAddress?.code?.message}
              >
                <Controller
                  control={control}
                  name={'merchantAddress.code'}
                  render={({ field }) => (
                    <Input {...field} placeholder={'Mã kho hàng'} />
                  )}
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={8}>
              <FormItem
                label={<span>Tên kho hàng</span>}
                required={true}
                validateStatus={errors.merchantAddress?.name && 'error'}
                help={errors.merchantAddress?.name?.message}
              >
                <Controller
                  control={control}
                  name={'merchantAddress.name'}
                  render={({ field }) => (
                    <Input {...field} placeholder={'Nhập tên kho hàng'} />
                  )}
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={8}>
              <FormItem
                label={<span>Số điện thoại</span>}
                required={true}
                validateStatus={errors.merchantAddress?.tel && 'error'}
                help={errors.merchantAddress?.tel?.message}
              >
                <Controller
                  control={control}
                  name={'merchantAddress.tel'}
                  render={({ field }) => <Input {...field} />}
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={8}>
              <FormItem
                label={'Thành phố/Tỉnh'}
                required={true}
                validateStatus={errors.provinceId && 'error'}
                help={errors.provinceId?.message}
              >
                <Controller
                  control={control}
                  name={'provinceId'}
                  render={({ field }) => (
                    <Select filterOption={filterOption} showSearch {...field}>
                      {provinces.map((province) => (
                        <Select.Option
                          value={province.id}
                          key={`province-${province.id}`}
                        >
                          {province.name}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={8}>
              <FormItem
                label={'Quận/Huyện'}
                required={true}
                validateStatus={errors.districtId && 'error'}
                help={errors.districtId?.message}
              >
                <Controller
                  control={control}
                  name={'districtId'}
                  render={({ field }) => (
                    <Select filterOption={filterOption} showSearch {...field}>
                      {districts.map((district) => (
                        <Select.Option
                          value={district.id}
                          key={`district-${district.id}`}
                        >
                          {district.name}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={8}>
              <FormItem
                label={'Phường/Xã'}
                required={true}
                validateStatus={errors.wardId && 'error'}
                help={errors.wardId?.message}
              >
                <Controller
                  control={control}
                  name={'wardId'}
                  render={({ field }) => (
                    <Select showSearch filterOption={filterOption} {...field}>
                      {wards.map((ward) => (
                        <Select.Option value={ward.id} key={`ward-${ward.id}`}>
                          {ward.name}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={8}>
              <FormItem
                label={<span>Địa chỉ</span>}
                required={true}
                validateStatus={errors.merchantAddress?.address && 'error'}
                help={errors.merchantAddress?.address?.message}
              >
                <Controller
                  control={control}
                  name={'merchantAddress.address'}
                  render={({ field }) => (
                    <Input {...field} placeholder={'Địa chỉ'} />
                  )}
                />
              </FormItem>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} xl={8}>
              <FormItem label={'Hiển thị'}>
                <Controller
                  control={control}
                  name={`isActive`}
                  defaultValue={true}
                  render={({ field }) => (
                    <Switch
                      checked={field.value}
                      {...field}
                      onChange={(value) => {
                        setValue(`isActive`, value)
                      }}
                    />
                  )}
                />
              </FormItem>
            </Col>
          </Row>
          <FooterBar
            right={
              <Button htmlType={'submit'} type={'primary'}>
                Lưu
              </Button>
            }
          />
        </Form>
      </Card>
    </Content>
  )
}
export default MerchantAddressDetail

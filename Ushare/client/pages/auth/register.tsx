import { LockOutlined, PhoneOutlined, UserOutlined } from '@ant-design/icons'
import { signInWithPhoneNumber } from '@firebase/auth'
import { yupResolver } from '@hookform/resolvers/yup'
import { useLocalStorageState } from 'ahooks'
import { Button, Col, Form, Input, Row, Spin, Typography } from 'antd'
import axios, { AxiosRequestConfig } from 'axios'
import firebase from 'firebase'
import Head from 'next/head'
import { useRouter } from 'next/router'
import React, { FC, useEffect, useState } from 'react'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import OtpInput from 'react-otp-input'
import { useRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'
import { AuthService, RegisterDto } from 'services'
import { serviceOptions } from 'services/serviceOptions'
import { alertError, alertSuccess, convertTel } from 'utils'
import { app } from 'utils/firebase'
import * as yup from 'yup'
import { SchemaOf } from 'yup'
import styles from './auth.module.css'
import RecaptchaVerifier = firebase.auth.RecaptchaVerifier
import ConfirmationResult = firebase.auth.ConfirmationResult

interface Inputs {
  user: RegisterDto
  passwordConfirmation?: string
}

const phoneRegExp = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/
const schema: SchemaOf<Inputs> = yup.object().shape({
  user: yup.object().shape({
    password: yup
      .string()
      .required('Chưa nhập mật khẩu.')
      .min(8, 'Mật khẩu quá ngắn - tối thiểu phải có 8 ký tự.'),
    fullName: yup.string().required('Chưa nhập Họ và tên'),
    tel: yup
      .string()
      .required('Chưa nhập số điện thoại')
      .matches(phoneRegExp, 'Số điện thoại không hợp lệ'),
    referralCode: yup.string(),
    email: yup.string(),
  }),
  passwordConfirmation: yup
    .string()
    .oneOf([yup.ref('user.password'), null], 'Mật khẩu phải trùng khớp'),
})

const Register: FC = () => {
  const setToken = useLocalStorageState('token')[1]
  const [isLoading, setIsLoading] = useRecoilState(loadingState)
  const [reCaptcha, setReCaptcha] = useState<RecaptchaVerifier>()
  const [showOTP, setShowOTP] = useState(false)
  const [final, setFinal] = useState<ConfirmationResult>(null)
  const [otp, setOtp] = useState('')
  const form = useForm<Inputs>({ mode: 'all', resolver: yupResolver(schema) })
  const {
    control,
    handleSubmit,
    setValue,
    getValues,
    formState: { errors },
  } = form
  const router = useRouter()
  const { aff } = router.query
  const captchaRef = React.useRef(null)

  useEffect(() => {
    const axiosConfig: AxiosRequestConfig = {
      baseURL: process.env.NEXT_PUBLIC_API_URL || 'http://localhost:4000/',
      timeout: 60000, // 1 phút
    }
    serviceOptions.axios = axios.create(axiosConfig)

    const tmpReCaptcha = new RecaptchaVerifier(
      'recaptcha-container',
      {
        size: 'invisible',
      },
      app
    )
    setReCaptcha(tmpReCaptcha)
  }, [])

  useEffect(() => {
    if (aff && String(aff) != '') setValue('user.referralCode', String(aff))
  }, [aff])

  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    setIsLoading(true)
    AuthService.authControllerRegister({
      body: { ...data.user, tel: data.user.tel.trim() },
    })
      .then(() => {
        signInWithPhoneNumber(
          firebase.auth,
          convertTel(data.user.tel.trim()),
          reCaptcha
        )
          .then((result) => {
            setIsLoading(false)
            setShowOTP(true)
            setFinal(result)
          })
          .catch((e) => {
            setIsLoading(false)
            alertError(e)
          })
      })
      .catch((e) => {
        setIsLoading(false)
        alertError(e)
      })
  }

  const handleChangeOtp = (value: string) => {
    setOtp(value)
    if (value.length === 6) {
      setIsLoading(true)
      final.confirm(value).then((result) => {
        result.user.getIdToken().then((idToken) => {
          AuthService.authControllerVerifyAccount({
            body: {
              idToken: idToken,
              tel: getValues('user.tel'),
            },
          }).then((response) => {
            setIsLoading(false)
            alertSuccess('Xác thực tài khoản thành công!')
            setToken('token')
            router.push('/').then()
          })
        })
      })
    }
  }

  return (
    <>
      <Head>
        <title>Đăng ký - UShare</title>
      </Head>
      <Spin spinning={isLoading} tip="Đang tải...">
        <div className={styles.loginContainer}>
          <div
            className={`flex items-center justify-center py-32 px-4 sm:px-6 lg:px-8`}
          >
            <div className="max-w-sm w-full space-y-8">
              <div id="recaptcha-container" ref={captchaRef} />
              {showOTP ? (
                <Row>
                  <Col
                    className={'p-5 rounded-lg'}
                    style={{
                      boxShadow: 'rgba(0, 0, 0, 0.1) 0px 0px 50px 0px',
                    }}
                  >
                    <Typography className={'text-2xl font-medium text-center'}>
                      Kích hoạt tài khoản
                    </Typography>
                    <Typography
                      className={'text-center text-sm m-auto mb-4'}
                      style={{ color: '#9098B1' }}
                    >
                      Kiểm tra tin nhắn điện thoại của bạn
                      <br /> Chúng tôi đã gửi mã đến số điện thoại{' '}
                      {getValues('user.tel')}
                    </Typography>
                    <div className={'text-center'}>
                      <OtpInput
                        value={otp}
                        onChange={handleChangeOtp}
                        numInputs={6}
                        isInputNum={true}
                        containerStyle={'justify-center'}
                        inputStyle={
                          'border mx-2 text-2xl rounded-md h-14 input-opt'
                        }
                      />
                    </div>
                  </Col>
                </Row>
              ) : (
                <Form
                  className="mt-8 space-y-6"
                  onFinish={handleSubmit(onSubmit)}
                >
                  <div className="py-6 text-center w-full">
                    <img
                      className={styles.logo}
                      src={'/images/ushare-logo.svg'}
                      width={'30%'}
                    />
                  </div>
                  <div className="rounded-md shadow-sm-space-y-px space-y-4">
                    <Form.Item
                      validateStatus={errors.user?.fullName && 'error'}
                      help={
                        errors.user?.fullName && errors.user?.fullName?.message
                      }
                    >
                      <Controller
                        control={control}
                        name={'user.fullName'}
                        render={({ field }) => (
                          <Input
                            {...field}
                            size="large"
                            placeholder="Họ và tên"
                            prefix={<UserOutlined />}
                          />
                        )}
                      />
                    </Form.Item>
                    {/*<Form.Item*/}
                    {/*  validateStatus={errors.user?.email && 'error'}*/}
                    {/*  help={errors.user?.email && errors.user?.email?.message}*/}
                    {/*>*/}
                    {/*  <Controller*/}
                    {/*    control={control}*/}
                    {/*    name={'user.email'}*/}
                    {/*    render={({ field }) => (*/}
                    {/*      <Input*/}
                    {/*        {...field}*/}
                    {/*        size="large"*/}
                    {/*        placeholder="Email"*/}
                    {/*        prefix={<MailOutlined />}*/}
                    {/*      />*/}
                    {/*    )}*/}
                    {/*  />*/}
                    {/*</Form.Item>*/}
                    <Form.Item
                      validateStatus={errors.user?.tel && 'error'}
                      help={errors.user?.tel && errors.user?.tel?.message}
                    >
                      <Controller
                        control={control}
                        name={'user.tel'}
                        render={({ field }) => (
                          <Input
                            {...field}
                            size="large"
                            placeholder="Số điện thoại"
                            prefix={<PhoneOutlined />}
                          />
                        )}
                      />
                    </Form.Item>
                    <Form.Item
                      validateStatus={errors.user?.password && 'error'}
                      help={
                        errors.user?.password && errors.user?.password?.message
                      }
                    >
                      <Controller
                        control={control}
                        name={'user.password'}
                        render={({ field }) => (
                          <Input.Password
                            {...field}
                            size="large"
                            placeholder="Mật khẩu"
                            prefix={<LockOutlined />}
                          />
                        )}
                      />
                    </Form.Item>
                    <Form.Item
                      validateStatus={errors.passwordConfirmation && 'error'}
                      help={
                        errors.passwordConfirmation &&
                        errors.passwordConfirmation.message
                      }
                    >
                      <Controller
                        control={control}
                        name={'passwordConfirmation'}
                        render={({ field }) => (
                          <Input.Password
                            {...field}
                            size="large"
                            placeholder="Xác nhận mật khẩu"
                            prefix={<LockOutlined />}
                          />
                        )}
                      />
                    </Form.Item>
                    <Form.Item>
                      <Controller
                        control={control}
                        name={'user.referralCode'}
                        render={({ field }) => (
                          <Input
                            {...field}
                            size="large"
                            placeholder="Mã giới thiệu"
                          />
                        )}
                      />
                    </Form.Item>
                    <Form.Item>
                      <Button
                        type="primary"
                        size={'large'}
                        htmlType={'submit'}
                        className={'w-full'}
                      >
                        Đăng ký
                      </Button>
                    </Form.Item>
                  </div>
                </Form>
              )}
            </div>
          </div>
        </div>
      </Spin>
    </>
  )
}

export default Register

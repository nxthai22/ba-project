import { Card, Checkbox } from 'antd'
import React, { FC, useState } from 'react'
import { ColumnsType } from 'antd/es/table'
import { ServiceParams } from 'utils/interfaces'
import { RoleEntity, RolesService } from 'services'
import DataTable from 'components/common/DataTable'
import Content from 'components/layout/AdminLayout/Content'
import { formatDate } from '../../../utils'
import { ROLE_TYPE } from '@/constants'

const Index: FC = () => {
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['createdAt,DESC'],
  })

  const columns: ColumnsType<RoleEntity> = [
    {
      dataIndex: 'name',
      title: 'Tên',
    },
    {
      dataIndex: 'type',
      title: 'Quyền',
      render: (value) => ROLE_TYPE[value],
    },
    {
      dataIndex: 'viewForMerchant',
      title: 'Hiển thị cho NCC',
      render: (value) => {
        return (<Checkbox defaultChecked={value} disabled />)
      },
    },

  ]
  return (
    <Content
      title={'Quản lý Nhóm quyền'}
      endButtons={[
        {
          text: 'Thêm mới',
          href: '/settings/role/create',
          type: 'primary',
          visible: true,
        },
      ]}
    >
      <Card>
        <DataTable
          rowKey={'id'}
          service={RolesService.getManyBase}
          serviceParams={tableServiceParams}
          columns={columns}
          action={['edit']}
          path={'/settings/role'}
        />
      </Card>
    </Content>
  )
}
export default Index

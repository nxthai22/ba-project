import * as yup from 'yup'
import { useRouter } from 'next/router'
import {Button, Card, Form, Input, Radio, Select} from 'antd'
import React, { FC, useEffect, useState } from 'react'
import { yupResolver } from '@hookform/resolvers/yup'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { alertError, modifyEntity } from 'utils'
import { EnumRoleEntityType, RoleEntity, RolesService } from 'services'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import Content from 'components/layout/AdminLayout/Content'
import {NOTIFICATION_RECEIVED_TYPE_ADMIN, ROLE_TYPE} from '@/constants'

interface Inputs {
  role: RoleEntity
}

const schema = yup.object().shape({
  role: yup.object().shape({
    name: yup.string().trim().required('Chưa nhập tên nhóm'),
    type: yup.string().required('Chưa chọn quyền'),
  }),
})
const Detail: FC = () => {
  const [title, setTitle] = useState('Thêm mới Nhóm quyền')
  const router = useRouter()
  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm<Inputs>({ mode: 'all', resolver: yupResolver(schema) })

  useEffect(() => {
    if (Number(router?.query?.id)) {
      setTitle('Cập nhật Nhóm quyền')
      RolesService.getOneBase({
        id: Number(router?.query?.id),
      })
        .then((response) => {
          setValue('role', response)
        })
        .catch((e) => alertError(e))
    }else{
      // set default value for viewForMerchant
      setValue('role.viewForMerchant', true)
    }
  }, [router])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    modifyEntity(RolesService, data.role, title, (response) => {
      return router.push(`/settings/role`)
    }).then()
  }

  return (
    <Content title={title} onBack={() => router.push('/settings/role')}>
      <Form
        onFinish={handleSubmit(onSubmit)}
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 12 }}
      >
        <Card>
          <Form.Item
            label={<span>Tên Nhóm</span>}
            required={true}
            validateStatus={errors.role?.name && 'error'}
            help={errors.role?.name && errors.role?.name?.message}
          >
            <Controller
              control={control}
              name={'role.name'}
              render={({ field }) => (
                <Input {...field} size="middle" placeholder="Tên Nhóm" />
              )}
            />
          </Form.Item>
          <Form.Item
            label={<span>Quyền</span>}
            required={true}
            validateStatus={errors.role?.type && 'error'}
            help={errors.role?.type && errors.role?.type?.message}
          >
            <Controller
              control={control}
              name={'role.type'}
              render={({ field }) => (
                <Select
                  {...field}
                  allowClear
                  style={{ width: '100%' }}
                  placeholder={'Chọn loại quyền'}
                  disabled={router?.query?.id !== 'create'}
                >
                  {Object.keys(EnumRoleEntityType).map((roleType) => (
                    <Select.Option
                      value={roleType}
                      key={`tbl-sl-shipping-${roleType}`}
                    >
                      {ROLE_TYPE[roleType]}
                    </Select.Option>
                  ))}
                </Select>
              )}
            />
          </Form.Item>
          <Form.Item
            label={<span>Hiển thị cho NCC</span>}
            required={false}
            validateStatus={errors.role?.viewForMerchant && 'error'}
            help={errors.role?.type && errors.role?.viewForMerchant?.message}
          >
            <Controller
              control={control}
              name={'role.viewForMerchant'}
              render={({ field }) => (
                <Radio.Group {...field}>
                  <Radio
                    value={true}
                    key={`tbl-sl-view-for-merchant-${true}`}
                  >Hiển thị
                  </Radio>
                  <Radio
                    value={false}
                    key={`tbl-sl-view-for-merchant-${false}`}
                  >Không hiển thị
                  </Radio>
                </Radio.Group>
              )}
            />
          </Form.Item>
        </Card>
        <FooterBar
          right={
            <Button htmlType={'submit'} type={'primary'}>
              Lưu
            </Button>
          }
        />
      </Form>
    </Content>
  )
}
export default Detail

import Content from 'components/layout/AdminLayout/Content'
import { Button, Card, Form, Input, Select, Table, Tabs } from 'antd'
import React, { useEffect, useState } from 'react'
import {
  BankEntity,
  BankInfo,
  BankService,
  ConfigBanner,
  ConfigService,
} from 'services'
import {
  Controller,
  SubmitHandler,
  useFieldArray,
  useForm,
} from 'react-hook-form'
import UploadMedia from 'components/common/UploadMedia'
import { useSetRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'
import RichTextEditor from 'components/common/RichTextEditor'
import { DeleteOutlined, PlusOutlined } from '@ant-design/icons'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import { alertError, alertSuccess, filterOption } from 'utils'
import FormItem from 'components/common/FormItem'

interface Inputs {
  home?: {
    id: number
    value: ConfigBanner[]
  }
  make_money?: {
    id: number
    value: ConfigBanner[]
  }
  join_community?: {
    id: number
    value: ConfigBanner[]
  }
  current_offers?: {
    id: number
    value: ConfigBanner[]
  }
  flash_sale?: {
    id: number
    value: ConfigBanner[]
  }
  bank_info?: {
    id: number
    value: BankInfo
  }
}

const ConfigKeys = [
  'home',
  'make_money',
  'join_community',
  'current_offers',
  'flash_sale',
  'bank_info',
]

const Settings = () => {
  const setIsLoading = useSetRecoilState(loadingState)
  const [currentTabKey, setCurrentTabKey] = useState('home')
  const [banks, setBanks] = useState<BankEntity[]>([])
  const { control, setValue, handleSubmit } = useForm<Inputs>({
    defaultValues: {
      home: {
        value: [],
      },
      make_money: {
        value: [],
      },
      join_community: {
        value: [],
      },
      current_offers: {
        value: [],
      },
      flash_sale: {
        value: [],
      },
      bank_info: {
        value: {},
      },
    },
  })
  const {
    fields: homeFields,
    append: homeAppend,
    remove: homeRemove,
  } = useFieldArray({
    control,
    name: 'home.value',
  })
  const {
    fields: makeMoneyFields,
    append: makeMoneyAppend,
    remove: makeMoneyRemove,
  } = useFieldArray({
    control,
    name: 'make_money.value',
  })
  const {
    fields: joinCommunityFields,
    append: joinCommunityAppend,
    remove: joinCommunityRemove,
  } = useFieldArray({
    control,
    name: 'join_community.value',
  })
  const {
    fields: currentOffersFields,
    append: currentOffersAppend,
    remove: currentOffersRemove,
  } = useFieldArray({
    control,
    name: 'current_offers.value',
  })
  const {
    fields: flashSaleFields,
    append: flashSaleAppend,
    remove: flashSaleRemove,
  } = useFieldArray({
    control,
    name: 'flash_sale.value',
  })

  useEffect(() => {
    setIsLoading(true)
    Promise.all([
      ConfigService.getManyBase({
        filter: [`key||in||${ConfigKeys.join(',')}`],
      }),
      BankService.getManyBase({
        limit: 100,
      }),
    ])
      .then(([configResponse, bankResponse]) => {
        setIsLoading(false)
        configResponse.data.forEach((config) => {
          if (ConfigKeys.includes(config.key)) {
            setValue(`${config.key}`, config)
            setValue(`${config.key}.value`, config.value)
          }
        })
        setBanks(bankResponse.data)
      })
      .catch((e) => {
        setIsLoading(false)
        alertError(e)
      })
  }, [])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    if (data[currentTabKey]) {
      setIsLoading(true)
      ConfigService.updateOneBase({
        id: data[currentTabKey].id,
        body: {
          value: data[currentTabKey].value,
        },
      })
        .then(() => {
          setIsLoading(false)
          alertSuccess(
            `Cập nhật cấu hình ${data[currentTabKey].name} thành công`
          )
        })
        .catch((e) => {
          setIsLoading(false)
          alertError(e)
        })
    }
  }

  return (
    <Content title={'Thiết lập'}>
      <Card>
        <Form onFinish={handleSubmit(onSubmit)} layout={'vertical'}>
          <Tabs
            defaultActiveKey={currentTabKey}
            onChange={(value) => setCurrentTabKey(value)}
          >
            <Tabs.TabPane tab={'Banner trang chủ'} key={'home'}>
              <Table
                pagination={false}
                dataSource={homeFields}
                columns={[
                  {
                    dataIndex: 'image',
                    title: 'Ảnh',
                    render: (value, record, index) => (
                      <Controller
                        control={control}
                        name={`home.value.${index}.image`}
                        render={({ field }) => {
                          return (
                            <UploadMedia
                              files={field.value && [field.value]}
                              multiple={false}
                              onUploaded={(value) => {
                                setValue(`home.value.${index}.image`, value)
                              }}
                            />
                          )
                        }}
                      />
                    ),
                  },
                  {
                    dataIndex: 'url',
                    title: 'Đường dẫn',
                    width: 300,
                    render: (value, record, index) => (
                      <Controller
                        control={control}
                        name={`home.value.${index}.url`}
                        render={({ field }) => <Input {...field} />}
                      />
                    ),
                  },
                  {
                    dataIndex: 'content',
                    title: 'Nội dung',
                    render: (value, record, index) => (
                      <Controller
                        control={control}
                        name={`home.value.${index}.content`}
                        render={({ field }) => <RichTextEditor {...field} />}
                      />
                    ),
                  },
                  {
                    key: 'action',
                    render: (value, record, index) => (
                      <Button onClick={() => homeRemove(index)}>
                        <DeleteOutlined />
                      </Button>
                    ),
                  },
                ]}
                footer={() => (
                  <div className={'text-center'}>
                    <Button onClick={() => homeAppend({})}>
                      <PlusOutlined /> Thêm mới
                    </Button>
                  </div>
                )}
              />
            </Tabs.TabPane>
            <Tabs.TabPane tab={'Kiếm tiền với Ushare'} key={'make_money'}>
              <Table
                pagination={false}
                dataSource={makeMoneyFields}
                columns={[
                  {
                    dataIndex: 'image',
                    title: 'Ảnh',
                    render: (value, record, index) => (
                      <Controller
                        control={control}
                        name={`make_money.value.${index}.image`}
                        render={({ field }) => (
                          <UploadMedia
                            files={field.value && [field.value]}
                            multiple={false}
                            onUploaded={(value) => {
                              setValue(`make_money.value.${index}.image`, value)
                            }}
                          />
                        )}
                      />
                    ),
                  },
                  {
                    dataIndex: 'url',
                    title: 'Đường dẫn Video',
                    width: 300,
                    render: (value, record, index) => (
                      <Controller
                        control={control}
                        name={`make_money.value.${index}.url`}
                        render={({ field }) => <Input {...field} />}
                      />
                    ),
                  },
                  {
                    dataIndex: 'content',
                    title: 'Kiểu hiển thị',
                    render: (value, record, index) => (
                      <Controller
                        control={control}
                        name={`make_money.value.${index}.type`}
                        render={({ field }) => (
                          <Select {...field} defaultValue={'video'}>
                            <Select.Option key={'video'}>Video</Select.Option>
                            <Select.Option key={'image'}>Ảnh</Select.Option>
                          </Select>
                        )}
                      />
                    ),
                  },
                  {
                    key: 'action',
                    render: (value, record, index) => (
                      <Button onClick={() => makeMoneyRemove(index)}>
                        <DeleteOutlined />
                      </Button>
                    ),
                  },
                ]}
                footer={() => (
                  <div className={'text-center'}>
                    <Button onClick={() => makeMoneyAppend({})}>
                      <PlusOutlined /> Thêm mới
                    </Button>
                  </div>
                )}
              />
            </Tabs.TabPane>
            <Tabs.TabPane
              tab={'Tham gia cộng đồng ushare'}
              key={'join_community'}
            >
              <Table
                pagination={false}
                dataSource={joinCommunityFields}
                columns={[
                  {
                    dataIndex: 'image',
                    title: 'Ảnh',
                    render: (value, record, index) => (
                      <Controller
                        control={control}
                        name={`join_community.value.${index}.image`}
                        render={({ field }) => (
                          <UploadMedia
                            files={field.value && [field.value]}
                            multiple={false}
                            onUploaded={(value) => {
                              setValue(
                                `join_community.value.${index}.image`,
                                value
                              )
                            }}
                          />
                        )}
                      />
                    ),
                  },
                  {
                    dataIndex: 'url',
                    title: 'Đường dẫn',
                    width: 300,
                    render: (value, record, index) => (
                      <Controller
                        control={control}
                        name={`join_community.value.${index}.url`}
                        render={({ field }) => <Input {...field} />}
                      />
                    ),
                  },
                  {
                    key: 'action',
                    render: (value, record, index) => (
                      <Button onClick={() => joinCommunityRemove(index)}>
                        <DeleteOutlined />
                      </Button>
                    ),
                  },
                ]}
                footer={() => (
                  <div className={'text-center'}>
                    <Button onClick={() => joinCommunityAppend({})}>
                      <PlusOutlined /> Thêm mới
                    </Button>
                  </div>
                )}
              />
            </Tabs.TabPane>
            <Tabs.TabPane tab={'Ưu đãi hiện có'} key={'current_offers'}>
              <Table
                pagination={false}
                dataSource={currentOffersFields}
                columns={[
                  {
                    dataIndex: 'image',
                    title: 'Ảnh',
                    render: (value, record, index) => (
                      <Controller
                        control={control}
                        name={`current_offers.value.${index}.image`}
                        render={({ field }) => (
                          <UploadMedia
                            files={field.value && [field.value]}
                            multiple={false}
                            onUploaded={(value) => {
                              setValue(
                                `current_offers.value.${index}.image`,
                                value
                              )
                            }}
                          />
                        )}
                      />
                    ),
                  },
                  {
                    dataIndex: 'url',
                    title: 'Đường dẫn',
                    width: 300,
                    render: (value, record, index) => (
                      <Controller
                        control={control}
                        name={`current_offers.value.${index}.url`}
                        render={({ field }) => <Input {...field} />}
                      />
                    ),
                  },
                  {
                    key: 'action',
                    render: (value, record, index) => (
                      <Button onClick={() => currentOffersRemove(index)}>
                        <DeleteOutlined />
                      </Button>
                    ),
                  },
                ]}
                footer={() => (
                  <div className={'text-center'}>
                    <Button onClick={() => currentOffersAppend({})}>
                      <PlusOutlined /> Thêm mới
                    </Button>
                  </div>
                )}
              />
            </Tabs.TabPane>
            <Tabs.TabPane tab={'Banner Deal chớp nhoáng'} key={'flash_sale'}>
              <Table
                pagination={false}
                dataSource={flashSaleFields}
                columns={[
                  {
                    dataIndex: 'image',
                    title: 'Ảnh',
                    render: (value, record, index) => (
                      <Controller
                        control={control}
                        name={`flash_sale.value.${index}.image`}
                        render={({ field }) => (
                          <UploadMedia
                            files={field.value && [field.value]}
                            multiple={false}
                            onUploaded={(value) => {
                              setValue(`flash_sale.value.${index}.image`, value)
                            }}
                          />
                        )}
                      />
                    ),
                  },
                  {
                    dataIndex: 'url',
                    title: 'Đường dẫn',
                    width: 300,
                    render: (value, record, index) => (
                      <Controller
                        control={control}
                        name={`flash_sale.value.${index}.url`}
                        render={({ field }) => <Input {...field} />}
                      />
                    ),
                  },
                  {
                    key: 'action',
                    render: (value, record, index) => (
                      <Button onClick={() => flashSaleRemove(index)}>
                        <DeleteOutlined />
                      </Button>
                    ),
                  },
                ]}
                footer={() => (
                  <div className={'text-center'}>
                    <Button onClick={() => flashSaleAppend({})}>
                      <PlusOutlined /> Thêm mới
                    </Button>
                  </div>
                )}
              />
            </Tabs.TabPane>
            <Tabs.TabPane tab={'STK Ngân hàng'} key={'bank_info'}>
              <FormItem label={'Số tài khoản'}>
                <Controller
                  control={control}
                  name={`bank_info.value.bankNumber`}
                  render={({ field }) => <Input {...field} />}
                />
              </FormItem>
              <FormItem label={'Chủ tài khoản'}>
                <Controller
                  control={control}
                  name={`bank_info.value.bankAccount`}
                  render={({ field }) => <Input {...field} />}
                />
              </FormItem>
              <FormItem label={'Ngân hàng'}>
                <Controller
                  control={control}
                  name={`bank_info.value.bankId`}
                  render={({ field }) => (
                    <Select
                      {...field}
                      filterOption={filterOption}
                      showSearch
                      allowClear
                    >
                      {banks.map((bank) => (
                        <Select.Option value={bank.id} key={`bank-${bank.id}`}>
                          {bank.name} ({bank.shortName})
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                />
              </FormItem>
              <FormItem label={'Chi nhánh'}>
                <Controller
                  control={control}
                  name={`bank_info.value.bankBranch`}
                  render={({ field }) => <Input {...field} />}
                />
              </FormItem>
              <FormItem
                label={'Email kế toán'}
                extra={
                  <>
                    Những email này sẽ nhận được Thông báo khi có CTV xác nhận
                    đã chuyển tiền
                    <br />
                    <strong>Lưu ý: </strong>Các email ngăn cách nhau bởi dấu
                    &quot;,&quot;
                  </>
                }
              >
                <Controller
                  control={control}
                  name={`bank_info.value.accountantEmails`}
                  render={({ field }) => <Input {...field} />}
                />
              </FormItem>
            </Tabs.TabPane>
          </Tabs>
          <FooterBar
            right={
              <Button htmlType={'submit'} type={'primary'}>
                Lưu
              </Button>
            }
          />
        </Form>
      </Card>
    </Content>
  )
}
export default Settings

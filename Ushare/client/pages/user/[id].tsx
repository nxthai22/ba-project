import React, { FC, useEffect, useState } from 'react'
import Content from 'components/layout/AdminLayout/Content'
import { Button, Card, Col, Form, Input, Row, Select } from 'antd'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import { Controller, SubmitHandler, useForm, useWatch } from 'react-hook-form'
import {
  BankEntity,
  BankService,
  EnumRoleEntityType,
  EnumUserEntityGender,
  EnumUserEntityStatus,
  MerchantEntity,
  MerchantsService,
  RoleEntity,
  RolesService,
  UserEntity,
  UsersService,
} from 'services'
import DatePicker from 'components/common/DatePicker'
import { alertError, filterOption, getStatusUser, modifyEntity } from 'utils'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useRouter } from 'next/router'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { authState, loadingState } from 'recoil/Atoms'
import FormItem from 'components/common/FormItem'

interface Inputs {
  user: UserEntity
  userLeaderId?: number
}
const schema_create = yup.object().shape({
  user: yup.object().shape({
    fullName: yup.string().required('Chưa nhập Họ và tên'),
    tel: yup.string().required('Chưa nhập Số điện thoại'),
    password: yup.string().required('Chưa nhập mật khẩu'),
  }),
})
const schema = yup.object().shape({
  user: yup.object().shape({
    fullName: yup.string().required('Chưa nhập Họ và tên'),
    tel: yup.string().required('Chưa nhập Số điện thoại'),
  }),
})

const UserDetail: FC = () => {
  const user = useRecoilValue(authState)
  const [title, setTitle] = useState<string>('Thông tin thành viên')
  const router = useRouter()
  const setIsLoading = useSetRecoilState(loadingState)
  const [roles, setRoles] = useState<RoleEntity[]>()
  const [banks, setBanks] = useState<BankEntity[]>()
  const [userInfo, setUserInfo] = useState<UserEntity>()
  const [merchants, setMerchants] = useState<MerchantEntity[]>([])
  const [userLeaders, setUserLeaders] = useState<UserEntity[]>([])
  const [isShowButtonSave, setIsShowButtonSave] = useState(true)
  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(
      router.query?.id === 'create' ? schema_create : schema
    ),
    defaultValues: {
      user: {
        gender: EnumUserEntityGender.other,
        status: EnumUserEntityStatus.inactive,
      },
    },
  })
  const watchRoleId = useWatch({
    control,
    name: 'user.roleId',
  })

  useEffect(() => {
    //Quyền Nhà phân phối lấy ra danh sách nhà cung cấp
    if (watchRoleId && ![1, 2].includes(watchRoleId) && user?.roleId === 1)
      MerchantsService.getManyBase({
        sort: ['createdAt,DESC'],
      })
        .then((merchantsResponse) => {
          setMerchants(merchantsResponse.data)
        })
        .catch((errors) => {
          alertError(errors)
        })
    //Quyền CTV thì lấy ra danh sách trưởng nhóm
    else if (watchRoleId === 2) {
      UsersService.getManyBase({
        // filter: [`roleId||eq||6`, `id||ne||${Number(router?.query?.id)}`],
        filter: [`roleId||eq||6`],
        sort: ['createdAt,DESC'],
      })
        .then((usersResponse) => {
          const leaders = usersResponse.data?.reduce(
            (arr: UserEntity[], curr) => {
              if (!arr.includes(curr) && curr.roleId === 6) arr.push(curr)
              return arr
            },
            []
          )
          setUserLeaders(leaders)
        })
        .catch((errors) => {
          alertError(errors)
        })
    }
  }, [watchRoleId])

  useEffect(() => {
    setIsLoading(true)
    if (router.query?.id) {
      if (router.query?.id === 'create') {
        setIsShowButtonSave(
          user.type === EnumRoleEntityType.admin ||
            user.type === EnumRoleEntityType.merchant
        )
        setTitle('Thêm mới thành viên')
      }
      Promise.all([
        router.query?.id !== 'create' &&
          UsersService.getOneBase({
            id: Number(router?.query?.id),
            join: ['leaders'],
          }),
        RolesService.getManyBase({ limit: 1000 }),
        BankService.getManyBase(),
      ])
        .then(([userResponse, roleResponse, bankResponse]) => {
          if (userResponse) {
            setIsShowButtonSave(
              (user.type === EnumRoleEntityType.merchant &&
                userResponse?.merchantId === user.merchantId) ||
                user.type === EnumRoleEntityType.admin
            )
            setValue('user', userResponse)
            if (userResponse.dob)
              setValue('user.dob', new Date(userResponse.dob))
            setValue('user.password', '')
            if (userResponse.nationalIssueDate)
              setValue(
                'user.nationalIssueDate',
                new Date(userResponse.nationalIssueDate)
              )
            setUserInfo(userResponse)
          }

          setRoles(roleResponse.data)
          setBanks(bankResponse.data)
          setValue('userLeaderId', userResponse?.leaders?.[0]?.id)
          setIsLoading(false)
        })
        .catch((error) => {
          alertError(error)
          setIsLoading(false)
        })
    }
  }, [router])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    // @ts-ignore
    data.user.leaders = data.user.leaders ? data.user.leaders : []
    data.user.leaders?.push(
      userLeaders?.find((user) => {
        if (user.id === data.userLeaderId)
          return {
            id: user.id,
          }
      })
    )
    if (data?.user?.password === '') delete data?.user?.password
    modifyEntity(
      UsersService,
      data.user,
      'Cập nhật thông tin thành viên',
      (response) => {
        return router.push(`/user`)
      }
    ).then()
  }
  return (
    <>
      <Content title={title} onBack={() => router.push('/user')}>
        <div>
          <Card>
            <Form onFinish={handleSubmit(onSubmit)}>
              <Row gutter={[16, 16]}>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem
                    label={<span>Tên thành viên</span>}
                    validateStatus={errors.user?.fullName && 'error'}
                    required={true}
                    help={
                      errors.user?.fullName && errors.user?.fullName?.message
                    }
                  >
                    <Controller
                      control={control}
                      name={'user.fullName'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          size="middle"
                          placeholder="Tên thành viên"
                          disabled={user?.roleId !== 1}
                        />
                      )}
                    />
                  </FormItem>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem
                    label={<span>Mật khẩu</span>}
                    required={router.query?.id === 'create'}
                    validateStatus={errors.user?.password && 'error'}
                    help={
                      errors.user?.password && errors.user?.password?.message
                    }
                    extra={'Nhập mật khẩu mới để đổi Mật khẩu cho thành viên'}
                  >
                    <Controller
                      control={control}
                      name={'user.password'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          type={'password'}
                          size="middle"
                          placeholder="Nhập mật khẩu mới"
                          autoComplete="new-password"
                          disabled={user?.roleId !== 1}
                        />
                      )}
                    />
                  </FormItem>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem label="Email:">
                    <Controller
                      control={control}
                      name={'user.email'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          size="middle"
                          placeholder="Email"
                          disabled={user?.roleId !== 1}
                        />
                      )}
                    />
                  </FormItem>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem
                    label="SĐT:"
                    required={true}
                    validateStatus={errors.user?.tel && 'error'}
                    help={errors.user?.tel && errors.user?.tel?.message}
                  >
                    <Controller
                      control={control}
                      name={'user.tel'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          disabled={router.query?.id !== 'create'}
                          size="middle"
                          placeholder="Số điện thoại"
                        />
                      )}
                    />
                  </FormItem>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem label={'Giới tính'}>
                    <Controller
                      name={'user.gender'}
                      control={control}
                      render={({ field }) => (
                        <Select
                          showSearch
                          placeholder="Vui lòng chọn giới tính"
                          size={'middle'}
                          {...field}
                          disabled={user?.roleId !== 1}
                        >
                          <Select.Option value={EnumUserEntityGender.other}>
                            Khác
                          </Select.Option>
                          <Select.Option value={EnumUserEntityGender.male}>
                            Nam
                          </Select.Option>
                          <Select.Option value={EnumUserEntityGender.female}>
                            Nữ
                          </Select.Option>
                        </Select>
                      )}
                    />
                  </FormItem>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem label="Ngày sinh:">
                    <Controller
                      name={'user.dob'}
                      control={control}
                      render={({ field }) => (
                        <DatePicker
                          {...field}
                          allowClear
                          format={'dd/MM/y'}
                          placeholder={'Ngày sinh'}
                          className={'w-full'}
                          disabled={user?.roleId !== 1}
                        />
                      )}
                    />
                  </FormItem>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem label="CMND/TCC:">
                    <Controller
                      control={control}
                      name={'user.nationalId'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          size="middle"
                          placeholder="Số CMT/TCC"
                          disabled={user?.roleId !== 1}
                        />
                      )}
                    />
                  </FormItem>{' '}
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem label="Ngày cấp:">
                    <Controller
                      name={'user.nationalIssueDate'}
                      control={control}
                      render={({ field }) => (
                        <DatePicker
                          {...field}
                          allowClear
                          placeholder={'Ngày cấp'}
                          format={'dd/MM/y'}
                          className={'w-full'}
                          disabled={user?.roleId !== 1}
                        />
                      )}
                    />
                  </FormItem>{' '}
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem label="Nơi cấp:">
                    <Controller
                      control={control}
                      name={'user.nationalIssueBy'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          size="middle"
                          placeholder="Nơi cấp"
                          disabled={user?.roleId !== 1}
                        />
                      )}
                    />
                  </FormItem>{' '}
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem label="Địa chỉ:">
                    <Controller
                      control={control}
                      name={'user.address'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          size="middle"
                          placeholder="Địa chỉ"
                          disabled={user?.roleId !== 1}
                        />
                      )}
                    />
                  </FormItem>{' '}
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  {/*<FormItem label='Thuộc trưởng nhóm:'>*/}
                  {/*  <Controller*/}
                  {/*    control={control}*/}
                  {/*    name={'user.address'}*/}
                  {/*    render={({ field }) =>*/}
                  {/*      <Input  {...field}  size="middle" placeholder="Địa chỉ" />}*/}
                  {/*  />*/}
                  {/*</FormItem>*/}
                  <FormItem label="Ngân hàng">
                    <Controller
                      control={control}
                      name={'user.bankId'}
                      render={({ field }) => (
                        // <Input
                        //   {...field}
                        //   size='middle'
                        //   placeholder='Tên ngân hàng'
                        // />
                        <Select
                          showSearch
                          filterOption={filterOption}
                          placeholder="Chọn ngân hàng"
                          {...field}
                          disabled={user?.roleId !== 1}
                        >
                          {banks &&
                            banks.map((bank) => (
                              <Select.Option key={bank.id} value={bank.id}>
                                {bank.shortName}
                              </Select.Option>
                            ))}
                        </Select>
                      )}
                    />
                  </FormItem>{' '}
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem label="Chi nhánh">
                    <Controller
                      control={control}
                      name={'user.bankBranch'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          size="middle"
                          placeholder="Chi nhánh"
                          disabled={user?.roleId !== 1}
                        />
                      )}
                    />
                  </FormItem>{' '}
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem label="STK">
                    <Controller
                      control={control}
                      name={'user.bankNumber'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          size="middle"
                          placeholder="Số tài khoản"
                          disabled={user?.roleId !== 1}
                        />
                      )}
                    />
                  </FormItem>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem label="Chủ tài khoản">
                    <Controller
                      control={control}
                      name={'user.bankAccountName'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          size="middle"
                          placeholder="Chủ tài khoản"
                          disabled={user?.roleId !== 1}
                        />
                      )}
                    />
                  </FormItem>
                </Col>
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem label={'Quyền'}>
                    <Controller
                      name={'user.roleId'}
                      control={control}
                      render={({ field }) => (
                        <Select
                          showSearch
                          filterOption={filterOption}
                          placeholder="Vui lòng chọn Quyền"
                          {...field}
                          disabled={user?.roleId !== 1}
                        >
                          {roles &&
                            roles.map((role) => (
                              <Select.Option key={role.id} value={role.id}>
                                {role.name}
                              </Select.Option>
                            ))}
                        </Select>
                      )}
                    />
                  </FormItem>
                </Col>
                {watchRoleId &&
                  ![1, 2].includes(watchRoleId) &&
                  user.roleId === 1 && (
                    <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                      <FormItem label={'Nhà cung cấp'}>
                        <Controller
                          name={'user.merchantId'}
                          control={control}
                          render={({ field }) => (
                            <Select
                              showSearch
                              filterOption={filterOption}
                              placeholder="Vui lòng chọn nhà cung cấp"
                              {...field}
                              disabled={user?.roleId !== 1}
                            >
                              {merchants?.map(
                                (merchantValue, merchantIndex) => (
                                  <Select.Option
                                    key={`sl-merchant-${merchantIndex}`}
                                    value={merchantValue.id}
                                  >
                                    {merchantValue.name}
                                  </Select.Option>
                                )
                              )}
                            </Select>
                          )}
                        />
                      </FormItem>
                    </Col>
                  )}
                {watchRoleId === 2 && user.roleId !== 6 && (
                  <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                    <FormItem label={'Trưởng nhóm'}>
                      <Controller
                        name={'userLeaderId'}
                        control={control}
                        render={({ field }) => (
                          <Select
                            showSearch
                            filterOption={filterOption}
                            placeholder="Vui lòng chọn trưởng nhóm"
                            {...field}
                            disabled={
                              ![
                                EnumRoleEntityType.admin,
                                EnumRoleEntityType.merchant,
                              ].includes(user.type)
                            }
                          >
                            {userLeaders?.map((userLeader) => (
                              <Select.Option
                                key={`sl-userLeader-${userLeader.id}`}
                                value={userLeader.id}
                              >
                                {userLeader.fullName}
                              </Select.Option>
                            ))}
                          </Select>
                        )}
                      />
                    </FormItem>
                  </Col>
                )}
                <Col xs={24} sm={24} md={24} lg={12} xl={8}>
                  <FormItem label={'Trạng thái'}>
                    <Controller
                      name={'user.status'}
                      control={control}
                      render={({ field }) => (
                        <Select
                          showSearch
                          filterOption={filterOption}
                          placeholder="Vui lòng chọn trạng thái"
                          {...field}
                          disabled={user?.roleId !== 1}
                        >
                          {Object.keys(EnumUserEntityStatus).map(
                            (value: EnumUserEntityStatus, index) => (
                              <Select.Option
                                key={`sl-status-${index}`}
                                value={value}
                              >
                                {getStatusUser(value)}
                              </Select.Option>
                            )
                          )}
                        </Select>
                      )}
                    />
                  </FormItem>
                </Col>
              </Row>
              <FooterBar
                right={
                  [
                    EnumRoleEntityType.merchant,
                    EnumRoleEntityType.admin,
                  ].includes(user.type) && (
                    <Button htmlType={'submit'} type={'primary'}>
                      Lưu
                    </Button>
                  )
                }
              />
            </Form>
          </Card>
        </div>
      </Content>
    </>
  )
}

export default UserDetail

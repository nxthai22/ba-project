import React, { FC, useEffect, useState } from 'react'
import { Button, Card, Col, Form, Input, Row, Select, Space, Tag } from 'antd'
import DataTable from 'components/common/DataTable'
import {
  EnumRoleEntityType,
  EnumUserEntityStatus,
  RoleEntity,
  RolesService,
  UserEntity,
  UsersService,
} from 'services'
import { ServiceParams } from 'utils/interfaces'
import { ColumnsType } from 'antd/es/table'
import {
  alertError,
  filterOption,
  formatDate,
  getStatusUser,
  hasPermission,
} from 'utils'
import Content from 'components/layout/AdminLayout/Content'
import { Controller, SubmitHandler, useForm, useWatch } from 'react-hook-form'
import { useRecoilValue } from 'recoil'
import { authState, countDataTable } from 'recoil/Atoms'

interface Inputs {
  searchText?: string
  roleId: number
}

const ListUser: FC = () => {
  const user = useRecoilValue(authState)
  const countDatatable = useRecoilValue(countDataTable)
  const [roles, setRoles] = useState<RoleEntity[]>()
  const { control, handleSubmit, reset } = useForm<Inputs>({
    defaultValues: {
      searchText: '',
    },
  })
  const watchRoleId = useWatch({
    control,
    name: 'roleId',
  })
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['createdAt,DESC'],
    join: ['role'],
  })
  const columns: ColumnsType<UserEntity> = [
    {
      dataIndex: 'id',
      title: 'Mã',
      width: 150,
    },
    {
      dataIndex: 'fullName',
      title: 'Họ tên',
    },
    {
      dataIndex: 'tel',
      title: 'SĐT',
      width: 150,
    },
    {
      dataIndex: 'role',
      title: 'Quyền',
      width: 150,
      render: (value) => value?.name,
    },
    {
      dataIndex: 'createdAt',
      title: 'Ngày đăng ký',
      render: (value) => formatDate(value),
      width: 150,
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: (value) =>
        value === EnumUserEntityStatus.active ? (
          <Tag color={'success'}>{getStatusUser(value)}</Tag>
        ) : value === EnumUserEntityStatus.inactive ? (
          <Tag color={'warning'}>{getStatusUser(value)}</Tag>
        ) : value === EnumUserEntityStatus.banned ? (
          <Tag color={'error'}>{getStatusUser(value)}</Tag>
        ) : value === EnumUserEntityStatus.deleted ? (
          <Tag color={'error'}>{getStatusUser(value)}</Tag>
        ) : null,
    },
  ]

  useEffect(() => {
    const params: any = {
      limit: 1000,
      page: 1,
    }
    if(user?.merchantId){
      params.filter = `viewForMerchant||eq||true`;
    }
    RolesService.getManyBase(params)
      .then((roleResponse) => {
        setRoles(roleResponse.data)
      })
      .catch((e) => alertError(e))
  }, [])

  useEffect(() => {
    if (watchRoleId)
      setTableServiceParams((prevState) => {
        const existRoleIdIndex = prevState.filter.findIndex((filter) =>
          filter.includes('roleId')
        )
        if (existRoleIdIndex > -1) {
          prevState.filter.splice(existRoleIdIndex, 1)
        }
        return {
          ...prevState,
          filter: [...prevState.filter, `roleId||eq||${watchRoleId}`],
        }
      })
  }, [watchRoleId])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const filter = []
    const or = []
    if (data?.searchText) {
      filter.push(`fullName||$contL||${data?.searchText}`)
      or.push(`tel||eq||${data?.searchText}`)
    }
    if (watchRoleId) {
      filter.push(`roleId||eq||${watchRoleId}`)
    }
    // else if (data?.tel) {
    //   filter.push(`tel||eq||${data?.tel}`)
    // }
    setTableServiceParams((prevState) => ({
      ...prevState,
      filter: filter,
      or: or,
    }))
  }

  const onReset = () => {
    reset()
    setTableServiceParams({
      sort: ['createdAt,DESC'],
      join: ['role'],
    })
  }

  return (
    <Content
      title={'Quản lý Thành viên'}
      endButtons={[
        {
          text: 'Thêm mới',
          href: '/user/create',
          type: 'primary',
          visible:
            user.type === EnumRoleEntityType.admin &&
            hasPermission(user, ['users_createOneBase']),
        },
      ]}
    >
      <Card>
        <Form
          autoComplete={'off'}
          onFinish={handleSubmit(onSubmit)}
          onReset={onReset}
          colon={false}
        >
          <Row gutter={[16, 16]}>
            <Col xs={24} sm={24} md={8} lg={8}>
              <Form.Item label={'Nhập tên hoặc SĐT'}>
                <Controller
                  control={control}
                  name={'searchText'}
                  render={({ field }) => <Input {...field} />}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8} lg={8}>
              <Form.Item label={'Quyền'}>
                <Controller
                  name={'roleId'}
                  control={control}
                  render={({ field }) => (
                    <Select
                      allowClear={true}
                      showSearch
                      filterOption={filterOption}
                      placeholder="Vui lòng chọn Quyền"
                      {...field}
                    >
                      {roles &&
                        roles.map((role) => (
                          <Select.Option key={role.id} value={role.id}>
                            {role?.name}
                          </Select.Option>
                        ))}
                    </Select>
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={8} className={'text-right'}>
              <Form.Item>
                <Space>
                  <Button htmlType={'reset'}>Đặt lại</Button>
                  <Button type={'primary'} htmlType={'submit'}>
                    Tìm kiếm
                  </Button>
                </Space>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>
      <Card
        title={<span className={'text-2xl'}>{countDatatable} Thành viên</span>}
      >
        <DataTable
          service={UsersService.getManyBase}
          serviceParams={tableServiceParams}
          columns={columns}
          action={['edit']}
          path={'/user'}
          // disabledAction={() => !hasPermission(user, ['users_createOneBase'])}
        />
      </Card>
    </Content>
  )
}
export default ListUser

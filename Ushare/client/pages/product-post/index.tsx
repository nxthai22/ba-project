import React, { FC, useState } from 'react'
import { Button, Card, Col, Form, Input, Row, Select, Space, Tag } from 'antd'
import DataTable from 'components/common/DataTable'
import {
  EnumProductPostEntityStatus,
  EnumRoleEntityType,
  ProductPostEntity,
  ProductPostService,
} from 'services'
import FormItem from 'components/common/FormItem'
import { ServiceParams } from 'utils/interfaces'
import { ColumnsType } from 'antd/es/table'
import { formatDate, hasPermission } from 'utils'
import Content from 'components/layout/AdminLayout/Content'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { useRecoilValue } from 'recoil'
import { authState, countDataTable } from 'recoil/Atoms'
import ShowMoreText from 'react-show-more-text'

interface Inputs {
  searchText?: string
  status?: string
}

const ListUser: FC = () => {
  const countDatatable = useRecoilValue(countDataTable)
  const { control, handleSubmit, reset } = useForm<Inputs>({
    defaultValues: {
      searchText: '',
    },
  })
  const user = useRecoilValue(authState)
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['updatedAt,DESC'],
    join: ['product'],
    ...([
      EnumRoleEntityType.management_partner,
      EnumRoleEntityType.leader,
      EnumRoleEntityType.merchant,
      EnumRoleEntityType.accountant,
    ].includes(user?.type) && {
      filter: [`product.merchantId||eq||${user.merchantId}`],
    }),
  })
  const columns: ColumnsType<ProductPostEntity> = [
    {
      dataIndex: 'content',
      title: 'Nội dung',
      width: 550,
      render: (value) => {
        return (
          <ShowMoreText
            /* Default options */
            lines={3}
            more="Xem thêm"
            less="Thu gọn"
            className="content-css"
            anchorClass="text-blue-500"
            // onClick={executeOnClick}
            expanded={false}
            width={500}
            truncatedEndingComponent={'... '}
          >
            <div
              key={`content_${value.id}`}
              dangerouslySetInnerHTML={{ __html: value }}
            />
          </ShowMoreText>
        )
      },
    },
    {
      dataIndex: 'product',
      title: 'Sản phẩm',
      render: (value) => value?.name,
    },
    {
      dataIndex: 'createdAt',
      title: 'Ngày tạo',
      render: (value) => formatDate(value),
    },
    {
      dataIndex: 'updatedAt',
      title: 'Ngày sửa',
      render: (value) => formatDate(value),
    },
    // {
    //   dataIndex: 'createdBy',
    //   title: 'Người tạo',
    //   render: value => value.fullName,
    //   width:200
    // },

    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: (value) =>
        value === EnumProductPostEntityStatus.published ? (
          <Tag color={'success'}>Hiển thị</Tag>
        ) : value === EnumProductPostEntityStatus.draf ? (
          <Tag color={'warning'}>Không hiển thị</Tag>
        ) : null,
      width: 200,
    },
  ]

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const filter = []
    const or = []
    if (data?.searchText) {
      filter.push(`content||$contL||${data?.searchText}`)
      or.push(`product.name||$contL||${data?.searchText}`)
    }
    if (data?.status) {
      filter.push(`status||eq||${data?.status}`)
    }
    setTableServiceParams((prevState) => ({
      ...prevState,
      filter: filter,
      or: or,
    }))
  }

  const onReset = () => {
    reset()
    setTableServiceParams({
      sort: ['createdAt,DESC'],
      join: ['product'],
    })
  }

  return (
    <Content
      title={'Quản lý bài đăng'}
      endButtons={[
        {
          text: 'Thêm mới',
          href: '/product-post/create',
          type: 'primary',
          visible: true,
        },
      ]}
    >
      <Card>
        <Form
          autoComplete={'off'}
          onFinish={handleSubmit(onSubmit)}
          onReset={onReset}
          colon={false}
        >
          <Row gutter={[16, 16]}>
            <Col xs={24} md={8}>
              <Form.Item label={'Nhập nội dung hoặc sản phẩm'}>
                <Controller
                  control={control}
                  name={'searchText'}
                  render={({ field }) => <Input {...field} />}
                />
              </Form.Item>
            </Col>
            <Col xs={24} md={8}>
              <Form.Item label={'Trạng thái'}>
                <Controller
                  control={control}
                  name={'status'}
                  render={({ field }) => (
                    <Select {...field}>
                      <Select.Option
                        value={EnumProductPostEntityStatus.published}
                      >
                        Hiển thị
                      </Select.Option>
                      <Select.Option value={EnumProductPostEntityStatus.draf}>
                        Không hiển thị
                      </Select.Option>
                    </Select>
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} md={8} style={{ textAlign: 'right' }}>
              <FormItem>
                <Space>
                  <Button htmlType={'reset'}>Đặt lại</Button>
                  <Button type={'primary'} htmlType={'submit'}>
                    Tìm kiếm
                  </Button>
                </Space>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Card>
      <Card
        title={<span className={'text-2xl'}>{countDatatable} Bài đăng</span>}
      >
        <DataTable
          service={ProductPostService.getManyBase}
          serviceParams={tableServiceParams}
          deleteService={ProductPostService.deleteOneBase}
          columns={columns}
          action={['edit', 'delete']}
          disabledDeleteAction={() =>
            !hasPermission(user, ['product-post_deleteOneBase'])
          }
          path={'/product-post'}
        />
      </Card>
    </Content>
  )
}
export default ListUser

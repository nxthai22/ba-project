import React, { FC, useEffect, useState } from 'react'
import Content from 'components/layout/AdminLayout/Content'
import { Button, Card, Form, Input, Switch } from 'antd'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import { Controller, SubmitHandler, useForm, useWatch } from 'react-hook-form'
import {
  EnumProductPostEntityStatus,
  EnumUpdateProductPostDtoStatus,
  ProductPostService,
  ProductsService,
  UpdateProductPostDto,
} from 'services'
import { alertError, modifyEntity } from 'utils'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { Router, useRouter } from 'next/router'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { authState, loadingState } from 'recoil/Atoms'
import UploadMedia from 'components/common/UploadMedia'
import DebounceSelect from 'components/common/DebounceSelect'

interface Inputs {
  productPost: UpdateProductPostDto
  productSelected?: {
    value: number
    label: string
  }
}

const schema = yup.object().shape({
  productPost: yup.object().shape({
    content: yup.string().required('Chưa nhập nội dung'),
  }),
})
const ProductPostDetail: FC = () => {
  const [title, setTitle] = useState('Tạo mới bài đăng')
  const [isStatus, setIsStatus] = useState<boolean>(true)
  const [merchantName, setMerchantName] = useState<string>('')
  const [catName, setCatName] = useState<string>('')
  const [fileList, setFileList] = useState<string[]>([])
  const router = useRouter()
  const user = useRecoilValue(authState)
  const setIsLoading = useSetRecoilState(loadingState)
  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors, isDirty, isSubmitSuccessful, dirtyFields },
  } = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
  })
  const leaveMessage =
    'Bạn có chắc chắc muốn rời khỏi trang khi chưa Lưu dữ liệu?'

  const watchProductSelected = useWatch({
    control,
    name: 'productSelected',
  })
  useEffect(() => {
    const routeChangeStart = (url: any) => {
      if (
        router.asPath !== url &&
        isDirty &&
        !isSubmitSuccessful &&
        !confirm(leaveMessage)
      ) {
        router.events.emit('routeChangeError')
        router.replace(router, router.asPath).then()
        throw 'Abort route change. Please ignore this error.'
      }
    }

    const beforeunload = (e: any) => {
      if (isDirty && !isSubmitSuccessful) {
        e.preventDefault()
        e.returnValue = leaveMessage
        return leaveMessage
      }
    }

    window.addEventListener('beforeunload', beforeunload)
    Router.events.on('routeChangeStart', routeChangeStart)

    return () => {
      window.removeEventListener('beforeunload', beforeunload)
      Router.events.off('routeChangeStart', routeChangeStart)
    }
  }, [isDirty, isSubmitSuccessful])

  useEffect(() => {
    if (router?.query?.id) {
      if (Number(router?.query?.id)) {
        setIsLoading(true)
        setTitle('Cập nhật bài đăng')
        ProductPostService.getOneBase({
          id: Number(router?.query?.id),
          join: ['product', 'product.productCategory'],
        })
          .then((response) => {
            setValue(
              'productPost',
              response as unknown as UpdateProductPostDto,
              {
                shouldDirty: true,
              }
            )
            setFileList(response.media)
            setIsStatus(
              response.status == EnumProductPostEntityStatus.published
            )
            if (response?.product?.id)
              setValue(
                'productSelected',
                {
                  value: response?.product?.id,
                  label: response?.product?.name,
                },
                {
                  shouldDirty: false,
                }
              )
            setCatName(response?.product?.productCategory?.name)
            setIsLoading(false)
          })
          .catch((e) => {
            alertError(e)
            setIsLoading(false)
          })
      }
    }
  }, [router?.query?.id])

  useEffect(() => {
    if (watchProductSelected?.value) {
      setIsLoading(true)
      ProductsService.getOneBase({
        id: Number(watchProductSelected.value),
        join: ['merchant', 'productCategory'],
      })
        .then((responseProduct) => {
          setMerchantName(responseProduct?.merchant?.name)
          setCatName(responseProduct?.productCategory?.name)
          setIsLoading(false)
        })
        .catch((e) => {
          alertError(e)
          setIsLoading(false)
        })
    }
  }, [watchProductSelected])

  const getProducts = (name: string) => {
    if (name !== '')
      return ProductsService.getManyBase({
        filter: [`name||$contL||${name}`],
        userId: Number(user.sub),
      }).then((response) =>
        response.data?.map((product) => ({
          label: product.name,
          value: product.id,
        }))
      )
  }

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    data.productPost = {
      id: Number(router?.query?.id),
      status: isStatus
        ? EnumUpdateProductPostDtoStatus.published
        : EnumUpdateProductPostDtoStatus.draf,
      media: fileList,
      productId: data.productSelected?.value || null,
      content: data.productPost.content,
      title: data.productPost.title,
    }
    modifyEntity(ProductPostService, data.productPost, title, () => {
      return router.push(`/product-post`)
    }).then()
  }
  const onChange = (checked) => {
    setIsStatus(checked)
  }
  return (
    <>
      <Content title={title} onBack={() => router.push('/product-post')}>
        <Form
          onFinish={handleSubmit(onSubmit)}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 16 }}
        >
          <Card>
            <Form.Item
              label={'Sản phẩm'}
              validateStatus={errors.productPost?.productId && 'error'}
              help={
                errors.productPost?.productId &&
                errors.productPost?.productId?.message
              }
            >
              <Controller
                name={'productSelected'}
                control={control}
                render={({ field }) => (
                  <DebounceSelect
                    showSearch
                    allowClear
                    placeholder={'Nhập tên sản phẩm để tìm kiếm'}
                    fetchOptions={getProducts}
                    {...field}
                    style={{
                      width: '100%',
                    }}
                  />
                )}
              />
            </Form.Item>
            <Form.Item label="Danh mục:">{catName}</Form.Item>
            <Form.Item label="Nhà cung cấp:">{merchantName}</Form.Item>
            <Form.Item label={<span>Tiêu đề</span>}>
              <Controller
                control={control}
                name={'productPost.title'}
                render={({ field }) => (
                  <Input {...field} size="middle" placeholder="Tiêu đề" />
                )}
              />
            </Form.Item>
            <Form.Item
              label={'Nội dung'}
              validateStatus={errors.productPost?.content && 'error'}
              required={true}
              help={
                errors.productPost?.content &&
                errors.productPost?.content?.message
              }
            >
              <Controller
                control={control}
                name={'productPost.content'}
                render={({ field }) => <Input.TextArea {...field} rows={10} />}
              />
            </Form.Item>
            <Form.Item label={'Trạng thái'}>
              <Switch onChange={onChange} checked={isStatus} />
            </Form.Item>
            <Form.Item label={<span>Upload ảnh/video</span>}>
              <UploadMedia files={fileList} onImageChange={setFileList} />
            </Form.Item>
          </Card>
          <FooterBar
            right={
              <Button htmlType={'submit'} type={'primary'}>
                Lưu
              </Button>
            }
          />
        </Form>
      </Content>
    </>
  )
}

export default ProductPostDetail

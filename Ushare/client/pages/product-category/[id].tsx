import React, { FC, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useSetRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'
import {
  CreateProductCategoryDto,
  EnumCreateProductCategoryDtoState,
  EnumProductCategoryAttributeEntityControlType,
  ProductCategoriesService,
  ProductCategoryEntity,
} from 'services'
import {
  Controller,
  SubmitHandler,
  useFieldArray,
  useForm,
  useWatch,
} from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { alertError, modifyEntity } from 'utils'
import Content from 'components/layout/AdminLayout/Content'
import {
  Button,
  Card,
  Col,
  Form,
  Input,
  Row,
  Select,
  Switch,
  Tag,
  Tooltip,
} from 'antd'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import UploadMedia from '../../components/common/UploadMedia'
import {
  DeleteOutlined,
  InfoCircleOutlined,
  PlusOutlined,
} from '@ant-design/icons'

interface Inputs {
  productsCategory: CreateProductCategoryDto
}

const schema = yup.object().shape({
  productsCategory: yup.object().shape({
    name: yup.string().trim().required('Chưa nhập tên danh mục'),
    productCategoryAttributes: yup.array().of(
      yup.object().shape({
        controlType: yup.string().required('Chưa chọn loại'),
        name: yup.string().trim().required('Chưa nhập tên'),
      })
    ),
  }),
})
const CategoryDetail: FC = () => {
  const [title, setTitle] = useState('Tạo mới danh mục sản phẩm')
  const [parentCategories, setParentCategories] = useState<
    ProductCategoryEntity[]
  >([])
  const [fileList, setFileList] = useState<string[]>([])
  const { TextArea } = Input
  const router = useRouter()
  const setIsLoading = useSetRecoilState(loadingState)
  const {
    control,
    handleSubmit,
    setValue,
    getValues,
    formState: { errors },
  } = useForm<Inputs>({ mode: 'all', resolver: yupResolver(schema) })

  const {
    fields: productCategoryAttributesFields,
    append: productCategoryAttributesAppend,
    remove: productCategoryAttributesRemove,
  } = useFieldArray({
    control,
    name: 'productsCategory.productCategoryAttributes',
  })

  const watchColor = useWatch({
    control,
    name: 'productsCategory.color',
  })

  useEffect(() => {
    if (router?.query?.id) {
      setIsLoading(true)

      if (router?.query?.id != 'create') setTitle('Cập nhật danh mục sản phẩm')
      const productsCategoryId = Number(router?.query?.id)
      const filter = []
      if (productsCategoryId) filter.push(`id||ne||${productsCategoryId}`)
      Promise.all([
        productsCategoryId &&
          ProductCategoriesService.getOneBase({
            id: productsCategoryId,
          }),
        ProductCategoriesService.getManyBase({
          filter: filter,
        }),
      ])
        .then(([productCategoryResponse, parentCategoriesResponse]) => {
          setParentCategories(parentCategoriesResponse.data)
          setValue(
            'productsCategory',
            productCategoryResponse as CreateProductCategoryDto
          )
          if (productCategoryResponse?.productCategoryAttributes?.length > 0) {
            productCategoryResponse?.productCategoryAttributes?.map(
              (attribute, index) => {
                attribute.units = attribute.units?.map((unit) => {
                  return {
                    id: index,
                    name: unit,
                  }
                })
              }
            )
            setValue(
              'productsCategory.productCategoryAttributes',
              productCategoryResponse.productCategoryAttributes
            )
          }
          if (productCategoryResponse?.image) {
            const tmpImages = []
            tmpImages.push(productCategoryResponse.image)
            setFileList(tmpImages)
          }
          setIsLoading(false)
        })
        .catch((error) => {
          alertError(error)
          setIsLoading(false)
        })
    }
  }, [router?.query?.id])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    if (fileList && fileList.length > 0)
      data.productsCategory.image = fileList?.[0]
    else data.productsCategory.image = null
    modifyEntity(
      ProductCategoriesService,
      data.productsCategory,
      title,
      (response) => {
        return router.push(`/product-category`)
      }
    ).then()
  }
  return (
    <>
      <Content title={title} onBack={() => router.push('/product-category')}>
        <Card>
          <Form
            labelCol={{ xs: 24, sm: 24, md: 24, xl: 4, lg: 4 }}
            wrapperCol={{ xs: 24, sm: 24, md: 24, xl: 20, lg: 20 }}
          >
            <Form.Item
              label={'Tên danh mục'}
              required={true}
              validateStatus={errors.productsCategory?.name && 'error'}
              help={
                errors.productsCategory?.name &&
                errors.productsCategory?.name?.message
              }
            >
              <Controller
                name={'productsCategory.name'}
                control={control}
                render={({ field }) => (
                  <Input {...field} placeholder={'Nhập tên danh mục'} />
                )}
              />
            </Form.Item>
            <Form.Item
              label={'Mã danh mục'}
              validateStatus={errors.productsCategory?.code && 'error'}
              help={
                errors.productsCategory?.code &&
                errors.productsCategory?.code?.message
              }
            >
              <Controller
                name={'productsCategory.code'}
                control={control}
                render={({ field }) => (
                  <Input {...field} placeholder={'Nhập mã danh mục'} />
                )}
              />
            </Form.Item>
            <Form.Item label={'Danh mục cha'}>
              <Controller
                control={control}
                name={'productsCategory.parentId'}
                render={({ field }) => (
                  <Select {...field} placeholder="Chọn danh mục cha" allowClear>
                    {parentCategories &&
                      parentCategories?.map((parent) => (
                        <Select.Option key={parent.id} value={parent.id}>
                          {parent.name}
                        </Select.Option>
                      ))}
                  </Select>
                )}
              />
            </Form.Item>
            <Form.Item label={'Chọn màu'}>
              <Controller
                name={'productsCategory.color'}
                control={control}
                render={({ field }) => (
                  <Input
                    {...field}
                    placeholder={'Chọn màu'}
                    type={'color'}
                    className={'w-24 h-12'}
                  />
                )}
              />
            </Form.Item>
            <Form.Item label={<span>Upload ảnh</span>}>
              <div
                style={{
                  backgroundColor: watchColor,
                  maxWidth: 104,
                  height: 104,
                }}
              >
                <UploadMedia
                  multiple={false}
                  files={fileList}
                  onImageChange={setFileList}
                />
              </div>
            </Form.Item>

            <Form.Item label={'Mô tả'}>
              <Controller
                name={'productsCategory.description'}
                control={control}
                render={({ field }) => (
                  <TextArea {...field} rows={4} placeholder={'Nhập mô tả'} />
                )}
              />
            </Form.Item>
            <Form.Item label={<span>Thuộc tính</span>} required={false}>
              <div className={'border px-4 pb-4'}>
                <Row gutter={[8, 8]}>
                  <Col
                    xs={24}
                    sm={12}
                    md={12}
                    xl={4}
                    lg={4}
                    className="mb-2 mt-1"
                  >
                    Lựa chọn <span className={'text-red-500'}>*</span>
                  </Col>
                  <Col
                    xs={24}
                    sm={12}
                    md={12}
                    xl={4}
                    lg={4}
                    className="mb-2 mt-1"
                  >
                    Tên thuộc tính <span className={'text-red-500'}>*</span>
                  </Col>
                  <Col
                    xs={24}
                    sm={12}
                    md={12}
                    xl={6}
                    lg={6}
                    className="mb-2 mt-1"
                  >
                    Giá trị{' '}
                    <Tooltip title="Bấm vào thêm giá trị hoặc nhắp đúp vào giá trị để thêm/sửa. Sau đó bấm ra ngoài để áp dụng">
                      <InfoCircleOutlined />
                    </Tooltip>
                  </Col>
                  <Col
                    xs={24}
                    sm={12}
                    md={12}
                    xl={6}
                    lg={6}
                    className="mb-2 mt-1"
                  >
                    Đơn vị{' '}
                    <Tooltip title="Bấm vào thêm giá trị hoặc nhắp đúp vào giá trị để thêm/sửa. Sau đó bấm ra ngoài để áp dụng">
                      <InfoCircleOutlined />
                    </Tooltip>
                  </Col>
                  <Col
                    xs={24}
                    sm={12}
                    md={12}
                    xl={2}
                    lg={2}
                    className="mb-2 mt-1 text-center"
                  >
                    Đối tác tự thêm
                  </Col>
                </Row>
                {productCategoryAttributesFields.length > 0 &&
                  productCategoryAttributesFields?.map((item, index) => (
                    <Row
                      key={`productsCategory_${index}`}
                      gutter={[8, 8]}
                      className={'border-b mb-4'}
                    >
                      <Col xs={24} sm={12} md={12} xl={4} lg={4}>
                        <Form.Item
                          validateStatus={
                            errors.productsCategory
                              ?.productCategoryAttributes?.[index]
                              ?.controlType && 'error'
                          }
                          help={
                            errors.productsCategory
                              ?.productCategoryAttributes?.[index]?.controlType
                              ?.message
                          }
                        >
                          <Controller
                            control={control}
                            name={`productsCategory.productCategoryAttributes.${index}.id`}
                            render={({ field }) => (
                              <Input {...field} type={'hidden'} />
                            )}
                          />
                          <Controller
                            control={control}
                            name={`productsCategory.productCategoryAttributes.${index}.controlType`}
                            render={({ field }) => (
                              <Select
                                allowClear
                                showSearch
                                placeholder="Chọn thuộc tính"
                                size={'middle'}
                                {...field}
                              >
                                <Select.Option
                                  value={
                                    EnumProductCategoryAttributeEntityControlType.text
                                  }
                                >
                                  Đối tác tự nhập
                                </Select.Option>
                                <Select.Option
                                  value={
                                    EnumProductCategoryAttributeEntityControlType.select
                                  }
                                >
                                  Chọn 1 giá trị có sẵn
                                </Select.Option>
                                <Select.Option
                                  value={
                                    EnumProductCategoryAttributeEntityControlType.multi_select
                                  }
                                >
                                  Chọn nhiều giá trị có sẵn
                                </Select.Option>
                              </Select>
                            )}
                          />
                        </Form.Item>
                      </Col>
                      <Col xs={24} sm={12} md={12} xl={4} lg={4}>
                        <Form.Item
                          validateStatus={
                            errors.productsCategory
                              ?.productCategoryAttributes?.[index]?.name &&
                            'error'
                          }
                          help={
                            errors.productsCategory
                              ?.productCategoryAttributes?.[index]?.name
                              ?.message
                          }
                        >
                          <Controller
                            control={control}
                            name={`productsCategory.productCategoryAttributes.${index}.name`}
                            defaultValue={''}
                            render={({ field }) => (
                              <Input
                                {...field}
                                placeholder={'Nhập tên thuộc tính'}
                              />
                            )}
                          />
                        </Form.Item>
                      </Col>
                      <Col xs={24} sm={12} md={12} xl={6} lg={6}>
                        <NestedValuesFieldArray
                          name={`productsCategory.productCategoryAttributes.${index}.productCategoryAttributeValues`}
                          {...{ control, getValues }}
                        />
                      </Col>
                      <Col xs={24} sm={12} md={12} xl={6} lg={6}>
                        <NestedValuesFieldArray
                          name={`productsCategory.productCategoryAttributes.${index}.units`}
                          {...{ control }}
                        />
                      </Col>
                      <Col
                        xs={24}
                        sm={12}
                        md={12}
                        xl={2}
                        lg={2}
                        className={'text-center'}
                      >
                        <CanAdd
                          name={`productsCategory.productCategoryAttributes.${index}.canAdd`}
                          {...{ control, setValue, getValues }}
                        />
                      </Col>
                      <Col
                        xs={24}
                        sm={2}
                        md={2}
                        xl={2}
                        lg={2}
                        className={'text-right'}
                      >
                        <Form.Item>
                          <Button
                            onClick={() =>
                              productCategoryAttributesRemove(index)
                            }
                          >
                            <DeleteOutlined /> Xóa
                          </Button>
                        </Form.Item>
                      </Col>
                    </Row>
                  ))}
                <Col className={'w-full mt-4'}>
                  <Button
                    onClick={() => productCategoryAttributesAppend({})}
                    className={'w-full'}
                  >
                    <PlusOutlined /> Thêm thuộc tính
                  </Button>
                </Col>
              </div>
            </Form.Item>
            <Form.Item label={'Hiển thị'}>
              <Controller
                control={control}
                name={`productsCategory.state`}
                render={({ field }) => (
                  <Switch
                    defaultChecked
                    {...field}
                    checked={field.value === 'active'}
                    onChange={(value) => {
                      setValue(
                        `productsCategory.state`,
                        value
                          ? EnumCreateProductCategoryDtoState.active
                          : EnumCreateProductCategoryDtoState.inActive
                      )
                    }}
                  />
                )}
              />
            </Form.Item>
            <FooterBar
              right={
                <Button type={'primary'} onClick={handleSubmit(onSubmit)}>
                  Lưu
                </Button>
              }
            />
          </Form>
        </Card>
      </Content>
    </>
  )
}

const CanAdd = ({ name, control, getValues, setValue }) => {
  const watchControlType = useWatch({
    control,
    name: name.replace('canAdd', 'controlType'),
  })
  return (
    watchControlType !== 'text' && (
      <Controller
        control={control}
        name={name}
        render={({ field }) => (
          <Switch
            defaultChecked={getValues(name)}
            {...field}
            onChange={(value) => {
              setValue(name, value)
            }}
          />
        )}
      />
    )
  )
}
const NestedValuesFieldArray: FC<any> = ({ name, control }) => {
  const [inputVisible, setInputVisible] = useState(false)
  const [inputValue, setInputValue] = useState('')
  const [editInputIndex, setEditInputIndex] = useState(-1)
  const [editInputValue, setEditInputValue] = useState('')
  const watchControlType = useWatch({
    control,
    name: name
      .replace('productCategoryAttributeValues', 'controlType')
      .replace('units', 'controlType'),
  })
  const { fields, remove, append, update } = useFieldArray({
    control,
    name,
  })

  const handleInputChange = (e) => {
    if (e.target.value.trim() !== '') setInputValue(e.target.value)
  }
  const showInput = () => {
    setInputVisible(true)
  }

  const handleInputConfirm = (event) => {
    if (event.target.value !== '') {
      append({
        name: event.target.value.trim(),
        id: 0,
      })
      setInputValue('')
      setInputVisible(false)
    }
  }

  const handleEditInputChange = (e) => {
    if (e.target.value.trim() !== '') setEditInputValue(e.target.value)
  }

  const handleEditInputConfirm = () => {
    update(editInputIndex, { id: editInputIndex, name: editInputValue })
    setEditInputIndex(-1)
    setEditInputValue('')
  }
  return (
    <>
      {fields.map((item, index) => {
        return (
          <React.Fragment
            key={`productCategoryAttributeValues-${item?.id || index}`}
          >
            <Controller
              control={control}
              name={`${name}.${index}.name`}
              render={({ field }) => {
                if (editInputIndex === index) {
                  return (
                    <Input
                      {...field}
                      // ref={saveEditInputRef}
                      // key={field.value}
                      className="tag-input mb-2 mr-2"
                      style={{ width: 95 }}
                      value={editInputValue}
                      onChange={handleEditInputChange}
                      onBlur={handleEditInputConfirm}
                      onPressEnter={handleEditInputConfirm}
                      disabled={watchControlType === 'text'}
                    />
                  )
                } else {
                  const isLongTag = field.value?.length > 20
                  return (
                    <Tag
                      {...field}
                      style={{ padding: 5 }}
                      // key={tag}
                      className={'mb-2'}
                      closable={true}
                      onClose={() => remove(index)}
                      disabled={watchControlType === 'text'}
                    >
                      <span
                        className={'text-sm'}
                        onDoubleClick={(e) => {
                          setEditInputIndex(index)
                          setEditInputValue(field.value)
                          e.preventDefault()
                        }}
                      >
                        {isLongTag
                          ? `${field.value.slice(0, 20)}...`
                          : field.value}
                      </span>
                    </Tag>
                  )
                }
              }}
            />
            <Controller
              control={control}
              name={`${name}.${index}.id`}
              render={({ field }) => <Input {...field} type={'hidden'} />}
            />
          </React.Fragment>
        )
      })}
      {inputVisible && watchControlType !== 'text' && (
        <Input
          value={inputValue}
          type="text"
          className="tag-input mb-2"
          style={{ width: 95 }}
          onChange={handleInputChange}
          onBlur={handleInputConfirm}
          onPressEnter={handleInputConfirm}
          disabled={watchControlType === 'text'}
        />
      )}
      {!inputVisible === true && watchControlType !== 'text' && (
        <Tag onClick={showInput} className="py-1 border-dashed mb-2">
          <PlusOutlined /> Thêm giá trị
        </Tag>
      )}
    </>
  )
}

export default CategoryDetail

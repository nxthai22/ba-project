import { ColumnsType } from 'antd/es/table'
import React, { FC, useState } from 'react'
import { Button, Card, Col, Form, Input, Row, Space } from 'antd'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { ServiceParams } from 'utils/interfaces'
import { ProductCategoriesService, ProductCategoryEntity } from 'services'
import Content from 'components/layout/AdminLayout/Content'
import DataTable from 'components/common/DataTable'
import FormItem from '../../components/common/FormItem'
import { useRecoilValue } from 'recoil'
import { countDataTable } from 'recoil/Atoms'
import { formatDate, getCdnFile } from 'utils'

interface Inputs {
  name: string
}

const Index: FC = () => {
  const [title] = useState<string>('Quản lý danh mục sản phẩm')
  const countDatatable = useRecoilValue(countDataTable)
  const { control, handleSubmit, reset } = useForm<Inputs>({
    defaultValues: {
      name: '',
    },
  })
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['createdAt,DESC'],
    join: ['parent'],
  })
  const columns: ColumnsType<ProductCategoryEntity> = [
    {
      dataIndex: 'name',
      title: 'Tên danh mục',
      width: 300,
      render: (value, record) => {
        return (
          <Row className={'items-center'} gutter={[8, 8]}>
            <Col
              span={4}
              className={'p-2 rounded-md'}
              style={{
                backgroundColor: record.color,
              }}
            >
              <img
                className={'items-center'}
                src={getCdnFile(record?.image)}
                width={'50px'}
              />
            </Col>
            <Col span={20}>{value}</Col>
          </Row>
        )
      },
    },
    {
      dataIndex: 'code',
      title: 'Mã danh mục',
    },
    {
      dataIndex: 'parent',
      title: 'Danh mục cha',
      width: 300,
      render: (value) => (value ? value.name : '-'),
    },
    {
      dataIndex: 'description',
      title: 'Mô tả',
      width: 500,
    },
    {
      dataIndex: 'createdAt',
      title: 'Ngày tạo',
      render: (value) => formatDate(value),
      width: 200,
    },
  ]
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const filter = []
    const or = []
    if (data?.name)
      filter.push(`name||$contL||${data?.name}`) &&
        or.push(`code||$contL||${data?.name}`)

    setTableServiceParams((prevState) => ({
      ...prevState,
      filter: filter,
      or: or,
    }))
  }
  const onReset = () => {
    reset()
    setTableServiceParams({
      sort: ['createdAt,DESC'],
    })
  }
  return (
    <Content
      title={title}
      endButtons={[
        {
          text: 'Thêm mới',
          type: 'primary',
          visible: true,
          href: '/product-category/create',
        },
      ]}
    >
      <Card>
        <Form
          autoComplete={'off'}
          onFinish={handleSubmit(onSubmit)}
          onReset={onReset}
          colon={false}
        >
          <Row gutter={[8, 8]}>
            <Col xs={24} sm={24} md={24} lg={12}>
              <Form.Item label={'Tên/mã danh mục'}>
                <Controller
                  control={control}
                  name={'name'}
                  render={({ field }) => (
                    <Input
                      {...field}
                      placeholder={'Nhập Tên hoặc mã danh mục '}
                    />
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} className={'text-right'}>
              <FormItem>
                <Space>
                  <Button htmlType={'reset'}>Đặt lại</Button>
                  <Button type={'primary'} htmlType={'submit'}>
                    Tìm kiếm
                  </Button>
                </Space>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Card>
      <Card
        title={<span className={'text-2xl'}>{countDatatable} Danh mục</span>}
      >
        <DataTable
          service={ProductCategoriesService.getManyBase}
          serviceParams={tableServiceParams}
          deleteService={ProductCategoriesService.deleteOneBase}
          columns={columns}
          action={['edit', 'delete']}
          path={'/product-category'}
        />
      </Card>
    </Content>
  )
}
export default Index

import {
  DeleteOutlined,
  PlusOutlined,
} from '@ant-design/icons'
import { yupResolver } from '@hookform/resolvers/yup'
import {
  Avatar,
  Button,
  Card,
  Col,
  Form,
  Input,
  List,
  Modal,
  Row,
  Space,
  Table,
  Tag,
  Typography,
} from 'antd'
import { ColumnsType } from 'antd/es/table'
import { TableRowSelection } from 'antd/es/table/interface'
import BasicInfo from 'components/admin/FlashSale/Product/BasicInfo'
import ModalFlashSale from 'components/admin/FlashSale/Product/ModalFlashSale'
import DataTable from 'components/common/DataTable'
import InputNumber from 'components/common/InputNumber'
import Content from 'components/layout/AdminLayout/Content'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import { StatusLabelByTime } from 'enums'
import { sortBy, uniq, uniqBy } from 'lodash'
import { useRouter } from 'next/router'
import React, { FC, useEffect, useState } from 'react'
import {
  Control,
  Controller,
  FieldError,
  SubmitHandler,
  useFieldArray,
  useForm,
} from 'react-hook-form'
import { useRecoilValue } from 'recoil'
import { authState, countDataTable } from 'recoil/Atoms'
import {
  FlashSaleDetailDto,
  FlashSaleEntity,
  FlashSaleService,
  ProductEntity,
  ProductsService,
} from 'services'
import {
  alertError,
  alertSuccess,
  formatCurrency,
  formatNumber,
  getCdnFile,
  getStatusLabelByTime,
} from 'utils'
import { ServiceParams } from 'utils/interfaces'
import * as yup from 'yup'

enum CommissionValueType {
  AMOUNT = 'amount', // Tính theo số lượng
  PERCENT = 'percent', // Tính theo phần trăm
}

const ErrorMessages = {
  yup: {
    qty: 'Chưa chọn số lượng sản phẩm được dùng cho chương trình',
  },
  product: {
    stock: 'Số lượng sản phẩm trong kho không còn. Vui lòng kiểm tra lại!',
    filter: {
      promotionPercent: 'Vui lòng nhập vào giá trị giữa 1 và 99 .',
    },
  },
  variant: {
    discountValue: 'Giá khuyến mãi phải thấp hơn giá trị niêm yết của sản phẩm',
    qty: 'Số lượng sản phẩm khuyến mãi phải từ 1 đến ',
  },
}

interface Inputs {
  flashSales: IProductInputs[]
}

interface IVariantInputs {
  id: number
  name: string
  price: number
  stockNumber: number

  // Id chương trình flash sale
  flashSaleId: number

  // Id sản phẩm thêm vào chương trình flash sale
  productId: number

  // Id biến thể thêm vào chương trình flash sale
  productVariantId?: number

  // Mức giảm giá theo giá
  discountValue: number

  // Mức giảm giá phần trăm
  discountPercentValue: number

  // Loại giảm giá
  discountValueType: string

  // Số lượng sản phẩm được dùng cho chương trình
  qty: number
}

interface IProductInputs {
  id: number
  productId: number
  price?: number
  name: string
  images: string[]
  variantInputs: IVariantInputs[]
}

const schema = yup.object().shape({
  flashSales: yup.array().of(
    yup.object().shape({
      variantInputs: yup.array().of(
        yup.object().shape({
          discountValue: yup
            .number()
            .min(0, 'Số tiền giảm giá tham gia khuyến mãi phải lớn hơn 0'),
          qty: yup
            .number()
            .nullable()
            // .min(1, 'Số lượng sản phẩm tham gia khuyến mãi phải lớn hơn 0'),
        })
      ),
    })
  ),
})

const Index: FC = () => {
  const router = useRouter()
  const { id } = router.query
  const [title] = useState<string>('Thông tin flash sale')
  const [selectedFlashSale, setSelectedFlashSale] =
    useState<FlashSaleEntity>(undefined)
  const [isChooseTimeFrameVisible, setIsChooseTimeFrameVisible] =
    useState<boolean>(false)
  const [isAddProductVisible, setIsAddProductVisible] = useState<boolean>(false)
  const [canEdit, setCanEdit] = useState<boolean>(false)
  const [productFlashSaleViews, setProductFlashSaleViews] = useState<
    IProductInputs[]
  >([])
  const [selectedProductRowKeys, setSelectedProductRowKeys] = useState<
    string[]
  >([])
  const [selectedProductVariantRowKeys, setSelectedProductVariantRowKeys] =
    useState<string[]>([])
  const {
    control,
    setValue,
    handleSubmit,
    setError,
    clearErrors,
    formState: { errors },
  } = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
    defaultValues: {
      flashSales: [],
    },
  })
  const {
    fields: productFields,
    remove: removeProductField,
    append: appendProductField,
  } = useFieldArray({
    control,
    name: `flashSales`,
  })

  useEffect(() => {
    if (Number(id) && Number(id) != 0) {
      FlashSaleService.getOneBase({
        id: Number(id),
        join: ['flashSaleDetail'],
      }).then((response) => {
        setSelectedFlashSale(response)
        if (response?.flashSaleDetail?.length > 0) {
          const tmpSelectedProductRowKeys = []
          const tmpSelectedProductVariantRowKeys = []
          response.flashSaleDetail.map((flashSaleDetail) => {
            tmpSelectedProductRowKeys.push(
              `product-${flashSaleDetail.productId}`
            )
            if (flashSaleDetail?.productVariantId) {
              tmpSelectedProductVariantRowKeys.push(
                `product-variant-${flashSaleDetail?.productVariantId}`
              )
            } else {
              tmpSelectedProductVariantRowKeys.push(
                `product-${flashSaleDetail?.productId}`
              )
            }
          })
          setSelectedProductVariantRowKeys(tmpSelectedProductVariantRowKeys)
          setSelectedProductRowKeys(tmpSelectedProductRowKeys)
        }
        const statusValue = getStatusLabelByTime(
          response.startTime,
          response.endTime
        )

        if (statusValue === StatusLabelByTime.COMMING) {
          setCanEdit(true)
        }

        getProductList(response, statusValue)
      })
    }
    if (id === 'create') {
      setCanEdit(true)
    }
  }, [id])

  const variantsViewFormater = (
    flashSaleDetail: Array<any>,
    product: ProductEntity
  ) => {
    const variants = []
    if (product.variants.length > 0) {
      product.variants.map((variant) => {
        const flashSaleVariant = flashSaleDetail.find(
          (detail) => detail.productVariantId === variant.id
        )
        if ((!canEdit && flashSaleVariant) || canEdit) {
          variants.push({
            ...variant,
            name: variant.name,
            price: variant.price,
            qty: flashSaleVariant?.quantity,
            productVariantId: variant.id,
            discountValue: flashSaleVariant?.discountValue || 0,
            stockNumber:
              variant.stock?.reduce(
                (total, { quantity }) => total + quantity,
                0
              ) || 0,
          })
        }
      })
    } else {
      const flashSaleVariant = flashSaleDetail?.find(
        (flashSaleDetailProduct) =>
          flashSaleDetailProduct.productId === product.id
      )
      if ((!canEdit && flashSaleVariant) || canEdit) {
        variants.push({
          productId: product.id,
          name: product.name,
          price: product?.rawPrice || product?.price || 0,
          qty: flashSaleVariant?.quantity,
          productVariantId: 0,
          discountValue: flashSaleVariant?.discountValue || 0,
          stockNumber:
            product.stock?.reduce(
              (total, { quantity }) => total + quantity,
              0
            ) || 0,
        })
      }
    }
    return variants
  }

  const fillDataIntoProductInputs = (
    formattedProductInputs: IProductInputs[]
  ) => {
    formattedProductInputs.forEach((productInput, productIndex) => {
      productInput.variantInputs.forEach((variantInput: any, variantIndex) => {
        const discountPercentValue = Math.floor(
          ((variantInput.price - variantInput.discountValue) /
            variantInput.price) *
            100
        )
        setValue(
          `flashSales.${productIndex}.variantInputs.${variantIndex}.discountValue`,
          variantInput.discountValue
        )
        setValue(
          `flashSales.${productIndex}.variantInputs.${variantIndex}.discountPercentValue`,
          discountPercentValue
        )
        setValue(
          `flashSales.${productIndex}.variantInputs.${variantIndex}.qty`,
          variantInput.qty
        )
        setValue(
          `flashSales.${productIndex}.variantInputs.${variantIndex}.productVariantId`,
          variantInput.productVariantId
        )
      })
    })
  }

  const getProductList = (
    selectedFlashSale,
    statusLabelByTime: StatusLabelByTime
  ) => {
    const productIds = selectedFlashSale.flashSaleDetail.map(
      ({ productId }) => productId
    )
    if (productIds.length > 0)
      ProductsService.getManyBase({
        filter: [`id||$in||${productIds.join()}`],
        join: 'variants',
      }).then((response) => {
        const formattedProductFields = response.data.map((product) => {
          return {
            id: product.id,
            productId: product.id,
            images: product.images,
            name: product.name,
            price: product?.price,
            variantInputs: variantsViewFormater(
              selectedFlashSale.flashSaleDetail,
              product
            ),
          }
        })
        if (statusLabelByTime === StatusLabelByTime.COMMING) {
          appendProductField(formattedProductFields)
          fillDataIntoProductInputs(formattedProductFields)
        } else {
          setProductFlashSaleViews(formattedProductFields)
        }
      })
  }

  const buildProductListInputs = (products: ProductEntity[]) => {
    const productInputFieldArray: IProductInputs[] = []
    products.map((product) => {
      const existProduct = productFields.find(
        (productField) => productField.productId === product.id
      )
      if (!existProduct) {
        const variants =
          product.variants && product.variants.length
            ? product.variants
            : [
                {
                  id: product.id,
                  name: product.name,
                  price: product.price,
                  stock: product.stock,
                },
              ]
        const variantInputs = variants.map((variant) => {
          const variantStockNumber =
            variant.stock?.reduce(
              (total, { quantity }) => total + quantity,
              0
            ) || 0
          return {
            id: variant.id || product.id,
            name: variant.name,
            price: variant.price,
            stockNumber: variantStockNumber,
            flashSaleId: selectedFlashSale.id,
            productId: product.id,
            productVariantId: product.variants?.length > 0 ? variant.id : null,
            discountValue: variant.price,
            discountValueType: CommissionValueType.AMOUNT,
            discountPercentValue: 0,
            qty: 0,
          }
        })
        productInputFieldArray.push({
          id: product.id,
          productId: product.id,
          name: product.name,
          images: product.images,
          price: product?.price,
          variantInputs,
        })
      }
    })
    if (productInputFieldArray.length > 0)
      appendProductField(productInputFieldArray)
  }

  const checkProductInStock = (): boolean => {
    const isStockEmpty = productFields.some((productField) =>
      productField.variantInputs.some((variantField) => {
        return !variantField.stockNumber
      })
    )

    if (isStockEmpty) {
      alertError(ErrorMessages.product.stock)
    }

    return !isStockEmpty
  }

  const validateForm = (): boolean => {
    let isValid = true

    productFields.forEach((productInput, parentIndex) => {
      productInput.variantInputs.forEach((variantInput, index) => {
        if (
          selectedProductVariantRowKeys.includes(
            variantInput.id
              ? `product-${variantInput.id}`
              : `product-variant-${variantInput.productId}`
          )
        ) {
          if (!variantInput.qty) {
            setError(`flashSales.${parentIndex}.variantInputs.${index}.qty`, {
              message: 'Số lượng sản phẩm phải lớn hơn 0',
            })
            isValid = false
          } else {
            clearErrors(`flashSales.${parentIndex}.variantInputs.${index}.qty`)
          }
          if (variantInput.discountValue === variantInput.price) {
            setError(
              `flashSales.${parentIndex}.variantInputs.${index}.discountValue`,
              {
              message: ErrorMessages.variant.discountValue,
              }
            )
            isValid = false
          } else if (variantInput.qty > variantInput.stockNumber) {
            setError(`flashSales.${parentIndex}.variantInputs.${index}.qty`, {
              message: `${ErrorMessages.variant.qty} ${variantInput.stockNumber}.`,
            })
            isValid = false
          } else {
            if (variantInput.qty > selectedFlashSale.maxQuantity) {
              setError(`flashSales.${parentIndex}.variantInputs.${index}.qty`, {
                message: `Số lượng sản phẩm lớn hơn số lượng yêu cầu của chương trình.`,
              })
              isValid = false
            } else {
              clearErrors(
                `flashSales.${parentIndex}.variantInputs.${index}.qty`
              )
            }
          }
        }
      })
    })

    return isValid
  }

  const formDataFormatter = (
    flashSales: IProductInputs[]
  ): FlashSaleDetailDto[] => {
    const formattedFormData: FlashSaleDetailDto[] = []
    flashSales.forEach((productInput) => {
      productInput.variantInputs.forEach((variantInput) => {
        if (
          variantInput.qty &&
          (selectedProductVariantRowKeys.includes(
            variantInput.productVariantId
              ? `product-variant-${variantInput.productVariantId}`
              : `product-${variantInput.productId}`
          ) ||
            selectedProductVariantRowKeys.includes(
              variantInput.productVariantId
                ? `product-variant-${variantInput.productVariantId}`
                : `product-${variantInput.productId}`
            ))
        ) {
          formattedFormData.push({
            flashSaleId: id === 'create' ? selectedFlashSale.id : Number(id),
            productId: variantInput.productId,
            productVariantId: variantInput.productVariantId,
            discountValue: variantInput.discountValue,
            discountValueType: CommissionValueType.AMOUNT,
            qty: variantInput.qty,
          })
        }
      })
    })

    return formattedFormData
  }

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    // if (!checkProductInStock()) return
    if (!validateForm()) {
      alertError('Kiểm tra khuyến mãi tại các sản phẩm đã chọn')
      return
    }
    const resquestParams = formDataFormatter(data.flashSales)
    FlashSaleService.flashSaleControllerUpdateProductInFlashSaleByMerchantId({
      id: id === 'create' ? selectedFlashSale.id : Number(id),
      body: resquestParams,
    })
      .then(() => {
        alertSuccess(`Cập nhật thông tin chương trình flash sale thành công`)
        router.push(`/flash-sale/product`).then()
      })
      .catch((error) => alertError(error))
  }

  const selectProduct: TableRowSelection<IProductInputs> = {
    onSelectAll: (selected) => {
      if (selected) {
        const tmpSelectedProductRowKeys = []
        const tmpSelectedProductVariantRowKeys = []
        productFields.forEach((row) => {
          tmpSelectedProductRowKeys.push(`product-${row.productId}`)
          row?.variantInputs.forEach((variantInput) => {
            tmpSelectedProductVariantRowKeys.push(
              variantInput?.productVariantId
                ? `product-variant-${variantInput?.productVariantId}`
                : `product-${variantInput?.productId}`
            )
          })
        })
        setSelectedProductRowKeys(tmpSelectedProductRowKeys)
        setSelectedProductVariantRowKeys(tmpSelectedProductVariantRowKeys)
      } else {
        setSelectedProductRowKeys([])
        setSelectedProductVariantRowKeys([])
      }
    },
    onSelect: (record: IProductInputs, selected: boolean) => {
      const variantRowKeys = record.variantInputs.map(
        ({ productVariantId, productId }) =>
          productVariantId
            ? `product-variant-${productVariantId}`
            : `product-${productId}`
      )
      if (selected) {
        const selectedRowKeys = uniq([
          ...selectedProductRowKeys,
          `product-${record.productId}`,
        ])
        setSelectedProductRowKeys(selectedRowKeys)
        setSelectedProductVariantRowKeys(
          uniq([...selectedProductVariantRowKeys, ...variantRowKeys])
        )
      } else {
        const selectedRowKeys = [...selectedProductRowKeys].filter(
          (id) => id !== `product-${record.productId}`
        )
        setSelectedProductRowKeys(selectedRowKeys)

        const filteredVariantRowKeys = selectedProductVariantRowKeys.filter(
          (id) => !variantRowKeys.includes(id)
        )
        setSelectedProductVariantRowKeys(filteredVariantRowKeys)
      }
    },
    getCheckboxProps: (record: IProductInputs) => ({
      id: String(record.id),
      indeterminate:
        record?.variantInputs?.some(({ productVariantId, productId }) =>
          selectedProductVariantRowKeys.includes(
            productVariantId
              ? `product-variant-${productVariantId}`
              : `product-${productId}`
          )
        ) &&
        !record?.variantInputs?.every(({ productVariantId, productId }) =>
          selectedProductVariantRowKeys.includes(
            productVariantId
              ? `product-variant-${productVariantId}`
              : `product-${productId}`
          )
        ),
    }),
    selectedRowKeys: selectedProductRowKeys,
  }

  const selectVariant: TableRowSelection<IVariantInputs> = {
    onSelect: (record: IVariantInputs, selected: boolean) => {
      const variantInputs =
        productFields.find(({ productId }) => productId === record.productId)
          ?.variantInputs || []
      if (selected) {
        const selectedRowKeys = uniq([
          ...selectedProductVariantRowKeys,
          record.productVariantId
            ? `product-variant-${record.productVariantId}`
            : `product-${record.productId}`,
        ])
        setSelectedProductVariantRowKeys(selectedRowKeys)
        const isCheckAll = variantInputs.every(
          ({ productVariantId, productId }) =>
            selectedRowKeys.includes(
              productVariantId
                ? `product-variant-${productVariantId}`
                : `product-${productId}`
            )
        )
        if (isCheckAll) {
          setSelectedProductRowKeys(
            uniq([...selectedProductRowKeys, `product-${record.productId}`])
          )
        }
      } else {
        const selectedRowKeys = [...selectedProductVariantRowKeys].filter(
          (id) =>
            id !==
            (record.productVariantId
              ? `product-variant-${record.productVariantId}`
              : `product-${record.productId}`)
        )
        setSelectedProductVariantRowKeys(selectedRowKeys)
        setSelectedProductRowKeys(
          [...selectedProductRowKeys].filter(
            (id) => id !== `product-${record.productId}`
          )
        )
      }
    },
    getCheckboxProps: (record: IVariantInputs) => {
      return {
        id: String(
          record.productVariantId
            ? `product-variant-${record.productVariantId}`
            : `product-${record.productId}`
        ),
      }
    },
    selectedRowKeys: selectedProductVariantRowKeys,
  }

  const productColumns: ColumnsType<IProductInputs> = [
    {
      title: 'Phân loại hàng',
      dataIndex: 'name',
      key: 'name',
      width: '25%',
      render: (value, record) => {
        return (
          <div className={'flex'}>
            {record?.images && record?.images.length > 0 && (
              <div className={'mr-3 w-15'}>
                <img
                  src={getCdnFile(record?.images?.[0])}
                  width={'60px'}
                  alt={value}
                />
              </div>
            )}
            <div className="flex items-center font-medium">
              <a
                href={`/product/edit/${record.productId}`}
                target={'_blank'}
                rel={'noreferrer'}
              >
                {value}
              </a>
            </div>
          </div>
        )
      },
    },
    {
      title: 'Giá gốc',
      width: '10%',
      dataIndex: 'price',
      render: (value) => formatCurrency(value),
    },
    { title: 'Giá đã giảm', width: '15%' },
    { title: 'Khuyến Mãi', width: '15%' },
    { title: 'SL khuyến mãi', width: '15%' },
    { title: 'Kho hàng', width: '10%' },
    {
      title: 'Hành động',
      width: '10%',
      align: 'center',
      render: (_value, _record, index) => (
        <Button
          icon={<DeleteOutlined onClick={() => removeProductField(index)} />}
        />
      ),
    },
  ]

  const expandedProductRowRender = (
    product: IProductInputs,
    parentIndex: number
  ) => {
    const columns: ColumnsType<IVariantInputs> = [
      { title: 'Phân loại hàng', dataIndex: 'name', width: '25%' },
      {
        title: 'Giá gốc',
        dataIndex: 'price',
        width: '10%',
        render: (value) => formatCurrency(value),
      },
      {
        title: 'Giá đã giảm',
        width: '30%',
        render: (_value, record, index) => {
          const handleOnChangeAmount = (amount: number) => {
            {
              const percent = Math.floor(
                ((record.price - amount) / record.price) * 100
              )
              setValue(
                `flashSales.${parentIndex}.variantInputs.${index}.discountValue`,
                amount
              )
              setValue(
                `flashSales.${parentIndex}.variantInputs.${index}.discountPercentValue`,
                percent
              )

              if (record.price === amount) {
                setError(
                  `flashSales.${parentIndex}.variantInputs.${index}.discountValue`,
                  {
                    message:
                      'Giá khuyến mãi phải thấp hơn giá trị niêm yết của sản phẩm',
                  }
                )
              } else {
                setError(
                  `flashSales.${parentIndex}.variantInputs.${index}.discountValue`,
                  undefined
                )
              }
            }
          }
          const handleOnChangePercent = (percent: number) => {
            const amount =
              record.price - Math.floor(record.price * (percent / 100))
            setValue(
              `flashSales.${parentIndex}.variantInputs.${index}.discountValue`,
              amount
            )
            setValue(
              `flashSales.${parentIndex}.variantInputs.${index}.discountPercentValue`,
              percent
            )
            setError(
              `flashSales.${parentIndex}.variantInputs.${index}.discountValue`,
              undefined
            )
            setError(
              `flashSales.${parentIndex}.variantInputs.${index}.discountPercentValue`,
              undefined
            )
          }

          return (
            <ProductRowRender
              name={'discount'}
              errors={errors}
              control={control}
              record={record}
              productInputIndex={parentIndex}
              variantInputIndex={index}
              onChangeAmount={(value) => handleOnChangeAmount(value)}
              onChangePercent={(value) => handleOnChangePercent(value)}
            />
          )
        },
      },
      {
        title: 'SL sản phẩm khuyến mãi',
        width: '15%',
        render: (_value, record, index) => {
          const handleOnChangeQuantity = (qty: number) => {
            if (qty === 0 || qty > record.stockNumber) {
              setError(`flashSales.${parentIndex}.variantInputs.${index}.qty`, {
                message: `${ErrorMessages.variant.qty} ${record.stockNumber}.`,
              })
            } else {
              setValue(
                `flashSales.${parentIndex}.variantInputs.${index}.qty`,
                qty
              )
              clearErrors(
                `flashSales.${parentIndex}.variantInputs.${index}.qty`
              )
            }
          }

          return (
            <ProductRowRender
              name={'promotion'}
              errors={errors}
              control={control}
              record={record}
              productInputIndex={parentIndex}
              variantInputIndex={index}
              onChangeQuantity={handleOnChangeQuantity}
            />
          )
        },
      },
      {
        title: 'Kho hàng',
        dataIndex: 'stockNumber',
        width: '10%',
        render: (value) => formatNumber(value),
      },
      { title: 'Hành động', width: '10%' },
    ]

    return (
      <Table
        rowKey={(record) =>
          record.productVariantId
            ? `product-variant-${record.productVariantId}`
            : `product-${record.productId}`
        }
        showHeader={false}
        columns={columns}
        rowSelection={selectVariant}
        dataSource={product.variantInputs}
        pagination={false}
      />
    )
  }

  const productColumnsViewMode: ColumnsType<IProductInputs> = [
    {
      title: 'Phân loại hàng',
      dataIndex: 'name',
      key: 'name',
      width: '40%',
      render: (_value, record) => (
        <Row>
          <Col className={'mr-3 w-15'}>
            {record?.images && record?.images.length > 0 && (
              <img
                src={getCdnFile(record?.images?.[0])}
                width={'60px'}
                alt={record.name}
              />
            )}
          </Col>
          <Col className="flex items-center font-medium">
            <a
              href={`/product/edit/${record.id}`}
              target={'_blank'}
              rel={'noreferrer'}
            >
              {record.name}
            </a>
          </Col>
        </Row>
      ),
    },
    {
      title: 'Giá gốc',
      width: '10%',
    },
    { title: 'Giá đã giảm', width: '10%' },
    { title: 'Khuyến Mãi', width: '10%' },
    { title: 'SL sản phẩm khuyến mãi', width: '20%' },
    { title: 'Kho hàng', width: '10%' },
  ]

  const expandedRowProductViewRender = (product: IProductInputs) => {
    const columns: ColumnsType<IVariantInputs> = [
      { dataIndex: 'name', width: '40%' },
      {
        dataIndex: 'price',
        width: '10%',
        render: (value) => formatCurrency(value),
      },
      {
        dataIndex: 'discountValue',
        width: '10%',
        render: (value) => formatCurrency(value),
      },
      {
        render: (_value, record) => {
          return (
            <Tag color={'processing'}>
              {Math.abs(
                Math.ceil(
                  ((record.discountValue - record.price) / record.price) * 100
                )
              )}
              %
            </Tag>
          )
        },
        width: '10%',
      },
      {
        dataIndex: 'qty',
        width: '20%',
      },
      {
        dataIndex: 'stockNumber',
        width: '10%',
      },
    ]

    return (
      <Table
        rowKey={(record) =>
          record.id
            ? `product-variant-${record.id}`
            : `product-${record.productId}`
        }
        showHeader={false}
        columns={columns}
        dataSource={product.variantInputs}
        pagination={false}
      />
    )
  }

  const onBulkUpdate = (data: TableFilterInputs) => {
    productFields.forEach((productInput, productIndex) => {
      productInput.variantInputs.forEach((variantInput, variantIndex) => {
        if (
          !selectedProductVariantRowKeys.includes(
            variantInput.productVariantId
              ? `product-variant-${variantInput.productVariantId}`
              : `product-${variantInput.productId}`
          )
        )
          return
        if (data.promotionPercent) {
          const price =
            variantInput.price -
            Math.floor(variantInput.price * (data.promotionPercent / 100))
          setValue(
            `flashSales.${productIndex}.variantInputs.${variantIndex}.discountValue`,
            price
          )
          setValue(
            `flashSales.${productIndex}.variantInputs.${variantIndex}.discountPercentValue`,
            data.promotionPercent
          )
        }
        if (data.promotionNumber) {
          setValue(
            `flashSales.${productIndex}.variantInputs.${variantIndex}.qty`,
            data.promotionNumber
          )
        }
      })
    })
    validateForm()
  }

  return (
    <Content title={title} onBack={() => router.push('/flash-sale/product')}>
      <Form
        onFinish={handleSubmit(onSubmit)}
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 18 }}
      >
        <BasicInfo
          canEdit={canEdit}
          selectedFlashSale={selectedFlashSale}
          onShowModalFlashSale={setIsChooseTimeFrameVisible}
        />

        <Card
          title={<ProductListTitle />}
          extra={
            canEdit ? (
              <Button
                type="primary"
                disabled={!selectedFlashSale}
                icon={<PlusOutlined />}
                onClick={() => setIsAddProductVisible(true)}
              >
                Thêm sản phẩm
              </Button>
            ) : (
              ''
            )
          }
        >
          {id && id !== 'create' && !canEdit ? (
            <Table
              rowKey={(record) =>
                record.id
                  ? `product-variant-${record.id}`
                  : `product-${record.productId}`
              }
              dataSource={productFlashSaleViews}
              columns={productColumnsViewMode}
              pagination={false}
              sticky={true}
              expandable={{
                expandedRowRender: expandedRowProductViewRender,
                defaultExpandAllRows: true,
              }}
              scroll={{
                x: 1200,
              }}
            />
          ) : (
            <>
              {productFields.length > 0 && (
                <TableFilter
                  selectedRowKeys={selectedProductVariantRowKeys}
                  onUpdate={onBulkUpdate}
                />
              )}
              <Table
                style={{ display: productFields.length > 0 ? 'block' : 'none' }}
                rowKey={(record) => `product-${record.productId}`}
                dataSource={productFields}
                columns={productColumns}
                rowSelection={selectProduct}
                pagination={false}
                sticky={true}
                expandable={{
                  expandedRowRender: expandedProductRowRender,
                  defaultExpandAllRows: true,
                }}
                scroll={{
                  x: 1500,
                }}
              />
            </>
          )}
        </Card>

        {canEdit && (
          <FooterBar
            right={
              <Button
                htmlType={'submit'}
                type={'primary'}
                disabled={!selectedFlashSale}
              >
                Lưu
              </Button>
            }
          />
        )}
      </Form>

      <ModalSelectionFlashSale
        visible={isChooseTimeFrameVisible}
        onOk={(flashSale) => {
          setSelectedFlashSale(flashSale)
          setIsChooseTimeFrameVisible(false)
        }}
        onCancel={() => setIsChooseTimeFrameVisible(false)}
      />

      <ModalSelectionProducts
        visible={isAddProductVisible}
        onOk={(products) => {
          if (products?.length > selectedFlashSale.maxProductNumber) {
            alertError(
              'Số lượng sản phẩm đã chọn vượt quá số lượng sản phẩm cho phép trong một chương trình'
            )
          } else {
            buildProductListInputs(products)
            setIsAddProductVisible(false)
          }
        }}
        onCancel={() => setIsAddProductVisible(false)}
        selectedRows={selectedProductRowKeys}
        maxProductNumber={selectedFlashSale?.maxProductNumber}
      />
    </Content>
  )
}

interface TableFilterProps {
  selectedRowKeys?: number[]
  onUpdate?: (data: TableFilterInputs) => void
  onDelete?: (selectedRowKeys: number[]) => void
}

interface TableFilterInputs {
  promotionPercent: number
  promotionNumber: number
}

interface TableFilterErrors {
  promotionPercent?: {
    message: string
  }
}

const TableFilter: FC<TableFilterProps> = ({
  selectedRowKeys = [],
  onUpdate,
  onDelete,
}) => {
  const [errors, setErrors] = useState<TableFilterErrors>({})
  const { control, setValue, getValues } = useForm<TableFilterInputs>({})

  const onApply = () => {
    if (!onValidateForm()) return

    const promotionPercent = getValues('promotionPercent')
    const promotionNumber = getValues('promotionNumber')

    if (onUpdate) {
      onUpdate({
        promotionPercent,
        promotionNumber,
      })
    }
  }

  const onValidateForm = (): boolean => {
    let isValid = true
    const promotionPercent = getValues('promotionPercent')

    if (promotionPercent === 100) {
      setErrors({
        promotionPercent: {
          message: ErrorMessages.product.filter.promotionPercent,
        },
      })
      isValid = false
    } else {
      setErrors({
        promotionPercent: undefined,
      })
    }

    return isValid
  }

  return (
    <Card>
      <Form>
        <Row gutter={20} className="mb-3">
          <Col span={6}>
            <Typography.Text strong>Chỉnh sửa hàng loạt</Typography.Text>
          </Col>
          <Col span={4}>
            <Typography.Text strong>Khuyến Mãi</Typography.Text>
          </Col>
          <Col span={4}>
            <Typography.Text strong>SL khuyến mãi</Typography.Text>
          </Col>
          <Col span={10} />
        </Row>
        <Row gutter={20}>
          <Col span={6}>
            Phân loại hàng đã chọn:{' '}
            <Typography.Text strong>{selectedRowKeys.length}</Typography.Text>
          </Col>
          <Col span={4}>
            <Form.Item
              validateStatus={errors?.promotionPercent && 'error'}
              help={errors?.promotionPercent?.message}
            >
              <Controller
                control={control}
                name="promotionPercent"
                render={({ field }) => (
                  <InputNumber
                    {...field}
                    min={0}
                    max={100}
                    addonAfter="%GIẢM"
                    onChange={(value) => {
                      setValue('promotionPercent', value as number)
                      onValidateForm()
                    }}
                  />
                )}
              />
            </Form.Item>
          </Col>
          <Col span={4}>
            <Controller
              control={control}
              name="promotionNumber"
              render={({ field }) => <InputNumber {...field} />}
            />
          </Col>
          <Col span={8} offset={2}>
            <Row>
              <Col span={12}>
                <Button
                  type="default"
                  disabled={
                    !selectedRowKeys.length ||
                    (!getValues('promotionPercent') &&
                      !getValues('promotionNumber'))
                  }
                  onClick={onApply}
                >
                  {selectedRowKeys.length
                    ? 'Cập nhật được chọn'
                    : 'Cập nhật hàng loạt'}
                </Button>
              </Col>
              {/*<Col span={12}>*/}
              {/*  <Button*/}
              {/*    danger*/}
              {/*    onClick={() => onDelete && onDelete(selectedRowKeys)}*/}
              {/*  >*/}
              {/*    Xóa*/}
              {/*  </Button>*/}
              {/*</Col>*/}
            </Row>
          </Col>
        </Row>
      </Form>
    </Card>
  )
}

const ProductListTitle: FC = () => {
  return (
    <>
      <Typography.Text>Sản phẩm tham gia Flash Sale</Typography.Text>
      <Typography.Text type="secondary" className="block text-sm">
        Vui lòng kiểm tra tiêu chí sản phẩm trước khi thêm sản phẩm vào chương
        trình khuyến mãi của bạn.
      </Typography.Text>
    </>
  )
}

interface IModalSelectionFlashSaleProps {
  visible: boolean
  onOk?: (flashSale: FlashSaleEntity) => void
  onCancel?: () => void
}

/**
 * Selection flash sale modal
 * @param visible Whether the dialog is visible
 * @param onOk Return selected flash sale
 * @param onCancel invisible dialog
 * @returns JSX
 */
const ModalSelectionFlashSale: FC<IModalSelectionFlashSaleProps> = ({
  visible,
  onOk,
  onCancel,
}) => {
  const [flashSales, setFlashSales] = useState<FlashSaleEntity[]>([])
  const [selectedFlashSaleId, setSelectedFlashSaleId] = useState<number>(0)

  // Get flash sales
  const fetchFlashSales = (startTime: Date = new Date()) => {
    Promise.all([
      FlashSaleService.getManyBase({
        limit: 100,
        sort: ['name,ASC'],
        filter: [`startTime||$gt||${new Date(startTime).toISOString()}`],
      }),
      FlashSaleService.getManyBase({
        limit: 100,
        sort: ['name,ASC'],
        filter: [`startTime||$gt||${new Date(startTime).toISOString()}`],
        join: ['flashSaleDetail'],
      }),
    ])
      .then(([response, responseCheck]) => {
        const flashSalesJoined = responseCheck.data?.filter(
          (data) => data.flashSaleDetail?.length > 0
        )
        const flashSales = sortBy(
          response.data?.filter(
            (data) =>
              !flashSalesJoined?.find((fl) => fl.id === data.id) &&
              new Date(data.startTime).getDate() ===
                new Date(startTime).getDate() &&
              new Date(data.startTime).getMonth() ===
                new Date(startTime).getMonth() &&
              new Date(data.startTime).getFullYear() ===
                new Date(startTime).getFullYear()
          ),
          'startTime'
        )
        setFlashSales(flashSales)
      })
      .catch((error) => alertError(error))
  }

  const callback = () => {
    if (selectedFlashSaleId) {
      const flashSale = flashSales.find(({ id }) => id === selectedFlashSaleId)
      onOk(flashSale)
    }
  }

  useEffect(() => {
    if (visible) fetchFlashSales()
  }, [visible])

  return (
    <>
      <ModalFlashSale
        visible={visible}
        onOk={onOk}
        onCancel={onCancel}
        callback={callback}
        fetchFlashSales={fetchFlashSales}
        flashSales={flashSales}
        onSelectFlashSale={setSelectedFlashSaleId}
        selectedFlashSaleId={selectedFlashSaleId}
      />
    </>
  )
}

enum EnumProductRowRender {
  discount = 'discount',
  promotion = 'promotion',
}

interface IProductRowRenderProps {
  name: keyof typeof EnumProductRowRender
  errors: {
    flashSales?: {
      variantInputs?: {
        discountValue?: FieldError
        qty?: FieldError
      }[]
    }[]
  }
  control: Control<Inputs>
  record: IVariantInputs
  productInputIndex: number
  variantInputIndex: number
  onChangeAmount?: (price: number) => void
  onChangePercent?: (percent: number) => void
  onChangeQuantity?: (percent: number) => void
}

const ProductRowRender: FC<IProductRowRenderProps> = ({
  name,
  errors,
  control,
  record,
  productInputIndex,
  variantInputIndex,
  onChangeAmount,
  onChangePercent,
  onChangeQuantity,
}) => {
  switch (name) {
    case EnumProductRowRender.discount:
      return (
        <Form.Item
          required={true}
          validateStatus={
            errors.flashSales?.[productInputIndex]?.variantInputs?.[
              variantInputIndex
            ]?.discountValue?.message && 'error'
          }
          help={
            errors.flashSales?.[productInputIndex]?.variantInputs?.[
              variantInputIndex
            ]?.discountValue?.message
          }
          style={{ marginBottom: 0 }}
          wrapperCol={{ span: 24 }}
        >
          <Row>
            <Col span={11}>
              <Controller
                key={`discount-productVariantId-${record.id}`}
                control={control}
                name={`flashSales.${productInputIndex}.variantInputs.${variantInputIndex}.productVariantId`}
                render={({ field }) => <Input type={'hidden'} {...field} />}
              />
              <Controller
                key={`discount-amount-${record.id}`}
                control={control}
                name={`flashSales.${productInputIndex}.variantInputs.${variantInputIndex}.discountValue`}
                render={({ field }) => (
                  <InputNumber
                    {...field}
                    min={0}
                    max={record.price}
                    disabled={!record.stockNumber}
                    style={{ width: 150 }}
                    addonBefore="₫"
                    onChange={(value) =>
                      onChangeAmount && onChangeAmount(Number(value))
                    }
                  />
                )}
              />
            </Col>
            <Col span={4}>
              <span>Hoặc</span>
            </Col>
            <Col span={9}>
              <Controller
                key={`discount-percent-${record.id}`}
                control={control}
                name={`flashSales.${productInputIndex}.variantInputs.${variantInputIndex}.discountPercentValue`}
                render={({ field }) => (
                  <InputNumber
                    {...field}
                    disabled={!record.stockNumber}
                    defaultValue={0}
                    min={0}
                    max={100}
                    style={{ width: 100 }}
                    addonAfter="%"
                    onChange={(value) =>
                      onChangePercent && onChangePercent(Number(value))
                    }
                  />
                )}
              />
            </Col>
          </Row>
        </Form.Item>
      )
    case EnumProductRowRender.promotion:
      return (
        <Form.Item
          required={true}
          validateStatus={
            errors.flashSales?.[productInputIndex]?.variantInputs?.[
              variantInputIndex
            ]?.qty?.message && 'error'
          }
          help={
            errors.flashSales?.[productInputIndex]?.variantInputs?.[
              variantInputIndex
            ]?.qty?.message
          }
          style={{ marginBottom: 0 }}
        >
          <Controller
            key={`stock-number-${record.id}`}
            control={control}
            name={`flashSales.${productInputIndex}.variantInputs.${variantInputIndex}.qty`}
            render={({ field }) => (
              <InputNumber
                {...field}
                disabled={!record.stockNumber}
                max={record.stockNumber}
                onChange={(value) =>
                  onChangeQuantity && onChangeQuantity(Number(value))
                }
              />
            )}
          />
        </Form.Item>
      )

    default:
      return <></>
  }
}

interface IModalSelectionProductProps {
  visible: boolean
  onOk?: (selectedProducts: ProductEntity[]) => void
  onCancel?: () => void
  selectedRows: string[]
  maxProductNumber: number
}

interface IModalSelectionProductInputs {
  searchText?: string
}

const ModalSelectionProducts: FC<IModalSelectionProductProps> = ({
  visible,
  onOk,
  onCancel,
  selectedRows,
  maxProductNumber,
}: IModalSelectionProductProps) => {
  const user = useRecoilValue(authState)
  const countDatatable = useRecoilValue(countDataTable)
  const [selectedProducts, setSelectedProducts] = useState<ProductEntity[]>([])
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>()

  const [tableProductServiceParams, setTableProductServiceParams] =
    useState<ServiceParams>({
      sort: ['createdAt,DESC'],
      filter: [`merchantId||$eq||${user.merchantId}`],
      join: ['variants'],
    })

  const tableProductColumns: ColumnsType<ProductEntity> = [
    {
      dataIndex: 'name',
      title: 'sản phẩm',
      width: 300,
      render: (value, record) => {
        return (
          <List.Item key={record.id}>
            <List.Item.Meta
              avatar={<Avatar src={record.images?.[0]} />}
              title={<a>{record.name}</a>}
              description={record.shortDescription}
            />
          </List.Item>
        )
      },
    },
  ]

  const {
    control: productControl,
    setValue: setProductValue,
    handleSubmit: handleSubmitSearchProducts,
  } = useForm<IModalSelectionProductInputs>({
    defaultValues: {
      searchText: '',
    },
  })
  const [form] = Form.useForm()

  const onSubmitProductForm: SubmitHandler<IModalSelectionProductInputs> = (
    data
  ) => {
    const filter = []
    if (data?.searchText) {
      filter.push(`name||$contL||${data?.searchText}`)
    }
    filter.push(`merchantId||$eq||${user.merchantId}`)

    setTableProductServiceParams((prevState) => ({
      ...prevState,
      filter: filter,
      join: ['variants'],
    }))
  }

  const onResetSearchProducts = () => {
    setProductValue('searchText', '')
    setTableProductServiceParams({
      filter: [`merchantId||$eq||${user.merchantId}`],
      sort: ['createdAt,DESC'],
      join: ['variants'],
    })
  }

  useEffect(() => {
    if (visible) {
      setSelectedProducts([])
    }
    setSelectedRowKeys(selectedRows)
  }, [visible])

  return (
    <>
      <Modal
        title="Chọn Sản Phẩm"
        width={'960px'}
        visible={visible}
        onOk={() => onOk && onOk(selectedProducts)}
        onCancel={() => onCancel && onCancel()}
      >
        <Card>
          <Form
            form={form}
            autoComplete={'off'}
            onFinish={handleSubmitSearchProducts(onSubmitProductForm)}
            onReset={onResetSearchProducts}
          >
            <Row gutter={[16, 16]}>
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form.Item label={'Nhập tên'}>
                  <Controller
                    control={productControl}
                    name={'searchText'}
                    render={({ field }) => (
                      <Input {...field} placeholder="Nhập tên sản phẩm" />
                    )}
                  />
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} className={'text-right'}>
                <Form.Item>
                  <Space>
                    <Button htmlType={'reset'}>Đặt lại</Button>
                    <Button type={'primary'} htmlType={'submit'}>
                      Tìm kiếm
                    </Button>
                  </Space>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>

        {user.merchantId && (
          <Card
            title={
              <span className={'text-2xl'}>{countDatatable} sản phẩm</span>
            }
            extra={`Đã chọn ${selectedRowKeys?.length}/${maxProductNumber} sản phẩm`}
          >
            <DataTable
              service={ProductsService.getManyBase}
              serviceParams={tableProductServiceParams}
              columns={tableProductColumns}
              rowKey={(record) => `product-${record.id}`}
              rowSelection={{
                type: 'checkbox',
                onSelectAll: (selected, selectedRows, changeRows) => {
                  if (selected) {
                    setSelectedProducts((prevState) =>
                      uniqBy([...prevState, ...selectedRows], 'id')
                    )
                    setSelectedRowKeys((prevState) =>
                      uniq([
                        ...prevState,
                        ...selectedRows.map((row) => `product-${row.id}`),
                      ])
                    )
                  } else {
                    setSelectedProducts((prevState) => {
                      changeRows.map((row) => {
                        const index = prevState.findIndex(
                          (product) => product.id === row.id
                        )
                        if (index > -1) {
                          prevState.splice(index, 1)
                        }
                      })

                      return prevState
                    })
                    setSelectedRowKeys((prevState) => {
                      changeRows.map((row) => {
                        const index = prevState.findIndex(
                          (key) => key === `product-${row.id}`
                        )
                        if (index > -1) {
                          prevState.splice(index, 1)
                        }
                      })
                      return prevState
                    })
                  }
                },
                onSelect: (record, selected) => {
                  if (selected) {
                    setSelectedProducts((prevState) =>
                      uniqBy([...prevState, record], 'id')
                    )
                    setSelectedRowKeys((prevState) =>
                      uniq([...prevState, `product-${record.id}`])
                    )
                  } else {
                    setSelectedProducts((prevState) => {
                      const index = prevState.findIndex(
                        (product) => product.id === record.id
                      )
                      if (index > -1) {
                        prevState.splice(index, 1)
                      }
                      return prevState
                    })
                    setSelectedRowKeys((prevState) => {
                      const index = prevState.findIndex(
                        (key) => key === `product-${record.id}`
                      )
                      if (index > -1) {
                        prevState.splice(index, 1)
                      }
                      return prevState
                    })
                  }
                },
                onChange: (
                  selectedRowKeys: React.Key[],
                  selectedRows: ProductEntity[]
                ) => {
                  setSelectedProducts((prevState) =>
                    uniqBy([...prevState, ...selectedRows], 'id')
                  )
                  setSelectedRowKeys((prevState) =>
                    uniq([...prevState, ...selectedRowKeys])
                  )
                },
                selectedRowKeys: selectedRowKeys,
                getCheckboxProps: (record) => ({
                  disabled: selectedRows.find(
                    (key) => Number(key.split('-')[1]) === record.id
                  ),
                }),
              }}
              scroll={{ y: 300 }}
            />
          </Card>
        )}
      </Modal>
    </>
  )
}

export default Index

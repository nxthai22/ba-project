import { EditOutlined, EyeOutlined } from '@ant-design/icons'
import { Button, Card, Col, Form, Row, Select, Space, Tag, Tooltip } from 'antd'
import { ColumnsType } from 'antd/es/table'
import DatePicker from 'components/common/DatePicker'
import Link from 'components/common/Link'
import Content from 'components/layout/AdminLayout/Content'
import { StatusLabelByTime } from 'enums'
import { FC, useState } from 'react'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { useRecoilValue } from 'recoil'
import { countDataTable } from 'recoil/Atoms'
import { FlashSaleEntity, FlashSaleService } from 'services'
import { formatDate, getStatusLabelByTime } from 'utils'
import { ServiceParams } from 'utils/interfaces'
import DataTable from 'components/common/DataTable'

interface Inputs {
  searchRangeTimeValue?: [Date, Date]
  status?: string
}

const Index: FC = () => {
  const [title] = useState<string>('Sản phẩm flash sale')
  const countDatatable = useRecoilValue(countDataTable)
  const { control, handleSubmit, reset } = useForm<Inputs>({})
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['createdAt,DESC'],
    join: ['flashSaleDetail'],
  })

  const columns: ColumnsType<FlashSaleEntity> = [
    {
      title: 'Khung giờ',
      width: 400,
      render: (_value, record) => {
        return (
          <span>
            {formatDate(record.startTime, 'HH:mm dd/MM/yyyy')} -{' '}
            {formatDate(record.endTime, 'HH:mm dd/MM/yyyy')}
          </span>
        )
      },
    },
    {
      dataIndex: 'flashSaleDetail',
      title: 'Sản Phẩm',
      render: (value) => `Số sản phẩm tham gia ${value?.length}`,
    },
    {
      title: 'Trạng thái',
      render: (_value, record) => {
        const statusValue = getStatusLabelByTime(
          record.startTime,
          record.endTime
        )

        if (statusValue === StatusLabelByTime.INPROGRESS) {
          return <Tag color={'processing'}>{StatusLabelByTime.INPROGRESS}</Tag>
        }

        if (statusValue === StatusLabelByTime.COMMING) {
          return <Tag color={'volcano'}>{StatusLabelByTime.COMMING}</Tag>
        }

        return <Tag color={'default'}>{StatusLabelByTime.OVER}</Tag>
      },
    },
    {
      title: 'Thao tác',
      key: 'operation',
      align: 'center',
      render: (_, record) => {
        const statusValue = getStatusLabelByTime(
          record.startTime,
          record.endTime
        )

        return (
          <Space>
            <Link href={`/flash-sale/product/${record.id}`}>
              <Button
                shape={'circle'}
                icon={
                  statusValue === StatusLabelByTime.COMMING ? (
                    <Tooltip title={'Sửa'}>
                      <EditOutlined />
                    </Tooltip>
                  ) : (
                    <Tooltip title={'Xem'}>
                      <EyeOutlined />
                    </Tooltip>
                  )
                }
              />
            </Link>
          </Space>
        )
      },
    },
  ]

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const filter = []
    if (data?.searchRangeTimeValue?.length) {
      filter.push(
        `startTime||$gt||${new Date(
          data.searchRangeTimeValue[0]
        ).toISOString()}}`,
        `endTime||$lt||${new Date(data.searchRangeTimeValue[1]).toISOString()}`
      )
    }
    if (data?.status) {
      switch (data.status) {
        case 'coming':
          filter.push(`startTime||$gte||${new Date().toISOString()}}`)
          break
        case 'inprogress':
          filter.push(
            `startTime||$lte||${new Date().toISOString()}}`,
            `endTime||$gte||${new Date().toISOString()}`
          )
          break
        case 'over':
          filter.push(`endTime||$lte||${new Date().toISOString()}}`)
          break
      }
    }
    setTableServiceParams((prevState) => ({
      ...prevState,
      filter: filter,
    }))
  }

  const onReset = () => {
    reset()
    setTableServiceParams({
      sort: ['createdAt,DESC'],
      join: ['flashSaleDetail'],
    })
  }

  return (
    <Content
      title={title}
      endButtons={[
        {
          text: 'Thêm mới',
          href: '/flash-sale/product/create',
          type: 'primary',
          visible: true,
        },
      ]}
    >
      <Card>
        <Form
          autoComplete={'off'}
          onFinish={handleSubmit(onSubmit)}
          onReset={onReset}
          colon={false}
        >
          <Row gutter={[16, 16]}>
            <Col xs={24} md={8}>
              <Form.Item label={'Khung giờ'}>
                <Controller
                  control={control}
                  name={'searchRangeTimeValue'}
                  render={({ field }) => (
                    <DatePicker.RangePicker
                      {...field}
                      showTime
                      allowClear={true}
                      format="YYYY-MM-DD HH:mm"
                    />
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} md={8}>
              <Form.Item label={'Trạng thái'}>
                <Controller
                  control={control}
                  name={'status'}
                  render={({ field }) => (
                    <Select {...field} allowClear>
                      <Select.Option value={'coming'}>
                        Sắp diễn ra
                      </Select.Option>
                      <Select.Option value={'inprogress'}>
                        Đang diễn ra
                      </Select.Option>
                      <Select.Option value={'over'}>Đã kết thúc</Select.Option>
                    </Select>
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} md={8} className={'text-right'}>
              <Form.Item>
                <Space>
                  <Button htmlType={'reset'}>Đặt lại</Button>
                  <Button type={'primary'} htmlType={'submit'}>
                    Tìm kiếm
                  </Button>
                </Space>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>

      <Card
        title={<span className={'text-2xl'}>{countDatatable} Sản phẩm</span>}
      >
        <DataTable
          service={FlashSaleService.getManyBase}
          serviceParams={tableServiceParams}
          columns={columns}
        />
      </Card>
    </Content>
  )
}

export default Index

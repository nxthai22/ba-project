import { Button, Card, Col, Form, Input, Row, Select, Space, Tag } from 'antd'
import { ColumnsType } from 'antd/es/table'
import DataTable from 'components/common/DataTable'
import Content from 'components/layout/AdminLayout/Content'
import { FC, useState } from 'react'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { FlashSaleEntity, FlashSaleService } from 'services'
import { formatDate, getStatusLabelByTime, hasPermission } from 'utils'
import { ServiceParams } from 'utils/interfaces'
import { useRecoilValue } from 'recoil'
import { authState } from 'recoil/Atoms'

export enum StatusLabelByTime {
  INPROGRESS = 'Đang diễn ra',
  COMMING = 'Sắp diễn ra',
  OVER = 'Đã kết thúc',
}

interface Inputs {
  searchText?: string
  status?: string
}

const Index: FC = () => {
  const user = useRecoilValue(authState)
  const [title] = useState<string>('Flash sale')
  const { control, handleSubmit, reset } = useForm<Inputs>({
    defaultValues: {
      searchText: '',
    },
  })
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['createdAt,DESC'],
    join: ['flashSaleDetail'],
  })

  const columns: ColumnsType<FlashSaleEntity> = [
    {
      dataIndex: 'id',
      title: 'ID',
      width: 150,
    },
    {
      dataIndex: 'name',
      title: 'Tên',
    },
    {
      title: 'Trạng thái',
      render: (_value, record) => {
        const statusValue = getStatusLabelByTime(
          record.startTime,
          record.endTime
        )

        if (statusValue === StatusLabelByTime.INPROGRESS) {
          return <Tag color={'processing'}>{StatusLabelByTime.INPROGRESS}</Tag>
        }

        if (statusValue === StatusLabelByTime.COMMING) {
          return <Tag color={'volcano'}>{StatusLabelByTime.COMMING}</Tag>
        }

        return <Tag color={'default'}>{StatusLabelByTime.OVER}</Tag>
      },
    },
    {
      dataIndex: 'startTime',
      title: 'Bắt đầu',
      render: (value) => formatDate(value),
    },
    {
      dataIndex: 'endTime',
      title: 'Kết thúc',
      render: (value) => formatDate(value),
    },
    {
      dataIndex: 'maxProductNumber',
      title: 'Số sản phẩm tham gia',
    },
    {
      dataIndex: 'maxQuantity',
      title: 'Số lượng khuyến mãi',
    },
  ]

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const filter = []
    const or = []
    if (data?.searchText) {
      filter.push(`name||$contL||${data?.searchText}`)
    }
    if (data?.status) {
      const now = new Date().toISOString()
      switch (data.status) {
        case 'comming':
          filter.push(`startTime||$gt||${now}`)
          break
        case 'over':
          filter.push(`endTime||$lt||${now}`)
          break
        case 'inprogress':
          filter.push(`startTime||$lt||${now}`)
          filter.push(`endTime||$gt||${now}`)
          break
      }
    }
    setTableServiceParams((prevState) => ({
      ...prevState,
      filter: filter,
      or: or,
    }))
  }

  const onReset = () => {
    reset()
    setTableServiceParams({
      sort: ['createdAt,DESC'],
    })
  }

  return (
    <Content
      title={title}
      endButtons={[
        {
          text: 'Thêm mới',
          href: '/flash-sale/create',
          type: 'primary',
          visible: hasPermission(user, ['flash-sale_createOneBase']),
        },
      ]}
    >
      <Card>
        <Form
          autoComplete={'off'}
          onFinish={handleSubmit(onSubmit)}
          onReset={onReset}
          colon={false}
        >
          <Row gutter={[16, 16]}>
            <Col xs={24} sm={24} md={24} lg={8}>
              <Form.Item label={'Tên chương trình'}>
                <Controller
                  control={control}
                  name={'searchText'}
                  render={({ field }) => (
                    <Input
                      {...field}
                      placeholder="Nhập tên chương trình cần tìm kiếm"
                    />
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={8}>
              <Form.Item label={'Trạng thái'}>
                <Controller
                  control={control}
                  name={'status'}
                  render={({ field }) => (
                    <Select
                      {...field}
                      placeholder="Nhập tên chương trình cần tìm kiếm"
                    >
                      <Select.Option value={'comming'}>
                        Sắp diễn ra
                      </Select.Option>
                      <Select.Option value={'inprogress'}>
                        Đang diễn ra
                      </Select.Option>
                      <Select.Option value={'over'}>Đã kết thúc</Select.Option>
                    </Select>
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={8} className={'text-right'}>
              <Form.Item>
                <Space>
                  <Button htmlType={'reset'}>Đặt lại</Button>
                  <Button type={'primary'} htmlType={'submit'}>
                    Tìm kiếm
                  </Button>
                </Space>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>

      <Card>
        <DataTable
          service={FlashSaleService.getManyBase}
          serviceParams={tableServiceParams}
          columns={columns}
          action={['edit']}
          path={'/flash-sale'}
          disabledAction={(record: FlashSaleEntity) => {
            const statusValue = getStatusLabelByTime(
              record.startTime,
              record.endTime
            )
            const disabledStatus = [
              StatusLabelByTime.OVER,
              StatusLabelByTime.INPROGRESS,
            ]
            return disabledStatus.includes(statusValue)
          }}
        />
      </Card>
    </Content>
  )
}

export default Index

import React, { FC, useEffect, useState } from 'react'
import Content from 'components/layout/AdminLayout/Content'
import {
  EnumFilterRevenueWithPagingField,
  MerchantEntity,
  MerchantsService,
  RevenueService,
  RevenueStatisticResponse,
} from 'services'
import { Controller, SubmitHandler, useForm, useWatch } from 'react-hook-form'
import { ColumnsType } from 'antd/es/table'
import {
  Button,
  Card,
  Col,
  Form,
  Input,
  Row,
  Select,
  Space,
  Table,
  Tooltip,
} from 'antd'
import DatePicker from '../../components/common/DatePicker'
import { endOfMonth, format, getQuarter, getYear, startOfMonth } from 'date-fns'
import { DownloadOutlined } from '@ant-design/icons'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { authState, loadingState } from 'recoil/Atoms'
import {
  alertError,
  filterOption,
  formatCurrency,
  formatDate,
  formatNumber,
} from 'utils'
import { flatten } from 'lodash'
import ExcelJS from 'exceljs'

interface Inputs {
  searchText?: string
  searchTimesType?: string
  date?: [Date, Date]
  merchantId?: number
  quarter: Date
  year: Date
  field: EnumFilterRevenueWithPagingField
}

const Index: FC = () => {
  const user = useRecoilValue(authState)
  const setIsLoading = useSetRecoilState(loadingState)
  const [title] = useState<string>('Thống kê chi tiết doanh thu CTV')
  const [revenueReportDetailsData, setRevenueReportDetailsData] = useState<
    RevenueStatisticResponse[]
  >([])
  const [merchants, setMerchants] = useState<MerchantEntity[]>([])
  const [isDownloading, setIsDownloading] = useState<boolean>(false)
  const [tableServiceParams, setTableServiceParams] = useState<any>({
    fromDate: startOfMonth(new Date()).toISOString(),
    toDate: startOfMonth(new Date()).toISOString(),
    limit: 10,
    page: 1,
    field: EnumFilterRevenueWithPagingField.fullName,
  })
  const { control, handleSubmit, reset, getValues } = useForm<Inputs>({
    defaultValues: {
      searchText: '',
      date: [new Date(), new Date()],
      field: EnumFilterRevenueWithPagingField.fullName,
    },
  })
  const watchSearchTimesType = useWatch({
    control,
    name: 'searchTimesType',
    defaultValue: 'month',
  })
  const [columns, setColumns] = useState<ColumnsType>([
    {
      dataIndex: 'user',
      title: 'Cộng tác viên',
      render: (value) => value?.fullName,
    },
    {
      dataIndex: 'user',
      title: 'SĐT CTV',
      render: (value) => value?.tel,
    },
    {
      dataIndex: 'totalCommissionRevenue',
      title: 'Tổng chiết khấu (VNĐ)',
      render: (value) => formatNumber(value),
    },
  ])
  useEffect(() => {
    user?.roleId === 1 &&
      MerchantsService.getManyBase({
        limit: 0,
        sort: ['name,ASC'],
      })
        .then((merchantsResponse) => {
          setMerchants(merchantsResponse?.data)
        })
        .catch((e) => {
          alertError(e)
        })
  }, [])
  useEffect(() => {
    setIsLoading(true)
    RevenueService.revenueControllerCalculateRevenueStatisticDetail(
      tableServiceParams
    )
      .then((responseData) => {
        setIsLoading(false)
        responseData.data = responseData.data.map((userData) => {
          let columnData = {}
          userData.comissions.map((monthData) => {
            Object.keys(monthData).map((key) => {
              if (key !== 'date') {
                columnData = {
                  ...columnData,
                  ...{
                    [`${key}_${monthData.commissionTimeType}_${format(
                      new Date(monthData.date),
                      'MMyyyy'
                    )}`]: monthData[key],
                  },
                }
              }
            })
            // columnData = {...columnData, []}
          })
          return { ...userData, ...columnData }
        })
        setRevenueReportDetailsData(responseData?.data)
      })
      .catch((e) => {
        setIsLoading(false)
        alertError(e)
      })
  }, [tableServiceParams])
  useEffect(() => {
    if (revenueReportDetailsData.length > 0) {
      const createColumnMonth = []
      createColumnMonth.push(
        revenueReportDetailsData?.[0]?.comissions?.map((dateValue) => {
          return {
            title: `${
              dateValue.commissionTimeType === 'month' ? 'Tháng' : 'Quý'
            } ${
              dateValue.commissionTimeType === 'month'
                ? format(new Date(dateValue.date), 'MM/yyyy')
                : getQuarter(new Date(dateValue.date)) +
                  '/' +
                  getYear(new Date(dateValue.date))
            } `,
            children: [
              {
                title: 'Doanh số (VNĐ)',
                dataIndex: `totalRevenueCompleted_${
                  dateValue.commissionTimeType
                }_${format(new Date(dateValue.date), 'MMyyyy')}`,
                align: 'center',
                render: (value) => formatNumber(value),
              },
              {
                title: 'Tỉ lệ chiết khấu (%)',
                dataIndex: `commissionValue_${
                  dateValue.commissionTimeType
                }_${format(new Date(dateValue.date), 'MMyyyy')}`,
                align: 'center',
                render: (value) => formatNumber(value),
              },
              {
                title: 'Chiết khấu (VNĐ)',
                dataIndex: `totalCommissionCompleted_${
                  dateValue.commissionTimeType
                }_${format(new Date(dateValue.date), 'MMyyyy')}`,
                align: 'center',
                render: (value) => formatNumber(value),
              },
            ],
          }
        })
      )
      setColumns((prevState) => [...prevState, ...flatten(createColumnMonth)])
    }
  }, [revenueReportDetailsData])
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    setRevenueReportDetailsData([])
    setColumns([
      {
        dataIndex: 'user',
        title: 'Cộng tác viên',
        render: (value) => value?.fullName,
      },
      {
        dataIndex: 'user',
        title: 'SĐT CTV',
        render: (value) => value?.tel,
      },
      {
        dataIndex: 'totalCommissionRevenue',
        title: 'Tổng chiết khấu (VNĐ)',
      },
    ])
    let fromDate = '',
      toDate = '',
      textSearch = ''
    Object.keys(data).map((key) => {
      if (data?.[key]) {
        if (key === 'date') {
          if (data?.date) {
            fromDate = startOfMonth(startOfMonth(data?.date[0])).toISOString()
            toDate = startOfMonth(data?.date[1]).toISOString()
          }
        }
        if (key === 'searchText') {
          textSearch = data?.[key]
        }
      }
    })
    setTableServiceParams({
      fromDate: fromDate,
      toDate: toDate,
      textSearch: textSearch,
      merchantId: getValues('merchantId'),
      field: getValues('field'),
    })
  }
  const onReset = () => {
    reset()
    setRevenueReportDetailsData([])
    setColumns([
      {
        dataIndex: 'user',
        title: 'Cộng tác viên',
        render: (value) => value?.fullName,
      },
      {
        dataIndex: 'user',
        title: 'SĐT CTV',
        render: (value) => value?.tel,
      },
      {
        dataIndex: 'totalCommissionRevenue',
        title: 'Tổng chiết khấu (VNĐ)',
      },
    ])
    setTableServiceParams({
      fromDate: startOfMonth(new Date()).toISOString(),
      toDate: startOfMonth(new Date()).toISOString(),
      limit: 10,
      page: 1,
      field: EnumFilterRevenueWithPagingField.fullName,
    })
  }
  const onDownload = () => {
    const tableService = {
      ...tableServiceParams,
      limit: Number.MAX_SAFE_INTEGER,
      page: 1,
    }
    setIsDownloading(true)
    const merchantId = getValues('merchantId')
    const dates = getValues('date')
    Promise.all([
      merchantId &&
        MerchantsService.getOneBase({
          id: merchantId,
          join: ['ward', 'ward.district', 'ward.district.province'],
        }),
      RevenueService.revenueControllerCalculateRevenueStatisticDetail(
        tableService
      ),
    ])
      .then(([merchantResponse, response]) => {
        setIsDownloading(false)
        if (!response || response.data?.length < 1) {
          alertError('Không có dữ liệu')
          return
        }
        // @ts-ignore
        const workbook = new ExcelJS.Workbook()
        const worksheet = workbook.addWorksheet('Sheet 1')

        const filename = `Doanh thu CTV - ${format(new Date(), 'd_m_Y_h_i_s')}`
        /* Thông tin nhà cung cấp */
        worksheet.addRow(['', merchantResponse?.name || ''])
        worksheet.getRow(1).font = {
          bold: true,
          size: 12,
          name: 'Times New Roman',
        }
        worksheet.getRow(1).alignment = {
          horizontal: 'left',
          vertical: 'middle',
        }
        worksheet.addRow(['', merchantResponse?.address || ''])
        worksheet.addRow(['', merchantResponse?.tel || ''])
        const font = { size: 12, name: 'Times New Roman' }
        worksheet.getRow(1).font = font
        worksheet.getRow(1).alignment = {
          horizontal: 'left',
          vertical: 'middle',
        }
        worksheet.getRow(2).font = font
        worksheet.getRow(2).alignment = {
          horizontal: 'left',
          vertical: 'middle',
        }
        /* Tiêu đề báo cáo*/
        worksheet.mergeCells('B4:AI4')
        worksheet.getCell('AI4').font = {
          bold: true,
          size: 18,
          name: 'Times New Roman',
        }
        worksheet.getCell('AI4').alignment = {
          horizontal: 'left',
          vertical: 'middle',
        }
        worksheet.getCell('AI4').value = 'TỔNG HỢP CHIẾT KHẤU CỘNG TÁC VIÊN'
        worksheet.mergeCells('B5:AI5')
        worksheet.getCell('AI5').font = {
          size: 12,
          name: 'Times New Roman',
          italic: true,
        }
        worksheet.getCell('AI5').alignment = {
          horizontal: 'left',
          vertical: 'middle',
        }
        worksheet.getCell('AI5').value = `Từ ngày: ${formatDate(
          startOfMonth(dates?.[0]),
          'dd/MM/yyyy'
        )} đến ngày: ${formatDate(endOfMonth(dates?.[1]), 'dd/MM/yyyy')}`
        worksheet.addRow('')

        /* Logo */
        // const imageLogoId = workbook.addImage({
        //   buffer: fs.readFileSync('../../public/to.logo.png'),
        //   extension: 'png',
        // })
        // worksheet.mergeCells('A1:A6')
        // worksheet.addImage(imageLogoId, 'A1:A6')
        /* Header */
        const headerRow1 = [
          'STT',
          'CTV',
          'Sđt CTV',
          'Tên NH',
          'STK',
          'Tên người nhận',
          'Hình thức chuyển',
          'Tỉnh/TP',
          'Nội dung',
          'Tổng chiết khấu (vnđ)',
        ]
        const headerRow2 = [
          'STT',
          'CTV',
          'Sđt CTV',
          'Tên NH',
          'STK',
          'Tên người nhận',
          'Hình thức chuyển',
          'Tỉnh/TP',
          'Nội dung',
          'Tổng chiết khấu (vnđ)',
        ]
        response.data?.[0]?.comissions?.map((commission, index) => {
          switch (commission.commissionTimeType) {
            case 'month':
              headerRow1.push(
                `Tháng ${formatDate(new Date(commission.date), 'MM/yyyy')}`
              )
              break
            case 'quarter':
              headerRow1.push(
                `Quý ${getQuarter(new Date(commission.date))}/${getYear(
                  new Date(commission.date)
                )}`
              )
              break
          }
          headerRow1.push('')
          headerRow1.push('')
          headerRow2.push(
            ...['Doanh số (vnđ)', 'Tỉ lệ chiết khấu (%)', 'Chiết khấu (vnđ)']
          )
        })
        worksheet.addRow(headerRow1)
        worksheet.addRow(headerRow2)
        worksheet.mergeCells('A7:A8')
        worksheet.mergeCells('B7:B8')
        worksheet.mergeCells('C7:C8')
        worksheet.mergeCells('D7:D8')
        worksheet.mergeCells('E7:E8')
        worksheet.mergeCells('F7:F8')
        worksheet.mergeCells('G7:G8')
        worksheet.mergeCells('H7:H8')
        worksheet.mergeCells('I7:I8')
        worksheet.mergeCells('J7:J8')

        for (let i = 11; i <= headerRow1.length; i = i + 3) {
          worksheet.mergeCells(7, i, 7, i + 2)
          worksheet.getCell(7, i).fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: '92D050' },
          }
          worksheet.getCell(7, i).alignment = {
            horizontal: 'center',
            vertical: 'middle',
          }
          worksheet.getCell(7, i).border = {
            top: { style: 'thin' },
            left: { style: 'thin' },
            bottom: { style: 'thin' },
            right: { style: 'thin' },
          }
        }
        const indexColumnQuarters = []
        for (let i = 1; i <= headerRow2.length; i++) {
          // worksheet.getCell(8, i).font = {
          //   color: { argb: 'FFFFFF' },
          // }
          if (headerRow1[i]?.includes('Quý')) indexColumnQuarters.push(i + 1)
          worksheet.getCell(8, i).fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: '0070C0' },
          }
          worksheet.getCell(8, i).alignment = {
            horizontal: 'center',
            vertical: 'middle',
          }
          worksheet.getCell(8, i).border = {
            top: { style: 'thin' },
            left: { style: 'thin' },
            bottom: { style: 'thin' },
            right: { style: 'thin' },
          }
        }
        indexColumnQuarters?.map((idx) => {
          worksheet.getCell(7, idx).fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: 'FFC599' },
          }
        })
        worksheet.getCell(7, 10).fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: 'FFFF00' },
        }
        // worksheet.getRows(7, 2).map((row) => {
        //   row.font = {
        //     size: 12,
        //     name: 'Times New Roman',
        //     bold: true,
        //   }
        //   row.alignment = {
        //     horizontal: 'center',
        //     vertical: 'middle',
        //   }
        //   row.border = {
        //     top: { style: 'thin' },
        //     left: { style: 'thin' },
        //     bottom: { style: 'thin' },
        //     right: { style: 'thin' },
        //   }
        //   row.fill = {
        //     type: 'pattern',
        //     pattern: 'solid',
        //     fgColor: { argb: 'FBDAD7' },
        //   }
        // })

        response.data?.map((record, index) => {
          const row = []
          /* STT */
          row.push(index + 1)
          /* Tên CTV */
          row.push(record.user?.fullName)
          /* Sđt CTV */
          row.push(record.user?.tel)
          /* Tên NH */
          row.push(record.user?.bank?.name)
          /* STK */
          row.push(record.user?.bankNumber)
          /* Tên người nhận */
          row.push(record.user?.bankAccountName)
          /* Hình thức chuyển */
          row.push('')
          /* Tỉnh/TP */
          row.push(record.user?.ward?.district?.province?.name)
          /* Nội dung */
          row.push('')
          /* Tổng chiết khấu */
          row.push(formatCurrency(record.totalCommissionRevenue))
          /* Dữ liệu theo tháng/quý */
          record.comissions?.map((commission) => {
            row.push(formatCurrency(commission.totalRevenueCompleted))
            row.push(commission.commissionValue)
            row.push(formatCurrency(commission.totalCommissionCompleted))
          })
          worksheet.addRow(row).border = {
            top: { style: 'thin' },
            left: { style: 'thin' },
            bottom: { style: 'thin' },
            right: { style: 'thin' },
          }
        })

        workbook.xlsx.writeBuffer().then((xls64) => {
          const a = document.createElement('a')
          const data = new Blob([xls64], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
          })

          const url = URL.createObjectURL(data)
          a.href = url
          a.download = `${filename} - ${formatDate(new Date())}`
          document.body.appendChild(a)
          a.click()
          setTimeout(function () {
            document.body.removeChild(a)
            window.URL.revokeObjectURL(url)
          }, 200)
        })
      })
      .catch((e) => {
        setIsDownloading(false)
        alertError(e)
      })
  }
  return (
    <>
      <Content title={title}>
        <Card>
          <Form
            autoComplete={'off'}
            onFinish={handleSubmit(onSubmit)}
            onReset={onReset}
            layout="vertical"
          >
            <Row gutter={[8, 8]}>
              <Col xs={24} sm={24} md={24} xl={8} lg={8}>
                <Form.Item label={'Tìm kiếm theo'}>
                  <Input.Group compact>
                    <Controller
                      control={control}
                      name={'field'}
                      render={({ field }) => (
                        <Select
                          {...field}
                          defaultValue="fullName"
                          style={{ width: '30%' }}
                        >
                          <Select.Option value="fullName">
                            Tên CTV
                          </Select.Option>
                          <Select.Option value="tel">Sđt CTV</Select.Option>
                          <Select.Option value="bankAccountName">
                            Người nhận
                          </Select.Option>
                        </Select>
                      )}
                    />

                    <Controller
                      control={control}
                      name={'searchText'}
                      render={({ field }) => (
                        <Input
                          {...field}
                          placeholder={'Nhập thông tin'}
                          style={{ width: '70%' }}
                        />
                      )}
                    />
                  </Input.Group>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={24} xl={6} lg={6}>
                <Form.Item label={'Khoảng thời gian'}>
                  <Input.Group compact>
                    <Controller
                      control={control}
                      name={'searchTimesType'}
                      render={({ field }) => (
                        <Select
                          defaultValue="month"
                          style={{ width: '30%' }}
                          {...field}
                        >
                          <Select.Option value="month">Tháng</Select.Option>
                          {/*<Select.Option value="quarter">Quý</Select.Option>*/}
                          {/*<Select.Option value="year">Năm</Select.Option>*/}
                        </Select>
                      )}
                    />
                    {watchSearchTimesType === 'month' ? (
                      <Controller
                        control={control}
                        name={'date'}
                        render={({ field }) => (
                          <DatePicker.RangePicker
                            {...field}
                            format={'MM/YYYY'}
                            picker="month"
                            style={{
                              width: '70%',
                            }}
                          />
                        )}
                      />
                    ) : watchSearchTimesType === 'quarter' ? (
                      <Controller
                        control={control}
                        name={'quarter'}
                        render={({ field }) => (
                          <DatePicker
                            {...field}
                            picker={'quarter'}
                            format={'QQ/YYYY'}
                            style={{
                              width: '70%',
                            }}
                          />
                        )}
                      />
                    ) : (
                      <Controller
                        control={control}
                        name={'year'}
                        render={({ field }) => (
                          <DatePicker.YearPicker
                            {...field}
                            format={'YYYY'}
                            style={{
                              width: '70%',
                            }}
                          />
                        )}
                      />
                    )}
                  </Input.Group>
                </Form.Item>
              </Col>
              {user.roleId === 1 && (
                <Col xs={24} sm={24} md={24} xl={6} lg={6}>
                  <Form.Item label={'Nhà cung cấp'}>
                    <Controller
                      name={'merchantId'}
                      control={control}
                      render={({ field }) => (
                        <Select
                          showSearch
                          filterOption={filterOption}
                          placeholder="Vui lòng chọn nhà cung cấp"
                          allowClear
                          {...field}
                        >
                          {merchants &&
                            merchants.map((m) => (
                              <Select.Option
                                key={`merchant_${m.id}`}
                                value={m.id}
                              >
                                {m.name}
                              </Select.Option>
                            ))}
                        </Select>
                      )}
                    />
                  </Form.Item>
                </Col>
              )}
              <Col
                xs={24}
                sm={24}
                md={24}
                xl={4}
                lg={4}
                className={'text-right'}
              >
                <Form.Item label={<></>}>
                  <Space>
                    <Button htmlType={'reset'}>Đặt lại</Button>
                    <Button type={'primary'} htmlType={'submit'}>
                      Tìm kiếm
                    </Button>
                  </Space>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
        <Card
          title={title}
          extra={
            <Tooltip title={'Tải về'}>
              <Button
                type={'primary'}
                loading={isDownloading}
                onClick={onDownload}
              >
                <DownloadOutlined />
              </Button>
            </Tooltip>
          }
        >
          <Table
            scroll={{ x: 1300 }}
            size={'small'}
            columns={columns}
            dataSource={revenueReportDetailsData}
          />
        </Card>
      </Content>
    </>
  )
}

export default Index

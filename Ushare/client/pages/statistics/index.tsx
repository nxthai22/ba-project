import React, { FC, useEffect, useState } from 'react'
import Content from 'components/layout/AdminLayout/Content'
import {
  DetailRefrralStatistic,
  RevenueReferralStatistic,
  RevenueService,
} from 'services'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { ColumnsType } from 'antd/es/table'
import {
  Button,
  Card,
  Col,
  Form,
  Input,
  Modal,
  Row,
  Space,
  Table,
  Tooltip,
} from 'antd'
import DatePicker from '../../components/common/DatePicker'
import FormItem from '../../components/common/FormItem'
import { format, startOfMonth } from 'date-fns'
import { DownloadOutlined } from '@ant-design/icons'
import { useSetRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'
import { alertError, formatCurrency, formatDate } from 'utils'
import ExcelJS from 'exceljs'

interface Inputs {
  searchText?: string
  date?: [Date, Date]
}

const Index: FC = () => {
  const setIsLoading = useSetRecoilState(loadingState)
  const [title] = useState<string>('Thống kê hoa hồng thành viên')
  const [isReportDetailVisible, setIsReportDetailVisible] = useState(false)
  const [
    calculateReferralCommissionReportData,
    setCalculateReferralCommissionReportData,
  ] = useState<RevenueReferralStatistic[]>([])
  const [detailRefrralStatisticData, setDetailRefrralStatisticData] = useState<
    DetailRefrralStatistic[]
  >([])
  const [userId, setUserId] = useState(0)
  const [fullName, setFullName] = useState('')
  const [monthDetail, setMonthDetail] = useState('')
  const [isDownloading, setIsDownloading] = useState<boolean>(false)
  const [tableServiceParams, setTableServiceParams] = useState<any>({
    fromDate: startOfMonth(new Date()).toISOString(),
    toDate: startOfMonth(new Date()).toISOString(),
    limit: 10,
    page: 1,
  })
  const { control, handleSubmit, reset } = useForm<Inputs>({
    defaultValues: {
      searchText: '',
      date: [new Date(), new Date()],
    },
  })

  const defaultColumns: ColumnsType<RevenueReferralStatistic> = [
    {
      dataIndex: 'referral',
      title: 'Cộng tác viên',
      render: (value) => value?.fullName,
      width: 150,
    },
    {
      dataIndex: 'members',
      title: 'Số TV đã giới thiệu',
      align: 'center',
      width: 100,
    },
    {
      title: 'Tổng hoa hồng (VNĐ)',
      dataIndex: 'totalCommission',
      width: 150,
      render: (value) => formatCurrency(value),
    },
  ]

  const [columns, setColumns] =
    useState<ColumnsType<RevenueReferralStatistic>>(defaultColumns)

  const columnDetail: ColumnsType<DetailRefrralStatistic> = [
    {
      dataIndex: 'user',
      title: 'Tên thành viên',
      render: (value) => value?.fullName,
      width: 150,
    },
    {
      dataIndex: 'user',
      title: 'Ngày đăng ký',
      align: 'center',
      render: (value) => formatDate(value?.createdAt),
    },
    {
      dataIndex: 'user',
      title: 'Hạn nhận hoa hồng',
      align: 'center',
      render: (value) => formatDate(value?.referralBonusExpire),
    },
    {
      dataIndex: 'firstOrderCommission',
      title: 'Thưởng đơn đầu tiên (VNĐ)',
      render: (value) => formatCurrency(value),
    },
    {
      dataIndex: 'revenueCommission',
      title: 'Thưởng doanh số (VNĐ)',
      render: (value) => formatCurrency(value),
    },
    {
      dataIndex: 'totalCommission',
      title: 'Tổng hoa hồng từ thành viên (VNĐ)',
      render: (value) => formatCurrency(value),
    },
  ]

  useEffect(() => {
    setIsLoading(true)
    RevenueService.revenueControllerCalculateReferralCommissionReport(
      tableServiceParams
    )
      .then((responseData) => {
        setIsLoading(false)
        setColumns(() => {
          const tmpPrevState = [...defaultColumns]
          responseData?.data.map((revenueReferralData) => {
            revenueReferralData.statistic.map((month, monthIndex) => {
              tmpPrevState.splice(monthIndex + 2, 0, {
                title: `Tháng ${format(new Date(month.date), 'M/yyyy')}`,
                dataIndex: 'statistic',
                align: 'center',
                render: (value, render) => {
                  return value?.[monthIndex]?.totalReferralCommission > 0 ? (
                    <Tooltip title={'Xem chi tiết'}>
                      <a
                        className={'text-blue-500'}
                        onClick={() => {
                          setUserId(render?.referral?.id)
                          setFullName(render?.referral.fullName)
                          setMonthDetail(value?.[monthIndex]?.date)
                          setIsReportDetailVisible(true)
                        }}
                      >
                        {formatCurrency(
                          value?.[monthIndex]?.totalReferralCommission
                        )}
                      </a>
                    </Tooltip>
                  ) : (
                    formatCurrency(0)
                  )
                },
              })
            })
          })

          return tmpPrevState
        })
        setCalculateReferralCommissionReportData(responseData?.data)
      })
      .catch((e) => {
        setIsLoading(false)
        alertError(e)
      })
  }, [tableServiceParams])

  useEffect(() => {
    if (userId && userId > 0) {
      setIsLoading(true)
      RevenueService.revenueControllerDetailReferralCommissionReport({
        userId: userId,
        fromDate: monthDetail,
        toDate: monthDetail,
        limit: 0,
        page: 1,
      })
        .then((responseData) => {
          setIsLoading(false)
          setDetailRefrralStatisticData(responseData)
        })
        .catch((e) => {
          setIsLoading(false)
          alertError(e)
        })
    }
  }, [userId, monthDetail])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    let fromDate = '',
      toDate = '',
      textSearch = ''
    Object.keys(data).map((key) => {
      if (data?.[key]) {
        if (key === 'date') {
          if (data?.date) {
            fromDate = startOfMonth(startOfMonth(data?.date[0])).toISOString()
            toDate = startOfMonth(data?.date[1]).toISOString()
          }
        }
        if (key === 'searchText') {
          textSearch = data?.[key]
        }
      }
    })
    setTableServiceParams({
      fromDate: fromDate,
      toDate: toDate,
      textSearch: textSearch,
    })
  }

  const onReset = () => {
    reset()
    setTableServiceParams({
      fromDate: startOfMonth(new Date()).toISOString(),
      toDate: startOfMonth(new Date()).toISOString(),
      limit: 10,
      page: 1,
    })
  }

  const onDownload = () => {
    setIsDownloading(true)
    if (
      calculateReferralCommissionReportData &&
      calculateReferralCommissionReportData.length > 0
    ) {
      setIsDownloading(false)
      const workbook = new ExcelJS.Workbook()
      const worksheet = workbook.addWorksheet('Sheet 1')
      const filename = `${title} - ${format(new Date(), 'd_m_Y_h_i_s')}`
      const headerRows = ['STT']
      columns.map((column) => {
        if (column.key !== 'operation') headerRows.push(column.title as string)
      })
      worksheet.addRow(headerRows)

      calculateReferralCommissionReportData.map((data: any, index: number) => {
        const row = [index + 1]
        columns.map((column) => {
          // @ts-ignore
          if (column.key !== 'operation' && column?.dataIndex) {
            if (column.render) {
              if (
                typeof column.render(data?.[column.dataIndex], data, index) ==
                'object'
              ) {
                row.push(
                  // @ts-ignore
                  column.render(data?.[column.dataIndex], data, index).props
                    .children.props.children
                )
              } else
                row.push(
                  // @ts-ignore
                  column.render(data?.[column.dataIndex], data, index)
                )
            } else {
              // @ts-ignore
              row.push(data?.[column?.dataIndex])
            }
          }
        })
        worksheet.addRow(row)
      })
      workbook.xlsx.writeBuffer().then((xls64) => {
        const a = document.createElement('a')
        const data = new Blob([xls64], {
          type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        })

        const url = URL.createObjectURL(data)
        a.href = url
        a.download = `${filename} - ${formatDate(new Date())}`
        document.body.appendChild(a)
        a.click()
        setTimeout(function () {
          document.body.removeChild(a)
          window.URL.revokeObjectURL(url)
        }, 0)
      })
    } else {
      alertError('Không có dữ liệu! Không thể tải')
      setIsDownloading(false)
    }
  }

  return (
    <>
      <Content title={title}>
        <Card>
          <Form
            autoComplete={'off'}
            onFinish={handleSubmit(onSubmit)}
            onReset={onReset}
            colon={false}
          >
            <Row gutter={[8, 8]}>
              <Col xs={24} sm={24} md={24} xl={8} lg={8}>
                <Form.Item label={'Tên CTV'}>
                  <Controller
                    control={control}
                    name={'searchText'}
                    render={({ field }) => (
                      <Input
                        {...field}
                        placeholder={'Nhập tên cộng tác viên'}
                      />
                    )}
                  />
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={24} xl={10} lg={10}>
                <Form.Item label={'Từ tháng'}>
                  <Controller
                    control={control}
                    name={'date'}
                    render={({ field }) => (
                      <DatePicker.RangePicker
                        allowClear={false}
                        format={'MM/YYYY'}
                        picker="month"
                        {...field}
                        style={{
                          width: '100%',
                        }}
                      />
                    )}
                  />
                </Form.Item>
              </Col>
              <Col
                xs={24}
                sm={24}
                md={24}
                xl={6}
                lg={6}
                className={'text-right'}
              >
                <FormItem>
                  <Space>
                    <Button htmlType={'reset'}>Đặt lại</Button>
                    <Button type={'primary'} htmlType={'submit'}>
                      Tìm kiếm
                    </Button>
                  </Space>
                </FormItem>
              </Col>
            </Row>
          </Form>
        </Card>
        <Card
          title={title}
          extra={
            <Tooltip title={'Tải về'}>
              <Button
                type={'primary'}
                onClick={onDownload}
                loading={isDownloading}
              >
                <DownloadOutlined />
              </Button>
            </Tooltip>
          }
        >
          <Table
            scroll={{ x: 1300 }}
            size={'small'}
            columns={columns}
            dataSource={calculateReferralCommissionReportData}
          />
        </Card>
      </Content>
      <Modal
        title={
          <label>
            Chi tiết thống kê hoa hồng của cộng tác viên: <b>{fullName}</b>
          </label>
        }
        visible={isReportDetailVisible}
        onCancel={() => {
          setIsReportDetailVisible(false)
        }}
        destroyOnClose={true}
        width={1300}
        onOk={() => {
          setIsReportDetailVisible(false)
        }}
      >
        <Table
          scroll={{ x: 700 }}
          size={'small'}
          pagination={false}
          columns={columnDetail}
          dataSource={detailRefrralStatisticData}
        />
      </Modal>
    </>
  )
}

export default Index

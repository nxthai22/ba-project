import {
  addDays,
  addMonths,
  addQuarters,
  addWeeks,
  addYears,
  endOfDay,
  endOfMonth,
  endOfQuarter,
  endOfWeek,
  endOfYear,
  format,
  startOfDay,
  startOfMonth,
  startOfQuarter,
  startOfWeek,
  startOfYear,
} from 'date-fns'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { useRouter } from 'next/router'
import { ColumnsType } from 'antd/es/table'
import React, { FC, useEffect, useRef, useState } from 'react'
import { authState, countDataTable, loadingState } from 'recoil/Atoms'
import { TableRowSelection } from 'antd/es/table/interface'
import { Controller, SubmitHandler, useForm, useWatch } from 'react-hook-form'
import {
  DownloadOutlined,
  ExclamationCircleOutlined,
  InboxOutlined,
  PrinterOutlined,
  SearchOutlined,
  UploadOutlined,
} from '@ant-design/icons'
import {
  Button,
  Card,
  Col,
  Form,
  Input,
  List,
  Menu,
  Modal,
  PaginationProps,
  Row,
  Select,
  Space,
  Spin,
  Table,
  Tag,
  Upload,
} from 'antd'
import { ServiceParams } from 'utils/interfaces'
import {
  alertError,
  alertSuccess,
  formatCurrency,
  formatDate,
  formatNumber,
  getOrderShippingStatus,
  getOrderStatusDesc,
  getPaymentStatusDesc,
  getShippingPartnerName,
  hasPermission,
} from 'utils'
import {
  BankInfo,
  BankService,
  ConfigService,
  EnumChangePaymentStatusDtoPaymentStatus,
  EnumOrderEntityPaymentStatus,
  EnumOrderEntityPaymentType,
  EnumOrderEntityStatus,
  EnumOrderHistoryEntityStatus,
  EnumRoleEntityType,
  EnumShippingInfoStatus,
  EnumStatusCountResponseDtoStatus,
  MerchantAddressEntity,
  MerchantEntity,
  MerchantsService,
  OrderEntity,
  OrderPaymentHistoryEntity,
  OrderProductEntity,
  OrdersService,
  StatusCountResponseDto,
  UsersService,
} from 'services'
import Link from 'components/common/Link'
import DatePicker from 'components/common/DatePicker'
import BatchDelivery from 'components/common/BatchDelivery'
import Content from 'components/layout/AdminLayout/Content'
import ExcelJS from 'exceljs'
import OrdersPrint from 'components/common/OrdersPrint'
import { useReactToPrint } from 'react-to-print'
import { SHIPPING_STATUS } from '@/constants'
import { UploadRequestOption } from 'rc-upload/lib/interface'
import DataTable from 'components/common/DataTable'

interface Inputs {
  q?: string
  type?: string
  dates?: [Date, Date]
  shippingStatus?: EnumShippingInfoStatus
  paymentStatus?: EnumOrderEntityPaymentStatus
}

const Order: FC = () => {
  const router = useRouter()
  const { status } = router.query
  const [title, setTitle] = useState('Danh sách Đơn hàng')
  const setIsLoading = useSetRecoilState(loadingState)
  const isLoading = useRecoilValue(loadingState)
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([])
  const countDatatable = useRecoilValue(countDataTable)
  const [merchantAddress, setMerchantAddress] = useState<
    MerchantAddressEntity[]
  >([])
  const { control, handleSubmit, reset, getValues, setValue } = useForm<Inputs>(
    {
      defaultValues: {
        q: '',
        type: 'code',
        dates: [addDays(new Date(), -7), new Date()],
        paymentStatus: EnumOrderEntityPaymentStatus.deposited,
      },
    }
  )

  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['createdAt,DESC'],
    join: [
      'user',
      'ward',
      'ward.district',
      'ward.district.province',
      'orderProducts',
      'orderProducts.product',
      'orderProducts.variant',
      'merchantAddress',
      'group',
    ],
    filter: [
      `createdAt||between||${startOfDay(
        addDays(new Date(), -7)
      ).toISOString()},${endOfDay(new Date()).toISOString()}`,
    ],
  })
  const [selectedMenu, setSelectedMenu] = useState('all')
  const [statusCount, setStatusCount] = useState<StatusCountResponseDto[]>([])
  const [isBatchDeliveryVisible, setIsBatchDeliveryVisible] = useState(false)
  const [deliveryIds, setSeliveryIds] = useState([])
  const user = useRecoilValue(authState)
  const [isDownloading, setIsDownloading] = useState<boolean>(false)
  const [isTemplateDownloading, setIsTemplateDownloading] =
    useState<boolean>(false)
  const [ordersToPrint, setOrdersToPrint] = useState<OrderEntity[]>([])
  const [merchant, setMerchant] = useState<MerchantEntity>()
  const ordersPrintRef = useRef(null)
  const handlePrint = useReactToPrint({
    content: () => ordersPrintRef.current,
    pageStyle: `@page {
      size: auto;
      margin: 10mm 0mm;
    }
  `,
    onBeforePrint: () => {
      setTitle(
        merchant?.name
          ? `${merchant?.name} / Ushare / Thông tin giao hàng`
          : 'Ushare / Thông tin giao hàng'
      )
    },
    onAfterPrint: () => {
      setTitle('Danh sách đơn hàng')
    },
  })
  const [bankInfo, setBankInfo] = useState<BankInfo>()
  const [state, setState] = useState<any>()
  const [searchInput, setSearchInput] = useState<any>(null)
  const [visibleChangeDeposit, setVisibleChangeDeposit] =
    useState<boolean>(false)
  const [visibleDetailGroupBuying, setVisibleDetailGroupBuying] =
    useState<boolean>(false)
  const [orderGroupBuying, setOrderGroupBuying] = useState<OrderEntity[]>([])
  const [selectedModalRowKeys, setSelectedModalRowKeys] = useState<React.Key[]>(
    []
  )
  const [paginationModalGroupBuying, setPaginationModalGroupBuying] =
    useState<PaginationProps>({
      current: 1,
      pageSize: 10,
      total: 0,
      disabled: false,
    })
  const [tableModalServiceParams, setTableModalServiceParams] =
    useState<ServiceParams>({
      sort: ['createdAt,DESC'],
      join: [
        'user',
        'ward',
        'ward.district',
        'ward.district.province',
        'orderProducts',
        'orderProducts.product',
        'orderProducts.variant',
        'merchantAddress',
        'group',
        'group.user',
      ],
      filter: [],
      page: 1,
      limit: 10,
    })
  const watchDates = useWatch({
    control,
    name: 'dates',
  })

  useEffect(() => {
    setTimeout(() => {
      if (status && status != '') {
        setSelectedMenu(String(status))
        setTableServiceParams((prevState) => {
          let statusIndex = -1
          prevState.filter?.map((filter, index) => {
            if (filter?.includes('status')) {
              statusIndex = index
            }
          })
          if (statusIndex > -1) prevState.filter.splice(statusIndex, 1)
          if (status != 'all') {
            if (status === 'cancelled')
              return {
                ...prevState,
                filter: [
                  ...prevState.filter,
                  `status||in||${EnumOrderEntityStatus.cancelled},${EnumOrderEntityStatus.merchant_cancelled}`,
                ],
              }
            else {
              return {
                ...prevState,
                filter: [...prevState.filter, `status||eq||${status}`],
              }
            }
          } else
            return {
              ...prevState,
            }
        })
      }
    }, 500)
  }, [status])

  useEffect(() => {
    const dates = getValues('dates')
    Promise.all([
      OrdersService.orderControllerGetStatusAdminCount({
        fromDate: new Date(dates?.[0]).toISOString(),
        toDate: new Date(dates?.[1]).toISOString(),
      }),
      user?.merchantId &&
        MerchantsService.getOneBase({
          id: user?.merchantId,
        }),
    ]).then(([response, merchantResponse]) => {
      setMerchant(merchantResponse)
      setStatusCount(response)
    })
  }, [])

  useEffect(() => {
    setIsLoading(true)
    Promise.all([OrdersService.getManyBase(tableModalServiceParams)])
      .then(([response]) => {
        setIsLoading(false)
        setOrderGroupBuying(response.data)
        setPaginationModalGroupBuying({
          current: response.page,
          pageSize: paginationModalGroupBuying.pageSize,
          total: response.total,
        })
      })
      .catch((err) => {
        setIsLoading(false)
        alertError(err)
      })
  }, [tableModalServiceParams])
  const rowSelection: TableRowSelection<OrderEntity> = {
    onChange: (selectedRowKeys: React.Key[]) => {
      setSelectedRowKeys(selectedRowKeys)
    },
    getCheckboxProps: (record: OrderEntity) => ({
      id: String(record.id),
    }),
  }
  const rowModalSelection: TableRowSelection<OrderEntity> = {
    onChange: (selectedRowKeys: React.Key[]) => {
      setSelectedModalRowKeys(selectedRowKeys)
    },
    getCheckboxProps: (record: OrderEntity) => ({
      id: String(record.id),
    }),
  }
  // @ts-ignore
  // @ts-ignore
  const getColumnSearchProps = (dataIndex, placeholderInput?: string) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            setSearchInput(node)
          }}
          placeholder={
            placeholderInput ? placeholderInput : `Search ${dataIndex}`
          }
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) => {
      if (dataIndex === 'orderProducts') {
        return record[dataIndex].some((prod) =>
          prod.product?.name?.toLowerCase().includes(value.toLowerCase())
        )
      }
      if (dataIndex === 'group') {
        return record[dataIndex]
          ? record[dataIndex]['name']
              .toString()
              .toLowerCase()
              .includes(value.toLowerCase())
          : ''
      }
      return record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : ''
    },
    onFilterDropdownVisibleChange: (visible) => {
      // if (visible) {
      //   setTimeout(() => searchInput?.select(), 100)
      // }
    },
    render: (text) => (state.searchedColumn === dataIndex ? text : text),
  })
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm()
    setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    })
  }
  const handleReset = (clearFilters) => {
    clearFilters()
    setState({ searchText: '' })
  }
  const columns: ColumnsType<OrderEntity> = [
    {
      dataIndex: 'group',
      title: <div className={'text-center'}>Nhóm</div>,
      width: 150,
      key: 'group-name',
      ...getColumnSearchProps('group', 'Tìm kiếm theo tên nhóm'),
      render: (value, record) => {
        return (
          <div className={'text-center'}>
            <Button
              type={'link'}
              onClick={() => {
                setVisibleDetailGroupBuying(true)
                setTableModalServiceParams((prevState) => {
                  return {
                    ...prevState,
                    filter: [`groupId||eq||${record.groupId}`],
                  }
                })
              }}
            >
              <span
                style={{
                  color: '#1890ff',
                }}
              >
                {record.group?.name}
              </span>
            </Button>
          </div>
        )
      },
    },
    {
      dataIndex: 'code',
      title: <div className={'text-center'}>Mã ĐH</div>,
      width: 180,
      key: 'order-code',
      ...getColumnSearchProps('code', 'Tìm kiếm theo Mã ĐH'),
      render: (value, record) => {
        return (
          <div className={'text-center'}>
            <Link href={`/order/${record.id}`}>
              <span
                style={{
                  color: '#1890ff',
                }}
              >
                {record.code}
              </span>
            </Link>
          </div>
        )
      },
    },
    {
      dataIndex: 'fullname',
      title: 'Thông tin khách hàng',
      key: 'fullname-customer',
      ...getColumnSearchProps('fullname', 'Tìm kiếm theo tên KH'),
      render: (_, record) => {
        return (
          <div>
            <p>{record.fullname}</p>
            <p>{record.tel}</p>
          </div>
        )
      },
    },
    {
      dataIndex: 'orderProducts',
      title: 'Sản phẩm',
      width: 300,
      ...getColumnSearchProps('orderProducts', 'Tìm kiếm theo tên SP'),
      key: 'orderProducts',
      render: (value: OrderProductEntity[]) => {
        if (value.length > 0) {
          return (
            <List
              className="demo-loadmore-list items-center"
              itemLayout="horizontal"
              dataSource={value}
              renderItem={(item) => (
                <List.Item
                  className={'items-center'}
                  style={{ borderBottom: 'none' }}
                >
                  <List.Item.Meta
                    className={'items-center '}
                    avatar={
                      <img src={item?.product?.images?.[0]} width={'60px'} />
                    }
                    title={<span>{item?.product?.name}</span>}
                    description={
                      <>
                        {item?.variant && (
                          <>
                            <br />
                            Phân loại: {item?.variant?.name}
                          </>
                        )}
                      </>
                    }
                  />
                  <div>x{item?.quantity}</div>
                </List.Item>
              )}
            />
          )
        } else {
          return null
        }
      },
    },
    {
      dataIndex: 'total',
      title: 'Tổng giá bán',
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.total - b.total,
      render: (value, record) => {
        return (
          <div>
            <p>{formatCurrency(value)}</p>
            <p>
              {record.paymentType === 'cod'
                ? 'Thanh toán khi nhận hàng'
                : 'Chuyển khoản ngân hàng'}
            </p>
          </div>
        )
      },
    },
    {
      dataIndex: 'orderProducts',
      title: 'Tổng hoa hồng',
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.commission - b.commission,
      render: (value: OrderProductEntity[]) => {
        if (value.length > 0) {
          const commission =
            value.reduce((total, curr) => {
              return total + Number(curr.commission || 0)
            }, 0) || 0
          return (
            <>
              <p>{formatCurrency(commission)}</p>
            </>
          )
        }
      },
    },
    {
      dataIndex: 'paymentStatus',
      title: 'Trạng thái cọc',
      filters: [
        {
          text: 'Chưa cọc',
          value: 'wait_deposit',
        },
        {
          text: 'Đã xác nhận cọc',
          value: 'confirm_deposit',
        },
        {
          text: 'Đã đặt cọc',
          value: 'deposited',
        },
        {
          text: 'Đã hoàn cọc',
          value: 'returned_deposit',
        },
      ],
      onFilter: (value, record) => record.paymentStatus === value,
      render: (value) => getPaymentStatusDesc(value),
    },
    {
      dataIndex: 'status',
      title: 'Trạng thái',
      filters: [
        {
          text: 'Chờ xác nhận',
          value: 'created',
        },
        {
          text: 'Chờ ghép đơn',
          value: 'waiting_for_grouping',
        },
        {
          text: 'Đã xác nhận',
          value: 'confirmed',
        },
        {
          text: 'Đang giao',
          value: 'shipping',
        },
        {
          text: 'Đã giao, chờ đối soát',
          value: 'shipped',
        },
        {
          text: 'Đã đối soát',
          value: 'completed',
        },
        {
          text: 'Đã huỷ',
          value: 'cancelled',
        },
        {
          text: 'Đối tác huỷ',
          value: 'merchant_cancelled',
        },
        {
          text: 'Hoàn hàng',
          value: 'return',
        },
      ],
      onFilter: (value, record) => record.status === value,
      render: (value) => getOrderStatusDesc(value),
    },
    {
      dataIndex: 'shippingInfos',
      title: 'Vận chuyển',
      width: 150,
      render: (value) => getOrderShippingStatus(value?.[0]?.status),
    },
    {
      dataIndex: 'shippingInfos',
      title: 'Đối tác vận chuyển',
      width: 100,
      render: (value) =>
        getShippingPartnerName(value?.[0]?.partner || value?.[1]?.partner),
    },
    {
      dataIndex: 'createdAt',
      title: 'Ngày tạo',
      render: (value) => formatDate(value),
    },
    // {
    //   dataIndex: user.type === EnumRoleEntityType.admin ? 'importCode' : '',
    //   title: user.type === EnumRoleEntityType.admin ? 'Mã đơn MISA' : '',
    // },
  ]
  const modalColums = [
    {
      dataIndex: 'code',
      title: 'Mã đơn hàng',
    },
    {
      dataIndex: 'paymentStatus',
      title: 'Trạng thái cọc',
      filters: [
        {
          text: 'Chưa cọc',
          value: 'wait_deposit',
        },
        {
          text: 'Đã xác nhận cọc',
          value: 'confirm_deposit',
        },
        {
          text: 'Đã đặt cọc',
          value: 'deposited',
        },
        {
          text: 'Đã hoàn cọc',
          value: 'returned_deposit',
        },
      ],
      onFilter: (value, record) => record.paymentStatus === value,
      render: (value) => getPaymentStatusDesc(value),
    },
    {
      dataIndex: 'user',
      title: 'CTV',
      render: (value, record) => record.user?.fullName,
    },
    {
      dataIndex: 'status',
      title: 'Trạng thái đơn hàng',
      filters: [
        {
          text: 'Chờ xác nhận',
          value: 'created',
        },
        {
          text: 'Chờ ghép đơn',
          value: 'waiting_for_grouping',
        },
        {
          text: 'Đã xác nhận',
          value: 'confirmed',
        },
        {
          text: 'Đang giao',
          value: 'shipping',
        },
        {
          text: 'Đã giao, chờ đối soát',
          value: 'shipped',
        },
        {
          text: 'Đã đối doát',
          value: 'completed',
        },
        {
          text: 'Đã huỷ',
          value: 'cancelled',
        },
        {
          text: 'Đối tác huỷ',
          value: 'merchant_cancelled',
        },
        {
          text: 'Hoàn hàng',
          value: 'return',
        },
      ],
      onFilter: (value, record) => record.status === value,
      render: (value) => getOrderStatusDesc(value),
    },
    {
      dataIndex: 'orderProducts',
      title: 'Sản phẩm',
      width: 250,
      ...getColumnSearchProps('orderProducts', 'Tìm kiếm theo tên SP'),
      render: (value: OrderProductEntity[]) => {
        return (
          <List
            className="w-full"
            itemLayout="horizontal"
            dataSource={value}
            renderItem={(item) => (
              <List.Item
                className={'items-center'}
                style={{ borderBottom: 'none' }}
              >
                <List.Item.Meta
                  className={'items-center '}
                  title={<span>{item?.product?.name}</span>}
                  description={
                    <>
                      Phân loại:{' '}
                      {item?.variant ? item?.variant.name : item.variantTitle}
                    </>
                  }
                />
              </List.Item>
            )}
          />
        )
      },
    },
    {
      dataIndex: 'orderProducts',
      title: 'Số lượng',
      render: (value: OrderProductEntity[]) => {
        const totalQty = value.reduce((total, curr) => {
          return total + Number(curr.quantity || 0)
        }, 0)
        return formatNumber(totalQty)
      },
    },
    {
      dataIndex: 'total',
      title: 'Tổng giá bán',
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.total - b.total,
      render: (value, record) => {
        return (
          <>
            <div className={'flex flex-col'}>
              <div>{formatCurrency(value)}</div>
              <div>{getPaymentTypeName(record.paymentType)}</div>
            </div>
          </>
        )
      },
    },
    {
      dataIndex: 'commission',
      title: 'Tổng hoa hồng',
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.commission - b.commission,
      render: (value) => formatCurrency(value),
    },
    {
      dataIndex: 'paymentHistories',
      title: 'Ngày đặt cọc',
      render: (value: OrderPaymentHistoryEntity[]) =>
        formatDate(value?.[0]?.createdAt),
    },
    {
      dataIndex: 'createdAt',
      title: 'Ngày tạo',
      render: (value) => formatDate(value),
    },
  ]
  const removeFilter = (filters: string[], key) => {
    let existIndex = -1
    if (filters.length > 0) {
      filters?.map((filter, index) => {
        if (filter?.includes(key)) {
          existIndex = index
        }
      })
      if (existIndex > -1) filters.splice(existIndex, 1)
    }
    return filters
  }

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const dates = getValues('dates')
    setTableServiceParams((prevState) => {
      if (data.q) {
        removeFilter(prevState.filter, 'code')
        removeFilter(prevState.filter, 'fullname')
        removeFilter(prevState.filter, 'tel')
        removeFilter(prevState.filter, 'user.tel')
        removeFilter(prevState.filter, 'user.fullName')
        removeFilter(prevState.filter, 'merchantAddress.name')
        // removeFilter(prevState.filter, 'importCode')
        let filterTxt = ''
        switch (data.type) {
          case 'code':
            filterTxt = `code||$contL||${data?.q}`
            removeFilter(prevState.filter, 'fullname')
            removeFilter(prevState.filter, 'tel')
            removeFilter(prevState.filter, 'user.tel')
            removeFilter(prevState.filter, 'user.fullName')
            removeFilter(prevState.filter, 'merchantAddress.name')
            // removeFilter(prevState.filter, 'importCode')
            break
          case 'fullname':
            filterTxt = `fullname||$contL||${data?.q}`
            removeFilter(prevState.filter, 'code')
            removeFilter(prevState.filter, 'tel')
            removeFilter(prevState.filter, 'user.tel')
            removeFilter(prevState.filter, 'user.fullName')
            removeFilter(prevState.filter, 'merchantAddress.name')
            // removeFilter(prevState.filter, 'importCode')
            break
          case 'tel':
            filterTxt = `tel||$contL||${data?.q}`
            removeFilter(prevState.filter, 'code')
            removeFilter(prevState.filter, 'fullname')
            removeFilter(prevState.filter, 'user.tel')
            removeFilter(prevState.filter, 'user.fullName')
            removeFilter(prevState.filter, 'merchantAddress.name')
            // removeFilter(prevState.filter, 'importCode')
            break
          case 'partner':
            filterTxt = `user.tel||$contL||${data?.q}`
            removeFilter(prevState.filter, 'tel')
            removeFilter(prevState.filter, 'code')
            removeFilter(prevState.filter, 'fullname')
            removeFilter(prevState.filter, 'user.fullName')
            removeFilter(prevState.filter, 'merchantAddress.name')
            // removeFilter(prevState.filter, 'importCode')
            break
          case 'partner_name':
            filterTxt = `user.fullName||$contL||${data?.q}`
            removeFilter(prevState.filter, 'tel')
            removeFilter(prevState.filter, 'code')
            removeFilter(prevState.filter, 'fullname')
            removeFilter(prevState.filter, 'user.tel')
            removeFilter(prevState.filter, 'merchantAddress.name')
            // removeFilter(prevState.filter, 'importCode')
            break
          case 'merchant_address':
            filterTxt = `merchantAddress.name||$contL||${data?.q}`
            removeFilter(prevState.filter, 'tel')
            removeFilter(prevState.filter, 'code')
            removeFilter(prevState.filter, 'fullname')
            removeFilter(prevState.filter, 'user.tel')
            removeFilter(prevState.filter, 'user.fullName')
            // removeFilter(prevState.filter, 'importCode')
            break
          // case 'import_code':
          //   filterTxt = `importCode||$contL||${data?.q}`
          //   removeFilter(prevState.filter, 'tel')
          //   removeFilter(prevState.filter, 'code')
          //   removeFilter(prevState.filter, 'fullname')
          //   removeFilter(prevState.filter, 'user.tel')
          //   removeFilter(prevState.filter, 'user.fullName')
          //   removeFilter(prevState.filter, 'merchantAddress.name')
          //   break
        }
        prevState = {
          ...prevState,
          filter: [...removeFilter(prevState.filter, data.type), filterTxt],
        }
      } else {
        removeFilter(prevState.filter, 'code')
        removeFilter(prevState.filter, 'fullname')
        removeFilter(prevState.filter, 'tel')
        removeFilter(prevState.filter, 'user.tel')
        removeFilter(prevState.filter, 'user.fullName')
        removeFilter(prevState.filter, 'merchantAddress.name')
        // removeFilter(prevState.filter, 'importCode')
      }

      prevState = {
        ...prevState,
        shippingStatus: data?.shippingStatus,
      }

      if (dates?.length > 0) {
        prevState = {
          ...prevState,
          filter: [
            ...removeFilter(prevState.filter, 'createdAt'),
            `createdAt||between||${format(
              dates?.[0],
              'yyyy-MM-dd'
            )}  00:00:00,${format(dates?.[1], 'yyyy-MM-dd')} 23:59:59`,
          ],
        }
      } else {
        removeFilter(prevState.filter, 'createdAt')
      }
      return { ...prevState }
    })
  }
  const onRangePickerChange = (dateStrings: [string, string]) => {
    setTableServiceParams((prevState) => {
      const filter = prevState.filter
      removeFilter(filter, 'createdAt')
      prevState = {
        ...prevState,
        filter: [
          ...filter,
          `createdAt||between||${startOfDay(
            new Date(dateStrings?.[0])
          ).toISOString()},${endOfDay(
            new Date(dateStrings?.[1])
          ).toISOString()}`,
        ],
      }
      return { ...prevState }
    })
    setIsLoading(true)
    OrdersService.orderControllerGetStatusAdminCount({
      fromDate: new Date(dateStrings?.[0]).toISOString(),
      toDate: new Date(dateStrings?.[1]).toISOString(),
    })
      .then((response) => {
        setIsLoading(false)
        setStatusCount(response)
      })
      .catch((err) => {
        setIsLoading(false)
        alertError(err)
      })
  }
  const resetForm = () => {
    reset()
    setTableServiceParams({
      sort: ['updatedAt,DESC'],
      join: [
        'user',
        'ward',
        'ward.district',
        'ward.district.province',
        'orderProducts',
        'orderProducts.product',
        'merchantAddress',
      ],
      filter: [status && status != '' ? `status||eq||${status}` : []],
    })
  }
  const onUpdatePaymentStatus = () => {
    setIsLoading(true)
    const paymentStatus = getValues('paymentStatus')
    const orderIds = visibleDetailGroupBuying
      ? selectedModalRowKeys?.map((orderId) => orderId)
      : selectedRowKeys?.map((orderId) => orderId)
    OrdersService.orderControllerUpdateDepositStatus({
      body: {
        orderIds: orderIds as number[],
        paymentStatus: paymentStatus,
      },
    })
      .then((response) => {
        setIsLoading(false)
        alertSuccess('Đổi trạng thái đặt cọc thành công')
      })
      .catch((err) => {
        setIsLoading(false)
        alertError(err)
      })
  }

  const batchDelivery = () => {
    Promise.all([
      OrdersService.getManyBase({
        filter: [
          `id||in||${selectedRowKeys.map((orderId) => orderId).join(',')}`,
          `status||in||${EnumOrderEntityStatus.confirmed}`,
        ],
        limit: 100,
      }),
      OrdersService.getManyBase({
        filter: [
          `id||in||${selectedRowKeys.map((orderId) => orderId).join(',')}`,
          `status||notin||${EnumOrderEntityStatus.confirmed}`,
        ],
        limit: 100,
      }),
    ])
      .then(([response, otherResponse]) => {
        const validIds = [],
          inValidIds = [],
          validCodes = [],
          inValidCodes = []
        selectedRowKeys.map((orderId) => {
          const validOrder = response.data.find((order) => order.id == orderId)
          if (validOrder) {
            validCodes.push(validOrder?.code)
            validIds.push(orderId)
          } else {
            const inValidOrder = otherResponse.data.find(
              (order) => order.id == orderId
            )
            inValidCodes.push(inValidOrder?.code)
            inValidIds.push(orderId)
          }
        })
        setSeliveryIds(validIds)
        Modal.confirm({
          title: 'Xác nhận thông tin',
          icon: <ExclamationCircleOutlined />,
          content: (
            <Row>
              <Col xs={24}>
                Đơn hàng hợp lệ (
                <span className={'text-green-500 font-bold'}>
                  {validCodes.length}
                </span>
                ): {validCodes.join(', ')}
              </Col>
              <Col xs={24}>
                Đơn hàng không hợp lệ (
                <span className={'text-red-500 font-bold'}>
                  {inValidCodes.length}
                </span>
                ): {inValidCodes.join(', ')}
              </Col>
            </Row>
          ),
          okButtonProps: {
            disabled: validIds.length === 0,
          },
          onOk() {
            setIsBatchDeliveryVisible(true)
          },
        })
      })
      .catch((error) => alertError(error))
  }

  const printInvoice = () => {
    setIsLoading(true)
    Promise.all([
      OrdersService.getManyBase({
        filter: [
          `id||in||${selectedRowKeys.map((orderId) => orderId).join(',')}`,
          `status||in||${EnumOrderEntityStatus.shipping}`,
        ],
        join: [
          `merchant`,
          'merchant.bank',
          `merchantAddress`,
          'merchantAddress.ward',
          'merchantAddress.ward.district',
          'merchantAddress.ward.district.province',
          'ward',
          'ward.district',
          'ward.district.province',
          'orderProducts',
          'orderProducts.product',
          'orderProducts.variant',
          'histories',
          'histories.user',
        ],
        limit: 100,
      }),
      ConfigService.getOneBase({
        id: 18,
      }),
    ])
      .then(([orderResponse, bankInfoConfigResponse]) => {
        const tmpBankInfo = bankInfoConfigResponse?.value as unknown as BankInfo
        BankService.getOneBase({
          id: tmpBankInfo?.bankId,
        }).then((bank) => {
          tmpBankInfo.bank = bank
          setIsLoading(false)
          setOrdersToPrint(orderResponse.data)
          setBankInfo(tmpBankInfo)
          handlePrint()
        })
      })
      .catch((e) => {
        setIsLoading(false)
        alertError(e)
      })
  }
  /* Export đơn hàng */
  const getPaymentTypeName = (paymentType: EnumOrderEntityPaymentType) => {
    switch (paymentType) {
      case EnumOrderEntityPaymentType.cod:
        return 'Thanh toán khi nhận hàng'
      case EnumOrderEntityPaymentType.bacs:
        return 'Thanh toán chuyển khoản'
    }
  }

  const onTemplateDownload = () => {
    const a = document.createElement('a')
    // let url = 'https://ushare-dev.hn.ss.bfcplatform.vn/template/order_import_template.xlsx'
    let url =
      process.env.NEXT_PUBLIC_S3_URL + 'template/order_import_template.xlsx'
    a.href = url
    a.download = 'order_import_template' + `${formatDate(new Date())}` + '.xlsx'
    document.body.appendChild(a)
    a.click()
    setTimeout(function () {
      document.body.removeChild(a)
      window.URL.revokeObjectURL(url)
    }, 200)
  }

  const onDownload = () => {
    setIsDownloading(true)
    const dates = getValues('dates')
    if (dates?.length < 1) {
      setIsDownloading(false)
      alertError('Cần chọn khoảng ngày tạo đơn hàng')
      return
    }
    Promise.all([
      OrdersService.getManyBase({
        join: [
          'user',
          'user.bank',
          'user.leaders',
          'ward',
          'ward.district',
          'ward.district.province',
          'orderProducts',
          'orderProducts.product',
          'orderProducts.variant',
          'histories',
          'merchantAddress',
          'merchantAddress.merchant',
        ],
        filter: [
          ...removeFilter(tableServiceParams.filter, 'createdAt'),
          `createdAt||between||${startOfDay(dates[0]).toISOString()},${endOfDay(
            dates[1]
          ).toISOString()}`,
          status ? `status||eq||${status}` : [],
        ],
        sort: ['createdAt,DESC'],
        limit: 1000,
      }),
      user.merchantId &&
        MerchantsService.getOneBase({
          id: user.merchantId,
          join: ['ward', 'ward.district', 'ward.district.province'],
        }),
      UsersService.getManyBase({
        filter: [
          user.type === EnumRoleEntityType.merchant
            ? `merchantId||eq||${user.merchantId}`
            : [],
        ],
      }),
    ]).then(([response, merchantResponse, userResponse]) => {
      setIsDownloading(false)
      if (response?.data?.length < 1) {
        alertError('Không có dữ liệu')
        return
      }
      const workbook = new ExcelJS.Workbook()
      const worksheet = workbook.addWorksheet('Sheet 1')

      const filename = `${title} - ${format(new Date(), 'd_m_Y_h_i_s')}`
      /* Thông tin nhà cung cấp */
      worksheet.addRow([
        [EnumRoleEntityType.admin, EnumRoleEntityType.accountant].includes(
          user.type as EnumRoleEntityType
        )
          ? 'Ushare'
          : [
              EnumRoleEntityType.merchant,
              EnumRoleEntityType.merchant_accountant,
              EnumRoleEntityType.leader,
            ].includes(user.type as EnumRoleEntityType)
          ? merchantResponse?.name
          : '',
      ])
      worksheet.getRow(0).font = {
        bold: true,
        size: 12,
        name: 'Times New Roman',
      }
      worksheet.getRow(0).alignment = { horizontal: 'left', vertical: 'middle' }
      worksheet.addRow(merchantResponse?.address || '')
      worksheet.addRow(merchantResponse?.tel || '')
      const font = { size: 12, name: 'Times New Roman' }
      worksheet.getRow(1).font = font
      worksheet.getRow(1).alignment = { horizontal: 'left', vertical: 'middle' }
      worksheet.getRow(2).font = font
      worksheet.getRow(2).alignment = { horizontal: 'left', vertical: 'middle' }

      /* Tiêu đề báo cáo*/
      worksheet.mergeCells('A4:AI4')
      worksheet.getCell('AI4').font = {
        bold: true,
        size: 18,
        name: 'Times New Roman',
      }
      worksheet.getCell('AI4').alignment = {
        horizontal: 'center',
        vertical: 'middle',
      }
      worksheet.getCell('AI4').value =
        'CHI TIẾT DOANH THU THEO HÓA ĐƠN VÀ MẶT HÀNG'
      worksheet.mergeCells('A5:AI5')
      worksheet.getCell('AI5').font = {
        size: 12,
        name: 'Times New Roman',
        italic: true,
      }
      worksheet.getCell('AI5').alignment = {
        horizontal: 'center',
        vertical: 'middle',
      }
      worksheet.getCell('AI5').value = `Từ ngày: ${formatDate(
        dates?.[0],
        'dd/MM/yyyy'
      )} đến ngày: ${formatDate(dates?.[1], 'dd/MM/yyyy')}`
      worksheet.addRow('')
      /* Header */
      const headerRows = [
        'Nhà cung cấp',
        'Ngày tạo đơn',
        'Ngày hoàn thành',
        'Giờ hoàn thành',
        'Mã đơn hàng',
        'Mã SKU',
        'Tên sản phẩm',
        'Phân loại',
        'Số lượng',
        'Đơn giá',
        'Thành tiền SP',
        'Giảm giá SP',
        'Giảm giá đơn hàng',
        'Tổng giảm giá',
        'Doanh thu',
        'Hình thức thanh toán',
        'Tiền đặt cọc',
        'Còn phải thu',
        '% hoa hồng',
        'Hoa hồng sản phẩm',
        'Phí vận chuyển',
        'Đơn vị vận chuyển',
        'Trạng thái vận chuyển',
        'Khách hàng',
        'SĐT Khách hàng',
        'Địa chỉ giao hàng',
        'Phường',
        'Quận',
        'Thành phố',
        'Tên CTV',
        'SĐT CTV',
        'STK ngân hàng',
        'NV phụ trách',
        'Trạng thái đơn hàng',
        'Lý do hủy/hoàn',
        'Ngày đối soát',
        'Mã cửa hàng',
        'Ghi chú',
      ]
      worksheet.addRow(headerRows)
      worksheet.getRow(7).font = {
        size: 12,
        name: 'Times New Roman',
        bold: true,
      }
      worksheet.getRow(7).alignment = {
        horizontal: 'center',
        vertical: 'middle',
      }
      worksheet.getRow(7).border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' },
      }
      worksheet.getRow(7).fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FBDAD7' },
      }
      let lastIndex = 7
      response.data.map((order, index) => {
        lastIndex += order.orderProducts?.length || 0
        let receivable = 0
        order.orderProducts?.map((orderProduct, prodIndex) => {
          const row = []
          /* Nhà cung cấp */
          row.push(order.merchantAddress?.merchant?.name)
          /* Ngày tạo đơn */
          row.push(formatDate(order?.createdAt, 'dd/MM/yyyy'))
          /* Ngày hoàn thành */
          const completedAt = order.histories?.find(
            (his) => his.status === EnumOrderHistoryEntityStatus.completed
          )?.createdAt
          row.push(completedAt ? formatDate(completedAt, 'dd/MM/yyyy') : '')
          /* Giờ hoàn thành */
          row.push(completedAt ? formatDate(completedAt, 'HH:mm:ss') : '')
          /* Mã đơn hàng */
          row.push(order.code)
          /* Mã SKU */
          row.push(
            orderProduct.variant
              ? orderProduct.variant?.SKU
              : orderProduct.product?.SKU ?? ''
          )
          /* Tên sản phẩm */
          row.push(orderProduct.product?.name)
          /* Phân loại */
          row.push(orderProduct.variantTitle)
          /* Số lượng */
          row.push(orderProduct.quantity)
          /* Đơn giá */
          row.push(orderProduct.price)
          /* Thành tiền SP */
          const intoMoney =
            orderProduct.quantity * Number(orderProduct.price || 0)
          row.push(intoMoney)
          /* Giảm giá SP */
          row.push(
            Number(orderProduct.discountVoucherProduct || 0) +
              Number(orderProduct.discountVoucherShop || 0)
          )
          /* Giảm giá đơn hàng */
          row.push(order.discount)
          /* Tổng giảm giá */
          // const orderDiscountPerProduct = Math.floor(
          //   order.discount / (order.orderProducts?.length || 1)
          // )
          row.push(orderProduct.discount)
          /* Doanh thu */
          row.push(intoMoney - orderProduct.discount)
          /* Hình thức thanh toán */
          row.push(getPaymentTypeName(order.paymentType))
          /* Tiền đặt cọc */
          row.push(order.deposit || 0)
          /* Còn phải thu */
          row.push(order.total - order.deposit)
          /* % hoa hồng */
          row.push(orderProduct.product.commissionPercent || 0)
          /* Hoa hồng sản phẩm */
          row.push(orderProduct.commission)
          /* Phí vận chuyển */
          const totalFee =
            order.shippingInfos?.reduce((total, curr) => {
              return total + Number(curr?.fee || 0)
            }, 0) || 0
          row.push(totalFee)
          /* Đơn vị vận chuyển */
          const shippingPartner = order.shippingInfos?.reduce((name, curr) => {
            return (
              name +
              (curr?.partner ? getShippingPartnerName(curr?.partner) + ',' : '')
            )
          }, '')
          row.push(shippingPartner)
          /* Trạng thái vận chuyển */
          row.push(getOrderShippingStatus(order.shippingInfos?.[0]?.status))
          /* Khách hàng */
          row.push(order.fullname)
          /* SĐT Khách hàng */
          row.push(order.tel)
          /* Địa chỉ giao hàng */
          row.push(order.shippingAddress)
          /* Phường */
          row.push(order.ward?.name)
          /* Quận */
          row.push(order.ward?.district?.name)
          /* Thành phố */
          row.push(order.ward?.district?.province?.name)
          /* Tên CTV */
          row.push(order.user?.fullName)
          /* SĐT CTV */
          row.push(order.user?.tel)
          /* STK ngân hàng */
          row.push(order.user?.bankNumber)
          /* NV phụ trách */
          const leaderIds = userResponse.data?.map((u) => u.id)
          const leader = order.user?.leaders?.find((leader) =>
            leaderIds.includes(leader.id)
          )
          row.push(leader ? leader.fullName : '')
          /* Trạng thái đơn hàng */
          row.push(getOrderStatusDesc(order.status))
          /* Lý do hủy/hoàn */
          row.push(order.cancel_reason)
          /* Ngày đối soát */
          row.push(completedAt ? formatDate(completedAt, 'dd/MM/yyyy') : '')
          /* Mã cửa hàng */
          row.push(order.merchantAddress?.name)
          /* Ghi chú */
          row.push(order.note)
          /* Mã đơn MISA */
          // row.push(order.importCode)
          worksheet.addRow(row).border = {
            top: { style: 'thin' },
            left: { style: 'thin' },
            bottom: { style: 'thin' },
            right: { style: 'thin' },
          }
        })
        if (order.orderProducts?.length > 1) {
          worksheet.mergeCells(
            `M${lastIndex - order.orderProducts?.length + 1}:M${lastIndex}`
          )
          worksheet.getCell(`M${lastIndex}`).value = order.discount
          worksheet.getCell(`M${lastIndex}`).alignment = { vertical: 'middle' }

          worksheet.mergeCells(
            `Q${lastIndex - order.orderProducts?.length + 1}:Q${lastIndex}`
          )
          worksheet.getCell(`Q${lastIndex}`).value = order.deposit
          worksheet.getCell(`Q${lastIndex}`).alignment = { vertical: 'middle' }

          worksheet.mergeCells(
            `R${lastIndex - order.orderProducts?.length + 1}:R${lastIndex}`
          )
          worksheet.getCell(`R${lastIndex}`).value = order.total - order.deposit
          worksheet.getCell(`R${lastIndex}`).alignment = { vertical: 'middle' }
        }
        worksheet.getColumn('J').numFmt = '#,###'
        worksheet.getColumn('K').numFmt = '#,###'
        worksheet.getColumn('L').numFmt = '#,###'
        worksheet.getColumn('M').numFmt = '#,###'
        worksheet.getColumn('N').numFmt = '#,##0.00'
        worksheet.getColumn('O').numFmt = '#,###'
        worksheet.getColumn('Q').numFmt = '#,###'
        worksheet.getColumn('R').numFmt = '#,###'
        worksheet.getColumn('T').numFmt = '#,##0.00'
        worksheet.getColumn('U').numFmt = '#,###'
        // worksheet.getColumn('U').numFmt = '#,###'
      })
      workbook.xlsx.writeBuffer().then((xls64) => {
        const a = document.createElement('a')
        const data = new Blob([xls64], {
          type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        })

        const url = URL.createObjectURL(data)
        a.href = url
        a.download = `${filename} - ${formatDate(new Date())}`
        document.body.appendChild(a)
        a.click()
        setTimeout(function () {
          document.body.removeChild(a)
          window.URL.revokeObjectURL(url)
        }, 200)
      })
    })
  }

  const upLoadFile = (options: UploadRequestOption) => {
    const { onSuccess, onError, file } = options
    setIsLoading(true)
    OrdersService.orderControllerImportOrder({ file: file })
      .then((response) => {
        setIsLoading(false)
        onSuccess('OK')
        alertSuccess(
          `Tải lên thành công ${response.totalRecordSuccess} bản ghi`
        )
        resetForm()
        if (response.importOrderErrors?.length > 0) {
          const workbook = new ExcelJS.Workbook()
          const worksheet = workbook.addWorksheet('Danh sách mã đơn hàng lỗi')
          const filename = `Danh sách đơn hàng lỗi - ${format(
            new Date(),
            'd_m_Y_h_i_s'
          )}`
          /* Header */
          const headerRows = [
            'Mã kho hàng',
            'Số HĐ',
            'Mã SKU',
            'Ngày tạo đơn',
            'Ngày hóa đơn',
            'Giờ hóa đơn',
            'Đơn giá',
            'Số lượng',
            'Tiền hàng',
            'Tiền KM',
            'Doanh thu',
            'Đặt cọc',
            'Mã vận đơn',
            'ĐVVC',
            'Hình thức thanh toán',
            'Phí giao hàng',
            'Sđt khách hàng',
            'Tên KH',
            'Địa chỉ KH',
            'Phường',
            'Quận/Huyện',
            'Tỉnh/TP',
            'Tên CTV',
            'Sđt CTV',
            'Ghi chú đơn hàng',
            'Lỗi',
          ]
          worksheet.addRow(headerRows)
          worksheet.getRow(1).font = {
            size: 12,
            name: 'Times New Roman',
            bold: true,
          }
          worksheet.getRow(1).alignment = {
            horizontal: 'center',
            vertical: 'middle',
          }
          worksheet.getRow(1).fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: 'FDE9D9' },
          }
          response.importOrderErrors?.map((importError, index) => {
            const row = []
            /* Mã kho hàng */
            row.push(importError.merchantAddressCode)
            /* Số HĐ */
            row.push(importError.orderImportCode)
            /* Mã SKU */
            row.push(importError.SKU)
            /* Ngày tạo đơn */
            row.push(formatDate(importError.orderCreatedAt, 'MM/dd/yyyy'))
            /* Ngày hóa đơn */
            row.push(formatDate(importError.orderCompletedAt, 'MM/dd/yyyy'))
            /* Giờ hóa đơn */
            row.push(
              formatDate(importError.orderCompletedAt, 'MM/dd/yyyy HH:mm:ss')
            )
            /* Đơn giá */
            row.push(importError.productPrice)
            /* Số lượng */
            row.push(importError.orderProductQty)
            /* Tiền hàng */
            row.push(
              Number(importError.productPrice || 0) *
                Number(importError.orderProductQty || 0)
            )
            /* Tiền KM */
            row.push(importError.orderProductDiscount)
            /* Doanh thu */
            row.push(importError.orderProductPrice)
            /* Đặt cọc */
            row.push(importError.orderDeposit)
            /* Mã vận đơn */
            row.push(importError.orderShippingInfo?.partner_id)
            /* ĐVVC */
            row.push(importError.orderShippingInfo?.partner)
            /* Hình thức thanh toán */
            row.push(importError.orderPaymentType)
            /* Phí giao hàng */
            row.push(importError.orderShippingInfo?.fee)
            /* Sđt khách hàng */
            row.push(importError.orderTel)
            /* Tên KH */
            row.push(importError.orderFullname)
            /* Địa chỉ KH */
            row.push(importError.orderShippingAddress)
            /* Phường */
            row.push('')
            /* Quận/Huyện */
            row.push('')
            /* Tỉnh/TP */
            row.push('')
            /* Tên CTV */
            row.push(importError.partner?.fullName)
            /* Sđt CTV */
            row.push(importError.partner?.tel)
            /* Ghi chú đơn hàng */
            row.push(importError.orderNote)
            /* Lỗi */
            row.push(importError.errorMessage)
            worksheet.addRow(row)
          })
          workbook.xlsx.writeBuffer().then((xls64) => {
            const a = document.createElement('a')
            const data = new Blob([xls64], {
              type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            })

            const url = URL.createObjectURL(data)
            a.href = url
            a.download = `${filename} - ${formatDate(new Date())}`
            document.body.appendChild(a)
            a.click()
            setTimeout(function () {
              document.body.removeChild(a)
              window.URL.revokeObjectURL(url)
            }, 200)
          })
        }
      })
      .catch((er) => {
        setIsLoading(false)
        onError(er)
        alertError(er)
      })
  }

  const findImportOrder = (options: UploadRequestOption) => {
    const { onSuccess, onError, file } = options
    setIsLoading(true)
    OrdersService.orderControllerFindImportOrder({ file: file })
      .then((response) => {
        setIsLoading(false)
        onSuccess('OK')
        if (response.orders?.length > 0) {
          const workbook = new ExcelJS.Workbook()
          const worksheet = workbook.addWorksheet('Danh sách đơn hàng')
          const filename = `Danh sách đơn hàng tìm kiếm - ${format(
            new Date(),
            'd_m_Y_h_i_s'
          )}`
          /* Header */
          const headerRows = [
            'Mã đơn hàng',
            'Số HĐ',
            'Trạng thái',
            'Nhà CC',
            'Tổng giá trị',
            'Giá trị tính',
          ]
          worksheet.addRow(headerRows)
          worksheet.getRow(1).font = {
            size: 12,
            name: 'Times New Roman',
            bold: true,
          }
          worksheet.getRow(1).alignment = {
            horizontal: 'center',
            vertical: 'middle',
          }
          worksheet.getRow(1).fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: 'FDE9D9' },
          }
          response.orders?.map((order: OrderEntity, index) => {
            const row = []
            /* Mã đơn hàng */
            row.push(order.code)
            /* Số HĐ */
            row.push(order.importCode)
            /* Trạng thái */
            row.push(getOrderStatusDesc(order.status))
            /* Nhà CC */
            row.push(order.merchantId)
            /* Tổng giá trị */
            row.push(formatNumber(order.total))
            worksheet.addRow(row)
          })
          workbook.xlsx.writeBuffer().then((xls64) => {
            const a = document.createElement('a')
            const data = new Blob([xls64], {
              type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            })

            const url = URL.createObjectURL(data)
            a.href = url
            a.download = `${filename} - ${formatDate(new Date())}`
            document.body.appendChild(a)
            a.click()
            setTimeout(function () {
              document.body.removeChild(a)
              window.URL.revokeObjectURL(url)
            }, 200)
          })
        }
      })
      .catch((er) => {
        setIsLoading(false)
        onError(er)
        alertError(er)
      })
  }
  // @ts-ignore
  return (
    <Content
      title={title}
      endButtons={[
        {
          text: 'Thêm mới',
          href: '/order/create',
          type: 'primary',
          visible: true,
        },
      ]}
      endElement={
        <>
          <div className={'flex gap-1 justify-end items-center'}>
            <label className={'text-slate-400'}>Ngày tạo đơn</label>
            <Controller
              control={control}
              name={'dates'}
              render={({ field }) => (
                <DatePicker.RangePicker
                  format={'d/MM/y'}
                  allowClear={false}
                  renderExtraFooter={() => (
                    <>
                      <Button
                        type={'primary'}
                        size={'small'}
                        ghost={true}
                        className={'bg-[#E2F1FF] rounded mr-1'}
                        onClick={() => {
                          onRangePickerChange([
                            startOfDay(new Date()).toString(),
                            endOfDay(new Date()).toString(),
                          ])
                          setValue('dates', [
                            startOfDay(new Date()),
                            endOfDay(new Date()),
                          ])
                        }}
                      >
                        Hôm nay
                      </Button>
                      <Button
                        type={'primary'}
                        size={'small'}
                        ghost={true}
                        className={'bg-[#E2F1FF] rounded mr-1'}
                        onClick={() => {
                          onRangePickerChange([
                            startOfDay(addDays(new Date(), -1)).toString(),
                            endOfDay(addDays(new Date(), -1)).toString(),
                          ])
                          setValue('dates', [
                            startOfDay(addDays(new Date(), -1)),
                            endOfDay(addDays(new Date(), -1)),
                          ])
                        }}
                      >
                        Hôm qua
                      </Button>
                      <Button
                        type={'primary'}
                        size={'small'}
                        ghost={true}
                        className={'bg-[#E2F1FF] rounded mr-1'}
                        onClick={() => {
                          onRangePickerChange([
                            startOfWeek(new Date()).toString(),
                            endOfWeek(new Date()).toString(),
                          ])
                          setValue('dates', [
                            startOfWeek(new Date()),
                            endOfWeek(new Date()),
                          ])
                        }}
                      >
                        Tuần này
                      </Button>
                      <Button
                        type={'primary'}
                        size={'small'}
                        ghost={true}
                        className={'bg-[#E2F1FF] rounded mr-1'}
                        onClick={() => {
                          onRangePickerChange([
                            startOfWeek(addWeeks(new Date(), -1)).toString(),
                            endOfWeek(addWeeks(new Date(), -1)).toString(),
                          ])
                          setValue('dates', [
                            startOfWeek(addWeeks(new Date(), -1)),
                            endOfWeek(addWeeks(new Date(), -1)),
                          ])
                        }}
                      >
                        Tuần trước
                      </Button>
                      <Button
                        type={'primary'}
                        size={'small'}
                        ghost={true}
                        className={'bg-[#E2F1FF] rounded mr-1'}
                        onClick={() => {
                          onRangePickerChange([
                            startOfMonth(new Date()).toString(),
                            endOfMonth(new Date()).toString(),
                          ])
                          setValue('dates', [
                            startOfMonth(new Date()),
                            endOfMonth(new Date()),
                          ])
                        }}
                      >
                        Tháng này
                      </Button>
                      <Button
                        type={'primary'}
                        size={'small'}
                        ghost={true}
                        className={'bg-[#E2F1FF] rounded mr-1'}
                        onClick={() => {
                          onRangePickerChange([
                            startOfMonth(addMonths(new Date(), -1)).toString(),
                            endOfMonth(addMonths(new Date(), -1)).toString(),
                          ])
                          setValue('dates', [
                            startOfMonth(addMonths(new Date(), -1)),
                            endOfMonth(addMonths(new Date(), -1)),
                          ])
                        }}
                      >
                        Tháng trước
                      </Button>
                      <Button
                        type={'primary'}
                        size={'small'}
                        ghost={true}
                        className={'bg-[#E2F1FF] rounded mr-1'}
                        onClick={() => {
                          onRangePickerChange([
                            startOfQuarter(new Date()).toString(),
                            endOfQuarter(new Date()).toString(),
                          ])
                          setValue('dates', [
                            startOfQuarter(new Date()),
                            endOfQuarter(new Date()),
                          ])
                        }}
                      >
                        Quý này
                      </Button>
                      <Button
                        type={'primary'}
                        size={'small'}
                        ghost={true}
                        className={'bg-[#E2F1FF] rounded mr-1'}
                        onClick={() => {
                          onRangePickerChange([
                            startOfQuarter(
                              addQuarters(new Date(), -1)
                            ).toString(),
                            endOfQuarter(
                              addQuarters(new Date(), -1)
                            ).toString(),
                          ])
                          setValue('dates', [
                            startOfQuarter(addQuarters(new Date(), -1)),
                            endOfQuarter(addQuarters(new Date(), -1)),
                          ])
                        }}
                      >
                        Quý trước
                      </Button>
                      <Button
                        type={'primary'}
                        size={'small'}
                        ghost={true}
                        className={'bg-[#E2F1FF] rounded mr-1'}
                        onClick={() => {
                          onRangePickerChange([
                            startOfYear(new Date()).toString(),
                            endOfYear(new Date()).toString(),
                          ])
                          setValue('dates', [
                            startOfYear(new Date()),
                            endOfYear(new Date()),
                          ])
                        }}
                      >
                        Năm này
                      </Button>
                      <Button
                        type={'primary'}
                        size={'small'}
                        ghost={true}
                        className={'bg-[#E2F1FF] rounded mr-1'}
                        onClick={() => {
                          onRangePickerChange([
                            startOfYear(addYears(new Date(), -1)).toString(),
                            endOfYear(addYears(new Date(), -1)).toString(),
                          ])
                          setValue('dates', [
                            startOfYear(addYears(new Date(), -1)),
                            endOfYear(addYears(new Date(), -1)),
                          ])
                        }}
                      >
                        Năm trước
                      </Button>
                    </>
                  )}
                  {...field}
                  style={{
                    width: '100%',
                  }}
                  onCalendarChange={onRangePickerChange}
                />
              )}
            />
            <Button
              type={'primary'}
              onClick={onDownload}
              loading={isDownloading}
              disabled={!watchDates || watchDates?.length <= 0}
            >
              <DownloadOutlined />
            </Button>
          </div>
        </>
      }
    >
      <Card>
        <Menu
          selectedKeys={[selectedMenu]}
          mode="horizontal"
          className={'w-full'}
        >
          {[
            'all',
            EnumOrderEntityStatus.waiting_for_grouping,
            EnumOrderEntityStatus.created,
            EnumOrderEntityStatus.confirmed,
            EnumOrderEntityStatus.shipping,
            EnumOrderEntityStatus.shipped,
            EnumOrderEntityStatus.completed,
            EnumOrderEntityStatus.cancelled,
            // EnumOrderEntityStatus.refunded,
            EnumOrderEntityStatus.return,
          ].map((status) => {
            let count = 0
            if (status === EnumOrderEntityStatus.cancelled) {
              count = statusCount
                .filter((count) =>
                  [
                    EnumStatusCountResponseDtoStatus.cancelled,
                    EnumStatusCountResponseDtoStatus.merchant_cancelled,
                  ].includes(count.status)
                )
                .reduce((total, count) => {
                  return total + Number(count.count)
                }, 0)
            } else {
              count =
                statusCount.filter((count) => count.status === status)?.[0]
                  ?.count || 0
            }
            return (
              <Menu.Item
                key={status}
                // onClick={() => router.push()}
              >
                <Link href={`${router.route}/?status=${status}`}>
                  {status === 'all' ? 'Tất cả' : getOrderStatusDesc(status)}{' '}
                  {count !== 0 && <Tag className={'rounded-full'}>{count}</Tag>}
                </Link>
              </Menu.Item>
            )
          })}
        </Menu>
        <Form className={'mt-6'} onFinish={handleSubmit(onSubmit)}>
          {/*<div className={'flex gap-4 justify-end'}>*/}
          {/*  <FormItem label={'Ngày đặt hàng'}>*/}
          {/*    <Controller*/}
          {/*      control={control}*/}
          {/*      name={'dates'}*/}
          {/*      render={({ field }) => (*/}
          {/*        <DatePicker.RangePicker*/}
          {/*          format={'d/MM/y'}*/}
          {/*          {...field}*/}
          {/*          style={{*/}
          {/*            width: '100%',*/}
          {/*          }}*/}
          {/*        />*/}
          {/*      )}*/}
          {/*    />*/}
          {/*  </FormItem>*/}
          {/*  <Button*/}
          {/*    type={'primary'}*/}
          {/*    onClick={onDownload}*/}
          {/*    loading={isDownloading}*/}
          {/*    disabled={!watchDates || watchDates?.length <= 0}*/}
          {/*  >*/}
          {/*    <DownloadOutlined />*/}
          {/*  </Button>*/}
          {/*</div>*/}
          <div
            className={'flex gap-4'}
            style={{ justifyContent: 'space-between' }}
          >
            <Input.Group compact style={{ width: '60%' }}>
              <Controller
                control={control}
                name={'type'}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="Tìm kiếm theo"
                    style={{ width: '30%' }}
                    defaultValue="id"
                  >
                    <Select.Option value="code">Mã đơn hàng</Select.Option>
                    <Select.Option value="fullname">
                      Tên khách hàng
                    </Select.Option>
                    <Select.Option value="tel">SĐT khách hàng</Select.Option>
                    <Select.Option value="partner">
                      SĐT Cộng tác viên
                    </Select.Option>
                    <Select.Option value="partner_name">
                      Tên Cộng tác viên
                    </Select.Option>
                    <Select.Option value="merchant_address">
                      Tên kho hàng
                    </Select.Option>
                    {/*<Select.Option value="group_name">Tên nhóm</Select.Option>*/}
                  </Select>
                )}
              />
              <Controller
                control={control}
                name={'q'}
                render={({ field }) => (
                  <Input
                    {...field}
                    // suffix={<SearchOutlined />}
                    style={{ width: '70%' }}
                    placeholder="Nhập vào"
                  />
                )}
              />
            </Input.Group>
            <Form.Item style={{ width: '30%' }}>
              <Controller
                control={control}
                name={'shippingStatus'}
                render={({ field }) => (
                  <Select
                    {...field}
                    placeholder="Tìm kiếm theo trạng thái vận đơn"
                    allowClear={true}
                  >
                    {Object.keys(SHIPPING_STATUS).map((shippingStatus) => (
                      <Select.Option
                        value={shippingStatus}
                        key={`tbl-sl-shipping-${shippingStatus}-partner-${shippingStatus}`}
                      >
                        {SHIPPING_STATUS[shippingStatus]}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              />
            </Form.Item>
            <Form.Item>
              <Space>
                <Button type={'primary'} htmlType={'submit'}>
                  Tìm
                </Button>
                <Button onClick={resetForm}>Nhập lại</Button>
              </Space>
            </Form.Item>
          </div>
        </Form>
      </Card>
      <Card
        title={<span className={'text-2xl'}>{countDatatable} Đơn hàng</span>}
        extra={
          <>
            <Space>
              {user.type === EnumRoleEntityType.admin && (
                <>
                  {process.env.NEXT_PUBLIC_ENV !== 'production' && (
                    <Button
                      onClick={onTemplateDownload}
                      // https://ushare-dev.hn.ss.bfcplatform.vn/template/order_import_template.xlsx
                    >
                      <DownloadOutlined /> Download template
                    </Button>
                  )}
                  {process.env.NEXT_PUBLIC_ENV !== 'production' && (
                    <Upload
                      name={'file'}
                      maxCount={1}
                      customRequest={upLoadFile}
                      accept={
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                      }
                      showUploadList={false}
                    >
                      <Button icon={<UploadOutlined />}>Import đơn hàng</Button>
                    </Upload>
                  )}
                  {/*<Upload*/}
                  {/*  name={'file'}*/}
                  {/*  maxCount={1}*/}
                  {/*  customRequest={findImportOrder}*/}
                  {/*  accept={*/}
                  {/*    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'*/}
                  {/*  }*/}
                  {/*  showUploadList={false}*/}
                  {/*>*/}
                  {/*  <Button icon={<UploadOutlined />}>Tìm kiếm đơn</Button>*/}
                  {/*</Upload>*/}
                </>
              )}
              {hasPermission(user, [
                'orders_OrderController_calculateFee',
                'orders_OrderController_createShipment',
              ]) &&
                status === 'confirmed' && (
                  <Button
                    size={'large'}
                    type={'primary'}
                    icon={<InboxOutlined />}
                    onClick={batchDelivery}
                    disabled={selectedRowKeys.length === 0}
                  >
                    Giao hàng loạt
                  </Button>
                )}
              {status === 'shipping' && (
                <Button
                  size={'large'}
                  type={'primary'}
                  icon={<PrinterOutlined />}
                  onClick={printInvoice}
                  disabled={selectedRowKeys.length === 0}
                >
                  In phiếu
                </Button>
              )}
              {EnumRoleEntityType.accountant === user?.type && (
                <Button
                  size={'large'}
                  type={'primary'}
                  ghost={true}
                  onClick={() => {
                    setVisibleChangeDeposit(true)
                  }}
                  disabled={selectedRowKeys.length === 0}
                >
                  Chuyển trạng thái cọc
                </Button>
              )}
            </Space>
          </>
        }
      >
        <DataTable
          scroll={{
            x: 1500,
          }}
          service={OrdersService.getManyBase}
          serviceParams={tableServiceParams}
          columns={columns}
          path={'/order'}
          rowSelection={rowSelection}
        />
      </Card>
      <div style={{ display: 'none' }}>
        <OrdersPrint
          orders={ordersToPrint}
          ref={ordersPrintRef}
          bankInfo={bankInfo}
        />
      </div>
      <Modal
        title="Thông tin Vận chuyển"
        visible={isBatchDeliveryVisible}
        onCancel={() => setIsBatchDeliveryVisible(false)}
        width={1600}
        closable={false}
        maskClosable={false}
        destroyOnClose={true}
        footer={false}
      >
        <Spin spinning={isLoading}>
          <BatchDelivery
            orderIds={deliveryIds}
            onSuccess={() => {
              router.reload()
            }}
            onCancel={() => setIsBatchDeliveryVisible(false)}
          />
        </Spin>
      </Modal>
      <Modal
        title="Chuyển trạng thái đặt cọc"
        visible={visibleChangeDeposit}
        onCancel={() => setVisibleChangeDeposit(false)}
        width={500}
        closable={true}
        maskClosable={false}
        destroyOnClose={true}
        footer={false}
      >
        <Row gutter={[24, 24]} className={'w-full items-center justify-center'}>
          <Col className={'flex items-center justify-center'}>
            Trạng thái mới
          </Col>
          <Col>
            <Controller
              control={control}
              name={'paymentStatus'}
              render={({ field }) => (
                <Select {...field}>
                  {Object.keys(EnumOrderEntityPaymentStatus).map((status) => (
                    <Select.Option value={status}>
                      {getPaymentStatusDesc(
                        EnumOrderEntityPaymentStatus[status]
                      )}
                    </Select.Option>
                  ))}
                </Select>
              )}
            />
          </Col>
        </Row>
        <Row gutter={[24, 24]} className={'mt-7'}>
          <Col
            xs={24}
            md={24}
            lg={24}
            xl={24}
            className={'w-full flex items-center justify-end'}
          >
            <Button type={'primary'} onClick={onUpdatePaymentStatus}>
              Xác nhận
            </Button>
          </Col>
        </Row>
      </Modal>
      <Modal
        title={
          <>
            <div className={'flex flex-row items-center justify-start'}>
              <div className={'mr-10'}>
                Nhóm:{' '}
                <b className={'pl-1.5'}>{orderGroupBuying?.[0]?.group?.name}</b>
              </div>
              <div className={'mr-10'}>
                Đã xác nhận cọc:{' '}
                <b className={'text-[#FF000F] pl-2'}>
                  {orderGroupBuying.reduce((total, curr) => {
                    if (
                      curr.paymentStatus ===
                      EnumOrderEntityPaymentStatus.deposited
                    )
                      return total + 1
                    return total
                  }, 0)}
                </b>
                / <b className={'text-[#4CAF50]'}>{orderGroupBuying?.length}</b>
              </div>
              <div className={'mr-8'}>
                Tỷ lệ hủy:{' '}
                <b className={'text-[#FF000F] pl-2'}>
                  {Math.round(
                    (orderGroupBuying.reduce((total, curr) => {
                      if (
                        [
                          EnumOrderEntityStatus.cancelled,
                          EnumOrderEntityStatus.merchant_cancelled,
                        ].includes(curr.status)
                      )
                        return total + 1
                      return total
                    }, 0) || 0 / Number(orderGroupBuying?.length || 1)) * 100
                  )}
                </b>
                %
              </div>
            </div>
          </>
        }
        visible={visibleDetailGroupBuying}
        onCancel={() => setVisibleDetailGroupBuying(false)}
        width={1500}
        closable={true}
        maskClosable={false}
        destroyOnClose={true}
        footer={false}
      >
        <div className={'mb-4'}>
          {EnumRoleEntityType.accountant === user?.type && (
            <Button
              type={'primary'}
              ghost={true}
              disabled={selectedModalRowKeys?.length === 0}
              onClick={() => {
                setVisibleChangeDeposit(true)
              }}
            >
              Chuyển trạng thái cọc
            </Button>
          )}
        </div>
        <Table
          dataSource={orderGroupBuying}
          loading={isLoading}
          pagination={{
            ...paginationModalGroupBuying,
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} của ${Intl.NumberFormat('vi-VN').format(
                total
              )}`,
            showSizeChanger: true,
            onChange: (page, size) => {
              setTableModalServiceParams((prevState) => {
                return {
                  ...prevState,
                  page: page,
                  limit: size,
                }
              })
            },
          }}
          scroll={{ x: 1000 }}
          size={'small'}
          columns={modalColums}
          rowSelection={rowModalSelection}
        />
      </Modal>
    </Content>
  )
}
export default Order

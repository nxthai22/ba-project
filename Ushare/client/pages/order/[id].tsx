import Icon, {
  CaretRightOutlined,
  CheckCircleOutlined,
  DeleteOutlined,
  DollarCircleOutlined,
  EnvironmentOutlined,
  FormOutlined,
  GiftFilled,
  HistoryOutlined,
  HomeOutlined,
  NumberOutlined,
  PrinterOutlined,
  UsergroupAddOutlined,
  UserOutlined,
} from '@ant-design/icons'
import { yupResolver } from '@hookform/resolvers/yup'
import {
  Alert,
  Button,
  Card,
  Col,
  Collapse,
  Descriptions,
  Form,
  InputNumber,
  List,
  Menu,
  Modal,
  Popconfirm,
  Radio,
  Row,
  Select,
  Space,
  Spin,
  Table,
} from 'antd'
import { useRouter } from 'next/router'
import { FC, useEffect, useRef, useState } from 'react'
import { Controller, SubmitHandler, useForm, useWatch } from 'react-hook-form'
import { MdOutlineLocalShipping } from 'react-icons/md'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { authState, loadingState } from 'recoil/Atoms'
import {
  alertError,
  alertSuccess,
  filterOption,
  formatCurrency,
  formatDate,
  formatNumber,
  getFullAddress,
  getOrderStatusDesc,
  getPaymentStatusDesc,
  getShippingPartnerName,
  hasPermission,
  modifyEntity,
} from 'utils'
import * as yup from 'yup'
import { SchemaOf } from 'yup'

import {
  BankInfo,
  BankService,
  CalculateVoucherDto,
  ConfigService,
  ConfirmDepositDto,
  CreateOrderDto,
  CreateOrderProduct,
  DetailProudct,
  EnumChangeStatusDtoStatus,
  EnumCreateOrderDtoPaymentType,
  EnumOrderEntityPaymentStatus,
  EnumOrderEntityPaymentType,
  EnumOrderEntityStatus,
  EnumOrderHistoryEntityStatus,
  EnumRoleEntityType,
  EnumShippingInfoPartner,
  EnumShippingInfoStatus,
  MerchantAddressEntity,
  MerchantAddressesService,
  OrderEntity,
  OrderProductEntity,
  OrdersService,
  ProductEntity,
  ProductsService,
  ProductStockEntity,
  ProductVariantEntity,
  UpdateOrderDto,
  UserEntity,
  UsersService,
  VoucherEntity,
  VouchersService,
} from 'services'

import TextArea from 'antd/lib/input/TextArea'
import BatchDelivery from 'components/common/BatchDelivery'
import DebounceSelect from 'components/common/DebounceSelect'
import FormItem from 'components/common/FormItem'
import Image from 'components/common/Image'
import OrdersPrint from 'components/common/OrdersPrint'
import CustomerAddress from 'components/customer/customer-address/customer-address'
import Content from 'components/layout/AdminLayout/Content'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import { useReactToPrint } from 'react-to-print'
import OrderStep from 'components/admin/Order/Id/OrderStep'
import { OrderStepHistory } from 'components/admin/Order/Id/OrderStepHistory'

const FAILED_STATUS = [
  EnumOrderHistoryEntityStatus.cancelled,
  EnumOrderHistoryEntityStatus.refunded,
  EnumOrderHistoryEntityStatus.merchant_cancelled,
  EnumOrderHistoryEntityStatus.failed,
  EnumOrderHistoryEntityStatus.return,
]

interface Inputs {
  order?: CreateOrderDto | UpdateOrderDto
  provinceId?: number
  districtId?: number
  wardId: number
  paymentType?: EnumCreateOrderDtoPaymentType
  tel: string
  shippingAddress: string
  fullname: string
  productSelected?: {
    value: number
  }
  productQuanity?: number
  cancel_reason?: string
  deposit?: number
  voucherMerchantId?: number
  voucherUshareId?: number
  productChange?: number
  voucherMerchantChange?: number
  voucherUshareChange?: number
}

const phoneRegExp = /([+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/
const schema: SchemaOf<Inputs> = yup.object().shape({
  order: yup.object().shape({
    merchantAddressId: yup.number(),
    wardId: yup.number(),
    fullname: yup.string(),
    shippingAddress: yup.string(),
    tel: yup.string(),
    note: yup.string().nullable(),
    paymentType: yup
      .mixed<EnumCreateOrderDtoPaymentType>()
      .oneOf(Object.values(EnumCreateOrderDtoPaymentType)),
    pubId: yup?.number(),
    products: yup.array().of(
      yup.object().shape({
        id: yup.number(),
        quantity: yup.number(),
        variantId: yup.number(),
      })
    ),
  }),
  productSelected: yup
    .object()
    .shape({
      value: yup.number(),
    })
    .nullable(true),
  cancel_reason: yup.string(),
  productQuanity: yup
    .number()
    .typeError('Chưa nhập số lượng')
    .min(1, 'Số lượng ít nhất phải là 1')
    .required('Chưa nhập số lượng'),
  provinceId: yup.number(),
  districtId: yup.number(),
  paymentType: yup
    .mixed<EnumCreateOrderDtoPaymentType>()
    .oneOf(Object.values(EnumCreateOrderDtoPaymentType)),
  fullname: yup.string().required('Chưa nhập Họ tên Khách hàng'),
  tel: yup
    .string()
    .required('Chưa nhập số điện thoại')
    .matches(phoneRegExp, 'Số điện thoại không hợp lệ'),
  shippingAddress: yup.string().required('Chưa nhập địa chỉ giao hàng.'),
  wardId: yup
    .number()
    .required('Chưa chọn Phường/Xã giao hàng.')
    .typeError('Chưa chọn Phường/Xã giao hàng.'),
})

const Detail: FC = () => {
  const router = useRouter()
  const { id } = router.query
  const [title, setTitle] = useState('Tạo đơn hàng')
  const [order, setOrder] = useState<OrderEntity>()
  const setIsLoading = useSetRecoilState(loadingState)
  const isLoading = useRecoilValue(loadingState)
  const {
    control,
    setValue,
    getValues,
    formState: { errors },
    handleSubmit,
    setError,
    clearErrors,
  } = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
    defaultValues: {
      order: {},
      tel: '',
      shippingAddress: '',
      productSelected: {},
      productQuanity: 1,
      fullname: '',
      paymentType: EnumCreateOrderDtoPaymentType.cod,
    },
  })
  const [isUserFormVisible, setIsUserFormVisible] = useState(false)
  const [isCustomerFormVisible, setIsCustomerFormVisible] = useState(false)
  const [isAddProductFormVisible, setIsAddProductFormVisible] = useState(false)
  const [isPromotionFormVisible, setIsPromotionFormVisible] = useState(false)
  const [
    isChangeMerchantAddressFormVisible,
    setIsChangeMerchantAddressFormVisible,
  ] = useState(false)
  const [merchantAddresses, setMerchantAddress] = useState<
    MerchantAddressEntity[]
  >([])
  const [collaborators, setCollaborators] = useState<UserEntity[]>([])
  const [collaborator, setCollaborator] = useState<UserEntity>()
  const [voucherMerchants, setVoucherMerchants] = useState<VoucherEntity[]>([])
  const [voucherUshares, setVoucherUshares] = useState<VoucherEntity[]>([])
  const [voucherMerchantId, setVoucherMerchantId] = useState<number>(0)
  const [voucherUshareId, setVoucherUshareId] = useState<number>(0)

  const [currentStep, setCurrentStep] = useState(-1)
  const [products, setProducts] = useState<OrderProductEntity[]>([])
  const [product, setProduct] = useState<DetailProudct>()
  const [variant, setVariant] = useState<ProductVariantEntity>()
  const [isDeliveryVisible, setIsDeliveryVisible] = useState<boolean>(false)
  const [addressInfo, setAddressInfo] = useState<Inputs>()
  const [isShowModalMerchantCancel, setIsShowModalMerchantCancel] =
    useState(false)
  const [isShowModalCancel, setIsShowModalCancel] = useState(false)
  const [isShowModalReturn, setIsShowModalReturn] = useState(false)
  const [isShowModalConfirmDeposit, setIsShowModalConfirmDeposit] =
    useState(false)
  const [isShowModalNote, setIsShowModalNote] = useState(false)
  const user = useRecoilValue(authState)
  const orderPrintRef = useRef(null)
  const handlePrint = useReactToPrint({
    content: () => orderPrintRef.current,
    pageStyle: `@page {
      size: auto;
      margin: 10mm 0mm;
    }
  `,
    onBeforePrint: () => {
      setTitle(
        order?.merchant?.name
          ? `${order?.merchant?.name} / Ushare / Thông tin giao hàng`
          : 'Ushare / Thông tin giao hàng'
      )
    },
    onAfterPrint: () => {
      setTitle('Chi tiết đơn hàng')
    },
  })
  const [bankInfo, setBankInfo] = useState<BankInfo>()
  const [shippingStatus, setShippingStatus] =
    useState<EnumShippingInfoStatus>(null)
  const [isOtherPartnerShipping, setIsOtherPartnerShipping] =
    useState<boolean>(false)

  const watchProductSelected = useWatch({
    control,
    name: 'productSelected',
  })

  const watchOrder = useWatch({
    control,
    name: 'order',
  })

  const watchCancelReason = useWatch({
    control,
    name: 'cancel_reason',
    defaultValue: '',
  })
  const watchOrderMerchantAddressId = useWatch({
    control,
    name: 'order.merchantAddressId',
    defaultValue: 0,
  })

  const watchProductChange = useWatch({
    control,
    name: 'productChange',
    defaultValue: 0,
  })

  const watchVoucherMerchantChange = useWatch({
    control,
    name: 'voucherMerchantChange',
    defaultValue: 0,
  })

  const watchVoucherMerchantId = useWatch({
    control,
    name: 'voucherMerchantId',
    defaultValue: 0,
  })

  const watchVoucherUshareChange = useWatch({
    control,
    name: 'voucherUshareChange',
    defaultValue: 0,
  })
  const watchVoucherUshareId = useWatch({
    control,
    name: 'voucherUshareId',
    defaultValue: 0,
  })

  useEffect(() => {
    const query = new URLSearchParams(location.search)
    if (router?.query?.id == 'create' && query.get('productId')) {
      // setValue('productSelected',query.get('productId'))
      ProductsService.getOneBase({
        id: Number(query.get('productId')),
      })
        .then((response) => {
          const tmpProduct: OrderProductEntity = {
            orderId: 0,
            productId: response.id,
            quantity: getValues('productQuanity') || 1,
            product: response as unknown as ProductEntity,
            price: response.price,
          }
          const tmpProducts: OrderProductEntity[] = []
          tmpProducts.push(tmpProduct)
          setProducts(tmpProducts)
        })
        .catch((error) => alertError(error))
    }
  }, [router?.query?.id])
  useEffect(() => {
    setOrder(watchOrder as unknown as OrderEntity)
  }, [watchOrder])

  useEffect(() => {
    if (watchProductSelected?.value) {
      ProductsService.getOneBase({
        id: watchProductSelected?.value,
        join: [`variants`, 'stocks'],
      })
        .then((response) => {
          setProduct(response)
          const stockQuantity =
            response.stock.find(
              (stock) =>
                stock.merchantAddressId === watchOrder?.merchantAddressId
            )?.quantity || 0
          if (stockQuantity >= getValues('productQuanity'))
            clearErrors('productQuanity')
        })
        .catch((error) => alertError(error))
    }
  }, [watchProductSelected])

  useEffect(() => {
    UsersService.getManyBase({
      filter: [
        `role.type||eq||${EnumRoleEntityType.partner}`,
        user.type === EnumRoleEntityType.merchant
          ? `merchantId||eq||${user.merchantId}`
          : [],
      ],
      sort: ['createdAt,DESC'],
      join: ['role'],
      limit: 1000,
    })
      .then((response) => setCollaborators(response.data))
      .catch((error) => alertError(error))
  }, [])

  // useEffect(() => {
  //   // if (products.length != 0)
  //   VouchersService.voucherControllerGetManyVoucherForGuest()
  //     .then((response) => {
  //       setVoucherUshares(response.data)
  //       const existVoucher = response?.data?.find(
  //         (voucher) => voucher.id === voucherUshareId
  //       )
  //       if (existVoucher) {
  //         setVoucherUshareId(voucherUshareId)
  //         setValue('voucherUshareId', voucherUshareId)
  //       } else {
  //         setVoucherUshareId(null)
  //         setValue('voucherUshareId', null)
  //       }
  //       setValue('voucherUshareChange', null)
  //     })
  //     .catch((error) => alertError(error))
  // }, [watchVoucherUshareChange])

  useEffect(() => {
    if (watchProductChange && products.length != 0) {
      // calculate data
      const voucherIds = []
      if (voucherMerchantId) voucherIds.push(voucherMerchantId)
      if (voucherUshareId) voucherIds.push(voucherUshareId)
      const body: CalculateVoucherDto = {
        products: products.map((op) => {
          const orderProduct: CreateOrderProduct = {
            quantity: op.quantity,
            variantId: op.variantId,
            id: op.productId,
          }
          return orderProduct
        }),
        merchantAddressId: order.merchantAddressId,
        voucherIds,
        tel: addressInfo?.tel,
        fullname: addressInfo?.fullname,
        shippingAddress: addressInfo?.shippingAddress,
      }
      VouchersService.voucherControllerCalculateDiscountOrder({
        body,
      })
        .then((response) => {
          setOrder({
            ...order,
            total: response.totalPrice,
            commission: response.totalCommission,
            voucherMerchantDiscount: response.voucherMerchantDiscount,
            voucherUshareDiscount: response.voucherUshareDiscount,
          })
          setValue('productChange', null)
        })
        .catch((error) => alertError(error))
    }
  }, [watchProductChange])
  useEffect(() => {
    if (Number(id) && Number(id) != 0) {
      setTitle('Chi tiết đơn hàng')
      setIsLoading(true)
      OrdersService.getOneBase({
        id: Number(id),
        join: [
          'user',
          'ward',
          'ward.district',
          'ward.district.province',
          'orderProducts',
          'orderProducts.product',
          'orderProducts.product.stocks',
          'orderProducts.product.stocks.merchantAddress',
          'orderProducts.variant',
          'histories',
          'histories.user',
          'histories.user.role',
          'merchant',
          'merchantAddress',
          'merchantAddress.ward',
          'merchantAddress.ward.district',
          'merchantAddress.ward.district.province',
          'group',
          'group.orders',
          'paymentHistories',
          'paymentHistories.user',
        ],
      })
        .then((response) => {
          setOrder(response)
          setProducts(response.orderProducts)
          setValue('order', response as unknown as CreateOrderDto)
          setValue('fullname', response.fullname)
          setValue('shippingAddress', response.shippingAddress)
          setValue('tel', response.tel)
          setValue('provinceId', response.ward?.district?.provinceId)
          setValue(
            'paymentType',
            response.paymentType as unknown as EnumCreateOrderDtoPaymentType
          )
          setValue('deposit', response?.deposit)
          setValue('wardId', response.wardId)
          const addressInfo: Inputs = {
            fullname: response.fullname,
            wardId: response.wardId,
            provinceId: response.ward?.district?.provinceId,
            districtId: response.ward?.districtId,
            tel: response.tel,
            shippingAddress: response.shippingAddress,
          }
          setAddressInfo(addressInfo)
          if (
            FAILED_STATUS.includes(
              response.status as unknown as EnumOrderHistoryEntityStatus
            )
          ) {
            setCurrentStep(response?.histories?.length - 1)
          } else {
            switch (response.status) {
              case EnumOrderEntityStatus.confirmed:
                setCurrentStep(1)
                break
              case EnumOrderEntityStatus.shipping:
                setCurrentStep(2)
                break
              case EnumOrderEntityStatus.shipped:
                setCurrentStep(3)
                break
              case EnumOrderEntityStatus.completed:
                setCurrentStep(4)
                break
              default:
                setCurrentStep(0)
            }
          }
          if (response.orderProducts?.length > 0) {
            const merchantAddress = response.orderProducts?.reduce(
              (arr: ProductStockEntity[], orderProduct) => {
                const merchantAddresses = orderProduct.product?.stocks?.filter(
                  (st) =>
                    !arr?.find(
                      (x) => x.merchantAddressId === st.merchantAddressId
                    )
                )
                if (merchantAddresses?.length > 0) {
                  arr.push(...merchantAddresses)
                }
                return arr
              },
              []
            )
            if (merchantAddress?.length > 0) {
              setMerchantAddress(
                merchantAddress.map((addr) => addr.merchantAddress)
              )
            }
          }
          if (
            response.status === EnumOrderEntityStatus.shipping &&
            response?.shippingInfos?.length > 0
          )
            response.shippingInfos?.map((shipInfo) => {
              if (shipInfo?.status && !shippingStatus)
                setShippingStatus(shipInfo.status)
              if (shipInfo?.partner === EnumShippingInfoPartner.other)
                setIsOtherPartnerShipping(true)
            })
          else {
            setShippingStatus(null)
            setIsOtherPartnerShipping(false)
          }
          setIsLoading(false)
          /* Danh sách voucher */
          const now = new Date()
          Promise.all([
            VouchersService.getManyBase({
              productIds: products?.map((p) => p.productId)?.join(','),
              filter: [
                `merchantId||eq||${response.merchantId}`,
                `startDate||lt||${now.toISOString()}`,
                `endDate||gt||${now.toISOString()}`,
              ],
              sort: ['createdAt,DESC'],
            }),
            VouchersService.getManyBase({
              filter: [
                `merchantId||eq||${response.merchantId}`,
                `productIds||isnull`,
                `startDate||lt||${now.toISOString()}`,
                `endDate||gt||${now.toISOString()}`,
              ],
              sort: ['createdAt,DESC'],
            }),
            VouchersService.getManyBase({
              filter: [
                `merchantId||isnull`,
                `startDate||lt||${now.toISOString()}`,
                `endDate||gt||${now.toISOString()}`,
              ],
              sort: ['createdAt,DESC'],
            }),
          ])
            .then(([response, voucherShop, voucherUshare]) => {
              setVoucherMerchants([...response?.data, ...voucherShop?.data])
              setVoucherUshares([...voucherUshare?.data])
              let existVoucher = response?.data?.find(
                (voucher) => voucher.id === voucherMerchantId
              )
              if (!existVoucher)
                existVoucher = voucherShop?.data?.find(
                  (voucher) => voucher.id === voucherMerchantId
                )
              if (existVoucher) {
                setVoucherMerchantId(voucherMerchantId)
                setValue('voucherMerchantId', voucherMerchantId)
              } else {
                setVoucherMerchantId(null)
                setValue('voucherMerchantId', null)
              }
              setValue('voucherMerchantChange', null)
            })
            .catch((error) => alertError(error))
        })
        .catch((error) => {
          setIsLoading(false)
          alertError(error)
        })
    } else {
      MerchantAddressesService.getManyBase({
        filter: [
          user.merchantId ? `merchantId||eq||${user.merchantId}` : [],
          `status||eq||active`,
        ],
      }).then((response) => {
        setMerchantAddress(response.data)
      })
    }
  }, [id])

  useEffect(() => {
    const now = new Date()
    if (id === 'create' && products.length != 0)
      Promise.all([
        VouchersService.getManyBase({
          productIds: products?.map((p) => p.productId)?.join(','),
          filter: [
            `merchantId||eq||${user.merchantId}`,
            `startDate||lt||${now.toISOString()}`,
            `endDate||gt||${now.toISOString()}`,
          ],
          sort: ['createdAt,DESC'],
        }),
        VouchersService.getManyBase({
          filter: [
            `merchantId||eq||${user.merchantId}`,
            `productIds||isnull`,
            `startDate||lt||${now.toISOString()}`,
            `endDate||gt||${now.toISOString()}`,
          ],
          sort: ['createdAt,DESC'],
        }),
        VouchersService.getManyBase({
          filter: [
            `merchantId||isnull`,
            `endDate||gte||${now.toISOString()}`,
            `startDate||lte||${now.toISOString()}`,
          ],
          sort: ['createdAt,DESC'],
        }),
      ])
        .then(([response, voucherShop, voucherUshare]) => {
          setVoucherMerchants([...response.data, ...voucherShop.data])
          setVoucherUshares([...voucherUshare.data])
          let existVoucher = response?.data?.find(
            (voucher) => voucher.id === voucherMerchantId
          )
          if (!existVoucher)
            existVoucher = voucherShop?.data?.find(
              (voucher) => voucher.id === voucherMerchantId
            )
          if (existVoucher) {
            setVoucherMerchantId(voucherMerchantId)
            setValue('voucherMerchantId', voucherMerchantId)
          } else {
            setVoucherMerchantId(null)
            setValue('voucherMerchantId', null)
          }
          setValue('voucherMerchantChange', null)
        })
        .catch((error) => alertError(error))
  }, [watchVoucherMerchantChange, watchVoucherUshareChange])

  const printInvoice = () => {
    setIsLoading(true)
    Promise.all([
      ConfigService.getOneBase({
        id: 18,
      }),
    ])
      .then(([bankInfoConfigResponse]) => {
        const tmpBankInfo = bankInfoConfigResponse?.value as unknown as BankInfo
        BankService.getOneBase({
          id: tmpBankInfo?.bankId,
        }).then((bank) => {
          tmpBankInfo.bank = bank
          setIsLoading(false)
          setBankInfo(tmpBankInfo)
          handlePrint()
        })
      })
      .catch((e) => {
        setIsLoading(false)
        alertError(e)
      })
  }

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    let canSubmit = true
    products.forEach((product) => {
      let stockQuantity = 0
      if (product?.variant) {
        stockQuantity =
          product?.variant?.stock?.find(
            (stock) => stock.merchantAddressId === watchOrderMerchantAddressId
          )?.quantity || 0
      } else {
        stockQuantity =
          product?.product?.stock?.find(
            (stock) => stock.merchantAddressId === watchOrderMerchantAddressId
          )?.quantity || 0
      }
      if (stockQuantity < product?.quantity) {
        canSubmit = false
      }
    })
    if (!canSubmit) {
      alertError('Có sản phẩm không đủ số lượng trong Kho! Không thể lưu!')
    } else {
      data.order = {
        fullname: data.fullname,
        tel: data.tel,
        wardId: data.wardId,
        shippingAddress: data.shippingAddress,
        products: products.map((product) => ({
          id: product.productId,
          quantity: product.quantity,
          variantId: product.variantId,
        })),
        paymentType: data.paymentType,
        merchantAddressId: data?.order?.merchantAddressId,
        note: data?.order?.note,
      }
      if (String(id) === 'create') {
        const voucherIds: number[] = []
        if (voucherMerchantId) voucherIds.push(voucherMerchantId)
        if (voucherUshareId) voucherIds.push(voucherUshareId)
        data.order = [
          {
            ...data.order,
            voucherIds,
            pubId: collaborator ? collaborator.id : Number(user?.sub),
          },
        ]
      } else {
        if (Number(id) !== 0) {
          data.order = {
            ...data.order,
            id: Number(id),
          }
        }
      }
      modifyEntity(
        OrdersService,
        data.order,
        String(id) === 'create' ? 'Tạo đơn hàng' : 'Cập nhật đơn hàng',
        (response) => {
          if (response) return router.push(`/order`)
          else return router.push(`/order/${id}`)
        }
      ).then()
    }
  }

  const getProducts = (name: string) => {
    if (name !== '')
      return ProductsService.getManyBase({
        filter: [
          `name||$contL||${name}`,
          user.type === EnumRoleEntityType.admin
            ? []
            : user.merchantId
            ? `merchantId||eq||${user.merchantId}`
            : [],
        ],
      }).then((response) =>
        response.data?.map((product) => ({
          label: product.name,
          value: product.id,
        }))
      )
  }

  const changeStatus = (status: EnumChangeStatusDtoStatus) => {
    if (
      status === EnumChangeStatusDtoStatus.confirmed &&
      order?.orderProducts?.length == 0
    ) {
      alertError('Đơn hàng chưa có sản phẩm! Không thể xác nhận!')
    } else {
      if (status != EnumChangeStatusDtoStatus.merchant_cancelled) {
        setIsLoading(true)
        OrdersService.orderControllerChangeStatus({
          id: Number(id),
          body: {
            status: status,
          },
        })
          .then(() => {
            setIsLoading(false)
            alertSuccess('Cập nhật đơn hàng thành công')
            router.reload()
          })
          .catch((error) => {
            setIsLoading(false)
            alertError(error)
          })
      }
    }
  }
  const cancelOrder = (status) => {
    let alertMessage = ''
    switch (status) {
      case EnumChangeStatusDtoStatus.merchant_cancelled:
        alertMessage = 'Hủy đơn hàng thành công'
        break
      case EnumChangeStatusDtoStatus.cancelled:
        alertMessage = 'Hủy đơn hàng thành công'
        break
      case EnumChangeStatusDtoStatus.return:
        alertMessage = 'Hoàn hàng thành công'
        break
    }
    const order: UpdateOrderDto = getValues(
      'order'
    ) as unknown as UpdateOrderDto
    if (order?.id) {
      OrdersService.orderControllerChangeStatus({
        id: Number(order?.id),
        body: {
          status: status,
          cancel_reason: getValues('cancel_reason'),
        },
      })
        .then(() => {
          alertSuccess(alertMessage)
          setIsShowModalCancel(false)
          setIsShowModalMerchantCancel(false)
          router.reload()
          setIsLoading(false)
        })
        .catch((error) => {
          setIsLoading(false)
          alertError(error)
        })
    }
  }
  const changeMerchantAddress = () => {
    const order = getValues('order')
    if (!order.merchantAddressId) {
      setIsChangeMerchantAddressFormVisible(false)
      return
    }
    setIsLoading(true)
    MerchantAddressesService.getOneBase({
      id: Number(order.merchantAddressId),
    })
      .then((response) => {
        setOrder((prevState) => ({ ...prevState, merchantAddress: response }))
        products.forEach((product, productIndex) => {
          let stockQuantity = 0
          if (product?.variant) {
            stockQuantity =
              product?.variant?.stock?.find(
                (stock) =>
                  stock.merchantAddressId === watchOrderMerchantAddressId
              )?.quantity || 0
          } else {
            stockQuantity =
              product?.product?.stock?.find(
                (stock) =>
                  stock.merchantAddressId === watchOrderMerchantAddressId
              )?.quantity || 0
          }
          if (stockQuantity < product?.quantity) {
            setError(`order.products.${productIndex}`, {
              message: 'Số lượng sản phẩm trong kho không đủ',
            })
          }
        })
        setIsChangeMerchantAddressFormVisible(false)
        setIsLoading(false)
      })
      .catch((error) => {
        setIsLoading(false)
        alertError(error)
      })
  }
  const confirmDeposit = () => {
    setIsLoading(true)
    const data: ConfirmDepositDto = {
      orderId: Number(router.query?.id || 0),
      deposit: getValues('deposit'),
    }
    OrdersService.orderControllerConfirmDeposit({ body: data })
      .then((response) => {
        setIsLoading(false)
        setIsShowModalConfirmDeposit(false)
        router.reload()
        alertSuccess('Xác nhận đặt cọc thành công')
      })
      .catch((error) => {
        setIsLoading(false)
        alertError(error)
      })
  }
  const updateNote = () => {
    setIsShowModalNote(false)
    if (id !== 'create' && user.type !== EnumRoleEntityType.merchant) {
      setIsLoading(true)
      const orderUpdate = getValues('order')
      OrdersService.orderControllerUpdateNote({
        id: Number(id),
        body: orderUpdate,
      })
        .then((response) => {
          setIsLoading(false)
          alertSuccess('Cập nhật ghi chú thành công')
        })
        .catch((error) => {
          setIsLoading(false)
          alertError(error)
        })
    }
  }

  const updateShippingStatus = (status) => {
    setIsLoading(true)
    OrdersService.orderControllerUpdateShippingStatus({
      id: Number(id),
      body: {
        shippingStatus: status,
      },
    })
      .then((response) => {
        setIsLoading(false)
        setShippingStatus(status)
        alertSuccess('Cập nhật trạng thái vận chuyển thành công')
      })
      .catch((error) => {
        setIsLoading(false)
        alertError(error)
      })
  }
  return (
    <>
      <Content title={title} onBack={() => router.push('/order')}>
        <Form onFinish={handleSubmit(onSubmit)} className="px-2" colon={false}>
          {order?.id && (
            <OrderStep
              currentStep={currentStep}
              FAILED_STATUS={FAILED_STATUS}
              order={order}
            />
          )}
          <Row gutter={[16, 8]}>
            <Col xs={24} sm={24} md={20} lg={20}>
              {order?.id &&
                user?.permissions.includes(
                  'orders_OrderController_changeStatus'
                ) &&
                [
                  EnumOrderEntityStatus.confirmed,
                  EnumOrderEntityStatus.shipping,
                  EnumOrderEntityStatus.created,
                  EnumOrderEntityStatus.processing,
                  EnumOrderEntityStatus.shipped,
                ].includes(order.status) && (
                  <Card
                    title={
                      <div className={'flex items-center'}>
                        <DollarCircleOutlined style={{ color: '#1890ff' }} />{' '}
                        <span className={'ml-2'}>
                          {getOrderStatusDesc(order.status)}
                        </span>
                      </div>
                    }
                  >
                    <Card className={'bg-gray-50'}>
                      <div className={'flex items-center justify-between'}>
                        <div>
                          <div className={'uppercase text-gray-400'}>
                            Tiếp theo bạn có thể
                          </div>
                        </div>
                        <div className={'text-right'}>
                          <Space>
                            {user?.type === EnumRoleEntityType.admin ? (
                              <>
                                <Button
                                  onClick={() =>
                                    changeStatus(
                                      EnumChangeStatusDtoStatus.cancelled
                                    )
                                  }
                                >
                                  Huỷ đơn hàng
                                </Button>
                                {order.status ===
                                  EnumOrderEntityStatus.shipped && (
                                  <Button
                                    type={'primary'}
                                    onClick={() =>
                                      changeStatus(
                                        EnumChangeStatusDtoStatus.completed
                                      )
                                    }
                                  >
                                    Đã đối soát
                                  </Button>
                                )}
                              </>
                            ) : user?.type === EnumRoleEntityType.merchant ? (
                              <>
                                {[EnumOrderEntityStatus.created].includes(
                                  order.status
                                ) && (
                                  <Button
                                    onClick={() =>
                                      setIsShowModalMerchantCancel(true)
                                    }
                                  >
                                    Huỷ đơn hàng
                                  </Button>
                                )}
                                {[
                                  EnumOrderEntityStatus.confirmed,
                                  EnumOrderEntityStatus.shipping,
                                ].includes(order.status) && (
                                  <Button
                                    onClick={() => setIsShowModalCancel(true)}
                                  >
                                    Huỷ đơn hàng
                                  </Button>
                                )}
                                {order.status ===
                                  EnumOrderEntityStatus.created &&
                                  addressInfo?.wardId && (
                                    <Button
                                      type={'primary'}
                                      onClick={() =>
                                        changeStatus(
                                          EnumChangeStatusDtoStatus.confirmed
                                        )
                                      }
                                    >
                                      Xác nhận đơn hàng
                                    </Button>
                                  )}
                                {/*{order.status ===*/}
                                {/*  EnumOrderEntityStatus.confirmed && (*/}
                                {/*  <Button*/}
                                {/*    type={'primary'}*/}
                                {/*    onClick={() =>*/}
                                {/*      changeStatus(*/}
                                {/*        EnumChangeStatusDtoStatus.processing*/}
                                {/*      )*/}
                                {/*    }*/}
                                {/*  >*/}
                                {/*    Đã chuẩn bị hàng*/}
                                {/*  </Button>*/}
                                {/*)}*/}
                                {order.status ===
                                  EnumOrderEntityStatus.confirmed && (
                                  <Button
                                    type={'primary'}
                                    onClick={() => setIsDeliveryVisible(true)}
                                  >
                                    Giao hàng
                                  </Button>
                                )}
                                {order.status ===
                                  EnumOrderEntityStatus.shipped && (
                                  <Button
                                    type={'primary'}
                                    onClick={() =>
                                      changeStatus(
                                        EnumChangeStatusDtoStatus.completed
                                      )
                                    }
                                  >
                                    Đã đối soát
                                  </Button>
                                )}
                                {order.status ===
                                  EnumOrderEntityStatus.shipping && (
                                  <>
                                    <Button
                                      onClick={printInvoice}
                                      icon={<PrinterOutlined />}
                                    >
                                      In phiếu
                                    </Button>
                                    <div style={{ display: 'none' }}>
                                      <OrdersPrint
                                        orders={[order]}
                                        ref={orderPrintRef}
                                        bankInfo={bankInfo}
                                      />
                                    </div>
                                  </>
                                )}
                                {order.status ===
                                  EnumOrderEntityStatus.shipping && (
                                  <Button
                                    type={'primary'}
                                    onClick={() =>
                                      changeStatus(
                                        EnumChangeStatusDtoStatus.shipped
                                      )
                                    }
                                  >
                                    Đã giao, chờ đối soát
                                  </Button>
                                )}
                                {EnumOrderEntityStatus.shipping ===
                                  order.status &&
                                  shippingStatus ===
                                    EnumShippingInfoStatus.fail && (
                                    <Button
                                      type={'primary'}
                                      onClick={() => {
                                        setIsShowModalReturn(true)
                                      }}
                                    >
                                      Đã hoàn hàng
                                    </Button>
                                  )}
                                {EnumOrderEntityStatus.shipping ===
                                  order.status &&
                                  isOtherPartnerShipping && (
                                    <Button
                                      type={'primary'}
                                      onClick={() => {
                                        updateShippingStatus(
                                          EnumShippingInfoStatus.fail
                                        )
                                      }}
                                    >
                                      Giao hàng thất bại
                                    </Button>
                                  )}
                              </>
                            ) : user?.type === EnumRoleEntityType.leader ? (
                              <>
                                {order.status ===
                                  EnumOrderEntityStatus.created &&
                                  addressInfo?.wardId && (
                                    <Button
                                      type={'primary'}
                                      onClick={() =>
                                        changeStatus(
                                          EnumChangeStatusDtoStatus.confirmed
                                        )
                                      }
                                    >
                                      Xác nhận đơn hàng
                                    </Button>
                                  )}
                              </>
                            ) : (
                              <>
                                {/*{user?.type === EnumRoleEntityType.accountant && (*/}
                                {/*  <Button*/}
                                {/*    onClick={() =>*/}
                                {/*      changeStatus(*/}
                                {/*        EnumChangeStatusDtoStatus.cancelled*/}
                                {/*      )*/}
                                {/*    }*/}
                                {/*  >*/}
                                {/*    Huỷ đơn hàng*/}
                                {/*  </Button>*/}
                                {/*)}*/}
                                {user?.type === EnumRoleEntityType.accountant &&
                                  [
                                    EnumOrderEntityStatus.created,
                                    EnumOrderEntityStatus.processing,
                                    EnumOrderEntityStatus.confirmed,
                                  ].includes(order.status) &&
                                  EnumOrderEntityPaymentStatus.confirm_deposit ===
                                    order.paymentStatus &&
                                  user.type === EnumRoleEntityType.accountant &&
                                  !user.merchantId && (
                                    <Button
                                      onClick={() =>
                                        setIsShowModalConfirmDeposit(true)
                                      }
                                      type={'primary'}
                                    >
                                      Xác nhận cọc
                                    </Button>
                                  )}
                              </>
                            )}
                          </Space>
                        </div>
                      </div>
                    </Card>
                  </Card>
                )}
              <Card>
                <Row>
                  <Col xs={24} sm={24} md={24} lg={24}>
                    <List itemLayout="horizontal">
                      {order?.id && (
                        <List.Item>
                          <List.Item.Meta
                            avatar={
                              <NumberOutlined
                                className={'pr-2'}
                                style={{ color: '#1890ff' }}
                              />
                            }
                            title={'Mã Đơn hàng'}
                            description={order?.code}
                          />
                        </List.Item>
                      )}
                      {order?.user?.roleId !== 1 && (
                        <List.Item
                          extra={
                            order?.user ? (
                              <a
                                href={`/user/${order?.user?.id}`}
                                target={'_blank'}
                                rel={'noreferrer'}
                                className={'px-4'}
                              >
                                Xem
                              </a>
                            ) : (
                              !order?.id && (
                                <Button
                                  type="link"
                                  onClick={() => setIsUserFormVisible(true)}
                                >
                                  Sửa
                                </Button>
                              )
                            )
                          }
                        >
                          <List.Item.Meta
                            avatar={
                              <UserOutlined
                                className={'pr-2'}
                                style={{ color: '#1890ff' }}
                              />
                            }
                            title={'Cộng tác viên'}
                            description={
                              order?.user ? (
                                <>
                                  {order?.user?.fullName}, {order?.user?.tel}
                                </>
                              ) : collaborator ? (
                                <>
                                  {collaborator?.fullName}, {collaborator?.tel}
                                </>
                              ) : (
                                'Chưa có thông tin'
                              )
                            }
                          />
                        </List.Item>
                      )}
                      <List.Item
                        extra={
                          (!order?.id ||
                            [
                              EnumOrderEntityStatus.created,
                              EnumOrderEntityStatus.confirmed,
                              EnumOrderEntityStatus.processing,
                            ].includes(order?.status)) && (
                            <Button
                              type="link"
                              onClick={() => setIsCustomerFormVisible(true)}
                            >
                              Sửa
                            </Button>
                          )
                        }
                      >
                        <List.Item.Meta
                          avatar={
                            <EnvironmentOutlined
                              className={'pr-2'}
                              style={{ color: '#1890ff' }}
                            />
                          }
                          title={'Địa chỉ nhận hàng'}
                          description={
                            addressInfo?.fullname ? (
                              <>
                                {addressInfo?.fullname}, {addressInfo?.tel}
                                <br />
                                {addressInfo?.shippingAddress},{' '}
                                {getFullAddress(order?.ward)}
                              </>
                            ) : addressInfo?.shippingAddress ? (
                              addressInfo?.shippingAddress
                            ) : (
                              'Chưa có thông tin'
                            )
                          }
                        />
                      </List.Item>
                      {(errors?.fullname ||
                        errors?.wardId ||
                        errors?.tel ||
                        errors?.shippingAddress) && (
                        <Alert
                          message={
                            errors?.fullname?.message ||
                            errors?.wardId?.message ||
                            errors?.tel?.message ||
                            errors?.shippingAddress?.message
                          }
                          type="error"
                          showIcon
                        />
                      )}
                      {!addressInfo?.wardId ? (
                        <Alert
                          message="Cảnh báo"
                          description="Địa chỉ giao hàng không hợp lệ! Vui lòng nhập đầy đủ thông tin!"
                          type="warning"
                          showIcon
                          closable
                        />
                      ) : null}
                      <List.Item
                        extra={
                          (([EnumOrderEntityStatus.created].includes(
                            order?.status
                          ) &&
                            merchantAddresses?.length > 0) ||
                            id === 'create') && (
                            <Button
                              type="link"
                              onClick={() =>
                                setIsChangeMerchantAddressFormVisible(true)
                              }
                            >
                              Sửa
                            </Button>
                          )
                        }
                      >
                        <List.Item.Meta
                          avatar={
                            <HomeOutlined
                              className={'pr-2'}
                              style={{ color: '#1890ff' }}
                            />
                          }
                          title={'Kho hàng'}
                          description={
                            order?.merchantAddress?.name ? (
                              <>
                                {order?.merchantAddress?.name},{' '}
                                {order?.merchantAddress?.tel}
                                <br />
                                {order?.merchantAddress?.address},{' '}
                                {getFullAddress(order?.merchantAddress?.ward)}
                              </>
                            ) : (
                              'Chưa có thông tin'
                            )
                          }
                        />
                      </List.Item>
                      {order?.id && (
                        <List.Item>
                          <List.Item.Meta
                            avatar={
                              <Icon
                                component={MdOutlineLocalShipping}
                                className={'pr-2'}
                                style={{ color: '#1890ff' }}
                              />
                            }
                            title={'Thông tin vận chuyển'}
                            description={
                              order?.shippingInfos ? (
                                <>
                                  {order?.shippingInfos?.map(
                                    (shippingInfo, shippingInfoIndex) => (
                                      <div
                                        key={`shippingInfo-${shippingInfoIndex}`}
                                        className="mb-2"
                                      >
                                        Đơn vị vận chuyển:{' '}
                                        <strong>
                                          {getShippingPartnerName(
                                            shippingInfo?.partner
                                          )}
                                        </strong>
                                        <br />
                                        {shippingInfo?.partner === 'other' ? (
                                          <>
                                            Người giao hàng:{' '}
                                            <strong>
                                              {
                                                shippingInfo?.shipperInfo
                                                  ?.fullName
                                              }
                                            </strong>
                                            <br />
                                            SĐT người giao hàng:{' '}
                                            <strong>
                                              {shippingInfo?.shipperInfo?.tel}
                                            </strong>
                                            <br />
                                          </>
                                        ) : (
                                          <>
                                            Mã vận đơn:{' '}
                                            <strong>
                                              {shippingInfo?.partner_id}
                                            </strong>
                                            <br />
                                            Trạng thái chi tiết:{' '}
                                            <strong>
                                              {shippingInfo?.statusName}
                                            </strong>
                                            <br />
                                          </>
                                        )}
                                      </div>
                                    )
                                  )}
                                </>
                              ) : (
                                'Chưa có thông tin'
                              )
                            }
                          />
                        </List.Item>
                      )}
                      <List.Item
                        extra={
                          (!order?.id ||
                            [
                              EnumOrderEntityStatus.created,
                              EnumOrderEntityStatus.confirmed,
                              EnumOrderEntityStatus.processing,
                            ].includes(order?.status)) &&
                          hasPermission(user, [
                            'orders_OrderController_updateNote',
                          ]) && (
                            <Button
                              type="link"
                              onClick={() => setIsShowModalNote(true)}
                            >
                              Sửa
                            </Button>
                          )
                        }
                      >
                        <List.Item.Meta
                          avatar={
                            <FormOutlined
                              className={'pr-2'}
                              style={{ color: '#1890ff' }}
                            />
                          }
                          title={'Ghi chú đơn hàng'}
                          description={order?.note}
                        />
                      </List.Item>
                      {order?.groupId && (
                        <>
                          <List.Item>
                            <List.Item.Meta
                              avatar={
                                <UsergroupAddOutlined
                                  className={'pr-2'}
                                  style={{ color: '#1890ff' }}
                                />
                              }
                              title={'Thông tin nhóm'}
                              description={
                                <>
                                  <div
                                    className={
                                      'flex items-center justify-start'
                                    }
                                  >
                                    <a className={'mr-3'}>
                                      {order?.group?.name}
                                    </a>
                                    <div className={'mr-3'}>
                                      Hoàn thành:{' '}
                                      <b className={'text-[#FF000F] pl-2'}>
                                        {order?.group?.orders?.reduce(
                                          (total, curr) => {
                                            if (
                                              curr.paymentStatus ===
                                              EnumOrderEntityPaymentStatus.deposited
                                            )
                                              return total + 1
                                            return total
                                          },
                                          0
                                        )}
                                      </b>
                                      /
                                      <b className={'text-[#4CAF50]'}>
                                        {order?.group?.orders?.length || 0}
                                      </b>
                                    </div>
                                    <div className={'ml-5'}>
                                      Tỷ lệ hủy:{' '}
                                      <b className={'text-[#FF000F] pl-2'}>
                                        {Math.round(
                                          (order?.group?.orders?.reduce(
                                            (total, curr) => {
                                              if (
                                                [
                                                  EnumOrderEntityStatus.cancelled,
                                                  EnumOrderEntityStatus.merchant_cancelled,
                                                ].includes(curr.status)
                                              )
                                                return total + 1
                                              return total
                                            },
                                            0
                                          ) ||
                                            0 /
                                              Number(
                                                order?.group?.orders?.length ||
                                                  1
                                              )) * 100
                                        )}
                                        %
                                      </b>
                                    </div>
                                  </div>
                                </>
                              }
                            />
                          </List.Item>
                          <List.Item>
                            <List.Item.Meta
                              avatar={
                                <HistoryOutlined
                                  className={'pr-2'}
                                  style={{ color: '#1890ff' }}
                                />
                              }
                              title={
                                <>
                                  <div className={'flex flex-row'}>
                                    {/*<div>{'Lịch sử đặt cọc'}</div>*/}
                                    <div>
                                      {order?.paymentHistories?.length > 0 && (
                                        <Collapse
                                          bordered={false}
                                          ghost={true}
                                          className={'custom-collapse'}
                                          expandIconPosition={'right'}
                                        >
                                          <Collapse.Panel
                                            key={1}
                                            header={'Lịch sử đặt cọc'}
                                            className={'p-0'}
                                          >
                                            {order?.paymentHistories?.map(
                                              (history) => {
                                                return (
                                                  <div
                                                    className={
                                                      'flex flex-row items-start justify-start'
                                                    }
                                                  >
                                                    <div
                                                      className={
                                                        'pr-2.5 flex items-center justify-center'
                                                      }
                                                    >
                                                      {formatDate(
                                                        history.createdAt,
                                                        'dd/MM/yyyy HH:mm:ss'
                                                      )}
                                                      <CheckCircleOutlined
                                                        style={{
                                                          color: '#1890FF',
                                                          marginLeft: 8,
                                                          fontSize: 11,
                                                        }}
                                                      />
                                                    </div>
                                                    <div
                                                      className={
                                                        'flex flex-col items-start justify-start'
                                                      }
                                                    >
                                                      <p className={'mb-0'}>
                                                        {getPaymentStatusDesc(
                                                          history.paymentStatus
                                                        )}
                                                      </p>
                                                      <p
                                                        className={
                                                          'mb-0 text-gray-300'
                                                        }
                                                      >
                                                        {history.user?.fullName}
                                                      </p>
                                                    </div>
                                                  </div>
                                                )
                                              }
                                            )}
                                          </Collapse.Panel>
                                        </Collapse>
                                      )}
                                    </div>
                                  </div>
                                </>
                              }
                            />
                          </List.Item>
                        </>
                      )}
                    </List>
                  </Col>
                </Row>
              </Card>
              <Card
                title={
                  <div className={'flex items-center'}>
                    <DollarCircleOutlined style={{ color: '#1890ff' }} />{' '}
                    <span className={'ml-2'}>Thông tin thanh toán</span>
                  </div>
                }
                extra={
                  (!order?.id ||
                    order?.status === EnumOrderEntityStatus.created) && (
                    <Row gutter={[16, 16]}>
                      {(!order?.id ||
                        order.status === EnumOrderEntityStatus.created) && (
                        <Col xs={8}>
                          <Button
                            onClick={() => {
                              if (products?.length == 0) {
                                alertError('Chưa chọn sản phẩm')
                              } else {
                                setIsPromotionFormVisible(true)
                              }
                            }}
                          >
                            <GiftFilled />
                          </Button>
                        </Col>
                      )}
                      <Col xs={16}>
                        <Button
                          type={'primary'}
                          onClick={() => {
                            if (!watchOrderMerchantAddressId) {
                              alertError(
                                'Chưa chọn Kho hàng nên không thể thêm sản phẩm'
                              )
                            } else {
                              setIsAddProductFormVisible(true)
                              setValue('productQuanity', 1)
                            }
                          }}
                        >
                          Thêm sản phẩm
                        </Button>
                      </Col>
                    </Row>
                  )
                }
              >
                <Table
                  dataSource={products}
                  pagination={false}
                  rowKey={'id'}
                  scroll={{ x: 900 }}
                  columns={[
                    {
                      key: 'number',
                      title: 'STT',
                      render: (_, record, index) => index + 1,
                    },
                    {
                      dataIndex: 'products',
                      title: 'Sản phẩm',
                      render: (_, record) => {
                        return (
                          <div className={'flex flex-row items-start'}>
                            <div className={'mr-2'}>
                              <Image
                                src={
                                  record?.product?.images?.[0] ||
                                  '/images/no-image.png'
                                }
                                width={50}
                                height={50}
                              />
                            </div>
                            <span>
                              {record.product.name}
                              {record.variant && (
                                <>
                                  <br />
                                  Phân loại: {record.variant.name}
                                </>
                              )}
                            </span>
                          </div>
                        )
                      },
                    },
                    {
                      dataIndex: 'price',
                      title: 'Đơn giá',
                      className: 'text-right',
                      render: (value) => formatNumber(value),
                    },
                    {
                      dataIndex: 'quantity',
                      title: 'Số lượng',
                      render: (value, record, index) => {
                        return (
                          <FormItem
                            className={'mb-0'}
                            validateStatus={
                              errors?.order?.products?.[index] && 'error'
                            }
                            help={errors?.order?.products?.[index]?.message}
                          >
                            {id === 'create' ? (
                              <InputNumber
                                value={value}
                                min={1}
                                onChange={(value) => {
                                  setProducts((prevState) => {
                                    const tmpProducts = [...prevState]
                                    tmpProducts[index].quantity = value
                                    setValue('productChange', 1)
                                    return tmpProducts
                                  })
                                }}
                              />
                            ) : (
                              formatNumber(value)
                            )}
                          </FormItem>
                        )
                      },
                    },
                    {
                      dataIndex: 'variant',
                      title: 'Tồn kho',
                      render: (value, record) => {
                        if (record?.variant) {
                          return (
                            record?.variant?.stock?.find(
                              (stock) =>
                                stock.merchantAddressId ===
                                watchOrderMerchantAddressId
                            )?.quantity || 0
                          )
                        } else {
                          return (
                            record?.product?.stock?.find(
                              (stock) =>
                                stock.merchantAddressId ===
                                watchOrderMerchantAddressId
                            )?.quantity || 0
                          )
                        }
                      },
                    },
                    {
                      dataIndex: 'total',
                      title: 'Thành tiền',
                      className: 'text-right',
                      render: (_, record) =>
                        formatNumber(record.price * record.quantity),
                    },
                    {
                      dataIndex: 'action',
                      title: 'Hành động',
                      render: (value, record, index) => {
                        return (
                          <Popconfirm
                            title="Bạn chắc chắn muốn xoá Sản phẩm này?"
                            onConfirm={() => {
                              setProducts((prevState) => {
                                const tmpProducts = [...prevState]
                                tmpProducts.splice(index, 1)
                                return tmpProducts
                              })
                            }}
                          >
                            {([
                              EnumOrderEntityStatus.cancelled,
                              EnumOrderEntityStatus.merchant_cancelled,
                              EnumOrderEntityStatus.created,
                            ].includes(order.status) ||
                              id === 'create') &&
                              products?.length > 1 && (
                                <Button
                                  shape={'circle'}
                                  key={index}
                                  icon={<DeleteOutlined />}
                                />
                              )}
                          </Popconfirm>
                        )
                      },
                    },
                  ]}
                  summary={(pageData) => {
                    let total = 0

                    pageData.forEach(
                      ({ price, quantity, commission, discount }) => {
                        total += price * quantity
                      }
                    )

                    return (
                      <>
                        <Table.Summary.Row>
                          <Table.Summary.Cell
                            index={0}
                            colSpan={3}
                            className={'text-right'}
                          >
                            Phương thức thanh toán
                          </Table.Summary.Cell>
                          <Table.Summary.Cell
                            index={1}
                            colSpan={2}
                            className={'text-right'}
                          >
                            <Controller
                              control={control}
                              render={({ field }) => (
                                <Radio.Group
                                  {...field}
                                  defaultValue={EnumOrderEntityPaymentType.cod}
                                  disabled={
                                    order?.id &&
                                    ![
                                      EnumOrderEntityStatus.created,
                                      EnumOrderEntityStatus.processing,
                                      EnumOrderEntityStatus.confirmed,
                                    ].includes(order?.status)
                                  }
                                >
                                  <Space direction="vertical">
                                    <Radio
                                      value={EnumOrderEntityPaymentType.cod}
                                    >
                                      Trả tiền mặt khi nhận hàng
                                    </Radio>
                                    <Radio
                                      value={EnumOrderEntityPaymentType.bacs}
                                    >
                                      Chuyển khoản ngân hàng
                                    </Radio>
                                  </Space>
                                </Radio.Group>
                              )}
                              name={'paymentType'}
                            />
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                        <Table.Summary.Row>
                          <Table.Summary.Cell
                            index={0}
                            colSpan={3}
                            className={'text-right'}
                          >
                            Tổng tiền sản phẩm
                          </Table.Summary.Cell>
                          <Table.Summary.Cell
                            index={1}
                            colSpan={2}
                            className={'text-right'}
                          >
                            {formatCurrency(total)}
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                        <Table.Summary.Row>
                          <Table.Summary.Cell
                            index={0}
                            colSpan={3}
                            className={'text-right'}
                          >
                            Phí vận chuyển
                          </Table.Summary.Cell>
                          <Table.Summary.Cell
                            index={1}
                            colSpan={2}
                            className={'text-right'}
                          >
                            {formatCurrency(
                              order?.shippingInfos?.reduce(
                                (total, shipInfo) => {
                                  return total + Number(shipInfo?.fee || 0)
                                },
                                0
                              )
                            )}
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                        <Table.Summary.Row>
                          <Table.Summary.Cell
                            index={0}
                            colSpan={3}
                            className={'text-right'}
                          >
                            Giảm giá của shop
                          </Table.Summary.Cell>
                          <Table.Summary.Cell
                            index={1}
                            colSpan={2}
                            className={'text-right'}
                          >
                            {formatCurrency(order?.voucherMerchantDiscount)}
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                        <Table.Summary.Row>
                          <Table.Summary.Cell
                            index={0}
                            colSpan={3}
                            className={'text-right'}
                          >
                            Giảm giá của Ushare
                          </Table.Summary.Cell>
                          <Table.Summary.Cell
                            index={1}
                            colSpan={2}
                            className={'text-right'}
                          >
                            {formatCurrency(order?.voucherUshareDiscount)}
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                        <Table.Summary.Row>
                          <Table.Summary.Cell
                            index={0}
                            colSpan={3}
                            className={'text-right'}
                          >
                            Hoa hồng
                          </Table.Summary.Cell>
                          <Table.Summary.Cell
                            index={1}
                            colSpan={2}
                            className={'text-right'}
                          >
                            {formatCurrency(order?.commission)}
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                        {/*<Table.Summary.Row>*/}
                        {/*  <Table.Summary.Cell*/}
                        {/*    index={0}*/}
                        {/*    colSpan={3}*/}
                        {/*    className={'text-right'}*/}
                        {/*  >*/}
                        {/*    Giảm giá*/}
                        {/*  </Table.Summary.Cell>*/}
                        {/*  <Table.Summary.Cell*/}
                        {/*    index={1}*/}
                        {/*    colSpan={2}*/}
                        {/*    className={'text-right'}*/}
                        {/*  >*/}
                        {/*    {formatCurrency(totalDiscount)}*/}
                        {/*  </Table.Summary.Cell>*/}
                        {/*</Table.Summary.Row>*/}
                        {order?.id && (
                          <>
                            {order?.id && (
                              <Table.Summary.Row>
                                <Table.Summary.Cell
                                  index={0}
                                  colSpan={3}
                                  className={'text-right'}
                                >
                                  Đã cọc
                                </Table.Summary.Cell>
                                <Table.Summary.Cell
                                  index={1}
                                  colSpan={2}
                                  className={'text-right'}
                                >
                                  {formatCurrency(order?.deposit)}
                                </Table.Summary.Cell>
                              </Table.Summary.Row>
                            )}
                          </>
                        )}
                        <Table.Summary.Row>
                          <Table.Summary.Cell
                            index={0}
                            colSpan={3}
                            className={'text-right font-semibold'}
                          >
                            Còn phải thu
                          </Table.Summary.Cell>
                          <Table.Summary.Cell
                            index={1}
                            colSpan={2}
                            className={'text-right font-semibold'}
                          >
                            {formatCurrency(
                              Number(order?.total || 0) -
                                Number(order?.deposit || 0)
                            )}
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                      </>
                    )
                  }}
                />
              </Card>
            </Col>
            <Col xs={24} sm={24} md={4} lg={4}>
              <div className={'uppercase text-center mb-4 text-gray-400'}>
                Lịch sử đơn hàng
              </div>
              {order?.id && <OrderStepHistory order={order} />}
            </Col>
          </Row>
          {(!order?.id ||
            [
              EnumOrderEntityStatus.created,
              EnumOrderEntityStatus.confirmed,
              EnumOrderEntityStatus.processing,
            ].includes(order?.status)) && (
            <FooterBar
              right={
                <Button htmlType={'submit'} type={'primary'}>
                  Lưu
                </Button>
              }
            />
          )}
        </Form>
        <Modal
          title="Thông tin Cộng tác viên"
          visible={isUserFormVisible}
          onOk={() => {
            const pubId = getValues('order.pubId')
            const collaborator = collaborators
              .filter((c) => c.id === pubId)
              .shift()
            setCollaborator(collaborator)
            setIsUserFormVisible(false)
          }}
          onCancel={() => setIsUserFormVisible(false)}
        >
          <Row gutter={[8, 0]}>
            <Col xs={24} sm={24} md={24} lg={24}>
              <Form.Item
                label={'Cộng tác viên'}
                required
                labelCol={{ md: 24 }}
                labelAlign={'left'}
              >
                <Controller
                  control={control}
                  name={'order.pubId'}
                  render={({ field }) => (
                    <Select filterOption={filterOption} showSearch {...field}>
                      {collaborators.map((user) => (
                        <Select.Option
                          value={user.id}
                          key={`collaborator-${user.id}`}
                        >
                          {user.tel + ' - ' + user.fullName}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                />
              </Form.Item>
            </Col>
          </Row>
        </Modal>
        <Modal
          title="Chương trình khuyến mãi"
          visible={isPromotionFormVisible}
          onOk={() => {
            const voucherMerchantIdValue = getValues('voucherMerchantId')
            setVoucherMerchantId(voucherMerchantIdValue)
            const voucherUshareIdValue = getValues('voucherUshareId')
            setVoucherUshareId(voucherUshareIdValue)
            setValue('productChange', 1)
            setIsPromotionFormVisible(false)
          }}
          onCancel={() => setIsPromotionFormVisible(false)}
        >
          <Row gutter={[8, 0]}>
            <Col xs={24} sm={24} md={12} lg={12}>
              <Form.Item
                label={'Khuyến mãi của shop'}
                labelCol={{ md: 24 }}
                labelAlign={'left'}
              >
                <Controller
                  control={control}
                  name={'voucherMerchantId'}
                  render={({ field }) => (
                    <Select filterOption={filterOption} showSearch {...field}>
                      {voucherMerchants.map((voucher) => (
                        <Select.Option
                          value={voucher.id}
                          key={`voucherMerchant-${voucher.id}`}
                        >
                          {voucher.code + ' - ' + voucher.name}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={12} lg={12}>
              <Form.Item
                label={'Khuyến mãi của sàn'}
                labelCol={{ md: 24 }}
                labelAlign={'left'}
              >
                <Controller
                  control={control}
                  name={'voucherUshareId'}
                  render={({ field }) => (
                    <Select filterOption={filterOption} showSearch {...field}>
                      {voucherUshares.map((voucher) => (
                        <Select.Option
                          value={voucher.id}
                          key={`voucherUshare-${voucher.id}`}
                        >
                          {voucher.code + ' - ' + voucher.name}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={24}>
              <Form.Item
                label={'Deal sốc'}
                labelCol={{ md: 24 }}
                labelAlign={'left'}
              >
                <Controller
                  control={control}
                  name={''}
                  render={({ field }) => (
                    <Select filterOption={filterOption} showSearch {...field}>
                      {/* {collaborators.map((user) => (
                        <Select.Option
                          value={user.id}
                          key={`collaborator-${user.id}`}
                        >
                          {user.tel + ' - ' + user.fullName}
                        </Select.Option>
                      ))} */}
                    </Select>
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={24}>
              <Form.Item
                label={'Combo quà tặng'}
                labelCol={{ md: 24 }}
                labelAlign={'left'}
              >
                <Controller
                  control={control}
                  name={''}
                  render={({ field }) => (
                    <Select filterOption={filterOption} showSearch {...field}>
                      {/* {collaborators.map((user) => (
                        <Select.Option
                          value={user.id}
                          key={`collaborator-${user.id}`}
                        >
                          {user.tel + ' - ' + user.fullName}
                        </Select.Option>
                      ))} */}
                    </Select>
                  )}
                />
              </Form.Item>
            </Col>
          </Row>
        </Modal>
        <Modal
          title="Thông tin Khách hàng"
          visible={isCustomerFormVisible}
          okButtonProps={{
            form: 'customer-address-form',
            htmlType: 'submit',
          }}
          onCancel={() => setIsCustomerFormVisible(false)}
        >
          <CustomerAddress
            value={addressInfo as any}
            onChange={(value) => {
              setAddressInfo(value)
              setOrder((prevState) => {
                return {
                  ...prevState,
                  ward: value.ward,
                }
              })
              setValue('fullname', value.fullname)
              setValue('wardId', value.wardId)
              setValue('tel', value.tel)
              setValue('shippingAddress', value.shippingAddress)
              setIsCustomerFormVisible(false)
            }}
          ></CustomerAddress>
        </Modal>
        <Modal
          title="Thêm Sản phẩm"
          visible={isAddProductFormVisible}
          onOk={() => {
            if (Number(getValues('productQuanity'))) {
              let stockQuantity = 0
              if (product?.variants?.length > 0) {
                stockQuantity =
                  variant?.stock.find(
                    (stock) =>
                      stock.merchantAddressId === watchOrderMerchantAddressId
                  )?.quantity || 0
              } else {
                stockQuantity =
                  product?.stocks?.find(
                    (stock) =>
                      stock.merchantAddressId === watchOrderMerchantAddressId
                  )?.quantity || 0
              }
              if (getValues('productQuanity') > stockQuantity) {
                setError('productQuanity', {
                  message: 'Số lượng không được phép lớn hơn tồn kho',
                })
              } else {
                clearErrors('productQuanity')
                setProducts((prveState) => {
                  const tmpProducts = [...prveState]
                  const existProductIndex = tmpProducts.findIndex(
                    (tmpProduct) => tmpProduct.productId === product.id
                  )
                  if (existProductIndex > -1) {
                    tmpProducts[existProductIndex].quantity =
                      Number(tmpProducts[existProductIndex].quantity) +
                      Number(getValues('productQuanity'))
                    return tmpProducts
                  }
                  return [
                    ...tmpProducts,
                    {
                      productId: product.id,
                      quantity: getValues('productQuanity') || 1,
                      product: product,
                      price: product.price,
                      variantId: variant?.id || null,
                      variant: variant,
                      orderId: Number(id),
                    },
                  ]
                })
                setValue('productChange', 1)
                setValue('voucherMerchantChange', 1)
                setValue('voucherUshareChange', 1)

                setValue('productSelected', null)
                // setValue('productQuanity', 1)
                setProduct(null)
                setVariant(null)
                setIsAddProductFormVisible(false)
              }
            } else alertError('Số lượng phải lớn hơn 0')
          }}
          onCancel={() => {
            setVariant(null)
            setProduct(null)
            setValue('productSelected', null)
            setValue('productQuanity', 1)
            setIsAddProductFormVisible(false)
            clearErrors('productQuanity')
          }}
        >
          <Row gutter={[8, 0]}>
            <Col xs={24} sm={24} md={24} lg={24}>
              <Form.Item
                label={'Tên sản phẩm'}
                required
                labelCol={{ md: 24 }}
                labelAlign={'left'}
              >
                <Controller
                  control={control}
                  name={'productSelected'}
                  render={({ field }) => (
                    <DebounceSelect
                      showSearch
                      placeholder={'Nhập tên sản phẩm để tìm kiếm'}
                      fetchOptions={getProducts}
                      {...field}
                      style={{
                        width: '100%',
                      }}
                    />
                  )}
                />
              </Form.Item>
            </Col>
            {product && (
              <>
                {product?.variants?.length > 0 ? (
                  <Col xs={24} sm={24} md={24} lg={24} className={'mb-6'}>
                    <Card title={'Danh sách phân loại'}>
                      <Table
                        dataSource={product?.variants}
                        columns={[
                          {
                            dataIndex: 'name',
                            title: 'Phân loại',
                          },
                          {
                            dataIndex: 'price',
                            title: 'Giá bán',
                            render: (value) => formatCurrency(value),
                          },
                          {
                            dataIndex: 'stock',
                            title: 'Tồn kho',
                            render: (value, record) => {
                              return (
                                record.stock.find(
                                  (stock) =>
                                    stock.merchantAddressId ===
                                    watchOrder?.merchantAddressId
                                )?.quantity || 0
                              )
                            },
                          },
                        ]}
                        rowKey={'id'}
                        rowSelection={{
                          type: 'radio',
                          onChange: (_, selectedRows) => {
                            setVariant(selectedRows[0])
                            const stockQuantity =
                              selectedRows[0]?.stock.find(
                                (stock) =>
                                  stock.merchantAddressId ===
                                  watchOrderMerchantAddressId
                              )?.quantity || 0
                            if (stockQuantity >= getValues('productQuanity'))
                              clearErrors('productQuanity')
                          },
                        }}
                        pagination={false}
                      />
                    </Card>
                  </Col>
                ) : (
                  <>
                    <Col xs={24} sm={24} md={24} lg={24} className={'mb-6'}>
                      <Card title={'Danh sách phân loại'}>
                        <Descriptions layout={'vertical'}>
                          <Descriptions.Item label="Giá niêm yết">
                            {formatCurrency(product?.priceBeforeDiscount)}
                          </Descriptions.Item>
                          <Descriptions.Item label="Giá bán">
                            {formatCurrency(product?.price)}
                          </Descriptions.Item>
                          <Descriptions.Item label="Tồn kho">
                            {product?.stocks?.find(
                              (stock) =>
                                stock.merchantAddressId ===
                                watchOrder?.merchantAddressId
                            )?.quantity || 0}
                          </Descriptions.Item>
                        </Descriptions>
                      </Card>
                    </Col>
                  </>
                )}
              </>
            )}
            <Col xs={24} sm={24} md={24} lg={24}>
              <Form.Item
                label={'Số lượng'}
                required
                labelCol={{ md: 24 }}
                labelAlign={'left'}
                validateStatus={errors?.productQuanity && 'error'}
                help={errors?.productQuanity?.message}
              >
                <Controller
                  control={control}
                  name={'productQuanity'}
                  render={({ field }) => (
                    <InputNumber {...field} style={{ width: '100%' }} min={1} />
                  )}
                />
              </Form.Item>
            </Col>
          </Row>
        </Modal>
        <Modal
          title="Thông tin Vận chuyển"
          visible={isDeliveryVisible}
          onCancel={() => setIsDeliveryVisible(false)}
          width={1000}
          closable={false}
          maskClosable={false}
          destroyOnClose={true}
          footer={false}
        >
          <Spin spinning={isLoading}>
            <BatchDelivery
              orderIds={[order?.id]}
              onSuccess={() => {
                router.reload()
              }}
              onCancel={() => {
                setIsDeliveryVisible(false)
              }}
            />
          </Spin>
        </Modal>
        <Modal
          title={`Nhập lý do ${
            isShowModalCancel
              ? 'hủy hàng'
              : isShowModalReturn
              ? 'hoàn hàng'
              : isShowModalMerchantCancel
              ? 'hủy hàng'
              : ''
          }`}
          visible={
            isShowModalCancel || isShowModalReturn || isShowModalMerchantCancel
          }
          onCancel={() => {
            setIsShowModalCancel(false)
            setIsShowModalReturn(false)
            setIsShowModalMerchantCancel(false)
          }}
          width={600}
          closable={false}
          maskClosable={false}
          destroyOnClose={true}
          footer={[
            <Button
              key="back"
              onClick={() => {
                setIsShowModalCancel(false)
                setIsShowModalReturn(false)
                setIsShowModalMerchantCancel(false)
              }}
            >
              Đóng
            </Button>,
            <>
              {watchCancelReason?.length > 0 && (
                <Button
                  key="submit"
                  type="primary"
                  onClick={() => {
                    cancelOrder(
                      isShowModalCancel
                        ? EnumChangeStatusDtoStatus.cancelled
                        : isShowModalReturn
                        ? EnumChangeStatusDtoStatus.return
                        : isShowModalMerchantCancel
                        ? EnumChangeStatusDtoStatus.merchant_cancelled
                        : null
                    )
                  }}
                >
                  Lưu
                </Button>
              )}
            </>,
          ]}
        >
          <Row>
            <Col xs={24}>
              <Controller
                control={control}
                name={'cancel_reason'}
                render={({ field }) => (
                  <TextArea
                    {...field}
                    rows={4}
                    placeholder={`Nhập lý do ${
                      isShowModalCancel
                        ? 'hủy hàng'
                        : isShowModalReturn
                        ? 'hoàn hàng'
                        : isShowModalMerchantCancel
                        ? 'hủy hàng'
                        : ''
                    }`}
                  />
                )}
              />
            </Col>
          </Row>
        </Modal>
        <Modal
          title="Thay đổi kho lấy hàng"
          visible={isChangeMerchantAddressFormVisible}
          onCancel={() => setIsChangeMerchantAddressFormVisible(false)}
          width={600}
          onOk={changeMerchantAddress}
          okText={'Lưu'}
        >
          <Row>
            <Col xs={24}>
              <Form.Item
                label={'Chọn kho hàng'}
                labelCol={{ md: 24 }}
                labelAlign={'left'}
              >
                <Controller
                  control={control}
                  name={'order.merchantAddressId'}
                  render={({ field }) => (
                    <Select
                      {...field}
                      showSearch
                      filterOption={filterOption}
                      style={{ width: '100%' }}
                    >
                      {merchantAddresses.map((merchantAddress) => (
                        <Select.Option
                          value={merchantAddress.id}
                          key={`merchantAddress-${merchantAddress.id}`}
                        >
                          {merchantAddress.name}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                />
              </Form.Item>
            </Col>
          </Row>
        </Modal>
        <Modal
          title={`Xác nhận tiền đặt cọc đơn hàng ${order?.code}`}
          visible={isShowModalConfirmDeposit}
          onCancel={() => setIsShowModalConfirmDeposit(false)}
          cancelText={'Đóng'}
          width={600}
          onOk={confirmDeposit}
          okText={'Xác nhận'}
        >
          <Row>
            <Col xs={24}>
              <Form.Item
                label={'Số tiền cọc:'}
                labelCol={{ md: 24 }}
                labelAlign={'left'}
              >
                <Controller
                  control={control}
                  name={'deposit'}
                  render={({ field }) => (
                    <InputNumber
                      {...field}
                      className={'w-full'}
                      addonAfter={'đ'}
                      formatter={(value) =>
                        `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                      }
                    />
                  )}
                />
              </Form.Item>
            </Col>
          </Row>
        </Modal>
        <Modal
          title={`Ghi chú đơn hàng ${order?.code}`}
          visible={isShowModalNote}
          onCancel={() => setIsShowModalNote(false)}
          cancelText={'Đóng'}
          width={600}
          onOk={updateNote}
          okText={'Lưu'}
        >
          <Row>
            <Col xs={24}>
              <Form.Item
                label={'Ghi chú:'}
                labelCol={{ md: 24 }}
                labelAlign={'left'}
              >
                <Controller
                  control={control}
                  name={'order.note'}
                  render={({ field }) => (
                    // <Input {...field} className={'w-full'} />
                    <TextArea
                      {...field}
                      rows={4}
                      placeholder="Nhập ghi chú đơn hàng"
                      className={'w-full'}
                    />
                  )}
                />
              </Form.Item>
            </Col>
          </Row>
        </Modal>
      </Content>
    </>
  )
}
export default Detail

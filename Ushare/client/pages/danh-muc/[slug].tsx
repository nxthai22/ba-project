import React, { FC, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import {
  GetManyProductEntityResponseDto,
  ProductCategoriesService,
  ProductCategoryEntity,
  ProductsService,
  ProvinceEntity,
  ProvincesService,
} from 'services'
import { alertError, filterOption, flattenCategoryTree } from 'utils'
import { useSetRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'
import Head from 'next/head'
import {
  Breadcrumb,
  Button,
  Checkbox,
  Form,
  Input,
  Menu,
  Pagination,
  Select,
} from 'antd'
import Link from 'components/common/Link'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import HomeProductBlock from 'components/common/HomeProductBlock'
import InputNumber from 'components/common/InputNumber'
import NoProduct from 'components/guest/common/NoProduct'

interface Inputs {
  attributeFilters?: AttributeFilter[]
  minPrice?: number
  maxPrice: number
}

interface AttributeFilter {
  attributeId?: number
  attributeValueIds?: number[]
  attributeUnitIndexs?: number[]
}

const Category: FC = () => {
  const router = useRouter()

  const setIsLoading = useSetRecoilState(loadingState)
  const { slug, pub_id, page } = router.query
  const [productCategory, setProductCategory] =
    useState<ProductCategoryEntity>()
  const [breadcrumCategories, setBreadcrumCategories] = useState<
    ProductCategoryEntity[]
  >([])
  const [categories, setCategories] = useState<ProductCategoryEntity[]>([])
  const [provinces, setProvinces] = useState<ProvinceEntity[]>([])
  const [filters, setFilters] = useState([])
  const [selectedMenus, setSelectedMenus] = useState([])
  const [provinceId, setProvinceId] = useState(0)
  const [products, setProducts] = useState<GetManyProductEntityResponseDto>()
  const [attributeFilters, setAttributeFilters] = useState([])
  const [paramFilterByProductAttributes, setParamFilterByProductAttributes] =
    useState({})
  const { control, handleSubmit, reset } = useForm<Inputs>({
    defaultValues: {},
  })
  const [sort, setSort] = useState('saleAmount')

  useEffect(() => {
    if (slug) {
      const slugs = String(slug).split('_')
      if (Number(slugs[1])) {
        setIsLoading(true)
        Promise.all([
          ProductCategoriesService.getOneBase({
            id: Number(slugs[1]),
            join: ['parent', 'childrens'],
          }),
          ProductCategoriesService.getManyBase({
            filter: ['parentId||isnull'],
            join: ['childrens'],
            sort: ['createdAt,ASC'],
          }),
          ProvincesService.getManyBase({
            limit: 100,
            sort: ['position,ASC'],
          }),
        ])
          .then(([categoryResponse, categoriesResponse, provincesResponse]) => {
            const tmpSelectedMenus = [`category-${Number(slugs[1])}`]
            const categoryParents = flattenCategoryTree(
              categoryResponse.parent,
              []
            )
            categoryParents.map((categoryParent) =>
              tmpSelectedMenus.push(`category-${categoryParent.id}`)
            )
            setSelectedMenus(tmpSelectedMenus)
            setIsLoading(false)
            setProductCategory(categoryResponse)
            setCategories(categoriesResponse.data)
            setProvinces(provincesResponse.data)
            setBreadcrumCategories(categoryParents)
            setFilters([`productCategoryId||eq||${Number(slugs[1])}`])
            setSort('saleAmount')
          })
          .catch((e) => {
            setIsLoading(false)
            alertError(e)
          })
      }
    }
  }, [router])

  useEffect(() => {
    if (paramFilterByProductAttributes && slug && filters.length > 0) {
      const slugs = String(slug).split('_')
      if (Number(slugs[1])) {
        let sort = []
        if (paramFilterByProductAttributes?.sortValue) {
          switch (paramFilterByProductAttributes?.sortValue) {
            case 'commission':
              sort = ['commission,DESC']
              break
            case 'createdAt':
              sort = ['createdAt,DESC']
              break
            case 'saleAmount':
              sort = ['saleAmount,DESC']
              break
            case 'price':
              sort = ['price,DESC']
              break
            default:
              sort = ['createdAt,DESC']
          }
        } else {
          sort = ['saleAmount,DESC']
        }
        setIsLoading(true)
        const categoryIds = []
        if (productCategory) {
          filters.splice(
            filters.findIndex((filter) => filter.includes('productCategoryId')),
            1
          )
          categoryIds.push(productCategory.id)
          if (productCategory?.childrens?.length > 0) {
            productCategory?.childrens?.map((children) => {
              categoryIds.push(children.id)
            })
          }
          filters.push(`productCategoryId||in||${categoryIds.join(',')}`)
        }
        ProductsService.getManyBase({
          join: [`productAttributes`],
          filter: filters,
          ...(provinceId && { provinceId: provinceId }),
          ...(attributeFilters.length > 0 && {
            attributeFilters: JSON.stringify(attributeFilters),
          }),
          limit: 12,
          page: Number(page) || 0,
          sort: sort,
        })
          .then((productsResponse) => {
            setIsLoading(false)
            setProducts(productsResponse)
          })
          .catch((e) => {
            setIsLoading(false)
            alertError(e)
          })
      }
    }
  }, [
    paramFilterByProductAttributes,
    provinceId,
    attributeFilters,
    filters,
    productCategory,
  ])

  const onchangeFilterSort = (value) => {
    const tmpParam = { ...paramFilterByProductAttributes }
    setParamFilterByProductAttributes({
      ...tmpParam,
      sortValue: value,
    })
  }

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    setIsLoading(true)
    const attributeFilter = []
    data?.attributeFilters?.map((attributeValueFilterItem) => {
      if (
        attributeValueFilterItem?.attributeValueIds?.length > 0 ||
        attributeValueFilterItem?.attributeUnitIndexs?.length > 0
      ) {
        attributeFilter.push(attributeValueFilterItem)
      }
    })
    const filter = [...filters]
    data.minPrice = data.minPrice ?? 0
    data.maxPrice = data.maxPrice ?? 0
    if (!isNaN(data.minPrice) && !isNaN(data.maxPrice)) {
      data.minPrice = Number(data.minPrice)
      data.maxPrice = Number(data.maxPrice)
      const priceIndex = filters.findIndex((tmpFilter) => {
        return tmpFilter.includes('price')
      })
      if (data.minPrice + data.maxPrice > 0) {
        if (priceIndex > -1) {
          filter[
            priceIndex
          ] = `price||between||${data.minPrice},${data.maxPrice}`
        } else {
          filter.push(`price||between||${data.minPrice},${data.maxPrice}`)
        }
      } else {
        priceIndex > -1 ? filter.splice(priceIndex, 1) : filter
      }
    }
    if (filter.length > 0) setFilters(filter)
    if (attributeFilter.length > 0) setAttributeFilters(attributeFilter)
    else setAttributeFilters([])
    setIsLoading(false)
  }

  return (
    <>
      <Head>
        <title>{productCategory?.name}</title>
      </Head>
      <div className={'product-category flex max-w-6xl m-auto flex-col pb-10'}>
        <div
          className={
            'flex justify-between items-center py-4 md:px-0 px-4 flex-col md:flex-row'
          }
        >
          <div className={'md:w-9/12 w-full'}>
            <Breadcrumb
              separator=">"
              className={'md:text-base text-xs'}
              style={{
                color: '#80B4FB',
              }}
            >
              <Breadcrumb.Item>DANH MỤC</Breadcrumb.Item>
              {breadcrumCategories &&
                breadcrumCategories.map((category) => (
                  <Breadcrumb.Item key={`menu-category-${category.id}`}>
                    <Link
                      href={{
                        pathname: `/danh-muc/${category.slug}_${category.id}`,
                        query: {
                          ...(pub_id && { pub_id: Number(pub_id) }),
                        },
                      }}
                    >
                      <span
                        style={{
                          color: '#80B4FB',
                        }}
                      >
                        {category.name}
                      </span>
                    </Link>
                  </Breadcrumb.Item>
                ))}
              <Breadcrumb.Item>
                <span
                  style={{
                    color: '#223263',
                  }}
                >
                  {productCategory?.name}
                </span>
              </Breadcrumb.Item>
            </Breadcrumb>
          </div>
          <div className={'md:w-3/12 w-full my-2 md:my-0'}>
            <div className={'flex flex-row'}>
              <Select
                className={'sort w-full'}
                placeholder={'Sắp xếp theo'}
                onChange={(value) => {
                  onchangeFilterSort(value)
                  setSort(value)
                }}
                value={sort}
              >
                <Select.Option value={'saleAmount'}>Bán chạy</Select.Option>
                <Select.Option value={'createdAt'}>Mới nhất</Select.Option>
                <Select.Option value={'price'}>Giá cao nhất</Select.Option>
              </Select>
            </div>
          </div>
        </div>
        <div className={'flex gap-8 px-2 md:px-0'}>
          <div className={'md:w-3/12 md:block hidden'}>
            <div className={'bg-price py-5 rounded-xl'}>
              <div
                className={
                  'text-white text-2xl font-bold uppercase border-b border-white pb-3 px-4 mb-2'
                }
              >
                DANH MỤC
              </div>
              <div>
                <Menu
                  mode="vertical"
                  className={'bg-transparent border-0 left-menu'}
                  selectedKeys={selectedMenus}
                >
                  {categories &&
                    categories?.map((category) => (
                      <>
                        {category?.childrens?.length > 0 ? (
                          <Menu.SubMenu
                            key={`category-${category.id}`}
                            title={category.name}
                            className={'uppercase text-white'}
                          >
                            {category?.childrens?.map((children) => (
                              <Menu.Item
                                className={'uppercase text-secondary'}
                                key={`category-${children.id}`}
                                onClick={() => {
                                  router
                                    .push({
                                      pathname: `/danh-muc/${children.slug}_${children.id}`,
                                      query: {
                                        ...(pub_id && {
                                          pub_id: Number(pub_id),
                                        }),
                                      },
                                    })
                                    .then()
                                }}
                              >
                                {children.name}
                              </Menu.Item>
                            ))}
                          </Menu.SubMenu>
                        ) : (
                          <Menu.Item
                            className={'uppercase text-white'}
                            key={`category-${category.id}`}
                            onClick={() => {
                              router
                                .push({
                                  pathname: `/danh-muc/${category.slug}_${category.id}`,
                                  query: {
                                    ...(pub_id && { pub_id: Number(pub_id) }),
                                  },
                                })
                                .then()
                            }}
                          >
                            {category.name}
                          </Menu.Item>
                        )}
                      </>
                    ))}
                </Menu>
              </div>
            </div>
            <div className={'mt-7'}>
              <Select
                placeholder={'Kho hàng tại'}
                filterOption={filterOption}
                allowClear
                showSearch
                className={'province'}
                onChange={(value) => setProvinceId(value)}
              >
                <Select.Option
                  className={'text-primary text-sm border-t-1 py-3'}
                >
                  Tất cả
                </Select.Option>
                {provinces?.map((province) => (
                  <Select.Option
                    key={`province-${province.id}`}
                    value={province.id}
                    className={'text-primary text-sm border-t-1 py-3'}
                  >
                    {province.name}
                  </Select.Option>
                ))}
              </Select>
            </div>
            <Form autoComplete={'off'} onFinish={handleSubmit(onSubmit)}>
              <div className={'mt-5 bg-white py-4 px-3 rounded-md'}>
                {productCategory?.productCategoryAttributes?.length > 0 && (
                  <>
                    <div className={'text-price font-medium text-base'}>
                      Thông số kỹ thuật
                    </div>
                    {productCategory?.productCategoryAttributes?.map(
                      (attribute, attributeIndex) => {
                        return (
                          <div
                            className={'mt-3'}
                            key={`attribute-${attribute.id}`}
                          >
                            <Controller
                              control={control}
                              name={`attributeFilters.${attributeIndex}.attributeId`}
                              defaultValue={attribute.id}
                              render={({ field }) => (
                                <Input {...field} type={'hidden'} />
                              )}
                            />
                            <div
                              className={'text-secondary text-sm font-medium'}
                            >
                              {attribute?.name}
                            </div>
                            <div className={'mt-3 flex gap-3 flex-wrap'}>
                              <Controller
                                control={control}
                                name={`attributeFilters.${attributeIndex}.attributeValueIds`}
                                render={({ field }) => (
                                  <Checkbox.Group {...field}>
                                    <div className={'flex flex-wrap'}>
                                      {attribute.productCategoryAttributeValues.map(
                                        (value) => {
                                          if (value?.name)
                                            return (
                                              <div>
                                                <Checkbox
                                                  value={value.id}
                                                  // checked={false}
                                                >
                                                  {value?.name}
                                                </Checkbox>
                                              </div>
                                            )
                                        }
                                      )}
                                    </div>
                                  </Checkbox.Group>
                                )}
                              />
                            </div>
                            <hr className={'border-dotted mt-3'} />
                            <div className={'mt-3 flex gap-3 flex-wrap'}>
                              <Controller
                                control={control}
                                name={`attributeFilters.${attributeIndex}.attributeUnitIndexs`}
                                render={({ field }) => (
                                  <Checkbox.Group {...field}>
                                    <div className={'flex flex-wrap'}>
                                      {attribute?.units?.length > 0 &&
                                        attribute?.units?.map(
                                          (unit, unitIndex) => {
                                            return (
                                              <div
                                                key={`attributeUnitIndexs-${unitIndex}`}
                                              >
                                                <Checkbox value={unitIndex}>
                                                  {unit}
                                                </Checkbox>
                                              </div>
                                            )
                                          }
                                        )}
                                    </div>
                                  </Checkbox.Group>
                                )}
                              />
                            </div>
                            {attributeIndex <
                              attribute.productCategoryAttributeValues.length -
                                1 && (
                              <div
                                className={'mt-3'}
                                style={{
                                  borderBottom: '0.5px solid #B1BFD3',
                                }}
                              />
                            )}
                          </div>
                        )
                      }
                    )}
                  </>
                )}
                <div className={'text-price font-medium text-base'}>
                  Khoảng giá (VNĐ)
                </div>
                <div className={'flex gap-4 items-center mt-4 price-range'}>
                  <Controller
                    control={control}
                    name={'minPrice'}
                    render={({ field }) => (
                      <InputNumber
                        {...field}
                        placeholder={'Từ'}
                        className={'rounded-md h-10'}
                        style={{
                          height: 40,
                        }}
                      />
                    )}
                  />
                  <div className={'text-price w-3 h-px bg-price'} />
                  <Controller
                    control={control}
                    name={'maxPrice'}
                    render={({ field }) => (
                      <InputNumber
                        {...field}
                        placeholder={'Đến'}
                        className={'rounded-md h-10'}
                      />
                    )}
                  />
                </div>
                <div className={'flex gap-2 mt-9'}>
                  <Button
                    className={
                      'w-6/12 rounded-md h-11 text-secondary text-base border-secondary text-medium'
                    }
                    onClick={() => {
                      reset({
                        attributeFilters: [],
                        minPrice: 0,
                        maxPrice: 0,
                      })
                    }}
                    htmlType={'submit'}
                  >
                    Thiết lập lại
                  </Button>
                  <Button
                    className={
                      'w-6/12 rounded-md border-0 bg-price text-white h-11 text-base text-medium'
                    }
                    type={'primary'}
                    htmlType={'submit'}
                  >
                    Áp dụng
                  </Button>
                </div>
              </div>
              {/*<div className={'mt-5  bg-white py-4 px-3 rounded-md'}>*/}
              {/*  <div className={'text-secondary font-medium text-base'}>*/}
              {/*    Khoảng giá (VNĐ)*/}
              {/*  </div>*/}
              {/*  <div className={'flex gap-4 items-center mt-4 price-range'}>*/}
              {/*    <Controller*/}
              {/*      control={control}*/}
              {/*      name={'minPrice'}*/}
              {/*      render={({ field }) => (*/}
              {/*        <InputNumber*/}
              {/*          {...field}*/}
              {/*          placeholder={'Từ'}*/}
              {/*          className={'rounded-md h-10'}*/}
              {/*          style={{*/}
              {/*            height: 40,*/}
              {/*          }}*/}
              {/*        />*/}
              {/*      )}*/}
              {/*    />*/}
              {/*    <div className={'text-price w-3 h-px bg-price'} />*/}
              {/*    <Controller*/}
              {/*      control={control}*/}
              {/*      name={'maxPrice'}*/}
              {/*      render={({ field }) => (*/}
              {/*        <InputNumber*/}
              {/*          {...field}*/}
              {/*          placeholder={'Đến'}*/}
              {/*          className={'rounded-md h-10'}*/}
              {/*        />*/}
              {/*      )}*/}
              {/*    />*/}
              {/*  </div>*/}
              {/*  <div className={'mt-6 flex justify-end'}>*/}
              {/*    <Button*/}
              {/*      className={*/}
              {/*        'bg-price text-white text-base rounded-full px-4 border-0'*/}
              {/*      }*/}
              {/*      type={'primary'}*/}
              {/*      htmlType={'submit'}*/}
              {/*      // onClick={() => {*/}
              {/*      //   reset({*/}
              {/*      //     minPrice: 0,*/}
              {/*      //     maxPrice: 0,*/}
              {/*      //   })*/}
              {/*      // }}*/}
              {/*    >*/}
              {/*      Lọc*/}
              {/*    </Button>*/}
              {/*  </div>*/}
              {/*</div>*/}
            </Form>
          </div>
          <div className={'md:w-9/12 w-full'}>
            {products?.data?.length > 0 ? (
              <div className={'grid md:grid-cols-3 grid-cols-2 md:gap-4 gap-2'}>
                {products?.data.map((product) => (
                  <HomeProductBlock
                    key={`product=${product.id}`}
                    product={product}
                    pub_id={Number(pub_id)}
                  />
                ))}
              </div>
            ) : (
              <NoProduct />
            )}
            {products?.pageCount > 1 && (
              <div className={'mt-12 flex justify-center mb-10'}>
                <Pagination
                  showSizeChanger={false}
                  defaultCurrent={products.page}
                  defaultPageSize={12}
                  total={products.total}
                  onChange={(page, pageSize) =>
                    router.push({
                      pathname: `/danh-muc/${productCategory.slug}_${productCategory.id}`,
                      query: {
                        ...(pub_id && { pub_id: Number(pub_id) }),
                        page,
                        pageSize,
                      },
                    })
                  }
                />
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  )
}
export default Category

import { ColumnsType } from 'antd/es/table'
import React, { FC, useState } from 'react'
import { Button, Card, Col, Form, Input, Row, Space } from 'antd'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { ServiceParams } from 'utils/interfaces'
import { BankService, ProductCategoryEntity } from 'services'
import Content from 'components/layout/AdminLayout/Content'
import DataTable from 'components/common/DataTable'
import FormItem from '../../components/common/FormItem'
import { useRecoilValue } from 'recoil'
import { countDataTable } from '../../recoil/Atoms'
import { formatDate } from '../../utils'

interface Inputs {
  name: string
}

const Bank: FC = () => {
  const [title] = useState<string>('Quản lý Ngân hàng')
  const countDatatable = useRecoilValue(countDataTable)
  const { control, handleSubmit, reset } = useForm<Inputs>({
    defaultValues: {
      name: '',
    },
  })
  const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
    sort: ['createdAt,DESC'],
    join: ['parent'],
  })
  const columns: ColumnsType<ProductCategoryEntity> = [
    {
      dataIndex: 'name',
      title: 'Tên',
      width: 500,
    },
    {
      dataIndex: 'shortName',
      title: 'Viết tắt',
    },
    {
      dataIndex: 'createdAt',
      title: 'Ngày tạo',
      render: (value) => formatDate(value, 'HH:mm dd/MM/y'),
      width: 200,
    },
  ]
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    const filter = []
    const or = []
    if (data?.name)
      filter.push(`name||$contL||${data?.name}`) &&
        or.push(`shortName||$contL||${data?.name}`)

    setTableServiceParams((prevState) => ({
      ...prevState,
      filter: filter,
      or: or,
    }))
  }
  const onReset = () => {
    reset()
    setTableServiceParams({
      sort: ['createdAt,DESC'],
    })
  }
  return (
    <Content
      title={title}
      endButtons={[
        {
          text: 'Thêm mới',
          type: 'primary',
          visible: true,
          href: '/bank/create',
        },
      ]}
    >
      <Card>
        <Form
          autoComplete={'off'}
          onFinish={handleSubmit(onSubmit)}
          onReset={onReset}
          colon={false}
        >
          <Row gutter={[8, 16]}>
            <Col xs={24} md={12} lg={8} />
          </Row>
          <Row gutter={[8, 8]}>
            <Col xs={24} sm={24} md={24} lg={12}>
              <Form.Item label={'Tên/ Viết tắt ngân hàng'}>
                <Controller
                  control={control}
                  name={'name'}
                  render={({ field }) => (
                    <Input
                      {...field}
                      placeholder={'Nhập Tên hoặc tên viết tắt'}
                    />
                  )}
                />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={24} lg={12} className={'text-right'}>
              <FormItem>
                <Space>
                  <Button htmlType={'reset'}>Đặt lại</Button>
                  <Button type={'primary'} htmlType={'submit'}>
                    Tìm kiếm
                  </Button>
                </Space>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Card>
      <Card
        title={<span className={'text-2xl'}>{countDatatable} Ngân hàng</span>}
      >
        <DataTable
          service={BankService.getManyBase}
          serviceParams={tableServiceParams}
          columns={columns}
          action={['edit']}
          path={'/bank'}
        />
      </Card>
    </Content>
  )
}
export default Bank

import React, { FC, useEffect, useState } from 'react'
import { Button, Card, Col, Form, Input } from 'antd'
import FormItem from 'components/common/FormItem'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import Content from 'components/layout/AdminLayout/Content'
import {
  BankEntity,
  BankService,
} from 'services'
import * as yup from 'yup'
import { useRouter } from 'next/router'
import { useSetRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'
import { yupResolver } from '@hookform/resolvers/yup'
import { alertError, modifyEntity } from 'utils'
import FooterBar from 'components/layout/AdminLayout/FooterBar'

interface Inputs {
  bank: BankEntity
}

const schema = yup.object().shape({
  bank: yup.object().shape({
    name: yup.string().required('Chưa nhập Tên ngân hàng'),
    shortName: yup.string().required('Chưa nhập Tên viết tắt ngân hàng'),
  }),
})
const EditBank: FC = () => {
  const [title, setTitle] = useState<string>('Thông tin Ngân hàng')
  const setIsLoading = useSetRecoilState(loadingState)
  const router = useRouter()

  useEffect(() => {
    setIsLoading(true)
    if (router.query?.id) {
      if (router.query?.id == 'create') {
        setTitle('Thêm mới Ngân hàng')
      }
      Promise.all([
        router.query?.id !== 'create' &&
          BankService.getOneBase({
            id: Number(router?.query?.id),
          }),
      ])
        .then(([userResponse]) => {
          setValue('bank', userResponse)
          setIsLoading(false)
        })
        .catch((error) => {
          alertError(error)
          setIsLoading(false)
        })
    }
  }, [router])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    modifyEntity(BankService, data.bank, 'Cập nhật thông tin Ngân hàng', () => {
      return router.push(`/bank`)
    }).then()
  }

  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm<Inputs>({
    mode: 'all',
    resolver: yupResolver(schema),
  })
  return (
    <Content title={title} onBack={() => router.push('/bank')}>
      <Card>
        <Form onFinish={handleSubmit(onSubmit)} autoComplete={'off'} colon={false}>
          <Col xs={24} sm={24} md={24} lg={12} xl={8}>
            <FormItem
              label={<span>Tên</span>}
              validateStatus={errors.bank?.name && 'error'}
              required={true}
              help={errors.bank?.name && errors.bank?.name?.message}
            >
              <Controller
                control={control}
                name={'bank.name'}
                render={({ field }) => (
                  <Input
                    {...field}
                    size="middle"
                    placeholder="Tên thành viên"
                  />
                )}
              />
            </FormItem>
          </Col>
          <Col xs={24} sm={24} md={24} lg={12} xl={8}>
            <FormItem
              label={<span>Tên viết tắt</span>}
              validateStatus={errors.bank?.shortName && 'error'}
              required={true}
              help={errors.bank?.shortName && errors.bank?.shortName?.message}
            >
              <Controller
                control={control}
                name={'bank.shortName'}
                render={({ field }) => (
                  <Input {...field} size="middle" placeholder="Tên viết tắt" />
                )}
              />
            </FormItem>
          </Col>
          <FooterBar
            right={
              <Button htmlType={'submit'} type={'primary'}>
                Lưu
              </Button>
            }
          />
        </Form>
      </Card>
    </Content>
  )
}

export default EditBank

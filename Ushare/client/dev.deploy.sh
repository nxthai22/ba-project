ssh root@143.198.80.63 'bash -s' <<'ENDSSH'
  cd /root/root
  docker-compose pull client
  docker stop ushare-client-dev
  docker-compose up -d client
  docker image prune -f
ENDSSH

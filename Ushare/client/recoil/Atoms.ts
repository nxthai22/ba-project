import { atom } from 'recoil'
import { JwtPayload } from 'jwt-decode'
import { ProductEntity } from 'services'

export interface CartProduct {
  quantity?: number
  replace?: boolean
  productVariantId?: number
  merchantAddressId?: number
  id?: number
}

export interface CartState {
  [pub_id: number]: {
    [merchantAddressId: number]: CartProduct[]
  }
}

export interface Auth extends JwtPayload {
  username: string
  tel?: string
  fullName?: string
  permissions: string[]
  roleId: number
  type: string
  merchantId: number
}

export interface OrderInfo {
  fullname?: string
  tel?: string
  shippingAddress?: string
  note?: string
}

export const authState = atom<Auth>({
  key: 'AuthState',
  default: null,
})

export const siderCollapedState = atom<boolean>({
  key: 'SiderCollaped',
  default: false,
})

export const loadingState = atom<boolean>({
  key: 'LoadingState',
  default: false,
})

export const sidebarCollapsedState = atom<boolean>({
  key: 'SidebarCollapsedState',
  default: false,
})

export const countDataTable = atom<number>({
  key: 'CountDataTable',
  default: 0,
})
export const cartState = atom<CartState>({
  key: 'CartState',
  default: {},
})
export const buyNowCartState = atom<CartState>({
  key: 'BuyNowCartState',
  default: {},
})
export const showAddToCartSuccessState = atom<boolean>({
  key: 'ShowAddToCartSuccessState',
  default: false,
})

export const orderInfoState = atom<OrderInfo>({
  key: 'OrderInfoState',
  default: null,
})

export const childSelectedRowKeysState = atom({
  key: 'ChildSelectedRowKeysState',
  default: [],
})

export const merchantAddressCodesState = atom({
  key: 'MerchantAddressCodesState',
  default: {},
})

import { selector } from 'recoil'
import { cartState } from 'recoil/Atoms'

export const cartStatusState = selector({
  key: 'CartStatusState',
  get: ({ get }) => {
    const cart = get(cartState)
    const totalItems = cart ? Object.entries(cart).length : 0
    return {
      totalItems,
    }
  },
})

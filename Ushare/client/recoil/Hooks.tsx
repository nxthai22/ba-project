import { useSetRecoilState } from 'recoil'
import { buyNowCartState, CartProduct, cartState } from 'recoil/Atoms'
import { useRouter } from 'next/router'
import { setCookie } from 'nookies'
import { Modal } from 'antd'

export const useAddCartProducts = () => {
  const setCart = useSetRecoilState(cartState)
  const router = useRouter()
  return (products: CartProduct[], showPopup = true, pub_id: number) => {
    if (!pub_id) pub_id = 0
    setCart((prevState) => {
      let copy = { ...prevState?.[pub_id] } || {}
      products.map((product) => {
        const merchantProducts = copy?.[product.merchantAddressId]
          ? [...copy[product.merchantAddressId]]
          : []
        if (product.quantity === 0) {
          const productIndex = merchantProducts.findIndex(
            (merchantProduct) =>
              product.productVariantId === merchantProduct.productVariantId &&
              product.id === merchantProduct.id
          )
          if (productIndex > -1) {
            merchantProducts.splice(productIndex, 1)
          }
        } else {
          const productIndex = copy?.[product.merchantAddressId]?.findIndex(
            (merchantAddressProduct) => {
              if (product?.productVariantId) {
                return (
                  merchantAddressProduct?.productVariantId ===
                  product?.productVariantId
                )
              } else return merchantAddressProduct.id === product.id
            }
          )
          if (productIndex === undefined || productIndex === -1) {
            merchantProducts.push(product)
          } else if (productIndex >= 0) {
            merchantProducts[productIndex] = {
              ...copy[product.merchantAddressId]?.[productIndex],
              ...(product.quantity && {
                quantity: product?.replace
                  ? product?.quantity
                  : (product?.quantity || 1) +
                    (copy[product.merchantAddressId]?.[productIndex]
                      ?.quantity || 0),
              }),
              ...(product.productVariantId && {
                productVariantId: product.productVariantId,
              }),
            }
          }
        }
        copy = {
          ...copy,
          [product.merchantAddressId]: merchantProducts,
        }
      })
      const tmpCartState = {
        ...prevState,
        ...{ [pub_id]: copy },
      }
      setCookie(null, 'cart', JSON.stringify(tmpCartState), {
        path: '/',
      })
      return tmpCartState
    })
    if (router.asPath != '/cart' && showPopup) {
      let secondsToGo = 2
      const modal = Modal.success({
        maskClosable: true,
        okText: null,
        okButtonProps: { style: { display: 'none' } },
        icon: null,
        centered: true,
        className: 'add-to-cart-success',
        width: 717,
        content: (
          <div>
            <div className={'mx-auto w-24 md:w-48 add-to-cart-success-icon'}>
              <svg
                width="100%"
                viewBox="0 0 193 180"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M35.6005 51.4534C36.5851 51.4534 37.4153 51.4534 38.2454 51.4534C61.8602 51.4534 85.471 51.4688 109.086 51.4148C110.619 51.4109 111.29 51.8199 111.804 53.3055C114.754 61.8676 119.936 68.8631 127.125 74.35C128.156 75.1372 128.565 75.9243 128.558 77.2169C128.5 84.9302 128.53 92.6473 128.53 100.364C128.53 101.117 128.53 101.869 128.53 102.861C132.936 102.861 137.102 102.907 141.261 102.784C141.647 102.772 142.23 101.742 142.338 101.117C143.319 95.5489 144.211 89.9656 145.137 84.3861C145.218 83.8922 145.35 83.406 145.489 82.7694C154.713 84.4093 163.617 83.599 172.551 79.7867C172.324 80.7205 172.192 81.361 172.007 81.9861C168.304 94.291 164.597 106.592 160.887 118.893C160.111 121.471 159.412 122.026 156.779 122.03C120.812 122.034 84.8455 122.034 48.879 122.026C46.3615 122.026 45.4542 121.247 44.8248 118.565C40.6741 100.932 36.5388 83.2941 32.3958 65.6605C29.415 52.9659 26.5114 40.252 23.4186 27.5805C20.9861 17.61 14.8237 10.8228 5.20177 7.20345C4.24034 6.84075 3.25189 6.54364 2.29433 6.17322C0.460284 5.45939 -0.369861 3.90439 0.155254 2.22978C0.688091 0.520439 2.31363 -0.478926 4.14767 0.231048C7.66904 1.59698 11.3024 2.87802 14.4955 4.8343C22.5615 9.77711 27.4419 17.1431 29.6042 26.3264C31.4305 34.0821 33.2452 41.8378 35.0716 49.5897C35.2028 50.1492 35.3843 50.6932 35.6005 51.4534ZM58.5859 70.6998C58.5357 70.8966 58.4971 70.9583 58.5087 71.0162C60.1844 81.2877 61.8447 91.5592 63.59 101.819C63.6633 102.24 64.505 102.795 65.0186 102.83C66.8758 102.953 68.7484 102.88 70.6134 102.88C72.7679 102.88 74.9224 102.88 77.0962 102.88C77.0962 91.9604 77.0962 81.3842 77.0962 70.7037C70.8759 70.6998 64.7908 70.6998 58.5859 70.6998ZM96.456 102.691C100.865 102.691 105.059 102.691 109.24 102.691C109.24 91.9373 109.24 81.3726 109.24 70.7384C104.923 70.7384 100.73 70.7384 96.456 70.7384C96.456 81.442 96.456 92.0068 96.456 102.691Z"
                  fill="#80B4FB"
                />
                <path
                  d="M154.331 77.0521C132.948 77.0135 115.778 59.7349 115.824 38.2968C115.867 17.2214 133.138 0.00450922 154.196 0.0469533C175.842 0.0893974 192.947 17.1944 192.897 38.7405C192.846 59.8661 175.506 77.0907 154.331 77.0521ZM153.173 49.6255C152.606 48.7188 152.328 48.0242 151.841 47.5342C147.134 42.7804 142.404 38.0499 137.655 33.3347C135.999 31.6871 134.149 31.6138 132.744 33.0492C131.408 34.4151 131.458 36.2672 133.014 37.8338C139.273 44.1309 145.547 50.4088 151.853 56.6558C153.814 58.5966 156.166 58.2532 157.401 55.8031C162.729 45.2422 168.008 34.662 173.27 24.0703C174.193 22.2105 173.514 20.2928 171.815 19.4863C170.193 18.7146 168.421 19.3474 167.44 21.161C166.371 23.1404 165.401 25.17 164.394 27.1803C160.78 34.4267 157.158 41.6653 153.173 49.6255Z"
                  fill="#80B4FB"
                />
                <path
                  d="M146.368 160.612C148.731 169.066 147.561 173.974 142.469 177.574C137.978 180.75 131.916 180.703 127.476 177.462C122.483 173.816 121.348 168.838 123.738 160.743C107.726 160.743 91.7564 160.743 75.5821 160.743C78.5706 168.356 77.2115 174.684 69.6668 178.681C64.9369 181.186 59.0332 179.99 55.23 176.22C50.9132 171.941 50.446 166.728 53.7666 159.528C53.0523 158.91 52.2492 158.351 51.6198 157.641C48.8321 154.5 48.1062 150.881 49.89 147.091C52.6507 141.223 55.6393 135.458 58.6008 129.686C58.8517 129.196 59.6047 128.709 60.1529 128.675C62.0063 128.551 63.8751 128.628 66.0489 128.628C65.6937 129.419 65.4388 130.044 65.1415 130.646C62.1607 136.623 59.1683 142.596 56.1837 148.573C54.396 152.154 55.7126 154.353 59.6857 154.357C89.6598 154.369 119.634 154.365 149.612 154.365C152.863 154.365 154.346 155.387 154.28 157.587C154.218 159.709 152.824 160.612 149.619 160.616C148.592 160.612 147.569 160.612 146.368 160.612Z"
                  fill="#80B4FB"
                />
              </svg>
            </div>
            <div
              className={
                'text-center font-semibold text-secondary text-base md:text-2xl mt-5 md:mt-9'
              }
            >
              Sản phẩm đã được thêm vào giỏ hàng!
            </div>
          </div>
        ),
      })
      const timer = setInterval(() => {
        secondsToGo -= 1
      }, 1000)
      setTimeout(() => {
        clearInterval(timer)
        modal.destroy()
      }, secondsToGo * 1000)
    }
  }
}

export const useAddCartBuyNow = () => {
  const setCartBuyNow = useSetRecoilState(buyNowCartState)
  return (product: CartProduct, pub_id: number) => {
    if (!pub_id) pub_id = 0
    setCartBuyNow((prevState) => {
      return {
        [pub_id]: {
          [product.merchantAddressId]: [product],
        },
      }
    })
  }
}

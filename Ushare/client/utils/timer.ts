import {
  startOfWeek,
  startOfMonth,
  startOfQuarter,
  startOfYear,
  endOfWeek,
  endOfMonth,
  endOfQuarter,
  endOfYear,
  subDays,
  subWeeks,
  subMonths,
  subQuarters,
  subYears,
} from 'date-fns'

const today = new Date()
const lastWeek = subWeeks(today, 1)
const lastMonth = subMonths(today, 1)
const lastQuarter = subQuarters(today, 1)
const lastYear = subYears(today, 1)

export const getTime = {
  today,
  yesterday: subDays(today, 1),
  thisWeekStart: startOfWeek(today),
  thisWeekEnd: endOfWeek(today),
  lastWeek,
  lastWeekStart: startOfWeek(lastWeek),
  lastWeekEnd: endOfWeek(lastWeek),
  thisMonthStart: startOfMonth(today),
  thisMonthEnd: endOfMonth(today),
  lastMonth,
  lastMonthStart: startOfMonth(lastMonth),
  lastMonthEnd: endOfMonth(lastMonth),
  thisQuarterStart: startOfQuarter(today),
  thisQuarterEnd: endOfQuarter(today),
  lastQuarter,
  lastQuarterStart: startOfQuarter(lastQuarter),
  lastQuarterEnd: endOfQuarter(lastQuarter),
  thisYearStart: startOfYear(today),
  thisYearEnd: endOfYear(today),
  lastYear,
  lastYearStart: startOfYear(lastYear),
  lastYearEnd: endOfYear(lastYear),
}

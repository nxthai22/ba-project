import slugify from 'slugify'
import { differenceInMilliseconds, format, isWithinInterval } from 'date-fns'
import viLocale from 'date-fns/locale/vi'
import { notification } from 'antd'
import { OptionData, OptionGroupData } from 'rc-select/lib/interface'
import {
  DistrictEntity,
  EnumDistrictEntityType,
  EnumOrderEntityPaymentStatus,
  EnumOrderEntityStatus,
  EnumOrderHistoryEntityType,
  EnumOrderPaymentHistoryEntityPaymentStatus,
  EnumProvinceEntityType,
  EnumShippingInfoPartner,
  EnumShippingInfoStatus,
  EnumUserEntityStatus,
  EnumUserTransactionEntityStatus,
  EnumWardEntityType,
  OrderHistoryEntity,
  ProvinceEntity,
  WardEntity,
} from 'services'
import { Auth } from 'recoil/Atoms'
import { StatusLabelByTime } from 'enums'

export const formatCurrency = (value: number): string => {
  if (isNaN(value)) value = 0
  return Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND',
  }).format(value)
}

export const formatNumber = (value: number): string => {
  return Intl.NumberFormat('vi-VN').format(value)
}

export const getSlug = (name: string): string => {
  if (typeof name === 'string') {
    return slugify(name, {
      locale: 'vi',
    })
  }
  return ''
}

// Removes empty query parameters from the query object
export const filterQuery = (query: Record<string, any>): Record<string, any> =>
  Object.keys(query).reduce<any>((obj, key) => {
    if (query[key]?.length) {
      obj[key] = query[key]
    }
    return obj
  }, {})

export const filterdQuery = (
  key: string,
  filters: string | string[]
): string[] => {
  if (typeof filters === 'string') filters = [filters]

  return (
    filters &&
    filters.map((filterData) => {
      const filter = filterData.split('||')
      if (filter[0] === key) {
        filter.shift()
        return filter.join('||')
      }
    }, [])
  )
}
export const formatDate = (
  date?: string | number | Date,
  formatString?: string
): string => {
  if (!date) date = new Date()
  date = new Date(date)
  if (!formatString) formatString = 'HH:mm:ss dd/MM/y'
  return format(date, formatString, { locale: viLocale })
}

export const formatDateDDMMYY = (date?: number | Date): string => {
  if (!date) date = new Date()
  date = new Date(date)
  return format(date, 'dd-MM-Y', { locale: viLocale })
}
export const alertSuccess = (message: string) => {
  return notification.success({
    placement: 'topRight',
    message: (
      <div
        dangerouslySetInnerHTML={{
          __html: message,
        }}
      />
    ),
  })
}
export const alertError = (error: any) => {
  if (typeof error?.response?.data?.message === 'object') {
    const messages =
      error?.response?.data?.message?.message || error?.response?.data?.message
    return notification.error({
      placement: 'topRight',
      message: (
        <>
          {messages?.map((errorMsg, index) => (
            <div
              key={`success-msg-${index}`}
              dangerouslySetInnerHTML={{ __html: errorMsg }}
            />
          ))}
        </>
      ),
    })
  }
  return notification.error({
    placement: 'topRight',
    message: (
      <div
        dangerouslySetInnerHTML={{
          __html: error?.response?.data?.message || error?.message || error,
        }}
      />
    ),
  })
}

export const filterOption = (
  input: string,
  option: OptionData | OptionGroupData | undefined
) => {
  const inputValue = input
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .replace(/đ/g, 'd')
      .replace(/Đ/g, 'D')
      .toLowerCase(),
    optionValue =
      typeof option?.children === 'string' ? option?.children : option?.title

  return (
    optionValue
      ?.normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .replace(/đ/g, 'd')
      .replace(/Đ/g, 'D')
      .toLowerCase()
      .indexOf(inputValue) >= 0
  )
}

export const modifyEntity = (
  service,
  data,
  title: string | { message: string } = '',
  onSucess: (response) => void,
  onError?: (error) => void
) => {
  return new Promise<any>((resolve) => {
    if (Number(data.id))
      return resolve(
        service.updateOneBase({
          id: data.id,
          body: data,
        })
      )
    else
      return resolve(
        service.createOneBase({
          body: data,
        })
      )
  })
    .then((response) => {
      if (typeof title === 'object') alertSuccess(title.message)
      else alertSuccess(`${title ? title : ''} thành công`)

      onSucess(response)
    })
    .catch((error) => {
      if (onError) {
        onError(error)
      }
      alertError(error)
      return null
    })
}
export const getStatusUser = (status: EnumUserEntityStatus) => {
  switch (status) {
    case EnumUserEntityStatus.active:
      return 'Hoạt động'
    case EnumUserEntityStatus.inactive:
      return 'Không hoạt động'
    case EnumUserEntityStatus.banned:
      return 'Đã khóa'
    case EnumUserEntityStatus.deleted:
      return 'Đã xóa'
  }
}
export const getRouteName = (route) => {
  switch (route) {
    case 'cdn':
      return 'CDN'
    case 'config':
      return 'Cài đặt'
    case 'customers':
      return 'Khách hàng'
    case 'flash-sale':
      return 'Flash Sale'
    case 'merchant-addresses':
      return 'Kho hàng'
    case 'merchants':
      return 'Nhà cung cấp'
    case 'orders':
      return 'Đơn hàng'
    case 'product-categories':
      return 'Danh mục Sản phẩm'
    case 'product-post':
      return 'Bài đăng Sản phẩm'
    case 'products':
      return 'Sản phẩm'
    case 'revenue':
      return 'Thống kê Doanh thu'
    case 'shipping-addresses':
      return 'Địa chỉ Nhận hàng'
    case 'users':
      return 'Cộng tác viên'
    case 'vouchers':
      return 'Mã giảm giá'
    case 'wallets':
      return 'Ví'
    case 'cart-product':
      return 'Chi tiết giỏ hàng'
    case 'cart':
      return 'Giỏ hàng'
    case 'notification-configs':
      return 'Cài đặt thông báo'
    case 'order-stats':
      return 'Thống kê đơn hàng'
    case 'collaborator-stats':
      return 'Thống kê CTV'
    case 'product-stats':
      return 'Thống kê sản phẩm'
    case 'customer-stats':
      return 'Thống kê khách hàng'
    case 'group-buying':
      return 'Mua chung'
    default:
      return ''
  }
}
export const getStatusTransaction = (status: string): string => {
  switch (status) {
    case EnumUserTransactionEntityStatus.created:
      return 'Chờ xử lý'
    case EnumUserTransactionEntityStatus.completed:
      return 'Đã xác nhận'
    case EnumUserTransactionEntityStatus.cancelled:
      return 'Đã hủy'
    default:
      return ''
  }
}
export const getWardTypeName = (ward?: WardEntity, uppercase = false) => {
  if (ward)
    switch (ward.type) {
      case EnumWardEntityType.ward:
        return uppercase ? 'Phường' : 'phường'
      case EnumWardEntityType.commune:
        return uppercase ? 'Xã' : 'xã'
      case EnumWardEntityType.town:
        return uppercase ? 'Thị trấn' : 'thị trấn'
    }
  return ''
}

export const getWardName = (ward?: WardEntity) => {
  let prefix = ''
  if (ward)
    switch (ward.type) {
      case EnumWardEntityType.ward:
        prefix = 'Phường'
        break
      case EnumWardEntityType.commune:
        prefix = 'Xã'
        break
      case EnumWardEntityType.town:
        prefix = 'Thị trấn'
        break
    }
  return prefix + ' ' + ward.name
}

export const getDistrictTypeName = (
  district?: DistrictEntity,
  uppercase = false
) => {
  if (district)
    switch (district.type) {
      case EnumDistrictEntityType.district:
        return uppercase ? 'Huyện' : 'huyện'
      case EnumDistrictEntityType.town:
        return uppercase ? 'Quận' : 'quận'
      case EnumDistrictEntityType.township:
        return uppercase ? 'Thị xã' : 'thị xã'
      case EnumDistrictEntityType.city:
        return uppercase ? 'Thành phố' : 'thành phố'
    }
  return ''
}

export const getDistrictName = (district?: DistrictEntity) => {
  let prefix = ''
  if (district)
    switch (district.type) {
      case EnumDistrictEntityType.district:
        prefix = 'Huyện'
        break
      case EnumDistrictEntityType.town:
        prefix = 'Quận'
        break
      case EnumDistrictEntityType.township:
        prefix = 'Thị xã'
        break
      case EnumDistrictEntityType.city:
        prefix = 'Thành phố'
        break
    }
  return prefix + ' ' + district.name
}

export const getProvinceTypeName = (
  province?: ProvinceEntity,
  uppercase = false
) => {
  if (province)
    switch (province.type) {
      case EnumProvinceEntityType.city:
        return uppercase ? 'Thành phố' : 'thành phố'
      case EnumProvinceEntityType.province:
        return uppercase ? 'Tỉnh' : 'tỉnh'
    }
  return ''
}

export const getProvinceName = (province?: ProvinceEntity) => {
  let prefix = ''
  if (province)
    switch (province.type) {
      case EnumProvinceEntityType.city:
        prefix = 'Thành phố'
        break
      case EnumProvinceEntityType.province:
        prefix = 'Tỉnh'
        break
    }
  return prefix + ' ' + province.name
}
export const getFullAddress = (ward: WardEntity) => {
  if (ward)
    return `${getWardTypeName(ward)} ${ward.name}, ${getDistrictTypeName(
      ward.district
    )} ${ward?.district.name}, ${getProvinceTypeName(
      ward?.district?.province
    )} ${ward?.district?.province.name}`
  return ''
}

export const getOrderStatusDesc = (
  status: EnumOrderEntityStatus | OrderHistoryEntity | string
) => {
  let tmpStatus = status
  if (typeof status === 'object') tmpStatus = status?.status

  switch (tmpStatus) {
    case EnumOrderEntityStatus.created:
      return 'Chờ xác nhận'
    case EnumOrderEntityStatus.confirmed:
      return 'Đã xác nhận'
    case EnumOrderEntityStatus.cancelled:
      return 'Đã huỷ'
    case EnumOrderEntityStatus.merchant_cancelled:
      return 'Đã huỷ bởi Đối tác'
    case EnumOrderEntityStatus.processing:
      return 'Đang xử lý'
    case EnumOrderEntityStatus.pending:
      return 'Chờ xác nhận'
    case EnumOrderEntityStatus.onhold:
      return 'Tạm giữ'
    case EnumOrderEntityStatus.completed:
      return 'Đã đối soát'
    case EnumOrderEntityStatus.refunded:
      return 'Hoàn tiền hàng'
    case EnumOrderEntityStatus.return:
      return 'Hoàn hàng'
    case EnumOrderEntityStatus.failed:
      return 'Giao hàng Thất bại'
    case EnumOrderEntityStatus.shipping:
      if (typeof status === 'object') {
        if (status.type === EnumOrderHistoryEntityType.shipment)
          return 'Đang giao cho Khách'
        return 'Chuyển Giao hàng'
      }
      return 'Đang giao'
    case EnumOrderEntityStatus.shipped:
      if (
        typeof status === 'object' &&
        status.type === EnumOrderHistoryEntityType.shipment
      )
        return 'Đã giao thành công'
      return 'Đã giao, chờ đối soát'
    case EnumOrderEntityStatus.ready_to_pick:
      return 'Tiếp nhận đơn hàng'
    case EnumOrderEntityStatus.picking:
      return 'Lấy hàng'
    case EnumOrderEntityStatus.waiting_for_grouping:
      return 'Chờ ghép đơn'
  }
}
export const getPaymentStatusDesc = (
  status:
    | EnumOrderEntityPaymentStatus
    | EnumOrderPaymentHistoryEntityPaymentStatus
    | string
) => {
  let tmpStatus = status
  switch (tmpStatus) {
    case EnumOrderEntityPaymentStatus.wait_deposit:
      return 'Chờ CTV xác nhận đặt cọc'
    case EnumOrderEntityPaymentStatus.confirm_deposit:
      return 'CTV xác nhận đặt cọc'
    case EnumOrderEntityPaymentStatus.deposited:
      return 'Kế toán xác nhận đặt cọc'
    case EnumOrderEntityPaymentStatus.returned_deposit:
      return 'Đã hoàn cọc'
  }
}
export const getShippingPartnerName = (partner: EnumShippingInfoPartner) => {
  switch (partner) {
    case EnumShippingInfoPartner.ghn:
      return 'Giao hàng nhanh'
    case EnumShippingInfoPartner.ghtk:
      return 'Giao hàng tiết kiệm'
    case EnumShippingInfoPartner.viettelpost:
      return 'Viettel post'
    case EnumShippingInfoPartner.other:
      return 'Khác'
    default:
      return ''
  }
}

export const getOrderShippingStatus = (status: EnumShippingInfoStatus) => {
  switch (status) {
    case EnumShippingInfoStatus.ready_to_pick:
      return 'Chờ giao/lấy hàng'
    case EnumShippingInfoStatus.shipping:
      return 'Đang giao hàng'
    case EnumShippingInfoStatus.fail:
      return 'Thất bại'
    case EnumShippingInfoStatus.shipped:
      return 'Chờ thu COD'
    case EnumShippingInfoStatus.cancelled:
      return 'Huỷ giao hàng'
    case EnumShippingInfoStatus.completed:
      return 'Hoàn thành'
    case EnumShippingInfoStatus.return:
      return 'Đã hoàn trả thành công về Kho'
  }
}

export const getCdnFile = (url?: string) => {
  if (!url) return ''
  if (!url.includes('http') && !url.includes('https'))
    return process.env.NEXT_PUBLIC_IMAGE_URL + url
  return url
}
export const hasPermission = (user: Auth, permissions?: string[]): boolean => {
  if (user?.roleId === 1) return true
  let hasPermission = false
  permissions?.map((permission) => {
    if (user?.permissions?.includes(permission)) hasPermission = true
  })
  return hasPermission
}
export const hasPermissionType = (user: Auth, types?: string[]): boolean => {
  return types.includes(user?.type)
}
const facebookAppId = process.env.NEXT_PUBLIC_FACEBOOK_APP_ID

export function initFacebookSdk() {
  return new Promise((resolve) => {
    // wait for facebook sdk to initialize before starting the react app
    // @ts-ignore
    window.fbAsyncInit = function () {
      // @ts-ignore
      window.FB.init({
        appId: facebookAppId,
        cookie: true,
        xfbml: true,
        version: 'v8.0',
      })
    }

    // load facebook sdk script
    ;(function (d, s, id) {
      let js,
        fjs = d.getElementsByTagName(s)[0]
      if (d.getElementById(id)) {
        return
      }
      js = d.createElement(s)
      js.id = id
      js.src = 'https://connect.facebook.net/en_US/sdk.js'
      fjs.parentNode.insertBefore(js, fjs)
    })(document, 'script', 'facebook-jssdk')
  })
}

export const _findInTree = (route, path): string[] => {
  // If current node name matches the search name, return
  // empty array which is the beginning of our parent result
  if ([route.path, route.key].includes(path)) {
    return [route.path || route.key]
  }

  // Otherwise, if this node has a tree field/value, recursively
  // process the nodes in this tree array
  if (Array.isArray(route.children)) {
    for (const children of route.children) {
      // Recursively process treeNode. If an array result is
      // returned, then add the treeNode.name to that result
      // and return recursively
      const childResult = _findInTree(children, path)
      if (Array.isArray(childResult)) {
        return [route.path || route.key].concat(childResult)
      }
    }
  }
}
//  "exponential step" curry function
//   return tuple where
//   [0] is the number of inputs we need our slider to have
//   [1] is our output transform function
export const scaleTransform = (
  min: number,
  max: number,
  intervals: number[]
): [number, (input: number) => number] => {
  //determine how many "points" we need
  const distributions = intervals.length
  const descretePoints = Math.ceil(
    (max - min) /
      intervals.reduce((total, step) => total + step / distributions, 0)
  )

  return [
    descretePoints,
    (input: number) => {
      const stepTransforms = intervals.map((s, i) => {
        const setCount = Math.min(
          Math.ceil(input - (descretePoints * i) / distributions),
          Math.round(descretePoints / distributions)
        )
        return setCount > 0 ? setCount * s : 0
      })

      let lastStep = 0
      const out =
        Math.round(
          stepTransforms.reduce((total, num, i) => {
            if (num) {
              lastStep = i
            }
            return total + num
          })
        ) + min

      const currentUnit = intervals[lastStep]
      return Math.min(
        Math.round(out / currentUnit) * currentUnit, //round to nearest step
        max
      )
    },
  ]
}

export const getStatusLabelByTime = (
  startTime: Date | string,
  endTime: Date | string
): StatusLabelByTime => {
  const now = new Date()
  const isInprogress = isWithinInterval(now, {
    start: new Date(startTime),
    end: new Date(endTime),
  })

  if (isInprogress) {
    return StatusLabelByTime.INPROGRESS
  }

  const isComming = differenceInMilliseconds(
    new Date(startTime).getTime(),
    now.getTime()
  )

  if (isComming > 0) {
    return StatusLabelByTime.COMMING
  }

  return StatusLabelByTime.OVER
}

export const replaceStartEndSpace = (value: string) => {
  const wsRegex = /^\s+|\s+$/g
  const newValue = value?.replace(wsRegex, '')
  return newValue
}

export const convertTel = (tel: string) => {
  if (tel.charAt(0) === '0') tel = tel.replace(tel.charAt(0), '+84')
  return tel
}

export const nonAccentVietnamese = (str: string): string => {
  str = str.toLowerCase()
  // We can also use this instead of from line 11 to line 17
  // str = str.replace(/\u00E0|\u00E1|\u1EA1|\u1EA3|\u00E3|\u00E2|\u1EA7|\u1EA5|\u1EAD|\u1EA9|\u1EAB|\u0103|\u1EB1|\u1EAF|\u1EB7|\u1EB3|\u1EB5/g, "a");
  // str = str.replace(/\u00E8|\u00E9|\u1EB9|\u1EBB|\u1EBD|\u00EA|\u1EC1|\u1EBF|\u1EC7|\u1EC3|\u1EC5/g, "e");
  // str = str.replace(/\u00EC|\u00ED|\u1ECB|\u1EC9|\u0129/g, "i");
  // str = str.replace(/\u00F2|\u00F3|\u1ECD|\u1ECF|\u00F5|\u00F4|\u1ED3|\u1ED1|\u1ED9|\u1ED5|\u1ED7|\u01A1|\u1EDD|\u1EDB|\u1EE3|\u1EDF|\u1EE1/g, "o");
  // str = str.replace(/\u00F9|\u00FA|\u1EE5|\u1EE7|\u0169|\u01B0|\u1EEB|\u1EE9|\u1EF1|\u1EED|\u1EEF/g, "u");
  // str = str.replace(/\u1EF3|\u00FD|\u1EF5|\u1EF7|\u1EF9/g, "y");
  // str = str.replace(/\u0111/g, "d");
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
  str = str.replace(/đ/g, 'd')
  // Some system encode vietnamese combining accent as individual utf-8 characters
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, '') // Huyền sắc hỏi ngã nặng
  str = str.replace(/\u02C6|\u0306|\u031B/g, '') // Â, Ê, Ă, Ơ, Ư
  return str
}

export const flattenCategoryTree = (category, data) => {
  if (category) {
    data.push(category)
    if (category.parent) {
      flattenCategoryTree(category.parent, data)
    }
  }
  return data
}

export const youtubeVIDParser = (url) => {
  const regExp =
    /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/
  const match = url.match(regExp)
  return match && match[7].length == 11 ? match[7] : false
}

export const num2Word2 = (function () {
  var t = [
      'không',
      'một',
      'hai',
      'ba',
      'bốn',
      'năm',
      'sáu',
      'bảy',
      'tám',
      'chín',
    ],
    r = function (r, n) {
      var o = '',
        a = Math.floor(r / 10),
        e = r % 10
      return (
        a > 1
          ? ((o = ' ' + t[a] + ' mươi'), 1 == e && (o += ' mốt'))
          : 1 == a
          ? ((o = ' mười'), 1 == e && (o += ' một'))
          : n && e > 0 && (o = ' lẻ'),
        5 == e && a >= 1
          ? (o += ' lăm')
          : 4 == e && a >= 1
          ? (o += ' tư')
          : (e > 1 || (1 == e && 0 == a)) && (o += ' ' + t[e]),
        o
      )
    },
    n = function (n, o) {
      var a = '',
        e = Math.floor(n / 100),
        n = n % 100
      return (
        o || e > 0
          ? ((a = ' ' + t[e] + ' trăm'), (a += r(n, !0)))
          : (a = r(n, !1)),
        a
      )
    },
    o = function (t, r) {
      var o = '',
        a = Math.floor(t / 1e6),
        t = t % 1e6
      a > 0 && ((o = n(a, r) + ' triệu'), (r = !0))
      var e = Math.floor(t / 1e3),
        t = t % 1e3
      return (
        e > 0 && ((o += n(e, r) + ' ngàn'), (r = !0)),
        t > 0 && (o += n(t, r)),
        o
      )
    }
  return {
    convert: function (r) {
      if (0 == r) return t[0]
      var n = '',
        a = '',
        ty
      do
        (ty = r % 1e9),
          (r = Math.floor(r / 1e9)),
          (n = r > 0 ? o(ty, !0) + a + n : o(ty, !1) + a + n),
          (a = ' tỷ')
      while (r > 0)
      return n.trim()
    },
  }
})()

export const capitalizeFirstLetter = (words) => {
  const separateWord = words.toLowerCase().split('-')
  for (let i = 0; i < separateWord.length; i++) {
    separateWord[i] =
      separateWord[i].charAt(0).toUpperCase() + separateWord[i].substring(1)
  }
  return separateWord.join('')
}

export const compactNumber = (value: number) => {
  if (value < 1000) return value
  if (value >= 1000 && value < 1000000) {
    const num = Math.round((value * 10) / 1000) / 10
    if (num === 1000) return '1M'
    return num.toString() + 'K'
  }
  if (value >= 1000000 && value < 1000000000) {
    const num = Math.round((value * 10) / 1000000) / 10
    if (num === 1000) return '1B'
    return num.toString() + 'M'
  }
  if (value >= 1000000000)
    return (Math.round((value * 10) / 1000000000) / 10).toString() + 'B'
}

import { EnumShippingInfoStatus } from '../services'

export const SITE_NAME = 'UShare'
export const SITE_SLOGAN = 'Kinh doanh kiểu mới - Bán hàng rảnh tay'
export const SHIPPING_PARTNER = {
  ghtk: 'Giao hàng tiết kiệm',
  ghn: 'Giao hàng nhanh',
  viettelpost: 'Viettel Post',
  other: 'Khác',
}
export const ROLE_TYPE = {
  admin: 'Admin',
  merchant: 'Nhà cung cấp',
  partner: 'Cộng tác viên',
  leader: 'Trưởng nhóm',
  other: 'Khác',
  accountant: 'Kế toán Ushare',
  management_partner: 'Quản lý CTV',
  merchant_accountant: 'Kế toán NCC',
}

export const SHIPPING_STATUS = {
  ready_to_pick: 'Chờ giao/lấy hàng',
  fail: 'Thất bại',
  shipping: 'Đang giao hàng',
  shipped: 'Chờ thu COD',
  return: 'Đã hoàn trả thành công về Kho',
  completed: 'Hoàn thành',
  cancelled: 'Huỷ giao hàng',
}

export const NOTIFICATION_TYPE = {
  offer: 'Ưu đãi',
  product:'Sản phẩm',
  highlight:'Nổi bật',
  other:'Khác'
}

export const NOTIFICATION_TYPE_ADMIN = {
  offer: 'Ưu đãi',
  other:'Khác'
}


export const NOTIFICATION_STATUS = {
  new: 'Chờ duyệt',
  approved:'Đã duyệt',
  rejected:'Đã từ chối',
  sent:'Đã gửi'
}

export const NOTIFICATION_RECEIVED_TYPE_MERCHANT = {
  followed_collaborator: 'Cộng tác viên đang theo dõi',
  collaborator: 'Tất cả CTV',
  staff: 'Nhân viên',
}

export const NOTIFICATION_RECEIVED_TYPE_ADMIN= {
  collaborator: 'Tất cả CTV',
  ushare_staff: 'Tất cả đối tác',
  all: 'Tất cả mọi người'
}

export const NOTIFICATION_RECEIVED_TYPE_ALL= {
  followed_collaborator: 'Cộng tác viên đang theo dõi',
  staff: 'Nhân viên',
  collaborator: 'Tất cả CTV',
  ushare_staff: 'Tất cả đối tác',
  all: 'Tất cả mọi người'
}



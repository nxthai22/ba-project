/* eslint-disable @typescript-eslint/no-var-requires */
import { MailerModule } from '@nestjs-modules/mailer';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
import { Module, ValidationPipe } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_PIPE } from '@nestjs/core';
import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { getMetadataArgsStorage } from 'typeorm';

const PIPES = [
  {
    provide: APP_PIPE,
    useFactory: () => new ValidationPipe(),
  },
];

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env', 'ormconfig.env'],
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        return {
          type: 'postgres',
          host: configService.get<string>('TYPEORM_HOST'),
          port: configService.get<number>('TYPEORM_PORT'),
          username: configService.get<string>('TYPEORM_USERNAME'),
          password: configService.get<string>('TYPEORM_PASSWORD'),
          database: configService.get<string>('TYPEORM_DATABASE'),
          // logging: true,
          // entities: [join(__dirname, 'src/entities', '*.entity.ts')],
          entities: getMetadataArgsStorage().tables.map((tbl) => tbl.target),
          // synchronize: !['development', 'production'].includes(
          //   process.env.NODE_ENV,
          // ),
        };
      },
    }),
    MailerModule.forRootAsync({
      useFactory: () => ({
        defaults: {
          from: 'cskh.ushare@gmail.com',
        },
        transport: {
          host: 'smtp.gmail.com',
          port: 587,
          auth: {
            user: 'cskh.ushare@gmail.com',
            pass: 'CSKH2022.',
          },
        },
        template: {
          dir: join(__dirname, '../templates/'),
          adapter: new PugAdapter(),
          options: {
            strict: true,
          },
        },
      }),
    }),
    ScheduleModule.forRoot(),
  ],
  providers: [...PIPES],
})
export class CoreModule {}

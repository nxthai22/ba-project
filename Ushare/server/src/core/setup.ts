import { ConfigService } from '@nestjs/config';
import { NestFastifyApplication } from '@nestjs/platform-fastify';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { BankInfo, ConfigBanner } from 'entities/config.entity';
import { fastifyHelmet } from 'fastify-helmet';
import { config } from 'aws-sdk';
import { ServiceAccount } from 'firebase-admin';
import * as firebase from 'firebase-admin';

export const setup = async (app: NestFastifyApplication) => {
  setupSwagger(app);
  setupAWS(app);
  setupFirebase(app);
  await setupHelmet(app);
  await setupCors(app);
  await setupMultipart(app);
};

export function setupSwagger(app: NestFastifyApplication): void {
  // if (process.env.NODE_ENV !== 'production') {
    const docsConfig = new DocumentBuilder()
    .setTitle("UKG's Ushare")
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, docsConfig, {
    extraModels: [ConfigBanner, BankInfo],
  });
    SwaggerModule.setup('docs', app, document);
  // }
}

export async function setupHelmet(app: NestFastifyApplication): Promise<void> {
  await app.register(fastifyHelmet, {
    contentSecurityPolicy: {
      directives: {
        defaultSrc: [`'self'`],
        styleSrc: [`'self'`, `'unsafe-inline'`],
        imgSrc: [`'self'`, 'data:', 'validator.swagger.io'],
        scriptSrc: [`'self'`, `https: 'unsafe-inline'`],
      },
    },
  });
}

export async function setupCors(app: NestFastifyApplication): Promise<void> {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  await app.register(require('fastify-cors'));
}

export async function setupMultipart(
  app: NestFastifyApplication,
): Promise<void> {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  await app.register(require('fastify-multipart'), {
    limits: {
      fieldNameSize: 100, // Max field name size in bytes
      fieldSize: 100, // Max field value size in bytes
      fields: 50, // Max number of non-file fields
      fileSize: 15000000, // For multipart forms, the max file size in bytes
      files: 50, // Max number of file fields
      headerPairs: 2000, // Max number of header key=>value pairs
    },
  });
}

export function setupAWS(app: NestFastifyApplication) {
  const configService = app.get(ConfigService);
  config.update({
    accessKeyId: configService.get('BIZFLYS_STORAGE_ACCESS_KEY'),
    secretAccessKey: configService.get('BIZFLY_STORAGE_SECRET_KEY'),
    region: 'hn',
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    endpoint: 'https://hn.ss.bfcplatform.vn',
    apiVersions: {
      s3: '2006-03-01',
    },
  });
}

export function setupFirebase(app: NestFastifyApplication) {
  const configService = app.get(ConfigService);
  const firebaseConfig: ServiceAccount = {
    projectId: configService.get('FIREBASE_PROJECT_ID'),
    privateKey: configService
      .get('FIREBASE_PRIVATE_KEY')
      ?.replace(/\\n/g, '\n'),
    clientEmail: configService.get('FIREBASE_CLIENT_EMAIL'),
  };
  // Initialize the firebase admin app
  firebase.initializeApp({
    credential: firebase.credential.cert(firebaseConfig),
    databaseURL: configService.get('FIREBASE_DATABASE_URL'),
  });
}

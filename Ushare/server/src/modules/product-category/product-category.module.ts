import { Module } from "@nestjs/common";
import { ProductCategoryService } from "./product-category.service";
import { ProductCategoryController } from "./product-category.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ProductCategoryEntity } from "entities/product-category.entity";
import { ProductCategoryAttributeEntity } from "../../entities/product-category-attribute.entity";
import { ProductCategoryAttributeValueEntity } from "../../entities/product-category-attribute-value.entity";

@Module({
  imports: [TypeOrmModule.forFeature([ProductCategoryEntity, ProductCategoryAttributeEntity, ProductCategoryAttributeValueEntity])],
  controllers: [ProductCategoryController],
  providers: [ProductCategoryService],
  exports: [ProductCategoryService]
})
export class ProductCategoryModule {
}

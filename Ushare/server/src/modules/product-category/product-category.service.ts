import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Connection, EntityManager, In, Not, Repository } from 'typeorm';
import { ErrorMessage } from 'enums';
import { ProductCategoryEntity } from 'entities/product-category.entity';
import { CreateProductCategoryDto } from './dto/create-product-category.dto';
import { ProductCategoryAttributeEntity } from 'entities/product-category-attribute.entity';
import { ProductCategoryAttributeValueEntity } from 'entities/product-category-attribute-value.entity';
import { UserEntity } from 'entities/user.entity';
import { slugify } from 'utils';

@Injectable()
export class ProductCategoryService extends TypeOrmCrudService<ProductCategoryEntity> {
  constructor(
    @InjectRepository(ProductCategoryEntity)
    public repo: Repository<ProductCategoryEntity>,
    @InjectRepository(ProductCategoryAttributeEntity)
    public categoryAttributeRepo: Repository<ProductCategoryAttributeEntity>,
    @InjectRepository(ProductCategoryAttributeValueEntity)
    public categoryAttributeValueRepo: Repository<ProductCategoryAttributeValueEntity>,
    @InjectConnection() private connection: Connection,
  ) {
    super(repo);
  }

  /* Thêm mới danh mục */
  async create(dto: CreateProductCategoryDto): Promise<ProductCategoryEntity> {
    const productCategory = await this.connection.transaction(
      async (manager) => {
        let itemCheck = await this.repo.findOne({
          where: {
            name: dto.name,
          },
        });
        if (!itemCheck && dto.code) {
          itemCheck = await this.repo.findOne({
            where: {
              code: dto.code,
            },
          });
        }
        if (itemCheck) {
          throw new BadRequestException(ErrorMessage.CATEGORY_ALREADY_EXISTS);
        }
        const createdProductCategory = await this.repo.save({
          ...dto,
          slug: slugify(dto.name),
        });
        // const createdProductCategory = await manager.save<ProductCategoryEntity>(dto);
        if (dto.productCategoryAttributes?.length > 0) {
          await Promise.all(
            dto.productCategoryAttributes.map(async (attribute) => {
              attribute.productCategoryId = createdProductCategory.id;
              // const categoryAttribute = await manager.save<ProductCategoryAttributeEntity>(attribute);
              const categoryAttribute = await this.categoryAttributeRepo.save(
                attribute,
              );
              if (attribute.productCategoryAttributeValues?.length > 0) {
                attribute.productCategoryAttributeValues.map(
                  async (attributeValue) => {
                    attributeValue.productCategoryAttributeId =
                      categoryAttribute.id;
                  },
                );
                // await manager.save<ProductCategoryAttributeValueEntity>(attribute.productCategoryAttributeValues);
                await this.categoryAttributeValueRepo.save(
                  attribute.productCategoryAttributeValues,
                );
              }
            }),
          );
        }
        return createdProductCategory;
      },
    );
    return productCategory;
  }

  /* Cập nhật danh mục */
  async update(
    id: number,
    dto: CreateProductCategoryDto,
  ): Promise<ProductCategoryEntity> {
    const productCategoryUpdate = await this.connection.transaction(
      async (manager) => {
        let itemCheck = await this.repo.findOne({
          where: {
            name: dto.name,
            id: Not(In([id])),
          },
        });
        if (!itemCheck && dto.code) {
          itemCheck = await this.repo.findOne({
            where: {
              code: dto.code,
              id: Not(In([id])),
            },
          });
        }
        if (itemCheck) {
          throw new BadRequestException(ErrorMessage.CATEGORY_ALREADY_EXISTS);
        }
        const itemUpdate = await this.repo.findOne({
          where: {
            id: id,
          },
          relations: ['productCategoryAttributes'],
        });

        if (!itemUpdate)
          throw new NotFoundException('Không tìm thấy danh mục!');
        itemUpdate.name = dto.name;
        itemUpdate.description = dto.description;
        itemUpdate.state = dto.state;
        itemUpdate.color = dto.color;
        itemUpdate.parentId = dto.parentId;
        itemUpdate.image = dto.image;
        itemUpdate.slug = slugify(dto.name);
        itemUpdate.code = dto.code;
        await this.repo.save(this.repo.create(itemUpdate));

        // Xóa những thuộc tính bị xóa
        const productCategoryAttributeDeletedIds =
          itemUpdate.productCategoryAttributes
            ?.filter(
              (att) =>
                !dto.productCategoryAttributes?.find(
                  (_att) => _att.id === att.id,
                ),
            )
            ?.map((att) => att.id);
        if (productCategoryAttributeDeletedIds?.length > 0) {
          await this.categoryAttributeRepo.softDelete({
            id: In(productCategoryAttributeDeletedIds),
          });
          await this.categoryAttributeValueRepo.softDelete({
            productCategoryAttributeId: In(productCategoryAttributeDeletedIds),
          });
        }

        if (dto.productCategoryAttributes?.length > 0) {
          // Cập nhật thuộc tính
          await Promise.all(
            dto.productCategoryAttributes.map(async (attribute) => {
              if (attribute.id) {
                attribute.deletedAt = null;
              } else {
                attribute.id = (
                  await this.categoryAttributeRepo.save(attribute)
                ).id;
                attribute.productCategoryId = itemUpdate.id;
              }
              const attributeUpdate = await this.categoryAttributeRepo.save(
                attribute,
              );
              if (attribute.productCategoryAttributeValues?.length > 0) {
                attribute.productCategoryAttributeValues.map(
                  async (attributeValue) => {
                    attributeValue.productCategoryAttributeId =
                      attributeUpdate.id;
                    if (attributeValue.id) {
                      attributeValue.deletedAt = null;
                    }
                  },
                );
                await this.categoryAttributeValueRepo.save(
                  attribute.productCategoryAttributeValues,
                );
                // await manager.save<ProductCategoryAttributeValueEntity>(attribute.productCategoryAttributeValues);
              }
            }),
          );
          return await this.repo.findOne({
            where: { id: id },
            relations: [
              'productCategoryAttributes',
              'productCategoryAttributes.productCategoryAttributeValues',
            ],
          });
        }
      },
    );
    return productCategoryUpdate;

    // const allowedUpdateFields: Array<keyof CreateProductCategoryDto> = [
    //   "name",
    //   "code",
    //   "description"
    // ];
    //
    // const updateParams = Object.keys(dto).reduce(
    //   (params, key: keyof CreateProductCategoryDto) => {
    //     if (dto[key] && allowedUpdateFields.includes(key)) {
    //       params[key] = dto[key];
    //     }
    //
    //     return params;
    //   },
    //   {}
    // );
    //
    // const { affected } = await this.repo.update({ id }, updateParams);
    //
    // if (!affected) {
    //   throw new InternalServerErrorException(ErrorMessage.UPDATE);
    // }
    //
    // return this.repo.findOne({ id });
  }

  /* Đối tác thêm Giá trị thuộc tính*/
  async merchantAddAttributeValue(
    dto: ProductCategoryAttributeValueEntity,
    user: UserEntity,
    manager: EntityManager,
  ): Promise<number[]> {
    if (!user.merchantId) {
      throw new NotFoundException('Không tìm thấy đối tác');
    }
    const attributeValue = await manager.save(
      ProductCategoryAttributeValueEntity,
      dto,
    );

    return [attributeValue.id];
  }
}

import { ApiProperty } from "@nestjs/swagger";
import { ProductCategoryEntity } from "entities/product-category.entity";
import { ProductCategoryAttributeEntity } from "../../../entities/product-category-attribute.entity";
import { ProductCategoryAttributeValueEntity } from "../../../entities/product-category-attribute-value.entity";

export class CreateProductCategoryDto extends ProductCategoryEntity {

}

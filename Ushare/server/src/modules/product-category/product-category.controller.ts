import {
  CrudController,
  CrudRequest,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { Param, Patch, Post } from '@nestjs/common';
import { ApiBody, ApiResponse } from '@nestjs/swagger';
import { ProductCategoryEntity } from 'entities/product-category.entity';
import { Crud } from 'decorators/crud.decorator';
import { Auth } from 'decorators/auth.decorator';
import { ProductCategoryService } from './product-category.service';
import { CreateProductCategoryDto } from './dto/create-product-category.dto';

@Crud({
  controller: 'product-categories',
  name: 'Danh mục sản phẩm',
  model: {
    type: ProductCategoryEntity,
  },
  routes: {
    include: ['deleteOneBase'],
    updateOneBase: {
      decorators: [Auth()],
    },
    deleteOneBase: {
      decorators: [Auth()],
    },
    createManyBase: {
      decorators: [Auth()],
    },
    createOneBase: {
      decorators: [Auth()],
    },
  },
  query: {
    join: {
      parent: {},
      productCategoryAttributes: {},
      'productCategoryAttributes.productCategoryAttributeValues': {},
      childrens: {},
    },
  },
})
export class ProductCategoryController
  implements CrudController<ProductCategoryEntity>
{
  constructor(public readonly service: ProductCategoryService) {}

  get base(): CrudController<ProductCategoryEntity> {
    return this;
  }

  @Post()
  @Override('createOneBase')
  @ApiResponse({ type: ProductCategoryEntity })
  @ApiBody({ type: CreateProductCategoryDto })
  createOne(
    @ParsedBody() dto: CreateProductCategoryDto,
  ): Promise<ProductCategoryEntity> {
    dto.productCategoryAttributes?.map((attribute) => {
      const unitArray = attribute.units?.map((unit) => {
        if (typeof unit !== 'string') return unit.name;
        return unit;
      });
      attribute.units = unitArray;
    });
    return this.service.create(dto);
  }

  @Patch(':id')
  @Override('updateOneBase')
  @ApiResponse({ type: ProductCategoryEntity })
  @ApiBody({ type: CreateProductCategoryDto })
  updateOne(
    @Param('id') id: number,
    @ParsedBody() dto: CreateProductCategoryDto,
  ): Promise<ProductCategoryEntity> {
    dto.productCategoryAttributes?.map((attribute) => {
      const unitArray = attribute.units?.map((unit) => {
        if (typeof unit !== 'string') return unit.name;
        return unit;
      });
      attribute.units = unitArray;
    });
    return this.service.update(id, dto);
  }

  @Override()
  async getOne(@ParsedRequest() req: CrudRequest) {
    req.parsed.join.push({
      field: 'productCategoryAttributes',
    });
    req.parsed.join.push({
      field: 'productCategoryAttributes.productCategoryAttributeValues',
    });
    return await this.base.getOneBase(req);
  }

  @Override()
  async deleteOne(@Param('id') id: number) {
    return await this.service.repo.softDelete({
      id: id,
    });
  }
}

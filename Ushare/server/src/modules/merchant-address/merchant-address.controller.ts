import { Get, Param, Patch, Post } from '@nestjs/common';
import { ApiBody, ApiResponse } from '@nestjs/swagger';
import {
  CrudController,
  CrudRequest,
  GetManyDefaultResponse,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { Auth } from 'decorators/auth.decorator';
import { Crud } from 'decorators/crud.decorator';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { CreateMerchantAddressDto } from './dto/create-merchant-address.dto';
import { CustomManyResponseDto } from './dto/custom-many.response.dto';
import { MerchantAddressService } from './merchant-address.service';
import { User } from 'decorators/user.decorator';
import { UserEntity } from 'entities/user.entity';

@Crud({
  controller: 'merchant-addresses',
  name: 'Kho hàng',
  model: {
    type: MerchantAddressEntity,
  },
  query: {
    join: {
      ward: {},
      merchant: {},
      'ward.district': {
        alias: 'district',
      },
      'ward.district.province': {},
    },
  },
})
export class MerchantAddressController
  implements CrudController<MerchantAddressEntity>
{
  constructor(public readonly service: MerchantAddressService) {}

  get base(): CrudController<MerchantAddressEntity> {
    return this;
  }

  @Override('createOneBase')
  @Auth()
  @Post()
  @ApiResponse({ type: MerchantAddressEntity })
  @ApiBody({ type: CreateMerchantAddressDto })
  async createOne(
    @ParsedBody() dto: CreateMerchantAddressDto,
    @User() user: UserEntity,
  ): Promise<MerchantAddressEntity> {
    return this.service.create(dto, user);
  }

  @Override('updateOneBase')
  @Auth()
  @Patch(':id')
  @ApiResponse({ type: MerchantAddressEntity })
  @ApiBody({ type: CreateMerchantAddressDto })
  async updateOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: CreateMerchantAddressDto,
    @Param('id') id: number,
    @User() user: UserEntity,
  ): Promise<MerchantAddressEntity> {
    return await this.service.update(id, dto, user);
  }
}

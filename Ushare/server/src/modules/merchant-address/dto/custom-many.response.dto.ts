import { ApiProperty } from '@nestjsx/crud/lib/crud';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';

export class CustomManyResponseDto extends MerchantAddressEntity {
  @ApiProperty({ description: 'Số lượng' })
  amount: number;

  @ApiProperty({ description: 'Tổng' })
  total: number;
}

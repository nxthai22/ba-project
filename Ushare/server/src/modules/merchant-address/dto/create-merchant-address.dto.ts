import { PickType } from '@nestjs/swagger';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';

export class CreateMerchantAddressDto extends PickType(MerchantAddressEntity, [
  'name',
  'code',
  'tel',
  'address',
  'status',
  'wardId',
]) {}

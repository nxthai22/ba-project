import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Connection, In, Not, Repository } from 'typeorm';
import { ErrorMessage, ShippingPartner } from 'enums';
import { ProductEntity } from 'entities/product.entity';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { CreateMerchantAddressDto } from './dto/create-merchant-address.dto';
import { ShippingService } from '../shipping/shipping.service';
import { UserEntity } from 'entities/user.entity';

@Injectable()
export class MerchantAddressService extends TypeOrmCrudService<MerchantAddressEntity> {
  constructor(
    @InjectRepository(MerchantAddressEntity)
    public repo: Repository<MerchantAddressEntity>,
    @InjectRepository(ProductEntity)
    public productRepo: Repository<ProductEntity>,
    public shippingService: ShippingService,
  ) {
    super(repo);
  }

  async create(
    dto: CreateMerchantAddressDto,
    user: UserEntity,
  ): Promise<MerchantAddressEntity> {
    // await this.checkForExistence(dto, user);
    let itemCheck = await this.repo.findOne({
      where: {
        merchantId: user.merchantId,
        name: dto.name,
      },
    });
    if (!itemCheck && dto.code) {
      itemCheck = await this.repo.findOne({
        where: {
          merchantId: user.merchantId,
          code: dto.code,
        },
      });
    }
    if (itemCheck) {
      throw new BadRequestException(ErrorMessage.WAREHOUSE_ALREADY_EXISTS);
    }
    const result = await this.repo.save({
      ...dto,
      merchantId: user.merchantId,
    });
    const newMerchantAddress = await this.repo.findOne({
      where: {
        id: result.id,
      },
      relations: ['ward', 'ward.district', 'ward.district.province'],
    });
    await Promise.all(
      Object.keys(ShippingPartner).map(async (partner) => {
        const shopId = await this.shippingService.createStore(
          newMerchantAddress,
          ShippingPartner[partner],
        );
        switch (partner) {
          case 'VIETTELPOST':
            newMerchantAddress.vtpId = shopId;
            if (!shopId) {
              throw new BadRequestException(
                'Không tạo được kho hàng sang đối tác vận chuyển Viettel post',
              );
            }
            break;
        }
      }),
    );
    return await this.repo.save(newMerchantAddress);
  }

  async update(
    id: number,
    dto: CreateMerchantAddressDto,
    user: UserEntity,
  ): Promise<MerchantAddressEntity> {
    let itemCheck = await this.repo.findOne({
      where: {
        merchantId: user.merchantId,
        name: dto.name,
        id: Not(In([id])),
      },
    });
    if (!itemCheck && dto.code) {
      itemCheck = await this.repo.findOne({
        where: {
          merchantId: user.merchantId,
          code: dto.code,
          id: Not(In([id])),
        },
      });
    }
    if (itemCheck) {
      throw new BadRequestException(ErrorMessage.WAREHOUSE_ALREADY_EXISTS);
    }
    const itemUpdate = await this.repo.findOne({
      where: {
        id: id,
      },
      relations: ['ward', 'ward.district', 'ward.district.province'],
    });
    itemUpdate.name = dto.name;
    itemUpdate.code = dto.code;
    itemUpdate.tel = dto.tel;
    itemUpdate.address = dto.address;
    itemUpdate.status = dto.status;
    if (itemUpdate.wardId !== dto.wardId) {
      itemUpdate.wardId = dto.wardId;
      await Promise.all(
        Object.keys(ShippingPartner).map(async (partner) => {
          const shopId = await this.shippingService.createStore(
            itemUpdate,
            ShippingPartner[partner],
          );
          switch (partner) {
            case 'VIETTELPOST':
              itemUpdate.vtpId = shopId;
              if (!shopId) {
                throw new BadRequestException(
                  'Không tạo được kho hàng sang đối tác vận chuyển Viettel post',
                );
              }
              break;
          }
        }),
      );
    }
    delete itemUpdate.ward;
    return await this.repo.save(itemUpdate);
  }

  async checkForExistence(dto: CreateMerchantAddressDto, user: UserEntity) {
    const { name, code } = dto;

    const merchant = await this.repo.findOne({
      where: [
        { merchantId: user.merchantId, name },
        { merchantId: user.merchantId, code },
      ],
    });

    if (merchant) {
      throw new BadRequestException(ErrorMessage.WAREHOUSE_ALREADY_EXISTS);
    }
  }
}

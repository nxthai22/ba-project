import { Test, TestingModule } from '@nestjs/testing';
import { MerchantAddressService } from './merchant-address.service';

describe('MerchantAddressService', () => {
  let service: MerchantAddressService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MerchantAddressService],
    }).compile();

    service = module.get<MerchantAddressService>(MerchantAddressService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

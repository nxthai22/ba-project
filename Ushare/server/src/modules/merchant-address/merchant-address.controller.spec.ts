import { Test, TestingModule } from '@nestjs/testing';
import { MerchantAddressController } from './merchant-address.controller';

describe('MerchantAddressController', () => {
  let controller: MerchantAddressController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MerchantAddressController],
    }).compile();

    controller = module.get<MerchantAddressController>(MerchantAddressController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { MerchantAddressEntity } from "entities/merchant-address.entity";
import { ProductEntity } from "entities/product.entity";
import { MerchantAddressController } from "./merchant-address.controller";
import { MerchantAddressService } from "./merchant-address.service";
import { ShippingModule } from "../shipping/shipping.module";

@Module({
  imports: [
    TypeOrmModule.forFeature(
      [MerchantAddressEntity, ProductEntity]),
    ShippingModule],
  controllers: [MerchantAddressController],
  providers: [MerchantAddressService],
  exports: [MerchantAddressService]
})
export class MerchantAddressModule {
}

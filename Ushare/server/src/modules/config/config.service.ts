import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { addMonths, endOfMonth, startOfMonth } from 'date-fns';
import { OrderStatus } from 'enums';
import {
  Between,
  EntityManager,
  In,
  IsNull,
  LessThan,
  LessThanOrEqual,
  MoreThanOrEqual,
  Repository,
  Not,
  QueryRunner,
} from 'typeorm';
import {
  ConfigEntity,
  ConfigType,
  AmountCollaboratorCommission,
} from 'entities/config.entity';

import { OrderEntity } from 'entities/order.entity';
import { UserEntity } from 'entities/user.entity';
import { ConfigDto } from './dto/config.dto';

@Injectable()
export class ConfigService extends TypeOrmCrudService<ConfigEntity> {
  constructor(
    @InjectRepository(ConfigEntity) public repo: Repository<ConfigEntity>,
    @InjectRepository(UserEntity)
    public userRepo: Repository<UserEntity>,
    @InjectRepository(OrderEntity)
    public orderRepo: Repository<OrderEntity>,
  ) {
    super(repo);
  }

  async create(dto: ConfigDto): Promise<ConfigEntity> {
    const newConfig = new ConfigEntity();
    newConfig.updatedAt = new Date();

    Object.keys(dto).forEach((property) => {
      if (dto[property]) newConfig[property] = dto[property];
    });

    switch (dto.key) {
      case ConfigType.FIRSTORDER:
      case ConfigType.REGISTER:
        newConfig.value = dto.referralCommission;
        break;
      case ConfigType.AMOUNT_COLLABORATOR:
        newConfig.value = dto.amountCollaboratorCommission;
        break;
      case ConfigType.REVENUE:
      case ConfigType.REVENUE_QUARTER:
        newConfig.value = dto.merchantCommission;
        break;
      default:
        if (dto.value) newConfig.value = dto.value;
        break;
    }

    const createdConfig = await this.repo.save(newConfig);

    return createdConfig;
  }

  async update(id: number, dto: ConfigDto): Promise<ConfigEntity> {
    const config = await this.repo.findOne({ id });

    if (!config) throw new NotFoundException('Không tìm thấy config');

    Object.keys(dto).forEach((property) => {
      config[property] = dto[property];
    });

    return this.repo.save(config);
  }

  /** Cập nhật thời gian hưởng hoa hồng cho người giới thiệu */
  async updateRangetimeReferralCommission(
    order: OrderEntity,
    queryRunner?: QueryRunner,
  ): Promise<void> {
    const user = await this.userRepo.findOne({
      where: {
        id: order.userId,
      },
    });
    console.log(!user?.referralParentId);
    if (!user?.referralParentId || order.status !== OrderStatus.COMPLETED) {
      return;
    }

    const firstOrders = await this.orderRepo.find({
      where: {
        id: Not(order.id),
        configCommissionId: Not(IsNull()),
        status: order.status,
        userId: user.id,
        createdAt: LessThan(new Date()),
      },
    });
    if (firstOrders.length) return;

    // Danh sách cấu hình thưởng, hoa hồng của UShare
    const commissionConfigs = await this.repo.find({
      where: {
        merchantId: IsNull(),
        key: In([
          ConfigType.FIRSTORDER,
          ConfigType.REGISTER,
          ConfigType.AMOUNT_COLLABORATOR,
        ]),
        startDate: LessThanOrEqual(new Date()),
        endDate: MoreThanOrEqual(new Date()),
      },
    });
    if (!commissionConfigs.length) return;

    const firstOrderConfig = commissionConfigs?.find(
      ({ key }) => key === ConfigType.FIRSTORDER,
    );
    if (firstOrderConfig) {
      if (queryRunner) {
        const orderUpdate = await queryRunner.manager.findOne(OrderEntity, {
          id: order.id,
        });
        await queryRunner.manager.save(OrderEntity, {
          ...orderUpdate,
          configCommissionId: firstOrderConfig.id,
        });
      } else {
        await this.orderRepo.update(
          { id: order.id },
          { configCommissionId: firstOrderConfig.id },
        );
      }
    }

    // Danh sách CTV của người giới thiệu trong tháng
    const referralUsers = await this.userRepo.find({
      where: {
        referralParentId: user.referralParentId,
        referralBonusStart: Between(
          startOfMonth(new Date()),
          endOfMonth(new Date()),
        ),
      },
    });

    const amountCollaboratorConfig = commissionConfigs?.find(
      ({ key }) => key === ConfigType.AMOUNT_COLLABORATOR,
    );
    if (
      amountCollaboratorConfig &&
      amountCollaboratorConfig.value &&
      Array.isArray(amountCollaboratorConfig.value)
    ) {
      const amountCollCommissionConfigs =
        amountCollaboratorConfig.value as AmountCollaboratorCommission[];

      const userConfig = amountCollCommissionConfigs?.find(({ min, max }) => {
        return (
          min <= (referralUsers.length || 1) &&
          max >= (referralUsers.length || 1)
        );
      });
      user.referralBonusStart = startOfMonth(new Date());

      if (userConfig) {
        user.referralBonusExpire = addMonths(
          user.referralBonusStart,
          userConfig.rangeTime,
        );
        user.valueBonus = userConfig?.value;
        user.valueBonusType = userConfig?.valueType;
      }
    }

    if (queryRunner) {
      await queryRunner.manager.save(UserEntity, user);
      return;
    }

    await this.userRepo.save(user);

    return;
  }
}

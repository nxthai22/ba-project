import { BadRequestException, Body, Param, Patch, Post } from '@nestjs/common';
import { ApiBody, ApiOperation } from '@nestjs/swagger';
import { CrudController, Override } from '@nestjsx/crud';
import { Auth } from 'decorators/auth.decorator';
import { Crud } from 'decorators/crud.decorator';
import { ConfigEntity, ConfigType } from 'entities/config.entity';
import { ConfigService } from './config.service';
import { ConfigDto } from './dto/config.dto';

@Crud({
  controller: 'config',
  name: 'Cấu hình',
  model: {
    type: ConfigEntity,
  },
})
export class ConfigController implements CrudController<ConfigEntity> {
  constructor(public readonly service: ConfigService) {}

  @Post()
  @Auth()
  @Override('createOneBase')
  @ApiOperation({ summary: 'Tạo cấu hình' })
  @ApiBody({ type: ConfigDto })
  createOne(@Body() dto: ConfigDto): Promise<ConfigEntity> {
    if (!Object.keys(dto).length) {
      throw new BadRequestException(
        'Configuration phải chứa ít nhất một thuộc tính',
      );
    }

    if (dto.key && !Object.values(ConfigType).includes(dto.key)) {
      throw new BadRequestException(
        '{key} không hợp lệ cho enum config_key_enum',
      );
    }

    return this.service.create(dto);
  }

  @Patch(':id')
  @Auth()
  @Override('updateOneBase')
  @ApiOperation({ summary: 'Sửa cấu hình' })
  @ApiBody({ type: ConfigDto })
  async updateOne(
    @Param('id') id: number,
    @Body() dto: ConfigDto,
  ): Promise<ConfigEntity> {
    return this.service.update(id, dto);
  }
}

import { ApiProperty } from '@nestjs/swagger';

export type ClassType<T = any> = new (...args: any[]) => T;

export function PaginatedResponseDto<T extends ClassType>(ResourceCls: T) {
  class Paginated {
    @ApiProperty()
    public count: number;

    @ApiProperty()
    public page: number;

    @ApiProperty()
    public pageCount: number;

    @ApiProperty()
    public total: number;

    @ApiProperty({ type: [ResourceCls] })
    public data: T[];
  }

  return Paginated;
}

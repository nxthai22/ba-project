export class PatienceDiff {
  private aLines;
  private bLines;
  private diffPlusFlag;
  constructor(_aLines, _bLines, _diffPlusFlag) {
    this.aLines = _aLines;
    this.bLines = _bLines;
    this.diffPlusFlag = _diffPlusFlag;
  }
  //
  // findUnique finds all unique values in arr[lo..hi], inclusive.  This
  // function is used in preparation for determining the longest common
  // subsequence.  Specifically, it first reduces the array range in question
  // to unique values.
  //
  // Returns an ordered Map, with the arr[i] value as the Map key and the
  // array index i as the Map value.
  //
  findUnique(arr, lo, hi) {
    var lineMap = new Map();

    for (let i = lo; i <= hi; i++) {
      let line = arr[i];
      if (lineMap.has(line)) {
        lineMap.get(line).count++;
        lineMap.get(line).index = i;
      } else {
        lineMap.set(line, { count: 1, index: i });
      }
    }

    lineMap.forEach((val, key, map) => {
      if (val.count !== 1) {
        map.delete(key);
      } else {
        map.set(key, val.index);
      }
    });

    return lineMap;
  }

  //
  // uniqueCommon finds all the unique common entries between aArray[aLo..aHi]
  // and bArray[bLo..bHi], inclusive.  This function uses findUnique to pare
  // down the aArray and bArray ranges first, before then walking the comparison
  // between the two arrays.
  //
  // Returns an ordered Map, with the Map key as the common line between aArray
  // and bArray, with the Map value as an object containing the array indexes of
  // the matching unique lines.
  //
  uniqueCommon(aArray, aLo, aHi, bArray, bLo, bHi) {
    let ma = this.findUnique(aArray, aLo, aHi);
    let mb = this.findUnique(bArray, bLo, bHi);

    ma.forEach((val, key, map) => {
      if (mb.has(key)) {
        map.set(key, { indexA: val, indexB: mb.get(key) });
      } else {
        map.delete(key);
      }
    });

    return ma;
  }

  //
  // longestCommonSubsequence takes an ordered Map from the function uniqueCommon
  // and determines the Longest Common Subsequence (LCS).
  //
  // Returns an ordered array of objects containing the array indexes of the
  // matching lines for a LCS.
  //
  longestCommonSubsequence(abMap) {
    var ja = [];

    // First, walk the list creating the jagged array.
    abMap.forEach((val, key, map) => {
      let i = 0;
      while (ja[i] && ja[i][ja[i].length - 1].indexB < val.indexB) {
        i++;
      }

      if (!ja[i]) {
        ja[i] = [];
      }

      if (0 < i) {
        val.prev = ja[i - 1][ja[i - 1].length - 1];
      }

      ja[i].push(val);
    });

    // Now, pull out the longest common subsequence.
    var lcs = [];
    if (0 < ja.length) {
      let n = ja.length - 1;
      var lcs = [ja[n][ja[n].length - 1]];
      while (lcs[lcs.length - 1].prev) {
        lcs.push(lcs[lcs.length - 1].prev);
      }
    }

    return lcs.reverse();
  }

  // "result" is the array used to accumulate the aLines that are deleted, the
  // lines that are shared between aLines and bLines, and the bLines that were
  // inserted.
  private result = [];
  private deleted = 0;
  private inserted = 0;

  // aMove and bMove will contain the lines that don't match, and will be returned
  // for possible searching of lines that moved.

  private aMove = [];
  private aMoveIndex = [];
  private bMove = [];
  private bMoveIndex = [];
  private uniqueCommonMap;
  //
  // addToResult simply pushes the latest value onto the "result" array.  This
  // array captures the diff of the line, aIndex, and bIndex from the aLines
  // and bLines array.
  //
  addToResult(aIndex, bIndex) {
    if (bIndex < 0) {
      this.aMove.push(this.aLines[aIndex]);
      this.aMoveIndex.push(this.result.length);
      this.deleted++;
    } else if (aIndex < 0) {
      this.bMove.push(this.bLines[bIndex]);
      this.bMoveIndex.push(this.result.length);
      this.inserted++;
    }

    this.result.push({
      line: 0 <= aIndex ? this.aLines[aIndex] : this.bLines[bIndex],
      aIndex: aIndex,
      bIndex: bIndex,
    });
  }

  //
  // addSubMatch handles the lines between a pair of entries in the LCS.  Thus,
  // this function might recursively call recurseLCS to further match the lines
  // between aLines and bLines.
  //
  addSubMatch(aLo, aHi, bLo, bHi) {
    // Match any lines at the beginning of aLines and bLines.
    while (aLo <= aHi && bLo <= bHi && this.aLines[aLo] === this.bLines[bLo]) {
      this.addToResult(aLo++, bLo++);
    }

    // Match any lines at the end of aLines and bLines, but don't place them
    // in the "result" array just yet, as the lines between these matches at
    // the beginning and the end need to be analyzed first.
    let aHiTemp = aHi;
    while (aLo <= aHi && bLo <= bHi && this.aLines[aHi] === this.bLines[bHi]) {
      aHi--;
      bHi--;
    }

    // Now, check to determine with the remaining lines in the subsequence
    // whether there are any unique common lines between aLines and bLines.
    //
    // If not, add the subsequence to the result (all aLines having been
    // deleted, and all bLines having been inserted).
    //
    // If there are unique common lines between aLines and bLines, then let's
    // recursively perform the patience diff on the subsequence.
    this.uniqueCommonMap = this.uniqueCommon(
      this.aLines,
      aLo,
      aHi,
      this.bLines,
      bLo,
      bHi,
    );
    if (this.uniqueCommonMap.size === 0) {
      while (aLo <= aHi) {
        this.addToResult(aLo++, -1);
      }
      while (bLo <= bHi) {
        this.addToResult(-1, bLo++);
      }
    } else {
      this.recurseLCS(aLo, aHi, bLo, bHi, this.uniqueCommonMap);
    }

    // Finally, let's add the matches at the end to the result.
    while (aHi < aHiTemp) {
      this.addToResult(++aHi, ++bHi);
    }
  }

  //
  // recurseLCS finds the longest common subsequence (LCS) between the arrays
  // aLines[aLo..aHi] and bLines[bLo..bHi] inclusive.  Then for each subsequence
  // recursively performs another LCS search (via addSubMatch), until there are
  // none found, at which point the subsequence is dumped to the result.
  //
  recurseLCS(aLo, aHi, bLo, bHi, uniqueCommonMap) {
    var x = this.longestCommonSubsequence(
      uniqueCommonMap ||
        this.uniqueCommon(this.aLines, aLo, aHi, this.bLines, bLo, bHi),
    );
    if (x.length === 0) {
      this.addSubMatch(aLo, aHi, bLo, bHi);
    } else {
      if (aLo < x[0].indexA || bLo < x[0].indexB) {
        this.addSubMatch(aLo, x[0].indexA - 1, bLo, x[0].indexB - 1);
      }

      let i;
      for (i = 0; i < x.length - 1; i++) {
        this.addSubMatch(
          x[i].indexA,
          x[i + 1].indexA - 1,
          x[i].indexB,
          x[i + 1].indexB - 1,
        );
      }

      if (x[i].indexA <= aHi || x[i].indexB <= bHi) {
        this.addSubMatch(x[i].indexA, aHi, x[i].indexB, bHi);
      }
    }
  }

  getPatienceDiff() {
    this.recurseLCS(
      0,
      this.aLines.length - 1,
      0,
      this.bLines.length - 1,
      this.uniqueCommonMap,
    );

    if (this.diffPlusFlag) {
      return {
        lines: this.result,
        lineCountDeleted: this.deleted,
        lineCountInserted: this.inserted,
        lineCountMoved: 0,
        aMove: this.aMove,
        aMoveIndex: this.aMoveIndex,
        bMove: this.bMove,
        bMoveIndex: this.bMoveIndex,
      };
    }
  }
}
export class PatienceDiffPlus {
  public static getPatienceDiffPlus(str1: string[], str2: string[]) {
    const patienceDiff = new PatienceDiff(str1, str2, true);
    const difference = patienceDiff.getPatienceDiff();
    let aMoveNext = difference.aMove;
    let aMoveIndexNext = difference.aMoveIndex;
    let bMoveNext = difference.bMove;
    let bMoveIndexNext = difference.bMoveIndex;

    do {
      // const aMove = aMoveNext;
      // const aMoveIndex = aMoveIndexNext;
      // const bMove = bMoveNext;
      // const bMoveIndex = bMoveIndexNext;
      const aMove = difference.aMove;
      const aMoveIndex = difference.aMoveIndex;
      const bMove = difference.bMove;
      const bMoveIndex = difference.bMoveIndex;

      delete difference.aMove;
      delete difference.aMoveIndex;
      delete difference.bMove;
      delete difference.bMoveIndex;

      aMoveNext = [];
      aMoveIndexNext = [];
      bMoveNext = [];
      bMoveIndexNext = [];

      const subPatienceDiff = new PatienceDiff(aMove, bMove, true);
      let subDiff = subPatienceDiff.getPatienceDiff();
      var lastLineCountMoved = difference.lineCountMoved;

      subDiff?.lines?.forEach((v, i) => {
        if (0 <= v.aIndex && 0 <= v.bIndex) {
          difference.lines[aMoveIndex[v.aIndex]].moved = true;
          difference.lines[bMoveIndex[v.bIndex]].aIndex = aMoveIndex[v.aIndex];
          difference.lines[bMoveIndex[v.bIndex]].moved = true;
          difference.lineCountInserted--;
          difference.lineCountDeleted--;
          difference.lineCountMoved++;
        } else if (v.bIndex < 0) {
          aMoveNext.push(aMove[v.aIndex]);
          aMoveIndexNext.push(aMoveIndex[v.aIndex]);
        } else {
          bMoveNext.push(bMove[v.bIndex]);
          bMoveIndexNext.push(bMoveIndex[v.bIndex]);
        }
      });
    } while (0 < difference.lineCountMoved - lastLineCountMoved);

    return difference;
  }

  public static calculateDiffPercent(
    str1: string,
    str2: string,
    character: string,
  ) {
    const diff = this.getPatienceDiffPlus(
      str1.split(character),
      str2.split(character),
    );
    const numeratorTemp =
      (diff.lines?.length || 0) -
      (diff.lineCountMoved || 0) -
      (diff.lineCountDeleted || 0) -
      (diff.lineCountInserted || 0);
    const numerator = numeratorTemp < 0 ? 0 : numeratorTemp;
    const denominator =
      diff.lines?.length - diff.lineCountMoved <= 0
        ? 1
        : diff.lines?.length - diff.lineCountMoved;
    return numerator / denominator;
  }
}

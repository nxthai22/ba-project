import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

/** Dữ liệu trả về phân trang */
export class SearchResult {
  @ApiProperty({
    description: 'Trang',
    default: 1,
  })
  page: number;

  @ApiProperty({
    description: 'Số bản ghi trên 1 trang',
  })
  limit: number;

  @ApiProperty({
    description: 'Tổng số bản ghi',
  })
  totalCount: number;

  @ApiProperty({
    description: 'Tổng số trang',
  })
  totalPage: number;

  @ApiProperty({
    description: 'Dữ liệu trả về',
  })
  data: any;
}

/** Tìm kiếm phân trang */
export class SearchCriteria {
  @IsNotEmpty()
  @ApiProperty({ description: 'Trang', required: true, default: 1 })
  page: number;

  @IsNotEmpty()
  @ApiProperty({
    description: 'Số bản ghi trên 1 trang',
    required: true,
    default: 10,
  })
  limit: number;

  @ApiProperty({ description: 'Ký tự tìm kiếm', required: false })
  textSearch: string;
}

/** Phân trang */
export class PagingHelper {
  public static getPages(obj: object[], search: SearchCriteria): SearchResult {
    const startNumber = (search.page - 1) * search.limit;
    const endNumber =
      search.page * search.limit > obj.length
        ? obj.length - (search.page - 1) * search.limit
        : search.page * search.limit;
    const data =
      obj.length > search.limit ? obj.slice(startNumber, endNumber) : obj;
    const totalPage =
      Number(obj.length) % Number(search.limit) === 0
        ? Number(obj.length) / Number(search.limit)
        : Math.trunc(Number(obj.length) / Number(search.limit)) + 1;
    const result: SearchResult = {
      page: search.page,
      limit: search.limit,
      totalPage: totalPage,
      totalCount: obj.length,
      data: data,
    };
    return result;
  }
}

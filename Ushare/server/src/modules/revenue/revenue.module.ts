import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigEntity } from 'entities/config.entity';
import { OrderProductEntity } from 'entities/order-product.entity';
import { OrderRevenueEntity } from 'entities/order-revenue.entity';
import { OrderEntity } from 'entities/order.entity';
import { ProductEntity } from 'entities/product.entity';
import { RevenueStatisticEntity } from 'entities/revenue-statistic.entity';
import { RoleEntity } from 'entities/role.entity';
import { UserEntity } from 'entities/user.entity';
import { RevenueController } from './revenue.controller';
import { RevenueService } from './revenue.service';
import { UserModule } from '../user/user.module';
import { ProductVariantEntity } from '../../entities/product-variant.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      OrderEntity,
      ProductEntity,
      ProductVariantEntity,
      OrderProductEntity,
      UserEntity,
      ConfigEntity,
      OrderRevenueEntity,
      RevenueStatisticEntity,
      RoleEntity,
    ]),
    UserModule,
  ],
  controllers: [RevenueController],
  providers: [RevenueService],
  exports: [RevenueService],
})
export class RevenueModule {}

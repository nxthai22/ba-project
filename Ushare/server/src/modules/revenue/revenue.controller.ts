import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import {
  ApiBody,
  ApiOkResponse,
  ApiOperation,
  ApiProperty,
  ApiTags,
} from '@nestjs/swagger';
import { Auth } from 'decorators/auth.decorator';
import { User } from 'decorators/user.decorator';
import { RevenueStatisticEntity } from 'entities/revenue-statistic.entity';
import { UserEntity } from 'entities/user.entity';
import { FilterRevenueWithPaging } from './dto/request-fillter.dto';
import {
  RevenueResponse,
  TopUserCommissionResponse,
} from './dto/revenue-response.dto';
import {
  DetailRefrralStatistic,
  RevenueReferralStatistic,
  RevenueStatisticDetail,
  RevenueStatisticResponse,
} from './dto/revenue-statistic-response.dto';
import { RevenueService } from './revenue.service';
import { GetManyDefaultResponse } from '@nestjsx/crud/lib/interfaces/get-many-default-response.interface';
import {
  RevenueStatisticByUserDto,
  RevenueStatisticByUserResponse,
} from './dto/revenue-statistic.dto';
import { ApiImplicitQuery } from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';
import { RoleType } from '../../entities/role.entity';

export class RevenueReferralStatisticResponse
  implements GetManyDefaultResponse<RevenueReferralStatistic>
{
  @ApiProperty({
    description: 'count',
  })
  count: number;
  @ApiProperty({
    description: 'data',
    type: [RevenueReferralStatistic],
  })
  data: RevenueReferralStatistic[];
  @ApiProperty({
    description: 'page',
  })
  page: number;
  @ApiProperty({
    description: 'pageCount',
  })
  pageCount: number;
  @ApiProperty({
    description: 'total',
  })
  total: number;
}

export class RevenueStatisticResponses
  implements GetManyDefaultResponse<RevenueStatisticResponse>
{
  @ApiProperty({
    description: 'count',
  })
  count: number;
  @ApiProperty({
    description: 'data',
    type: [RevenueStatisticResponse],
  })
  data: RevenueStatisticResponse[];
  @ApiProperty({
    description: 'page',
  })
  page: number;
  @ApiProperty({
    description: 'pageCount',
  })
  pageCount: number;
  @ApiProperty({
    description: 'total',
  })
  total: number;
}

@ApiTags('Revenue')
@Controller('revenue')
export class RevenueController {
  constructor(private service: RevenueService) {}

  @Auth()
  @Post('/summary')
  @ApiOperation({
    summary: 'Hoa hồng',
  })
  @ApiBody({
    description: 'Thông tin tìm kiếm Hoa hồng',
    type: FilterRevenueWithPaging,
  })
  @ApiOkResponse({
    type: RevenueResponse,
  })
  async calculateRevenue(
    @Body() dto: FilterRevenueWithPaging,
    @User() user: UserEntity,
  ): Promise<any> {
    return await this.service.calculateRevenueNew(dto, user);
  }

  @Auth()
  @Post('/revenue-month-report')
  @ApiOperation({
    summary: 'Hoa hồng theo khoảng ngày',
  })
  @ApiOkResponse({
    type: [RevenueStatisticEntity],
  })
  async caculateRevenueStatistic(
    @Body() dto: FilterRevenueWithPaging,
    @User() user: UserEntity,
  ): Promise<any> {
    return await this.service.caculateRevenueStatistic(dto, user);
  }

  @Auth()
  @Get('/revenue-statistic-detail-report')
  @ApiOperation({
    summary: 'Doanh thu CTV',
  })
  @ApiOkResponse({
    type: RevenueStatisticResponses,
  })
  async calculateRevenueStatisticDetail(
    @Query() dto: FilterRevenueWithPaging,
    @User() user: UserEntity,
  ): Promise<GetManyDefaultResponse<RevenueStatisticDetail>> {
    return await this.service.calculateRevenueStatisticDetail(dto, user);
  }

  @Auth()
  @Get('/referral-commission-report')
  @ApiOperation({
    summary: 'Hoa hồng thưởng giới thiệu',
  })
  @ApiOkResponse({
    type: RevenueReferralStatisticResponse,
  })
  async calculateReferralCommissionReport(
    @Query() dto: FilterRevenueWithPaging,
    @User() user: UserEntity,
  ): Promise<GetManyDefaultResponse<RevenueReferralStatistic>> {
    return await this.service.calculateReferralCommission(dto, user);
  }

  @Auth()
  @Get('/detail-referral-commission')
  @ApiOperation({
    summary: 'Chi tiết Hoa hồng thưởng giới thiệu',
  })
  @ApiOkResponse({
    type: [DetailRefrralStatistic],
  })
  async detailReferralCommissionReport(
    @Query() dto: FilterRevenueWithPaging,
    @User() user: UserEntity,
  ): Promise<DetailRefrralStatistic[]> {
    return await this.service.detailReferralCommission(dto, user);
  }

  @Post('/top-commission')
  @ApiOperation({
    summary: 'Top Hoa hồng theo sản phẩm + hoa hồng theo doanh thu',
  })
  @ApiOkResponse({
    type: [RevenueStatisticResponse],
  })
  @HttpCode(200)
  async getTopUserCommissionReport(
    @Body() dto: FilterRevenueWithPaging,
  ): Promise<RevenueStatisticResponse[]> {
    return await this.service.getTopUserCommission(dto);
  }

  @Post('/top-commission-new')
  @ApiOperation({
    summary: 'Top Hoa hồng theo sản phẩm + hoa hồng theo doanh thu (Mới)',
  })
  @ApiOkResponse({
    type: TopUserCommissionResponse,
  })
  @HttpCode(200)
  async getTopUserCommissionReportNew(
    @Body() dto: FilterRevenueWithPaging,
  ): Promise<TopUserCommissionResponse> {
    return await this.service.getTopUserCommissionNew(dto);
  }

  @Auth()
  @Get('/commission')
  @ApiOperation({
    summary: 'Thống kê Hoa hồng 7 ngày gần nhất',
  })
  async calculateCommission(
    @User() user: UserEntity,
    @Query() dto: FilterRevenueWithPaging,
  ): Promise<any[]> {
    return await this.service.calculateCommission(dto, user);
  }

  @Auth()
  @Get('/revenueChart')
  @ApiOperation({
    summary: 'Biểu đồ thống kê Hoa hồng',
  })
  async calculateRevenueChart(
    @Query() dto: FilterRevenueWithPaging,
    @User() user: UserEntity,
  ): Promise<any[]> {
    return await this.service.calculateRevenueChart(dto, user);
  }

  @Auth()
  @Post('/top-sale')
  @ApiOperation({
    summary: 'Top 10 sản phẩm bán chạy',
  })
  @ApiBody({
    description: 'Thông tin tìm kiếm top sản phẩm bán chạy',
    type: FilterRevenueWithPaging,
  })
  async getTopProductSale(
    @Body() dto: FilterRevenueWithPaging,
    @User() user: UserEntity,
  ): Promise<any[]> {
    return await this.service.getTopProductSale(dto, user);
  }

  @Auth()
  @Get('/top-product-revenue-rate')
  @ApiOperation({
    summary: 'Tỉ trọng Doanh thu hàng hoá',
  })
  async getTopProductRevenueRate(
    @Query() dto: FilterRevenueWithPaging,
    @User() user: UserEntity,
  ): Promise<any[]> {
    return await this.service.getTopProductRevenueRate(dto, user);
  }

  @Auth()
  @Get('/revenue-by-user')
  @ApiOperation({
    summary: 'Thống kê Hoa hồng theo User',
  })
  @ApiOkResponse({
    type: [RevenueStatisticByUserResponse],
  })
  @HttpCode(200)
  async revenueByUser(
    @Query() query: RevenueStatisticByUserDto,
    @User() user: UserEntity,
  ): Promise<RevenueStatisticByUserResponse[]> {
    return await this.service.getRevenueStatisticByUser(query, user);
  }

  @Auth()
  @Patch('/execute-statistic-revenue')
  @ApiOperation({
    summary: 'Thống kê lại doanh thu theo tháng mong muốn',
  })
  @ApiImplicitQuery({
    name: 'month',
    type: Date,
    required: true,
    description: 'Tháng thống kê',
  })
  async executeRevenueStatistic(
    @Query('month') month: Date,
    @User() user: UserEntity,
  ): Promise<void> {
    if (user.role?.type !== RoleType.ADMIN)
      throw new BadRequestException('Bạn không có quyền thao tác');
    await this.service.executeRevenueStatistic(new Date(month));
  }
}

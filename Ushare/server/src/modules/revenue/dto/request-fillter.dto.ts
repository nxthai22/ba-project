import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';
import { SearchCriteria } from 'modules/base/base-search';

export enum FieldName {
  NAME = 'fullName',
  TEL = 'tel',
  BANKBRANCH = 'bankBranch',
  BANKACCOUNTNAME = 'bankAccountName'
}

export class FilterRevenueWithPaging {
  @ApiProperty({
    description: 'Ký tự tìm kiếm',
    required: false,
  })
  textSearch: string;

  @ApiProperty({
    description: 'Ngày bắt đầu',
    type: Date,
    required: true,
    default: new Date(
      new Date(new Date().setDate(new Date().getDate() - 7)).setHours(0, 0, 0),
    ),
  })
  fromDate: Date;

  @ApiProperty({
    description: 'Ngày kết thúc',
    type: Date,
    required: true,
    default: new Date(new Date().setHours(0, 0, 0)),
  })
  toDate: Date;

  @ApiProperty({
    description: 'Id đối tác',
    required: false,
  })
  merchantId?: number;

  @ApiProperty({
    description: 'Trường',
    required: false,
    enum: FieldName,
  })
  field?: FieldName;

  @IsOptional()
  @ApiProperty({
    description: 'Id người dùng',
    required: false,
    type: Number,
  })
  userId?: number;

  @ApiProperty({
    description: 'Số lượng record/trang',
    type: Number,
    required: false,
    default: 10,
  })
  limit?: number;

  @ApiProperty({
    description: 'Trang',
    type: Number,
    required: false,
    default: 1,
  })
  page?: number;
}

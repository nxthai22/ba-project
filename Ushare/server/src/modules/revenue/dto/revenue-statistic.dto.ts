import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional } from 'class-validator';
import { SearchCriteria } from 'modules/base/base-search';
import { addDays, startOfMonth } from 'date-fns';
import { Revenue } from './revenue-response.dto';

export enum FillterType {
  Day = 'day',
  Week = 'week',
  Month = 'month',
  Quarter = 'quarter',
  Year = 'year',
}

export class RevenueStatisticByUserDto {
  @ApiProperty({
    description: 'Tháng bắt đầu thực hiện tìm kiếm',
    default: addDays(new Date(), -7),
  })
  fromDate: Date;

  @ApiProperty({
    description: 'Tháng kết thúc thực hiện tìm kiếm',
    default: new Date(),
  })
  toDate: Date;

  @ApiProperty({
    description: 'Loại tìm kiếm',
    type: 'enum',
    enum: FillterType,
    default: FillterType.Day,
  })
  fillterType: FillterType;

  @ApiProperty({
    description: 'Id NCC',
    type: Number,
    required: false,
  })
  merchantId?: number;
}

export class RevenueStatisticByUserResponse {
  @ApiProperty({
    description: 'Ngày',
    type: Date,
  })
  date: Date;

  @ApiProperty({
    description: 'Doanh thu',
    type: Revenue,
  })
  revenue: Revenue;
}

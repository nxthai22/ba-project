import { ApiProperty } from '@nestjs/swagger';
import { CommissionValueType } from 'entities/config.entity';
import { MerchantEntity } from 'entities/merchant.entity';
import { UserEntity } from 'entities/user.entity';

/** Object doanh thu dùng cho báo cáo */
export class RevenueStatistic {
  @ApiProperty({
    description: 'Id CTV',
  })
  userId?: number;

  @ApiProperty({
    description: 'Id NCC',
  })
  merchantId?: number;

  @ApiProperty({
    description: 'Tháng/Quý tính doanh thu/thưởng',
  })
  date?: Date;

  @ApiProperty({
    description: 'Tổng doanh thu hoàn thành',
  })
  totalRevenueCompleted?: number;

  @ApiProperty({
    description: 'Tổng chiết khấu theo doanh thu hoàn thành',
  })
  totalCommissionCompleted?: number;

  @ApiProperty({
    description: 'Tổng chiết khấu theo doanh thu',
  })
  totalCommission?: number;

  @ApiProperty({
    description: 'Thưởng giới thiệu',
  })
  totalReferralCommission?: number;

  // @ApiProperty({
  //   description: 'Chi tiết hoa hồng giới thiệu',
  // })
  // detailReferralCommissions?: DetailRefrralStatistic[];

  @ApiProperty({
    description: 'Hoa hồng/thưởng được áp dụng',
  })
  commissionValue?: number;

  @ApiProperty({
    description: 'Loại giá trị của hoa hồng/thưởng (tiền/%)',
  })
  commissionValueType?: CommissionValueType;

  @ApiProperty({
    description: 'Khoảng thời gian thưởng (Tháng, quý, năm)',
  })
  commissionTimeType?: CommissionTimeType;
}

export class RevenueReferralStatistic {
  @ApiProperty({
    description: 'Thông tin người giới thiệu',
  })
  referral: UserEntity;

  @ApiProperty({
    description: 'Số lượng thành viên được giới thiêu',
    type: Number,
  })
  members: number;

  @ApiProperty({
    description: 'Thống kê hoa hồng giới thiệu',
    type: [RevenueStatistic],
  })
  statistic: RevenueStatistic[];
  @ApiProperty({
    description: 'Thống kê hoa hồng giới thiệu',
    type: Number,
  })
  totalCommission?: number;
}

export class DetailRefrralStatistic {
  @ApiProperty({
    description: 'Thông tin thành viên được giới thiệu',
  })
  user: UserEntity;
  @ApiProperty({
    description: 'Thưởng đơn đầu tiên',
  })
  firstOrderCommission?: number | 0;
  @ApiProperty({
    description: 'Thưởng doanh thu trong tháng',
  })
  revenueCommission?: number | 0;
  @ApiProperty({
    description: 'Tổng hoa hồng',
  })
  totalCommission?: number | 0;
}
export class RevenueStatisticResponse {
  @ApiProperty({
    description: 'Id CTV',
  })
  userId?: number;

  @ApiProperty({
    description: 'Thông tin CTV',
  })
  user?: UserEntity;

  @ApiProperty({
    description: 'Id NCC',
  })
  merchantId?: number;

  @ApiProperty({
    description: 'Thông tin NCC',
  })
  merchant?: MerchantEntity;

  @ApiProperty({
    description: 'Thống kê toàn bộ doanh thu',
    required: false,
    type: [RevenueStatistic],
  })
  revenueStatistic?: RevenueStatistic[];

  @ApiProperty({
    description: 'Thống kê doanh thu theo tháng',
    required: false,
    type: [RevenueStatistic],
  })
  revenueStatisticByMonth?: RevenueStatistic[];

  @ApiProperty({
    description: 'Thống kê doanh thu theo quý',
    required: false,
    type: [RevenueStatistic],
  })
  revenueStatisticByQuarter?: RevenueStatistic[];

  @ApiProperty({
    description: 'Tổng chiết khấu theo doanh thu',
    required: false,
    type: Number,
  })
  totalCommissionRevenue?: number;
}

export class RevenueStatisticDetail {
  @ApiProperty({
    description: 'Id CTV',
  })
  userId?: number;

  @ApiProperty({
    description: 'Thông tin CTV',
    type: UserEntity,
  })
  user?: UserEntity;

  @ApiProperty({
    description: 'Thống kê chi tiết doanh thu',
    type: [RevenueStatistic],
    default: [],
  })
  comissions: RevenueStatistic[];

  @ApiProperty({
    description: 'Tổng chiết khấu theo doanh thu',
    required: false,
    type: Number,
  })
  totalCommissionRevenue?: number;
}

/** Khoảng thời gian thưởng (Tháng, quý, năm)  */
export enum CommissionTimeType {
  MONTH = 'month',
  QUARTER = 'quarter',
  YEAR = 'year',
}

import { ApiProperty, PickType } from "@nestjs/swagger";
import { OrderRevenueEntity } from "entities/order-revenue.entity";
import { UserEntity } from "entities/user.entity";

export class DetailResponse extends PickType(OrderRevenueEntity,
    ['orderId',
        'order',
        'status',
        'revenue',
        'merchantId',
        'month',
        'userId',
        'revenue'
    ]) {
    @ApiProperty({
        description: 'Thông tin người dùng'
    })
    user: UserEntity;
}
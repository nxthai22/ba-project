import { ApiProperty } from '@nestjs/swagger';
import { ConfigEntity } from 'entities/config.entity';
import { OrderStatus } from 'enums';
import { RevenueStatisticResponse } from './revenue-statistic-response.dto';

export class Revenue {
  @ApiProperty({
    description: 'Tổng',
  })
  sum: number;

  @ApiProperty({
    description: 'Trạng thái',
    enum: OrderStatus,
  })
  status: OrderStatus;

  @ApiProperty({
    description: 'Số lượng đơn hàng',
  })
  count: number;

  @ApiProperty({
    description: 'Phần trăm',
  })
  rate?: number;
}

export class RevenueResponse {
  @ApiProperty({
    description: 'Doanh thu ước tính',
    type: Revenue,
  })
  revenue: Revenue[];

  @ApiProperty({
    description: 'Doanh thu hoàn thành',
    type: Revenue,
  })
  revenueComplepted?: Revenue[];

  @ApiProperty({
    description: 'Hoa hồng ước tính',
    type: Revenue,
  })
  commission: Revenue[];

  @ApiProperty({
    description: 'Hoa hồng thực nhận',
    type: Revenue,
  })
  commissionComplelted?: Revenue[];

  @ApiProperty({
    description: 'Hoa hồng giới thiệu',
  })
  referralCommission: number;

  @ApiProperty({
    description: 'Cấu hình hoa hồng',
  })
  currConfigCommission?: ConfigEntity[];
}

export class TopProductSaleDataset {
  @ApiProperty({
    description: 'Label',
    required: false,
    type: [String],
  })
  label: string;

  @ApiProperty({
    description: 'Data',
    required: false,
    type: [String],
  })
  data: number[];
  backgroundColor: string[];
}

export class TopProductSale {
  @ApiProperty({
    description: 'Label',
    required: false,
    type: [String],
  })
  labels: string[];

  @ApiProperty({
    description: 'Giá trị của Dataset',
    required: false,
    type: [TopProductSaleDataset],
  })
  datasets?: TopProductSaleDataset[];
}

export class CommissionDataset {
  @ApiProperty({
    description: 'Label của Dataset',
  })
  label: string;

  @ApiProperty({
    description: 'Giá trị của Dataset',
  })
  data: number[];

  @ApiProperty({
    description: 'Màu của Dataset',
  })
  fill?: boolean;

  @ApiProperty({
    description: 'Màu nề',
  })
  backgroundColor?: string;

  @ApiProperty({
    description: 'Màu viền',
  })
  borderColor?: string;

  @ApiProperty({
    description: 'tension',
  })
  tension?: number;
}

export class CommissionResponse {
  @ApiProperty({
    description: 'Label của biểu đồ',
  })
  labels: string[];

  @ApiProperty({
    description: 'Dữ liệu của biểu đồ',
    type: [CommissionDataset],
  })
  datasets: CommissionDataset[];
}

export class TopUserCommissionResponse {
  @ApiProperty({
    description: 'Hoa hồng sản phẩm + hoa hồng doanh thu',
    type: [RevenueStatisticResponse],
  })
  revenues: RevenueStatisticResponse[];
  @ApiProperty({
    description: 'Cấu hình thưởng doanh thu theo NCC',
    type: ConfigEntity,
  })
  configRevenue?: ConfigEntity;
  @ApiProperty({
    description: 'Doanh thu tháng hiện tại của CTV',
    type: RevenueStatisticResponse,
  })
  revenueCurrentMonth?: RevenueStatisticResponse;
}

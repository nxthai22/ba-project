import { BadRequestException, Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { ApiProperty } from '@nestjs/swagger';
import { InjectRepository } from '@nestjs/typeorm';
import {
  addMonths,
  compareAsc,
  differenceInMonths,
  eachDayOfInterval,
  eachMonthOfInterval,
  eachQuarterOfInterval,
  endOfDay,
  endOfMonth,
  endOfQuarter,
  format,
  startOfDay,
  startOfMonth,
  startOfQuarter,
} from 'date-fns';
import {
  CommissionValueType,
  ConfigEntity,
  ConfigMerchantCommission,
  ConfigType,
  ReferralCommission,
} from 'entities/config.entity';
import { OrderProductEntity } from 'entities/order-product.entity';
import { OrderRevenueEntity } from 'entities/order-revenue.entity';
import { OrderEntity } from 'entities/order.entity';
import { ProductEntity } from 'entities/product.entity';
import { RevenueStatisticEntity } from 'entities/revenue-statistic.entity';
import { RoleEntity, RoleType } from 'entities/role.entity';
import { UserEntity } from 'entities/user.entity';
import { OrderStatus } from 'enums';
import {
  Between,
  In,
  LessThanOrEqual,
  MoreThan,
  MoreThanOrEqual,
  QueryRunner,
  Raw,
  Repository,
} from 'typeorm';
import { FilterRevenueWithPaging } from './dto/request-fillter.dto';
import {
  Revenue,
  RevenueResponse,
  TopUserCommissionResponse,
} from './dto/revenue-response.dto';
import {
  CommissionTimeType,
  DetailRefrralStatistic,
  RevenueReferralStatistic,
  RevenueStatistic,
  RevenueStatisticDetail,
  RevenueStatisticResponse,
} from './dto/revenue-statistic-response.dto';
import { _, sortBy } from 'lodash';
import { GetManyDefaultResponse } from '@nestjsx/crud/lib/interfaces/get-many-default-response.interface';
import { UserService } from '../user/user.service';
import {
  FillterType,
  RevenueStatisticByUserDto,
  RevenueStatisticByUserResponse,
} from './dto/revenue-statistic.dto';
import { ProductVariantEntity } from '../../entities/product-variant.entity';

export class OrderRevenueSummary {
  @ApiProperty({
    description: 'Trạng thái đơn hàng',
  })
  status?: OrderStatus;

  @ApiProperty({
    description: 'Tháng hoàn thành trạng thái',
  })
  month?: Date;

  @ApiProperty({
    description: 'Ngày hoàn thành trạng thái',
  })
  day?: Date;

  @ApiProperty({
    description: 'Id của đối tác',
  })
  merchantId?: number;

  @ApiProperty({
    description: 'Id của đối tác',
  })
  userId?: number;

  @ApiProperty({
    description: 'Tổng doanh thu',
  })
  totalRevenue: string;
}

export class ReferralCommissionSummary {
  @ApiProperty({
    description: 'Trạng thái đơn hàng',
  })
  status: OrderStatus;

  @ApiProperty({
    description: 'Tháng hoàn thành trạng thái',
  })
  month: Date;

  @ApiProperty({
    description: 'Id NCC',
  })
  merchantId?: number;

  @ApiProperty({
    description: 'Ngày bắt đầu tính hoa hồng cho người giới thiệu',
  })
  referralBonusStart?: Date;

  @ApiProperty({
    description: 'Ngày kết thúc tính hoa hồng cho người giới thiệu',
  })
  referralBonusExpire?: Date;

  @ApiProperty({
    description: 'Giá trị hoa hồng/thưởng được áp dụng',
  })
  valueBonus?: string;

  @ApiProperty({
    description: 'Loại giá trị hoa hồng/thưởng được áp dụng',
  })
  valueBonusType?: CommissionValueType;

  @ApiProperty({
    description: 'Id của người mua hàng',
  })
  userId?: number;

  @ApiProperty({
    description: 'Id của người giới thiệu',
  })
  referralId?: number;

  @ApiProperty({
    description: 'Tổng doanh thu',
  })
  totalRevenue: number;

  @ApiProperty({
    description: 'Tổng hoa hồng từ doanh thu',
  })
  totalCommissionRevenue?: number;

  @ApiProperty({
    description: 'Id cấu hình hoa hồng/thưởng cho đơn đầu tiên',
  })
  orderConfigCommissionId: number;
}

@Injectable()
export class RevenueService {
  constructor(
    @InjectRepository(OrderEntity) public orderRepo: Repository<OrderEntity>,
    @InjectRepository(ProductEntity)
    public productRepo: Repository<ProductEntity>,
    @InjectRepository(ProductVariantEntity)
    public productVariantRepo: Repository<ProductVariantEntity>,
    @InjectRepository(OrderProductEntity)
    public orderProRepo: Repository<OrderProductEntity>,
    @InjectRepository(ConfigEntity)
    public configRepo: Repository<ConfigEntity>,
    @InjectRepository(OrderRevenueEntity)
    public orderRevenueRepo: Repository<OrderRevenueEntity>,
    @InjectRepository(RevenueStatisticEntity)
    public revenueStatisticRepo: Repository<RevenueStatisticEntity>,
    @InjectRepository(RoleEntity)
    public roleRepo: Repository<RoleEntity>,
    public userService: UserService,
  ) {}

  /** Lấy danh sách thống kê doanh thu/hoa hồng theo từng CTV, từng NCC */
  async getRevenueStatistics(
    dto: FilterRevenueWithPaging,
    user: UserEntity,
  ): Promise<RevenueStatistic[]> {
    const startMonth = startOfMonth(new Date(dto.fromDate));
    const endMonth = endOfMonth(new Date(dto.toDate));

    // Lấy danh sách doanh thu
    const revenueStatisticQuery = this.revenueStatisticRepo
      .createQueryBuilder('revenueStatistic')
      .leftJoinAndSelect('revenueStatistic.user', 'user')
      .leftJoinAndSelect('revenueStatistic.merchant', 'merchant')
      .where('"revenueStatistic"."date" BETWEEN :fromDate AND :toDate', {
        fromDate: startMonth,
        toDate: endMonth,
      });
    // Doanh thu theo tháng hiện tại
    const startCurrMonth = startOfMonth(new Date());
    const endCurrMonth = endOfMonth(new Date());
    const revenueMonthsQuery = this.orderRevenueRepo
      .createQueryBuilder()
      .where('"status" = :status', {
        status: OrderStatus.COMPLETED,
      })
      .andWhere('"month" BETWEEN :fromDate AND :toDate', {
        fromDate: startCurrMonth,
        toDate: endCurrMonth,
      });

    // Kiểm tra điều kiện tìm kiếm
    // if (dto?.merchantId) {
    //   revenueStatisticQuery.andWhere(
    //     '"revenueStatistic"."merchantId" = :merchantId',
    //     {
    //       merchantId: dto?.merchantId,
    //     },
    //   );
    //   revenueMonthsQuery.andWhere('"merchantId" = :merchantId', {
    //     merchantId: dto?.merchantId,
    //   });
    // }
    if (user) {
      let userIds: number[] = [];
      switch (user?.role?.type) {
        case RoleType.PARTNER:
          revenueStatisticQuery.andWhere(
            '"revenueStatistic"."userId" = :userId',
            {
              userId: user.id,
            },
          );
          revenueMonthsQuery.andWhere('"userId" = :userId', {
            userId: user.id,
          });
          break;
        case RoleType.ADMIN:
          revenueStatisticQuery.andWhere(
            '"revenueStatistic"."merchantId" = :merchantId',
            {
              merchantId: dto.merchantId,
            },
          );
          revenueMonthsQuery.andWhere('"merchantId" = :merchantId', {
            merchantId: dto.merchantId,
          });
          if (dto?.field) {
            userIds = (
              await this.userService.repo
                .createQueryBuilder('user')
                .where(`LOWER("user"."${dto.field}") LIKE :textSearch`, {
                  textSearch: '%' + dto?.textSearch?.toLocaleLowerCase() + '%',
                })
                .getMany()
            )?.map((us) => us.id);
          }
          break;
        case RoleType.MERCHANT:
          revenueStatisticQuery.andWhere(
            '"revenueStatistic"."merchantId" = :merchantId',
            {
              merchantId: user.merchantId,
            },
          );
          revenueMonthsQuery.andWhere('"merchantId" = :merchantId', {
            merchantId: user.merchantId,
          });
          if (dto?.field) {
            userIds = (
              await this.userService.repo
                .createQueryBuilder('user')
                .where(`LOWER("user"."${dto.field}") LIKE :textSearch`, {
                  textSearch: '%' + dto?.textSearch?.toLocaleLowerCase() + '%',
                })
                .getMany()
            )?.map((us) => us.id);
          }
          break;
        case RoleType.LEADER:
          revenueStatisticQuery.andWhere(
            '"revenueStatistic"."merchantId" = :merchantId',
            {
              merchantId: user.merchantId,
            },
          );
          revenueMonthsQuery.andWhere('"merchantId" = :merchantId', {
            merchantId: user.merchantId,
          });
          userIds = (
            await this.userService.getMemberByLeader(null, user)
          )?.data?.map((us) => us.id);
        default:
          break;
      }
      if (!userIds || userIds?.length < 1) userIds.push(-1);
      if (dto.textSearch) {
        revenueStatisticQuery.andWhere(
          '"revenueStatistic"."userId" IN (:...userIds)',
          {
            userIds: userIds,
          },
        );
        revenueMonthsQuery.andWhere('"userId" IN (:...userIds)', {
          userIds: userIds,
        });
      }
    }
    const revenues = await revenueStatisticQuery.getMany();
    // Nếu tháng hiện tại nằm trong quý tìm kiếm thì thống kê doanh thu tháng hiện tại
    if (
      compareAsc(endOfMonth(new Date()), endMonth) <= 0 &&
      compareAsc(startOfMonth(new Date()), startMonth) >= 0
    ) {
      const configMonths = await this.configRepo.find({
        key: ConfigType.REVENUE,
        merchantId: user.merchantId || dto.merchantId,
        startDate: LessThanOrEqual(startCurrMonth),
        endDate: MoreThanOrEqual(endCurrMonth),
      });
      revenueMonthsQuery
        .groupBy('"userId"')
        .addGroupBy('"merchantId"')
        .select('"userId"')
        .addSelect('"merchantId"')
        .addSelect('SUM("revenue") as "totalRevenue"');
      const revenueMonths = (await revenueMonthsQuery.execute()) as {
        userId: number;
        merchantId: number;
        totalRevenue: number;
      }[];
      await Promise.all(
        revenueMonths?.map(async (rMonth) => {
          const revenue = new RevenueStatisticEntity();
          revenue.userId = rMonth.userId;
          revenue.user = await this.userService.repo.findOne({
            where: {
              id: rMonth.userId,
            },
          });
          revenue.totalRevenueCompleted = rMonth.totalRevenue;
          revenue.merchantId = rMonth.merchantId;
          const configMonth = (
            configMonths?.find((x) => x.merchantId === rMonth.merchantId)
              ?.value as ConfigMerchantCommission[]
          )?.find(
            (x) =>
              x?.min <= revenue.totalRevenueCompleted &&
              x?.max >= revenue.totalRevenueCompleted,
          );
          revenue.totalCommissionCompleted =
            (configMonth?.valueType === CommissionValueType.AMOUNT
              ? Number(configMonth?.value || 0)
              : (Number(configMonth?.value || 0) / 100) *
                revenue.totalRevenueCompleted) || 0;
          revenue.date = startCurrMonth;
          revenues.push(revenue);
        }),
      );
    }

    // Thống kê danh sách doanh thu CTV
    const revenueStatistics: RevenueStatistic[] = [];
    const merchantIds = revenues
      ?.map((r) => r.merchantId)
      ?.filter((curr, idx, arr) => arr.indexOf(curr) === idx);

    const configMerchants = await this.configRepo.find({
      where: {
        startDate: LessThanOrEqual(startMonth),
        endDate: MoreThanOrEqual(startMonth),
        merchantId: In([...merchantIds]),
      },
    });
    revenues?.forEach((revenue) => {
      const revenueStatistic = new RevenueStatistic();
      revenueStatistic.userId = revenue?.userId;
      revenueStatistic.merchantId = revenue?.merchantId;
      revenueStatistic.date = revenue?.date;
      revenueStatistic.totalRevenueCompleted = revenue?.totalRevenueCompleted;
      revenueStatistic.totalCommissionCompleted =
        revenue.totalCommissionCompleted;
      const configMerchant = (
        configMerchants?.find(
          (x) =>
            x?.merchantId === revenue?.merchantId &&
            x?.startDate <= revenue?.date &&
            x?.endDate >= revenue?.date,
        )?.value as ConfigMerchantCommission[]
      )?.find(
        (x) =>
          x.min <= revenueStatistic?.totalCommissionCompleted &&
          x.max >= revenue?.totalCommissionCompleted,
      );
      revenueStatistic.commissionValue = Number(configMerchant?.value || 0);
      revenueStatistic.commissionValueType =
        configMerchant?.valueType || CommissionValueType.PERCENT;
      revenueStatistic.commissionTimeType = CommissionTimeType.MONTH;
      revenueStatistics.push(revenueStatistic);
    });
    return revenueStatistics;
  }

  /** Thống kê doanh thu (dựa theo bảng cấu hình) */
  async calculateRevenueNew(
    dto: FilterRevenueWithPaging,
    user: UserEntity,
  ): Promise<RevenueResponse> {
    /** Doanh thu theo đơn hàng */
    const orderRevenuesQuery = this.orderRevenueRepo
      .createQueryBuilder('orderRevenue')
      .leftJoinAndSelect(
        'orderRevenue.order',
        'order',
        '"orderRevenue"."orderId" = "order"."id"',
      )
      .where('"orderRevenue"."status" IN (:...status)', {
        status: [OrderStatus.CONFIRMED],
      })
      .andWhere('"orderRevenue"."createdAt" BETWEEN :fromDate AND :toDate', {
        fromDate: startOfDay(new Date(dto.fromDate)),
        toDate: endOfDay(new Date(dto.toDate)),
      });
    if (user) {
      const role = await this.roleRepo.findOne({
        id: user?.roleId,
      });
      switch (role?.type) {
        case RoleType.PARTNER:
          orderRevenuesQuery.andWhere('"order"."userId" = :userId', {
            userId: user.id,
          });
          break;
        case RoleType.MERCHANT:
          orderRevenuesQuery.andWhere('"order"."merchantId" = :merchantId', {
            merchantId: user.merchantId,
          });
          if (dto?.field) {
            const userIds = (
              await this.userService.repo
                .createQueryBuilder('user')
                .where(`"user"."${dto.field}" = :textSearch`, {
                  textSearch: dto?.textSearch,
                })
                .getMany()
            )?.map((us) => us.id);
            orderRevenuesQuery.andWhere('"order"."userId" IN (:...userIds)', {
              userIds: userIds,
            });
          }
          break;
        case RoleType.LEADER:
          orderRevenuesQuery
            .leftJoin(
              'user_leader',
              'leader',
              '"leader"."userId" = "orderRevenue"."userId"',
            )
            .andWhere('"leader"."leaderId" = :leaderId', {
              leaderId: user.id,
            })
            .andWhere('"orderRevenue"."merchantId" = :merchantId', {
              merchantId: user.merchantId,
            });
          break;
        default:
          break;
      }
    }
    orderRevenuesQuery
      .groupBy('"order"."status"')
      .addGroupBy('"orderRevenue"."month"')
      .addGroupBy('"orderRevenue"."orderId"')
      .addGroupBy('date_trunc(\'day\', "orderRevenue"."createdAt")')
      .addGroupBy('"orderRevenue"."merchantId"')
      .addGroupBy('"orderRevenue"."userId"')
      .select('"order"."status" AS status')
      .addSelect(['"orderRevenue"."orderId"'])
      .addSelect('"orderRevenue"."userId"')
      .addSelect('"orderRevenue"."month"')
      .addSelect('date_trunc(\'day\', "orderRevenue"."createdAt") as day')
      .addSelect('"orderRevenue"."merchantId"')
      .addSelect('SUM("orderRevenue"."revenue")', 'totalRevenue')
      // .orderBy('"orderRevenue"."orderId"', 'ASC')
      .orderBy('day', 'ASC', 'NULLS FIRST');

    const orderRevenues =
      ((await orderRevenuesQuery.execute()) as OrderRevenueSummary[]) || [];
    // Doanh thu ước tính
    const revenueTotalResponse: Revenue = {
      status: null,
      count:
        orderRevenues.filter((rev) =>
          [
            OrderStatus.CONFIRMED,
            OrderStatus.SHIPPING,
            OrderStatus.SHIPPED,
          ].includes(rev.status),
        )?.length || 0,
      sum:
        orderRevenues.reduce((total, curr) => {
          if (
            [
              OrderStatus.CONFIRMED,
              OrderStatus.SHIPPING,
              OrderStatus.SHIPPED,
            ].includes(curr.status)
          )
            return total + Number(curr.totalRevenue);
          return total;
        }, 0) || 0,
      rate: 0,
    };
    // Doanh thu hoàn thành
    const orderCompleteds = orderRevenues.filter(
      (x) => x.status === OrderStatus.COMPLETED,
    );
    const revenueCompletedResponse: Revenue = {
      status: OrderStatus.COMPLETED,
      count: orderCompleteds?.length || 0,
      sum:
        orderRevenues.reduce((total, curr) => {
          if (curr.status === OrderStatus.COMPLETED)
            return total + Number(curr.totalRevenue);
          return total;
        }, 0) || 0,
    };
    /** Hoa hồng theo đối tác*/
    // Danh sách đối tác
    const merchantIds = orderRevenues
      .map((m) => m.merchantId)
      .filter((val, idx, arr) => arr.indexOf(val) === idx);
    const configMerchantCommissions = await this.configRepo.find({
      where: {
        key: ConfigType.REVENUE,
        startDate:
          LessThanOrEqual(orderRevenues?.[0]?.day) ||
          Between(
            orderRevenues?.[0].day,
            orderRevenues?.[orderRevenues?.length - 1]?.day,
          ),
        endDate: MoreThan(orderRevenues?.[0]?.day),
        merchantId: In(merchantIds),
      },
    });
    // Danh sách người dùng
    const userIds = orderRevenues
      .map((m) => m.userId)
      .filter((val, idx, arr) => arr.indexOf(val) === idx);
    // Kết quả trả về
    const commissionsResponse: Revenue[] = [];
    const commissionCompletedsResponse: Revenue[] = [];
    const referralCommissionResponse: Revenue[] = [];
    const currConfigCommission = [];

    await Promise.all(
      eachMonthOfInterval({
        start: new Date(dto.fromDate),
        end: new Date(dto.toDate),
      }).map(async (month) => {
        /** Hoa hồng theo đơn hàng */
        configMerchantCommissions.forEach((cfMechant) => {
          // Tính hoa hồng cho từng user
          userIds.forEach((userId) => {
            // Hoa hồng ước tính
            const _orders = orderRevenues.filter(
              (x) =>
                x.day >= cfMechant.startDate &&
                x.day <= cfMechant.endDate &&
                format(x.month, 'MM/yyyy') === format(month, 'MM/yyyy') &&
                x.userId === userId,
            );
            const _orderSum = _orders.reduce(function (total, curr) {
              return total + Number(curr.totalRevenue);
            }, 0);
            const _cfMerchantValue = (
              cfMechant.value as ConfigMerchantCommission[]
            ).find(
              (cf) =>
                Number(cf.min) <= _orderSum && Number(cf.max) >= _orderSum,
            );
            commissionsResponse.push({
              status: null,
              count: _orders?.length,
              sum:
                _cfMerchantValue?.valueType === CommissionValueType.AMOUNT
                  ? Number(_cfMerchantValue?.value) || 0
                  : _orderSum * (Number(_cfMerchantValue?.value) / 100 || 0),
              rate: Number(_cfMerchantValue?.value),
            });
            // Hoa hồng thực nhận
            const _orderCompleteds = _orders.filter(
              (x) => x.status === OrderStatus.COMPLETED,
            );
            const _orderSumCompleted = _orderCompleteds.reduce(function (
              total,
              curr,
            ) {
              return total + Number(curr.totalRevenue);
            },
            0);
            const _cfMerchantValueCompleted = (
              cfMechant.value as ConfigMerchantCommission[]
            ).find(
              (cf) =>
                Number(cf.min) <= _orderSumCompleted &&
                Number(cf.max) >= _orderSumCompleted,
            );
            commissionCompletedsResponse.push({
              status: OrderStatus.COMPLETED,
              count: _orderCompleteds?.length,
              sum:
                _cfMerchantValueCompleted?.valueType ===
                CommissionValueType.AMOUNT
                  ? Number(_cfMerchantValueCompleted?.value) || 0
                  : _orderSumCompleted *
                    (Number(_cfMerchantValueCompleted?.value) / 100 || 0),
              rate: Number(_cfMerchantValueCompleted?.value),
            });
          });
        });
        if (format(month, 'MM/yyyy') === format(new Date(), 'MM/yyyy')) {
          const currConfig = await this.configRepo.findOne({
            where: {
              key: ConfigType.REVENUE,
              merchantId: dto.merchantId || 1,
              startDate: LessThanOrEqual(new Date()),
              endDate: MoreThanOrEqual(new Date()),
            },
          });
          currConfigCommission.push(currConfig);
        }
      }),
    );

    return {
      revenue: [revenueTotalResponse],
      revenueComplepted: [revenueCompletedResponse],
      commission: [
        {
          status: null,
          count: commissionsResponse
            .filter((x) => x.status === null)
            .reduce(function (total, curr) {
              return total + Number(curr.count);
            }, 0),
          sum: commissionsResponse
            .filter((x) => x.status === null)
            .reduce(function (total, curr) {
              return total + Number(curr.sum);
            }, 0),
          rate: commissionsResponse.find((x) => x.status === null)?.rate || 0,
        },
      ],
      commissionComplelted: [
        {
          status: OrderStatus.COMPLETED,
          count: commissionCompletedsResponse
            .filter((x) => x.status === OrderStatus.COMPLETED)
            .reduce(function (total, curr) {
              return total + Number(curr.count);
            }, 0),
          sum: commissionCompletedsResponse
            .filter((x) => x.status === OrderStatus.COMPLETED)
            .reduce(function (total, curr) {
              return total + Number(curr.sum);
            }, 0),
          rate:
            commissionCompletedsResponse.find(
              (x) => x.status === OrderStatus.COMPLETED,
            )?.rate || 0,
        },
      ],
      referralCommission: referralCommissionResponse.reduce(function (
        total,
        curr,
      ) {
        return total + Number(curr.sum);
      },
      0),
      currConfigCommission: currConfigCommission,
    };
  }

  /** Tính thống kê theo khoảng thời gian mong muốn */
  async caculateRevenueStatistic(
    dto: FilterRevenueWithPaging,
    user: UserEntity,
  ): Promise<any> {
    const startDate = startOfMonth(new Date(dto.fromDate));
    const endDate = endOfMonth(new Date(dto.toDate));
    const query = this.revenueStatisticRepo
      .createQueryBuilder('revenueStatistic')
      .leftJoinAndSelect('revenueStatistic.user', 'user')
      .leftJoinAndSelect('revenueStatistic.merchant', 'merchant')
      .where('"revenueStatistic"."date" BETWEEN :fromDate AND :toDate', {
        fromDate: startDate,
        toDate: endDate,
      });
    if (user) {
      switch (user?.role?.type) {
        case RoleType.PARTNER:
          query.andWhere('"revenueStatistic"."userId" = :userId', {
            userId: user.id,
          });
          break;
        case RoleType.MERCHANT:
          query.andWhere('"revenueStatistic"."merchantId" = :merchantId', {
            merchantId: user.merchantId,
          });
          if (dto?.field) {
            const userIds = (
              await this.userService.repo
                .createQueryBuilder('user')
                .where(`LOWER("user"."${dto.field}") LIKE :textSearch`, {
                  textSearch: '%' + dto?.textSearch + '%',
                })
                .getMany()
            )?.map((us) => us.id);
            if (userIds && userIds?.length > 0) {
              query.andWhere('"revenueStatistic"."userId" IN(:...userIds)', {
                userIds: userIds,
              });
            }
          }
          break;
        default:
          break;
      }
    }
    return await query.getMany();
  }

  /** Thống kê chi tiết */
  async calculateRevenueStatisticDetail(
    dto: FilterRevenueWithPaging,
    user: UserEntity,
  ): Promise<GetManyDefaultResponse<RevenueStatisticDetail>> {
    const response: RevenueStatisticDetail[] = [];
    dto.fromDate = startOfQuarter(new Date(dto.fromDate));
    dto.toDate = endOfQuarter(new Date(dto.toDate));
    const revenues = await this.getRevenueStatistics(dto, user);
    // Danh sách cấu hình thưởng, hoa hồng
    const configMerchants = await this.configRepo.find({
      where: {
        merchantId:
          user?.role?.type === RoleType.ADMIN
            ? dto.merchantId
            : user.merchantId,
        startDate: LessThanOrEqual(dto.fromDate),
        endDate: MoreThanOrEqual(dto.fromDate),
      },
    });
    // Danh sách user
    const userIds = revenues
      ?.map((r) => r.userId)
      ?.filter((curr, idx, arr) => arr.indexOf(curr) === idx);
    const total = userIds?.length || 0;
    // Tính doanh thu theo tháng, quý cho từng user
    await Promise.all(
      userIds
        ?.slice((dto.page - 1) * dto.limit, dto.limit)
        ?.map(async (userId) => {
          const responseUser = new RevenueStatisticDetail();
          responseUser.userId = userId;
          responseUser.user = await this.userService.repo.findOne({
            where: {
              id: userId,
            },
            relations: [
              'bank',
              'ward',
              'ward.district',
              'ward.district.province',
            ],
          });
          responseUser.comissions = [];
          // Thống kê theo quý
          eachQuarterOfInterval({ start: dto.fromDate, end: dto.toDate }).map(
            (quarter) => {
              // Thưởng quý theo merchant
              const revenueStatisticQuarter = new RevenueStatistic();
              const revenueQuarters = revenues?.filter(
                (r) =>
                  r.userId === userId &&
                  r.date >= startOfQuarter(quarter) &&
                  r.date <= endOfQuarter(quarter),
              );
              const totalRevenueQuarter = revenueQuarters?.reduce(function (
                total,
                curr,
              ) {
                return total + Number(curr.totalRevenueCompleted || 0);
              },
              0);
              const configQuarter = (
                configMerchants?.find(
                  (cf) =>
                    cf.key === ConfigType.REVENUE_QUARTER &&
                    cf.startDate <= quarter &&
                    cf.endDate >= quarter,
                )?.value as ConfigMerchantCommission[]
              )?.find(
                (x) =>
                  x.min <= totalRevenueQuarter && x.max >= totalRevenueQuarter,
              );
              // revenueStatisticQuarter.userId = userId;
              revenueStatisticQuarter.merchantId = dto.merchantId;
              revenueStatisticQuarter.date = quarter;
              revenueStatisticQuarter.totalRevenueCompleted =
                totalRevenueQuarter;
              revenueStatisticQuarter.totalCommissionCompleted = Math.floor(
                configQuarter?.valueType === CommissionValueType.AMOUNT
                  ? Number(configQuarter?.value || 0)
                  : (Number(configQuarter?.value || 0) * totalRevenueQuarter) /
                      100,
              );
              revenueStatisticQuarter.commissionValue = Number(
                configQuarter?.value || 0,
              );
              revenueStatisticQuarter.commissionValueType =
                configQuarter?.valueType;
              revenueStatisticQuarter.commissionTimeType =
                CommissionTimeType.QUARTER;

              // Thống kê theo tháng
              eachMonthOfInterval({
                start: startOfQuarter(quarter),
                end: endOfQuarter(quarter),
              }).map((month) => {
                const revenueStatisticMonth = new RevenueStatistic();
                const revenueMonth = revenueQuarters.find(
                  (x) => format(x.date, 'MM/yyyy') === format(month, 'MM/yyyy'),
                );
                const configMonth = (
                  configMerchants?.find(
                    (cf) =>
                      cf.key === ConfigType.REVENUE &&
                      cf.startDate <= month &&
                      cf.endDate >= month,
                  )?.value as ConfigMerchantCommission[]
                )?.find(
                  (x) =>
                    x.min <= revenueMonth?.totalRevenueCompleted &&
                    x.max >= revenueMonth?.totalRevenueCompleted,
                );
                // revenueStatisticMonth.userId = userId;
                revenueStatisticMonth.merchantId = dto.merchantId;
                revenueStatisticMonth.date = month;
                revenueStatisticMonth.totalRevenueCompleted =
                  revenueMonth?.totalRevenueCompleted || 0;
                revenueStatisticMonth.totalCommissionCompleted =
                  Math.floor(revenueMonth?.totalCommissionCompleted) || 0;
                revenueStatisticMonth.commissionValue = Number(
                  configMonth?.value || 0,
                );
                revenueStatisticMonth.commissionValueType =
                  configMonth?.valueType;
                revenueStatisticMonth.commissionTimeType =
                  CommissionTimeType.MONTH;
                // Thêm doanh thu tháng vào mảng doanh thu cho từng user
                responseUser.comissions.push(revenueStatisticMonth);
              });
              // Thêm doanh thu quý vào mảng doanh thu cho từng user
              responseUser.comissions.push(revenueStatisticQuarter);
            },
          );
          // Thêm vào mảng kết quả trả về
          responseUser.totalCommissionRevenue = responseUser.comissions.reduce(
            (total, curr) => {
              return total + Number(curr.totalCommissionCompleted || 0);
            },
            0,
          );
          response.push(responseUser);
        }),
    );
    return {
      data: response,
      count: response?.length || 0,
      total: total,
      page: Number(dto.page),
      pageCount: Math.ceil(total / Number(dto.limit || 1)),
    };
  }

  /** Top CTV hoa hồng theo sản phẩm + hoa hồng theo doanh thu */
  async getTopUserCommission(
    dto: FilterRevenueWithPaging,
  ): Promise<RevenueStatisticResponse[]> {
    // Doanh thu những tháng đã thống kê
    const query = this.orderRevenueRepo
      .createQueryBuilder('orderRevenue')
      .where('"orderRevenue"."status" IN(:...status)', {
        status: [OrderStatus.COMPLETED, OrderStatus.CONFIRMED],
      })
      .groupBy('"orderRevenue"."userId"')
      .addGroupBy('"orderRevenue"."status"')
      .select('"orderRevenue"."userId"')
      .addSelect('"orderRevenue"."status"')
      .addSelect('SUM("orderRevenue"."revenue") as "totalRevenue"')
      .addSelect(
        'SUM("orderRevenue"."productCommission") as "totalProductCommission"',
      )
      .orderBy('"totalProductCommission"', 'DESC', 'NULLS LAST');
    if (dto.userId && dto.userId > 0) {
      query.andWhere('"orderRevenue"."userId" = :userId', {
        userId: dto.userId,
      });
    }
    if (dto.merchantId && dto.merchantId > 0) {
      query.andWhere('"orderRevenue"."merchantId" = :merchantId', {
        merchantId: dto.merchantId,
      });
    }
    if (!dto.userId && dto.limit > 0 && dto.page > 0)
      query.offset((dto.page - 1) * dto.limit).limit(dto.limit);
    // Tính hoa hồng theo sản phẩm
    const datas = (await query.execute()) as {
      userId: number;
      status: OrderStatus;
      totalProductCommission: number;
      totalRevenue: number;
    }[];
    const revenueProducts: RevenueStatisticResponse[] = [];
    await Promise.all(
      datas?.map(async (d) => {
        if (d.status === OrderStatus.COMPLETED) {
          const revenueUser = new RevenueStatisticResponse();
          revenueUser.userId = d.userId;
          revenueUser.user = await this.userService.repo.findOne({
            id: d.userId,
          });
          const totalCommission =
            datas.find(
              (x) =>
                x.userId === d?.userId && x.status === OrderStatus.CONFIRMED,
            )?.totalProductCommission || 0;
          revenueUser.revenueStatistic = [];
          revenueUser.revenueStatistic.push({
            userId: d?.userId,
            totalRevenueCompleted: d?.totalRevenue,
            totalCommissionCompleted: d?.totalProductCommission,
            totalCommission: totalCommission,
          });
          revenueProducts.push(revenueUser);
        }
      }),
    );
    // Tính hoa hồng theo doanh thu
    const userIds = datas?.reduce((arr, data) => {
      if (!arr?.includes(data.userId)) arr.push(data.userId);
      return arr;
    }, []);
    const users = await this.userService.repo.find({
      where: {
        id: In(userIds),
      },
      relations: ['role', 'merchants'],
    });
    const merchantIds = users?.map((user) => {
      return user?.merchants?.map((merchant) => merchant.id) || [];
    });
    const userMechantIds = dto.merchantId
      ? [dto.merchantId]
      : [
          ...merchantIds.reduce((arr, curr) => {
            arr.push(...curr);
            return arr;
          }, []),
        ] || [];

    // Cấu hình thưởng hoa hồng của NCC
    const configMerchants = await this.configRepo.find({
      where: {
        merchantId: In(userMechantIds),
      },
    });
    // Thống kê doanh thu
    const revenueStatisticQuery = this.revenueStatisticRepo
      .createQueryBuilder('revenueStatistic')
      .leftJoinAndSelect('revenueStatistic.user', 'user')
      .leftJoinAndSelect('revenueStatistic.merchant', 'merchant')
      .where('"revenueStatistic"."date" BETWEEN :fromDate AND :toDate', {
        fromDate: startOfMonth(new Date(2022, 1, 1)),
        toDate: endOfMonth(new Date()),
      });
    // Doanh thu theo tháng hiện tại
    const startCurrMonth = startOfMonth(new Date());
    const endCurrMonth = endOfMonth(new Date());
    const revenueMonthsQuery = this.orderRevenueRepo
      .createQueryBuilder()
      .where('"status" IN(:...status)', {
        status: [OrderStatus.COMPLETED, OrderStatus.CONFIRMED],
      })
      .andWhere('"month" BETWEEN :fromDate AND :toDate', {
        fromDate: startCurrMonth,
        toDate: endCurrMonth,
      });
    if (userIds?.length > 0) {
      revenueStatisticQuery.andWhere(
        '"revenueStatistic"."userId" IN(:...userId)',
        {
          userId: userIds,
        },
      );
      revenueMonthsQuery.andWhere('"userId" IN(:...userId)', {
        userId: userIds,
      });
    }
    revenueMonthsQuery
      .groupBy('"userId"')
      .addGroupBy('"status"')
      .addGroupBy('"merchantId"')
      .select('"userId"')
      .addSelect('"status"')
      .addSelect('"merchantId"')
      .addSelect('SUM("revenue") as "totalRevenue"');
    const revenues = await revenueStatisticQuery.getMany();
    const revenuesCurrentMonths = (await revenueMonthsQuery.execute()) as {
      userId: number;
      status: OrderStatus;
      merchantId: number;
      totalRevenue: number;
    }[];
    // Thống kê theo tháng
    const revenueStatisticMonths: RevenueStatisticResponse[] = [];
    let revenueCurrentMonth = new RevenueStatisticResponse();
    await Promise.all(
      users?.map(async (user) => {
        await Promise.all(
          eachMonthOfInterval({
            start: startOfMonth(new Date(2022, 1, 1)),
            end: endOfMonth(new Date()),
          }).map((month) => {
            Promise.all(
              userMechantIds.map((merchantId) => {
                const revenueStatisticMonth = new RevenueStatisticResponse();
                const configMerchat = configMerchants?.find(
                  (cf) =>
                    cf.key === ConfigType.REVENUE &&
                    cf.startDate <= month &&
                    cf.endDate >= month &&
                    cf.merchantId === merchantId,
                )?.value as ConfigMerchantCommission[];
                // Tính các tháng trước
                const revenueMonth = revenues.find(
                  (x) =>
                    format(x.date, 'MM/yyyy') === format(month, 'MM/yyyy') &&
                    x.userId === user.id,
                );
                const configMonth = configMerchat?.find(
                  (x) =>
                    x.min <= revenueMonth?.totalRevenueCompleted &&
                    x.max >= revenueMonth?.totalRevenueCompleted,
                );
                const commissionMonth =
                  configMonth?.valueType === CommissionValueType.AMOUNT
                    ? Number(configMonth?.value || 0)
                    : (Number(revenueMonth?.totalRevenueCompleted || 0) *
                        Number(configMonth?.value || 0)) /
                      100;
                // Tính tháng hiện tại
                let revenueCompletedCurrMonth = 0;
                let commissionCompletedCurrMonth = 0;
                let commissionConfirmedCurrMonth = 0;
                let revenueConfirmedCurrMonth = 0;
                if (
                  compareAsc(startOfMonth(month), startOfMonth(new Date())) ===
                  0
                ) {
                  // Cấu hình tháng hiện tại cho doanh thu hoàn thành
                  revenueCompletedCurrMonth =
                    revenuesCurrentMonths?.reduce((total, curr) => {
                      if (
                        curr.status === OrderStatus.COMPLETED &&
                        curr.merchantId === merchantId &&
                        curr.userId === user.id
                      )
                        return total + Number(curr.totalRevenue || 0);
                      return total;
                    }, 0) || 0;
                  const configCurrMonthComplete = configMerchat?.find(
                    (x) =>
                      x.min <= revenueCompletedCurrMonth &&
                      x.max >= revenueCompletedCurrMonth,
                  );
                  commissionCompletedCurrMonth =
                    configCurrMonthComplete?.valueType ===
                    CommissionValueType.AMOUNT
                      ? Number(configCurrMonthComplete?.value || 0)
                      : (revenueCompletedCurrMonth *
                          Number(configCurrMonthComplete?.value || 0)) /
                        100;
                  // Cấu hình tháng hiện tại cho doanh thu chờ đối soát
                  revenueConfirmedCurrMonth =
                    revenuesCurrentMonths?.reduce((total, curr) => {
                      if (
                        curr.status === OrderStatus.CONFIRMED &&
                        curr.merchantId === merchantId &&
                        curr.userId === user.id
                      )
                        return total + Number(curr.totalRevenue || 0);
                      return total;
                    }, 0) || 0;
                  const configCurrMonthConfirm = configMerchat?.find(
                    (x) =>
                      x.min <= revenueConfirmedCurrMonth &&
                      x.max >= revenueConfirmedCurrMonth,
                  );
                  commissionConfirmedCurrMonth =
                    configCurrMonthConfirm?.valueType ===
                    CommissionValueType.AMOUNT
                      ? Number(configCurrMonthConfirm?.value || 0)
                      : (revenueConfirmedCurrMonth *
                          Number(configCurrMonthConfirm?.value || 0)) /
                        100;
                  if (dto?.merchantId === merchantId)
                    revenueCurrentMonth = {
                      userId: user.id,
                      user: user,
                      merchantId: merchantId,
                      revenueStatistic: [
                        {
                          userId: user.id,
                          totalRevenueCompleted: revenueCompletedCurrMonth,
                          totalCommissionCompleted:
                            commissionCompletedCurrMonth,
                          totalCommission: commissionConfirmedCurrMonth,
                        },
                      ],
                    };
                }
                revenueStatisticMonth.userId = user.id;
                revenueStatisticMonth.user = user;
                revenueStatisticMonth.revenueStatistic = [];
                revenueStatisticMonth.revenueStatistic.push({
                  userId: user.id,
                  totalRevenueCompleted:
                    Number(revenueMonth?.totalRevenueCompleted || 0) +
                    revenueCompletedCurrMonth,
                  totalCommissionCompleted:
                    commissionMonth + commissionCompletedCurrMonth,
                  totalCommission:
                    commissionMonth + commissionConfirmedCurrMonth,
                });
                revenueStatisticMonth.merchantId = merchantId;
                revenueStatisticMonths.push(revenueStatisticMonth);
              }),
            );
          }),
        );
      }),
    );
    revenueProducts?.map((revenueProduct) => {
      const revenue = revenueStatisticMonths?.filter(
        (r) => r.userId === revenueProduct.userId,
      ) || [
        {
          userId: users?.[0]?.id,
          user: users?.[0],
          revenueStatistic: [],
        },
      ];
      const revenueStatistic =
        revenue?.reduce((arr, curr) => {
          arr.push(...curr.revenueStatistic);
          return arr;
        }, []) || [];
      const totalRevenueCompleted = [
        ...revenueProduct?.revenueStatistic,
      ].reduce((total, curr) => {
        return total + Number(curr.totalRevenueCompleted || 0);
      }, 0);
      const totalCommissionCompleted = [
        ...revenueProduct?.revenueStatistic,
        ...revenueStatistic,
      ].reduce((total, curr) => {
        return total + Number(curr.totalCommissionCompleted || 0);
      }, 0);
      const totalCommission = [
        ...revenueProduct?.revenueStatistic,
        ...revenueStatistic,
      ].reduce((total, curr) => {
        return total + Number(curr.totalCommission || 0);
      }, 0);
      revenueProduct.revenueStatistic = [
        {
          userId: revenueProduct.userId,
          totalRevenueCompleted: totalRevenueCompleted,
          totalCommissionCompleted: totalCommissionCompleted,
          totalCommission: totalCommission,
        },
      ];
    });
    return revenueProducts;
  }

  /** Top CTV hoa hồng theo sản phẩm + hoa hồng theo doanh thu */
  async getTopUserCommissionNew(
    dto: FilterRevenueWithPaging,
  ): Promise<TopUserCommissionResponse> {
    // Doanh thu những tháng đã thống kê
    const query = this.orderRevenueRepo
      .createQueryBuilder('orderRevenue')
      .where('"orderRevenue"."status" IN(:...status)', {
        status: [OrderStatus.COMPLETED, OrderStatus.CONFIRMED],
      })
      .groupBy('"orderRevenue"."userId"')
      .addGroupBy('"orderRevenue"."status"')
      .select('"orderRevenue"."userId"')
      .addSelect('"orderRevenue"."status"')
      .addSelect('SUM("orderRevenue"."revenue") as "totalRevenue"')
      .addSelect(
        'SUM("orderRevenue"."productCommission") as "totalProductCommission"',
      )
      .orderBy('"totalProductCommission"', 'DESC', 'NULLS LAST');
    if (dto.userId && dto.userId > 0) {
      query.andWhere('"orderRevenue"."userId" = :userId', {
        userId: dto.userId,
      });
    }
    if (dto.merchantId && dto.merchantId > 0) {
      query.andWhere('"orderRevenue"."merchantId" = :merchantId', {
        merchantId: dto.merchantId,
      });
    }
    if (!dto.userId && dto.limit > 0 && dto.page > 0)
      query.offset((dto.page - 1) * dto.limit).limit(dto.limit);
    // Tính hoa hồng theo sản phẩm
    const datas = (await query.execute()) as {
      userId: number;
      status: OrderStatus;
      totalProductCommission: number;
      totalRevenue: number;
    }[];
    const revenueProducts: RevenueStatisticResponse[] = [];
    await Promise.all(
      datas?.map(async (d) => {
        if (d.status === OrderStatus.CONFIRMED) {
          const revenueUser = new RevenueStatisticResponse();
          revenueUser.userId = d.userId;
          revenueUser.user = await this.userService.repo.findOne({
            id: d.userId,
          });
          const totalCommissionCompleted =
            datas.find(
              (x) =>
                x.userId === d?.userId && x.status === OrderStatus.COMPLETED,
            )?.totalProductCommission || 0;

          const totalRevenueCompleted =
            datas.find(
              (x) =>
                x.userId === d?.userId && x.status === OrderStatus.COMPLETED,
            )?.totalRevenue || 0;
          revenueUser.revenueStatistic = [];
          revenueUser.revenueStatistic.push({
            userId: d?.userId,
            totalRevenueCompleted: totalRevenueCompleted,
            totalCommissionCompleted: totalCommissionCompleted,
            totalCommission: d?.totalProductCommission,
          });
          revenueProducts.push(revenueUser);
        }
      }),
    );
    // Tính hoa hồng theo doanh thu
    const userIds = datas?.map((data) => data.userId);
    const users = await this.userService.repo.find({
      where: {
        id: In(userIds),
      },
      relations: ['role', 'merchants'],
    });
    const merchantIds = users?.map((user) => {
      return user?.merchants?.map((merchant) => merchant.id) || [];
    });
    const userMechantIds = dto.merchantId
      ? [dto.merchantId]
      : [
          ...merchantIds.reduce((arr, curr) => {
            arr.push(...curr);
            return arr;
          }, []),
        ] || [];

    // Cấu hình thưởng hoa hồng của NCC
    const configMerchants = await this.configRepo.find({
      where: {
        merchantId: In(userMechantIds),
      },
    });

    // Thống kê doanh thu
    const revenueStatisticQuery = this.revenueStatisticRepo
      .createQueryBuilder('revenueStatistic')
      .leftJoinAndSelect('revenueStatistic.user', 'user')
      .leftJoinAndSelect('revenueStatistic.merchant', 'merchant')
      .where('"revenueStatistic"."date" BETWEEN :fromDate AND :toDate', {
        fromDate: startOfMonth(new Date(2022, 1, 1)),
        toDate: endOfMonth(new Date()),
      });
    // Doanh thu theo tháng hiện tại
    const startCurrMonth = startOfMonth(new Date());
    const endCurrMonth = endOfMonth(new Date());
    const revenueMonthsQuery = this.orderRevenueRepo
      .createQueryBuilder()
      .where('"status" IN(:...status)', {
        status: [OrderStatus.COMPLETED, OrderStatus.CONFIRMED],
      })
      .andWhere('"month" BETWEEN :fromDate AND :toDate', {
        fromDate: startCurrMonth,
        toDate: endCurrMonth,
      });
    if (userIds?.length > 0) {
      revenueStatisticQuery.andWhere(
        '"revenueStatistic"."userId" IN(:...userId)',
        {
          userId: userIds,
        },
      );
      revenueMonthsQuery.andWhere('"userId" IN(:...userId)', {
        userId: userIds,
      });
    }
    revenueMonthsQuery
      .groupBy('"userId"')
      .addGroupBy('"status"')
      .addGroupBy('"merchantId"')
      .select('"userId"')
      .addSelect('"status"')
      .addSelect('"merchantId"')
      .addSelect('SUM("revenue") as "totalRevenue"');
    const revenues = await revenueStatisticQuery.getMany();
    const revenuesCurrentMonths = (await revenueMonthsQuery.execute()) as {
      userId: number;
      status: OrderStatus;
      merchantId: number;
      totalRevenue: number;
    }[];
    // Thống kê theo tháng
    const revenueStatisticMonths: RevenueStatisticResponse[] = [];
    let revenueCurrentMonth = new RevenueStatisticResponse();
    await Promise.all(
      users?.map(async (user) => {
        await Promise.all(
          eachMonthOfInterval({
            start: startOfMonth(new Date(2022, 1, 1)),
            end: endOfMonth(new Date()),
          }).map((month) => {
            Promise.all(
              userMechantIds.map((merchantId) => {
                const revenueStatisticMonth = new RevenueStatisticResponse();
                const configMerchat = configMerchants?.find(
                  (cf) =>
                    cf.key === ConfigType.REVENUE &&
                    cf.startDate <= month &&
                    cf.endDate >= month &&
                    cf.merchantId === merchantId,
                )?.value as ConfigMerchantCommission[];
                // Tính các tháng trước
                const revenueMonth = revenues.find(
                  (x) =>
                    format(x.date, 'MM/yyyy') === format(month, 'MM/yyyy') &&
                    x.userId === user.id,
                );
                const configMonth = configMerchat?.find(
                  (x) =>
                    x.min <= revenueMonth?.totalRevenueCompleted &&
                    x.max >= revenueMonth?.totalRevenueCompleted,
                );
                const commissionMonth =
                  configMonth?.valueType === CommissionValueType.AMOUNT
                    ? Number(configMonth?.value || 0)
                    : (Number(revenueMonth?.totalRevenueCompleted || 0) *
                        Number(configMonth?.value || 0)) /
                      100;
                // Tính tháng hiện tại
                let revenueCompletedCurrMonth = 0;
                let commissionCompletedCurrMonth = 0;
                let commissionConfirmedCurrMonth = 0;
                let revenueConfirmedCurrMonth = 0;
                if (
                  compareAsc(startOfMonth(month), startOfMonth(new Date())) ===
                  0
                ) {
                  // Cấu hình tháng hiện tại cho doanh thu hoàn thành
                  revenueCompletedCurrMonth =
                    revenuesCurrentMonths?.reduce((total, curr) => {
                      if (
                        curr.status === OrderStatus.COMPLETED &&
                        curr.merchantId === merchantId &&
                        curr.userId === user.id
                      )
                        return total + Number(curr.totalRevenue || 0);
                      return total;
                    }, 0) || 0;
                  const configCurrMonthComplete = configMerchat?.find(
                    (x) =>
                      x.min <= revenueCompletedCurrMonth &&
                      x.max >= revenueCompletedCurrMonth,
                  );
                  commissionCompletedCurrMonth =
                    configCurrMonthComplete?.valueType ===
                    CommissionValueType.AMOUNT
                      ? Number(configCurrMonthComplete?.value || 0)
                      : (revenueCompletedCurrMonth *
                          Number(configCurrMonthComplete?.value || 0)) /
                        100;
                  // Cấu hình tháng hiện tại cho doanh thu chờ đối soát
                  revenueConfirmedCurrMonth =
                    revenuesCurrentMonths?.reduce((total, curr) => {
                      if (
                        curr.status === OrderStatus.CONFIRMED &&
                        curr.merchantId === merchantId &&
                        curr.userId === user.id
                      )
                        return total + Number(curr.totalRevenue || 0);
                      return total;
                    }, 0) || 0;
                  const configCurrMonthConfirm = configMerchat?.find(
                    (x) =>
                      x.min <= revenueConfirmedCurrMonth &&
                      x.max >= revenueConfirmedCurrMonth,
                  );
                  commissionConfirmedCurrMonth =
                    configCurrMonthConfirm?.valueType ===
                    CommissionValueType.AMOUNT
                      ? Number(configCurrMonthConfirm?.value || 0)
                      : (revenueConfirmedCurrMonth *
                          Number(configCurrMonthConfirm?.value || 0)) /
                        100;
                  if (dto?.merchantId === merchantId)
                    revenueCurrentMonth = {
                      userId: user.id,
                      user: user,
                      merchantId: merchantId,
                      revenueStatistic: [
                        {
                          userId: user.id,
                          totalRevenueCompleted: revenueCompletedCurrMonth || 0,
                          totalCommissionCompleted:
                            commissionCompletedCurrMonth || 0,
                          totalCommission: commissionConfirmedCurrMonth || 0,
                        },
                      ],
                    };
                }
                revenueStatisticMonth.userId = user.id;
                revenueStatisticMonth.user = user;
                revenueStatisticMonth.revenueStatistic = [];
                revenueStatisticMonth.revenueStatistic.push({
                  userId: user.id,
                  totalRevenueCompleted:
                    Number(revenueMonth?.totalRevenueCompleted || 0) +
                    revenueCompletedCurrMonth,
                  totalCommissionCompleted:
                    commissionMonth + commissionCompletedCurrMonth,
                  totalCommission:
                    commissionMonth + commissionConfirmedCurrMonth,
                });
                revenueStatisticMonth.merchantId = merchantId;
                revenueStatisticMonths.push(revenueStatisticMonth);
              }),
            );
          }),
        );
      }),
    );
    revenueProducts?.map((revenueProduct) => {
      const revenue = revenueStatisticMonths?.filter(
        (r) => r.userId === revenueProduct.userId,
      ) || [
        {
          userId: users?.[0]?.id,
          user: users?.[0],
          revenueStatistic: [],
        },
      ];
      const revenueStatistic =
        revenue?.reduce((arr, curr) => {
          arr.push(...curr.revenueStatistic);
          return arr;
        }, []) || [];
      const totalRevenueCompleted = [
        ...revenueProduct?.revenueStatistic,
      ].reduce((total, curr) => {
        return total + Number(curr.totalRevenueCompleted || 0);
      }, 0);
      const totalCommissionCompleted = [
        ...revenueProduct?.revenueStatistic,
        ...revenueStatistic,
      ].reduce((total, curr) => {
        return total + Number(curr.totalCommissionCompleted || 0);
      }, 0);
      const totalCommission = [
        ...revenueProduct?.revenueStatistic,
        ...revenueStatistic,
      ].reduce((total, curr) => {
        return total + Number(curr.totalCommission || 0);
      }, 0);
      revenueProduct.user = users?.find(
        (user) => user.id === revenueProduct.userId,
      );
      revenueProduct.revenueStatistic = [
        {
          userId: revenueProduct.userId,
          totalRevenueCompleted: Math.round(totalRevenueCompleted) || 0,
          totalCommissionCompleted: Math.round(totalCommissionCompleted) || 0,
          totalCommission: Math.round(totalCommission) || 0,
        },
      ];
      revenueProduct.totalCommissionRevenue = totalCommissionCompleted || 0;
    });
    const config = configMerchants?.find(
      (config) =>
        config.startDate <= startOfMonth(new Date()) &&
        config.endDate >= endOfMonth(new Date()) &&
        config.key === ConfigType.REVENUE,
    );
    return {
      revenues: sortBy(revenueProducts, 'totalCommissionRevenue'),
      configRevenue: dto.merchantId
        ? { ...config, value: sortBy(config?.value, 'min') }
        : null,
      revenueCurrentMonth: dto.merchantId ? revenueCurrentMonth : null,
    };
  }

  /** Cập nhật thông tin doanh thu đơn hàng */
  async updateOrderRevenue(
    order: OrderEntity,
    queryRunner?: QueryRunner,
  ): Promise<void> {
    if (!order?.orderProducts?.length || order?.orderProducts?.length < 1)
      return;
    const revenue = new OrderRevenueEntity();
    revenue.createdAt = new Date();
    revenue.updatedAt = new Date();

    switch (order.status) {
      case OrderStatus.CONFIRMED:
      case OrderStatus.COMPLETED:
        revenue.orderId = order.id;
        revenue.status = order.status;
        revenue.merchantId = order.merchantId;
        revenue.revenue = order.total;
        revenue.month = startOfMonth(new Date());
        revenue.userId = order.userId;
        revenue.revenue = order.total;
        revenue.productCommission =
          order.orderProducts?.reduce((total, curr) => {
            return total + curr.commission;
          }, 0) || 0;
        if (queryRunner) {
          await queryRunner.manager.save(OrderRevenueEntity, revenue);
        } else {
          await this.orderRevenueRepo.save(revenue);
        }
        break;
      case OrderStatus.CANCELLED:
      case OrderStatus.MERCHANT_CANCELLED:
      case OrderStatus.REFUNDED:
        const revenueRemoves = await this.orderRevenueRepo.find({
          where: {
            orderId: order.id,
          },
        });

        if (queryRunner) {
          await queryRunner.manager.softRemove(
            OrderRevenueEntity,
            revenueRemoves,
          );
        } else {
          await this.orderRevenueRepo.softRemove(revenueRemoves);
        }
        break;
      default:
        break;
    }
  }

  /** Tính thống kê tự động theo tháng (ngày 10 hàng tháng) */
  @Cron('0 0 10 * *')
  // @Cron(CronExpression.EVERY_10_SECONDS)
  async executeRevenueStatistic(month?: Date): Promise<any> {
    const startDate = month
      ? startOfMonth(month)
      : startOfMonth(addMonths(new Date(), -1));
    const endDate = month
      ? endOfMonth(month)
      : endOfMonth(addMonths(new Date(), -1));
    await this.revenueStatisticRepo.delete({ date: startOfMonth(startDate) });
    const query = this.orderRevenueRepo
      .createQueryBuilder('orderRevenue')
      .where('"orderRevenue"."status" = :status', {
        status: OrderStatus.COMPLETED,
      })
      .andWhere('"orderRevenue"."month" BETWEEN :fromDate AND :toDate', {
        fromDate: startDate,
        toDate: endDate,
      })
      .groupBy('"orderRevenue"."userId"')
      .addGroupBy('"orderRevenue"."merchantId"')
      .addGroupBy('date_trunc(\'day\', "orderRevenue"."month")')
      .select('"orderRevenue"."userId"')
      .addSelect('"orderRevenue"."merchantId"')
      .addSelect('date_trunc(\'day\', "orderRevenue"."month") as "date"')
      .addSelect('SUM("orderRevenue"."revenue") as "totalRevenueCompleted"')
      .addSelect(
        'SUM("orderRevenue"."productCommission") as "totalProductCommission"',
      );
    const revenueStatistics = await query.execute();
    const merchantIds = revenueStatistics
      .map((m) => m.merchantId)
      .filter((curr, idx, arr) => arr.indexOf(curr) === idx);

    const configs = await this.configRepo.find({
      where: {
        merchantId: In(merchantIds),
        key: ConfigType.REVENUE,
      },
    });
    let revenueStatisticUpdate = [];
    await Promise.all(
      (revenueStatisticUpdate = revenueStatistics.map((order) => {
        const config = configs?.find(
          (x) =>
            x.merchantId === order.merchantId &&
            x.startDate <= order.date &&
            x.endDate >= order.date,
        )?.value as ConfigMerchantCommission[];
        const commissionValue = config?.find(
          (x) =>
            x?.min <= Number(order.totalRevenueCompleted || 0) &&
            x?.max >= Number(order.totalRevenueCompleted || 0),
        );
        const _revenueStatistic = new RevenueStatisticEntity();
        _revenueStatistic.createdAt = startDate;
        _revenueStatistic.updatedAt = startDate;
        _revenueStatistic.userId = order.userId;
        _revenueStatistic.date = order.date;
        _revenueStatistic.merchantId = order.merchantId;
        _revenueStatistic.totalRevenueCompleted = Number(
          order.totalRevenueCompleted || 0,
        );
        _revenueStatistic.totalCommissionCompleted =
          commissionValue?.valueType === CommissionValueType.AMOUNT
            ? Number(commissionValue?.value || 0)
            : (Number(commissionValue?.value || 0) / 100) *
              Number(order.totalRevenueCompleted || 0);
        _revenueStatistic.totalProductCommission = Number(
          order.totalProductCommission || 0,
        );
        return _revenueStatistic;
      })),
    );
    await this.revenueStatisticRepo.save(revenueStatisticUpdate);
  }

  /** Tính thưởng theo số lượng sản phẩm bán được trong tháng*/
  async calculateBonusByProductQty(
    userId: number,
    month: Date,
    configs: ConfigMerchantCommission[],
  ) {
    const query = this.orderRevenueRepo
      .createQueryBuilder('orderRevenue')
      .leftJoin(
        'order_product',
        'orderProduct',
        '"orderRevenue"."orderId" = "orderProduct"."orderId"',
      )
      .where('"orderRevenue"."status" = :status', {
        status: OrderStatus.COMPLETED,
      })
      .andWhere('"orderRevenue"."userId" = :userId', {
        userId: userId,
      })
      .andWhere('"orderRevenue"."month" BETWEEN :fromDate AND :toDate', {
        fromDate: startOfMonth(month),
        toDate: endOfMonth(month),
      })
      .groupBy('"orderProduct"."productId"')
      .select('"orderProduct"."productId" as "productId"')
      .addSelect('COUNT("orderProduct"."productId") as "product_qty"');
    const data = (await query.execute()) as {
      productId: number;
      product_qty: number;
    }[];
    const totalBonus = data
      .map((m) => {
        const config = configs?.find(
          (x) =>
            x.productIds?.includes(m.productId.toString()) &&
            x.min <= m.product_qty &&
            x.max >= m.product_qty,
        );
        if (config?.value) {
          return config?.valueType === CommissionValueType.AMOUNT
            ? Number(config?.value || 0)
            : 0;
        } else {
          return 0;
        }
      })
      .reduce(function (total, curr) {
        return total + Number(curr || 0);
      }, 0);
    return Number(totalBonus || 0);
  }

  /** Tính hoa hồng giới thiệu */
  async calculateReferralCommission(
    dto: FilterRevenueWithPaging,
    user: UserEntity,
  ): Promise<GetManyDefaultResponse<RevenueReferralStatistic>> {
    const diffMonth = differenceInMonths(
      new Date(dto.toDate),
      new Date(dto.fromDate),
    );
    if (Math.abs(diffMonth) > 12) {
      throw new BadRequestException('Khoảng tìm kiếm lớn nhất là 12 tháng');
    }
    const referralQuery = this.userService.repo
      .createQueryBuilder('user')
      .leftJoin(
        'order_revenue',
        'orderRevenue',
        '"orderRevenue"."userId" = "user"."id"',
      )
      .leftJoin('order', 'order', '"order"."id" = "orderRevenue"."orderId"')
      .where('"orderRevenue"."status" = :status', {
        status: OrderStatus.COMPLETED,
      });

    if (user) {
      switch (user?.role?.type) {
        case RoleType.PARTNER:
          referralQuery.andWhere('"user"."referralParentId" = :userId', {
            userId: user.id,
          });
          break;
        case RoleType.ADMIN:
          if (dto?.field) {
            const userIds = (
              await this.userService.repo
                .createQueryBuilder('user')
                .where(`LOWER("user"."${dto.field}") LIKE :textSearch`, {
                  textSearch: '%' + dto?.textSearch + '%',
                })
                .getMany()
            )?.map((us) => us.id);
            if (userIds && userIds?.length > 0) {
              referralQuery.andWhere(
                '"user"."referralParentId" IN(:...userIds)',
                {
                  userIds: userIds,
                },
              );
            }
          }
          break;
        default:
          break;
      }
    }
    referralQuery
      .andWhere('"user"."referralBonusStart" BETWEEN :fromDate AND :toDate', {
        fromDate: startOfMonth(new Date(dto.fromDate)),
        toDate: endOfMonth(new Date(dto.toDate)),
      })
      .andWhere('"orderRevenue"."month" BETWEEN :fromDate AND :toDate', {
        fromDate: startOfMonth(new Date(dto.fromDate)),
        toDate: endOfMonth(new Date(dto.toDate)),
      })
      .groupBy('"orderRevenue"."status"')
      .addGroupBy('"orderRevenue"."month"')
      .addGroupBy('"orderRevenue"."merchantId"')
      .addGroupBy('"user"."id"')
      .addGroupBy('"user"."referralParentId"')
      .addGroupBy('"user"."referralBonusStart"')
      .addGroupBy('"user"."referralBonusExpire"')
      .addGroupBy('"user"."valueBonus"')
      .addGroupBy('"user"."valueBonusType"')
      .addGroupBy('"order"."configCommissionId"')
      .select('"orderRevenue"."status" as "status"')
      .addSelect('"orderRevenue"."month" as "month"')
      .addSelect('"orderRevenue"."merchantId" as "merchantId"')
      .addSelect('"user"."referralBonusStart" as "referralBonusStart"')
      .addSelect('"user"."referralBonusExpire" as "referralBonusExpire"')
      .addSelect('"user"."id" as "userId"')
      .addSelect('"user"."referralParentId" as "referralId"')
      .addSelect('"user"."valueBonus"')
      .addSelect('"user"."valueBonusType"')
      .addSelect('"order"."configCommissionId" as "orderConfigCommissionId"')
      .addSelect('SUM("orderRevenue"."revenue")', 'totalRevenue')
      .addSelect(
        'SUM("orderRevenue"."productCommission")',
        'totalCommissionRevenue',
      )
      .orderBy('"user"."referralBonusStart"', 'ASC', 'NULLS FIRST')
      .having('"user"."referralParentId" IS NOT NULL');
    const referralCommissions =
      ((await referralQuery.execute()) as ReferralCommissionSummary[]) || [];
    const referralIds = referralCommissions
      .map((m) => m.referralId)
      .filter((curr, idx, arr) => curr && arr.indexOf(curr) === idx);
    const total = referralIds?.length || 0;

    const response: RevenueReferralStatistic[] = [];
    await Promise.all(
      referralIds
        ?.slice((dto.page - 1) * dto.limit, dto.limit)
        ?.map(async (referralId) => {
          const revenueReferral = new RevenueReferralStatistic();
          revenueReferral.referral = await this.userService.repo.findOne({
            id: referralId,
          });
          revenueReferral.members = await this.userService.repo.count({
            referralParentId: referralId,
          });
          revenueReferral.statistic = [];
          await Promise.all(
            eachMonthOfInterval({
              start: startOfMonth(new Date(dto.fromDate)),
              end: endOfMonth(new Date(dto.toDate)),
            }).map(async (month) => {
              let totalReferralCommission = 0;
              // Hoa hồng nếu user phát sinh đơn đầu tiên trong khoảng thời gian thống kê
              const firstOrderOfUserQuery = this.orderRevenueRepo
                .createQueryBuilder('orderRevenue')
                .leftJoinAndSelect(
                  'orderRevenue.order',
                  'order',
                  '"orderRevenue"."orderId" = "order"."id"',
                )
                .where('"orderRevenue"."status" = :status', {
                  status: OrderStatus.COMPLETED,
                })
                .andWhere(
                  '"orderRevenue"."createdAt" BETWEEN :fromDate AND :endDate',
                  {
                    fromDate: startOfMonth(month),
                    endDate: endOfMonth(month),
                  },
                )
                .andWhere('"order"."configCommissionId" IS NOT NULL')
                .andWhere('"orderRevenue"."userId" = :userId', {
                  userId: referralId,
                })
                .select('"orderRevenue"."orderId"')
                .addSelect('"orderRevenue"."createdAt"')
                .addSelect('"order"."configCommissionId"');
              const firstOrderOfUser = await firstOrderOfUserQuery.execute();

              // Nếu người dùng có đơn đầu tiên trong tháng thống kê thì tính hoa hồng phát sinh đơn đầu tiên và dký thành công
              if (firstOrderOfUser?.length > 0) {
                const config = await this.configRepo.find({
                  where: {
                    key: In([ConfigType.FIRSTORDER, ConfigType.REGISTER]),
                    startDate: LessThanOrEqual(
                      firstOrderOfUser?.[0]?.createdAt,
                    ),
                    endDate: MoreThanOrEqual(firstOrderOfUser?.[0]?.createdAt),
                  },
                });
                totalReferralCommission +=
                  Number(
                    (
                      config.find((x) => x.key === ConfigType.REGISTER)
                        ?.value as ReferralCommission
                    )?.f0 || 0,
                  ) +
                  Number(
                    (
                      config.find((x) => x.key === ConfigType.FIRSTORDER)
                        ?.value as ReferralCommission
                    )?.f1 || 0,
                  );
              }

              // Danh sách người được giới thiệu trong tháng
              const detailReferralCommissions: DetailRefrralStatistic[] = [];
              if (referralCommissions?.length > 0) {
                const referralCommissionUser = referralCommissions?.filter(
                  (x) =>
                    format(x.month, 'MM/yyyy') === format(month, 'MM/yyyy') &&
                    x.referralId === referralId,
                );
                // Danh sách id người được giới thiệu
                const userIds = referralCommissionUser
                  ?.map((m) => m.userId)
                  ?.filter((val, idx, arr) => arr.indexOf(val) === idx);
                // Tính hoa hồng theo doanh thu của người được giới thiệu
                await Promise.all(
                  userIds?.map(async (userId) => {
                    const detailReferralCommission =
                      new DetailRefrralStatistic();
                    detailReferralCommission.user =
                      await this.userService.repo.findOne({
                        id: userId,
                      });
                    // Kiểm tra xem người được giới thiệu có phát sinh đơn đầu tiên không
                    const isFirstOrder = referralCommissionUser?.find(
                      (x) =>
                        x.userId === userId &&
                        Number(x.orderConfigCommissionId || 0) > 0,
                    );
                    if (isFirstOrder) {
                      const config = (
                        await this.configRepo.findOne({
                          id: isFirstOrder.orderConfigCommissionId,
                        })
                      )?.value as ReferralCommission;
                      totalReferralCommission += Number(config?.f0 || 0);
                      detailReferralCommission.firstOrderCommission = Number(
                        config?.f0 || 0,
                      );
                    } else {
                      detailReferralCommission.firstOrderCommission = 0;
                    }
                    // Tổng doanh thu phát sinh trong tháng và nằm trong khoảng thời gian được hưởng hoa hồng giới thiệu
                    const commissionUsers = referralCommissionUser.filter(
                      (x) => x.userId === userId,
                    );
                    const merchantIds = commissionUsers.reduce(
                      (arr: number[], curr) => {
                        if (!arr.includes(curr.merchantId)) {
                          arr.push(curr.merchantId);
                        }
                        return arr;
                      },
                      [],
                    );
                    let sumCommission = 0;
                    if (
                      detailReferralCommission.user?.valueBonusType ===
                      CommissionValueType.AMOUNT
                    ) {
                      sumCommission += Number(
                        detailReferralCommission.user?.valueBonus || 0,
                      );
                    } else {
                      await Promise.all(
                        merchantIds?.map(async (merchantId) => {
                          const configMerchant = (
                            await this.configRepo.findOne({
                              where: {
                                merchantId: merchantId,
                                key: ConfigType.REVENUE,
                                startDate: LessThanOrEqual(month),
                                endDate: MoreThanOrEqual(month),
                              },
                            })
                          )?.value as ConfigMerchantCommission[];
                          const sumRevenueMerchant =
                            commissionUsers
                              .filter((x) => x.merchantId === merchantId)
                              .reduce((total, curr) => {
                                return total + Number(curr.totalRevenue);
                              }, 0) || 0;
                          const configMerchantValue = configMerchant?.find(
                            (x) =>
                              x.max >= sumRevenueMerchant &&
                              x.min <= sumRevenueMerchant,
                          );
                          const commission =
                            configMerchantValue?.valueType ===
                            CommissionValueType.AMOUNT
                              ? Number(configMerchantValue?.value || 0)
                              : (Number(configMerchantValue?.value || 0) /
                                  100) *
                                sumRevenueMerchant;
                          sumCommission +=
                            (Number(
                              detailReferralCommission.user?.valueBonus || 0,
                            ) /
                              100) *
                            commission;
                        }),
                      );
                    }
                    totalReferralCommission += sumCommission;
                    detailReferralCommission.revenueCommission = sumCommission;
                    detailReferralCommission.totalCommission =
                      Number(
                        detailReferralCommission.firstOrderCommission || 0,
                      ) + sumCommission;
                    detailReferralCommissions.push(detailReferralCommission);
                  }),
                );
              }

              const revenueStatistic = new RevenueStatistic();
              revenueStatistic.userId = referralId;
              revenueStatistic.date = month;
              revenueStatistic.totalReferralCommission =
                totalReferralCommission;
              // revenueStatistic.detailReferralCommissions =
              //   detailReferralCommissions;
              revenueReferral.statistic.push(revenueStatistic);
            }),
          );
          revenueReferral.totalCommission = revenueReferral.statistic.reduce(
            (total, curr) => {
              return total + curr.totalReferralCommission;
            },
            0,
          );
          revenueReferral.statistic = _.sortBy(
            revenueReferral.statistic,
            'date',
          );
          response.push(revenueReferral);
        }),
    );
    // return response;
    return {
      data: response,
      count: response?.length || 0,
      total: total,
      page: Number(dto.page),
      pageCount: Math.ceil(total / Number(dto.limit || 1)),
    };
  }

  /** Chi tiết hoa hồng giới thiệu theo */
  async detailReferralCommission(
    dto: FilterRevenueWithPaging,
    user: UserEntity,
  ): Promise<DetailRefrralStatistic[]> {
    const response: DetailRefrralStatistic[] = [];
    const referralId = Number(dto.userId);
    const month = new Date(dto.fromDate);
    const referralQuery = this.userService.repo
      .createQueryBuilder('user')
      .leftJoin(
        'order_revenue',
        'orderRevenue',
        '"orderRevenue"."userId" = "user"."id"',
      )
      .leftJoin('order', 'order', '"order"."id" = "orderRevenue"."orderId"')
      .where('"orderRevenue"."status" = :status', {
        status: OrderStatus.COMPLETED,
      })
      .andWhere('"user"."referralParentId" = :referralId', {
        referralId: referralId,
      })
      .andWhere(
        ':date BETWEEN "user"."referralBonusStart" AND "user"."referralBonusExpire"',
        {
          date: startOfMonth(month),
        },
      )
      .andWhere('"orderRevenue"."month" BETWEEN :fromDate AND :toDate', {
        fromDate: startOfMonth(new Date(dto.fromDate)),
        toDate: endOfMonth(new Date(dto.toDate)),
      })
      .groupBy('"orderRevenue"."status"')
      .addGroupBy('"orderRevenue"."month"')
      .addGroupBy('"orderRevenue"."merchantId"')
      .addGroupBy('"user"."id"')
      .addGroupBy('"user"."referralParentId"')
      .addGroupBy('"user"."referralBonusStart"')
      .addGroupBy('"user"."referralBonusExpire"')
      .addGroupBy('"user"."valueBonus"')
      .addGroupBy('"user"."valueBonusType"')
      .addGroupBy('"order"."configCommissionId"')
      .select('"orderRevenue"."status" as "status"')
      .addSelect('"orderRevenue"."month" as "month"')
      .addSelect('"orderRevenue"."merchantId" as "merchantId"')
      .addSelect('"user"."referralBonusStart" as "referralBonusStart"')
      .addSelect('"user"."referralBonusExpire" as "referralBonusExpire"')
      .addSelect('"user"."id" as "userId"')
      .addSelect('"user"."referralParentId" as "referralId"')
      .addSelect('"user"."valueBonus"')
      .addSelect('"user"."valueBonusType"')
      .addSelect('"order"."configCommissionId" as "orderConfigCommissionId"')
      .addSelect('SUM("orderRevenue"."revenue")', 'totalRevenue')
      .addSelect(
        'SUM("orderRevenue"."productCommission")',
        'totalCommissionRevenue',
      )
      .orderBy('"user"."referralBonusStart"', 'ASC', 'NULLS FIRST');
    const referralCommissions =
      ((await referralQuery.execute()) as ReferralCommissionSummary[]) || [];
    let totalReferralCommission = 0;
    // Hoa hồng nếu user phát sinh đơn đầu tiên trong khoảng thời gian thống kê
    const firstOrderOfUserQuery = this.orderRevenueRepo
      .createQueryBuilder('orderRevenue')
      .leftJoinAndSelect(
        'orderRevenue.order',
        'order',
        '"orderRevenue"."orderId" = "order"."id"',
      )
      .where('"orderRevenue"."status" = :status', {
        status: OrderStatus.COMPLETED,
      })
      .andWhere('"orderRevenue"."createdAt" BETWEEN :fromDate AND :endDate', {
        fromDate: startOfMonth(month),
        endDate: endOfMonth(month),
      })
      .andWhere('"order"."configCommissionId" IS NOT NULL')
      .andWhere('"orderRevenue"."userId" = :userId', {
        userId: referralId,
      })
      .select('"orderRevenue"."orderId"')
      .addSelect('"orderRevenue"."createdAt"')
      .addSelect('"order"."configCommissionId"');
    const firstOrderOfUser = await firstOrderOfUserQuery.execute();

    // Nếu người dùng có đơn đầu tiên trong tháng thống kê thì tính hoa hồng phát sinh đơn đầu tiên và dký thành công
    if (firstOrderOfUser?.length > 0) {
      const config = await this.configRepo.find({
        where: {
          key: In([ConfigType.FIRSTORDER, ConfigType.REGISTER]),
          startDate: LessThanOrEqual(firstOrderOfUser?.[0]?.createdAt),
          endDate: MoreThanOrEqual(firstOrderOfUser?.[0]?.createdAt),
        },
      });
      totalReferralCommission +=
        Number(
          (
            config.find((x) => x.key === ConfigType.REGISTER)
              ?.value as ReferralCommission
          )?.f0 || 0,
        ) +
        Number(
          (
            config.find((x) => x.key === ConfigType.FIRSTORDER)
              ?.value as ReferralCommission
          )?.f1 || 0,
        );
    }

    // Danh sách người được giới thiệu trong tháng
    const detailReferralCommissions: DetailRefrralStatistic[] = [];
    if (referralCommissions?.length > 0) {
      const referralCommissionUser = referralCommissions?.filter(
        (x) =>
          format(x.month, 'MM/yyyy') === format(month, 'MM/yyyy') &&
          x.referralId === referralId,
      );
      // Danh sách id người được giới thiệu
      const userIds = referralCommissionUser
        ?.map((m) => m.userId)
        ?.filter((val, idx, arr) => arr.indexOf(val) === idx);
      // Tính hoa hồng theo doanh thu của người được giới thiệu
      await Promise.all(
        userIds?.map(async (userId) => {
          const detailReferralCommission = new DetailRefrralStatistic();
          detailReferralCommission.user = await this.userService.repo.findOne({
            id: userId,
          });
          // Kiểm tra xem người được giới thiệu có phát sinh đơn đầu tiên không
          const isFirstOrder = referralCommissionUser?.find(
            (x) =>
              x.userId === userId && Number(x.orderConfigCommissionId || 0) > 0,
          );
          if (isFirstOrder) {
            const config = (
              await this.configRepo.findOne({
                id: isFirstOrder.orderConfigCommissionId,
              })
            )?.value as ReferralCommission;
            totalReferralCommission += Number(config?.f0 || 0);
            detailReferralCommission.firstOrderCommission = Number(
              config?.f0 || 0,
            );
          } else {
            detailReferralCommission.firstOrderCommission = 0;
          }
          // Tổng doanh thu phát sinh trong tháng và nằm trong khoảng thời gian được hưởng hoa hồng giới thiệu
          const commissionUsers = referralCommissionUser.filter(
            (x) => x.userId === userId,
          );
          const merchantIds = commissionUsers.reduce((arr: number[], curr) => {
            if (!arr.includes(curr.merchantId)) {
              arr.push(curr.merchantId);
            }
            return arr;
          }, []);
          let sumCommission = 0;
          if (
            detailReferralCommission.user?.valueBonusType ===
            CommissionValueType.AMOUNT
          ) {
            sumCommission += Number(
              detailReferralCommission.user?.valueBonus || 0,
            );
          } else {
            await Promise.all(
              merchantIds?.map(async (merchantId) => {
                const configMerchant = (
                  await this.configRepo.findOne({
                    where: {
                      merchantId: merchantId,
                      key: ConfigType.REVENUE,
                      startDate: LessThanOrEqual(month),
                      endDate: MoreThanOrEqual(month),
                    },
                  })
                )?.value as ConfigMerchantCommission[];
                const sumRevenueMerchant =
                  commissionUsers
                    .filter((x) => x.merchantId === merchantId)
                    .reduce((total, curr) => {
                      return total + Number(curr.totalRevenue);
                    }, 0) || 0;
                const configMerchantValue = configMerchant?.find(
                  (x) =>
                    x.max >= sumRevenueMerchant && x.min <= sumRevenueMerchant,
                );
                const commission =
                  configMerchantValue?.valueType === CommissionValueType.AMOUNT
                    ? Number(configMerchantValue?.value || 0)
                    : (Number(configMerchantValue?.value || 0) / 100) *
                      sumRevenueMerchant;
                sumCommission +=
                  (Number(detailReferralCommission.user?.valueBonus || 0) /
                    100) *
                  commission;
              }),
            );
          }
          detailReferralCommission.revenueCommission = sumCommission;
          detailReferralCommission.totalCommission =
            Number(detailReferralCommission.firstOrderCommission || 0) +
            sumCommission;
          detailReferralCommissions.push(detailReferralCommission);
        }),
      );
    }
    response.push(...detailReferralCommissions);
    return response;
  }

  /** Chi tiết doanh thu theo userId */
  async getRevenueStatisticByUser(
    dto: RevenueStatisticByUserDto,
    user: UserEntity,
  ): Promise<RevenueStatisticByUserResponse[]> {
    const startDate = startOfDay(new Date(dto.fromDate));
    const endDate = endOfDay(new Date(dto.toDate));
    let orderRevenues = await this.orderRevenueRepo.find({
      where: {
        status: OrderStatus.COMPLETED,
        userId: user.id,
        createdAt: Between(startDate, endDate),
        merchantId: Raw((alias) => {
          return dto.merchantId ? `${alias} = ${dto.merchantId}` : '1=1';
        }),
      },
    });
    const response: RevenueStatisticByUserResponse[] = [];
    switch (dto.fillterType) {
      case FillterType.Day:
      case FillterType.Week:
      case FillterType.Month:
        eachDayOfInterval({ start: startDate, end: endDate }).map((day) => {
          const dayRevenue = new RevenueStatisticByUserResponse();
          dayRevenue.date = day;
          const revenues = orderRevenues.reduce((arr, orderRevenue) => {
            if (
              compareAsc(
                startOfDay(orderRevenue.createdAt),
                startOfDay(day),
              ) === 0
            ) {
              arr.push(orderRevenue);
            }
            return arr;
          }, []);
          dayRevenue.revenue = {
            status: OrderStatus.COMPLETED,
            count: revenues?.length || 0,
            sum:
              revenues?.reduce((total, curr) => {
                return total + Number(curr.revenue || 0);
              }, 0) || 0,
          };
          response.push(dayRevenue);
        });
        break;
      case FillterType.Quarter:
      case FillterType.Year:
        eachMonthOfInterval({ start: startDate, end: endDate }).map((month) => {
          const monthRevenue = new RevenueStatisticByUserResponse();
          monthRevenue.date = month;
          // const revenues = groupBy(orderRevenues, format(month, 'yyyy/MM'));
          const revenues = orderRevenues.reduce((arr, orderRevenue) => {
            if (
              compareAsc(
                startOfMonth(orderRevenue.createdAt),
                startOfMonth(month),
              ) >= 0 &&
              compareAsc(
                startOfMonth(orderRevenue.createdAt),
                endOfMonth(month),
              ) <= 0
            ) {
              arr.push(orderRevenue);
            }
            return arr;
          }, []);
          monthRevenue.revenue = {
            status: OrderStatus.COMPLETED,
            count: revenues?.length || 0,
            sum:
              revenues?.reduce((total, curr) => {
                return total + Number(curr.revenue || 0);
              }, 0) || 0,
          };
          response.push(monthRevenue);
        });
        break;
    }
    return response;
  }

  /** Biểu đồ doanh thu */
  async calculateRevenueChart(
    dto: FilterRevenueWithPaging,
    user: UserEntity,
  ): Promise<any[]> {
    const startMont = new Date(dto.fromDate);
    const endMont = new Date(dto.toDate);
    const ordersQuery = this.orderRepo
      .createQueryBuilder('order')
      .leftJoinAndSelect(
        'order_history',
        'orderHistory',
        `CASE WHEN "order"."status" = '${OrderStatus.COMPLETED}' AND "orderHistory"."status" = '${OrderStatus.COMPLETED}' AND "order"."id" = "orderHistory"."orderId" THEN 1 ` +
          `WHEN "order"."status" = '${OrderStatus.SHIPPED}' AND "orderHistory"."status" = '${OrderStatus.SHIPPED}' AND "order"."id" = "orderHistory"."orderId" THEN 1 ELSE 0 END = 1`,
      )
      .where('"order"."status" IN(:...status)', {
        status: [OrderStatus.COMPLETED, OrderStatus.SHIPPED],
      })
      .andWhere('"orderHistory"."status" IN(:...status)', {
        status: [OrderStatus.COMPLETED, OrderStatus.SHIPPED],
      })
      .andWhere('"orderHistory"."createdAt" BETWEEN :fromDate AND :toDate', {
        fromDate: startMont,
        toDate: endMont,
      });
    switch (user?.role?.type) {
      case RoleType.MERCHANT:
        ordersQuery.andWhere('"order"."merchantId" = :merchantId', {
          merchantId: user.merchantId,
        });
        break;
      case RoleType.PARTNER:
        ordersQuery.andWhere('"order"."userId" = :userId', {
          userId: user.id,
        });
        break;
      case RoleType.LEADER:
        ordersQuery
          .leftJoin(
            'user_leader',
            'leader',
            '"leader"."userId" = "order"."userId"',
          )
          .andWhere('"leader"."leaderId" = :leaderId', {
            leaderId: user.id,
          })
          .andWhere('"order"."merchantId" = :merchantId', {
            merchantId: user.merchantId,
          });
        break;
    }

    ordersQuery
      .groupBy('1')
      .select('date_trunc(\'day\', "orderHistory"."createdAt") as day')
      .addSelect('SUM("order"."total")');
    const revenues = await ordersQuery.execute();
    const response = [];
    eachDayOfInterval({
      start: startOfDay(startMont),
      end: endOfDay(endMont),
    }).map((day) => {
      const revenue = revenues.find((tmpRevenue) => {
        return compareAsc(startOfDay(tmpRevenue.day), startOfDay(day)) === 0;
      });
      response.push({
        date: format(day, 'd/M'),
        value: revenue ? Number(revenue.sum) : 0,
      });
    });
    return response;
  }

  /** Top 10 sản phẩm bán chạy */
  async getTopProductSale(
    dto: FilterRevenueWithPaging,
    user: UserEntity,
  ): Promise<any[]> {
    const orderProductQuery = this.orderProRepo
      .createQueryBuilder('order_product')
      .leftJoin(OrderEntity, 'order', 'order.id = order_product.orderId')
      .leftJoin(
        ProductEntity,
        'product',
        'product.id = order_product.productId',
      )
      .where('order.status IN(:...status)', {
        status: [OrderStatus.COMPLETED, OrderStatus.SHIPPED],
      });

    switch (user?.role?.type) {
      case RoleType.MERCHANT:
      case RoleType.LEADER:
        orderProductQuery.andWhere('order.merchantId = :merchantId', {
          merchantId: user.merchantId,
        });
        break;
      case RoleType.PARTNER:
        orderProductQuery.andWhere('order.userId = :userId', {
          userId: user.id,
        });
        break;
    }

    orderProductQuery
      .andWhere('order.createdAt BETWEEN :fromDate AND :toDate', {
        fromDate: dto.fromDate,
        toDate: dto.toDate,
      })
      .groupBy('product.id')
      .select(
        'product.name, product.id, SUM("order_product"."quantity") as count',
      )
      .orderBy('count', 'DESC', 'NULLS LAST')
      .limit(10);

    // const totalProductSaleQuery = this.orderProRepo
    //   .createQueryBuilder('order_product')
    //   .leftJoin(OrderEntity, 'order', 'order.id = order_product.orderId')
    //   .where('order.status = :status', {
    //     status: OrderStatus.COMPLETED,
    //   });
    //
    // if (user?.roleId === 7) {
    //   totalProductSaleQuery.andWhere('order.merchantId = :merchantId', {
    //     merchantId: user.merchantId,
    //   });
    // } else {
    //   totalProductSaleQuery.andWhere('order.userId = :userId', {
    //     userId: user.id,
    //   });
    // }
    //
    // totalProductSaleQuery.andWhere(
    //   'order.createdAt BETWEEN :fromDate AND :toDate',
    //   {
    //     fromDate: dto.fromDate,
    //     toDate: dto.toDate,
    //   },
    // );
    const orderProducts = await orderProductQuery.execute();
    // const otherProductQuery = this.orderProRepo
    //   .createQueryBuilder('order_product')
    //   .leftJoin(OrderEntity, 'order', 'order.id = order_product.orderId')
    //   .where('order.status = :status', {
    //     status: OrderStatus.COMPLETED,
    //   });
    //
    // if (user?.roleId === 7) {
    //   orderProductQuery.andWhere('order.merchantId = :merchantId', {
    //     merchantId: user.merchantId,
    //   });
    // } else {
    //   orderProductQuery.andWhere('order.userId = :userId', {
    //     userId: user.id,
    //   });
    // }
    //
    // orderProductQuery.andWhere(
    //   'order.createdAt BETWEEN :fromDate AND :toDate',
    //   {
    //     fromDate: dto.fromDate,
    //     toDate: dto.toDate,
    //   },
    // );

    // if (orderProducts.length > 0) {
    //   otherProductQuery.andWhere(
    //     'order_product.productId NOT IN(:...productIds)',
    //     {
    //       productIds: orderProducts.map((product) => product.id),
    //     },
    //   );
    // }
    // const otherProductCount = await otherProductQuery.getCount();
    const data = orderProducts.map((product) => ({
      label: product.name,
      value: Number(product.count),
    }));
    // if (otherProductCount > 0) {
    //   data.push({ label: `Sản phẩm Khác`, value: otherProductCount });
    // }
    return data;
  }

  /** Tính toán hoa hồng */
  async calculateCommission(
    dto: FilterRevenueWithPaging,
    user: UserEntity,
  ): Promise<any[]> {
    const commissionQuery = await this.orderRepo
      .createQueryBuilder()
      .where('"userId" = :userId', {
        userId: user.id,
      })
      .andWhere('"createdAt" BETWEEN :fromDate AND :toDate', {
        fromDate: dto.fromDate,
        toDate: dto.toDate,
      })
      .groupBy('1')
      .select('date_trunc(\'day\', "createdAt") as day')
      .addSelect('SUM(total)');

    const commissions = (await commissionQuery.execute()) as {
      day: Date;
      sum: string;
    }[];
    const response = [];
    eachDayOfInterval({
      start: new Date(dto.fromDate),
      end: new Date(dto.toDate),
    }).map((day) => {
      const commission = commissions.find((tmpCommission) => {
        return tmpCommission.day.toString() === day.toString();
      });
      response.push({
        date: format(day, 'dd/MM'),
        'Doanh thu': commission ? Number(commission.sum) : 0,
      });
    });
    return response;
  }

  async getTopProductRevenueRate(
    dto: FilterRevenueWithPaging,
    user: UserEntity,
  ) {
    const query = this.orderRevenueRepo
      .createQueryBuilder('orderRevenue')
      .leftJoin(OrderEntity, 'order', 'order.id = orderRevenue.orderId')
      .leftJoin(
        OrderProductEntity,
        'orderProduct',
        'order.id = orderProduct.orderId',
      )
      .leftJoin(ProductEntity, 'product', 'product.id = orderProduct.productId')
      .where('order.status IN(:...status)', {
        status: [OrderStatus.COMPLETED, OrderStatus.SHIPPED],
      })
      .andWhere('order.createdAt BETWEEN :fromDate AND :toDate', {
        fromDate: new Date(dto.fromDate),
        toDate: new Date(dto.toDate),
      });
    switch (user?.role?.type) {
      case RoleType.MERCHANT:
      case RoleType.LEADER:
        query.andWhere('order.merchantId = :merchantId', {
          merchantId: user.merchantId,
        });
        break;
      case RoleType.PARTNER:
        query.andWhere('order.userId = :userId', {
          userId: user.id,
        });
        break;
    }
    query
      .select('SUM(order.total)', 'totalRevenue')
      .addSelect('orderProduct.productId', 'productId')
      .addSelect('product.name', 'productName')
      .addGroupBy('"productName"')
      .addGroupBy('"productId"')
      .orderBy('"totalRevenue"', 'DESC');
    const result = await query.getRawMany();
    const totalRevenueAllProduct =
      result?.reduce((total, curr) => {
        return total + Number(curr.totalRevenue || 0);
      }, 0) || 1;
    const top9Products = result?.slice(0, 9);
    const otherProductRevenue =
      totalRevenueAllProduct -
      top9Products?.reduce((total, curr) => {
        return total + Number(curr.totalRevenue || 0);
      }, 0);
    const data = top9Products?.map((product) => ({
      label: product.productName,
      value: Number(
        ((Number(product.totalRevenue) / totalRevenueAllProduct) * 100).toFixed(
          2,
        ),
      ),
    }));
    if (otherProductRevenue) {
      data.push({
        label: 'Sản phẩm khác',
        value: Number(
          ((otherProductRevenue / totalRevenueAllProduct) * 100).toFixed(2),
        ),
      });
    }
    return data;
  }
}

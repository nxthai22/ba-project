import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CrudController } from '@nestjsx/crud';
import { Crud } from 'decorators/crud.decorator';
import { ProductCategoryAttributeEntity } from 'entities/product-category-attribute.entity';
import { ProductCategoryAttributeService } from './product-category-attribute.service';
import { Auth } from 'decorators/auth.decorator';

@Crud({
  controller: 'product-category-attributes',
  name: 'Thuộc tính Sản phẩm',
  model: {
    type: ProductCategoryAttributeEntity,
  },
})
@Auth()
export class ProductCategoryAttributeController
  implements CrudController<ProductCategoryAttributeEntity>
{
  constructor(public readonly service: ProductCategoryAttributeService) {}
}

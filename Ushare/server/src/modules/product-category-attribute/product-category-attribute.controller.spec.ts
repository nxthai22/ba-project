import { Test, TestingModule } from '@nestjs/testing';
import { ProductCategoryAttributeController } from './product-category-attribute.controller';
import { ProductCategoryAttributeService } from './product-category-attribute.service';

describe('ProductCategoryAttributeController', () => {
  let controller: ProductCategoryAttributeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductCategoryAttributeController],
      providers: [ProductCategoryAttributeService],
    }).compile();

    controller = module.get<ProductCategoryAttributeController>(ProductCategoryAttributeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

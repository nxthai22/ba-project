import { Test, TestingModule } from '@nestjs/testing';
import { ProductCategoryAttributeService } from './product-category-attribute.service';

describe('ProductCategoryAttributeService', () => {
  let service: ProductCategoryAttributeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProductCategoryAttributeService],
    }).compile();

    service = module.get<ProductCategoryAttributeService>(ProductCategoryAttributeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ProductCategoryAttributeEntity } from 'entities/product-category-attribute.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ProductCategoryAttributeService extends TypeOrmCrudService<ProductCategoryAttributeEntity> {
  constructor(
    @InjectRepository(ProductCategoryAttributeEntity)
    public repo: Repository<ProductCategoryAttributeEntity>,
  ) {
    super(repo);
  }
}

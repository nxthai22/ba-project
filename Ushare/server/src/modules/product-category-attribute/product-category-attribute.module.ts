import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductCategoryAttributeEntity } from 'entities/product-category-attribute.entity';
import { ProductCategoryAttributeController } from './product-category-attribute.controller';
import { ProductCategoryAttributeService } from './product-category-attribute.service';
import { ProductCategoryAttributeValueEntity } from 'entities/product-category-attribute-value.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ProductCategoryAttributeEntity,
      ProductCategoryAttributeValueEntity,
    ]),
  ],
  controllers: [ProductCategoryAttributeController],
  providers: [ProductCategoryAttributeService],
  exports: [ProductCategoryAttributeService],
})
export class ProductCategoryAttributeModule {}

import { Controller } from '@nestjs/common';
import { PublicService } from './public.service';
import { ApiTags } from '@nestjs/swagger';
import { OrderService } from 'modules/order/order.service';
import { UserService } from 'modules/user/user.service';
import { ProductService } from 'modules/product/product.service';
import { ProductPostService } from 'modules/product-post/product-post.service';

@ApiTags('Public')
@Controller('public')
export class PublicController {
  constructor(
    private readonly service: PublicService,
    private readonly orderService: OrderService,
    private readonly userService: UserService,
    private readonly productService: ProductService,
    private readonly productPostService: ProductPostService,
  ) {}
}

import { Module } from '@nestjs/common';
import { PublicService } from './public.service';
import { PublicController } from './public.controller';
import { OrderModule } from 'modules/order/order.module';
import { UserModule } from 'modules/user/user.module';
import { ProductModule } from 'modules/product/product.module';
import { ProductPostModule } from 'modules/product-post/product-post.module';

@Module({
  imports: [OrderModule, UserModule, ProductModule, ProductPostModule],
  controllers: [PublicController],
  providers: [PublicService],
})
export class PublicModule {}

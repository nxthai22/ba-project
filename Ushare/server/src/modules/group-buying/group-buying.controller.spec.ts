import { Test, TestingModule } from '@nestjs/testing';
import { GroupBuyingController } from './group-buying.controller';

describe('GroupBuyingController', () => {
  let controller: GroupBuyingController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GroupBuyingController],
    }).compile();

    controller = module.get<GroupBuyingController>(GroupBuyingController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

import {
  BadRequestException,
  Get,
  OnModuleInit,
  Param,
  ParseIntPipe,
  UseInterceptors,
} from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { ApiOkResponse, ApiOperation, ApiParam } from '@nestjs/swagger';
import {
  CrudController,
  CrudRequest,
  CrudRequestInterceptor,
  ParsedRequest,
  GetManyDefaultResponse,
  Override,
} from '@nestjsx/crud';
import { Auth } from 'decorators/auth.decorator';
import { Crud } from 'decorators/crud.decorator';
import { OptionalAuth } from 'decorators/optional-auth.decorator';
import { User } from 'decorators/user.decorator';
import { GroupEntity } from 'entities/group.entity';
import { ORDER_STATUS_INVALID } from 'entities/order.entity';
import { UserEntity } from 'entities/user.entity';
import { OrderService } from 'modules/order/order.service';
import { In } from 'typeorm';
import { PaginatedGroupDto } from './dto/group-buying.dto';
import { GroupDto } from './dto/group.dto';
import { GroupBuyingService } from './group-buying.service';

import { GroupService } from './group.service';

@Crud({
  controller: 'groups',
  name: 'nhóm mua chung',
  serialize: {
    get: GroupDto,
  },
  model: {
    type: GroupEntity,
  },
  routes: {
    only: ['getOneBase'],
  },
  query: {
    join: {
      groupBuyingProduct: {},
      'groupBuyingProduct.groupBuyingCampaign': {},
      user: {
        allow: ['fullName', 'tel', 'avatar'],
      },
      orders: {
        allow: ['paymentStatus', 'status'],
      },
      'orders.user': {
        alias: 'userOrder',
        allow: ['fullName', 'tel', 'avatar'],
      },
    },
  },
  params: {
    productId: {
      type: 'number',
      disabled: true,
    },
  },
})
export class GroupController
  implements CrudController<GroupEntity>, OnModuleInit
{
  private orderService: OrderService;
  constructor(
    public readonly service: GroupService,
    private readonly groupBuyingService: GroupBuyingService,
    private moduleRef: ModuleRef,
  ) {}
  onModuleInit() {
    this.orderService = this.moduleRef.get(OrderService, { strict: false });
  }

  get base(): CrudController<GroupEntity> {
    return this;
  }

  @ApiOperation({
    summary: 'Lấy danh sách nhóm mua chung đang active theo sản phẩm',
  })
  @ApiOkResponse({
    type: PaginatedGroupDto,
  })
  @ApiParam({
    description: 'ID của sản phẩm',
    name: 'productId',
    type: Number,
  })
  @UseInterceptors(CrudRequestInterceptor)
  @Get('/product/:productId')
  @OptionalAuth()
  async getActiveGroupOfProduct(
    @ParsedRequest() req: CrudRequest,
    @Param('productId', ParseIntPipe) productId: number,
    @User() user: UserEntity,
  ): Promise<GetManyDefaultResponse<GroupEntity>> {
    const campaign = await this.groupBuyingService.findActiveCampaignOfProduct(
      productId,
    );

    if (!campaign) {
      return this.service.createPageInfo(
        [],
        0,
        req.parsed.limit,
        req.parsed.offset,
      );
    }

    req.parsed.join = [
      {
        field: 'groupBuyingProduct',
      },
      {
        field: 'groupBuyingProduct.groupBuyingCampaign',
      },
      {
        field: 'user',
      },
    ];
    req.parsed.search.$and.push({
      groupBuyingProductId: campaign.groupBuyingProducts[0].id,
    });

    if (req.parsed.sort.length === 0) {
      req.parsed.sort.push({
        field: 'remains',
        order: 'ASC',
      });
    }
    const groups = (await this.service.getMany(
      req,
    )) as GetManyDefaultResponse<GroupEntity>;

    // map user đã tham gia nhóm hay chưa
    const joinedGroup = {};
    const groupsIds = groups.data.map((gr) => gr.id);
    if (groupsIds.length > 0 && user) {
      const orders = await this.orderService.findActiveOrder({
        select: ['groupId'],
        where: {
          groupId: In(groupsIds),
          userId: user.id,
        },
      });
      orders.forEach((o) => (joinedGroup[o.groupId] = o.groupId));
    }
    groups.data.forEach((gr) => {
      gr.isJoined = joinedGroup[gr.id] ? true : false;
    });

    return groups;
  }

  @Override()
  async getOne(@ParsedRequest() req: CrudRequest): Promise<GroupDto> {
    req.parsed.join = [
      {
        field: 'groupBuyingProduct',
      },
      {
        field: 'groupBuyingProduct.groupBuyingCampaign',
      },
      {
        field: 'user',
      },
      {
        field: 'orders',
      },
      {
        field: 'orders.user',
      },
    ];
    const group = await this.base.getOneBase(req);
    group?.orders.forEach((order, idx) => {
      if (ORDER_STATUS_INVALID.includes(order.status)) {
        delete group.orders[idx];
      }
    });
    const [
      numberOfCancelOrder,
      numberOfDepositedOrder,
      totalOrder,
      numberOfWaitDeposit,
    ] = await Promise.all([
      this.orderService.countOrderCancelInGroup(group.id),
      this.orderService.countOrderDepositedInGroup(group.id),
      this.orderService.countOrderInGroup(group.id),
      this.orderService.countOrderWaitDepositInGroup(group.id),
    ]);
    return {
      ...group,
      numberOfCancelOrder,
      numberOfDepositedOrder,
      totalOrder,
      numberOfWaitDeposit,
    };
  }

  @ApiOperation({
    summary: 'Lấy số lượng group mà user đã join trong chiến dịch mua chung',
  })
  @ApiOkResponse({
    type: class CountJoinedGroupDto {
      count: number;
    },
  })
  @Get('/product/:productId/count-joined')
  @Auth()
  async countJoinedGroup(
    @Param('productId', ParseIntPipe) productId: number,
    @User() user: UserEntity,
  ) {
    const campaign = await this.groupBuyingService.findActiveCampaignOfProduct(
      productId,
    );
    if (!campaign) {
      throw new BadRequestException(
        'Sản phẩm hiện không có chương trình mua chung nào',
      );
    }
    return this.groupBuyingService
      .countJoinedGroupOfGBProduct(campaign.groupBuyingProducts[0].id, user.id)
      .then((count) => {
        return { count };
      });
  }
}

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { GroupEntity, GroupStatus } from 'entities/group.entity';
import { In, Repository } from 'typeorm';

@Injectable()
export class GroupService extends TypeOrmCrudService<GroupEntity> {
  constructor(
    @InjectRepository(GroupEntity)
    public repo: Repository<GroupEntity>,
  ) {
    super(repo);
  }

  findGroupActiveExpired() {
    return this.repo.find({
      join: {
        alias: 'group',
        innerJoin: {
          groupBuyingProduct: 'group.groupBuyingProduct',
        },
      },
      where: (qb) => {
        qb.where({
          status: In([GroupStatus.WAITING, GroupStatus.ACCEPTED]),
        }).andWhere(
          `"group"."createdAt"+ interval '1 hours' * "groupBuyingProduct"."timeToTeamUp" <= now()`,
        );
      },
    });
  }
}

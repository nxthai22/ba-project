import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  Logger,
  OnModuleInit,
} from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { Cron } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { IoTFleetHub } from 'aws-sdk';
import {
  GroupBuyingCampaignEntity,
  GroupBuyingCampaignStatus,
} from 'entities/group-buying-campaign.entity';
import { GroupBuyingProductEntity } from 'entities/group-buying-product.entity';
import { GroupBuyingVariantEntity } from 'entities/group-buying-variant.entity';
import { GroupEntity, GroupStatus } from 'entities/group.entity';
import { OrderEntity, ORDER_STATUS_INVALID } from 'entities/order.entity';
import { UserEntity } from 'entities/user.entity';
import { OrderPaymentStatus, OrderStatus } from 'enums';
import * as _ from 'lodash';
import { NotificationService } from 'modules/notification/notification.service';
import { OrderService } from 'modules/order/order.service';
import { ProductService } from 'modules/product/product.service';
import {
  EntityManager,
  In,
  LessThanOrEqual,
  MoreThan,
  Not,
  Repository,
  Transaction,
  TransactionManager,
} from 'typeorm';
import { CreateGroupBuyingDto } from './dto/create-group-buying.dto';
import { GroupService } from './group.service';

@Injectable()
export class GroupBuyingService extends TypeOrmCrudService<GroupBuyingCampaignEntity> {
  private readonly logger = new Logger(GroupBuyingService.name);

  constructor(
    @InjectRepository(GroupBuyingCampaignEntity)
    public repo: Repository<GroupBuyingCampaignEntity>,
    @InjectRepository(OrderEntity)
    public orderRepo: Repository<OrderEntity>,
    @InjectRepository(GroupBuyingProductEntity)
    public groupBuyingProductRepo: Repository<GroupBuyingProductEntity>,
    @InjectRepository(GroupBuyingVariantEntity)
    public groupBuyingVariantRepo: Repository<GroupBuyingVariantEntity>,
    @Inject(forwardRef(() => ProductService))
    private productService: ProductService,
    private groupService: GroupService,
    private notificationService: NotificationService,
  ) {
    super(repo);
  }

  async getOneForEachRole(
    id: number,
    user: UserEntity,
  ): Promise<GroupBuyingCampaignEntity> {
    const groupBuying = await this.repo.findOne(
      { id },
      {
        ...(user?.merchantId && { where: { merchantId: user?.merchantId } }),
        relations: [
          'groupBuyingProducts',
          'groupBuyingProducts.groupBuyingVariants',
        ],
      },
    );
    if (!groupBuying) {
      throw new BadRequestException('Cấu hình mua chung không tồn tại');
    }
    return groupBuying;
  }

  /** Thêm cấu hình mua chung */
  async createGroupBuying(
    dto: CreateGroupBuyingDto,
    user: UserEntity,
  ): Promise<GroupBuyingCampaignEntity> {
    const productsVariantImports = await this.checkProductInGroupBuying(
      dto,
      user,
    );
    const groupBuying: GroupBuyingCampaignEntity = {
      name: dto.name,
      startTime: dto.startTime,
      endTime: dto.endTime,
      merchantId: user.merchantId,
      groupBuyingProducts: productsVariantImports,
      productNumber: this.getNumberOfProduct(productsVariantImports),
    };
    return await this.repo.save(groupBuying);
  }

  async checkProductInGroupBuying(
    dto: CreateGroupBuyingDto,
    user: UserEntity,
  ): Promise<GroupBuyingProductEntity[]> {
    if (!dto.name || dto.name == '') {
      throw new BadRequestException('Tên chương trình không được bỏ trống');
    }
    if (!dto.startTime || new Date(dto.startTime) < new Date()) {
      throw new BadRequestException('Ngày bắt đầu không hợp lệ');
    }
    if (!dto.endTime || new Date(dto.endTime) < new Date()) {
      throw new BadRequestException('Ngày kết thúc không hợp lệ');
    }
    if (new Date(dto.endTime) < new Date(dto.startTime)) {
      throw new BadRequestException(
        'Ngày kết thúc không thể nhỏ hơn ngày bắt đầu',
      );
    }
    // check duplicate campaign's name
    const duplicatedCampaign = await this.repo.findOne({
      where: {
        name: dto.name,
        status: GroupBuyingCampaignStatus.ACTIVE,
      },
    });
    if (duplicatedCampaign) {
      throw new BadRequestException('Trùng tên với chương trình chưa kết thúc');
    }

    const groupBuyingProducts = dto.groupBuyingProducts;
    const productIds = groupBuyingProducts?.map(({ productId }) => productId);
    const productVariantIds = [];
    groupBuyingProducts?.forEach((prod) => {
      prod?.groupBuyingVariants?.forEach((variant) => {
        productVariantIds.push(variant.productVariantId);
      });
    });
    const uniqueProductIds = [...new Set(productIds)];
    const uniqueProductVariantIds = [...new Set(productVariantIds)];

    if (
      uniqueProductIds.length != productIds.length ||
      uniqueProductVariantIds.length != productVariantIds.length
    ) {
      throw new BadRequestException(
        'Sản phẩm có thể bị trùng lặp, vui lòng kiểm tra lại!',
      );
    }

    // check product is in another campaign
    const query = this.groupBuyingProductRepo
      .createQueryBuilder('groupBuyingProduct')
      .leftJoinAndSelect(
        'group_buying_campaign',
        'groupBuyingCampaign',
        '"groupBuyingCampaign"."id" = "groupBuyingProduct"."groupBuyingCampaignId"',
      )
      .where(
        '"groupBuyingCampaign"."startTime" < :newEndTime AND "groupBuyingCampaign"."endTime" > :newStartTime',
        {
          newStartTime: dto.startTime,
          newEndTime: dto.endTime,
        },
      )
      .andWhere('"groupBuyingProduct"."productId" IN (:...productIds)', {
        productIds: uniqueProductIds,
      })
      .select('"groupBuyingCampaign".*');
    const existedCampaign =
      ((await query.execute()) as GroupBuyingCampaignEntity[]) || [];
    if (existedCampaign && existedCampaign.length > 0) {
      throw new BadRequestException(
        'Sản phẩm đang áp dụng mua chung ở chương trình khác, vui lòng kiểm tra lại!',
      );
    }

    const variants = await this.productService.productVariantRepo.find({
      where: {
        id: In(uniqueProductVariantIds),
      },
      relations: ['product'],
    });
    const products = await this.productService.repo.find({
      where: {
        id: In(uniqueProductIds),
      },
      relations: ['stocks'],
    });

    // Kiểm tra số trạng thái của sản phẩm

    const productMerchantIds = products.map(({ merchantId }) => merchantId);
    const productVariantMerchantIds = variants.map(
      ({ product }) => product.merchantId,
    );

    const uniqueMerchantIds = [
      ...new Set([...productMerchantIds, ...productVariantMerchantIds]),
    ];

    if (
      uniqueMerchantIds.length > 1 ||
      uniqueMerchantIds[0] !== user?.merchantId
    ) {
      throw new BadRequestException('Sản phẩm đã chọn không cùng nhà cung cấp');
    }

    const productsVariantImports: GroupBuyingProductEntity[] = [];
    await Promise.all(
      groupBuyingProducts.map(async (element) => {
        const groupBuyingProduct = new GroupBuyingProductEntity();
        const product = products.find(
          (product) => product.id === element.productId,
        );
        if (product) {
          const maxQuantity = product?.stocks?.reduce(
            (total, stock) => total + stock.quantity,
            0,
          );
          if (element.maxQuantity > maxQuantity) {
            const product = await this.productService.repo.findOne({
              id: element.productId,
            });
            throw new BadRequestException(
              `Số lượng cho sản phẩm/biến thể ${product.name} vượt quá số lượng tồn kho hiện tại`,
            );
          }
          const groupBuyingVariants = element?.groupBuyingVariants?.map(
            (variant) => {
              const gbv = new GroupBuyingVariantEntity();
              gbv.productVariantId = variant.productVariantId;
              gbv.groupPrice = variant.groupPrice;
              gbv.maxQuantity = variant.maxQuantity;
              return gbv;
            },
          );
          groupBuyingProduct.groupBuyingVariants = groupBuyingVariants;
          groupBuyingProduct.productId = element.productId;
          groupBuyingProduct.groupPrice = element?.groupPrice;
          groupBuyingProduct.maxQuantity = element?.maxQuantity;
          groupBuyingProduct.maxQuantityPerUser = element.maxQuantityPerUser;
          groupBuyingProduct.minMemberNumber = element?.minMemberNumber || 0;
          groupBuyingProduct.timeToTeamUp = element?.timeToTeamUp;

          const groupBuyingPrice =
            this.findMinPriceOfGroupBuyingProduct(groupBuyingProduct);
          const discount = product.priceBeforeDiscount - groupBuyingPrice || 0;
          groupBuyingProduct.priceDiscount = discount > 0 ? discount : 0;
          productsVariantImports.push(groupBuyingProduct);
        } else {
          throw new BadRequestException(
            `Không tìm thấy Sản phẩm với ID: ${element?.productId}`,
          );
        }
      }),
    );
    return productsVariantImports;
  }

  getNumberOfProduct(productsVariantImports) {
    const productIds = productsVariantImports?.map(
      ({ productId }) => productId,
    );
    const uniqueProductIds = [...new Set(productIds)];
    return uniqueProductIds.length;
  }

  /** Cập nhật cấu hình mua chung */
  async updateGroupBuying(
    id: number,
    dto: CreateGroupBuyingDto,
    user: UserEntity,
  ): Promise<GroupBuyingCampaignEntity> {
    const groupBuying = await this.repo.findOne({
      where: {
        id,
        startTime: MoreThan(new Date()),
      },
      relations: ['groupBuyingProducts'],
    });
    if (!groupBuying) {
      throw new BadRequestException(
        'Không thể  cập nhật chương trình đã diễn ra/đã kết thúc',
      );
    }
    const productsVariantImports = await this.checkProductInGroupBuying(
      dto,
      user,
    );

    const updatedGroupBuying: GroupBuyingCampaignEntity = {
      name: dto.name,
      startTime: dto.startTime,
      endTime: dto.endTime,
      merchantId: user.merchantId,
      groupBuyingProducts: productsVariantImports,
      productNumber: this.getNumberOfProduct(productsVariantImports),
    };

    // remove
    await this.groupBuyingProductRepo.softRemove(
      groupBuying.groupBuyingProducts,
    );

    Object.assign(groupBuying, updatedGroupBuying);
    return await this.repo.save(groupBuying);
  }

  /** Xóa chương trình mua chung */
  async deleteGroupBuying(id: number, user: UserEntity) {
    const groupBuying = await this.repo.findOne({
      where: {
        id: id,
        startTime: MoreThan(new Date()),
      },
      relations: ['groupBuyingProducts'],
    });
    if (!groupBuying) {
      throw new BadRequestException(
        'Không thể xóa chương trình đã diễn ra/đã kết thúc',
      );
    }
    await this.repo.softDelete({ id: id });
    await this.groupBuyingProductRepo.softDelete({
      groupBuyingCampaignId: id,
    });
  }

  async findActiveCampaignOfProduct(
    productId: number,
    options = { relations: ['groupBuyingProducts'] },
  ): Promise<GroupBuyingCampaignEntity> {
    const now = new Date();
    return this.repo.findOne({
      relations: options.relations,
      join: {
        alias: 'groupBuyingCampaign',
        innerJoin: {
          groupBuyingProducts: 'groupBuyingCampaign.groupBuyingProducts',
        },
      },
      where: (qb) => {
        qb.where({
          startTime: LessThanOrEqual(now),
          endTime: MoreThan(now),
          status: GroupBuyingCampaignStatus.ACTIVE,
        }).andWhere('groupBuyingProducts.productId = :productId', {
          productId,
        });
      },
    });
  }

  async findAllActiveCampaignOfProduct(
    productIds: number[],
    options = { relations: ['groupBuyingProducts'] },
  ): Promise<GroupBuyingCampaignEntity[]> {
    console.log(productIds);
    const now = new Date();
    return this.repo.find({
      relations: options.relations,
      join: {
        alias: 'groupBuyingCampaign',
        innerJoin: {
          groupBuyingProducts: 'groupBuyingCampaign.groupBuyingProducts',
        },
      },
      where: (qb) => {
        qb.where({
          startTime: LessThanOrEqual(now),
          endTime: MoreThan(now),
        }).andWhere('groupBuyingProducts.productId IN (:...productIds) ', {
          productIds,
        });
      },
    });
  }

  findMinPriceOfGroupBuyingProduct(
    groupBuyingProduct: GroupBuyingProductEntity,
  ): number {
    if (groupBuyingProduct.groupBuyingVariants.length > 0) {
      return _.minBy(groupBuyingProduct.groupBuyingVariants, 'groupPrice')
        .groupPrice;
    }
    return groupBuyingProduct.groupPrice;
  }

  sumGroupBuyingStock(groupBuyingProduct: GroupBuyingProductEntity): number {
    if (groupBuyingProduct.groupBuyingVariants.length > 0) {
      let stock = 0;
      groupBuyingProduct.groupBuyingVariants.forEach((gbv) => {
        stock += gbv.maxQuantity - gbv.usedQuantity;
      });
      return stock;
    }
    return groupBuyingProduct.maxQuantity - groupBuyingProduct.usedQuantity;
  }

  @Transaction()
  async endGroupBuyingCampaign(
    groupBuyingCampaign: GroupBuyingCampaignEntity,
    status:
      | GroupBuyingCampaignStatus.MERCHANT_END
      | GroupBuyingCampaignStatus.EXPIRE_END,
    @TransactionManager() manager?: EntityManager,
  ) {
    groupBuyingCampaign.status = status;
    groupBuyingCampaign.endTime = new Date();
    await manager.save(GroupBuyingCampaignEntity, groupBuyingCampaign);

    const groupIds = [];
    groupBuyingCampaign.groupBuyingProducts.forEach((gbp) => {
      gbp.groups.forEach((g) => groupIds.push(g.id));
    });

    if (groupIds.length > 0) {
      const orders = await this.endGroupsTransaction(
        groupIds,
        status as unknown as GroupStatus.MERCHANT_END | GroupStatus.EXPIRE_END,
        manager,
      );
      // Notify orders change status
      this.notificationService.sendNotificationOfOrders(orders);
      return orders;
    }
  }

  async getActiveGroupBuyingProduct(
    productId: number,
  ): Promise<GroupBuyingProductEntity> {
    const query = this.groupBuyingProductRepo
      .createQueryBuilder('groupBuyingProduct')
      .leftJoin(
        'group_buying_campaign',
        'groupBuyingCampaign',
        '"groupBuyingCampaign"."id" = "groupBuyingProduct"."groupBuyingCampaignId"',
      )
      .where(
        '"groupBuyingCampaign"."startTime" < :now AND "groupBuyingCampaign"."endTime" > :now',
        {
          now: new Date(),
        },
      )
      .andWhere('"groupBuyingProduct"."productId" = :productId', {
        productId: productId,
      })
      .select('"groupBuyingProduct".*');
    const groupBuyingProducts =
      ((await query.execute()) as GroupBuyingProductEntity[]) || [];
    if (groupBuyingProducts && groupBuyingProducts.length > 0) {
      const groupBuyingProduct = groupBuyingProducts[0];
      return groupBuyingProduct;
    }
    return null;
  }

  async getActiveGroupBuyingVariant(
    productId: number,
    variantId: number,
  ): Promise<GroupBuyingVariantEntity> {
    const query = this.groupBuyingVariantRepo
      .createQueryBuilder('groupBuyingVariant')
      .leftJoin(
        'group_buying_product',
        'groupBuyingProduct',
        '"groupBuyingProduct"."id" = "groupBuyingVariant"."groupBuyingProductId"',
      )
      .leftJoin(
        'group_buying_campaign',
        'groupBuyingCampaign',
        '"groupBuyingCampaign"."id" = "groupBuyingProduct"."groupBuyingCampaignId"',
      )
      .where(
        '"groupBuyingCampaign"."startTime" < :now AND "groupBuyingCampaign"."endTime" > :now',
        {
          now: new Date(),
        },
      )
      .andWhere('"groupBuyingProduct"."productId" = :productId', {
        productId: productId,
      })
      .andWhere('"groupBuyingVariant"."productVariantId" = :productVariantId', {
        productVariantId: variantId,
      })
      .select('"groupBuyingVariant".*');
    const groupBuyingVariants =
      ((await query.execute()) as GroupBuyingVariantEntity[]) || [];
    if (groupBuyingVariants && groupBuyingVariants.length > 0) {
      const groupBuyingVariant = groupBuyingVariants[0];
      return groupBuyingVariant;
    }
    return null;
  }

  async updateProductUsedInCampaign(
    productId: number,
    variantId: number,
    quantityChange: number,
    manager?: EntityManager,
  ) {
    const groupBuyingProduct = await this.getActiveGroupBuyingProduct(
      productId,
    );
    if (groupBuyingProduct) {
      if (variantId) {
        const groupBuyingVariant = await this.getActiveGroupBuyingVariant(
          productId,
          variantId,
        );
        if (groupBuyingVariant) {
          await manager?.update(
            GroupBuyingVariantEntity,
            { id: groupBuyingVariant.id },
            {
              usedQuantity: groupBuyingVariant.maxQuantity - quantityChange,
            },
          );
        } else {
          await manager?.update(
            GroupBuyingProductEntity,
            { id: groupBuyingProduct.id },
            {
              usedQuantity: groupBuyingProduct.maxQuantity - quantityChange,
            },
          );
        }
      } else {
        await manager?.update(
          GroupBuyingProductEntity,
          { id: groupBuyingProduct.id },
          {
            usedQuantity: groupBuyingProduct.maxQuantity - quantityChange,
          },
        );
      }
    }
  }

  async countJoinedGroupOfGBProduct(
    groupBuyingProductId: number,
    userId: number,
  ) {
    return this.orderRepo.count({
      join: {
        alias: 'orders',
        innerJoin: {
          group: 'orders.group',
        },
      },
      where: (qb) => {
        qb.where({
          userId,
          status: Not(In(ORDER_STATUS_INVALID)),
        }).andWhere('group.groupBuyingProductId = :groupBuyingProductId', {
          groupBuyingProductId,
        });
      },
    });
  }

  /**
   * Tính tổng số user đã tham gia chương trình mua chung của sản phẩm
   * @param groupBuyingProductId
   */
  async countUserJoinedGBProduct(groupBuyingProductId: number) {
    const { count } = await this.orderRepo
      .createQueryBuilder('orders')
      .select('COUNT(DISTINCT("orders"."userId"))')
      .innerJoin('group', 'group', '"orders"."groupId" = group.id')
      .where('"group"."groupBuyingProductId" = :groupBuyingProductId', {
        groupBuyingProductId,
      })
      .getRawOne();
    return Number(count);
  }

  async endGroupsTransaction(
    groupIds: number[],
    status: GroupStatus.MERCHANT_END | GroupStatus.EXPIRE_END,
    manager: EntityManager,
  ) {
    await manager.update(
      GroupEntity,
      {
        status: In([GroupStatus.WAITING, GroupStatus.ACCEPTED]),
        id: In(groupIds),
      },
      { status: status as unknown as GroupStatus },
    );

    const cancelReason = {
      [GroupBuyingCampaignStatus.MERCHANT_END]:
        'Người mua chưa cọc tại thời điểm Shop kết thúc chương trình mua chung',
      [GroupBuyingCampaignStatus.EXPIRE_END]:
        'Người mua chưa cọc tại thời điểm chung trình hết thời gian',
    }[status];
    const orderCancelStatus = {
      [GroupBuyingCampaignStatus.MERCHANT_END]: OrderStatus.MERCHANT_CANCELLED,
      [GroupBuyingCampaignStatus.EXPIRE_END]: OrderStatus.CANCELLED,
    }[status];

    return await manager
      .createQueryBuilder()
      .update(OrderEntity)
      .set({
        status: orderCancelStatus,
        cancel_reason: cancelReason,
      })
      .where(
        '"groupId" IN (:...groupIds) and "paymentStatus" = :paymentStatus',
        {
          groupIds: groupIds,
          paymentStatus: OrderPaymentStatus.WAIT_DEPOSIT,
        },
      )
      .returning('*')
      .execute()
      .then((rs) => rs.raw as OrderEntity[]);
  }

  @Transaction()
  async findAndEndGroupActiveExpired(
    @TransactionManager() manager?: EntityManager,
  ) {
    const groups = await this.groupService.findGroupActiveExpired();
    const groupIds = groups.map((group) => group.id);
    if (groupIds.length > 0) {
      const orders = await this.endGroupsTransaction(
        groupIds,
        GroupStatus.EXPIRE_END,
        manager,
      );
      // notify orders change status
      this.notificationService.sendNotificationOfOrders(orders);
      return orders;
    }
  }

  @Cron('01 * * * * *')
  cronEndExpiredGroupBuying() {
    this.doCronEndExpiredGroupBuying().catch((e) => {
      this.logger.error({
        message: e.message,
        method: 'cronEndExpiredGroupBuying',
      });
    });
  }

  async doCronEndExpiredGroupBuying() {
    //find chương trình mua chung hết hạn
    const campaigns = await this.findGBCampaignActiveExpired();
    for (const campaign of campaigns) {
      await this.endGroupBuyingCampaign(
        campaign,
        GroupBuyingCampaignStatus.EXPIRE_END,
      );
    }

    //find group hết thời gian timeToTeamUp
    await this.findAndEndGroupActiveExpired();
  }

  findGBCampaignActiveExpired() {
    const now = new Date();
    return this.repo.find({
      where: {
        status: GroupBuyingCampaignStatus.ACTIVE,
        endTime: LessThanOrEqual(now),
      },
      relations: ['groupBuyingProducts', 'groupBuyingProducts.groups'],
    });
  }
}

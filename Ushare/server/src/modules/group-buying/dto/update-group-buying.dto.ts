import { IntersectionType, PickType } from '@nestjs/swagger';
import { GroupBuyingCampaignEntity } from 'entities/group-buying-campaign.entity';
import { CreateGroupBuyingDto } from './create-group-buying.dto';

export class UpdateGroupBuyingDto extends IntersectionType(
  PickType(GroupBuyingCampaignEntity, ['id']),
  CreateGroupBuyingDto,
) {}

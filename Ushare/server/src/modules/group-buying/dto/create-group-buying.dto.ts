import { ApiProperty, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber, Min, ValidateNested } from 'class-validator';
import { GroupBuyingCampaignEntity } from 'entities/group-buying-campaign.entity';
import { GroupBuyingProductEntity } from 'entities/group-buying-product.entity';
import { GroupBuyingVariantEntity } from 'entities/group-buying-variant.entity';

export class GroupBuyingVariantDto extends PickType(GroupBuyingVariantEntity, [
  'groupPrice',
  'maxQuantity',
  'productVariantId',
]) {}

export class GroupBuyingProductDto extends PickType(GroupBuyingProductEntity, [
  'groupPrice',
  'maxQuantity',
  'maxQuantityPerUser',
  'timeToTeamUp',
  'minMemberNumber',
  'maxGroupPerUser',
  'productId',
]) {
  @ApiProperty({
    description: 'Danh sách biến thể sản phẩm mua chung',
    type: [GroupBuyingVariantDto],
  })
  groupBuyingVariants: GroupBuyingVariantDto[];
}

export class CreateGroupBuyingDto extends PickType(GroupBuyingCampaignEntity, [
  'name',
  'startTime',
  'endTime',
]) {
  @ValidateNested({ each: true })
  @Type(() => GroupBuyingProductDto)
  @ApiProperty({
    description: 'Danh sách sản phẩm mua chung',
    type: [GroupBuyingProductDto],
  })
  groupBuyingProducts: GroupBuyingProductDto[];
}

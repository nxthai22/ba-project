import { ApiProperty } from '@nestjsx/crud/lib/crud';
import { GroupEntity } from 'entities/group.entity';

export class GroupDto extends GroupEntity {
  @ApiProperty({
    description: 'số đơn hủy trong nhóm',
  })
  numberOfCancelOrder: number;

  @ApiProperty({
    description: 'số đơn đã cọc trong nhóm',
  })
  numberOfDepositedOrder: number;

  @ApiProperty({
    description: 'tổng số đơn trong nhóm',
  })
  totalOrder: number;

  @ApiProperty({
    description: 'số đơn chưa cọc trong nhóm',
  })
  numberOfWaitDeposit: number;
}

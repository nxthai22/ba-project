import { ApiProperty } from '@nestjs/swagger';
import { GroupEntity } from 'entities/group.entity';
import { AttributeItemValue } from 'entities/product-variant.entity';
import { SearchCriteria } from 'modules/base/base-search';
import { PaginatedResponseDto } from 'modules/base/pagination.dto';

/** Tìm kiếm */
export class GroupBuyingFilterWithPaging {
  @ApiProperty({
    description: 'Tìm kiếm từ ngày',
    required: false,
  })
  fromDate?: Date;

  @ApiProperty({
    description: 'Tìm kiếm đến ngày',
    required: false,
  })
  toDate?: Date;

  @ApiProperty({
    description: 'Số lượng record/trang',
    type: Number,
    required: true,
    default: 10,
  })
  limit: number;

  @ApiProperty({
    description: 'Trang',
    type: Number,
    required: true,
    default: 1,
  })
  page: number;
}

export class GroupBuyingFilterDto extends SearchCriteria {
  @ApiProperty({
    description: 'Ngày có chương trình Flash sale',
    default: new Date(),
  })
  date: Date;

  @ApiProperty({
    description: 'Tìm kiếm từ ngày',
  })
  startTime?: Date;

  @ApiProperty({
    description: 'Tìm kiếm đến ngày',
  })
  endTime?: Date;

  @ApiProperty({
    description: 'Id chương trình Flash sale',
  })
  groupBuyingId?: number;

  @ApiProperty({
    description: 'Id danh mục sản phẩm',
  })
  productCategoryId?: number;

  @ApiProperty({
    description: 'Id tỉnh/thành phố',
  })
  provinceId?: number;
}

/** Sản phẩm trong chương trình GroupBuying */
export class ProductGroupBuying {
  @ApiProperty({
    description: 'Id sản phẩm',
  })
  productId: number;

  @ApiProperty({
    description: 'Tên sản phẩm',
  })
  productName: string;

  @ApiProperty({
    description: 'Giá sản phẩm',
  })
  productPrice: number;

  @ApiProperty({
    description: 'Giá biến thể sau giảm giá flash sale',
  })
  productPriceAfterDiscount: number;

  @ApiProperty({
    description: 'Ảnh sản phẩm',
  })
  productImages: string[];

  @ApiProperty({
    description: 'Danh sách biến thể',
  })
  variants: VariantGroupBuying[];
}

/** Biến thể trong chương trình GroupBuying */
export class VariantGroupBuying {
  @ApiProperty({
    description: 'Id biến thể',
  })
  variantId: number;

  @ApiProperty({
    description: 'Tên biến thể',
  })
  variantName: string;

  @ApiProperty({
    description: 'Giá trị biến thể',
  })
  variantPrice: number;

  @ApiProperty({
    description: 'Ảnh biến thể',
  })
  variantImages: string[];

  @ApiProperty({
    description: 'Giá biến thể sau giảm giá flash sale',
  })
  variantPriceAfterDiscount: number;

  @ApiProperty({
    description: 'Thuộc tính biến thể',
  })
  variantAttributeValue: AttributeItemValue[];
}

export class PaginatedGroupDto extends PaginatedResponseDto(GroupEntity) {}

import {
  BadRequestException,
  Delete,
  HttpCode,
  NotFoundException,
  Param,
  ParseIntPipe,
  Patch,
} from '@nestjs/common';
import {
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiProperty,
} from '@nestjs/swagger';
import {
  CrudController,
  CrudRequest,
  GetManyDefaultResponse,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { Auth } from 'decorators/auth.decorator';
import { Crud } from 'decorators/crud.decorator';
import { User } from 'decorators/user.decorator';
import {
  GroupBuyingCampaignEntity,
  GroupBuyingCampaignStatus,
} from 'entities/group-buying-campaign.entity';
import { UserEntity } from 'entities/user.entity';
import { CreateGroupBuyingDto } from './dto/create-group-buying.dto';
import { UpdateGroupBuyingDto } from './dto/update-group-buying.dto';
import { GroupBuyingService } from './group-buying.service';

export class GroupBuyingMerchant
  implements GetManyDefaultResponse<GroupBuyingCampaignEntity>
{
  @ApiProperty({
    description: 'count',
  })
  count: number;
  @ApiProperty({
    description: 'data',
    type: [GroupBuyingCampaignEntity],
  })
  data: GroupBuyingCampaignEntity[];
  @ApiProperty({
    description: 'page',
  })
  page: number;
  @ApiProperty({
    description: 'pageCount',
  })
  pageCount: number;
  @ApiProperty({
    description: 'total',
  })
  total: number;
}

@Crud({
  controller: 'group-buying',
  name: 'Mua chung',
  routes: {
    include: ['deleteOneBase'],
  },
  query: {
    join: {
      groupBuyingProducts: {},
      'groupBuyingProducts.groupBuyingVariants': {
        alias: 'groupBuyingVariants',
      },
    },
  },
  model: {
    type: GroupBuyingCampaignEntity,
  },
  dto: {
    create: CreateGroupBuyingDto,
    update: UpdateGroupBuyingDto,
    replace: UpdateGroupBuyingDto,
  },
})
export class GroupBuyingController
  implements CrudController<GroupBuyingCampaignEntity>
{
  constructor(public readonly service: GroupBuyingService) {}

  get base(): CrudController<GroupBuyingCampaignEntity> {
    return this;
  }

  @Override()
  @Auth()
  @HttpCode(200)
  async getOne(
    @Param('id') id: number,
    @ParsedRequest() req: CrudRequest,
    @User() user: UserEntity,
  ) {
    return await this.service.getOneForEachRole(id, user);
  }

  @Override()
  @Auth()
  @HttpCode(200)
  async createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: CreateGroupBuyingDto,
    @User() user: UserEntity,
  ) {
    return this.service.createGroupBuying(dto, user);
  }

  @Override()
  @Auth()
  @HttpCode(200)
  async updateOne(
    @Param('id') id: number,
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: UpdateGroupBuyingDto,
    @User() user: UserEntity,
  ) {
    return this.service.updateGroupBuying(id, dto, user);
  }

  @Override()
  @Auth()
  async deleteOne(
    @Param('id') id: number,
    @ParsedRequest() req: CrudRequest,
    @User() user: UserEntity,
  ): Promise<void> {
    return await this.service.deleteGroupBuying(id, user);
  }

  @Override()
  @Auth()
  async getMany(@ParsedRequest() req: CrudRequest, @User() user: UserEntity) {
    if (user?.merchantId) {
      req.parsed.search.$and.push({
        merchantId: {
          $eq: user?.merchantId,
        },
      });
    }
    return await this.base.getManyBase(req);
  }

  @ApiOperation({
    summary: 'Kết thúc chương trình mua chung',
  })
  @ApiOkResponse({
    type: String,
  })
  @Patch(':id/end')
  @Auth()
  async endCampaign(
    @Param('id', ParseIntPipe) id: number,
    @User() user: UserEntity,
  ): Promise<void> {
    const campaign = await this.service.repo.findOne(id, {
      relations: ['groupBuyingProducts', 'groupBuyingProducts.groups'],
      where: {
        merchantId: user.merchantId,
      },
    });
    if (!campaign) {
      throw new NotFoundException('Chương trình mua chung không tồn tại');
    }
    const now = new Date();
    if (campaign.startTime > now || campaign.endTime < now) {
      throw new BadRequestException(
        'Chỉ có thể kết thúc chương trình đang diễn ra',
      );
    }
    if (
      campaign.status === GroupBuyingCampaignStatus.EXPIRE_END ||
      campaign.status === GroupBuyingCampaignStatus.MERCHANT_END
    ) {
      throw new BadRequestException('Chương trình đã kết thúc');
    }

    await this.service.endGroupBuyingCampaign(
      campaign,
      GroupBuyingCampaignStatus.MERCHANT_END,
    );
  }
}

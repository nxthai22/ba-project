import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GroupBuyingCampaignEntity } from 'entities/group-buying-campaign.entity';
import { GroupBuyingProductEntity } from 'entities/group-buying-product.entity';
import { GroupBuyingVariantEntity } from 'entities/group-buying-variant.entity';
import { GroupEntity } from 'entities/group.entity';
import { OrderEntity } from 'entities/order.entity';
import { NotificationModule } from 'modules/notification/notification.module';
import { ProductModule } from 'modules/product/product.module';
import { GroupBuyingController } from './group-buying.controller';
import { GroupBuyingService } from './group-buying.service';
import { GroupController } from './group.controller';
import { GroupService } from './group.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      GroupBuyingCampaignEntity,
      GroupBuyingProductEntity,
      GroupBuyingVariantEntity,
      GroupEntity,
      OrderEntity,
    ]),
    forwardRef(() => ProductModule),
    NotificationModule,
  ],
  controllers: [GroupBuyingController, GroupController],
  providers: [GroupBuyingService, GroupService],
  exports: [GroupBuyingService, GroupService],
})
export class GroupBuyingModule {}

import { Test, TestingModule } from '@nestjs/testing';
import { ProductPostService } from './product-post.service';

describe('ProductPostService', () => {
  let service: ProductPostService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProductPostService],
    }).compile();

    service = module.get<ProductPostService>(ProductPostService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ProductPostEntity } from 'entities/product-post.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ProductEntity } from 'entities/product.entity';

@Injectable()
export class ProductPostService extends TypeOrmCrudService<ProductPostEntity> {
  constructor(
    @InjectRepository(ProductPostEntity)
    public repo: Repository<ProductPostEntity>,
    @InjectRepository(ProductEntity)
    public productRepo: Repository<ProductEntity>,
  ) {
    super(repo);
  }
}

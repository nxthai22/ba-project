import { Module } from '@nestjs/common';
import { ProductPostService } from './product-post.service';
import { ProductPostController } from './product-post.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductPostEntity } from 'entities/product-post.entity';
import { ProductEntity } from 'entities/product.entity';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    TypeOrmModule.forFeature([ProductPostEntity, ProductEntity]),
    HttpModule,
  ],
  controllers: [ProductPostController],
  providers: [ProductPostService],
  exports: [ProductPostService],
})
export class ProductPostModule {}

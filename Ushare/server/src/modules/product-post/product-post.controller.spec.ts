import { Test, TestingModule } from '@nestjs/testing';
import { ProductPostController } from './product-post.controller';
import { ProductPostService } from './product-post.service';

describe('ProductPostController', () => {
  let controller: ProductPostController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductPostController],
      providers: [ProductPostService],
    }).compile();

    controller = module.get<ProductPostController>(ProductPostController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

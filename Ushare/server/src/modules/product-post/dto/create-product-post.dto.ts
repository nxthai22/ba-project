import { OmitType, PickType } from '@nestjs/swagger';
import { ProductPostEntity } from 'entities/product-post.entity';

export class CreateProductPostDto extends PickType(ProductPostEntity, [
  'productId',
  'title',
  'content',
  'media',
  'status',
]) {}

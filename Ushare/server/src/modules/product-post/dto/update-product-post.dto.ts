import { IntersectionType, PickType } from '@nestjs/swagger';
import { CreateProductPostDto } from 'modules/product-post/dto/create-product-post.dto';
import { ProductPostEntity } from 'entities/product-post.entity';

export class UpdateProductPostDto extends IntersectionType(
  PickType(ProductPostEntity, ['id']),
  CreateProductPostDto,
) {}

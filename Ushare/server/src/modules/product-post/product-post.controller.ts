import { ProductPostService } from './product-post.service';
import {
  CrudController,
  CrudRequest,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import {
  ProductPostEntity,
  ProductPostStatus,
} from 'entities/product-post.entity';
import { Crud } from 'decorators/crud.decorator';
import { CreateProductPostDto } from 'modules/product-post/dto/create-product-post.dto';
import { Auth } from 'decorators/auth.decorator';
import { User } from 'decorators/user.decorator';
import { UserEntity } from 'entities/user.entity';
import { ApiBody } from '@nestjs/swagger';
import { Body, Post } from '@nestjs/common';
import { createReadStream, readdirSync } from 'fs';
import { join } from 'path';
import { HttpService } from '@nestjs/axios';
import { lastValueFrom } from 'rxjs';
import { UpdateProductPostDto } from 'modules/product-post/dto/update-product-post.dto';
import { RoleType } from 'entities/role.entity';

const FormData = require('form-data');

@Crud({
  model: {
    type: ProductPostEntity,
  },
  name: 'Bài đăng',
  controller: 'product-post',
  dto: {
    create: CreateProductPostDto,
    update: CreateProductPostDto,
    replace: CreateProductPostDto,
  },
  query: {
    join: {
      product: {},
      createdBy: {},
      modifiedBy: {},
    },
  },
  routes: {
    include: ['deleteOneBase'],
    updateOneBase: {
      decorators: [Auth()],
    },
    deleteOneBase: {
      decorators: [Auth()],
    },
    createManyBase: {
      decorators: [Auth()],
    },
    createOneBase: {
      decorators: [Auth()],
    },
  },
})
export class ProductPostController
  implements CrudController<ProductPostEntity>
{
  constructor(
    public readonly service: ProductPostService,
    public httpService: HttpService,
  ) {}

  get base(): CrudController<ProductPostEntity> {
    return this;
  }

  @Override()
  async getMany(@ParsedRequest() req: CrudRequest, @User() user: UserEntity) {
    if (user?.role?.type === RoleType.PARTNER)
      req.parsed.search.$and.push({
        status: {
          $eq: ProductPostStatus.PUBLISHED,
        },
      });
    if(req.parsed.sort?.length == 0){
      req.parsed.sort.push({
        field: 'createdAt',
        order: 'DESC',
      });
    }
    return this.base.getManyBase(req);
  }

  @Override()
  @ApiBody({
    type: CreateProductPostDto,
  })
  @Auth()
  async createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: CreateProductPostDto,
    @User() user: UserEntity,
  ) {
    // @ts-ignore
    return this.base.createOneBase(req, {
      ...dto,
      modifiedById: user.id,
      createdById: user.id,
    });
  }

  @Override()
  @ApiBody({
    type: UpdateProductPostDto,
  })
  @Auth()
  async updateOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: CreateProductPostDto,
    @User() user: UserEntity,
  ) {
    // @ts-ignore
    return this.base.updateOneBase(req, { ...dto, modifiedById: user.id });
  }

  @Post('add-content')
  @ApiBody({})
  @Auth()
  async addContent(@Body() dtos: any[]) {
    const path = join(__dirname, '../../../src/data/HASUTA');
    const folders = readdirSync(path);
    return Promise.all(
      dtos.map(async (dto) => {
        let product = null;
        if (Number(dto.merchantId)) {
          product = await this.service.productRepo.findOne({
            merchantProductId: Number(dto.merchantId),
          });
        }
        const newProductPost = new ProductPostEntity();
        newProductPost.productId = product?.id;
        newProductPost.content = dto.content;
        newProductPost.status = ProductPostStatus.PUBLISHED;
        newProductPost.createdById = 1;
        newProductPost.modifiedById = 1;

        let mediaFolder;
        folders.map((folder) => {
          const folderParts = folder.split('_');
          if (dto.id === Number(folderParts[0])) {
            mediaFolder = folder;
          }
        });
        // const medias =
        if (mediaFolder) {
          const files = readdirSync(path + '/' + mediaFolder);
          const fmData = new FormData();
          files.map((file) => {
            fmData.append(
              'files',
              createReadStream(path + '/' + mediaFolder + '/' + file),
              file,
            );
          });
          const upload = await lastValueFrom(
            this.httpService.post(
              'https://api.ushare.com.vn/cdn/upload',
              fmData,
              {
                headers: {
                  'Content-Type':
                    'multipart/form-data; boundary=' + fmData.getBoundary(),
                  Authorization:
                    'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZWwiOiIwOTY5NTQyMzMzIiwicm9sZUlkIjoxLCJmdWxsTmFtZSI6IkhvYW5nTlEiLCJwZXJtaXNzaW9ucyI6WyJwcm9kdWN0c19nZXRPbmVCYXNlIiwicHJvZHVjdHNfY3JlYXRlT25lQmFzZSIsInByb2R1Y3RzX2dldE1hbnlCYXNlIl0sInN1YiI6MSwiaWF0IjoxNjM1NDI2NzY3LCJleHAiOjE2MzgwMTg3NjcsImlzcyI6Imh0dHA6Ly91c2hhcmUudm4ifQ.2x4un7K4K7eyXGcYhVmKBQx68y66czfuRa79KKlSwZw',
                },
              },
            ),
          );
          newProductPost.media = upload?.data?.files;
        }
        return newProductPost;
      }),
    ).then((posts) => {
      return this.service.repo.save(posts);
    });
  }
}

import {
  CrudController,
  CrudRequest,
  GetManyDefaultResponse,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { Crud } from 'decorators/crud.decorator';
import { UserEntity } from 'entities/user.entity';
import { UserService } from './user.service';
import { Auth } from 'decorators/auth.decorator';
import {
  BadRequestException,
  Body,
  ForbiddenException,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ApiBody, ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { UserBonus, UserBonusFilterDto } from './dto/user-bonus.dto';
import { User } from 'decorators/user.decorator';
import { DeviceTokenEntity } from 'entities/device-token.entity';
import { RoleType } from 'entities/role.entity';
import { AddDeviceTokenDto } from 'modules/user/dto/add-device-token.dto';
import { UpdateAvatarDto } from './dto/update-avatar.dto';
import { ChangePasswordDto, UpdatePaymentInfoDto } from './dto/update-user.dto';
import { JwtService } from '@nestjs/jwt';
import { Not } from 'typeorm';

const bcrypt = require('bcrypt');

@Crud({
  name: 'Thành viên',
  controller: 'users',
  model: {
    type: UserEntity,
  },
  query: {
    exclude: ['password'],
    join: {
      ward: {},
      'ward.district': {
        alias: 'district',
      },
      'ward.district.province': {
        alias: 'province',
      },
      parent: {},
      referral: {},
      role: {},
      products: {},
      wallets: {},
      'wallets.histories': {},
      'wallets.transactions': {},
      walletHistories: {},
      walletTransactions: {},
      merchant: {},
      leaders: {
        eager: true,
        allow: ['fullName'],
        alias: 'leaders',
      },
      merchants: {
        eager: true,
        allow: ['name'],
      },
      bank: {},
      customers: {},
      'customers.addresses': {},
    },
  },
})
// @CrudAuth({
//   property: 'user',
//   persist: (user: UserEntity) => {
//     if (user?.role?.type === RoleType.MERCHANT) {
//       return {
//         merchantId: user.merchantId,
//         merchants: [
//           {
//             id: user.merchantId,
//           },
//         ],
//       };
//     }
//   },
//   filter: (user: UserEntity) => {
//     if (user?.role?.type === RoleType.MERCHANT) {
//       return {
//         'merchants.id': user.merchantId,
//         id: {
//           $ne: user.id,
//         },
//       };
//     }
//     if (user?.role?.type === RoleType.LEADER) {
//       return {
//         'merchants.id': user.merchantId,
//         'leaders.id': user.id,
//       };
//     }
//   },
// or: (user: UserEntity) => {
//   if (user?.role?.type === RoleType.MERCHANT) {
//     return {
//       merchantId: user.merchantId,
//       id: {
//         $ne: user.id,
//       },
//     };
//   }
// },
// })
@Auth()
export class UserController implements CrudController<UserEntity> {
  constructor(
    public readonly service: UserService,
    private jwtService: JwtService,
  ) {}

  get base(): CrudController<UserEntity> {
    return this;
  }

  @Override()
  async createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: UserEntity,
    @User() user: UserEntity,
  ) {
    const phoneRegExp = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/;
    if (!dto.tel.match(phoneRegExp)) {
      throw new BadRequestException(
        `Số điện thoại ${dto.tel} không đúng định dạng`,
      );
    }

    const checkUser = await this.service.repo.findOne({
      tel: dto.tel,
    });
    if (checkUser) {
      throw new BadRequestException(`Số điện thoại ${dto.tel} đã được đăng ký`);
    }
    let merchantId = 0;
    switch (user?.role?.type) {
      case RoleType.MERCHANT:
        merchantId = user.merchantId;
        break;
      case RoleType.ADMIN:
        merchantId = dto.merchantId;
        break;
    }
    const role = await this.service.roleRepo.findOne({ id: dto.roleId });
    if (
      [RoleType.PARTNER, RoleType.LEADER].includes(role?.type) &&
      merchantId
    ) {
      const merchant = await this.service.merchantService.repo.findOne({
        id: merchantId,
      });
      dto.merchants = [
        {
          ...merchant,
          id: merchantId,
        },
      ];
    }
    return this.base.createOneBase(req, dto);
  }

  @Override()
  @Auth()
  @ApiOperation({ summary: 'Sửa thành viên' })
  async updateOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: UserEntity,
    @User() user: UserEntity,
  ) {
    if (user?.role?.type === RoleType.PARTNER) {
      const partner = await this.service.repo.findOne({ id: user.id });
      partner.avatar = dto.avatar;
      return this.service.repo.save(partner);
    }

    // REVIEW: luôn kiểm tra số điện thoại được đk bất kể trong dto có hay ko?
    const checkTelUser = await this.service.repo.findOne({
      where: {
        tel: dto.tel,
        id: Not(dto.id),
      },
    });
    if (checkTelUser) {
      throw new BadRequestException(`Số điện thoại ${dto.tel} đã được đăng ký`);
    }
    if (user?.role?.type === RoleType.MERCHANT && !user.merchantId) {
      throw new ForbiddenException(
        'Người dùng không thuộc 1 Nhà phân phối nào nên không thể sửa user!',
      );
    }

    // REVIEW: Check sdt được đăng ký trước khi check sđt đúng định dạng ?
    const phoneRegExp = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/;
    if (!dto.tel.match(phoneRegExp)) {
      throw new BadRequestException(
        `Số điện thoại ${dto.tel} không đúng định dạng`,
      );
    }
    const userId = req.parsed.paramsFilter.find(
      (filter) => filter.field === 'id',
    )?.value;

    const userData = await this.service.findOne({
      where: {
        id: userId,
        merchantId: user.merchantId,
      },
      relations: ['leaders', 'merchants'],
    });
    const existLeaders = [];
    userData?.leaders
      ?.filter((leader) => leader.merchantId !== user.merchantId)
      ?.map((leader) => {
        existLeaders.push({
          id: leader.id,
        });
      });
    dto.leaders = [
      ...dto.leaders,
      //Giữ lại leader không phải của nhà cung cấp
      ...existLeaders,
    ] as unknown as UserEntity[];
    return this.base.updateOneBase(req, dto);
  }

  @Get('/top-bonus')
  @ApiOperation({
    summary: 'Danh sách top hoa hồng từ doanh thu F1',
  })
  @ApiOkResponse({
    type: UserBonus,
  })
  async TopBonus(
    @Query() dto: UserBonusFilterDto,
    @User() user: UserEntity,
  ): Promise<UserBonus> {
    return this.service.getTopBonusForUser(dto, user);
  }

  @Post('/device-token')
  @ApiOperation({
    summary: 'Thêm Firebase Messaging token',
  })
  @ApiOkResponse({
    type: DeviceTokenEntity,
  })
  @ApiBody({
    type: AddDeviceTokenDto,
  })
  async addDeviceToken(
    @Body() dto: AddDeviceTokenDto,
    @User() user: UserEntity,
  ): Promise<DeviceTokenEntity> {
    return this.service.addDeviceToken(dto, user);
  }

  @Override()
  async getMany(
    @ParsedRequest() req: CrudRequest,
    @User() user: UserEntity,
  ): Promise<GetManyDefaultResponse<UserEntity> | UserEntity[]> {
    const response = (await this.base.getManyBase(
      req,
    )) as GetManyDefaultResponse<UserEntity>;
    if (user.role.type !== RoleType.ADMIN) {
      let data = null;
      switch (user.role.type) {
        case RoleType.LEADER:
          data = await this.service.getMemberByLeader(req, user);
          break;
        case RoleType.MERCHANT:
          data = await this.service.getMemberByMerchant(req, user);
          break;
      }
      response.data = data?.data || [];
      response.count = response.data?.length || 0;
      response.total = data?.total || 0;
      response.pageCount = data?.pageCount || 0;
    }
    return response;
  }

  @Patch('/change-password/:id')
  @Auth()
  @ApiOperation({
    summary: 'Thay đổi mật khẩu',
  })
  @ApiBody({
    type: ChangePasswordDto,
  })
  async changePassword(
    @Param('id') id: number,
    @Body() dto: ChangePasswordDto,
    @User() user: UserEntity,
  ) {
    const checkPassword = await bcrypt.compare(dto.oldPassword, user.password);
    if (!checkPassword) {
      throw new BadRequestException('Mật khẩu cũ không đúng');
    }
    if (dto.newPassword !== dto.newPasswordConfirm) {
      throw new BadRequestException('Mật khẩu xác nhận không khớp');
    }
    const userUpdate = await this.service.repo.findOne({ id });
    if (!userUpdate) {
      throw new NotFoundException('Không tìm thấy CTV');
    }
    userUpdate.password = dto.newPassword;
    return await this.service.repo.save(userUpdate);
  }

  @Patch('/update-avatar')
  @ApiOperation({ summary: 'Cập nhật avatar' })
  @ApiBody({ type: UpdateAvatarDto })
  async updateAvatar(@Body() dto: UpdateAvatarDto, @User() user: UserEntity) {
    if (!dto.avatar) {
      throw new BadRequestException('Avatar không tồn tại.');
    }
    const partner = await this.service.repo.findOne({
      where: {
        id: user.id,
      },
      relations: ['role'],
    });
    const partnerUpdate = await this.service.repo.save({
      ...partner,
      avatar: dto.avatar,
    });
    let avatar = '';
    if (
      !partnerUpdate.avatar?.includes('http') &&
      !partnerUpdate.avatar?.includes('https')
    )
      avatar = 'https://ushare.hn.ss.bfcplatform.vn/' + partnerUpdate.avatar;
    else avatar = partnerUpdate.avatar;
    const tokenObject = {
      tel: partner.tel,
      roleId: partner.roleId,
      fullName: partner.fullName,
      permissions: partner?.role?.permissions || [],
      sub: partner.id,
      type: partner?.role?.type,
      merchantId: partner.merchantId,
      status: partner.status,
      avatar: avatar,
    };
    return {
      token: this.jwtService.sign(tokenObject),
    };
  }

  @Patch('/update-payment-info')
  @ApiOperation({ summary: 'Cập nhật thông tin thanh toán' })
  @ApiBody({ type: UpdatePaymentInfoDto })
  async updatePaymentInfo(
    @Body() dto: UpdatePaymentInfoDto,
    @User() user: UserEntity,
  ) {
    return this.service.repo.save({
      ...user,
      ...dto,
    });
  }
}

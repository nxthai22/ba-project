import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import {
  CommissionValueType,
  ConfigType,
  ReferralCommission,
} from 'entities/config.entity';
import { OrderRevenueEntity } from 'entities/order-revenue.entity';
import { UserEntity } from 'entities/user.entity';
import { OrderStatus } from 'enums';
import { ConfigService } from 'modules/config/config.service';
import {
  Between,
  Connection,
  In,
  IsNull,
  LessThanOrEqual,
  MoreThanOrEqual,
  Not,
  Raw,
  Repository,
} from 'typeorm';
import {
  UserBonus,
  UserBonusFilterDto,
  UserBonusResponse,
} from './dto/user-bonus.dto';
import { DeviceTokenEntity } from 'entities/device-token.entity';
import { AddDeviceTokenDto } from 'modules/user/dto/add-device-token.dto';
import { CrudRequest, GetManyDefaultResponse } from '@nestjsx/crud';
import { compareAsc, endOfDay, startOfDay } from 'date-fns';
import { MerchantService } from '../merchant/merchant.service';
import { RoleEntity, RoleType } from '../../entities/role.entity';

@Injectable()
export class UserService extends TypeOrmCrudService<UserEntity> {
  constructor(
    @InjectRepository(UserEntity)
    public repo: Repository<UserEntity>,
    @InjectRepository(DeviceTokenEntity)
    public deviceTokenRepo: Repository<DeviceTokenEntity>,
    @InjectRepository(RoleEntity)
    public roleRepo: Repository<RoleEntity>,
    @InjectRepository(OrderRevenueEntity)
    public orderRevenueRepo: Repository<OrderRevenueEntity>,
    public configService: ConfigService,
    public merchantService: MerchantService,
    @InjectConnection() private connection: Connection,
  ) {
    super(repo);
  }

  /** Danh sách thành viên của Trưởng nhóm */
  async getMemberByLeader(
    req: CrudRequest,
    user: UserEntity,
  ): Promise<GetManyDefaultResponse<UserEntity>> {
    const page = req?.parsed?.page || 1;
    const limit = req?.parsed?.limit || 10;
    const searchText = req?.parsed?.filter
      .find((x) => x.field === 'fullName' || x.field === 'tell')
      ?.value?.toLowerCase();
    const roleId = req?.parsed?.filter.find((x) => x.field === 'roleId')?.value;
    const query = this.connection
      .createQueryBuilder()
      .from('user_leader', 'userLeader')
      .leftJoin('user', 'user', '"user"."id" = "userLeader"."userId"')
      .leftJoin(
        'user_merchant',
        'userMerchant',
        '"userMerchant"."userId" = "userLeader"."userId"',
      )
      .where('"userLeader"."leaderId" = :leaderId', {
        leaderId: user.id,
      })
      .andWhere('"userMerchant"."merchantId" = :merchantId', {
        merchantId: user.merchantId,
      })
      .select('"user".*');
    if (searchText) {
      query.andWhere(
        '(LOWER("user"."fullName") LIKE :searchText OR "user"."tel" LIKE :searchText)',
        {
          searchText: '%' + searchText + '%',
        },
      );
    }
    if (roleId) {
      query.andWhere('"user"."roleId" = :roleId', {
        roleId: roleId,
      });
    }
    const total = (await query.execute())?.length || 0;
    if (limit) {
      query.offset((page - 1) * limit).limit(limit);
    }
    const data = await query.execute();
    const roleIds = data?.reduce((arr: number[], curr) => {
      if (!arr.includes(curr.roleId)) arr.push(curr.roleId);
      return arr;
    }, []);
    const roles = await this.roleRepo.find({
      where: {
        id: In(roleIds),
      },
    });
    data?.map((d) => {
      d.role = roles?.find((role) => role.id === d.roleId);
    });
    // const data = await query.execute();
    return {
      page: page || 1,
      data: data,
      total: total,
      count: data.length || 0,
      pageCount: Math.ceil(total / (limit || 1)),
    };
  }

  /** Danh sách thành viên của NCC (Trưởng nhóm/CTV/...) */
  async getMemberByMerchant(
    req: CrudRequest,
    user: UserEntity,
  ): Promise<GetManyDefaultResponse<UserEntity>> {
    const page = req?.parsed?.page || 1;
    const limit = req?.parsed?.limit || 10;
    const searchTextValue = req.parsed.filter
      .find((x) => x.field === 'fullName' || x.field === 'tell')
      ?.value;
    const searchText = searchTextValue ? String(searchTextValue).toLowerCase() : null;
    const roleId = req.parsed.filter.find((x) => x.field === 'roleId')?.value;
    const query = this.connection
      .createQueryBuilder()
      .from('user_merchant', 'userMerchant')
      .leftJoin('user', 'user', '"user"."id" = "userMerchant"."userId"')
      .where('"userMerchant"."merchantId" = :merchantId', {
        merchantId: user.merchantId,
      })
      .select('"user".*');
    if (searchText) {
      query.andWhere(
        '(LOWER("user"."fullName") LIKE :searchText OR "user"."tel" LIKE :searchText)',
        {
          searchText: '%' + searchText + '%',
        },
      );
    }
    if (roleId) {
      query.andWhere('"user"."roleId" = :roleId', {
        roleId: roleId,
      });
    }
    const datas = (await query.execute()) as UserEntity[];
    if (!roleId || roleId === 7) {
      let merchants: UserEntity[] = [];
      if (searchText) {
        merchants = await this.repo.find({
          where: {
            merchantId: user.merchantId,
            // roleId: 7,
            id: Not(user.id),
            fullName: Raw((alias) => {
              return `(LOWER("UserEntity"."fullName") LIKE '%${searchText}%' OR "UserEntity"."tel" LIKE '%${searchText}%')`;
            }),
          },
        });
      } else {
        merchants = await this.repo.find({
          where: {
            merchantId: user.merchantId,
            // roleId: 7,
            id: Not(user.id),
          },
        });
      }
      if (merchants) {
        datas.push(...merchants);
      }
    }
    let data: UserEntity[] = [];
    if (limit && datas?.length > 0) {
      // query.offset((page - 1) * limit).limit(limit);
      data = datas.slice(page - 1, page * limit);
    } else {
      data = datas;
    }
    const roleIds = data?.reduce((arr: number[], curr) => {
      if (!arr.includes(curr.roleId)) arr.push(curr.roleId);
      return arr;
    }, []);
    const roles = await this.roleRepo.find({
      where: {
        id: In(roleIds),
      },
    });
    data?.map((d) => {
      d.role = roles?.find((role) => role.id === d.roleId);
    });
    // const data = await query.execute();
    return {
      page: page || 1,
      data: data,
      total: datas?.length || 0,
      count: data.length || 0,
      pageCount: Math.ceil((datas?.length || 0) / (limit || 1)),
    };
  }

  /** Danh sách top hoa hồng từ doanh thu,thưởng đơn đầu tiên F1 */
  async getTopBonusForUser(
    dto: UserBonusFilterDto,
    user: UserEntity,
  ): Promise<UserBonus> {

    // Danh sách các user refer
    const userReferrals = await this.repo.find({
      where: {
        referralParentId: user.id,
        referralBonusStart: Not(IsNull()),
        referralBonusExpire: Not(IsNull()),
      },
      relations: ['ward'],
      // skip: dto.limit * (dto.page - 1),
      // take: dto.limit,
    });
    const result: UserBonusResponse[] = [];
    await Promise.all(
      userReferrals?.map(async (userInfo) => {
        // Với mỗi user refer lấy doanh thu trong khoảng thời gian bonus start - expired
        const orderRevenues = await this.orderRevenueRepo.find({
          createdAt: Between(
            userInfo.referralBonusStart,
            userInfo.referralBonusExpire,
          ),
          userId: userInfo.id,
          status: OrderStatus.COMPLETED,
        });

        // Config thưởng cho F0/F1
        const config = (
          await this.configService.repo.findOne({
            where: {
              startDate: LessThanOrEqual(userInfo.referralBonusStart),
              endDate: MoreThanOrEqual(userInfo.referralBonusStart),
              key: ConfigType.FIRSTORDER,
            },
          })
        )?.value as ReferralCommission;

        // Tính tổng bonus theo loại bonus của user số lượng/%
        const userBonusResponse = new UserBonusResponse();
        userBonusResponse.user = userInfo;
        userBonusResponse.totalReferralBonus = orderRevenues?.reduce(function (
          total,
          curr,
        ) {
          return total + userInfo?.valueBonusType === CommissionValueType.AMOUNT
            ? Number(userInfo.valueBonus || 0)
            : (Number(curr.productCommission || 0) *
                Number(userInfo.valueBonus || 0)) /
                100;
        },
        0);
        userBonusResponse.totalFirstOrderBonus = Number(config?.f0) || 0;
        result.push(userBonusResponse);
      }),
    );
    const totalFirstOrderBonus =
      result.reduce((total, curr) => {
        return total + Number(curr.totalFirstOrderBonus || 0);
      }, 0) || 0;
    const totalReferralBonus =
      result.reduce((total, curr) => {
        return total + Number(curr.totalReferralBonus || 0);
      }, 0) || 0;
    let userBonus = result;
    if (dto.fromDate && dto.toDate) {
      userBonus = result.filter(
        (us) =>
          compareAsc(us.user.createdAt, startOfDay(new Date(dto.fromDate))) >=
            0 &&
          compareAsc(us.user.createdAt, endOfDay(new Date(dto.toDate))) < 0,
      );
    }
    return {
      totalFirstOrderBonus: totalFirstOrderBonus,
      totalReferralBonus: totalReferralBonus,
      userBonus: userBonus,
    };
  }

  async addDeviceToken(
    dto: AddDeviceTokenDto,
    user: UserEntity,
  ): Promise<DeviceTokenEntity> {
    await this.deviceTokenRepo.delete({
      token: dto.token,
    });

    return await this.deviceTokenRepo.save({
      token: dto.token,
      userId: user.id,
    });
  }
}

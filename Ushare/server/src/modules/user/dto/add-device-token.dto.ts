import { PickType } from '@nestjs/swagger';
import { DeviceTokenEntity } from 'entities/device-token.entity';

export class AddDeviceTokenDto extends PickType(DeviceTokenEntity, ['token']) {}

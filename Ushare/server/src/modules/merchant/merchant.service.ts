import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { In, Not, Repository } from 'typeorm';
import { ErrorMessage, OrderStatus } from 'enums';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { MerchantEntity } from 'entities/merchant.entity';
import { OrderEntity } from 'entities/order.entity';
import { UserEntity } from 'entities/user.entity';
import { CreateMerchantDto } from './dto/create-merchant.dto';

@Injectable()
export class MerchantService extends TypeOrmCrudService<MerchantEntity> {
  constructor(
    @InjectRepository(MerchantEntity)
    public repo: Repository<MerchantEntity>,
    @InjectRepository(MerchantAddressEntity)
    public merchantAddressRepo: Repository<MerchantAddressEntity>,
    @InjectRepository(OrderEntity)
    public orderRepo: Repository<OrderEntity>,
  ) {
    super(repo);
  }
  /**
   * @deprecated ????
   * Lấy danh sách kho theo Merchant
   **/
  async getMerchantAddressByMerchant(
    merchantId: number,
  ): Promise<MerchantAddressEntity[]> {
    const merchantAddresses = await this.merchantAddressRepo.find({
      where: {
        merchantId: merchantId,
      },
      relations: ['ward', 'ward.district', 'ward.district.province'],
    });
    return merchantAddresses;
  }

  /**
   *  @deprecated ????
   * Thêm/Sửa kho
   * */
  async createOrUpdateMerchantAddress(
    dto: MerchantAddressEntity,
    user: UserEntity,
  ): Promise<MerchantAddressEntity> {
    const merchant = await this.repo.findOne(user?.merchantId);
    if (!merchant) {
      throw new NotFoundException('Không tìm thấy NCC!');
    }
    let merchantAddress = new MerchantAddressEntity();
    if (dto?.id) {
      merchantAddress = await this.merchantAddressRepo.findOne(dto.id);
      merchantAddress = dto;
    } else {
      merchantAddress = dto;
      merchantAddress.createdAt = new Date();
    }
    merchantAddress.updatedAt = new Date();
    merchantAddress.merchantId = user.merchantId;
    return await this.merchantAddressRepo.save(dto);
  }

  /**
   * @deprecated ????
   * Xóa kho
   * */
  async deleteMerchantAddress(id: number) {
    const orders = await this.orderRepo.find({
      where: {
        merchantAddressId: id,
        status: Not(
          In([
            OrderStatus.COMPLETED,
            OrderStatus.CANCELLED,
            OrderStatus.MERCHANT_CANCELLED,
            OrderStatus.RETURNED,
          ]),
        ),
      },
    });
    if (orders?.length > 0) {
      throw new BadRequestException(
        'Kho đang tồn tại đơn hàng chưa hoàn thành',
      );
    }
    return await this.merchantAddressRepo.softDelete({ id: id });
  }

  async checkForExistence(dto: CreateMerchantDto) {
    const { name, code } = dto;

    const merchant = await this.repo.findOne({
      where: [{ name }, { code }],
    });

    if (merchant) {
      throw new BadRequestException(ErrorMessage.MERCHANT_ALREADY_EXISTS);
    }
  }
}

import { PickType } from '@nestjs/swagger';
import { MerchantEntity } from 'entities/merchant.entity';

export class CreateMerchantDto extends PickType(MerchantEntity, [
  'name',
  'code',
  'wardId',
  'address',
  'tel',
  'status',
  'bankId',
  'bankAccount',
  'bankNumber',
  'bankBranch',
]) {}

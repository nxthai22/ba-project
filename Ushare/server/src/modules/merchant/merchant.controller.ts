import { BadRequestException, Param, Patch, Post } from '@nestjs/common';
import { ApiBody, ApiResponse } from '@nestjs/swagger';
import {
  CrudController,
  CrudRequest,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { In, Not } from 'typeorm';
import { ErrorMessage } from 'enums';
import { Auth } from 'decorators/auth.decorator';
import { Crud } from 'decorators/crud.decorator';
import { MerchantEntity } from 'entities/merchant.entity';
import { MerchantService } from 'modules/merchant/merchant.service';
import { CreateMerchantDto } from './dto/create-merchant.dto';

@Crud({
  controller: 'merchants',
  name: 'Đối tác',
  model: {
    type: MerchantEntity,
  },
  routes: {
    include: ['deleteOneBase'],
  },
})
@Auth()
export class MerchantController implements CrudController<MerchantEntity> {
  constructor(public readonly service: MerchantService) {}

  get base(): CrudController<MerchantEntity> {
    return this;
  }

  @Override()
  @ApiResponse({ type: MerchantEntity })
  @ApiBody({ type: CreateMerchantDto })
  async createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: CreateMerchantDto,
  ): Promise<MerchantEntity> {
    await this.service.checkForExistence(dto);
    return this.base.createOneBase(req, dto);
  }

  @Override()
  @ApiResponse({ type: MerchantEntity })
  @ApiBody({ type: CreateMerchantDto })
  async updateOne(
    @Param('id') id: number,
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: CreateMerchantDto,
  ): Promise<MerchantEntity> {
    const itemCheck = await this.service.repo.findOne({
      where: [
        { name: dto.name, id: Not(In([id])) },
        { code: dto.code, id: Not(In([id])) },
      ],
    });
    if (itemCheck) {
      throw new BadRequestException(ErrorMessage.MERCHANT_ALREADY_EXISTS);
    }
    return this.base.updateOneBase(req, dto);
  }
}

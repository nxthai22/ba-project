import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { MerchantEntity } from 'entities/merchant.entity';
import { OrderEntity } from 'entities/order.entity';
import { MerchantController } from 'modules/merchant/merchant.controller';
import { MerchantService } from 'modules/merchant/merchant.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      MerchantEntity,
      MerchantAddressEntity,
      OrderEntity,
    ]),
  ],
  controllers: [MerchantController],
  providers: [MerchantService],
  exports: [MerchantService],
})
export class MerchantModule {}

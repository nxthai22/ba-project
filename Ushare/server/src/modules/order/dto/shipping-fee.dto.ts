import { ApiProperty, PickType } from '@nestjs/swagger';
import { ShippingPartner } from 'enums';
import { OrderEntity, ShippingInfo } from 'entities/order.entity';
import { CreateSubShipmentDto } from './create-shipment.dto';

// export class ShippingFeeDto extends PickType(OrderEntity, [
//   'width',
//   'height',
//   'length',
//   'note',
// ]) {
//   @ApiProperty({
//     description: 'Kho',
//   })
//   merchantAddressId: number;
//
//   @ApiProperty({
//     description: 'ID đơn hàng',
//     type: Number,
//   })
//   orderId: number;
//
//   @ApiProperty({
//     description: 'Đơn vị vận chuyển',
//     required: false,
//     enum: ShippingPartner,
//   })
//   partner?: ShippingPartner;
//
//   @ApiProperty({
//     description: 'Thông tin giao vận đơn con',
//     required: false,
//     type: [CreateSubShipmentDto],
//   })
//   subShipments?: CreateSubShipmentDto[];
// }
export class SubShippingFeeResponseDto {
  @ApiProperty({
    description: 'Danh sách orderProductId trong đơn cha',
    type: [Number],
  })
  orderProductIds: number[];

  @ApiProperty({
    description: 'Thông tin vận chuyển',
    type: ShippingInfo,
  })
  shippingInfo: ShippingInfo;
}

export class ShippingFeeResponseDto {
  @ApiProperty({
    description: 'Danh sách orderProductId trong đơn cha',
  })
  orderId: number;

  @ApiProperty({
    description: 'Đối tác',
    required: false,
  })
  partner?: string;

  @ApiProperty({
    description: 'Danh sách orderProductId trong đơn cha',
    type: ShippingInfo,
  })
  shippingInfo?: ShippingInfo;

  @ApiProperty({
    description: 'Danh sách orderProductId trong đơn cha',
    type: [SubShippingFeeResponseDto],
    required: false,
  })
  subShippingInfo?: SubShippingFeeResponseDto[];
}

import { ApiProperty } from '@nestjs/swagger';
import { OrderStatus } from 'enums';

export class StatusCountResponseDto {
  @ApiProperty({
    description: 'Trạng thái',
    enum: OrderStatus,
  })
  status: OrderStatus;

  @ApiProperty({
    description: 'Số lượng đơn',
    type: Number,
  })
  count: number;
}

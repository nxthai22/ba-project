import { IntersectionType, PartialType, PickType } from '@nestjs/swagger';
import { OrderEntity } from 'entities/order.entity';
import { CreateOrderDto } from 'modules/order/dto/create-order.dto';

export class UpdateOrderDto extends IntersectionType(
  PartialType(PickType(OrderEntity, ['id', 'status'])),
  CreateOrderDto,
) {}

import { ApiProperty } from '@nestjs/swagger';
import { ProductEntity } from '../../../entities/product.entity';
import { ProductVariantEntity } from '../../../entities/product-variant.entity';
import { ShippingInfo } from '../../../entities/order.entity';
import { OrderPaymentType } from '../../../enums';
import { UserEntity } from '../../../entities/user.entity';
import { MerchantAddressEntity } from '../../../entities/merchant-address.entity';

export class IndexOrderImport {
  @ApiProperty({
    description: 'Vị trí cột Mã kho hàng',
    type: Number,
    default: 1,
  })
  rowStartData?: number | 1;
  @ApiProperty({
    description: 'Vị trí cột Mã kho hàng',
    type: Number,
    required: false,
  })
  merchantAddressCodeIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Mã đơn hàng',
    type: Number,
    required: false,
  })
  orderCodeIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Ngày tạo đơn hàng',
    type: Number,
    required: false,
  })
  orderCreatedAtIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Ngày hoàn thành đơn hàng',
    type: Number,
    required: false,
  })
  orderCompletedAtIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Giờ hoàn thành đơn hàng',
    type: Number,
    required: false,
  })
  orderCompletedAtTimeIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Mã SKU',
    type: Number,
    required: false,
  })
  SKUIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Đơn giá sản phẩm',
    type: Number,
    required: false,
  })
  productPriceIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Số lượng',
    type: Number,
    required: false,
  })
  orderProductQtyIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Tiền KM',
    type: Number,
    required: false,
  })
  orderProductDiscountIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Doanh thu',
    type: Number,
    required: false,
  })
  orderProductPriceIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Đặt cọc',
    type: Number,
    required: false,
  })
  orderDepositIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Mã vận đơn',
    type: Number,
    required: false,
  })
  orderShippingInfoPartnerIdIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột ĐVVC',
    type: Number,
    required: false,
  })
  orderShippingInfoPartnerIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Phí vận chuyển',
    type: Number,
    required: false,
  })
  orderShippingInfoFeeIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột SĐT Khách hàng',
    type: Number,
    required: false,
  })
  orderTelIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Tên KH',
    type: Number,
    required: false,
  })
  orderFullNameIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Địa chỉ KH',
    type: Number,
    required: false,
  })
  orderShippingAddressIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Phường',
    type: Number,
    required: false,
  })
  orderWardIdIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột SĐT CTV',
    type: Number,
    required: false,
  })
  orderUserTelIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Tên CTV',
    type: Number,
    required: false,
  })
  orderUserNameIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Ghi chú đơn hàng',
    type: Number,
    required: false,
  })
  orderNoteIndex?: number;
  @ApiProperty({
    description: 'Vị trí cột Hình thức thanh toán',
    type: Number,
    required: false,
  })
  orderPaymentTypeIndex?: number;
}

export class ImportObject {
  id?: number;

  @ApiProperty({
    description: 'Id NCC',
    type: Number,
    required: false,
  })
  merchantId?: number;
  @ApiProperty({
    description: 'Id Kho hàng',
    type: MerchantAddressEntity,
    required: false,
  })
  merchantAddress?: MerchantAddressEntity;
  @ApiProperty({
    description: 'Id Kho hàng',
    type: Number,
    required: false,
  })
  merchantAddressId?: number;
  @ApiProperty({
    description: 'Mã Kho hàng',
    type: Number,
    required: false,
  })
  merchantAddressCode?: string;
  @ApiProperty({
    description: 'Mã đơn hàng',
    type: String,
    required: false,
  })
  orderCode?: string;
  @ApiProperty({
    description: 'Mã đơn hàng import',
    type: String,
    required: false,
  })
  orderImportCode?: string;
  @ApiProperty({
    description: 'Ngày tạo đơn hàng',
    type: Date,
    required: false,
  })
  orderCreatedAt?: Date;
  @ApiProperty({
    description: 'Ngày hoàn thành đơn hàng',
    type: Date,
    required: false,
  })
  orderCompletedAt?: Date;
  @ApiProperty({
    description: 'Mã SKU',
    type: String,
    required: false,
  })
  SKU?: string;
  @ApiProperty({
    description: 'Đơn giá sản phẩm',
    type: Number,
    required: false,
  })
  productPrice?: number;
  @ApiProperty({
    description: 'Thông tin sản phẩm',
    type: ProductEntity,
    required: false,
  })
  product?: ProductEntity;
  @ApiProperty({
    description: 'Thông tin biến thể',
    type: ProductVariantEntity,
    required: false,
  })
  variant?: ProductVariantEntity;
  @ApiProperty({
    description: 'Số lượng',
    type: Number,
    required: false,
  })
  orderProductQty?: number;
  @ApiProperty({
    description: 'Doanh thu',
    type: Number,
    required: false,
  })
  orderProductPrice?: number;
  @ApiProperty({
    description: 'Tiền KM',
    type: Number,
    required: false,
  })
  orderProductDiscount?: number;
  @ApiProperty({
    description: 'Đặt cọc',
    type: Number,
    required: false,
  })
  orderDeposit?: number;
  @ApiProperty({
    description: 'Thông tin vận chuyển',
    type: ShippingInfo,
    required: false,
  })
  orderShippingInfo?: ShippingInfo;
  @ApiProperty({
    description: 'Thông tin thanh toán',
    type: 'enum',
    enum: OrderPaymentType,
    default: OrderPaymentType.COD,
    required: false,
  })
  orderPaymentType?: OrderPaymentType | null;
  @ApiProperty({
    description: 'Địa chỉ giao hàng',
    type: String,
    required: false,
  })
  orderShippingAddress?: string;
  @ApiProperty({
    description: 'Tên người nhận',
    type: String,
    required: false,
  })
  orderFullname?: string;
  @ApiProperty({
    description: 'Sđt người nhận',
    type: String,
    required: false,
  })
  orderTel?: string;
  @ApiProperty({
    description: 'Ghi chú đơn hàng',
    type: String,
    required: false,
  })
  orderNote?: string;
  @ApiProperty({
    description: 'Id phường/xã',
    type: Number,
    required: false,
  })
  orderWardId?: number;
  @ApiProperty({
    description: 'Thông tin CTV',
    type: UserEntity,
    required: false,
  })
  partner?: UserEntity;
  @ApiProperty({
    description: 'Thông báo lỗi',
    type: String,
    required: false,
  })
  errorMessage?: string;
}

export class ImportOrderResponse {
  @ApiProperty({
    description: 'Tổng số bản ghi cần import',
    type: Number,
  })
  totalRecord: number | 0;
  @ApiProperty({
    description: 'Tổng số bản ghi thành công',
    type: Number,
  })
  totalRecordSuccess: number | 0;
  @ApiProperty({
    description: 'Thông tin bản ghi lỗi',
    type: [ImportObject],
  })
  importOrderErrors?: ImportObject[];
}

import { ApiProperty, PickType } from '@nestjs/swagger';
import { ShippingPartner } from 'enums';
import { OrderEntity, ShipperInfo } from 'entities/order.entity';

export class CreateSubShipmentDto extends PickType(OrderEntity, [
  'height',
  'width',
  'length',
]) {
  @ApiProperty({
    description: 'Đối tác',
    required: false,
    enum: ShippingPartner,
  })
  partner?: ShippingPartner;

  @ApiProperty({
    description: 'ID sản phẩm trong đơn',
    required: false,
    type: Number,
  })
  orderProductId?: number;

  @ApiProperty({
    description: 'Thông tin Người vận chuyển đối với DVVC là Khác',
    required: false,
    type: ShipperInfo,
  })
  otherInfo?: ShipperInfo;
}

export class ShipmentRequestDto extends PickType(OrderEntity, [
  'height',
  'width',
  'length',
  'note',
]) {
  @ApiProperty({
    description: 'Kho',
  })
  merchantAddressId: number;

  @ApiProperty({
    description: 'Đối tác',
    enum: ShippingPartner,
    required: false,
  })
  partner?: ShippingPartner;

  @ApiProperty({
    description: 'ID Đơn hàng',
    type: Number,
  })
  orderId: number;

  @ApiProperty({
    description: 'Thông tin giao vận đơn con',
    required: false,
    type: [CreateSubShipmentDto],
  })
  subShipments?: CreateSubShipmentDto[];

  @ApiProperty({
    description: 'Phí vận chuyển đối tác khác',
    required: false,
    type: Number,
  })
  otherFee?: number | 0;

  @ApiProperty({
    description: 'Thông tin Người vận chuyển đối với DVVC là Khác',
    required: false,
    type: ShipperInfo,
  })
  otherInfo?: ShipperInfo;
}

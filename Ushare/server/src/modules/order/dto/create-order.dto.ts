import { ApiProperty, PickType } from '@nestjs/swagger';
import { OrderEntity } from 'entities/order.entity';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  Min,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export enum OrderCreateType {
  GROUP = 'group', // Mua chung
  RETAIL = 'retail', // Mua lẻ
}
export class CreateOrderProduct {
  @IsNotEmpty()
  @Min(1)
  @IsNumber()
  @ApiProperty({
    description: 'Số lượng',
    type: Number,
  })
  quantity: number;

  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  @ApiProperty({
    description: 'ID Sản phẩm',
    type: Number,
  })
  id: number;

  @IsOptional()
  @IsNumber()
  @ApiProperty({
    description: 'ID biến thể Sản phẩm',
    type: Number,
    required: false,
  })
  variantId?: number;

  @ApiProperty({
    description: 'Id nhóm mua chung',
    type: Number,
    required: false,
  })
  groupId?: number;
}

export class CreateOrderDto extends PickType(OrderEntity, [
  'fullname',
  'tel',
  'shippingAddress',
  'wardId',
  'note',
  'paymentType',
  'merchantAddressId',
]) {
  @ValidateNested({ each: true })
  @Type(() => CreateOrderProduct)
  @ApiProperty({
    description: 'Sản phẩm trong đơn hàng',
    required: false,
    type: [CreateOrderProduct],
  })
  products: CreateOrderProduct[];

  @IsNumber()
  @IsOptional()
  @ApiProperty({
    description: 'ID người giới thiệu/Id CTV',
    type: Number,
    required: false,
  })
  pubId: number;

  @ApiProperty({
    description: 'Danh sách Id voucher',
    type: [Number],
    required: false,
  })
  voucherIds?: number[];

  @ApiProperty({
    description: 'Danh sách mã voucher',
    type: [String],
    required: false,
  })
  voucherCodes?: string[];

  @ApiProperty({
    description: 'Id kho hàng',
    type: Number,
    required: false,
  })
  merchantAddressId?: number;

  @ApiProperty({
    description: 'Id nhóm mua chung',
    enum: OrderCreateType,
    required: false,
    default: OrderCreateType.RETAIL,
  })
  orderCreateType?: OrderCreateType;
}

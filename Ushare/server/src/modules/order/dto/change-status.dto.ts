import { ApiProperty, PickType } from '@nestjs/swagger';
import { OrderEntity } from 'entities/order.entity';
import { OrderPaymentStatus, OrderShippingStatus } from '../../../enums';

export class ChangeStatusDto extends PickType(OrderEntity, [
  'status',
  'cancel_reason',
]) {}

export class ChangePaymentTypeDto extends PickType(OrderEntity, [
  'paymentType',
  'deposit',
]) {
  @ApiProperty({
    description: 'Danh sách Id đơn hàng',
    required: true,
    type: [Number],
  })
  orderIds: number[];
}

export class ConfirmDepositDto {
  @ApiProperty({
    description: 'Id đơn hàng',
    required: true,
    type: Number,
  })
  orderId: number;

  @ApiProperty({
    description: 'Tiền cọc',
    required: true,
    type: Number,
  })
  deposit: number;
}

export class ChangeShippingStatusDto {
  @ApiProperty({
    description: 'Trạng thái vận chuyển',
    required: true,
    type: 'enum',
    enum: OrderShippingStatus,
  })
  shippingStatus: OrderShippingStatus;
}

export class ChangePaymentStatusDto {
  @ApiProperty({
    description: 'Danh sách Id đơn hàng',
    required: true,
    type: [Number],
  })
  orderIds: number[];

  @ApiProperty({
    description: 'Trạng thái thanh toán',
    required: false,
    enum: OrderPaymentStatus,
  })
  paymentStatus: OrderPaymentStatus;
}

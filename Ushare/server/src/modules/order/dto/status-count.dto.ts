import { ApiProperty } from '@nestjs/swagger';
import { OrderStatus } from '../../../enums';

export class StatusCountDto {
  @ApiProperty({
    description: 'Ngày tạo đơn',
    required: true,
    type: Date,
  })
  fromDate: Date;

  @ApiProperty({
    description: 'Ngày tạo đơn',
    required: true,
    type: Date,
  })
  toDate: Date;

  @ApiProperty({
    description: 'Ngày tạo đơn',
    required: false,
    type: 'enum',
    enum: OrderStatus,
  })
  status?: OrderStatus;
}

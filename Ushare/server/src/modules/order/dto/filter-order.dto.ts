import { ApiProperty } from '@nestjs/swagger';
import { SearchCriteria } from 'modules/base/base-search';

export class OrderFilterDto extends SearchCriteria {
  @ApiProperty({
    description: 'Ngày bắt đầu',
  })
  fromDate: Date;

  @ApiProperty({
    description: 'Ngày kết thúc',
  })
  toDate: Date;

  @ApiProperty({
    description: 'Id CTV',
  })
  userId?: number;
}

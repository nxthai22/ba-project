import { Module } from '@nestjs/common';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderEntity } from 'entities/order.entity';
import { OrderProductEntity } from 'entities/order-product.entity';
import { OrderHistoryEntity } from 'entities/order-history.entity';
import { ProductEntity } from 'entities/product.entity';
import { UserEntity } from 'entities/user.entity';
import { OrderRevenueEntity } from 'entities/order-revenue.entity';
import { ProductVariantEntity } from 'entities/product-variant.entity';
import { UserWalletEntity } from 'entities/user-wallet.entity';
import { UserWalletHistoryEntity } from 'entities/user-wallet-history.entity';
import { ShippingModule } from 'modules/shipping/shipping.module';
import { MerchantModule } from 'modules/merchant/merchant.module';
import { UserModule } from 'modules/user/user.module';
import { ProductModule } from 'modules/product/product.module';
import { RevenueModule } from 'modules/revenue/revenue.module';
import { FlashSaleModule } from 'modules/flash-sale/flash-sale.module';
import { ConfigModule } from 'modules/config/config.module';
import { VoucherModule } from '../voucher/voucher.module';
import { MerchantAddressModule } from 'modules/merchant-address/merchant-address.module';
import { CartProductModule } from '../cart-product/cart-product.module';
import { NotificationModule } from 'modules/notification/notification.module';

import { OrderHelper } from './helpers';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { GroupBuyingModule } from 'modules/group-buying/group-buying.module';
import { OrderPaymentHistoryEntity } from 'entities/order-deposit-history.entity';
@Module({
  imports: [
    ShippingModule,
    MerchantModule,
    UserModule,
    TypeOrmModule.forFeature([
      OrderEntity,
      OrderProductEntity,
      OrderHistoryEntity,
      ProductEntity,
      UserEntity,
      OrderRevenueEntity,
      ProductVariantEntity,
      UserWalletEntity,
      UserWalletHistoryEntity,
      MerchantAddressEntity,
      OrderPaymentHistoryEntity,
    ]),
    ProductModule,
    RevenueModule,
    FlashSaleModule,
    ConfigModule,
    VoucherModule,
    MerchantAddressModule,
    CartProductModule,
    NotificationModule,
    GroupBuyingModule,
  ],
  controllers: [OrderController],
  providers: [OrderService, OrderHelper],
  exports: [OrderService],
})
export class OrderModule {}

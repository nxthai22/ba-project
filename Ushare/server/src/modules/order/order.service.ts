import {
  BadRequestException,
  ForbiddenException,
  HttpStatus,
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import {
  endOfDay,
  format,
  isThisSecond,
  isWithinInterval,
  startOfDay,
  startOfMonth,
} from 'date-fns';
import {
  AmountCollaboratorCommission,
  ConfigType,
} from 'entities/config.entity';
import {
  OrderHistoryEntity,
  OrderHistoryType,
} from 'entities/order-history.entity';
import { OrderProductEntity } from 'entities/order-product.entity';
import { OrderRevenueEntity } from 'entities/order-revenue.entity';
import {
  OrderEntity,
  ORDER_STATUS_INVALID,
  ShippingInfo,
} from 'entities/order.entity';
import { ProductVariantEntity } from 'entities/product-variant.entity';
import { ProductEntity, ProductStatus } from 'entities/product.entity';
import { UserWalletHistoryEntity } from 'entities/user-wallet-history.entity';
import { UserWalletEntity } from 'entities/user-wallet.entity';
import { UserEntity, UserStatus } from 'entities/user.entity';
import {
  ErrorMessage,
  OrderPaymentStatus,
  OrderPaymentType,
  OrderShippingStatus,
  OrderStatus,
  ShippingPartner,
  WalletType,
} from 'enums';
import { _, groupBy } from 'lodash';
import { PagingHelper, SearchResult } from 'modules/base/base-search';
import { ConfigService } from 'modules/config/config.service';
import { FlashSaleService } from 'modules/flash-sale/flash-sale.service';
import { MerchantAddressService } from 'modules/merchant-address/merchant-address.service';
import { MerchantService } from 'modules/merchant/merchant.service';
import {
  ChangePaymentStatusDto,
  ChangePaymentTypeDto,
  ChangeStatusDto,
  ConfirmDepositDto,
} from 'modules/order/dto/change-status.dto';
import {
  CreateOrderDto,
  OrderCreateType,
} from 'modules/order/dto/create-order.dto';
import { ShipmentRequestDto } from 'modules/order/dto/create-shipment.dto';
import {
  ShippingFeeResponseDto,
  SubShippingFeeResponseDto,
} from 'modules/order/dto/shipping-fee.dto';
import { StatusCountResponseDto } from 'modules/order/dto/status-count.response.dto';
import { ProductService } from 'modules/product/product.service';
import { RevenueService } from 'modules/revenue/revenue.service';
import { ShippingService } from 'modules/shipping/shipping.service';
import {
  Between,
  Connection,
  EntityManager,
  FindConditions,
  FindManyOptions,
  In,
  IsNull,
  LessThanOrEqual,
  MoreThanOrEqual,
  Not,
  QueryRunner,
  Repository,
  Transaction,
  TransactionManager,
} from 'typeorm';
import { CartProductEntity } from '../../entities/cart-product.entity';
import { RoleType } from '../../entities/role.entity';
import { CartProductService } from '../cart-product/cart-product.service';
import { UpdateVoucherHistoryDto } from '../voucher/dto/update-voucher.dto';
import { VoucherService } from '../voucher/voucher.service';
import { OrderFilterDto } from './dto/filter-order.dto';
import { MailerService } from '@nestjs-modules/mailer';
import { CLIENT_DOMAIN } from '@/constants';
import { convertOrderStatusToText, formatCurrency, isInvalidDate } from 'utils';
import { FastifyRequest } from 'fastify';
import { Row } from 'exceljs';
import {
  ImportObject,
  ImportOrderResponse,
  IndexOrderImport,
} from './dto/import-order.dto';
import { hash } from 'bcrypt';
import { NotificationService } from 'modules/notification/notification.service';
import { UserService } from 'modules/user/user.service';
import { NotificationType } from 'entities/notification-config.entity';
import { OrderHelper } from './helpers';
import { StatusCountDto } from './dto/status-count.dto';
import { GroupBuyingService } from 'modules/group-buying/group-buying.service';
import { GroupService } from 'modules/group-buying/group.service';
import { GroupEntity, GroupStatus } from 'entities/group.entity';
import { nanoid } from 'nanoid';
import { OrderPaymentHistoryEntity } from 'entities/order-deposit-history.entity';
import ExcelJS = require('exceljs');

@Injectable()
export class OrderService extends TypeOrmCrudService<OrderEntity> {
  constructor(
    @InjectRepository(OrderPaymentHistoryEntity)
    public orderPaymentHistoryRepo: Repository<OrderPaymentHistoryEntity>,
    @InjectRepository(OrderEntity) public repo: Repository<OrderEntity>,
    @InjectRepository(OrderProductEntity)
    public orderProductRepo: Repository<OrderProductEntity>,
    @InjectRepository(OrderHistoryEntity)
    public orderHistoryRepo: Repository<OrderHistoryEntity>,
    @InjectRepository(ProductEntity)
    public productRepo: Repository<ProductEntity>,
    @InjectRepository(OrderRevenueEntity)
    public orderRevenueRepo: Repository<OrderRevenueEntity>,
    @InjectRepository(ProductVariantEntity)
    public productVariantRepo: Repository<ProductVariantEntity>,
    @InjectRepository(UserWalletEntity)
    public userWalletRepo: Repository<UserWalletEntity>,
    @InjectRepository(UserWalletHistoryEntity)
    public userWalletHistoryRepo: Repository<UserWalletHistoryEntity>,
    @InjectRepository(UserEntity)
    public userRepo: Repository<UserEntity>,
    @InjectConnection() private connection: Connection,
    private shippingService: ShippingService,
    private merchantService: MerchantService,
    private productService: ProductService,
    private revenueService: RevenueService,
    private flashSaleService: FlashSaleService,
    private configService: ConfigService,
    public voucherService: VoucherService,
    public merchantAddressService: MerchantAddressService,
    public cartProductService: CartProductService,
    public notificationService: NotificationService,
    public userService: UserService,
    public groupBuyingService: GroupBuyingService,
    public groupService: GroupService,
    @Inject(MailerService) private mailerService: MailerService,
    private readonly orderHelper: OrderHelper,
  ) {
    super(repo);
  }

  /** Tạo đơn hàng */
  async createOrder(
    dtos: CreateOrderDto[],
    user: UserEntity,
  ): Promise<OrderEntity[]> {
    const phoneRegExp = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/;
    await Promise.all(
      dtos.map(async (dto) => {
        if (!dto.tel.match(phoneRegExp)) {
          throw new BadRequestException(
            `Số điện thoại ${dto.tel} không đúng định dạng`,
          );
        }
        const products = await this.productRepo.find({
          where: {
            id: In(dto.products?.map((prod) => prod.id)),
          },
        });
        const productInActives = products?.filter(
          (prod) => prod.status === ProductStatus.INACTIVE,
        );
        if (productInActives?.length > 0) {
          const productInActiveNames = productInActives?.map((inactive) => {
            return inactive.name;
          });
          throw new BadRequestException(
            `Sản phẩm ${productInActiveNames.join(',')} tạm hết hàng`,
          );
        }
        const productDeletes = products?.filter(
          (prod) => prod.status === ProductStatus.DELETED,
        );
        if (productDeletes?.length > 0) {
          const productDeleteNames = productDeletes?.map((deleted) => {
            return deleted.name;
          });
          throw new BadRequestException(
            `Sản phẩm ${productDeleteNames.join(',')} đã ngừng kinh doanh`,
          );
        }
        const merchantIds = products?.reduce((arr: number[], curr) => {
          if (!arr.includes(curr.merchantId)) {
            arr.push(curr.merchantId);
          }
          return arr;
        }, []);
        if (merchantIds?.length > 1) {
          const product1 = products?.find(
            (prod) => prod.merchantId === Math.min(...merchantIds),
          )?.name;
          const product2 = products?.find(
            (prod) => prod.merchantId === Math.max(...merchantIds),
          )?.name;
          throw new BadRequestException(
            `Sản phẩm ${product1} và ${product2} không cùng nhà cung cấp!`,
          );
        }
      }),
    );
    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      // cause by voucher ushare can not be used Twice by one person
      // so voucher only can be used for the first order
      let shouldApplyUshareVoucher = true;
      const orderCreated = [];
      for (let i = 0; i < dtos.length; i++) {
        const dto = dtos[i];
        if (!dto?.products || dto?.products.length === 0) {
          throw new BadRequestException('Chưa chọn Sản phẩm cho đơn hàng');
        }
        const tmpProducts = dto?.products ? [...dto.products] : [];
        delete dto.products;
        const merchantAddress = await this.merchantAddressService.findOne({
          id: dto?.merchantAddressId,
        });
        if (!merchantAddress)
          throw new BadRequestException('Kho đặt hàng không tồn tại');

        if (dto?.pubId) {
          // const publisher = await this.userRepo.findOne(dto?.pubId);
          const publisher = await queryRunner.manager.findOne(
            UserEntity,
            dto?.pubId,
            {
              relations: ['merchants'],
            },
          );
          if (!publisher)
            throw new BadRequestException(
              'Thông tin người giới thiệu không đúng',
            );
          const query = this.connection
            .createQueryBuilder()
            .from('user_merchant', 'userMerchant')
            .where(
              '"userMerchant"."merchantId" = :merchantId AND "userMerchant"."userId" = :userId',
              {
                merchantId: merchantAddress.merchantId,
                userId: publisher.id,
              },
            )
            .select('"userMerchant".*');
          const userMerchants = (await query.execute()) as any[];
          if (!userMerchants || userMerchants.length == 0) {
            const merchants = [
              ...publisher.merchants,
              { id: merchantAddress.merchantId },
            ];
            await queryRunner.manager.save(UserEntity, {
              ...publisher,
              merchants: merchants,
            });
          }
        }

        const updatedOrder = await queryRunner.manager.save(OrderEntity, {
          ...dto,
          merchantId: merchantAddress.merchantId,
          status: OrderStatus.CREATED,
          userId: dto?.pubId ? dto?.pubId : user ? user.id : 1,
        });
        if (updatedOrder) {
          if (tmpProducts.length > 0) {
            const voucherDto: CreateOrderDto = {
              ...dto,
              products: tmpProducts,
            };
            // Tạo chi tiết đơn hàng
            const data = await this.createOrderProduct(
              voucherDto,
              updatedOrder.id,
              updatedOrder.userId,
              queryRunner,
              shouldApplyUshareVoucher,
            );
            if (data.group) {
              // mark Order relating with Group
              updatedOrder.groupId = data?.group?.id;
              updatedOrder.status = OrderStatus.WAITING_FOR_GROUPING;
              updatedOrder.paymentType = OrderPaymentType.BACS;
              updatedOrder.paymentStatus = OrderPaymentStatus.WAIT_DEPOSIT;
            }
            updatedOrder.total = data.totalPrice;
            updatedOrder.discount = data.totalVoucherDiscount;
            updatedOrder.commission = data.totalCommission;
            updatedOrder.voucherMerchantDiscount = data.voucherMerchantDiscount;
            updatedOrder.voucherUshareDiscount = data.voucherUshareDiscount;
            const realVoucherIds = [];
            if (data.voucherUshare) {
              shouldApplyUshareVoucher = false;
              realVoucherIds.push(data.voucherUshare.id);
            }
            if (data.voucherMerchant) {
              realVoucherIds.push(data.voucherMerchant.id);
            }
            dto.voucherCodes = [];
            dto.voucherIds = realVoucherIds;
          } else {
            updatedOrder.total = 0;
            updatedOrder.discount = 0;
          }
          if (merchantAddress) {
            updatedOrder.code = `${merchantAddress?.code}-${format(
              new Date(updatedOrder.createdAt),
              'yyMMdd',
            )}-${updatedOrder.id}`;
          }
          await queryRunner.manager.save(OrderEntity, updatedOrder);
          // Tạo lịch sử đơn hàng
          await this.createOrderHistory(
            updatedOrder,
            dto?.pubId ? dto?.pubId : user ? user.id : 1,
            queryRunner,
          );
          // Cập nhật lịch sử voucher
          const voucherHistoryDto: UpdateVoucherHistoryDto = {
            order: updatedOrder,
            voucherCodes: dto.voucherCodes || [],
            voucherIds: dto.voucherIds || [],
          };
          await this.voucherService.updateVoucherHistory(
            voucherHistoryDto,
            queryRunner,
          );
          // Xóa trong giỏ hàng nếu có
          const productIds = tmpProducts?.map((p) => p.id);
          const variantIds = tmpProducts?.map((v) => v.variantId);
          const cartProducts = await this.cartProductService.repo.find({
            where: {
              userId: updatedOrder.userId,
              productId: In(productIds),
              productVariantId: In(variantIds),
            },
          });
          if (cartProducts) {
            await queryRunner.manager.remove(CartProductEntity, cartProducts);
          }
        }
        orderCreated.push(updatedOrder);
      }
      await queryRunner.commitTransaction();

      // notification
      for (let i = 0; i < orderCreated.length; i++) {
        const order = orderCreated[i];
        const merchantId = order.merchantId;
        const query = this.userService.repo
          .createQueryBuilder('user')
          .select('"user".*')
          .leftJoin('role', 'role', '"role"."id" = "user"."roleId"')
          .where(
            '"user"."merchantId" = :merchantId AND "role"."type" = :roleType',
            {
              merchantId: merchantId,
              roleType: RoleType.MERCHANT,
            },
          );
        const adminMerchants: UserEntity[] = (await query.execute()) as
          | UserEntity[]
          | [];
        const userIds = adminMerchants.map((u) => u.id);
        // noti for CTV if order is created by admin merchant or guest via refer link
        if (order.userId && order.userId !== 1) {
          userIds.push(order.userId);
        }
        this.notificationService.sendNotificationToUsers({
          userIds,
          title: `Bạn có 1 đơn hàng ${convertOrderStatusToText(order.status)}`,
          body: `Đơn hàng ${
            order.code
          } đã chuyển trạng thái ${convertOrderStatusToText(order.status)}`,
          type: NotificationType.ORDER,
          data: {
            orderId: order.id,
          },
        });
      }

      return orderCreated;
    } catch (err) {
      await queryRunner.rollbackTransaction();
      console.log(err);
      switch (err.status) {
        case HttpStatus.BAD_REQUEST:
          throw new BadRequestException(err.message);
        case HttpStatus.NOT_FOUND:
          throw new NotFoundException(err.message);
        case HttpStatus.FORBIDDEN:
          throw new ForbiddenException(err.message);
        default:
          throw new InternalServerErrorException(err.message);
      }
    } finally {
      await queryRunner.release();
    }
  }

  /** Tính phí */
  async calculateFee(
    dtos: ShipmentRequestDto[],
    user: UserEntity,
  ): Promise<ShippingFeeResponseDto[]> {
    const result: ShippingFeeResponseDto[] = [];
    await Promise.all(
      dtos.map(async (dto) => {
        const subShipmentPartnerGroup = Object.keys(
          groupBy(dto.subShipments, 'partner'),
        );
        if (
          !dto.partner &&
          (!subShipmentPartnerGroup || subShipmentPartnerGroup?.length < 1)
        ) {
          throw new NotFoundException(
            'Đơn vị vận chuyển/Thông tin giao vận đơn con không được trống',
          );
        }
        const order = await this.repo.findOne({
          where: {
            id: dto.orderId,
          },
          relations: [
            'ward',
            'ward.district',
            'ward.district.province',
            'orderProducts',
            'orderProducts.product',
            'orderProducts.product.merchant',
            'merchantAddress',
          ],
        });
        if (!order) {
          throw new NotFoundException('Không tìm thấy mã đơn hàng');
        }
        const merchantAddress =
          await this.merchantService.merchantAddressRepo.findOne({
            where: {
              id: order?.merchantAddress?.id,
            },
            relations: ['ward', 'ward.district', 'ward.district.province'],
          });

        if (!merchantAddress) {
          throw new BadRequestException(
            'Địa chỉ Kho của đối tác không tồn tại',
          );
        }
        // {
        //   orderProductIds: orderProductIds,
        //     shippingInfo: await this.calculateFeeForShipment(
        //   shippingPartner as ShippingPartner,
        //   orderProductIds,
        //   user,
        // ),
        // }
        if (!dto.partner) {
          const subShippingInfos: SubShippingFeeResponseDto[] =
            await Promise.all(
              subShipmentPartnerGroup.map(async (partner) => {
                const _partner = Object.keys(ShippingPartner).find(
                  (x) => ShippingPartner[x] === partner,
                );
                const orderProductIds = dto.subShipments
                  .filter((x) => x.partner === ShippingPartner[_partner])
                  .map((subShipment) => subShipment.orderProductId);
                if (!orderProductIds || orderProductIds?.length < 1) {
                  throw new BadRequestException('Chưa chọn đối tác giao hàng');
                }
                return {
                  orderProductIds: orderProductIds,
                  shippingInfo: await this.calculateFeeForShipment(
                    ShippingPartner[_partner],
                    orderProductIds,
                    user,
                  ),
                };
              }),
            );
          result.push({
            orderId: order.id,
            shippingInfo: null,
            subShippingInfo: subShippingInfos,
          });
        } else {
          const orderProductIds = order.orderProducts.map((prod) => prod.id);
          const orderShippingInfo: ShippingFeeResponseDto = {
            orderId: order.id,
            shippingInfo: await this.calculateFeeForShipment(
              dto.partner,
              orderProductIds,
              user,
            ),
            subShippingInfo: [],
          };
          result.push(orderShippingInfo);
        }
      }),
    );
    return result;
  }

  async createShipment(
    dtos: ShipmentRequestDto[],
    user: UserEntity,
  ): Promise<OrderEntity[]> {
    const orders = await this.connection.transaction(async (manager) => {
      return Promise.all(
        dtos.map(async (dto) => {
          const subShipmentPartnerGroup = Object.keys(
            groupBy(dto.subShipments, 'partner'),
          );
          if (
            !dto.partner &&
            (!subShipmentPartnerGroup || subShipmentPartnerGroup?.length < 1)
          ) {
            throw new NotFoundException(
              'Đơn vị vận chuyển/Thông tin giao vận đơn con không được trống',
            );
          }
          const order = await this.repo.findOne(
            {
              id: dto.orderId,
            },
            {
              relations: [
                'ward',
                'ward.district',
                'ward.district.province',
                'orderProducts',
                'orderProducts.product',
                'merchantAddress',
              ],
            },
          );
          if (!order) {
            throw new NotFoundException('Không tìm thấy mã đơn hàng');
          }
          if (!order.shippingInfos) {
            order.shippingInfos = [];
          }
          const merchantAddress =
            await this.merchantService.merchantAddressRepo.findOne({
              where: {
                id: order.merchantAddress.id,
              },
              relations: ['ward', 'ward.district', 'ward.district.province'],
            });
          if (!merchantAddress) {
            throw new BadRequestException(
              'Địa chỉ Kho của đối tác không tồn tại',
            );
          }
          if (
            subShipmentPartnerGroup?.length > 1 &&
            order.orderProducts?.length != dto.subShipments?.length
          )
            throw new BadRequestException(
              `Đơn hàng ${order.id} tồn tại sản phẩm chưa chọn đối tác vận chuyển`,
            );
          /** Đơn hàng cha chưa chọn ĐTVC, Danh sách đơn hàng con chưa chọn hết ĐTVC */
          if (!dto.partner) {
            await Promise.all(
              subShipmentPartnerGroup.map(async (partner) => {
                if (dto?.partner === ShippingPartner.OTHER) {
                  order.shippingInfos.push({
                    partner: ShippingPartner.OTHER,
                    shipperInfo: dto?.otherInfo,
                    fee: dto.otherFee || 0,
                  });
                } else {
                  const orderProductIds = dto.subShipments
                    .filter((x) => x.partner === ShippingPartner[partner])
                    ?.map((subShipment) => subShipment.orderProductId);
                  if (!orderProductIds || orderProductIds?.length < 1) {
                    throw new BadRequestException(
                      'Chưa chọn đối tác giao hàng',
                    );
                  }
                  order.shippingInfos.push(
                    await this.createShipmentForPartner(
                      ShippingPartner[partner],
                      orderProductIds,
                      user,
                    ),
                  );
                }
              }),
            );
          } else {
            if (dto?.partner === ShippingPartner.OTHER) {
              order.shippingInfos.push({
                partner: ShippingPartner.OTHER,
                shipperInfo: dto?.otherInfo,
                fee: 0,
              });
            } else {
              const orderProductIds = order.orderProducts.map(
                (prod) => prod.id,
              );
              order.shippingInfos.push(
                await this.createShipmentForPartner(
                  dto.partner,
                  orderProductIds,
                  user,
                ),
              );
            }
          }
          order.status = OrderStatus.SHIPPING;
          order.merchantAddressId = dto.merchantAddressId;

          const history = new OrderHistoryEntity();
          history.orderId = order.id;
          history.status = order.status;
          if (user) history.userId = user.id;
          await this.orderHistoryRepo.save(history);

          return await this.repo.save(order);
        }),
      );
    });

    // notify to Collaborator
    orders.forEach((order) => {
      this.notificationService.sendNotificationToUser({
        userId: order.userId,
        title: `Bạn có 1 đơn hàng ${convertOrderStatusToText(order.status)}`,
        body: `Đơn hàng ${
          order.code
        } đã chuyển trạng thái ${convertOrderStatusToText(order.status)}`,
        type: NotificationType.ORDER,
        data: {
          orderId: order.id,
        },
      });
    });
    return orders;
  }

  async statusCount(user: UserEntity): Promise<StatusCountResponseDto[]> {
    return this.repo.query(
      `SELECT status, count("status") FROM "order" ${
        user.role?.type === RoleType.PARTNER
          ? `WHERE "userId"=${user.id}`
          : [
              RoleType.MANAGEMENT_PARTNER,
              RoleType.LEADER,
              RoleType.MERCHANT,
            ].includes(user.role?.type)
          ? `WHERE "merchantId" = ${user.merchantId}`
          : ''
      } GROUP BY status`,
    );
  }

  /**
   * Số lượng đơn hàng theo trạng thái và khoảng ngày tạo đơn
   * @param user
   */
  async statusCountAdmin(
    dto: StatusCountDto,
    user: UserEntity,
  ): Promise<StatusCountResponseDto[]> {
    const query = this.repo
      .createQueryBuilder()
      .where('"createdAt" BETWEEN :fromDate AND :toDate', {
        fromDate: startOfDay(new Date(dto.fromDate)),
        toDate: endOfDay(new Date(dto.toDate)),
      });
    if (dto.status)
      query.andWhere('"status" = :status', {
        status: dto.status,
      });
    if (user.role?.type === RoleType.PARTNER)
      query.andWhere(`"userId" = ${user.id}`);
    if (
      [
        RoleType.MANAGEMENT_PARTNER,
        RoleType.LEADER,
        RoleType.MERCHANT,
      ].includes(user.role?.type)
    )
      query.andWhere(`"merchantId" = ${user.merchantId}`);
    query
      .groupBy('"status"')
      .select('"status"')
      .addSelect('COUNT("status") as count');
    return (await query.execute()) as StatusCountResponseDto[];
  }

  /** Thay đổi trạng thái đơn hàng */
  async changeStatus(id: number, dto: ChangeStatusDto, user: UserEntity) {
    if (
      dto.status !== OrderStatus.CANCELLED &&
      user.role.type === RoleType.PARTNER
    ) {
      throw new ForbiddenException('Tài khoản không có quyền thực hiện');
    }
    const order = await this.repo.findOne({
      where: {
        id: id,
      },
      relations: [
        'user',
        'ward',
        'ward.district',
        'ward.district.province',
        'voucherHistories',
        'orderProducts',
        'orderProducts.product',
      ],
    });

    if (!order) throw new NotFoundException('Không tìm thấy đơn hàng');

    if (
      dto.status === OrderStatus.CONFIRMED &&
      user?.role?.type === RoleType.LEADER
    ) {
      const leaders = (
        await this.userRepo.findOne({
          where: {
            id: order.userId,
          },
          relations: ['leaders'],
        })
      )?.leaders;
      if (!leaders?.find((leader) => leader.id === user.id))
        throw new BadRequestException(
          'Không thể xác nhận đơn do CTV không thuộc quyền quản lý',
        );
    }
    const seller = await this.userRepo.findOne({ id: order.userId });

    const unallowedOrderStatus = [
      OrderStatus.COMPLETED,
      OrderStatus.CANCELLED,
      OrderStatus.MERCHANT_CANCELLED,
      OrderStatus.CANCELLED,
      OrderStatus.REFUNDED,
      OrderStatus.FAILED,
      OrderStatus.RETURNED,
    ];
    if (unallowedOrderStatus.includes(order.status)) {
      throw new BadRequestException(
        'Không thể thay đổi trạng thái của đơn đã ở trạng Hoàn thành hoặc Hủy!',
      );
    }

    if (order.status === dto.status) {
      throw new BadRequestException('Không thể thay đổi cùng trạng thái');
    }

    const orderStatusCancel = [
      OrderStatus.MERCHANT_CANCELLED,
      OrderStatus.CANCELLED,
    ];
    const cancelReason = (dto.cancel_reason || '').trim();

    if (orderStatusCancel.includes(dto.status) && !cancelReason) {
      throw new BadRequestException('Bắt buộc phải nhập Lý do huỷ');
    }

    if (OrderStatus.RETURNED === dto.status && !cancelReason) {
      throw new BadRequestException('Lý do Hoàn hàng không được trống');
    }

    if (
      OrderStatus.RETURNED === dto.status &&
      order.status !== OrderStatus.SHIPPING
    ) {
      throw new BadRequestException(
        'Không thể thay đổi trạng thái khi đơn hàng chưa "Hủy giao hàng" từ nhà vận chuyển và trạng thái không phải là "Đang giao"',
      );
    }

    if (
      order.groupId &&
      OrderStatus.CANCELLED === dto.status &&
      order.status !== OrderStatus.WAITING_FOR_GROUPING
    ) {
      throw new BadRequestException(
        ' Không thể hủy đơn nhóm khi đơn nhóm không ở trạng thái chờ ghép',
      );
    }

    order.status = dto.status;

    if ([...orderStatusCancel, OrderStatus.RETURNED].includes(dto.status)) {
      order.cancel_reason = cancelReason;
    }
    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      // Cập nhật tồn kho sản phẩm
      await this.productService.updateStockAfterChangeOrderStatus(
        order,
        queryRunner.manager,
      );

      // Cập nhật số lượng sản phẩm trong flash sale
      // await this.flashSaleService.updateQuantityProductFlashSale(
      //   order,
      //   queryRunner,
      // );

      //  Cập nhật lịch sử đơn hàng
      const history = await this.orderHistoryRepo.findOne({
        where: {
          orderId: id,
          status: dto.status,
        },
      });
      if (!history) {
        await this.createOrderHistory(order, user.id, queryRunner);
      }

      await queryRunner.manager.save(OrderEntity, order);

      // Cập nhật thời gian hưởng hoa hồng cho người giới thiệu
      await this.configService.updateRangetimeReferralCommission(
        order,
        queryRunner,
      );

      // Cập nhật thông tin doanh thu đơn hàng
      await this.revenueService.updateOrderRevenue(order, queryRunner);
      // Cập nhật lịch sử voucher
      const voucherHistoryDto: UpdateVoucherHistoryDto = {
        order: order,
      };
      await this.voucherService.updateVoucherHistory(
        voucherHistoryDto,
        queryRunner,
      );
      if (OrderStatus.COMPLETED === dto.status) {
        const orderRevenue = await this.orderRevenueRepo.findOne({
          orderId: id,
          userId: seller.id,
        });

        if (orderRevenue && orderRevenue.productCommission) {
          const wallet = await this.userWalletRepo.findOne({
            userId: seller.id,
            type: WalletType.BONUS,
          });
          // Cập nhật số dư mới vào ví
          if (wallet) {
            const affected = await queryRunner.manager.save(UserWalletEntity, {
              ...wallet,
              amount:
                wallet?.amount || 0 + orderRevenue?.productCommission || 0,
            });

            if (!affected) {
              new InternalServerErrorException(ErrorMessage.UPDATE);
            }

            // Lưu lịch sử biến động số dư
            await queryRunner.manager.save(UserWalletHistoryEntity, {
              userId: seller.id,
              walletId: wallet?.id,
              amount: orderRevenue.productCommission,
              walletType: wallet?.type,
              note: 'Tiền thưởng hoa hồng!',
            });
          }
        }
        const numberOfOrders = await this.repo.count({
          where: { userId: seller.id, status: OrderStatus.COMPLETED },
        });
        // Check first order
        if (numberOfOrders === 1) {
          await this.calculateCommissionForFirstOrder(
            seller.id,
            seller.referralParentId,
            queryRunner,
          );
        }

        if (seller.referralBonusStart && seller.referralBonusExpire) {
          await this.calculateCommissionForCollaboratorsReferred(
            seller,
            order.total,
            queryRunner,
          );
        }

        // Cập nhật số lượng bán cho sản phẩm
        const productIds = order?.orderProducts?.map((prod) => prod.productId);
        const products = await this.productRepo.find({
          where: {
            id: In(productIds),
          },
        });
        if (products && products?.length > 0) {
          products.map((prod) => {
            const qty =
              order?.orderProducts?.find(
                (orderProduct) => orderProduct.productId === prod.id,
              )?.quantity || 0;
            prod.saleAmount += qty;
          });
          await queryRunner.manager.save(ProductEntity, products);
        }
      }
      await queryRunner.commitTransaction();
      // send notification
      this.notificationService.sendNotificationOfOrder(order);

      return await this.repo.findOne({ id: order.id });
    } catch (err) {
      await queryRunner.rollbackTransaction();

      switch (err.status) {
        case HttpStatus.NOT_FOUND:
          throw new NotFoundException(err.message);
        case HttpStatus.FORBIDDEN:
          throw new ForbiddenException(err.message);
        case HttpStatus.BAD_REQUEST:
          throw new BadRequestException(err.message);
        default:
          throw new InternalServerErrorException(err.message);
      }
    } finally {
      await queryRunner.release();
    }
  }

  /** Tạo đơn hàng chi tiết */
  async createOrderProduct(
    dto: CreateOrderDto,
    orderId: number,
    userId: number,
    queryRunner?: QueryRunner,
    shouldApplyUshareVoucher?: boolean,
  ) {
    const merchantAddress = await this.merchantAddressService.repo.findOne({
      id: dto.merchantAddressId,
    });

    dto.products.map(async (dtoProduct) => {
      const product = await this.productRepo.findOne({
        where: { id: dtoProduct.id },
      });
      if (merchantAddress.merchantId !== product.merchantId) {
        throw new BadRequestException('Sản phẩm không thuộc đối tác.');
      }
    });
    let data;
    if (dto.orderCreateType && dto.orderCreateType === OrderCreateType.GROUP) {
      data = await this.calculateOrderProductForGroupBuying(
        dto,
        userId,
        queryRunner,
      );
    } else {
      data = await this.voucherService.calculateDiscountVoucher(
        dto,
        userId,
        queryRunner,
        shouldApplyUshareVoucher,
      );
    }

    const orderProductParams: OrderProductEntity[] = data.orderProducts;
    orderProductParams.map((order) => {
      order.orderId = orderId;
      return order;
    });
    if (orderProductParams.length === 0) return data;

    if (queryRunner) {
      data.orderProducts = await queryRunner.manager.save(orderProductParams);
    } else {
      data.orderProducts = await this.orderProductRepo.save(orderProductParams);
    }
    return data;
  }

  /** Tính mức giảm giá khi áp voucher */
  async calculateOrderProductForGroupBuying(
    dto: CreateOrderDto,
    userId: number,
    queryRunner?: QueryRunner,
  ) {
    const merchantAddress = await this.merchantAddressService.findOne({
      id: dto.merchantAddressId,
    });
    if (!merchantAddress) {
      throw new BadRequestException('Kho đặt hàng không tồn tại');
    }
    const products = dto?.products;
    const productIds = products.map(({ id }) => id);
    // find related Product and variants
    const productEntities = await this.productRepo.find({
      where: {
        id: In([...productIds]),
      },
      relations: ['variants'],
    });
    let group;
    const orderProducts: OrderProductEntity[] = [];
    for (let i = 0; i < products.length; i++) {
      const prod = products[i];
      const productEntity = _.find(productEntities, (p) => p.id === prod.id);
      if (!productEntity)
        throw new BadRequestException(`Sản phẩm không tồn tại`);
      const orderProduct = new OrderProductEntity();
      orderProduct.productId = prod.id;
      orderProduct.product = productEntity;
      orderProduct.quantity = prod.quantity;
      orderProduct.voucherIds = [];
      orderProduct.discount = 0;
      orderProduct.discountVoucherProduct = 0;
      orderProduct.discountVoucherShop = 0;
      let variant;
      let groupBuyingProduct;
      if (prod.variantId) {
        variant = productEntity.variants?.find((v) => v.id === prod.variantId);
      }
      if (variant) {
        const groupBuyingVariant =
          await this.groupBuyingService.getActiveGroupBuyingVariant(
            prod.id,
            variant.id,
          );
        if (groupBuyingVariant) {
          groupBuyingProduct =
            await this.groupBuyingService.groupBuyingProductRepo.findOne(
              groupBuyingVariant.groupBuyingProductId,
            );
          // Giới hạn sản phẩm còn lại được phép mua chung
          if (
            groupBuyingVariant.usedQuantity >= groupBuyingVariant.maxQuantity
          ) {
            throw new BadRequestException('Sản phẩm đã hết hàng cho mua chung');
          }
          orderProduct.price = groupBuyingVariant.groupPrice
            ? groupBuyingVariant.groupPrice
            : variant.price;
          orderProduct.commission =
            Math.round(
              (orderProduct.price *
                orderProduct.quantity *
                productEntity.commissionPercent) /
                100,
            ) || 0;
        } else {
          orderProduct.price = variant.price;
          orderProduct.commission = variant.commission * prod.quantity || 0;
        }
        orderProduct.variantId = prod.variantId;
        orderProduct.variantTitle = variant.name;
      } else {
        groupBuyingProduct =
          await this.groupBuyingService.getActiveGroupBuyingProduct(prod.id);
        if (groupBuyingProduct) {
          // Giới hạn sản phẩm còn lại được phép mua chung
          if (
            groupBuyingProduct.usedQuantity >= groupBuyingProduct.maxQuantity
          ) {
            throw new BadRequestException('Sản phẩm đã hết hàng cho mua chung');
          }
          orderProduct.price = groupBuyingProduct.groupPrice
            ? groupBuyingProduct.groupPrice
            : productEntity.price;
          orderProduct.commission =
            Math.round(
              (orderProduct.price *
                orderProduct.quantity *
                productEntity.commissionPercent) /
                100,
            ) || 0;
        } else {
          orderProduct.price = productEntity.price;
          orderProduct.commission =
            productEntity.commission * prod.quantity || 0;
        }
      }

      if (groupBuyingProduct) {
        // Giới hạn số lượng sản phẩm tối đa được mua cho 1 thành viên
        const numberOrderOfUser =
          await this.sumQuantityBookedOfGroupBuyingProduct(
            groupBuyingProduct?.id,
            userId,
          );
        if (
          groupBuyingProduct?.maxQuantityPerUser &&
          numberOrderOfUser + prod.quantity >
            groupBuyingProduct?.maxQuantityPerUser
        ) {
          throw new BadRequestException(
            'Giá ưu đãi chỉ áp dụng với số lượng sản phẩm nhất định',
          );
        }
        // Kiểm tra số lượng nhóm tối đa
        if (groupBuyingProduct?.maxGroupPerUser) {
          const numberOfParticipatingGroup =
            await this.countValidOrderOfGroupBuyingProduct(
              groupBuyingProduct?.id,
              userId,
            );
          if (
            numberOfParticipatingGroup >= groupBuyingProduct?.maxGroupPerUser
          ) {
            throw new BadRequestException(
              'Bạn đang tham gia tối đa số lượng nhóm cho phép, vui lòng hoàn tất nhóm để tiếp tục tham gia',
            );
          }
        }

        // Thông tin nhóm
        if (prod.groupId) {
          group = await this.groupService.repo.findOne(prod.groupId);
          if (!group || group.groupBuyingProductId !== groupBuyingProduct.id) {
            throw new BadRequestException('Nhóm không thuộc sản phẩm');
          }
        } else {
          // Kiểm tra user có đang là trưởng nhóm nào chưa hoàn thành không
          const existedGroup = await this.groupService.repo.findOne({
            where: {
              userId: userId,
              status: GroupStatus.WAITING,
              groupBuyingProductId: groupBuyingProduct.id,
            },
          });
          if (existedGroup) {
            throw new BadRequestException(
              'Không tạo được nhóm mới khi nhóm cũ đang chờ ghép đơn',
            );
          }
          // Tạo nhóm
          const ctv = await this.userRepo.findOne(userId);
          const groupEntity: GroupEntity = {
            name: ctv.fullName,
            userId: userId,
            ref: nanoid(),
            remains: groupBuyingProduct.minMemberNumber,
            groupBuyingProductId: groupBuyingProduct.id,
          };
          group = await this.groupService.repo.save(groupEntity);
        }
      } else {
        throw new BadRequestException('Sản phẩm không hỗ trợ mua chung');
      }

      orderProducts.push(orderProduct);
    }
    const totalPrice = _(orderProducts)
      .map((op) => op.price * op.quantity)
      .sum();
    const totalCommission = _(orderProducts)
      .map((op) => op.commission)
      .sum();
    return {
      voucherMerchant: null,
      voucherUshare: null,
      totalPrice: totalPrice ? Math.round(totalPrice) : 0,
      totalPriceBeforeDiscount: totalPrice ? Math.round(totalPrice) : 0,
      totalCommission: totalCommission ? Math.round(totalCommission) : 0,
      totalVoucherDiscount: 0,
      orderProducts,
      voucherMerchantDiscount: 0,
      voucherUshareDiscount: 0,
      group,
    };
  }

  /** Cập nhật lịch sử đơn hàng */
  async createOrderHistory(
    order: OrderEntity,
    userId?: number,
    queryRunner?: QueryRunner,
  ): Promise<OrderHistoryEntity> {
    const history = new OrderHistoryEntity();
    history.orderId = order.id;
    history.status = order.status;
    if (userId) history.userId = userId;

    if (queryRunner) {
      await queryRunner.manager.save(OrderHistoryEntity, history);
      return history;
    }

    return await this.orderHistoryRepo.save(history);
  }

  /** Tìm kiếm nhanh đơn hàng/doanh thu theo CTV */
  async FilterOrder(
    dto: OrderFilterDto,
    user: UserEntity,
  ): Promise<SearchResult> {
    const orders = await this.repo.find({
      where: {
        userId: user.id,
        createdAt: Between(
          startOfDay(new Date(dto.fromDate)),
          endOfDay(new Date(dto.toDate)),
        ),
      },
      order: {
        createdAt: 'DESC',
      },
      relations: ['revenue'],
    });
    return PagingHelper.getPages(orders, dto);
  }

  /** Thay đổi hình thức thanh toán */
  async changePaymentType(
    dto: ChangePaymentTypeDto,
    user: UserEntity,
  ): Promise<OrderEntity[]> {
    // if (
    //   user.role.type === RoleType.PARTNER &&
    //   dto.paymentType !== OrderPaymentType.CONFIRM_DEPOSIT
    // ) {
    //   throw new BadRequestException('Trạng thái thanh toán không đúng');
    // }
    const orders = await this.repo.find({
      where: {
        id: In(dto.orderIds),
        userId: user.id,
      },
      relations: ['merchant'],
    });
    if (!orders || orders?.length < 1) {
      throw new NotFoundException('Không tìm thấy đơn hàng');
    }
    let accountantEmails = [];
    const orderPaymentHistories: OrderPaymentHistoryEntity[] = [];
    if (dto?.deposit) {
      // const ordersTotal = orders?.reduce(
      //   (total, order) => order.total + total,
      //   0,
      // );
      // if (ordersTotal > dto?.deposit) {
      //   throw new BadRequestException(
      //     'Số tiền đặt cọc không được nhỏ hơn tổng giá trị đơn hàng',
      //   );
      // }
      const bankConfig = await this.configService.findOne({
        key: ConfigType.BANK_INFO,
      });

      accountantEmails = bankConfig?.value?.accountantEmails?.split(',');
    }
    return Promise.all(
      orders?.map(async (order) => {
        if (
          order.status !== OrderStatus.CREATED &&
          order.status !== OrderStatus.WAITING_FOR_GROUPING
        ) {
          throw new BadRequestException(
            `Không thể đổi hình thức thanh toán khi đơn hàng ${order.code} đã được xử lý`,
          );
        }
        if (user?.role?.type === RoleType.MERCHANT) {
          if (dto.deposit > order.total) {
            throw new BadRequestException(
              `Tiền thanh toán/đặt cọc của đơn hàng ${order.code} không được vượt quá giá trị đơn hàng`,
            );
          }
        }
        order.paymentType = dto.paymentType;
        if (dto?.deposit) {
          order.deposit = order.total;
          order.paymentStatus = OrderPaymentStatus.CONFIRM_DEPOSIT;
          if (accountantEmails?.length > 0) {
            this.mailerService
              .sendMail({
                to: accountantEmails,
                subject: `Xác nhận chuyển tiền tiền cho đơn hàng ${order?.code}`, // Subject line
                template: '../templates/deposit',
                context: {
                  user: user,
                  today: format(new Date(), 'dd/MM/yyyy hh:mm:ss'),
                  amount: formatCurrency(order.total),
                  domain: CLIENT_DOMAIN,
                  order: order,
                  orderUpdate: format(order.updatedAt, 'hh:mm:ss dd/MM/yyyy'),
                },
              })
              .catch((error) => {
                console.log('== Không gửi được email ==');
                // console.log(error);
              });
          } else {
            console.log(
              'Không gửi được email cho Kế toán của NCC vì không có Tài khoản nào được thiết lập là Kế toán và có email',
            );
          }
          // Tạo lịch sử  thanh toán
          orderPaymentHistories.push({
            orderId: order.id,
            userId: user.id,
            paymentStatus: OrderPaymentStatus.CONFIRM_DEPOSIT,
          });
          console.log('orderPaymentHistories', orderPaymentHistories);
          // notify order deposit
          try {
            const query = this.userService.repo
              .createQueryBuilder('user')
              .select('"user".*')
              .leftJoin('role', 'role', '"role"."id" = "user"."roleId"')
              .where('"role"."type" IN (:...roleTypes)', {
                roleTypes: [RoleType.ADMIN, RoleType.ACCOUNTANT],
              });
            const ushareStaff: UserEntity[] = (await query.execute()) as
              | UserEntity[]
              | [];
            const userIds = ushareStaff.map((u) => u.id);
            this.notificationService.sendNotificationToUsers({
              userIds,
              title: `Bạn có 1 đơn hàng đã chuyển khoản`,
              body: `Đơn hàng ${order.code} đã được xác nhận chuyển khoản bởi người mua`,
              type: NotificationType.ORDER,
              data: {
                orderId: order.id,
              },
            });
          } catch (err) {
            console.log(err);
          }
        }
        return order;
      }),
    ).then(async (orders) => {
      await this.orderPaymentHistoryRepo.save(orderPaymentHistories);
      return await this.repo.save(orders);
    });
  }

  /** Thay đổi trạng thái thanh toán */
  async updatePaymentStatus(
    dto: ChangePaymentStatusDto,
    user: UserEntity,
  ): Promise<OrderEntity[]> {
    if (user.role.type !== RoleType.ACCOUNTANT) {
      throw new BadRequestException(
        'Bạn không có quyền thay đổi trạng thái đặt cọc',
      );
    }
    const orders = await this.repo.find({
      where: {
        id: In(dto.orderIds),
      },
      relations: ['merchant', 'group', 'group.groupBuyingProduct'],
    });
    if (!orders || orders?.length < 1) {
      throw new NotFoundException('Không tìm thấy đơn hàng');
    }
    const orderPaymentHistories: OrderPaymentHistoryEntity[] = [];
    return Promise.all(
      orders?.map(async (order) => {
        if (order.status !== OrderStatus.CREATED) {
          throw new BadRequestException(
            `Không thể đổi hình thức thanh toán khi đơn hàng ${order.code} đã được xử lý`,
          );
        }
        if (order.paymentType !== OrderPaymentType.BACS) {
          throw new BadRequestException(
            `Hình thức thanh toán cho đơn hàng ${order.code} không áp dụng`,
          );
        }
        order.paymentStatus = dto.paymentStatus;
        orderPaymentHistories.push({
          orderId: order.id,
          userId: user.id,
          paymentStatus: dto.paymentStatus,
        });
        return order;
      }),
    ).then(async (orders) => {
      // save order payment history
      await this.orderPaymentHistoryRepo.save(orderPaymentHistories);
      const updatedOrders = await this.repo.save(orders);

      const groups = orders.filter((o) => o.group).map((o) => o.group);
      const uniqueGroups = _.uniqBy(groups, 'id');
      // checking group
      for (let i = 0; i < uniqueGroups.length; i++) {
        const group = uniqueGroups[i];
        await this.checkingGroupAfterAccountantVerifyDeposit(group);
      }

      // notify payment status
      await this.notificationService.sendNotificationPaymentStatusOfOrders(
        updatedOrders,
      );

      return updatedOrders;
    });
  }

  /** Kế toán xác nhận tiền cọc đơn hàng */
  @Transaction()
  async confirmDeposit(
    dto: ConfirmDepositDto,
    user: UserEntity,
    @TransactionManager() manager?: EntityManager,
  ): Promise<OrderEntity> {
    if (user.role?.type !== RoleType.ACCOUNTANT)
      throw new BadRequestException('Không có quyền (Kế toán) để thao tác');
    const order = await manager.findOne(
      OrderEntity,
      { id: dto.orderId },
      {
        relations: ['group', 'group.groupBuyingProduct'],
      },
    );
    if (!order) throw new NotFoundException('Không tìm thấy đơn hàng');
    if (
      ![
        OrderStatus.WAITING_FOR_GROUPING,
        OrderStatus.CREATED,
        OrderStatus.PROCESSING,
        OrderStatus.CONFIRMED,
      ].includes(order.status)
    )
      throw new BadRequestException(
        'Không thể xác nhận đặt cọc do đơn hàng không trong trạng thái: Đã tạo, Đang xử lý, Đã xác nhận',
      );
    if (order.paymentStatus === OrderPaymentStatus.DEPOSITED)
      throw new BadRequestException('Đơn hàng đã được xác nhận');

    order.deposit = dto.deposit;
    order.paymentStatus = OrderPaymentStatus.DEPOSITED;
    await manager.save(order);

    // checking group
    await this.checkingGroupAfterAccountantVerifyDeposit(order.group, manager);

    // notify payment status
    await this.notificationService.sendNotificationPaymentStatusOfOrder(order);

    return order;
  }

  async checkingGroupAfterAccountantVerifyDeposit(
    group: GroupEntity,
    @TransactionManager() manager?: EntityManager,
  ) {
    manager = manager ? manager : this.connection.createQueryRunner().manager;
    // Chuyển trạng thái các đơn đang chờ ghép nhóm sang đã tạo nếu đủ số lượng member tối thiểu
    if (group) {
      let groupStatus = group.status;
      if (groupStatus === GroupStatus.WAITING) {
        const numberOrderDepositedOfGroup = await manager.count(OrderEntity, {
          where: {
            paymentStatus: OrderPaymentStatus.DEPOSITED,
            groupId: group.id,
          },
        });
        if (
          numberOrderDepositedOfGroup >=
          group.groupBuyingProduct.minMemberNumber
        ) {
          groupStatus = GroupStatus.ACCEPTED;
          await manager.update(
            GroupEntity,
            {
              id: group.id,
            },
            {
              status: groupStatus,
            },
          );
        }
      }

      if (groupStatus === GroupStatus.ACCEPTED) {
        const changedOrders = await manager
          .createQueryBuilder()
          .update(OrderEntity)
          .set({
            status: OrderStatus.CREATED,
          })
          .where(
            '"status" = :status and "paymentStatus" = :paymentStatus and "groupId" = :groupId',
            {
              status: OrderStatus.WAITING_FOR_GROUPING,
              paymentStatus: OrderPaymentStatus.DEPOSITED,
              groupId: group.id,
            },
          )
          .returning('*')
          .execute()
          .then((rs) => rs.raw as OrderEntity[]);

        // Notify
        this.notificationService.sendNotificationOfOrders(changedOrders);
      }
    }
  }

  /** Tính phí vận chuyển */
  protected async calculateFeeForShipment(
    partner: ShippingPartner,
    orderProductIds: number[],
    user: UserEntity,
  ): Promise<ShippingInfo> {
    const orderProducts = await this.orderProductRepo.find({
      where: {
        id: In([...orderProductIds]),
      },
      relations: [
        'order',
        'order.ward',
        'order.ward.district',
        'order.ward.district.province',
        'product',
        'order.merchantAddress',
        'order.merchantAddress.ward',
        'order.merchantAddress.ward.district',
        'order.merchantAddress.ward.district.province',
      ],
    });
    if (orderProducts?.length < 1) {
      throw new NotFoundException('Không có sản phẩm nào được chọn');
    }
    if (orderProducts.find((x) => x.product?.merchantId !== user.merchantId)) {
      throw new BadRequestException('Sản phẩm không thuộc đối tác!');
    }
    try {
      switch (partner) {
        case ShippingPartner.GHTK:
          return await this.shippingService.calculateGhtkFee(
            orderProducts,
            user,
          );
        case ShippingPartner.GHN:
          return await this.shippingService.calculateGhnFee(
            orderProducts,
            user,
          );
        case ShippingPartner.VIETTELPOST:
          return await this.shippingService.calculateVtPFee(
            orderProducts,
            user,
          );
        default:
          throw new NotFoundException('Đối tác vận chuyển không đúng');
      }
    } catch {
      return new ShippingInfo();
    }
  }

  /** Tạo đơn sang đối tác vận chuyển */
  protected async createShipmentForPartner(
    partner: ShippingPartner,
    orderProductIds: number[],
    user: UserEntity,
  ): Promise<ShippingInfo> {
    try {
      switch (partner) {
        case ShippingPartner.GHTK:
          return await this.shippingService.createGhtkOrder(orderProductIds);
        case ShippingPartner.GHN:
          return await this.shippingService.createGhnOrder(orderProductIds);
        case ShippingPartner.VIETTELPOST:
          return await this.shippingService.createViettelPostOrder(
            orderProductIds,
          );
      }
    } catch {
      throw new BadRequestException(
        `Lỗi tạo đơn hàng sang đơn vị vận chuyển ${partner}`,
      );
    }
  }

  protected calculateCommissionForFirstOrder = async (
    userId: number,
    referralParentId: number,
    queryRunner: QueryRunner,
  ): Promise<void> => {
    const commissionConfig = await this.configService.find({
      where: {
        merchantId: IsNull(),
        key: In([ConfigType.FIRSTORDER, ConfigType.REGISTER]),
        startDate: LessThanOrEqual(new Date()),
        endDate: MoreThanOrEqual(new Date()),
      },
    });

    if (!commissionConfig.length) return;

    const firstOrderConfig = commissionConfig.find(
      ({ key }) => key === ConfigType.FIRSTORDER,
    );
    const registerConfig = commissionConfig.find(
      ({ key }) => key === ConfigType.REGISTER,
    );

    let amountUser = 0;
    let amountReferralUser = 0;

    if (registerConfig && registerConfig.value) {
      amountReferralUser += !isNaN(registerConfig.value['f0'])
        ? Number(registerConfig.value['f0'])
        : 0;
      amountUser += !isNaN(registerConfig.value['f1'])
        ? Number(registerConfig.value['f1'])
        : 0;
    }
    if (firstOrderConfig && firstOrderConfig.value) {
      amountReferralUser += !isNaN(firstOrderConfig.value['f0'])
        ? Number(firstOrderConfig.value['f0'])
        : 0;
      amountUser += !isNaN(firstOrderConfig.value['f1'])
        ? Number(firstOrderConfig.value['f1'])
        : 0;
    }

    const userWallet = await this.userWalletRepo.findOne({
      userId,
      type: WalletType.BONUS,
    });

    if (userWallet) {
      const wallet = await queryRunner.manager.findOne(UserWalletEntity, {
        where: {
          userId: userId,
          type: WalletType.BONUS,
        },
      });
      const affected = await queryRunner.manager.save(UserWalletEntity, {
        ...wallet,
        amount: userWallet.amount + amountUser,
      });

      if (!affected) {
        throw new InternalServerErrorException(ErrorMessage.UPDATE);
      }

      // Lưu lịch sử biến động số dư
      await queryRunner.manager.save(UserWalletHistoryEntity, {
        userId,
        walletId: userWallet.id,
        amount: amountUser,
        walletType: userWallet.type,
        note: 'Tiền thưởng của đơn hàng đầu tiên!',
      });
    }

    if (referralParentId) {
      const referralWallet = await this.userWalletRepo.findOne({
        userId: referralParentId,
        type: WalletType.BONUS,
      });

      if (referralWallet) {
        // Cộng tiền hoa hồng cho người giới thiệu [F0]
        const wallet = await queryRunner.manager.findOne(UserWalletEntity, {
          where: {
            userId: referralParentId,
            type: WalletType.BONUS,
          },
        });
        const affected = await queryRunner.manager.save(UserWalletEntity, {
          ...wallet,
          amount: referralWallet.amount + amountReferralUser,
        });

        if (!affected) {
          throw new InternalServerErrorException(ErrorMessage.UPDATE);
        }

        // Lưu lịch sử biến động số dư
        await queryRunner.manager.save(UserWalletHistoryEntity, {
          userId: referralParentId,
          walletId: referralWallet.id,
          amount: amountReferralUser,
          walletType: referralWallet.type,
          note: 'Tiền thưởng của đơn hàng đầu tiên cho người giới thiệu!',
        });
      }
    }
  };

  protected calculateCommissionForCollaboratorsReferred = async (
    seller: UserEntity,
    totalOrderValue: number,
    query: QueryRunner,
  ): Promise<void> => {
    const isValidBonus = isWithinInterval(new Date(), {
      start: seller.referralBonusStart,
      end: seller.referralBonusExpire,
    });

    if (!isValidBonus) return;

    const numberOfCollaborators = await this.userRepo.count({
      where: { referralParentId: seller.referralParentId },
    });

    if (numberOfCollaborators === 0) return;

    const commissionConfig = await this.configService.findOne({
      where: {
        merchantId: IsNull(),
        key: ConfigType.AMOUNT_COLLABORATOR,
        startDate: LessThanOrEqual(new Date()),
        endDate: MoreThanOrEqual(new Date()),
      },
    });

    if (!commissionConfig) return;

    const timeRangeConfigs = (commissionConfig.value ||
      []) as AmountCollaboratorCommission[];

    if (!timeRangeConfigs.length) return;

    const collaboratorConfig = timeRangeConfigs.find(({ min, max }) => {
      return numberOfCollaborators >= min && numberOfCollaborators <= max;
    });
    if (collaboratorConfig) {
      const amountReferralUser = Math.round(
        totalOrderValue * (collaboratorConfig.value / 100),
      );
      // Cộng tiền hoa hồng cho người giới thiệu [F0]
      const referralWallet = await this.userWalletRepo.findOne({
        userId: seller.referralParentId,
        type: WalletType.BONUS,
      });

      if (referralWallet) {
        const affected = await query.manager.save(UserWalletEntity, {
          ...referralWallet,
          amount: referralWallet.amount + amountReferralUser,
        });

        if (!affected) {
          throw new InternalServerErrorException(ErrorMessage.UPDATE);
        }

        // Lưu lịch sử biến động số dư
        await query.manager.save(UserWalletHistoryEntity, {
          userId: seller.referralParentId,
          walletId: referralWallet.id,
          amount: amountReferralUser,
          walletType: referralWallet.type,
          note: 'Tiền thưởng của đơn hàng cho người giới thiệu!',
        });
      }
    }
  };

  /** Import đơn hàng từ file danh sách */
  async importOrder(req: FastifyRequest): Promise<ImportOrderResponse> {
    const indexColumn: IndexOrderImport = {
      rowStartData: 2,
      merchantAddressCodeIndex: 1, //Vị trí cột Mã kho hàng
      orderCodeIndex: 2, //Vị trí cột Mã đơn hàng
      SKUIndex: 3, //Vị trí cột Mã SKU
      orderCreatedAtIndex: 4, //Vị trí cột Ngày tạo đơn hàng
      orderCompletedAtIndex: 5, //Vị trí cột Ngày hoàn thành đơn hàng
      orderCompletedAtTimeIndex: 6, //Vị trí cột thời gian hoàn thành đơn hàng
      productPriceIndex: 7, //Vị trí cột Đơn giá sản phẩm
      orderProductQtyIndex: 8, //Vị trí cột Số lượng
      orderProductDiscountIndex: 10, //Vị trí cột Tiền KM
      orderProductPriceIndex: 11, //Vị trí cột Doanh thu
      orderDepositIndex: 12, //Vị trí cột Đặt cọc
      orderShippingInfoPartnerIdIndex: 13, //Vị trí cột Mã vận đơn
      orderShippingInfoPartnerIndex: 14, //Vị trí cột ĐVVC
      orderPaymentTypeIndex: 15, //Vị trí cột Hình thức thanh toán
      orderShippingInfoFeeIndex: 16, //Vị trí cột Phí vận chuyển
      orderTelIndex: 17, //Vị trí cột SĐT Khách hàng
      orderFullNameIndex: 18, //Vị trí cột Tên KH
      orderShippingAddressIndex: 19, //Vị trí cột Địa chỉ KH
      // orderWardIdIndex?: number,//Vị trí cột Phường
      orderUserTelIndex: 24, //Vị trí cột SĐT CTV
      orderUserNameIndex: 23, //Vị trí cột SĐT CTV
      orderNoteIndex: 25, //Vị trí cột Ghi chú đơn hàng
    };
    const importObjects: ImportObject[] = [];
    const orderImportCodes = [];
    const orderImportErrors: ImportObject[] = [];
    // @ts-ignore
    const file = await req.file();
    try {
      const workbook = new ExcelJS.Workbook();
      await workbook.xlsx.read(file.file).then(async (_workbook) => {
        const worksheet = _workbook.getWorksheet(1);
        // Kiểm tra file lỗi
        if (!worksheet || !worksheet.lastRow)
          throw new BadRequestException(
            'File không hợp lệ, không tìm thấy Worksheet hoặc Row, vui lòng kiểm tra lại !',
          );
        // Cột không đủ giống template
        if (worksheet.lastColumn.number != 25)
          throw new BadRequestException(
            'File không hợp lệ, không đúng số cột, vui lòng kiểm tra lại !',
          );
        // không có tên cột nhưng có data trong cột
        const headerRow = worksheet.getRow(1);
        for (let i = 1; i <= headerRow.cellCount; i++) {
          if (headerRow.getCell(i).toString()?.trim()?.length < 1)
            throw new BadRequestException(
              'File không hợp lệ, tên cột trống, vui lòng kiểm tra lại !',
            );
        }
        // Kiểm tra tên cột so với template
        if (
          headerRow.getCell(1).toString()?.trim() !== 'Mã kho hàng' ||
          headerRow.getCell(2).toString()?.trim() !== 'Số HĐ' ||
          headerRow.getCell(3).toString()?.trim() !== 'Mã SKU' ||
          headerRow.getCell(4).toString()?.trim() !== 'Ngày tạo đơn' ||
          headerRow.getCell(5).toString()?.trim() !== 'Ngày hóa đơn' ||
          headerRow.getCell(6).toString()?.trim() !== 'Giờ hóa đơn' ||
          headerRow.getCell(7).toString()?.trim() !== 'Đơn giá' ||
          headerRow.getCell(8).toString()?.trim() !== 'Số lượng' ||
          headerRow.getCell(9).toString()?.trim() !== 'Tiền hàng' ||
          headerRow.getCell(10).toString()?.trim() !== 'Tiền KM' ||
          headerRow.getCell(11).toString()?.trim() !== 'Doanh thu' ||
          headerRow.getCell(12).toString()?.trim() !== 'Đặt cọc' ||
          headerRow.getCell(13).toString()?.trim() !== 'Mã vận đơn' ||
          headerRow.getCell(14).toString()?.trim() !== 'ĐVVC' ||
          headerRow.getCell(15).toString()?.trim() !== 'Hình thức thanh toán' ||
          headerRow.getCell(16).toString()?.trim() !== 'Phí giao hàng' ||
          headerRow.getCell(17).toString()?.trim() !== 'Sđt khách hàng' ||
          headerRow.getCell(18).toString()?.trim() !== 'Tên KH' ||
          headerRow.getCell(19).toString()?.trim() !== 'Địa chỉ KH' ||
          headerRow.getCell(20).toString()?.trim() !== 'Phường' ||
          headerRow.getCell(21).toString()?.trim() !== 'Quận/Huyện' ||
          headerRow.getCell(22).toString()?.trim() !== 'Tỉnh/TP' ||
          headerRow.getCell(23).toString()?.trim() !== 'Tên CTV' ||
          headerRow.getCell(24).toString()?.trim() !== 'Sđt CTV' ||
          headerRow.getCell(25).toString()?.trim() !== 'Ghi chú đơn hàng'
        )
          throw new BadRequestException(
            'File không hợp lệ, vui lòng kiểm tra lại !',
          );
        const rows = [];
        worksheet.eachRow((row, rowNumber) => {
          if (rowNumber < indexColumn.rowStartData) {
            return;
          }
          rows.push(row);
        });
        // file trông
        if (rows?.filter((row) => row.cellCount > 0)?.length < 1)
          throw new BadRequestException(
            'File không hợp lệ, vui lòng kiểm tra lại !',
          );
        for await (const row of rows) {
          const importObject = new ImportObject();
          importObject.orderImportCode = row
            .getCell(indexColumn.orderCodeIndex)
            .toString();
          const orderCreatedAt = (
            row.getCell(indexColumn.orderCreatedAtIndex)?.value as Date
          )?.toLocaleString('en-US', {
            timeZone: 'Etc/GMT-0',
          });
          importObject.orderCreatedAt = new Date(orderCreatedAt) || null;
          const completedDate =
            new Date(
              (
                row.getCell(indexColumn.orderCompletedAtIndex)?.value as Date
              )?.toLocaleString('en-US', {
                timeZone: 'Etc/GMT-0',
              }),
            ) || null;
          const completedTime =
            new Date(
              (
                row.getCell(indexColumn.orderCompletedAtTimeIndex)
                  ?.value as Date
              )?.toLocaleString('en-US', {
                timeZone: 'Etc/GMT-0',
              }),
            ) || null;
          importObject.orderCompletedAt =
            new Date(
              completedDate?.getFullYear(),
              completedDate?.getMonth(),
              completedDate?.getDate(),
              completedTime?.getHours(),
              completedTime?.getMinutes(),
              completedTime?.getSeconds(),
            ) || null;
          importObject.SKU = row.getCell(indexColumn.SKUIndex).toString();
          importObject.productPrice = Number(
            row.getCell(indexColumn.productPriceIndex).toString() || 0,
          );
          importObject.orderProductQty = Number(
            row.getCell(indexColumn.orderProductQtyIndex).toString() || 0,
          );
          importObject.orderProductPrice = Number(
            row.getCell(indexColumn.orderProductPriceIndex).toString() || 0,
          );
          importObject.orderProductDiscount = Number(
            row.getCell(indexColumn.orderProductDiscountIndex).toString() || 0,
          );
          importObject.orderDeposit = Number(
            row.getCell(indexColumn.orderDepositIndex).toString() || 0,
          );
          importObject.orderPaymentType =
            (row
              .getCell(indexColumn.orderPaymentTypeIndex)
              .toString() as OrderPaymentType) || null;
          importObject.orderShippingAddress = row
            .getCell(indexColumn.orderShippingAddressIndex)
            .toString();
          importObject.orderFullname = row
            .getCell(indexColumn.orderFullNameIndex)
            .toString();
          importObject.orderTel = row
            .getCell(indexColumn.orderTelIndex)
            .toString();
          importObject.orderNote = row
            .getCell(indexColumn.orderNoteIndex)
            .toString();
          importObject.orderShippingInfo = {
            status: OrderShippingStatus.COMPLETED,
            partner: row
              .getCell(indexColumn.orderShippingInfoPartnerIndex)
              .toString() as ShippingPartner,
            fee: Number(
              row.getCell(indexColumn.orderShippingInfoFeeIndex).toString() ||
                0,
            ),
            partner_id: row
              .getCell(indexColumn.orderShippingInfoPartnerIdIndex)
              .toString(),
            statusName: 'Hoàn thành',
          };
          importObject.merchantAddressCode = row
            .getCell(indexColumn.merchantAddressCodeIndex)
            .toString();

          // Validate trường thông tin
          if (
            !importObject.orderCreatedAt ||
            isInvalidDate(importObject.orderCreatedAt)
          ) {
            importObject.errorMessage = 'Ngày tạo đơn không đúng định dạng';
            if (importObject.orderImportCode) importObjects.push(importObject);
            continue;
          }

          if (
            !importObject.orderCompletedAt ||
            isInvalidDate(importObject.orderCompletedAt)
          ) {
            importObject.errorMessage = 'Ngày/giờ hóa đơn không đúng định dạng';
            if (importObject.orderImportCode) importObjects.push(importObject);
            continue;
          }
          if (!importObject.orderPaymentType) {
            importObject.errorMessage = `Phương thức thanh toán không đúng`;
            if (importObject.orderImportCode) importObjects.push(importObject);
            continue;
          }

          if (!importObject.orderShippingInfo?.partner) {
            importObject.errorMessage = `ĐVVC không đúng`;
            if (importObject.orderImportCode) importObjects.push(importObject);
            continue;
          }

          if (
            !importObject.orderImportCode ||
            importObject.orderImportCode?.length < 1
          ) {
            importObject.errorMessage = 'Số hóa đơn không để trống';
            if (importObject.orderImportCode) importObjects.push(importObject);
            continue;
          }

          const partnerTel = row
            .getCell(indexColumn.orderUserTelIndex)
            .toString();
          if (!partnerTel || partnerTel?.length < 1) {
            importObject.errorMessage = 'Sđt CTV không để trống';
            if (importObject.orderImportCode) importObjects.push(importObject);
            continue;
          }
          // Validate database
          const SKU = row.getCell(indexColumn.SKUIndex).toString()?.trim();
          if (!SKU || SKU?.length < 1) {
            importObject.errorMessage = 'Mã SKU không để trống';
            if (importObject.orderImportCode) importObjects.push(importObject);
            continue;
          }
          const jobFindProduct =
            this.orderHelper.findProductOrVariantBySKU(SKU);

          if (
            !importObject.merchantAddressCode ||
            importObject.merchantAddressCode?.length < 1
          ) {
            importObject.errorMessage = 'Mã kho hàng không tồn tại';
            if (importObject.orderImportCode) importObjects.push(importObject);
            continue;
          }
          const jobFindMerchantAddress =
            this.orderHelper.findMerchaneAddressByCode(
              importObject.merchantAddressCode,
            );

          const jobFindPartner = this.orderHelper.findPartnerByTel(partnerTel);

          const jobFindOrder = this.orderHelper.findOrderByImportCode(
            importObject.orderImportCode,
          );

          // eslint-disable-next-line prefer-const
          let [productOrVariant, merchantAddress, partner, orderImported] =
            await Promise.all([
              jobFindProduct,
              jobFindMerchantAddress,
              jobFindPartner,
              jobFindOrder,
            ]);
          const [product, variant] = productOrVariant;

          if (!product && !variant) {
            importObject.errorMessage = 'Mã SKU không tồn tại';
            if (importObject.orderImportCode) importObjects.push(importObject);
            continue;
          }
          if (orderImported?.id) {
            importObject.errorMessage = 'Đơn hàng đã tồn tại';
            if (importObject.orderImportCode) importObjects.push(importObject);
            continue;
          }
          importObject.product = product;
          importObject.variant = variant;
          importObject.id = orderImported?.id;
          importObject.merchantId = product
            ? product.merchantId
            : variant?.product?.merchantId;
          importObject.merchantAddressId = merchantAddress?.id;
          importObject.merchantAddressCode = merchantAddress
            ? merchantAddress?.code
            : importObject.merchantAddressCode;
          importObject.merchantAddress = merchantAddress;
          if (!merchantAddress?.id) {
            importObject.errorMessage = 'Mã kho không tồn tại';
            if (importObject.orderImportCode) importObjects.push(importObject);
            continue;
          }
          if (
            importObject.merchantId !== importObject.merchantAddress?.merchantId
          ) {
            importObject.errorMessage = `Sản phẩm không tồn tại trong kho hàng ${importObject.merchantAddressCode}`;
            if (importObject.orderImportCode) importObjects.push(importObject);
            continue;
          }
          if (!partner && partnerTel) {
            partner = await this.userRepo.save({
              tel: row.getCell(indexColumn.orderUserTelIndex).toString(),
              fullName: row.getCell(indexColumn.orderUserNameIndex).toString(),
              status: UserStatus.ACTIVE,
              roleId: 2,
              password: await hash('123456789', 10),
              merchants: [
                {
                  id: product
                    ? product.merchantId
                    : variant?.product?.merchantId,
                },
              ],
            });
          }
          importObject.partner = partner;
          if (importObject.orderImportCode) importObjects.push(importObject);
        }
        await Promise.all(
          importObjects?.map(async (obj) => {
            const imports = importObjects.filter(
              (o) => o.orderImportCode === obj.orderImportCode,
            );
            if (
              !orderImportCodes.includes(obj.orderImportCode) &&
              (!obj.errorMessage || obj.errorMessage?.length < 1)
            ) {
              if (
                imports?.filter(
                  (imp) => imp.errorMessage || imp.errorMessage?.length > 0,
                )?.length > 0
              ) {
                const errors = orderImportErrors.filter(
                  (er) => er.orderImportCode === obj.orderImportCode,
                );
                if (errors?.length < 1) orderImportErrors.push(...imports);
              } else {
                orderImportCodes.push(obj.orderImportCode);
                const queryRunner = this.connection.createQueryRunner();
                await queryRunner.connect();
                await queryRunner.startTransaction();
                try {
                  // Thêm mới order
                  const order = await queryRunner.manager.save(OrderEntity, {
                    status: OrderStatus.COMPLETED,
                    merchantId: obj.merchantId,
                    merchantAddressId: obj.merchantAddressId,
                    userId: obj.partner?.id,
                    fullname: obj.orderFullname,
                    shippingAddress: obj.orderShippingAddress,
                    wardId: obj.orderWardId,
                    tel: obj.orderTel,
                    total: imports?.reduce((total, curr) => {
                      return total + Number(curr.orderProductPrice || 0);
                    }, 0),
                    discount: imports?.reduce((total, curr) => {
                      return total + Number(curr.orderProductDiscount || 0);
                    }, 0),
                    voucherMerchantDiscount: imports?.reduce((total, curr) => {
                      return total + Number(curr.orderProductDiscount || 0);
                    }, 0),
                    paymentType: obj.orderPaymentType,
                    shippingInfos: obj.orderShippingInfo
                      ? [obj.orderShippingInfo]
                      : [],
                    note: obj.orderNote,
                    deposit: obj.orderDeposit,
                    importCode: obj.orderImportCode,
                    createdAt: obj.orderCreatedAt,
                    updatedAt: obj.orderCompletedAt,
                  });
                  const orderCode = `${obj?.merchantAddress?.code}-${format(
                    new Date(order.createdAt),
                    'yyMMdd',
                  )}-${order.id}`;
                  const orderUpdate = {
                    ...order,
                    code: orderCode,
                    updatedAt: new Date(obj.orderCompletedAt),
                  };
                  await queryRunner.manager.update(
                    OrderEntity,
                    { id: order.id },
                    {
                      ...order,
                      code: orderCode,
                      updatedAt: new Date(obj.orderCompletedAt),
                    },
                  );
                  //Thêm order-history
                  const orderHistories: OrderHistoryEntity[] = [
                    {
                      orderId: order.id,
                      type: OrderHistoryType.ORDER,
                      userId: order.userId,
                      status: OrderStatus.CREATED,
                      createdAt: order.createdAt,
                      updatedAt: order.createdAt,
                      order: null,
                      user: null,
                    },
                    {
                      orderId: order.id,
                      type: OrderHistoryType.ORDER,
                      userId: order.userId,
                      status: OrderStatus.COMPLETED,
                      createdAt: obj.orderCompletedAt,
                      updatedAt: obj.orderCompletedAt,
                      order: null,
                      user: null,
                    },
                  ];
                  await queryRunner.manager.save(
                    OrderHistoryEntity,
                    orderHistories,
                  );

                  // Thêm mới order-product
                  const orderProducts = imports.map((_import) => {
                    return {
                      orderId: order.id,
                      productId: _import.product
                        ? _import.product.id
                        : _import.variant?.product?.id,
                      variantTitle: _import.variant?.name,
                      variantId: _import.variant?.id,
                      quantity: _import.orderProductQty,
                      price: _import.productPrice,
                      discount: _import.orderProductDiscount,
                    };
                  }) as OrderProductEntity[];
                  await queryRunner.manager.save(
                    OrderProductEntity,
                    orderProducts,
                  );

                  // Thêm order-revenue
                  const orderRevenues: OrderRevenueEntity[] = [
                    {
                      orderId: order.id,
                      createdAt: order.createdAt,
                      updatedAt: order.updatedAt,
                      status: OrderStatus.CONFIRMED,
                      revenue: order.total,
                      merchantId: order.merchantId,
                      month: startOfMonth(order.createdAt),
                      order: null,
                      productCommission: 0,
                      userId: order.userId,
                    },
                    {
                      orderId: order.id,
                      createdAt: obj.orderCompletedAt,
                      updatedAt: obj.orderCompletedAt,
                      status: OrderStatus.COMPLETED,
                      revenue: order.total,
                      merchantId: order.merchantId,
                      month: startOfMonth(obj.orderCompletedAt),
                      order: null,
                      productCommission: 0,
                      userId: order.userId,
                    },
                  ];
                  await queryRunner.manager.save(
                    OrderRevenueEntity,
                    orderRevenues,
                  );
                  await queryRunner.commitTransaction();
                } catch (err) {
                  obj.errorMessage = err?.message;
                  orderImportErrors.push(obj);
                  const idx = orderImportCodes.findIndex(
                    (x) => x === obj.orderImportCode,
                  );
                  if (idx > -1) orderImportCodes.splice(idx, 1);
                  await queryRunner.rollbackTransaction();
                } finally {
                  await queryRunner.release();
                }
              }
            } else {
              const errors = orderImportErrors.filter(
                (er) => er.orderImportCode === obj.orderImportCode,
              );
              if (
                !orderImportCodes.includes(obj.orderImportCode) &&
                errors?.length < 1
              )
                orderImportErrors.push(...imports);
            }
          }),
        );
      });
    } catch (e) {
      throw new BadRequestException(e);
    }
    return {
      totalRecord: importObjects?.length || 0,
      totalRecordSuccess: orderImportCodes?.length || 0,
      importOrderErrors: orderImportErrors || [],
    };
  }

  /** Xóa đơn hàng từ file danh sách */
  async deleteImportOrder(req: FastifyRequest): Promise<ImportOrderResponse> {
    const indexColumn: IndexOrderImport = {
      orderCodeIndex: 2, //Vị trí cột Mã đơn hàng
      rowStartData: 2,
    };
    let orderImportCodeDelete = [];
    // @ts-ignore
    const file = await req.file();
    try {
      const workbook = new ExcelJS.Workbook();
      await workbook.xlsx.read(file.file).then(async (_workbook) => {
        const worksheet = _workbook.getWorksheet(1);
        const rows =
          (worksheet.getRows(
            indexColumn.rowStartData,
            worksheet.lastRow.number,
          ) as Row[]) || [];
        for await (const row of rows) {
          orderImportCodeDelete.push(
            row.getCell(indexColumn.orderCodeIndex).toString()?.trim(),
          );
        }
        const orderIds = (
          await this.repo.find({
            where: {
              importCode: In(orderImportCodeDelete),
            },
          })
        )?.map((order) => order.id);
        if (orderIds?.length > 0) {
          const queryRunner = this.connection.createQueryRunner();
          await queryRunner.connect();
          await queryRunner.startTransaction();
          try {
            await queryRunner.manager.softDelete(OrderEntity, {
              id: In(orderIds),
            });
            await queryRunner.manager.softDelete(OrderHistoryEntity, {
              orderId: In(orderIds),
            });
            await queryRunner.manager.softDelete(OrderRevenueEntity, {
              orderId: In(orderIds),
            });
            await queryRunner.manager.softDelete(OrderProductEntity, {
              orderId: In(orderIds),
            });
            await queryRunner.commitTransaction();
          } catch (err) {
            orderImportCodeDelete = [];
            await queryRunner.rollbackTransaction();
          } finally {
            await queryRunner.release();
          }
        }
      });
    } catch (e) {
      throw new BadRequestException(e);
    }
    return {
      totalRecord: orderImportCodeDelete?.length,
      totalRecordSuccess: orderImportCodeDelete?.length || 0,
      importOrderErrors: [],
    };
  }

  /** Cập nhật lại số lượng đã bán cho sản phẩm */
  async updateSaleAmountProduct(): Promise<void> {
    const query = this.orderProductRepo
      .createQueryBuilder('orderProduct')
      .leftJoin(OrderEntity, 'order', '"orderProduct"."orderId" = "order"."id"')
      .where('"order"."status" = :status', {
        status: OrderStatus.COMPLETED,
      })
      .groupBy('"orderProduct"."productId"')
      .select('"orderProduct"."productId"')
      .addSelect('SUM("orderProduct"."quantity") as sale_amount');
    const orderProducts =
      ((await query.execute()) as {
        productId: number;
        sale_amount: number;
      }[]) || [];
    const productIds = orderProducts?.map((orderProd) => orderProd.productId);
    const products = await this.productRepo.find({
      id: In(productIds),
    });
    products?.map((prod) => {
      prod.saleAmount = Number(
        orderProducts?.find((op) => op.productId === prod.id)?.sale_amount || 0,
      );
    });
    await this.productRepo.save(products);
  }

  /** Xóa đơn hàng từ file danh sách */
  async findImportOrder(req: FastifyRequest): Promise<any> {
    const indexColumn: IndexOrderImport = {
      orderCodeIndex: 2, //Vị trí cột Mã đơn hàng
      rowStartData: 2,
    };
    let orderImportCodeFinds = [];
    // @ts-ignore
    const file = await req.file();
    let orders = [];
    try {
      const workbook = new ExcelJS.Workbook();
      await workbook.xlsx.read(file.file).then(async (_workbook) => {
        const worksheet = _workbook.getWorksheet(1);
        const rows =
          (worksheet.getRows(
            indexColumn.rowStartData,
            worksheet.lastRow.number,
          ) as Row[]) || [];
        for await (const row of rows) {
          orderImportCodeFinds.push(
            row.getCell(indexColumn.orderCodeIndex).toString()?.trim(),
          );
        }
        orders = await this.repo.find({
          where: {
            importCode: In(orderImportCodeFinds),
          },
        });
      });
    } catch (e) {
      throw new BadRequestException(e);
    }
    return {
      importCode: orderImportCodeFinds,
      orders: orders,
    };
  }

  /** Cập nhật tên CTV */
  async updateUserImport(req: FastifyRequest): Promise<any> {
    let userImports = [];
    // @ts-ignore
    const file = await req.file();
    let userResponse = [];
    try {
      const workbook = new ExcelJS.Workbook();
      await workbook.xlsx.read(file.file).then(async (_workbook) => {
        const worksheet = _workbook.getWorksheet(1);
        const rows =
          (worksheet.getRows(2, worksheet.lastRow.number) as Row[]) || [];
        for await (const row of rows) {
          if (row.getCell(3).toString()?.trim())
            userImports.push({
              name: row.getCell(2).toString()?.trim(),
              tel: row.getCell(3).toString()?.trim(),
            });
        }
        const tels = userImports?.map((user) => user.tel);
        const users = await this.userRepo.find({
          where: {
            tel: In(tels),
          },
        });
        users?.map((user) => {
          user.fullName =
            userImports?.find((ui) => ui.tel === user.tel)?.name ||
            user.fullName;
        });
        userResponse = await this.userRepo.save(users);
      });
    } catch (e) {
      throw new BadRequestException(e);
    }
    return {
      users: userResponse,
    };
  }

  /**
   * Tính tổng số đơn đã đặt(nhóm đã join) của user trong chương trình mua chung sản phẩm
   * @param groupBuyingProductId
   * @param userId
   */
  countValidOrderOfGroupBuyingProduct(
    groupBuyingProductId: number,
    userId?: number,
  ): Promise<number> {
    return this.repo.count({
      join: {
        alias: 'orders',
        innerJoin: {
          group: 'orders.group',
        },
      },
      where: (qb) => {
        qb.where({
          ...(userId ? { userId } : null),
          status: Not(In(ORDER_STATUS_INVALID)),
        }).andWhere('group.groupBuyingProductId = :groupBuyingProductId', {
          groupBuyingProductId,
        });
      },
    });
  }

  /**
   * Tính tổng số sản phẩm user đã đặt trong chương trình mua chung sản phẩm
   * @param groupBuyingProductId
   * @param userId
   */
  async sumQuantityBookedOfGroupBuyingProduct(
    groupBuyingProductId: number,
    userId: number,
  ): Promise<number> {
    const { sum } = await this.repo
      .createQueryBuilder('orders')
      .select('SUM("orderProducts"."quantity")', 'sum')
      .innerJoin('group', 'group', '"orders"."groupId" = group.id')
      .innerJoin(
        'order_product',
        'orderProducts',
        '"orders"."id" = "orderProducts"."orderId"',
      )
      .where(
        '"orders"."userId" = :userId and "orders"."status" NOT IN (:...status)',
        {
          userId,
          status: ORDER_STATUS_INVALID,
        },
      )
      .andWhere('"group"."groupBuyingProductId" = :groupBuyingProductId', {
        groupBuyingProductId,
      })
      .getRawOne();
    return Number(sum);
  }

  countOrderDepositedInGroup(groupId: number) {
    return this.repo.count({
      where: {
        groupId,
        paymentStatus: OrderPaymentStatus.DEPOSITED,
      },
    });
  }

  countOrderInGroup(groupId: number) {
    return this.repo.count({
      where: {
        groupId,
      },
    });
  }

  countOrderCancelInGroup(groupId: number) {
    return this.repo.count({
      where: {
        groupId,
        status: In([OrderStatus.CANCELLED, OrderStatus.MERCHANT_CANCELLED]),
      },
    });
  }

  countOrderWaitDepositInGroup(groupId: number) {
    return this.repo.count({
      where: {
        groupId,
        paymentStatus: OrderPaymentStatus.WAIT_DEPOSIT,
      },
    });
  }

  findActiveOrder(options: FindManyOptions<OrderEntity> = {}) {
    const where: FindConditions<OrderEntity> = {
      status: Not(In(ORDER_STATUS_INVALID)),
    };
    options = _.merge(options, { where });
    return this.repo.find(options);
  }
}

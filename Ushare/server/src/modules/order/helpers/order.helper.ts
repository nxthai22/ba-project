
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { OrderEntity } from 'entities/order.entity';
import { ProductVariantEntity } from 'entities/product-variant.entity';
import { ProductEntity } from 'entities/product.entity';
import { UserEntity } from 'entities/user.entity';

@Injectable()
export class OrderHelper {
  constructor(
    @InjectRepository(ProductEntity)
    public productRepo: Repository<ProductEntity>,

    @InjectRepository(ProductVariantEntity)
    public productVariantRepo: Repository<ProductVariantEntity>,

    @InjectRepository(UserEntity)
    public userRepo: Repository<UserEntity>,

    @InjectRepository(MerchantAddressEntity)
    public merchantAddressRepo: Repository<MerchantAddressEntity>,

    @InjectRepository(OrderEntity) public orderRepo: Repository<OrderEntity>,
  ) { }

  async findProductBySKU(SKU?: string) {
    if (!SKU) {
      return null;
    }

    return this.productRepo.findOne({
      where: {
        SKU,
      },
      relations: ['merchant'],
    });
  }

  async findVariantBySKU(SKU: string) {
    if (!SKU) {
      return null;
    }

    return this.productVariantRepo.findOne({
      where: {
        SKU,
      },
      relations: ['product', 'product.merchant'],
    });
  }

  async findProductOrVariantBySKU(
    SKU: string,
  ): Promise<[ProductEntity, ProductVariantEntity]> {
    const product = await this.findProductBySKU(SKU);

    let variant;
    if (!product) {
      variant = await this.findVariantBySKU(SKU);
    }
    return [product, variant];
  }

  async findMerchaneAddressByCode(code: string) {
    if (!code) {
      return null;
    }

    return this.merchantAddressRepo.findOne({ where: { code } });
  }

  async findPartnerByTel(tel: string) {
    if (!tel) {
      return null;
    }

    return this.userRepo.findOne({ where: { tel } });
  }

  async findOrderByImportCode(importCode: string) {
    if (!importCode) {
      return null;
    }

    return this.orderRepo.findOne({ where: { importCode } });
  }
}

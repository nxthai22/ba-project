import {
  CrudAuth,
  CrudController,
  CrudRequest,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { OrderEntity } from 'entities/order.entity';
import { OrderService } from './order.service';
import { Crud } from 'decorators/crud.decorator';
import {
  BadRequestException,
  Body,
  ForbiddenException,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
  Req,
} from '@nestjs/common';
import { ShippingFeeResponseDto } from 'modules/order/dto/shipping-fee.dto';
import {
  ApiBody,
  ApiConsumes,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
} from '@nestjs/swagger';
import { User } from 'decorators/user.decorator';
import { UserEntity } from 'entities/user.entity';
import { OrderShippingStatus, OrderStatus, ShippingPartner } from 'enums';
import { CreateOrderDto } from 'modules/order/dto/create-order.dto';
import { UpdateOrderDto } from 'modules/order/dto/update-order.dto';
import { StatusCountResponseDto } from 'modules/order/dto/status-count.response.dto';
import {
  ChangePaymentStatusDto,
  ChangePaymentTypeDto,
  ChangeShippingStatusDto,
  ChangeStatusDto,
  ConfirmDepositDto,
} from 'modules/order/dto/change-status.dto';
import { Auth } from 'decorators/auth.decorator';
import { UserService } from 'modules/user/user.service';
import { SearchResult } from 'modules/base/base-search';
import { OrderFilterDto } from './dto/filter-order.dto';
import { ShipmentRequestDto } from './dto/create-shipment.dto';
import { RoleType } from 'entities/role.entity';
import { getConnection, In, Raw } from 'typeorm';
import { ApiImplicitQuery } from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';
import { FastifyRequest } from 'fastify';
import { ImportOrderResponse } from './dto/import-order.dto';
import { StatusCountDto } from './dto/status-count.dto';

@Crud({
  name: 'Đơn hàng',
  controller: 'orders',
  model: {
    type: OrderEntity,
  },
  routes: {
    deleteOneBase: {
      decorators: [Auth()],
    },
  },
  query: {
    join: {
      user: {},
      'user.bank': {},
      'user.leaders': {
        alias: 'user_leaders',
      },
      orderProducts: {},
      'orderProducts.product': {
        alias: 'orderProductsProduct',
      },
      'orderProducts.product.stocks': {
        alias: 'orderProductsProductStocks',
      },
      'orderProducts.product.stocks.merchantAddress': {
        alias: 'orderProductsProductStocksMerchantAddress',
      },
      'orderProducts.variant': {
        alias: 'orderProductsVariant',
      },
      ward: {},
      'ward.district': {
        alias: 'district',
      },
      'ward.district.province': {},
      histories: {},
      'histories.user': {
        alias: 'user_history',
      },
      'histories.user.role': {
        alias: 'user_role_history',
      },
      merchant: {},
      'merchant.bank': {
        alias: 'merchant_bank',
      },
      'merchant.addresses': {
        alias: 'merchant_addresses',
      },
      merchantAddress: {},
      'merchantAddress.merchant': {
        alias: 'merchantAddress_merchant',
      },
      'merchantAddress.ward': {
        alias: 'merchantAddressWard',
      },
      'merchantAddress.ward.district': {
        alias: 'merchantAddressWardDistrict',
      },
      'merchantAddress.ward.district.province': {
        alias: 'merchantAddressWardDistrictProvince',
      },
      voucherHistories: {},
      'voucherHistories.voucher': {
        alias: 'voucherHistoriesVoucher',
      },
      group: {},
      'group.groupBuyingProduct': {
        alias: 'group_groupBuyingProduct',
      },
      'group.user': {
        alias: 'group_user',
      },
      'group.orders': {
        alias: 'group_orders',
      },
      paymentHistories: {},
      'paymentHistories.user': {
        alias: 'paymentHistories_user',
      },
    },
  },
})
@CrudAuth({
  property: 'user',
  filter: (user: UserEntity) => {
    if (user?.role?.type === RoleType.PARTNER) {
      return {
        userId: user.id,
      };
    }
    if ([RoleType.MERCHANT, RoleType.LEADER].includes(user?.role.type)) {
      return {
        merchantId: user.merchantId,
      };
    }
  },
})
export class OrderController implements CrudController<OrderEntity> {
  constructor(
    public readonly service: OrderService,
    public readonly userService: UserService,
  ) {}

  get base(): CrudController<OrderEntity> {
    return this;
  }

  @Override()
  getOne(@ParsedRequest() req: CrudRequest) {
    if (req.parsed.join.find((x) => x.field === 'histories')) {
      req.parsed.sort.push({
        field: 'histories.id',
        order: 'ASC',
      });
    }
    return this.base.getOneBase(req);
  }

  @Post('/fee')
  @ApiOperation({
    summary: 'Tính phí vận chuyển',
  })
  @ApiOkResponse({
    type: [ShippingFeeResponseDto],
  })
  @ApiBody({
    description: 'Thông tin đơn hàng và Đối tác vận chuyển',
    type: [ShipmentRequestDto],
  })
  @Auth()
  async calculateFee(
    @Body() dtos: ShipmentRequestDto[],
    @User() user: UserEntity,
  ): Promise<ShippingFeeResponseDto[]> {
    return this.service.calculateFee(dtos, user);
  }

  @Post('/create-shipment')
  @ApiOperation({
    summary: 'Tạo đơn hàng Vận chuyển với đối tác',
  })
  @ApiBody({
    type: [ShipmentRequestDto],
  })
  @ApiOkResponse({
    type: [OrderEntity],
  })
  @Auth()
  async createShipment(
    @Body() dtos: ShipmentRequestDto[],
    @User() user: UserEntity,
  ): Promise<OrderEntity[]> {
    return await this.service.createShipment(dtos, user);
  }

  @Post('/change-status/:id')
  @ApiOperation({
    summary: 'Thay đổi trạng thái đơn hàng',
  })
  @ApiBody({
    type: ChangeStatusDto,
  })
  @ApiOkResponse({
    type: ChangeStatusDto,
  })
  @ApiParam({
    description: 'ID của đơn hàng',
    name: 'id',
    type: Number,
  })
  @Auth()
  async changeStatus(
    @Param('id') id: number,
    @Body() dto: ChangeStatusDto,
    @User() user: UserEntity,
  ): Promise<OrderEntity> {
    return await this.service.changeStatus(id, dto, user);
  }

  @Override()
  @ApiBody({
    type: UpdateOrderDto,
  })
  @Auth()
  async updateOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: UpdateOrderDto,
    @User() user: UserEntity,
  ) {
    const order = await this.service.repo.findOne({
      id: dto.id,
    });
    if (!order) throw new NotFoundException('Không tìm thấy đơn hàng');
    if (
      ![
        OrderStatus.CREATED,
        OrderStatus.CONFIRMED,
        OrderStatus.PROCESSING,
      ].includes(order.status)
    )
      throw new BadRequestException(
        'Đơn hàng đang ở trạng thái không thể cập nhật',
      );

    // Kiểm tra sản phẩm có cùng NCC
    const products = await this.service.productRepo.find({
      where: {
        id: In(dto.products?.map((prod) => prod.id)),
      },
    });
    const merchantIds = products?.reduce((arr: number[], curr) => {
      if (!arr.includes(curr.merchantId)) {
        arr.push(curr.merchantId);
      }
      return arr;
    }, []);
    if (
      products &&
      products?.length === 1 &&
      order.merchantId !== products?.[0]?.merchantId
    ) {
      throw new BadRequestException(
        `Sản phẩm ${products?.[0]?.name} không cùng nhà cung cấp hiện tại của đơn!`,
      );
    }
    if (merchantIds?.length > 1) {
      const product1 = products?.find(
        (prod) => prod.merchantId === Math.min(...merchantIds),
      )?.name;
      const product2 = products?.find(
        (prod) => prod.merchantId === Math.max(...merchantIds),
      )?.name;
      throw new BadRequestException(
        `Sản phẩm ${product1} và ${product2} không cùng nhà cung cấp!`,
      );
    }
    // Update đơn hàng
    const tmpProducts = dto?.products ? [...dto.products] : [];
    delete dto.products;
    if (tmpProducts.length > 0) {
      await this.service.orderProductRepo.delete({
        orderId: order.id,
      });
      const data = await this.service.createOrderProduct(
        { ...dto, products: tmpProducts },
        order.id,
        user.id,
      );
      const orderProductResult = data.orderProducts;
      dto.total = data.totalPrice;
      dto.discount = data.totalVoucherDiscount;
      dto.commission = data.totalCommission;
      dto.voucherMerchantDiscount = data.voucherMerchantDiscount;
      dto.voucherUshareDiscount = data.voucherUshareDiscount;
    } else {
      dto.total = 0;
      dto.discount = 0;
    }
    return await this.base.updateOneBase(req, dto);
  }

  @Override()
  @ApiBody({
    type: CreateOrderDto,
    isArray: true,
  })
  async createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dtos: CreateOrderDto[],
    @User() user?: UserEntity,
  ) {
    return await this.service.createOrder(dtos, user);
  }

  @Get('/status-count')
  @ApiOperation({
    summary: 'Lấy số lượng đơn hàng theo trạng thái',
  })
  @ApiOkResponse({
    type: [StatusCountResponseDto],
  })
  @Auth()
  async getStatusCount(
    @User() user: UserEntity,
  ): Promise<StatusCountResponseDto[]> {
    return this.service.statusCount(user);
  }

  @Get('/status-admin-count')
  @Auth()
  @ApiOperation({
    summary: 'Lấy số lượng đơn hàng theo trạng thái, theo ngày tạo',
  })
  @ApiOkResponse({
    type: [StatusCountResponseDto],
  })
  async getStatusAdminCount(
    @Query() dto: StatusCountDto,
    @User() user: UserEntity,
  ): Promise<StatusCountResponseDto[]> {
    return this.service.statusCountAdmin(dto, user);
  }

  @Override()
  @Auth()
  @ApiImplicitQuery({
    name: 'shippingStatus',
    type: 'enum',
    enum: OrderShippingStatus,
    required: false,
    description: 'Trạng thái vận đơn',
  })
  async getMany(
    @ParsedRequest() req: CrudRequest,
    @User() user: UserEntity,
    @Query('shippingStatus') shippingStatus?: OrderShippingStatus,
  ) {
    if (
      [
        RoleType.MANAGEMENT_PARTNER,
        RoleType.LEADER,
        RoleType.MERCHANT,
        RoleType.MERCHANT_ACCOUNTANT,
      ].includes(user.role?.type)
    )
      req.parsed.search.$and.push({
        merchantId: user?.merchantId,
      });
    if (user?.role?.type === RoleType.LEADER) {
      const query = await getConnection()
        .createQueryBuilder()
        .from('user_leader', 'leader')
        .where(`"leader"."leaderId" = ${user.id}`);
      const userIds = (await query.execute())?.map((leader) => leader.userId);
      req.parsed.search.$and.push({
        userId: {
          $in: userIds,
        },
      });
    }
    if (shippingStatus) {
      const orders = await this.service.repo.find({
        where: {
          shippingInfos: Raw((alias) => {
            return `${alias} @> '[{"status":"${shippingStatus}"}]'`;
          }),
        },
      });
      if (orders?.length > 0) {
        const orderIds = orders?.map((order) => order.id);
        req.parsed.search.$and.push({
          id: {
            $in: orderIds,
          },
        });
      } else {
        return {
          data: [],
          count: 0,
          total: 0,
          page: 1,
          pageCount: 0,
        };
      }
    }
    return this.base.getManyBase(req);
  }

  @Post('/filter-order')
  @ApiOperation({
    summary: 'Tìm kiếm nhanh đơn hàng/doanh thu theo CTV',
  })
  @ApiOkResponse({
    type: SearchResult,
  })
  @ApiBody({
    type: OrderFilterDto,
  })
  @Auth()
  async FilterOrder(
    @Body() dto: OrderFilterDto,
    @User() user: UserEntity,
  ): Promise<SearchResult> {
    return this.service.FilterOrder(dto, user);
  }

  @Post('/change-payment-type')
  @ApiOperation({
    summary: 'Thay đổi hình thức thanh toán đơn hàng',
  })
  @ApiBody({
    type: ChangePaymentTypeDto,
  })
  @ApiOkResponse({
    type: [OrderEntity],
  })
  @Auth()
  async changePaymentType(
    @Body() dto: ChangePaymentTypeDto,
    @User() user: UserEntity,
  ): Promise<OrderEntity[]> {
    return await this.service.changePaymentType(dto, user);
  }

  @Patch('/update-payment-status')
  @ApiOperation({
    summary: 'Thay đổi trạng thái thanh toán đơn hàng',
  })
  @ApiBody({
    type: ChangePaymentStatusDto,
  })
  @ApiOkResponse({
    type: [OrderEntity],
  })
  @Auth()
  async updateDepositStatus(
    @Body() dto: ChangePaymentStatusDto,
    @User() user: UserEntity,
  ): Promise<OrderEntity[]> {
    return await this.service.updatePaymentStatus(dto, user);
  }

  @Post('/confirm-deposit')
  @ApiOperation({
    summary: 'Kế toán xác nhận tiền đặt cọc',
  })
  @ApiBody({
    type: ConfirmDepositDto,
  })
  @ApiOkResponse({
    type: OrderEntity,
  })
  @Auth()
  async confirmDeposit(
    @Body() dto: ConfirmDepositDto,
    @User() user: UserEntity,
  ): Promise<OrderEntity> {
    return await this.service.confirmDeposit(dto, user);
  }

  @Patch('/update-note/:id')
  @ApiOperation({
    summary: 'Cập nhật ghi chú đơn hàng',
  })
  @ApiBody({
    type: CreateOrderDto,
  })
  @ApiOkResponse({
    type: OrderEntity,
  })
  @Auth()
  async updateNote(
    @Param('id') id: number,
    @Body() dto: CreateOrderDto,
    @User() user: UserEntity,
  ): Promise<OrderEntity> {
    const order = await this.service.repo.findOne({ id: id });
    if (!order) throw new BadRequestException('Đơn hàng không tồn tại');
    return await this.service.repo.save({ ...order, note: dto.note });
  }

  @Patch('/update-shipping-status/:id')
  @ApiOperation({
    summary: 'Cập nhật trạng thái vận chuyển đơn hàng',
  })
  @ApiBody({
    type: ChangeShippingStatusDto,
  })
  @ApiOkResponse({
    type: OrderEntity,
  })
  @Auth()
  async updateShippingStatus(
    @Param('id') id: number,
    @Body() dto: ChangeShippingStatusDto,
    @User() user: UserEntity,
  ): Promise<OrderEntity> {
    const order = await this.service.repo.findOne({ id: id });
    if (!order) throw new BadRequestException('Đơn hàng không tồn tại');
    order?.shippingInfos?.map((shippingInfo) => {
      if (shippingInfo?.partner === ShippingPartner.OTHER)
        shippingInfo.status = dto.shippingStatus;
    });
    return await this.service.repo.save(order);
  }

  @Post('/import')
  @ApiOperation({
    summary: 'Import danh sách đơn hàng',
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiOkResponse({
    type: ImportOrderResponse,
  })
  async importOrder(@Req() req: FastifyRequest): Promise<ImportOrderResponse> {
    return this.service.importOrder(req);
  }

  @Auth()
  @Post('/delete-import')
  @ApiOperation({
    summary: 'Xóa danh sách đơn hàng',
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiOkResponse({
    type: ImportOrderResponse,
  })
  async deleteImportOrder(
    @Req() req: FastifyRequest,
    @User() user: UserEntity,
  ): Promise<ImportOrderResponse> {
    if (user.role?.type !== RoleType.ADMIN)
      throw new ForbiddenException('Không có quyền thao tác');
    return this.service.deleteImportOrder(req);
  }

  @Auth()
  @Get('/update-sale-amount-product')
  @ApiOperation({
    summary: 'Cập nhật lại số lượng đã bán cho sản phẩm',
  })
  async updateSaleAmountProduct(@User() user: UserEntity): Promise<void> {
    if (user.role?.type !== RoleType.ADMIN)
      throw new ForbiddenException('Không có quyền thao tác');
    await this.service.updateSaleAmountProduct();
  }

  // @Auth()
  // @Post('/find-import')
  // @ApiOperation({
  //   summary: 'Tìm kiếm danh sách đơn hàng',
  // })
  // @ApiConsumes('multipart/form-data')
  // @ApiBody({
  //   schema: {
  //     type: 'object',
  //     properties: {
  //       file: {
  //         type: 'string',
  //         format: 'binary',
  //       },
  //     },
  //   },
  // })
  // async findImportOrder(
  //   @Req() req: FastifyRequest,
  //   @User() user: UserEntity,
  // ): Promise<ImportOrderResponse> {
  //   if (user.role?.type !== RoleType.ADMIN)
  //     throw new ForbiddenException('Không có quyền thao tác');
  //   return this.service.findImportOrder(req);
  // }

  @Auth()
  @Post('/update-user-import')
  @ApiOperation({
    description: 'Cập nhật số điện thoại CTV',
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  async updateUserImport(
    @Req() req: FastifyRequest,
    @User() user: UserEntity,
  ): Promise<ImportOrderResponse> {
    if (user.role?.type !== RoleType.ADMIN)
      throw new ForbiddenException('Không có quyền thao tác');
    return this.service.updateUserImport(req);
  }
}

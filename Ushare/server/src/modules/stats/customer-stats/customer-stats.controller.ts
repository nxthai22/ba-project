import { Controller, Get, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Auth } from 'decorators/auth.decorator';
import { User } from 'decorators/user.decorator';
import { UserEntity } from 'entities/user.entity';
import * as _ from 'lodash';
import { CustomerStatsService } from './customer-stats.service';
import { CustomerChartStatsDto, CustomerStatsQueryDto } from './dto';

@Controller('customer-stats')
@ApiTags('Customer Stats')
@Auth()
export class CustomerStatsController {
  constructor(private readonly customerStatsService: CustomerStatsService) {}

  @Get('')
  @ApiOperation({
    summary: 'Thống kê khách hàng',
    description:
      'Doanh thu trung bình; biểu đồ  tổng số khách hàng/đăng ký mới/khách hàng cũ quay lại mỗi ngày trong một khoảng thời gian',
  })
  @ApiOkResponse({
    type: CustomerChartStatsDto,
  })
  async customerStats(
    @User() user: UserEntity,
    @Query() params: CustomerStatsQueryDto,
  ) {
    if (user.isUshareRole()) {
      //
    } else if (user.isMerchantRole()) {
      params.merchantId = user.merchantId;
    } else if (user.isPartner()) {
      params.userId = user.id;
    } else if (user.isLeader()) {
      params.leaderId = user.id;
    } else {
      params.userId = user.id;
    }
    const p1 = this.customerStatsService.customerRegisterStats(params);
    const p2 = this.customerStatsService.customerHaveOrderStats(params);
    const p3 = this.customerStatsService.customerRevenueAvg(params);

    const [registerStats, orderStats, revenueAvg] = await Promise.all([
      p1,
      p2,
      p3,
    ]);
    const groupByTimes = _.groupBy(
      [...registerStats, ...orderStats],
      (item) => item.time,
    );

    const charts = _.map(
      groupByTimes,
      ([registerStatsItem, orderStatsItem]) => {
        return {
          time: registerStatsItem.time,
          countNewCustomer: registerStatsItem.countNewCustomer,
          totalCustomer: registerStatsItem.totalCustomer,
          countOldCustomer:
            orderStatsItem.countCustomerHaveOrder -
            registerStatsItem.countNewCustomer,
        };
      },
    );

    return {
      revenueAvg,
      charts,
    };
  }
}

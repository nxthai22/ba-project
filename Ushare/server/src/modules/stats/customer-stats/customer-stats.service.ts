import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { StatsCustomerEntity } from 'entities/stats-customer.entity';
import { StatsOrderCompletedEntity } from 'entities/stats-order-completed.entity';
import * as _ from 'lodash';
import {
  IsNull,
  LessThan,
  LessThanOrEqual,
  MoreThanOrEqual,
  Repository,
} from 'typeorm';
import * as timePeriodFn from 'utils/timeperiod';
import { CustomerStatsQueryDto } from './dto';

@Injectable()
export class CustomerStatsService {
  constructor(
    @InjectRepository(StatsOrderCompletedEntity)
    private statsOrderCompletedRepository: Repository<StatsOrderCompletedEntity>,

    @InjectRepository(StatsCustomerEntity)
    private statsCustomerRepository: Repository<StatsCustomerEntity>,
  ) {}

  /**
   * Số khách hàng mới trong khoảng thời gian
   * @param param
   * @returns
   */
  async customerRegisterStats({
    timePeriod,
    fromDate,
    toDate,
    merchantId,
    leaderId,
    ...rest
  }: CustomerStatsQueryDto) {
    const [startTime, endTime] = timePeriodFn.formatTimePeriod(
      new Date(fromDate),
      new Date(toDate),
      timePeriod,
    );

    const totalCustomer = await this.getTotalCustomer({
      toDate: startTime,
      merchantId,
      leaderId,
    });

    const query = this.statsCustomerRepository.createQueryBuilder('mc');
    const bucket = timePeriodFn.bucketFn(
      timePeriod,
      startTime,
      '"mc"."createdAt"',
    );

    query.select([
      `${bucket} as time`,
      'count(*) as "countNewCustomer"',
      `${totalCustomer} + sum(count(*)) OVER (ORDER BY ${bucket}) as "totalCustomer"`,
    ]);

    query.where({
      ...rest,
      type: merchantId ? 'merchant' : leaderId ? 'collaborator' : 'ushare',
      merchantId: merchantId ? merchantId : IsNull(),
    });
    query.andWhere({ createdAt: MoreThanOrEqual(startTime.toISOString()) });
    query.andWhere({ createdAt: LessThanOrEqual(endTime.toISOString()) });
    query.groupBy('time');

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = mc."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    let result = await query.execute();
    const resultMapDay = _.keyBy(result, (item) => item.time.toISOString());
    let latestTotalCustomer = totalCustomer;
    result = timePeriodFn
      .eachTimeInterval(timePeriod)({ start: startTime, end: endTime })
      .map((day) => {
        const dayData = resultMapDay[day.toISOString()];
        latestTotalCustomer = dayData
          ? dayData.totalCustomer
          : latestTotalCustomer;
        return {
          time: day,
          totalCustomer: parseInt(latestTotalCustomer),
          countNewCustomer: dayData?.countNewCustomer
            ? parseInt(dayData.countNewCustomer)
            : 0,
        };
      });
    return result;
  }

  /**
   * Số khách phát sinh đơn hàng mỗi ngày trong khoảng thời gian
   * @param param
   */
  async customerHaveOrderStats({
    timePeriod,
    fromDate,
    toDate,
    leaderId,
    ...rest
  }: CustomerStatsQueryDto) {
    const [startTime, endTime] = timePeriodFn.formatTimePeriod(
      new Date(fromDate),
      new Date(toDate),
      timePeriod,
    );

    const query = this.statsOrderCompletedRepository.createQueryBuilder('soc');

    const bucket = timePeriodFn.bucketFn(
      timePeriod,
      startTime,
      '"soc"."completedAt"',
    );

    query.select([
      `${bucket} as time`,
      'count(DISTINCT (soc.tel, soc."userId", soc."merchantId")) as "countCustomerHaveOrder"',
    ]);

    query.where(rest);
    query.andWhere({ completedAt: MoreThanOrEqual(startTime.toISOString()) });
    query.andWhere({ completedAt: LessThanOrEqual(endTime.toISOString()) });
    query.groupBy('time');

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = soc."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    let result = await query.execute();
    const resultMapDay = _.keyBy(result, (item) => item.time.toISOString());

    result = timePeriodFn
      .eachTimeInterval(timePeriod)({ start: startTime, end: endTime })
      .map((day) => {
        const dayData = resultMapDay[day.toISOString()];
        return {
          time: day,
          countCustomerHaveOrder: dayData?.countCustomerHaveOrder
            ? parseInt(dayData.countCustomerHaveOrder)
            : 0,
        };
      });
    return result;
  }

  /**
   * Giá trị mua hàng trung bình của các khách hàng phát sinh đơn hàng trong khoảng thời gian
   * @param param
   */
  async customerRevenueAvg({
    timePeriod,
    fromDate,
    toDate,
    leaderId,
    ...rest
  }: CustomerStatsQueryDto) {
    const [startTime, endTime] = timePeriodFn.formatTimePeriod(
      new Date(fromDate),
      new Date(toDate),
      timePeriod,
    );

    const query = this.statsOrderCompletedRepository.createQueryBuilder('soc');

    query.select(['sum(revenue)/count(DISTINCT soc."tel") as "revenueAvg"']);
    query.innerJoin('user', 'user', '"user"."id" = "userId"');
    query.innerJoin('user.role', 'userRole', '"userRole".id = 2');

    query.where(rest);
    query.andWhere({ completedAt: MoreThanOrEqual(startTime.toISOString()) });
    query.andWhere({ completedAt: LessThanOrEqual(endTime.toISOString()) });

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = soc."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    const result = await query.execute();
    return result[0].revenueAvg;
  }

  /**
   * Tính tổng cộng khách hàng tại 1 thời điểm
   */
  async getTotalCustomer({
    toDate,
    merchantId,
    leaderId,
  }: {
    toDate: Date;
    merchantId?: number;
    leaderId?: number;
  }) {
    const query = this.statsCustomerRepository.createQueryBuilder('mc');
    query.select(['count(*)']);

    query.where({
      merchantId: merchantId ? merchantId : IsNull(),
      type: merchantId ? 'merchant' : leaderId ? 'collaborator' : 'ushare',
      createdAt: LessThan(toDate.toISOString()),
    });

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = mc."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    const result = await query.execute();
    return result[0].count;
  }
}

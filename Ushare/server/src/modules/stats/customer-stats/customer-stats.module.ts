import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StatsOrderEntity } from 'entities/stats-order.entity';
import { StatsOrderCompletedEntity } from 'entities/stats-order-completed.entity';
import { CustomerStatsController } from './customer-stats.controller';
import { CustomerStatsService } from './customer-stats.service';
import { StatsCustomerEntity } from 'entities/stats-customer.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      StatsOrderEntity,
      StatsOrderCompletedEntity,
      StatsCustomerEntity,
    ]),
  ],
  controllers: [CustomerStatsController],
  providers: [CustomerStatsService],
})
export class CustomerStatsModule {}

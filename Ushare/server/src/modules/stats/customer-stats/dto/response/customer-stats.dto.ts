import { ApiProperty } from '@nestjs/swagger';

export class CustomerStatsDto {
  @ApiProperty({
    description: 'Số khách hàng mới',
  })
  countNewCustomer: number;

  @ApiProperty({
    description: 'Tổng số khách hàng',
  })
  totalCustomer: number;

  @ApiProperty({
    description: 'Khách hàng cũ',
  })
  countOldCustomer: number;

  @ApiProperty({
    description: 'Thời gian',
  })
  time: Date;
}

export class CustomerChartStatsDto {
  @ApiProperty({
    type: CustomerStatsDto,
    isArray: true,
    description:
      'Biêủ đồ  tổng số cộng tác viên/số cộng tác viên mới/số cộng tác viên có doanh thu mỗi ngày',
  })
  charts: CustomerStatsDto[];

  @ApiProperty({
    description: 'Doanh thu trung bình',
  })
  revenueAvg: number;
}

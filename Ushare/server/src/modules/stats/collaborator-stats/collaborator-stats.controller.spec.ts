import { Test, TestingModule } from '@nestjs/testing';
import { CollaboratorStatsController } from './collaborator-stats.controller';
import { CollaboratorStatsService } from './collaborator-stats.service';

describe('CollaboratorStatsController', () => {
  let controller: CollaboratorStatsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CollaboratorStatsController],
      providers: [CollaboratorStatsService],
    }).compile();

    controller = module.get<CollaboratorStatsController>(
      CollaboratorStatsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

import { Test, TestingModule } from '@nestjs/testing';
import { CollaboratorStatsService } from './collaborator-stats.service';

describe('CollaboratorStatsService', () => {
  let service: CollaboratorStatsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CollaboratorStatsService],
    }).compile();

    service = module.get<CollaboratorStatsService>(CollaboratorStatsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

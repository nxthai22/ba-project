import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { eachDayOfInterval } from 'date-fns';
import { MerchantCollaboratorEntity } from 'entities/merchant-collaborator.entity';
import { StatsOrderCompletedEntity } from 'entities/stats-order-completed.entity';
import * as _ from 'lodash';
import {
  IsNull,
  LessThan,
  LessThanOrEqual,
  MoreThanOrEqual,
  Repository,
} from 'typeorm';
import * as timePeriodFn from 'utils/timeperiod';
import { CollaboratorStatsQueryDto } from './dto';

@Injectable()
export class CollaboratorStatsService {
  constructor(
    @InjectRepository(StatsOrderCompletedEntity)
    private statsOrderCompletedEntityRepository: Repository<StatsOrderCompletedEntity>,

    @InjectRepository(MerchantCollaboratorEntity)
    private merchantCollaboratorRepository: Repository<MerchantCollaboratorEntity>,
  ) {}

  /**
   * Số cộng tác viên đăng ký mới mỗi ngày trong khoảng thời gian
   * @param param
   * @returns
   */
  async collaboratorRegisterStats({
    timePeriod,
    fromDate,
    toDate,
    leaderId,
    merchantId,
    ...rest
  }: CollaboratorStatsQueryDto) {
    const [startTime, endTime] = [new Date(fromDate), new Date(toDate)];

    const bucket = `1 day`;

    const totalCollaborator = await this.getTotalCollaborator({
      toDate: startTime,
      leaderId,
      merchantId,
    });

    const query = this.merchantCollaboratorRepository.createQueryBuilder('mc');

    query.select([
      `time_bucket('${bucket}', "mc"."createdAt", TIMESTAMPTZ '${startTime.toISOString()}') as time`,
      'count(*) as "countRegister"',
      `${totalCollaborator} + sum(count(*)) OVER (ORDER BY time_bucket('${bucket}', mc."createdAt", TIMESTAMPTZ '${startTime.toISOString()}')) as "totalCollaborator"`,
    ]);
    query.innerJoin(
      'user',
      'user',
      'user.id = mc."userId" and "user"."roleId" = 2',
    );

    query.where({
      ...rest,
      merchantId: merchantId ? merchantId : IsNull(),
    });
    query.andWhere({ createdAt: MoreThanOrEqual(startTime.toISOString()) });
    query.andWhere({ createdAt: LessThanOrEqual(endTime.toISOString()) });
    query.groupBy('time');

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = mc."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    let result = await query.execute();
    const resultMapDay = _.keyBy(result, (item) => item.time.toISOString());
    let latestTotalCollaborator = totalCollaborator;
    result = eachDayOfInterval({ start: startTime, end: endTime }).map(
      (day) => {
        const dayData = resultMapDay[day.toISOString()];
        latestTotalCollaborator = dayData
          ? dayData.totalCollaborator
          : latestTotalCollaborator;
        return {
          time: day,
          totalCollaborator: latestTotalCollaborator,
          countRegister: dayData?.countRegister || 0,
        };
      },
    );
    return result;
  }

  /**
   * Số cộng tác viên phát sinh doanh thu mỗi ngày trong khoảng thời gian
   * @param param
   */
  async collaboratorHaveRevenueStats({
    timePeriod,
    fromDate,
    toDate,
    leaderId,
    ...rest
  }: CollaboratorStatsQueryDto) {
    const [startTime, endTime] = [new Date(fromDate), new Date(toDate)];

    const bucket = `1 day`;

    const query =
      this.statsOrderCompletedEntityRepository.createQueryBuilder('soc');

    query.select([
      `time_bucket('${bucket}', "soc"."completedAt", TIMESTAMPTZ '${startTime.toISOString()}') as time`,
      'count(DISTINCT soc."userId") as "countCollaboratorHaveRevenue"',
    ]);
    query.innerJoin('user', 'user', '"user"."id" = soc."userId"');
    query.innerJoin('user.role', 'userRole', '"userRole".id = 2');

    query.where(rest);
    query.andWhere({ completedAt: MoreThanOrEqual(startTime.toISOString()) });
    query.andWhere({ completedAt: LessThanOrEqual(endTime.toISOString()) });
    query.groupBy('time');

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = soc."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    let result = await query.execute();
    const resultMapDay = _.keyBy(result, (item) => item.time.toISOString());

    result = eachDayOfInterval({ start: startTime, end: endTime }).map(
      (day) => {
        const dayData = resultMapDay[day.toISOString()];
        return {
          time: day,
          countCollaboratorHaveRevenue:
            dayData?.countCollaboratorHaveRevenue || 0,
        };
      },
    );
    return result;
  }

  /**
   * Doanh thu trung bình của các cộng tác viên phát sinh doanh thu trong khoảng thời gian
   * @param param
   */
  async collaboratorRevenueAvg({
    timePeriod,
    fromDate,
    toDate,
    leaderId,
    ...rest
  }: CollaboratorStatsQueryDto) {
    const [startTime, endTime] = [new Date(fromDate), new Date(toDate)];

    const query =
      this.statsOrderCompletedEntityRepository.createQueryBuilder('soc');

    query.select(['sum(revenue)/count(DISTINCT soc."userId") as "revenueAvg"']);
    query.innerJoin('user', 'user', '"user"."id" = "userId"');
    query.innerJoin('user.role', 'userRole', '"userRole".id = 2');

    query.where(rest);
    query.andWhere({ completedAt: MoreThanOrEqual(startTime.toISOString()) });
    query.andWhere({ completedAt: LessThanOrEqual(endTime.toISOString()) });

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = soc."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    const result = await query.execute();
    return result[0].revenueAvg;
  }

  /**
   * Tính tổng cộng tác viên của 1 thời điểm
   */
  async getTotalCollaborator({
    toDate,
    leaderId,
    merchantId,
  }: {
    toDate: Date;
    leaderId?: number;
    merchantId?: number;
  }) {
    const query = this.merchantCollaboratorRepository.createQueryBuilder('mc');
    query.select(['count(*)']);
    query.innerJoin(
      'user',
      'user',
      'user.id = mc."userId" and "user"."roleId" = 2',
    );

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = mc."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    query.where({
      merchantId: merchantId ? merchantId : IsNull(),
      createdAt: LessThan(toDate.toISOString()),
    });

    const result = await query.execute();
    return result[0].count;
  }
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MerchantCollaboratorEntity } from 'entities/merchant-collaborator.entity';
import { StatsOrderCompletedEntity } from 'entities/stats-order-completed.entity';
import { StatsOrderEntity } from 'entities/stats-order.entity';
import { CollaboratorStatsController } from './collaborator-stats.controller';
import { CollaboratorStatsService } from './collaborator-stats.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      StatsOrderEntity,
      StatsOrderCompletedEntity,
      MerchantCollaboratorEntity,
    ]),
  ],
  controllers: [CollaboratorStatsController],
  providers: [CollaboratorStatsService],
})
export class CollaboratorStatsModule {}

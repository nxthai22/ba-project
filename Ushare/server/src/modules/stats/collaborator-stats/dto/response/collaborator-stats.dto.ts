import { ApiProperty } from '@nestjs/swagger';

export class CollaboratorStatsDto {
  @ApiProperty({
    description: 'Số cộng tác viên đăng ký mới',
  })
  countRegister: number;

  @ApiProperty({
    description: 'Tổng số cộng tác viên',
  })
  totalCollaborator: number;

  @ApiProperty({
    description: 'Số cộng tác viên có doanh thu',
  })
  countCollaboratorHaveRevenue: number;

  @ApiProperty({
    description: 'Thời gian',
  })
  time: Date;
}

export class CollaboratorChartStatsDto {
  @ApiProperty({
    type: CollaboratorStatsDto,
    isArray: true,
    description:
      'Biêủ đồ  tổng số cộng tác viên/số cộng tác viên mới/số cộng tác viên có doanh thu mỗi ngày',
  })
  charts: CollaboratorStatsDto[];

  @ApiProperty({
    description: 'Doanh thu trung bình',
  })
  revenueAvg: number;
}

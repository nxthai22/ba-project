import { Controller, Get, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Auth } from 'decorators/auth.decorator';
import { User } from 'decorators/user.decorator';
import { UserEntity } from 'entities/user.entity';
import * as _ from 'lodash';
import { CollaboratorStatsService } from './collaborator-stats.service';
import { CollaboratorChartStatsDto, CollaboratorStatsQueryDto } from './dto';

@Controller('collaborator-stats')
@ApiTags('Collaborator Stats')
@Auth()
export class CollaboratorStatsController {
  constructor(
    private readonly collaboratorStatsService: CollaboratorStatsService,
  ) {}

  @Get('')
  @ApiOperation({
    summary: 'Thống kê cộng tác viên',
    description:
      'Doanh thu trung bình; biểu đồ  tổng số cộng tác viên/đăng ký mới/có doanh thu mỗi ngày trong một khoảng thời gian',
  })
  @ApiOkResponse({
    type: CollaboratorChartStatsDto,
  })
  async collaboratorStats(
    @User() user: UserEntity,
    @Query() params: CollaboratorStatsQueryDto,
  ) {
    if (user.isUshareRole()) {
      //
    } else if (user.isMerchantRole()) {
      params.merchantId = user.merchantId;
    } else if (user.isPartner()) {
      params.userId = user.id;
    } else if (user.isLeader()) {
      params.leaderId = user.id;
    } else {
      params.userId = user.id;
    }
    const p1 = this.collaboratorStatsService.collaboratorRegisterStats(params);
    const p2 =
      this.collaboratorStatsService.collaboratorHaveRevenueStats(params);
    const p3 = this.collaboratorStatsService.collaboratorRevenueAvg(params);

    const [registerStats, revenueStats, revenueAvg] = await Promise.all([
      p1,
      p2,
      p3,
    ]);
    const groupByTimes = _.groupBy(
      [...registerStats, ...revenueStats],
      (item) => item.time,
    );

    const charts = _.map(
      groupByTimes,
      ([registerStatsItem, revenueStatsItem]) => {
        return {
          time: registerStatsItem.time,
          countRegister: parseInt(registerStatsItem.countRegister || 0),
          totalCollaborator: parseInt(registerStatsItem.totalCollaborator || 0),
          countCollaboratorHaveRevenue: parseInt(
            revenueStatsItem.countCollaboratorHaveRevenue || 0,
          ),
          revenueAvg: revenueStatsItem.revenueAvg,
        };
      },
    );

    return {
      revenueAvg,
      charts,
    };
  }
}

import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsDateString,
  IsEnum,
  IsNumber,
  IsOptional,
  Max,
  ValidateIf,
} from 'class-validator';
import { TimePeriod } from 'enums';

export enum TopPartnerSortEnum {
  order = 'order',
  revenue = 'revenue',
  commission = 'commission',
}

export class OrderTopPartnerQueryDto {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  merchantId?: number;

  @ApiProperty({
    description: 'Sắp xếp theo đơn hàng, doanh thu, hoa hồng',
    required: false,
    enum: TopPartnerSortEnum,
    default: TopPartnerSortEnum.order,
  })
  @IsEnum(TopPartnerSortEnum)
  @IsOptional()
  sort: TopPartnerSortEnum = TopPartnerSortEnum.order;

  @ApiProperty({
    description: 'Khoảng thời gian',
    enum: TimePeriod,
    required: false,
  })
  @IsEnum(TimePeriod)
  @IsOptional()
  timePeriod?: TimePeriod;

  @ApiProperty({
    description: 'Ngày bắt đầu',
    type: Date,
  })
  @IsDateString()
  fromDate: Date;

  @ApiProperty({
    description: 'Ngày kết thúc',
    type: Date,
  })
  @IsDateString()
  toDate: Date;

  @ApiProperty({
    description: 'Số lượng record',
    type: Number,
    required: false,
    maximum: 100,
    default: 10,
  })
  @Type(() => Number)
  @Max(100)
  limit?: number = 10;

  userId?: number;
  leaderId?: number;
}

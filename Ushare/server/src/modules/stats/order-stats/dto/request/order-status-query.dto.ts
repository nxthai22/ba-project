import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsEnum, IsOptional, ValidateIf } from 'class-validator';
import { TimePeriod } from 'enums';

export class OrderStatusQueryDto {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  merchantId?: number;

  @ApiProperty({
    description: 'Khoảng thời gian',
    enum: TimePeriod,
    required: false,
  })
  @IsEnum(TimePeriod)
  @IsOptional()
  timePeriod?: TimePeriod;

  @ApiProperty({
    description: 'Ngày bắt đầu',
    type: Date,
  })
  @IsDateString()
  fromDate: Date;

  @ApiProperty({
    description: 'Ngày kết thúc',
    type: Date,
  })
  @IsDateString()
  toDate: Date;

  userId?: number;
  leaderId?: number;
}

import { ApiProperty } from '@nestjs/swagger';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';

class TopStockDto {
  @ApiProperty()
  revenue: number;

  @ApiProperty()
  name: number;
}

export class OrderRevenueTopStockDto {
  @ApiProperty({
    description: 'Id kho hàng',
  })
  merchantAddressId: number;

  @ApiProperty({
    description: 'Số đơn hàng',
  })
  count: number;

  @ApiProperty({
    description: 'Tổng doanh thu/giá trị đơn hàng',
  })
  revenue: number;

  @ApiProperty({
    type: TopStockDto,
  })
  merchantAddress: TopStockDto;
}

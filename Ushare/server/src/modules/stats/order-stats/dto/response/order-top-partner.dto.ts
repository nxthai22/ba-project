import { ApiProperty } from '@nestjs/swagger';

class TopPartnerDto {
  @ApiProperty()
  userId: number;

  @ApiProperty()
  fullName: string;
}

export class OrderTopPartnerDto {
  @ApiProperty({
    description: 'Id partner',
  })
  userId: number;

  @ApiProperty({
    description: 'Số đơn hàng',
  })
  count: number;

  @ApiProperty({
    description: 'Tổng doanh thu/giá trị đơn hàng',
  })
  revenue: number;

  @ApiProperty({
    description: 'Hoa hồng',
  })
  commission: number;

  @ApiProperty({
    type: TopPartnerDto,
  })
  user: TopPartnerDto;
}

import { ApiProperty } from '@nestjs/swagger';

export class OrderRevenueStatsDto {
  @ApiProperty({
    description: 'Số đơn hàng',
  })
  count: number;

  @ApiProperty({
    description: 'Tỉ lệ đơn hàng tăng/giảm',
  })
  countIncrease: number;

  @ApiProperty({
    description: 'Tổng doanh thu/giá trị đơn hàng',
  })
  revenue: number;

  @ApiProperty({
    description: 'Thời gian',
  })
  time: Date;
}

import { ApiProperty } from '@nestjs/swagger';
import { OrderStatus } from 'enums';

export class OrderStatusStatDto {
  @ApiProperty({
    description: 'Trạng thái đơn hàng',
    enum: OrderStatus,
  })
  status: OrderStatus;

  @ApiProperty({
    description: 'Thời gian',
  })
  time: Date;

  @ApiProperty({
    description: 'Số đơn hàng',
  })
  count: number;

  @ApiProperty({
    description: 'Số sản phẩm',
  })
  countProduct: number;

  @ApiProperty({
    description: 'Số đơn hàng của chu kì trước',
  })
  prevCount: number;

  @ApiProperty({
    description: 'Số sản phẩm của chu kì trước',
  })
  prevCountProduct: number;

  @ApiProperty({
    description: 'Tổng doanh thu/giá trị đơn hàng',
  })
  revenue: number;

  @ApiProperty({
    description: 'Tỉ lệ đơn hàng tăng/giảm so với chu kì trước',
  })
  countIncrease: number;

  @ApiProperty({
    description: 'Tỉ lệ sản phẩm tăng/giảm so với chu kì trước',
  })
  countProductIncrease: number;
}

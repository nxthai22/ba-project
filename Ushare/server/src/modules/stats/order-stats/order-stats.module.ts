import { Module } from '@nestjs/common';
import { OrderStatsService } from './order-stats.service';
import { OrderStatsController } from './order-stats.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StatsOrderEntity } from 'entities/stats-order.entity';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { UserEntity } from 'entities/user.entity';
import { StatsOrderCompletedEntity } from 'entities/stats-order-completed.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      StatsOrderEntity,
      StatsOrderCompletedEntity,
      MerchantAddressEntity,
      UserEntity,
    ]),
  ],
  controllers: [OrderStatsController],
  providers: [OrderStatsService],
})
export class OrderStatsModule {}

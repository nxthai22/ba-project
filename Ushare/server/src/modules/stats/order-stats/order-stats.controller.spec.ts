import { Test, TestingModule } from '@nestjs/testing';
import { OrderStatsController } from './order-stats.controller';
import { OrderStatsService } from './order-stats.service';

describe('OrderStatsController', () => {
  let controller: OrderStatsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrderStatsController],
      providers: [OrderStatsService],
    }).compile();

    controller = module.get<OrderStatsController>(OrderStatsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

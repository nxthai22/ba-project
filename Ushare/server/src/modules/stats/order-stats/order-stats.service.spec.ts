import { Test, TestingModule } from '@nestjs/testing';
import { OrderStatsService } from './order-stats.service';

describe('OrderStatsService', () => {
  let service: OrderStatsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OrderStatsService],
    }).compile();

    service = module.get<OrderStatsService>(OrderStatsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import { Controller, Get, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Auth } from 'decorators/auth.decorator';
import { User } from 'decorators/user.decorator';
import { UserEntity } from 'entities/user.entity';
import {
  OrderRevenueStatsDto,
  OrderRevenueStatsQueryDto,
  OrderRevenueTopStockDto,
  OrderRevenueTopStockQueryDto,
  OrderStatusQueryDto,
  OrderStatusStatDto,
  OrderTopPartnerDto,
  OrderTopPartnerQueryDto,
} from './dto';
import { OrderStatsService } from './order-stats.service';

@ApiTags('Order Stats')
@Controller('order-stats')
@Auth()
export class OrderStatsController {
  constructor(private readonly orderStatsService: OrderStatsService) {}

  @Get('/status')
  @ApiOperation({
    summary: 'Thống kê trạng thái đơn hàng',
  })
  @ApiOkResponse({
    type: OrderStatusStatDto,
    isArray: true,
  })
  async orderStatusStats(
    @User() user: UserEntity,
    @Query() params: OrderStatusQueryDto,
  ) {
    if (user.isUshareRole()) {
      //
    } else if (user.isMerchantRole()) {
      params.merchantId = user.merchantId;
    } else if (user.isPartner()) {
      params.userId = user.id;
    } else if (user.isLeader()) {
      params.leaderId = user.id;
    } else {
      params.userId = user.id;
    }

    return this.orderStatsService.orderStatusStats(params);
  }

  @Get('/revenue')
  @ApiOperation({
    summary: 'Thống kê doanh thu',
  })
  @ApiOkResponse({
    type: OrderRevenueStatsDto,
    isArray: true,
  })
  async orderRevenueStats(
    @User() user: UserEntity,
    @Query() params: OrderRevenueStatsQueryDto,
  ) {
    if (user.isUshareRole()) {
      //
    } else if (user.isMerchantRole()) {
      params.merchantId = user.merchantId;
    } else if (user.isPartner()) {
      params.userId = user.id;
    } else if (user.isLeader()) {
      params.leaderId = user.id;
    } else {
      params.userId = user.id;
    }

    return this.orderStatsService.orderRevenueStats(params);
  }

  @Get('/revenue-top-stock')
  @ApiOperation({
    summary: 'Thống kê các kho hàng có doanh thu cao nhất',
  })
  @ApiOkResponse({
    type: OrderRevenueTopStockDto,
    isArray: true,
  })
  async orderRevenueTopStock(
    @User() user: UserEntity,
    @Query() params: OrderRevenueTopStockQueryDto,
  ) {
    if (user.isUshareRole()) {
      //
    } else if (user.isMerchantRole()) {
      params.merchantId = user.merchantId;
    } else if (user.isPartner()) {
      params.userId = user.id;
    } else if (user.isLeader()) {
      params.leaderId = user.id;
    } else {
      params.userId = user.id;
    }
    return this.orderStatsService.orderRevenueTopStock(params);
  }

  @Get('/top-partner')
  @ApiOperation({
    summary:
      'Thống kê các cộng tác viên có doanh thu/hoa hồng/đơn hàng nhiều nhất',
  })
  @ApiOkResponse({
    type: OrderTopPartnerDto,
    isArray: true,
  })
  async orderRevenueTopPartner(
    @User() user: UserEntity,
    @Query() params: OrderTopPartnerQueryDto,
  ) {
    if (user.isUshareRole()) {
      //
    } else if (user.isMerchantRole()) {
      params.merchantId = user.merchantId;
    } else if (user.isPartner()) {
      params.userId = user.id;
    } else if (user.isLeader()) {
      params.leaderId = user.id;
    } else {
      params.userId = user.id;
    }
    return this.orderStatsService.orderRevenueTopPartner(params);
  }
}

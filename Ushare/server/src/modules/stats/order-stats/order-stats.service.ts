import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as percentage from 'calculate-percentages';
import * as dateFns from 'date-fns';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { OrderProductEntity } from 'entities/order-product.entity';
import { StatsOrderCompletedEntity } from 'entities/stats-order-completed.entity';
import { StatsOrderEntity } from 'entities/stats-order.entity';
import { UserEntity } from 'entities/user.entity';
import * as _ from 'lodash';
import {
  In,
  LessThanOrEqual,
  MoreThanOrEqual,
  Repository,
  SelectQueryBuilder
} from 'typeorm';
import * as timePeriodFn from 'utils/timeperiod';
import {
  OrderRevenueStatsDto,
  OrderRevenueStatsQueryDto,
  OrderStatusQueryDto,
  OrderStatusStatDto,
  OrderTopPartnerDto,
  OrderTopPartnerQueryDto,
  TopPartnerSortEnum
} from './dto';

@Injectable()
export class OrderStatsService {
  constructor(
    @InjectRepository(StatsOrderEntity)
    private statsOrderEntityRepository: Repository<StatsOrderEntity>,

    @InjectRepository(StatsOrderCompletedEntity)
    private statsOrderCompletedRepository: Repository<StatsOrderCompletedEntity>,

    @InjectRepository(MerchantAddressEntity)
    private merchantAddressRepository: Repository<MerchantAddressEntity>,

    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  /**
   * Thống kê số đơn hàng theo các trạng thái
   * @param params
   * @returns
   */
  async orderStatusStats({
    timePeriod,
    fromDate,
    toDate,
    leaderId,
    ...rest
  }: OrderStatusQueryDto): Promise<OrderStatusStatDto[]> {
    const [startTime, endTime] = [new Date(fromDate), new Date(toDate)];

    const numberDay = dateFns.differenceInDays(endTime, startTime) + 1;
    const compareTime = dateFns.subDays(startTime, numberDay);

    const query = this.statsOrderEntityRepository.createQueryBuilder('so');

    query.select([
      `time_bucket('${numberDay} day', so."createdAt", TIMESTAMPTZ '${compareTime.toISOString()}') as time`,
      'count(status)',
      'sum(revenue) as revenue',
      'status',
      'sum(c."countProduct") as "countProduct"',
    ]);
    query.innerJoin(
      (qb: SelectQueryBuilder<OrderProductEntity>) => {
        return qb
          .select(['count(*) as "countProduct"', '"orderId"'])
          .from('order_product', 'op')
          .groupBy('"orderId"');
      },
      'c',
      'c."orderId" = so."orderId"',
    );

    query.groupBy('time, status');
    query.where(rest);
    query.andWhere({ createdAt: MoreThanOrEqual(compareTime.toISOString()) });
    query.andWhere({ createdAt: LessThanOrEqual(endTime.toISOString()) });
    query.orderBy('"time"', 'DESC');

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = so."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    let result = await query.execute();
    result = _.filter(result, (item) => {
      return (
        !dateFns.isBefore(new Date(item.time), compareTime) &&
        !dateFns.isAfter(new Date(item.time), endTime)
      );
    });
    const statusGroups = _.groupBy(result, (item) => item.status);

    return _.map(statusGroups, ([period1, period2]) => {
      const currPeriod = dateFns.isBefore(period1.time, startTime)
        ? { count: 0, revenue: 0 }
        : period1;
      const lastPeriod = period2
        ? period2
        : dateFns.isBefore(period1.time, startTime)
        ? period1
        : { count: 0, revenue: 0 };

      return {
        time: currPeriod.time || lastPeriod.time,
        status: currPeriod.status || lastPeriod.status,
        count: parseInt(currPeriod.count || 0),
        countProduct: parseInt(currPeriod.countProduct || 0),
        revenue: parseInt(currPeriod.revenue || 0),
        prevCount: parseInt(lastPeriod.count || 0),
        prevCountProduct: parseInt(lastPeriod.countProduct || 0),
        countIncrease: percentage.differenceBetween(
          lastPeriod.count || 0,
          currPeriod.count || 0,
        ),
        countProductIncrease: percentage.differenceBetween(
          lastPeriod.countProduct || 0,
          currPeriod.countProduct || 0,
        ),
      };
    });
  }

  /**
   * Thống kê doanh thu của các đơn thành công
   * @param params
   * @returns
   */
  async orderRevenueStats({
    timePeriod,
    fromDate,
    toDate,
    leaderId,
    ...rest
  }: OrderRevenueStatsQueryDto): Promise<OrderRevenueStatsDto[]> {
    const [startTime, endTime] = [new Date(fromDate), new Date(toDate)];

    const bucket = `1 day`;

    const query = this.statsOrderCompletedRepository.createQueryBuilder('soc');

    query.select([
      `time_bucket( '${bucket}', soc."completedAt", TIMESTAMPTZ '${startTime.toISOString()}') as time`,
      'count(*)',
      'sum(revenue) as revenue',
    ]);

    query.groupBy('time');
    query.where(rest);
    query.andWhere({ completedAt: MoreThanOrEqual(startTime.toISOString()) });
    query.andWhere({ completedAt: LessThanOrEqual(endTime.toISOString()) });
    query.orderBy('time', 'ASC');

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = soc."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    let result = await query.execute();
    const resultMapDay = _.keyBy(result, (item) => item.time.toISOString());
    result = dateFns
      .eachDayOfInterval({ start: startTime, end: endTime })
      .map((day) => {
        const dayData = resultMapDay[day.toISOString()];
        return {
          time: day,
          count: dayData?.count ? parseInt(dayData?.count) : 0,
          revenue: dayData?.revenue ? parseInt(dayData?.revenue) : 0,
        };
      });
    return result;
  }

  /**
   * Thống kê các kho hàng có doanh thu cao nhất
   * @param params
   * @returns
   */
  async orderRevenueTopStock({
    timePeriod,
    fromDate,
    toDate,
    leaderId,
    ...rest
  }: OrderRevenueStatsQueryDto): Promise<OrderRevenueStatsDto[]> {
    const [startTime, endTime] = [new Date(fromDate), new Date(toDate)];

    const query = this.statsOrderCompletedRepository.createQueryBuilder('soc');

    query.select([
      '"merchantAddressId"',
      'count(*)',
      'sum(revenue) as revenue',
    ]);

    query.groupBy('"merchantAddressId"');
    query.where(rest);
    query.andWhere({ completedAt: MoreThanOrEqual(startTime.toISOString()) });
    query.andWhere({ completedAt: LessThanOrEqual(endTime.toISOString()) });

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = soc."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    query.orderBy('revenue', 'DESC');
    query.limit(10);

    const result = await query.execute();
    const merchantAddressIds = _.map(result, 'merchantAddressId');
    const merchantAddresses = await this.merchantAddressRepository.find({
      where: { id: In(merchantAddressIds) },
      select: ['id', 'name'],
    });

    return result.map((item) => {
      const merchantAddress = _.find(merchantAddresses, {
        id: item.merchantAddressId,
      });

      return {
        ...item,
        merchantAddress,
      };
    });
  }

  /**
   * Thống kê các cộng tác viên có doanh thu cao nhất
   * @param params
   * @returns
   */
  async orderRevenueTopPartner({
    timePeriod,
    fromDate,
    toDate,
    leaderId,
    sort,
    limit = 10,
    ...rest
  }: OrderTopPartnerQueryDto): Promise<OrderTopPartnerDto[]> {
    const [startTime, endTime] = [new Date(fromDate), new Date(toDate)];

    const query = this.statsOrderCompletedRepository.createQueryBuilder('soc');

    query.select([
      'soc."userId"',
      'count(*)',
      'sum(revenue) as revenue',
      'sum(commission) as commission',
    ]);

    query.innerJoin('user', 'user', '"user"."id" = soc."userId"');
    query.innerJoin('user.role', 'userRole', '"userRole".id = 2');

    query.groupBy('soc."userId"');
    query.where(rest);
    query.andWhere({ completedAt: MoreThanOrEqual(startTime.toISOString()) });
    query.andWhere({ completedAt: LessThanOrEqual(endTime.toISOString()) });

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = soc."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    query.limit(limit);

    let orderBy = 'count';
    switch (sort) {
      case TopPartnerSortEnum.commission:
        orderBy = 'commission';
        break;
      case TopPartnerSortEnum.revenue:
        orderBy = 'revenue';
        break;
      default:
        orderBy = 'count';
    }
    query.orderBy(orderBy, 'DESC');

    const result = await query.execute();

    const userIds = _.map(result, 'userId');
    const users = await this.userRepository.find({
      where: { id: In(userIds) },
      select: ['id', 'fullName'],
    });

    return result.map((item) => {
      const user = _.find(users, {
        id: item.userId,
      });

      return {
        ...item,
        user,
      };
    });
  }
}

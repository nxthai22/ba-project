import { Module } from '@nestjs/common';
import { CollaboratorStatsModule } from './collaborator-stats/collaborator-stats.module';
import { OrderStatsModule } from './order-stats/order-stats.module';
import { ProductStatsModule } from './product-stats/product-stats.module';
import { CustomerStatsModule } from './customer-stats/customer-stats.module';

@Module({
  controllers: [],
  providers: [],
  imports: [
    OrderStatsModule,
    CollaboratorStatsModule,
    ProductStatsModule,
    CustomerStatsModule,
  ],
})
export class StatsModule {}

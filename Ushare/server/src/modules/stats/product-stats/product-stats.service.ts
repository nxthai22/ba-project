import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { eachDayOfInterval } from 'date-fns';
import { OrderProductEntity } from 'entities/order-product.entity';
import { ProductEntity } from 'entities/product.entity';
import { StatsOrderCompletedEntity } from 'entities/stats-order-completed.entity';
import { StatsOrderStatusEventEntity } from 'entities/stats-order-status-event.entity';
import { StatsOrderEntity } from 'entities/stats-order.entity';
import { StatsProductEventEntity } from 'entities/stats-product.entity';
import * as _ from 'lodash';
import {
  In,
  LessThan,
  LessThanOrEqual,
  MoreThanOrEqual,
  Repository,
  SelectQueryBuilder,
} from 'typeorm';
import * as timePeriodFn from 'utils/timeperiod';
import {
  ProductStatsQueryDto,
  TopProductQueryDto,
  TopProductSortEnum,
} from './dto';

@Injectable()
export class ProductStatsService {
  constructor(
    @InjectRepository(StatsProductEventEntity)
    private statsProductEventRepository: Repository<StatsProductEventEntity>,

    @InjectRepository(StatsOrderCompletedEntity)
    private statsOrderCompletedRepository: Repository<StatsOrderCompletedEntity>,

    @InjectRepository(StatsOrderEntity)
    private statsOrderRepository: Repository<StatsOrderEntity>,

    @InjectRepository(StatsOrderStatusEventEntity)
    private statsOrderStatusEventRepository: Repository<StatsOrderStatusEventEntity>,

    @InjectRepository(ProductEntity)
    private productRepository: Repository<ProductEntity>,
  ) {}

  /**
   * Số sản phẩm được tạo mới mỗi ngày trong khoảng thời gian
   * @param param
   * @returns
   */
  async productCreateStats({
    timePeriod,
    fromDate,
    toDate,
    merchantId,
  }: ProductStatsQueryDto) {
    const [startTime, endTime] = [new Date(fromDate), new Date(toDate)];

    const bucket = `1 day`;

    const totalProduct = await this.getTotalProduct({
      toDate: startTime,
      merchantId,
    });

    const query = this.statsProductEventRepository.createQueryBuilder('spe');

    query.select([
      `time_bucket('${bucket}', "spe"."time", TIMESTAMPTZ '${startTime.toISOString()}') as timebucket`,
      `count(1) filter (where spe.event = 'create') as "countCreate"`,
      `count(1) filter (where spe.event = 'delete') as "countDelete"`,
    ]);

    query.where({ time: MoreThanOrEqual(startTime.toISOString()) });
    query.andWhere({ time: LessThanOrEqual(endTime.toISOString()) });

    if (merchantId) {
      query.where({ merchantId });
    }
    query.groupBy('timebucket');

    let result = await query.execute();
    const resultMapDay = _.keyBy(result, (item) =>
      item.timebucket.toISOString(),
    );
    let latestTotalproduct = totalProduct;
    result = eachDayOfInterval({ start: startTime, end: endTime }).map(
      (day) => {
        const dayData = resultMapDay[day.toISOString()];
        const countCreate = dayData?.countCreate
          ? parseInt(dayData?.countCreate)
          : 0;
        const countDelete = dayData?.countCreate
          ? parseInt(dayData?.countDelete)
          : 0;

        latestTotalproduct = latestTotalproduct + countCreate - countDelete;
        return {
          time: day,
          totalProduct: latestTotalproduct,
          countCreate,
          countDelete,
        };
      },
    );
    return result;
  }

  /**
   * Số mã sản phẩm phát sinh doanh thu mỗi ngày trong khoảng thời gian
   * @param param
   */
  async productHaveRevenueStats({
    timePeriod,
    fromDate,
    toDate,
    leaderId,
    ...rest
  }: ProductStatsQueryDto) {
    const [startTime, endTime] = [new Date(fromDate), new Date(toDate)];

    const bucket = `1 day`;

    const query = this.statsOrderCompletedRepository.createQueryBuilder('soc');

    query.select([
      `time_bucket('${bucket}', "soc"."completedAt", TIMESTAMPTZ '${startTime.toISOString()}') as time`,
      'count(DISTINCT op."productId") as "countProductHaveRevenue"',
    ]);
    query.innerJoin('order_product', 'op', 'op."orderId" = soc."orderId"');

    query.where(rest);
    query.andWhere({ completedAt: MoreThanOrEqual(startTime.toISOString()) });
    query.andWhere({ completedAt: LessThanOrEqual(endTime.toISOString()) });

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = soc."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    query.groupBy('time');

    let result = await query.execute();
    const resultMapDay = _.keyBy(result, (item) => item.time.toISOString());

    result = eachDayOfInterval({ start: startTime, end: endTime }).map(
      (day) => {
        const dayData = resultMapDay[day.toISOString()];
        return {
          time: day,
          countProductHaveRevenue: dayData?.countProductHaveRevenue
            ? parseInt(dayData?.countProductHaveRevenue)
            : 0,
        };
      },
    );
    return result;
  }

  /**
   * Số  mã sản phẩm bị hoàn mỗi ngày trong khoảng thời gian
   * @param param
   */
  async productReturnStats({
    timePeriod,
    fromDate,
    toDate,
    leaderId,
    ...rest
  }: ProductStatsQueryDto) {
    const [startTime, endTime] = [new Date(fromDate), new Date(toDate)];

    const bucket = `1 day`;

    const query =
      this.statsOrderStatusEventRepository.createQueryBuilder('sose');

    query.select([
      `time_bucket('${bucket}', "sose"."time", TIMESTAMPTZ '${startTime.toISOString()}') as timebucket`,
      'count(DISTINCT op."productId") as "countProductReturn"',
    ]);
    query.innerJoin('order_product', 'op', 'op."orderId" = sose."orderId"');

    query.where({
      ...rest,
      status: 'return',
    });
    query.andWhere({ time: MoreThanOrEqual(startTime.toISOString()) });
    query.andWhere({ time: LessThanOrEqual(endTime.toISOString()) });

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = sose."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    query.groupBy('timebucket');

    let result = await query.execute();
    const resultMapDay = _.keyBy(result, (item) =>
      item.timebucket.toISOString(),
    );

    result = eachDayOfInterval({ start: startTime, end: endTime }).map(
      (day) => {
        const dayData = resultMapDay[day.toISOString()];
        return {
          time: day,
          countProductReturn: dayData?.countProductReturn
            ? parseInt(dayData?.countProductReturn)
            : 0,
        };
      },
    );
    return result;
  }

  /**
   * Tính tổng số sản phẩm của 1 thời điểm
   */
  async getTotalProduct({
    toDate,
    merchantId,
  }: {
    toDate: Date;
    merchantId?: number;
  }) {
    const query = this.statsProductEventRepository.createQueryBuilder('spe');
    query.select([
      `
        count(1) filter (where spe.event = 'create') -
        count(1) filter (where spe.event = 'delete')
        as count
      `,
    ]);

    query.where({
      time: LessThan(toDate.toISOString()),
    });

    if (merchantId) {
      query.andWhere({ merchantId });
    }

    const result = await query.execute();
    return parseInt(result[0].count);
  }

  async topProduct({
    timePeriod,
    fromDate,
    toDate,
    leaderId,
    sort,
    limit = 10,
    ...rest
  }: TopProductQueryDto) {
    const [startTime, endTime] = [new Date(fromDate), new Date(toDate)];

    const query = this.statsOrderCompletedRepository.createQueryBuilder('soc');

    query.select([
      '"productId"',
      'sum(op."saleQuantity") as "saleQuantity"',
      'sum(op."saleRevenue") as "saleRevenue"',
    ]);
    query.innerJoin(
      (qb: SelectQueryBuilder<OrderProductEntity>) => {
        return qb
          .select([
            '"orderId"',
            '"productId"',
            'o.quantity as "saleQuantity"',
            'o.quantity * o.price as "saleRevenue"',
          ])
          .from('order_product', 'o');
      },
      'op',
      'op."orderId" = soc."orderId"',
    );

    query.groupBy('op."productId"');
    query.where(rest);
    query.andWhere({ completedAt: MoreThanOrEqual(startTime.toISOString()) });
    query.andWhere({ completedAt: LessThanOrEqual(endTime.toISOString()) });

    if (leaderId) {
      query.innerJoin(
        'user_leader',
        'leader',
        '"leader"."userId" = soc."userId"',
      );
      query.andWhere('"leader"."leaderId" = :leaderId', {
        leaderId,
      });
    }

    query.limit(limit);

    let orderBy = '"saleQuantity"';
    switch (sort) {
      case TopProductSortEnum.sale:
        orderBy = '"saleQuantity"';
        break;
      case TopProductSortEnum.revenue:
        orderBy = '"saleRevenue"';
        break;
      default:
        orderBy = '"saleQuantity"';
    }
    query.orderBy(orderBy, 'DESC');

    const result = await query.execute();

    const productIds = _.map(result, 'productId');
    const products = await this.productRepository.find({
      where: { id: In(productIds) },
      select: ['id', 'name'],
    });

    return result.map((item) => {
      const product = _.find(products, {
        id: item.productId,
      });

      return {
        ...item,
        product,
      };
    });
  }
}

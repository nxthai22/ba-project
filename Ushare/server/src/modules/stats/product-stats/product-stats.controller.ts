import { Controller, Get, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Auth } from 'decorators/auth.decorator';
import { User } from 'decorators/user.decorator';
import { UserEntity } from 'entities/user.entity';
import * as _ from 'lodash';
import { ProductChartStatsDto, ProductStatsQueryDto, TopProductDto, TopProductQueryDto } from './dto';
import { ProductStatsService } from './product-stats.service';

@Controller('product-stats')
@ApiTags('Product Stats')
@Auth()
export class ProductStatsController {
  constructor(private readonly productStatsService: ProductStatsService) {}

  @Get('')
  @ApiOperation({
    summary: 'Thống kê sản phẩm',
    description: 'Biểu đồ  tổng số sản phẩm/phát sinh doanh thu/hoàn về',
  })
  @ApiOkResponse({
    type: ProductChartStatsDto,
  })
  async collaboratorStats(
    @User() user: UserEntity,
    @Query() params: ProductStatsQueryDto,
  ) {
    if (user.isUshareRole()) {
      //
    } else if (user.isMerchantRole()) {
      params.merchantId = user.merchantId;
    } else if (user.isPartner()) {
      params.userId = user.id;
    } else if (user.isLeader()) {
      params.leaderId = user.id;
    } else {
      params.userId = user.id;
    }

    const p1 = this.productStatsService.productCreateStats(params);
    const p2 = this.productStatsService.productHaveRevenueStats(params);
    const p3 = this.productStatsService.productReturnStats(params);
    const [createStats, revenueStats, returnStats] = await Promise.all([
      p1,
      p2,
      p3,
    ]);
    const groupByTimes = _.groupBy(
      [...createStats, ...revenueStats, ...returnStats],
      (item) => item.time,
    );

    const charts = _.map(
      groupByTimes,
      ([createStatItem, revenueStatItem, returnStatItem]) => {
        return {
          time: createStatItem.time,
          totalProduct: parseInt(createStatItem.totalProduct || 0),
          countCreate: parseInt(createStatItem.countCreate || 0),
          countDelete: parseInt(createStatItem.countDelete || 0),
          countProductHaveRevenue: revenueStatItem.countProductHaveRevenue,
          countProductReturn: returnStatItem.countProductReturn,
        };
      },
    );

    return {
      charts,
    };
  }

  @Get('/top-product')
  @ApiOperation({
    summary: 'Thống kê các sản phẩm có doanh số/doanh thu/tồn kho nhiều nhất',
  })
  @ApiOkResponse({
    type: TopProductDto,
    isArray: true,
  })
  async orderRevenueTopPartner(
    @User() user: UserEntity,
    @Query() params: TopProductQueryDto,
  ) {
    if (user.isUshareRole()) {
      //
    } else if (user.isMerchantRole()) {
      params.merchantId = user.merchantId;
    } else if (user.isPartner()) {
      params.userId = user.id;
    } else if (user.isLeader()) {
      params.leaderId = user.id;
    } else {
      params.userId = user.id;
    }
    return this.productStatsService.topProduct(params);
  }
}

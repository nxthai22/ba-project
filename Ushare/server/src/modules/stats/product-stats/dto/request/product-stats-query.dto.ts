import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsDate, IsEnum, IsInt, IsOptional, ValidateIf } from 'class-validator';
import { TimePeriod } from 'enums';

export class ProductStatsQueryDto {
  @ApiProperty({
    required: false,
  })
  @IsInt()
  @Type(() => Number)
  @IsOptional()
  merchantId?: number;

  @ApiProperty({
    description: 'Khoảng thời gian',
    enum: TimePeriod,
    required: false,
  })
  @IsEnum(TimePeriod)
  @IsOptional()
  timePeriod?: TimePeriod;

  @ApiProperty({
    description: 'Ngày bắt đầu',
    type: Date,
  })
  @IsDate()
  @Type(() => Date)
  fromDate: Date;

  @ApiProperty({
    description: 'Ngày kết thúc',
    type: Date,
  })
  @IsDate()
  @Type(() => Date)
  toDate: Date;

  userId?: number;
  leaderId?: number;
}

import { ApiProperty } from '@nestjs/swagger';

export class ProductStatsDto {
  @ApiProperty({
    description: 'Tổng số sản phẩm',
  })
  totalProduct: number;

  @ApiProperty({
    description: 'Số sản phẩm tạo mới',
  })
  countCreate: number;

  @ApiProperty({
    description: 'Số sản phẩm bị xóa',
  })
  countDelete: number;

  @ApiProperty({
    description: 'Số sản phẩm có doanh thu',
  })
  countProductHaveRevenue: number;

  @ApiProperty({
    description: 'Số sản phẩm hoàn về',
  })
  countProductReturn: number;

  @ApiProperty({
    description: 'Thời gian',
  })
  time: Date;
}

export class ProductChartStatsDto {
  @ApiProperty({
    type: ProductStatsDto,
    isArray: true,
    description:
      'Biêủ đồ  tổng số sản phẩm/số sản phẩm phát sinh doanh thu/số sản phẩm hoàn về',
  })
  charts: ProductStatsDto[];
}

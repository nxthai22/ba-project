import { ApiProperty } from '@nestjs/swagger';

class TopProductInfoDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  name: string;
}

export class TopProductDto {
  @ApiProperty({
    description: 'Id product',
  })
  productId: number;

  @ApiProperty({
    description: 'Số sản phẩm đã bán',
  })
  saleQuantity: number;

  @ApiProperty({
    description: 'Tổng doanh thu',
  })
  saleRevenue: number;

  @ApiProperty({
    type: TopProductInfoDto,
  })
  product: TopProductInfoDto;
}

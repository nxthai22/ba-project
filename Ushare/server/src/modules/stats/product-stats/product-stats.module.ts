import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductEntity } from 'entities/product.entity';
import { StatsOrderCompletedEntity } from 'entities/stats-order-completed.entity';
import { StatsOrderStatusEventEntity } from 'entities/stats-order-status-event.entity';
import { StatsOrderEntity } from 'entities/stats-order.entity';
import { StatsProductEventEntity } from 'entities/stats-product.entity';
import { ProductStatsController } from './product-stats.controller';
import { ProductStatsService } from './product-stats.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      StatsProductEventEntity,
      StatsOrderCompletedEntity,
      StatsOrderEntity,
      StatsOrderStatusEventEntity,
      ProductEntity,
    ]),
  ],
  controllers: [ProductStatsController],
  providers: [ProductStatsService],
})
export class ProductStatsModule {}

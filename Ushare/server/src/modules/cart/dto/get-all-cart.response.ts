import { ApiProperty } from '@nestjs/swagger';
import { MerchantEntity } from 'entities/merchant.entity';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { CartProductEntity } from 'entities/cart-product.entity';
import { VoucherEntity } from 'entities/voucher.entity';

export class GetAllCartResponse {
  @ApiProperty({
    description: 'Kho hàng',
    type: MerchantAddressEntity,
  })
  merchantAddress: MerchantAddressEntity;

  @ApiProperty({
    description: 'Sản phẩm',
    type: [CartProductEntity],
  })
  products: CartProductEntity[];

  @ApiProperty({
    description: 'Tổng tiền hàng',
    type: Number,
  })
  totalPriceBeforeDiscount?: number;

  @ApiProperty({
    description: 'Giảm giá tiền hàng',
    type: Number,
  })
  totalDiscountPrice?: number;

  @ApiProperty({
    description: 'Tổng tiền thanh toán',
    type: Number,
  })
  totalPrice?: number;

  @ApiProperty({
    description: 'Giảm giá từ mã ưu đãi',
    type: Number,
  })
  totalVoucherDiscount?: number;

  @ApiProperty({
    description: 'Tổng hoa hồng sản phẩm',
    type: Number,
  })
  totalCommission?: number;

  @ApiProperty({
    description: 'Mã giảm giá',
    type: VoucherEntity,
  })
  voucher?: VoucherEntity;
}

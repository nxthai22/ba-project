import { Controller, Get } from '@nestjs/common';
import { CartService } from './cart.service';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { GetAllCartResponse } from 'modules/cart/dto/get-all-cart.response';
import { Auth } from 'decorators/auth.decorator';
import { User } from 'decorators/user.decorator';
import { UserEntity } from 'entities/user.entity';

@Controller('cart')
@ApiTags('Giỏ hàng')
export class CartController {
  constructor(private readonly service: CartService) {}

  @Get()
  @Auth()
  @ApiOperation({
    summary: 'Giỏ hàng',
  })
  @ApiOkResponse({
    type: [GetAllCartResponse],
  })
  async getAll(@User() user: UserEntity): Promise<GetAllCartResponse[]> {
    return await this.service.getAll(user);
  }
}

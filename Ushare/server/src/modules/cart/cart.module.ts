import { Module } from '@nestjs/common';
import { CartService } from './cart.service';
import { CartController } from './cart.controller';
import { CartProductModule } from 'modules/cart-product/cart-product.module';
import { VoucherModule } from '../voucher/voucher.module';
import { FlashSaleModule } from 'modules/flash-sale/flash-sale.module';

@Module({
  imports: [CartProductModule, VoucherModule, FlashSaleModule],
  controllers: [CartController],
  providers: [CartService],
})
export class CartModule {}

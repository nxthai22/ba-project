import { Injectable } from '@nestjs/common';
import { CartProductService } from 'modules/cart-product/cart-product.service';
import { GetAllCartResponse } from 'modules/cart/dto/get-all-cart.response';
import { UserEntity } from 'entities/user.entity';
import { _, groupBy, sortBy } from 'lodash';
import { VoucherService } from '../voucher/voucher.service';
import { FlashSaleService } from 'modules/flash-sale/flash-sale.service';
import { CreateOrderProduct } from 'modules/order/dto/create-order.dto';
import { ProductStatus } from '../../entities/product.entity';

@Injectable()
export class CartService {
  constructor(
    public cartProductService: CartProductService,
    public voucherService: VoucherService,
    public flashSaleService: FlashSaleService,
  ) {}

  async getAll(user: UserEntity): Promise<GetAllCartResponse[]> {
    const products = await this.cartProductService.repo.find({
      where: {
        userId: user.id,
      },
      order: { createdAt: 'DESC' },
      relations: ['product', 'merchantAddress', 'productVariant'],
    });
    products?.map((prod) => {
      if (prod?.product?.status !== ProductStatus.ACTIVE) prod.checked = false;
    });
    const groupByMerchantAddress = groupBy(products, 'merchantAddressId');
    const response: GetAllCartResponse[] = [];
    let shouldApplyUshareVoucher = true;
    const groupByMerchant = Object.keys(groupByMerchantAddress);
    for (let i = 0; i < groupByMerchant.length; i++) {
      const merchantAddressId = groupByMerchant[i];
      const cart = new GetAllCartResponse();
      cart.merchantAddress =
        groupByMerchantAddress[merchantAddressId][0].merchantAddress;
      cart.products = groupByMerchantAddress[merchantAddressId];
      let voucherCodes = [];
      cart.products
        .filter((v) => v.voucherCode || v.voucherUshareCode)
        .forEach((v) => {
          if (v.voucherCode) voucherCodes.push(v.voucherCode);
          if (v.voucherUshareCode) voucherCodes.push(v.voucherUshareCode);
        });
      voucherCodes = _.uniq(voucherCodes);
      // Tính giảm giá cho đơn hàng
      const createOrderProducts = cart.products
        .filter((cp) => cp.checked)
        .map((cp) => {
          const p = new CreateOrderProduct();
          p.quantity = cp.quantity;
          p.id = cp.productId;
          p.variantId = cp.productVariantId;
          return p;
        });

      // Update for unchecked item
      const uncheckedProducts = cart.products
        .filter((cp) => !cp.checked)
        .map((cp) => {
          const p = new CreateOrderProduct();
          p.quantity = cp.quantity;
          p.id = cp.productId;
          p.variantId = cp.productVariantId;
          return p;
        });

      if (uncheckedProducts.length > 0) {
        const data =
          await this.voucherService.calculateDiscountFlashSaleAndVoucher(
            uncheckedProducts,
            null,
            [],
            Number(merchantAddressId),
            shouldApplyUshareVoucher,
          );
        // Mapping price with flash sale
        for (let i = 0; i < cart.products.length; i++) {
          const prod = cart.products[i];
          if (!prod.checked) {
            // remove voucher
            cart.products[i].voucherCode = null;
            cart.products[i].voucherUshareCode = null;
            // update flashsale price
            const orderProduct = data.orderProducts.find(
              (op) => op.productId === prod.productId,
            );
            if (orderProduct) {
              prod.product.price = orderProduct.price;
              if (prod.productVariant)
                prod.productVariant.price = orderProduct.price;
              prod.commission = orderProduct.commission;
            }
            if (data.voucherMerchant) {
              cart.products[i].voucherCode = data.voucherMerchant.code;
            } else {
              cart.products[i].voucherCode = null;
            }
            if (data.voucherUshare) {
              cart.products[i].voucherUshareCode = data.voucherUshare.code;
            } else {
              cart.products[i].voucherUshareCode = null;
            }
          }
        }
      }

      if (createOrderProducts.length === 0) {
        response.push(cart);
      } else {
        const data =
          await this.voucherService.calculateDiscountFlashSaleAndVoucher(
            createOrderProducts,
            null,
            voucherCodes,
            Number(merchantAddressId),
            shouldApplyUshareVoucher,
          );
        cart.totalVoucherDiscount = data.totalVoucherDiscount;
        cart.totalPriceBeforeDiscount = data.totalPriceBeforeDiscount;
        cart.totalCommission = data.totalCommission;
        cart.totalPrice = data.totalPrice;
        cart.totalDiscountPrice = data.totalVoucherDiscount;
        cart.voucher = data.voucherMerchant ? data.voucherMerchant : null; // app uses this field to show VoucherCode of merchant
        if (data.voucherUshare) {
          shouldApplyUshareVoucher = false;
        }
        const orderProducts = data.orderProducts;
        // Mapping price with flash sale
        for (let i = 0; i < cart.products.length; i++) {
          const prod = cart.products[i];
          const orderProduct = orderProducts.find(
            (op) => op.productId === prod.productId,
          );
          if (orderProduct) {
            prod.product.price = orderProduct.price;
            if (prod.productVariant)
              prod.productVariant.price = orderProduct.price;
            prod.commission = orderProduct.commission;
          }
          if (data.voucherMerchant) {
            cart.products[i].voucherCode = data.voucherMerchant.code;
          } else {
            cart.products[i].voucherCode = null;
          }
          if (data.voucherUshare) {
            cart.products[i].voucherUshareCode = data.voucherUshare.code;
          } else {
            cart.products[i].voucherUshareCode = null;
          }
        }
        response.push(cart);
      }
    }
    return sortBy(response, 'merchantAddress.id');
  }
}

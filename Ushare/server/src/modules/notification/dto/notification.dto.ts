import { ApiProperty, PickType } from "@nestjs/swagger";
import { NotificationType } from "entities/notification-config.entity";
import { NotificationEntity } from "entities/notification.entity";

export class NotificationFilterWithPaging {
    @ApiProperty({
        description: 'Số lượng record/trang',
        type: Number,
        required: true,
        default: 10,
    })
    limit: number;

    @ApiProperty({
        description: 'Trang',
        type: Number,
        required: true,
        default: 1,
    })
    page: number;


    @ApiProperty({
        description: 'Loại',
        type: 'enum',
        enum: NotificationType,
        required: true,
    })
    type: NotificationType

}


export class NotificationDto extends PickType(NotificationEntity, [
    'id',
    'title',
    'body',
    'type',
    'data',
    'image',
    'attachedFile',
    'createdAt'
]) {

    @ApiProperty({
        description: 'Read',
        type: Boolean,
    })
    read: boolean;
}

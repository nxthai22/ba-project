import { NotificationType } from "entities/notification-config.entity";

export class FcmMessage {
  title: string;
  body: string;
  type: NotificationType;
  data: any;
}

export class FcmMulticastMessage extends FcmMessage {
  userIds: number[];
}

export class FcmTopicMessage extends FcmMulticastMessage {
  topic: string;
}

export class FcmTokenMessage extends FcmMessage {
  userId: number;
}

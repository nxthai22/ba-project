import { ApiProperty } from '@nestjs/swagger';
import { NotificationType } from 'entities/notification-config.entity';

export class NotificationCounterDto {
  @ApiProperty({
    description: 'Loại thông báo',
    type: 'enum',
    enum: NotificationType,
  })
  type: NotificationType;

  @ApiProperty({
    description: 'Tiêu đề thông báo gần nhất',
    type: String,
    required: false,
  })
  subTitle?: string;

  @ApiProperty({
    description: 'Số lượng thông báo chưa đọc',
    type: Number,
    required: false,
  })
  unreadCount?: number;
}

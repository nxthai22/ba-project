import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { CrudRequest, GetManyDefaultResponse } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { subMinutes } from 'date-fns';
import {
  NotificationConfigEntity,
  NotificationReceivedType,
  NotificationStatus,
  NotificationType,
} from 'entities/notification-config.entity';
import { NotificationTokenEntity } from 'entities/notification-token.entity';
import { NotificationUserEntity } from 'entities/notification-user.entity';
import {
  NotificationEntity,
  NotificationTrigger,
} from 'entities/notification.entity';
import { RoleType } from 'entities/role.entity';
import { UserEntity } from 'entities/user.entity';
import * as firebase from 'firebase-admin';
import {
  Message,
  MessagingPayload,
  MulticastMessage,
} from 'firebase-admin/lib/messaging/messaging-api';
import { UserService } from 'modules/user/user.service';
import { Between, getManager, In, Repository } from 'typeorm';
import {
  FcmMulticastMessage,
  FcmTokenMessage,
  FcmTopicMessage,
} from './dto/fcm-message.dto';
import { NotificationCounterDto } from './dto/notification-counter.dto';
import { RegisterFcmTokenDto } from './dto/register-fcm-token.dto';
import { _ } from 'lodash';
import {
  NotificationDto,
  NotificationFilterWithPaging,
} from './dto/notification.dto';
import { OrderPaymentStatus, OrderStatus } from 'enums';
import { OrderEntity } from 'entities/order.entity';
import { convertOrderStatusToText } from 'utils';

export enum FcmTopic {
  USHARE_ANNOUNCEMENT = 'ushare-announcement',
  USHARE_STAFF = 'ushare-staff',
  MERCHANT_STAFF = 'ushare-merchant-{id}-staff',
  USHARE_COLLABORATOR = 'ushare-collaborator',
  MERCHANT_COLLABORATOR = 'ushare-merchant-{id}-collaborator',
}

@Injectable()
export class NotificationService extends TypeOrmCrudService<NotificationEntity> {
  constructor(
    @InjectRepository(NotificationEntity)
    public repo: Repository<NotificationEntity>,
    @InjectRepository(NotificationTokenEntity)
    public notificationTokenRepo: Repository<NotificationTokenEntity>,
    @InjectRepository(NotificationConfigEntity)
    public notificationConfigRepo: Repository<NotificationConfigEntity>,
    @InjectRepository(NotificationUserEntity)
    public notificationUserRepo: Repository<NotificationUserEntity>,
    private userService: UserService,
  ) {
    super(repo);
  }
  private readonly logger = new Logger(NotificationService.name);

  async countUnread(user: UserEntity): Promise<NotificationCounterDto[]> {
    const query = this.notificationUserRepo
      .createQueryBuilder('notificationUser')
      .leftJoin(
        'notification',
        'notification',
        '"notificationUser"."notificationId" = "notification"."id"',
      )
      .where('"userId" = :userId AND read = :read', {
        userId: user.id,
        read: false,
      })
      .groupBy('"notification"."type"')
      .select('"notification"."type"')
      .addSelect('COUNT("notification"."id") AS count');
    const countData = (await query.execute()) as {
      type: string;
      count: number;
    }[];
    const response: NotificationCounterDto[] = [];

    // get first latest notification each type
    const entityManager = getManager();
    const notiData = (await entityManager.query(
      `select 
            distinct on (ntype) ntype,
            ntitle as title, 
            ncreate as createdAt
        from 
          (select
            "notification"."type" as ntype,
            "notification"."id" as nid, 
            "notification"."title" as ntitle,
            "notification"."createdAt" as ncreate
          from
            "notification_user" "notificationUser"
          left join "notification" "notification" on
            "notificationUser"."notificationId" = "notification"."id"
            and "notification"."deletedAt" is null
          where
            ( "userId" = $1 )
              and ( "notificationUser"."deletedAt" is null )
            group by
              "notification"."type",
              "notification"."id"
            order by
              "notification"."type") as ndata
        group by 
          ntype,
          ntitle,
          nid,
          ncreate
        order by 
          ntype,
          ncreate desc`,
      [user.id],
    )) as {
      ntype: string;
      title: string;
      createdAt: Date;
    }[];
    const data = {};
    countData.forEach((el) => {
      data[el.type] = { count: Number(el.count || 0) };
    });
    notiData.forEach((el) => {
      data[el.ntype] = { ...data[el.ntype], title: el.title };
    });

    Object.values(NotificationType).forEach((type) => {
      response.push({
        type: type,
        subTitle: data[type] ? data[type].title : '',
        unreadCount: data[type] ? (data[type].count ? data[type].count : 0) : 0,
      });
    });
    return response;
  }

  async getDetail(user: UserEntity, id: number): Promise<NotificationEntity> {
    const notiUser = await this.notificationUserRepo.findOne({
      where: {
        userId: user.id,
        notificationId: id,
      },
    });
    if (!notiUser) {
      throw new BadRequestException('Thông báo không tồn tại');
    }
    const noti = this.repo.findOne(id);

    // Mark as read
    await this.notificationUserRepo
      .createQueryBuilder()
      .update(NotificationUserEntity)
      .set({ read: true })
      .where(`"userId" = :userId AND "notificationId" = :notificationId `, {
        userId: user.id,
        notificationId: id,
      })
      .execute();

    return noti;
  }

  async getNotificationByType(
    user: UserEntity,
    req: NotificationFilterWithPaging,
  ): Promise<GetManyDefaultResponse<NotificationDto>> {
    const page = Number(req.page || 1);
    const limit = Number(req.limit || 0);
    const type = req.type;
    const query = this.repo
      .createQueryBuilder('notification')
      .leftJoinAndSelect(
        'notification_user',
        'notificationUser',
        '"notificationUser"."notificationId" = "notification"."id"',
      )
      .where(
        '"notificationUser"."userId" = :userId AND "notification"."type" = :type',
        {
          userId: user.id,
          type: type,
        },
      )
      .select('distinct on ("notification"."id") "notification"."id" ')
      .addSelect('"notification".*')
      .addSelect('"notificationUser"."read" as read')
      .orderBy('"notification"."id"', 'DESC')
      .addOrderBy('"notification"."createdAt"', 'DESC');
    const total = (await query.execute())?.length | 0;
    let pageCount = 1;
    if (limit) {
      query.limit(limit).offset((page - 1) * limit);
      pageCount = Math.ceil(total / limit);
    }
    const data = (await query.execute()) as NotificationDto[] | [];

    // mark as read
    if (page == 1) {
      await this.notificationUserRepo
        .createQueryBuilder()
        .update(NotificationUserEntity)
        .set({ read: true })
        .where(
          `"id" IN 
          (SELECT "nu"."id" FROM "notification_user" "nu" 
          INNER JOIN "notification" "n" ON "n"."id" = "nu"."notificationId" 
          WHERE "nu"."userId" = :userId AND "n"."type" = :type)`,
          {
            userId: user.id,
            type: type,
          },
        )
        .execute();
    }

    return {
      data: data,
      page: page,
      count: data?.length | 0,
      total: total,
      pageCount: pageCount,
    };
  }

  async registerFcmToken(dto: RegisterFcmTokenDto, user: UserEntity) {
    this.logger.log({
      method: 'registerFcmToken',
      dto: dto,
      userId: user.id,
    });
    // check if token exists on other account
    const currentTokenOfUser = await this.notificationTokenRepo.findOne({
      where: { fcmToken: dto.fcmToken },
    });

    // subscribe to corresponding topics
    const currentUser = await this.userService.repo.findOne(user.id, {
      relations: ['merchants'],
    });
    const merchantIds = currentUser.merchants?.map((m) => m.id) || [];
    const subscribeTopics = [FcmTopic.USHARE_ANNOUNCEMENT.toString()];
    switch (user.role?.type) {
      case RoleType.ADMIN:
        // admin ushare, no need to subscribe any thing
        break;
      case RoleType.MERCHANT:
        // Merchant should receive messages from ushare
        subscribeTopics.push(FcmTopic.USHARE_STAFF);
        break;
      case RoleType.ACCOUNTANT:
        // Ushare accountant is ushare's staff
        subscribeTopics.push(FcmTopic.USHARE_STAFF);
        break;
      case RoleType.LEADER:
      case RoleType.MANAGEMENT_PARTNER:
      case RoleType.MERCHANT_ACCOUNTANT:
        // ushare staff
        subscribeTopics.push(FcmTopic.USHARE_STAFF);
        // merchant staff
        for (let i = 0; i < merchantIds.length; i++) {
          const merchantId = merchantIds[i];
          subscribeTopics.push(
            FcmTopic.MERCHANT_STAFF.replace('{id}', String(merchantId)),
          );
        }
        break;
      case RoleType.PARTNER:
        // ushare collaborator
        subscribeTopics.push(FcmTopic.USHARE_COLLABORATOR);
        // merchant collaborator
        for (let i = 0; i < merchantIds.length; i++) {
          const merchantId = merchantIds[i];
          subscribeTopics.push(
            FcmTopic.MERCHANT_COLLABORATOR.replace('{id}', String(merchantId)),
          );
        }
        break;
      case RoleType.OTHER:
        break;
    }

    // save the token with user
    let notificationTokenEntity;
    if (currentTokenOfUser) {
      currentTokenOfUser.userId = user.id;
      currentTokenOfUser.topics = subscribeTopics;
      notificationTokenEntity = await this.notificationTokenRepo.save(
        currentTokenOfUser,
      );
    } else {
      const notificationToken: NotificationTokenEntity = {
        fcmToken: dto.fcmToken,
        userId: user.id,
        topics: subscribeTopics,
      };
      notificationTokenEntity = await this.notificationTokenRepo.save(
        notificationToken,
      );
    }

    for (let i = 0; i < subscribeTopics.length; i++) {
      const topic = subscribeTopics[i];
      firebase
        .messaging()
        .subscribeToTopic(dto.fcmToken, topic)
        .then((response) => {
          this.logger.log('Subscribe to topic: ', {
            token: dto.fcmToken,
            topic: topic,
            response,
          });
          if (response.failureCount > 0) {
            throw new BadRequestException('Token không hợp lệ');
          }
        })
        .catch((error) => {
          this.logger.error('Error subscribing to topic:', error);
          throw new BadRequestException('Token không hợp lệ');
        });
    }
    return notificationTokenEntity;
  }

  async unregisterFcmToken(dto: RegisterFcmTokenDto, user: UserEntity) {
    this.logger.log({
      method: 'unregisterFcmToken',
      dto: dto,
      userId: user.id,
    });
    const currentTokenOfUser = await this.notificationTokenRepo.findOne({
      where: { fcmToken: dto.fcmToken, userId: user.id },
    });
    if (!currentTokenOfUser) {
      throw new BadRequestException('Token không hợp lệ');
    }

    if (currentTokenOfUser.topics) {
      currentTokenOfUser.topics.forEach((topic) => {
        firebase
          .messaging()
          .unsubscribeFromTopic(dto.fcmToken, topic)
          .then((response) => {
            this.logger.log('Unsubscribe to topic: ', {
              token: dto.fcmToken,
              topic: topic,
              response,
            });
            if (response.failureCount > 0) {
              throw new BadRequestException('Token không hợp lệ');
            }
          })
          .catch((error) => {
            this.logger.error('Error unsubscribe to topic:', error);
            throw new BadRequestException('Token không hợp lệ');
          });
      });
    }
  }

  async sendNotificationToUsers(
    msg: FcmMulticastMessage,
    notiConfig?: NotificationConfigEntity,
  ) {
    this.logger.log({
      method: 'sendAutoNotificationToUsers',
      msg,
    });
    const notiLog: any = {};
    const notificationTokens = await this.notificationTokenRepo.find({
      where: { userId: In(msg.userIds) },
    });
    const tokens = notificationTokens.map((nt) => nt.fcmToken);
    const noti: NotificationEntity = await this.makeNotificationHistory(msg);
    const msgData = {
      ...msg.data,
      notiId: noti.id,
    };
    if (tokens && tokens.length > 0) {
      const message: MulticastMessage = {
        tokens: tokens,
        data: {
          type: msg.type,
          data: JSON.stringify(msgData),
        },
        notification: {
          title: msg.title,
          body: msg.body,
        },
        // custom icon for web
        webpush: {
          fcmOptions: {
            link: '/dashboard',
          },
          notification: {
            icon: '/logo.png',
          },
        },
      };
      notiLog.message = message;
      await firebase
        .messaging()
        .sendMulticast(message)
        .then((response) => {
          notiLog.response = response;
        })
        .catch((error) => {
          notiLog.error = error;
        });
    } else {
      this.logger.log({
        method: 'sendAutoNotificationToUsers',
        message: 'Can not send notification due to no fcmTokens',
      });
    }
    await this.makeNotificationRelation(noti, notiLog, notiConfig);
  }

  async makeNotificationHistory(
    msg: FcmMulticastMessage,
    notiConfig?: NotificationConfigEntity,
  ) {
    const notificationConfigId = notiConfig ? notiConfig.id : null;
    // create notification entity
    const notificationEntity: NotificationEntity = {
      notificationConfigId: notificationConfigId,
      title: msg.title,
      body: msg.body,
      type: msg.type,
      data: msg.data,
      userIds: msg.userIds,
      trigger: NotificationTrigger.AUTO,
      log: null,
    };
    return await this.repo.save(notificationEntity);
  }

  async makeNotificationRelation(
    noti: NotificationEntity,
    notiLog?: any,
    notiConfig?: NotificationConfigEntity,
  ) {
    const content = notiConfig ? notiConfig.content : null;
    const trigger = notiConfig
      ? NotificationTrigger.SCHEDULED
      : NotificationTrigger.AUTO;
    const attachedFile = notiConfig ? notiConfig.attachedFile : null;
    const image = notiConfig ? notiConfig.image : null;
    await this.repo.update(noti.id, {
      content: content,
      trigger: trigger,
      attachedFile: attachedFile,
      image: image,
      log: notiLog,
    });
    // linking notification-user
    const lsNotificationUser = [];
    const userIds = _.uniq(noti.userIds);
    for (let i = 0; i < userIds.length; i++) {
      const userId = noti.userIds[i];
      const notiUser: NotificationUserEntity = {
        userId: userId,
        notificationId: noti.id,
        read: false,
      };
      lsNotificationUser.push(notiUser);
    }
    await this.notificationUserRepo.save(lsNotificationUser);
  }

  async sendNotificationToUser(msg: FcmTokenMessage) {
    this.logger.log({
      method: 'sendNotificationToUser',
      msg,
    });
    const userIds = [msg.userId];
    await this.sendNotificationToUsers({ ...msg, userIds });
  }

  async sendNotificationToTopic(
    msg: FcmTopicMessage,
    notiConfig?: NotificationConfigEntity,
  ) {
    this.logger.log({
      method: 'sendNotificationToTopic',
      msg,
    });
    const imageUrl = notiConfig ? notiConfig.image : null;
    if (notiConfig && notiConfig.id) {
      const exists = await this.repo.findOne({
        where: { notificationConfigId: notiConfig.id },
      });
      if (exists) {
        this.logger.log({
          method: 'sendNotificationToTopic',
          message: `Already create Notification for notiConfigId=${notiConfig.id}`,
        });
        return;
      }
    }
    try {
      const noti: NotificationEntity = await this.makeNotificationHistory(
        msg,
        notiConfig,
      );
      const msgData = {
        ...msg.data,
        notiId: noti.id,
      };
      const message: Message = {
        topic: msg.topic,
        data: {
          type: msg.type,
          data: JSON.stringify(msgData),
        },
        notification: {
          title: msg.title,
          body: msg.body,
          ...(imageUrl && { imageUrl: imageUrl }),
        },
        // custom icon for web
        webpush: {
          fcmOptions: {
            link: '/dashboard',
          },
          notification: {
            icon: '/logo.png',
          },
        },
      };
      const notiLog: any = { message };
      await firebase
        .messaging()
        .send(message)
        .then((response) => {
          notiLog.response = response;
        })
        .catch((error) => {
          notiLog.error = error;
        });
      await this.makeNotificationRelation(noti, notiLog, notiConfig);
    } catch (err) {
      this.logger.error({
        method: 'sendNotificationToTopic',
        err,
      });
    }
  }

  @Cron(CronExpression.EVERY_MINUTE)
  async scheduleSendNotification(): Promise<any> {
    // query list approved notification within 5 mins
    const now = new Date();
    const listNeedToSend = await this.notificationConfigRepo.find({
      where: {
        status: NotificationStatus.APPROVED,
        scheduledTime: Between(subMinutes(now, 5), now),
      },
    });
    this.logger.log({
      method: 'scheduleSendNotification',
      listNeedToSend,
    });
    // send notification
    for (let i = 0; i < listNeedToSend.length; i++) {
      const notiConfig = listNeedToSend[i];
      try {
        let topic = null;
        const query = this.userService.repo
          .createQueryBuilder('user')
          .select('"user".*')
          .leftJoin('role', 'role', '"role"."id" = "user"."roleId"')
          .leftJoin(
            'user_merchant',
            'userMerchant',
            '"userMerchant"."userId" = "user"."id"',
          );
        switch (notiConfig.receivedType) {
          case NotificationReceivedType.COLLABORATOR:
            topic = FcmTopic.USHARE_COLLABORATOR;
            query.where('"role"."type" IN (:...roleTypes)', {
              roleTypes: [RoleType.PARTNER],
            });
            break;
          case NotificationReceivedType.FOLLOWED_COLLABORATOR:
            if (notiConfig.merchantId) {
              topic = FcmTopic.MERCHANT_COLLABORATOR.replace(
                '{id}',
                String(notiConfig.merchantId),
              );
              query.where(
                '"userMerchant"."merchantId" = :merchantId AND "role"."type" IN (:...roleTypes)',
                {
                  merchantId: notiConfig.merchantId,
                  roleTypes: [RoleType.PARTNER],
                },
              );
            }
            break;
          case NotificationReceivedType.USHARE_STAFT:
            topic = FcmTopic.USHARE_STAFF;
            query.where('"role"."type" IN (:...roleTypes)', {
              merchantId: notiConfig.merchantId,
              roleTypes: [
                RoleType.MERCHANT,
                RoleType.LEADER,
                RoleType.MANAGEMENT_PARTNER,
                RoleType.MERCHANT_ACCOUNTANT,
              ],
            });
            break;
          case NotificationReceivedType.STAFF:
            if (notiConfig.merchantId) {
              topic = FcmTopic.MERCHANT_STAFF.replace(
                '{id}',
                String(notiConfig.merchantId),
              );
              query.where(
                '"userMerchant"."merchantId" = :merchantId AND "role"."type" IN (:...roleTypes)',
                {
                  merchantId: notiConfig.merchantId,
                  roleTypes: [
                    RoleType.LEADER,
                    RoleType.MANAGEMENT_PARTNER,
                    RoleType.MERCHANT_ACCOUNTANT,
                  ],
                },
              );
            }
            break;
          case NotificationReceivedType.ALL:
            topic = FcmTopic.USHARE_ANNOUNCEMENT;
            break;
          default:
            break;
        }
        if (topic) {
          const users: UserEntity[] = (await query.execute()) as
            | UserEntity[]
            | [];
          const userIds = _.uniq(users.map((u) => u.id));
          const msg: FcmTopicMessage = {
            userIds,
            title: notiConfig.title,
            body: notiConfig.body,
            topic,
            type: notiConfig.type,
            data: {},
          };
          await this.sendNotificationToTopic(msg, notiConfig);
          // mark notification as SENT
          await this.notificationConfigRepo.update(
            { id: notiConfig.id },
            {
              status: NotificationStatus.SENT,
              updatedAt: () => '"updatedAt"',
            },
          );
        }
      } catch (err) {
        this.logger.error({
          method: 'scheduleSendNotification',
          err,
        });
        // mark notification as FAILED
        await this.notificationConfigRepo.update(
          { id: notiConfig.id },
          {
            status: NotificationStatus.FAILED,
            updatedAt: () => '"updatedAt"',
          },
        );
      }
    }
  }

  async sendNotificationOfOrders(orders: OrderEntity[]) {
    for (let i = 0; i < orders.length; i++) {
      const order = orders[i];
      await this.sendNotificationOfOrder(order);
    }
  }

  sendNotificationOfOrder(order: OrderEntity) {
    this.doSendNotificationOfOrder(order).catch((e) => {
      console.log(e);
    });
  }

  async doSendNotificationOfOrder(order: OrderEntity) {
    // Notify status change
    switch (order.status) {
      case OrderStatus.CONFIRMED: //Đã xác nhận
      case OrderStatus.PROCESSING: //Đang xử lý
      case OrderStatus.SHIPPING: //Đang giao
        // notify to Collaborator
        this.sendNotificationToUser({
          userId: order.userId,
          title: `Bạn có 1 đơn hàng ${convertOrderStatusToText(order.status)}`,
          body: `Đơn hàng ${
            order.code
          } đã chuyển trạng thái ${convertOrderStatusToText(order.status)}`,
          type: NotificationType.ORDER,
          data: {
            orderId: order.id,
          },
        });
        break;
      case OrderStatus.SHIPPED: //Đã giao, chờ đối soát
      case OrderStatus.COMPLETED: //Đã đối soát
      case OrderStatus.CANCELLED: //Đã huỷ
      case OrderStatus.RETURNED: //Hoàn
        // notify to Collaborator & merchant
        const merchantId = order.merchantId;
        const query = this.userService.repo
          .createQueryBuilder('user')
          .select('"user".*')
          .leftJoin('role', 'role', '"role"."id" = "user"."roleId"')
          .where(
            '"user"."merchantId" = :merchantId AND "role"."type" = :roleType',
            {
              merchantId: merchantId,
              roleType: RoleType.MERCHANT,
            },
          );
        const adminMerchants: UserEntity[] = (await query.execute()) as
          | UserEntity[]
          | [];
        const userIds = adminMerchants.map((u) => u.id);
        userIds.push(order.userId);
        this.sendNotificationToUsers({
          userIds,
          title: `Bạn có 1 đơn hàng ${convertOrderStatusToText(order.status)}`,
          body: `Đơn hàng ${
            order.code
          } đã chuyển trạng thái ${convertOrderStatusToText(order.status)}`,
          type: NotificationType.ORDER,
          data: {
            orderId: order.id,
          },
        });
        break;
      case OrderStatus.MERCHANT_CANCELLED: // Merchant huỷ
        this.sendNotificationToUser({
          userId: order.userId,
          title: `Bạn có 1 đơn hàng ${convertOrderStatusToText(order.status)}`,
          body: `Đơn hàng ${
            order.code
          } đã chuyển trạng thái ${convertOrderStatusToText(order.status)} \n
          Lý do: ${order.cancel_reason}
          `,
          type: NotificationType.ORDER,
          data: {
            orderId: order.id,
          },
        });
        break;
    }
  }

  async sendNotificationPaymentStatusOfOrders(orders: OrderEntity[]) {
    for (let i = 0; i < orders.length; i++) {
      const order = orders[i];
      await this.sendNotificationPaymentStatusOfOrder(order);
    }
  }

  sendNotificationPaymentStatusOfOrder(order: OrderEntity) {
    this.doSendNotificationOfOrderPaymentStatus(order).catch((e) => {
      console.log(e);
    });
  }

  async doSendNotificationOfOrderPaymentStatus(order: OrderEntity) {
    switch (order.paymentStatus) {
      case OrderPaymentStatus.WAIT_DEPOSIT:
      case OrderPaymentStatus.CONFIRM_DEPOSIT:
        break;
      case OrderPaymentStatus.DEPOSITED:
        if (order.deposit > 0) {
          // notify to Collaborator
          this.sendNotificationToUser({
            userId: order.userId,
            title: `Bạn có 1 đơn hàng đã xác nhận cọc`,
            body: `Đơn hàng ${order.code} xác nhận cọc thành công \n
            Trạng thái thanh toán: Đã xác nhận cọc`,
            type: NotificationType.ORDER,
            data: {
              orderId: order.id,
            },
          });
        } else {
          // notify to Collaborator
          this.sendNotificationToUser({
            userId: order.userId,
            title: `Bạn có 1 đơn hàng chưa xác nhận cọc`,
            body: `Đơn hàng ${order.code} xác nhận cọc thất bại. Vui lòng kiểm tra lại! \n
            Trạng thái thanh toán: Chưa đặt cọc`,
            type: NotificationType.ORDER,
            data: {
              orderId: order.id,
            },
          });
        }
        break;
      case OrderPaymentStatus.RETURNED_DEPOSIT:
        // notify to Collaborator
        this.sendNotificationToUser({
          userId: order.userId,
          title: `Bạn có 1 đơn hàng đã hoàn cọc`,
          body: `Đơn hàng ${order.code} đã được hoàn cọc
          Trạng thái cọc: Đã hoàn cọc`,
          type: NotificationType.ORDER,
          data: {
            orderId: order.id,
          },
        });
        break;
    }
  }
}

import { Body, Controller, Delete, Get, Param, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiOkResponse, ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';
import { CrudRequest, GetManyDefaultResponse, ParsedRequest } from '@nestjsx/crud';
import { Auth } from 'decorators/auth.decorator';
import { User } from 'decorators/user.decorator';
import { NotificationType } from 'entities/notification-config.entity';
import { NotificationTokenEntity } from 'entities/notification-token.entity';
import { NotificationEntity } from 'entities/notification.entity';
import { UserEntity } from 'entities/user.entity';
import { JwtAuthGuard } from 'guards/jwt-auth.guard';
import { FlashSaleFilterWithPaging } from 'modules/flash-sale/dto/flash-sale.dto';
import { NotificationCounterDto } from './dto/notification-counter.dto';
import { NotificationDto, NotificationFilterWithPaging } from './dto/notification.dto';
import { RegisterFcmTokenDto } from './dto/register-fcm-token.dto';
import { NotificationService } from './notification.service';
export class NotificationResponse
  implements GetManyDefaultResponse<NotificationDto>
{
  @ApiProperty({
    description: 'count',
  })
  count: number;
  @ApiProperty({
    description: 'data',
    type: [NotificationDto],
  })
  data: NotificationDto[];
  @ApiProperty({
    description: 'page',
  })
  page: number;
  @ApiProperty({
    description: 'pageCount',
  })
  pageCount: number;
  @ApiProperty({
    description: 'total',
  })
  total: number;
}

@ApiTags('Notifications')
@Controller('notifications')
export class NotificationController {
  constructor(public readonly service: NotificationService) { }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('/by-type')
  @ApiOperation({
    summary: 'Danh sách thông báo',
  })
  @ApiOkResponse({
    type: NotificationResponse,
  })
  async getNotificationByType(
    @Query() query: NotificationFilterWithPaging,
    @ParsedRequest() req: CrudRequest,
    @User() user: UserEntity,
  ): Promise<GetManyDefaultResponse<NotificationDto>> {
    return await this.service.getNotificationByType(user, query);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('/:id')
  @ApiOperation({
    summary: 'Chi tiết thông báo',
  })
  @ApiOkResponse({
    type: NotificationEntity,
  })
  async getDetail(@ParsedRequest() req: CrudRequest,
    @Param('id') id: number,
    @User() user: UserEntity,
  ) {
    return await this.service.getDetail(user, id);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('/count')
  @ApiOperation({
    summary: 'Số thông báo chưa đọc',
  })
  @ApiOkResponse({
    type: [NotificationCounterDto],
  })
  async countUnread(@ParsedRequest() req: CrudRequest,
    @User() user: UserEntity,
  ) {
    return await this.service.countUnread(user);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post('/tokens')
  @ApiOperation({
    summary: 'Đăng ký fcm token',
  })
  @ApiBody({
    type: RegisterFcmTokenDto,
  })
  @ApiOkResponse({
    type: NotificationTokenEntity,
  })
  async registerFcmToken(
    @ParsedRequest() req: CrudRequest,
    @Body() dto: RegisterFcmTokenDto,
    @User() user: UserEntity,
  ) {
    return await this.service.registerFcmToken(dto, user);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Delete('/tokens')
  @ApiOperation({
    summary: 'Bỏ đăng ký fcm token',
  })
  @ApiBody({
    type: RegisterFcmTokenDto,
  })
  @ApiOkResponse({
    type: NotificationTokenEntity,
  })
  async unregisterFcmToken(
    @ParsedRequest() req: CrudRequest,
    @Body() dto: RegisterFcmTokenDto,
    @User() user: UserEntity,
  ) {
    return await this.service.unregisterFcmToken(dto, user);
  }
  
}

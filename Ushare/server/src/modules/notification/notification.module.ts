import { Module } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NotificationEntity } from 'entities/notification.entity';
import { NotificationTokenEntity } from 'entities/notification-token.entity';
import { UserModule } from 'modules/user/user.module';
import { NotificationUserEntity } from 'entities/notification-user.entity';
import { NotificationController } from './notification.controller';
import { NotificationConfigEntity } from 'entities/notification-config.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      NotificationEntity,
      NotificationTokenEntity,
      NotificationUserEntity,
      NotificationConfigEntity,
    ]),
    UserModule],
  controllers: [NotificationController],
  providers: [NotificationService],
  exports: [NotificationService],
})
export class NotificationModule { }

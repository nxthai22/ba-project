import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { In, IsNull, Not } from 'typeorm';
import { OrderStatus, ShippingPartner } from 'enums';
import { lastValueFrom } from 'rxjs';
import { convertGhnStatus, convertGhtkStatus } from 'utils';
import {
  OrderHistoryEntity,
  OrderHistoryType,
} from 'entities/order-history.entity';
import { MerchantService } from 'modules/merchant/merchant.service';
import { OrderService } from 'modules/order/order.service';
import { ConfigService as NestConfigService } from '@nestjs/config';
import { HttpService } from '@nestjs/axios';

@Injectable()
export class TaskService {
  constructor(
    private nestConfigService: NestConfigService,
    private httpService: HttpService,
    private orderService: OrderService,
  ) {}

  private readonly logger = new Logger(TaskService.name);

  // @Cron(CronExpression.EVERY_5_MINUTES)
  // async updateShipmentStatus() {
  //   if (process.env.NODE_ENV !== 'local') {
  //     this.logger.debug('updateShipmentStatus - start');
  //
  //     const orders = await this.orderService.repo.find({
  //       where: {
  //         id: 59,
  //         status: In([OrderStatus.SHIPPING, OrderStatus.SHIPPED]),
  //         shippingInfo: Not(IsNull()),
  //       },
  //       relations: ['merchant'],
  //     });
  //     return Promise.all(
  //       orders.map(async (order) => {
  //         let shipmentStatus;
  //
  //         switch (order.shippingInfo.partner) {
  //           case ShippingPartner.GHTK:
  //             const ghtkTokenData = order?.merchant?.tokens.find(
  //               (token) => token.partner === ShippingPartner.GHTK,
  //             );
  //             if (ghtkTokenData) {
  //               const shipment = await lastValueFrom(
  //                 this.httpService.get(
  //                   `${this.nestConfigService.get<string>(
  //                     'GHTK_URL',
  //                   )}/services/shipment/v2/${order.shippingInfo.partner_id}`,
  //                   {
  //                     headers: {
  //                       Token: ghtkTokenData.token,
  //                     },
  //                   },
  //                 ),
  //               );
  //               shipmentStatus = convertGhtkStatus(
  //                 Number(shipment?.data?.order?.status),
  //               );
  //             }
  //             break;
  //           case ShippingPartner.GHN:
  //             const ghnTokenData = order?.merchant?.tokens.find(
  //               (token) => token.partner === ShippingPartner.GHN,
  //             );
  //             if (ghnTokenData) {
  //               const shipment = await lastValueFrom(
  //                 this.httpService.post(
  //                   `${this.nestConfigService.get<string>(
  //                     'GHN_URL',
  //                   )}/shiip/public-api/v2/shipping-order/detail`,
  //                   {
  //                     order_code: order.shippingInfo.partner_id,
  //                   },
  //                   {
  //                     headers: {
  //                       Token: ghnTokenData.token,
  //                     },
  //                   },
  //                 ),
  //               );
  //               shipmentStatus = convertGhnStatus(shipment?.data?.data?.status);
  //             }
  //             break;
  //         }
  //
  //         if (shipmentStatus && shipmentStatus != order.shippingInfo.status) {
  //           order.shippingInfo.status = shipmentStatus;
  //
  //           const history = new OrderHistoryEntity();
  //           history.orderId = order.id;
  //           history.status = shipmentStatus;
  //           history.type = OrderHistoryType.SHIPMENT;
  //
  //           if (
  //             [OrderStatus.SHIPPED, OrderStatus.COMPLETED].includes(
  //               shipmentStatus,
  //             )
  //           ) {
  //             order.status = shipmentStatus;
  //             history.type = OrderHistoryType.ORDER;
  //           }
  //
  //           await this.orderService.orderHistoryRepo.save(history);
  //
  //           await this.orderService.repo.save(order);
  //         }
  //       }),
  //     ).then((response) => {
  //       this.logger.debug('updateShipmentStatus - end');
  //     });
  //   }
  // }
}

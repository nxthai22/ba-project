import { Module } from '@nestjs/common';
import { TaskService } from './task.service';
import { OrderModule } from 'modules/order/order.module';
import { MerchantModule } from 'modules/merchant/merchant.module';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule as NestConfigModule } from '@nestjs/config';

@Module({
  imports: [OrderModule, MerchantModule, HttpModule, NestConfigModule],
  providers: [TaskService],
})
export class TaskModule {}

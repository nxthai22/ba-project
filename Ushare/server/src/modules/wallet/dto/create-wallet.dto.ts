import { PickType } from '@nestjs/swagger';
import { UserWalletEntity } from 'entities/user-wallet.entity';

export class CreateWalletDto extends PickType(UserWalletEntity, ['type', 'amount']) {}


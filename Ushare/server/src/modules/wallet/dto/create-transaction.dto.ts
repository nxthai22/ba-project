import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { UserTransactionEntity } from 'entities/user-transaction.entity';

export class DepositWalletDto extends PickType(UserTransactionEntity, [
  'amount',
]) {
  @ApiProperty({
    description: 'Firebase Token',
  })
  idToken: string;
}

export class WithdrawWalletDto extends PickType(UserTransactionEntity, [
  'amount',
  'note',
  'walletType',
]) {
  @ApiProperty({
    description: 'Firebase Token',
  })
  idToken: string;
}

import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';
import { UserTransactionEntity } from 'entities/user-transaction.entity';

export class UpdateTransactionDto extends PickType(UserTransactionEntity, [
  'note',
  'status',
]) {
  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Id giao dịch',
    type: String,
    required: true,
  })
  transactionId: number;
}

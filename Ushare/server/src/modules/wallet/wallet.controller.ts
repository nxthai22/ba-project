import { CrudController } from '@nestjsx/crud';
import { Post, Body, BadRequestException } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiBody } from '@nestjs/swagger';
import { Crud } from 'decorators/crud.decorator';
import { Auth } from 'decorators/auth.decorator';
import { User } from 'decorators/user.decorator';
import { UserWalletEntity } from 'entities/user-wallet.entity';
import { UserEntity } from 'entities/user.entity';
import {
  DepositWalletDto,
  WithdrawWalletDto,
} from 'modules/wallet/dto/create-transaction.dto';
import { WalletService } from './wallet.service';
import { MIN_AMOUNT } from 'config/wallet';
import { UserTransactionEntity } from 'entities/user-transaction.entity';
import { UpdateTransactionDto } from './dto/update-transaction.dto';

@Crud({
  name: 'Ví',
  controller: 'wallets',
  model: {
    type: UserWalletEntity,
  },
  query: {
    join: {
      histories: {},
      transactions: {},
    },
  },
})
@Auth()
export class WalletController implements CrudController<UserWalletEntity> {
  constructor(public readonly service: WalletService) {}

  @Post('/deposit')
  @ApiOperation({ summary: 'Nạp tiền vào ví' })
  @ApiOkResponse({ type: UserTransactionEntity })
  @ApiBody({ type: DepositWalletDto })
  async deposit(
    @Body() dto: DepositWalletDto,
    @User() user: UserEntity,
  ): Promise<UserTransactionEntity> {
    return await this.service.deposit(dto, user);
  }

  @Post('/withdraw')
  @ApiOperation({ summary: 'Rút tiền từ ví' })
  @ApiOkResponse({ type: UserTransactionEntity })
  @ApiBody({ type: WithdrawWalletDto })
  async withdraw(
    @Body() dto: WithdrawWalletDto,
    @User() user: UserEntity,
  ): Promise<UserTransactionEntity> {
    if (dto.amount < MIN_AMOUNT) {
      const formattedCurrency = new Intl.NumberFormat('vi-VN', {
        style: 'currency',
        currency: 'VND',
      }).format(MIN_AMOUNT);

      throw new BadRequestException(
        `Số thiểu có thể rút là ${formattedCurrency}`,
      );
    }

    return await this.service.withdraw(dto, user);
  }

  @Post('/transaction/update-status')
  @ApiOperation({ summary: 'Cập nhật giao dịch' })
  @ApiOkResponse({ type: UserTransactionEntity })
  @ApiBody({ type: UpdateTransactionDto })
  async updateTransactionStatus(
    @Body() dto: UpdateTransactionDto,
    @User() user: UserEntity,
  ): Promise<UserTransactionEntity> {
    return await this.service.updateTransactionStatus(dto, user);
  }
}

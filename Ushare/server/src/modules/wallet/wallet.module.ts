import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserWalletEntity } from 'entities/user-wallet.entity';
import { UserTransactionEntity } from 'entities/user-transaction.entity';
import { WalletController } from 'modules/wallet/wallet.controller';
import { WalletService } from 'modules/wallet/wallet.service';
import { UserModule } from 'modules/user/user.module';
import { VerifyOtpModule } from 'modules/verify-otp/verify-otp.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserWalletEntity, UserTransactionEntity]),
    UserModule,
    VerifyOtpModule,
  ],
  controllers: [WalletController],
  providers: [WalletService],
  exports: [WalletService],
})
export class WallModule {}

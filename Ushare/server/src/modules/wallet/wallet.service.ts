import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Connection, Repository } from 'typeorm';
import { TransactionStatus, TransactionType, WalletType } from 'enums';
import { UserWalletEntity } from 'entities/user-wallet.entity';
import { UserWalletHistoryEntity } from 'entities/user-wallet-history.entity';
import { UserEntity } from 'entities/user.entity';
import { UserTransactionEntity } from 'entities/user-transaction.entity';
import {
  DepositWalletDto,
  WithdrawWalletDto,
} from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { CreateWalletDto } from './dto/create-wallet.dto';
import { VerifyOtpService } from 'modules/verify-otp/verify-otp.service';
import { UserService } from 'modules/user/user.service';

@Injectable()
export class WalletService extends TypeOrmCrudService<UserWalletEntity> {
  constructor(
    @InjectRepository(UserWalletEntity)
    public repo: Repository<UserWalletEntity>,
    @InjectRepository(UserTransactionEntity)
    public userTransactionRepo: Repository<UserTransactionEntity>,
    @InjectConnection() private connection: Connection,
    private verifyOtpService: VerifyOtpService,
    private userService: UserService,
  ) {
    super(repo);
  }

  protected convertTel(tel: string) {
    if (tel.charAt(0) === '0') tel = tel.replace(tel.charAt(0), '+84');
    return tel;
  }

  async create(
    dto: CreateWalletDto,
    user: UserEntity,
  ): Promise<UserWalletEntity> {
    return await this.repo.save({
      userId: user.id,
      type: dto.type,
      amount: dto.amount,
    });
  }

  async deposit(
    dto: DepositWalletDto,
    user: UserEntity,
  ): Promise<UserTransactionEntity> {
    const currentWallet = await this.repo.findOne({
      userId: user.id,
      type: WalletType.CONSUMPTION,
    });

    if (!currentWallet) {
      throw new NotFoundException('Không tìm thấy ví');
    }

    const currentUser = await this.userService.repo.findOne({
      where: { id: user.id },
      select: ['sessionVerifyCode'],
    });

    const verifyCodeStatus = await this.verifyOtpService.verifyOtp({
      idToken: dto.idToken,
      phoneNumber: [this.convertTel(user.tel)],
    });

    if (!verifyCodeStatus)
      throw new BadRequestException(
        'Xác thực thất bại. Vui lòng kiểm tra số điện thoại!',
      );

    const createdTransaction = await this.userTransactionRepo.save({
      userId: user.id,
      walletId: currentWallet.id,
      amount: dto.amount,
      status: TransactionStatus.CREATED,
      walletType: WalletType.CONSUMPTION,
      transactionType: TransactionType.DEPOSIT,
      note: 'Nạp tiền!',
    });

    return createdTransaction;
  }

  async withdraw(
    dto: WithdrawWalletDto,
    user: UserEntity,
  ): Promise<UserTransactionEntity> {
    const currentWallet = await this.repo.findOne({
      userId: user.id,
      type: WalletType.CONSUMPTION,
    });

    if (!currentWallet) {
      throw new NotFoundException('Không tìm thấy ví');
    }

    if (currentWallet.amount < dto.amount) {
      throw new BadRequestException('Số dư không đủ, vui lòng nhập lại');
    }

    const verifyCodeStatus = await this.verifyOtpService.verifyOtp({
      idToken: dto.idToken,
      phoneNumber: [this.convertTel(user.tel)],
    });

    if (!verifyCodeStatus)
      throw new BadRequestException(
        'Xác thực thất bại. Vui lòng kiểm tra số điện thoại!',
      );

    return await this.userTransactionRepo.save({
      userId: user.id,
      walletId: currentWallet.id,
      amount: dto.amount,
      status: TransactionStatus.CREATED,
      walletType: dto.walletType,
      transactionType: TransactionType.WITHDRAWAL,
      note: dto.note || 'Rút tiền!',
    });
  }

  async updateTransactionStatus(
    dto: UpdateTransactionDto,
    userAdmin: UserEntity,
  ) {
    const transaction = await this.userTransactionRepo.findOne({
      id: dto.transactionId,
    });

    if (!transaction) {
      throw new NotFoundException('Không tìm thấy giao dịch');
    }

    if (TransactionStatus.COMPLETED === transaction.status) {
      throw new BadRequestException('Giao dịch này đã hoàn thành!');
    }

    if (transaction.status === dto.status) {
      throw new BadRequestException('Không thể thay đổi cùng trạng thái');
    }

    if (TransactionStatus.CANCELLED === dto.status) {
      const updatedTransaction = await this.userTransactionRepo.save({
        ...transaction,
        status: TransactionStatus.CANCELLED,
      });

      return updatedTransaction;
    }

    await this.connection.transaction(async (manager) => {
      // Cập nhật trạng thái transaction
      await manager.save(UserTransactionEntity, {
        ...transaction,
        status: dto.status,
        confirmId: userAdmin.id,
      });

      if (TransactionStatus.COMPLETED === dto.status) {
        // Kiểm tra thông tin ví hiện tại
        const wallet = await this.repo.findOne({
          id: transaction.walletId,
          type: transaction.walletType,
        });

        // Cập nhật số dư ví
        await manager.save(UserWalletEntity, {
          ...wallet,
          amount: wallet.amount + transaction.amount,
        });
      }

      const noteDefault =
        dto.status === TransactionStatus.COMPLETED
          ? 'Xác nhận giao dịch!'
          : 'Từ chối giao dịch!';

      // Tạo lịch sử giao dịch
      await manager.save(UserWalletHistoryEntity, {
        userId: transaction.userId,
        walletId: transaction.walletId,
        transactionId: transaction.id,
        amount: transaction.amount,
        walletType: transaction.walletType,
        transactionType: transaction.transactionType,
        note: dto.note || noteDefault,
      });
    });

    const updatedTransaction = await this.userTransactionRepo.findOne({
      id: dto.transactionId,
    });

    return updatedTransaction;
  }
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserTransactionEntity } from 'entities/user-transaction.entity';
import { UserWalletHistoryEntity } from 'entities/user-wallet-history.entity';
import { UserWalletEntity } from 'entities/user-wallet.entity';
import { UserTransactionController } from 'modules/user-transaction/user-transaction.controller';
import { UserTransactionService } from 'modules/user-transaction/user-transaction.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserTransactionEntity,
      UserWalletEntity,
      UserWalletHistoryEntity,
    ]),
  ],
  controllers: [UserTransactionController],
  providers: [UserTransactionService],
  exports: [UserTransactionService],
})
export class UserTransactionModule {}

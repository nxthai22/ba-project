import { Test, TestingModule } from '@nestjs/testing';
import { UserTransactionController } from 'modules/user-transaction/user-transaction.controller';

describe('TopupRequestController', () => {
  let controller: UserTransactionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserTransactionController],
      providers: [UserTransactionController],
    }).compile();

    controller = module.get<UserTransactionController>(
      UserTransactionController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

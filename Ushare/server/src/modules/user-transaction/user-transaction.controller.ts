import {
  CrudController,
  CrudRequest,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { Auth } from 'decorators/auth.decorator';
import { Crud } from 'decorators/crud.decorator';
import { User } from 'decorators/user.decorator';
import { UserEntity } from 'entities/user.entity';
import { UserTransactionEntity } from 'entities/user-transaction.entity';
import { UserTransactionService } from 'modules/user-transaction/user-transaction.service';
import { RoleType } from '../../entities/role.entity';
import { BadRequestException, Param, Post } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiParam } from '@nestjs/swagger';

@Crud({
  name: 'UserTransaction',
  controller: 'user-transaction',
  model: {
    type: UserTransactionEntity,
  },
  query: {
    join: {
      user: {},
      modifiedUser: {},
      bank: {},
      userWallet: {
        eager: false,
      },
      'userWallet.wallet': {},
    },
  },
  routes: {
    exclude: ['createManyBase', 'deleteOneBase'],
  },
})
@Auth()
export class UserTransactionController
  implements CrudController<UserTransactionEntity>
{
  constructor(public readonly service: UserTransactionService) {}

  get base(): CrudController<UserTransactionEntity> {
    return this;
  }

  @Override()
  createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: UserTransactionEntity,
    @User() user: UserEntity,
  ) {
    if (!user.bankId || !user.bankNumber || !user.bankAccountName) {
      throw new BadRequestException(
        'Thiếu thông tin ngân hàng. Vui lòng cập nhật thông tin ngân hàng trước khi giao dịch.',
      );
    }

    dto.bankId = user.bankId;
    dto.bankNumber = user.bankNumber;
    dto.bankAccountName = user.bankAccountName;

    return this.base.createOneBase(req, dto);
  }

  @Override()
  getMany(@ParsedRequest() req: CrudRequest, @User() user: UserEntity) {
    if (!user.role || user.role.type === RoleType.PARTNER) {
      req.parsed.search.$and.push({
        userId: {
          $eq: user.id,
        },
      });
    }
    return this.base.getManyBase(req);
  }

  @Auth()
  @Post('/verify/:id')
  @ApiOperation({ summary: 'Xác nhận nạp/rút tiền' })
  @ApiParam({ description: 'ID topup', name: 'id', type: Number })
  @ApiOkResponse({ type: UserTransactionEntity })
  async verify(@Param('id') id: number, @User() user: UserEntity) {
    return await this.service.verify(id, user);
  }

  @Auth()
  @Post('/cancel/:id')
  @ApiOperation({ summary: 'Huỷ lệnh nạp tiền' })
  @ApiParam({ description: 'ID topup', name: 'id', type: Number })
  @ApiOkResponse({ type: UserTransactionEntity })
  async cancel(@Param('id') id: number, @User() user: UserEntity) {
    return await this.service.cancel(id, user);
  }
}

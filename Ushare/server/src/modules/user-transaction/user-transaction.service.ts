import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Connection, Repository } from 'typeorm';
import { UserTransactionEntity } from 'entities/user-transaction.entity';
import { UserEntity } from 'entities/user.entity';
import { TransactionStatus, TransactionType, WalletType } from 'enums';
import { UserWalletEntity } from 'entities/user-wallet.entity';
import { UserWalletHistoryEntity } from 'entities/user-wallet-history.entity';

@Injectable()
export class UserTransactionService extends TypeOrmCrudService<UserTransactionEntity> {
  constructor(
    @InjectRepository(UserTransactionEntity)
    public repo: Repository<UserTransactionEntity>,
    @InjectRepository(UserWalletEntity)
    public userWalletRepo: Repository<UserWalletEntity>,
    @InjectRepository(UserWalletHistoryEntity)
    public userWalletHistoryRepo: Repository<UserWalletHistoryEntity>,
    @InjectConnection() private connection: Connection,
  ) {
    super(repo);
  }

  async verify(id: number, user: UserEntity) {
    const transaction = await this.repo.findOne({ id });
    if (!transaction) throw new NotFoundException('Không tìm giao dịch');

    if (transaction.status == TransactionStatus.COMPLETED) {
      throw new NotFoundException('Giao dịch đã hoàn thành');
    }

    if (transaction.status == TransactionStatus.CANCELLED) {
      throw new NotFoundException('Giao dịch đã hủy');
    }

    const userWallet = await this.userWalletRepo.findOne({
      where: {
        userId: transaction.userId,
        type: WalletType.CONSUMPTION,
      },
    });
    if (!userWallet) throw new NotFoundException('Không tìm thấy ví tiêu dùng');

    const userWalletHistoryParam = new UserWalletHistoryEntity();
    userWalletHistoryParam.userId = user.id;
    userWalletHistoryParam.walletId = userWallet.id;
    userWalletHistoryParam.amount = transaction.amount;
    userWalletHistoryParam.transactionId = transaction.id;
    userWalletHistoryParam.walletType = WalletType.CONSUMPTION;
    userWalletHistoryParam.transactionType = transaction.transactionType;

    if (transaction.transactionType === TransactionType.DEPOSIT) {
      userWallet.amount += transaction.amount;
    } else {
      userWallet.amount -= transaction.amount;
    }

    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      await queryRunner.manager.save(UserWalletEntity, userWallet);

      transaction.status = TransactionStatus.COMPLETED;
      transaction.confirmId = user.id;
      await queryRunner.manager.save(UserTransactionEntity, transaction);

      await queryRunner.manager.save(
        UserWalletHistoryEntity,
        userWalletHistoryParam,
      );

      await queryRunner.commitTransaction();

      return transaction;
    } catch (error) {
      await queryRunner.rollbackTransaction();

      throw new InternalServerErrorException(error.message);
    } finally {
      await queryRunner.release();
    }
  }

  async cancel(id: number, user: UserEntity): Promise<UserTransactionEntity> {
    const transaction = await this.repo.findOne({
      id: id,
    });
    if (!transaction) {
      throw new NotFoundException('Không tìm thấy yêu cầu nạp tiền');
    }

    if (transaction.status !== TransactionStatus.CREATED) {
      throw new NotFoundException('Không thể huỷ yêu cầu nạp');
    }

    transaction.status = TransactionStatus.CANCELLED;
    transaction.confirmId = user.id;

    return this.repo.save(transaction);
  }
}

import {
  BadRequestException,
  Body,
  Delete,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import {
  ApiBody,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
} from '@nestjs/swagger';
import { CrudAuth, CrudController, Override } from '@nestjsx/crud';
import { Auth } from 'decorators/auth.decorator';
import { Crud } from 'decorators/crud.decorator';
import { User } from 'decorators/user.decorator';
import { CustomerEntity } from 'entities/customer.entity';
import { UserEntity } from 'entities/user.entity';
import { CustomerService } from './customer.service';
import {
  CreateCustomerDto,
  UpdateAddressCustomerDto,
} from './dto/create-customer.dto';
import { Not } from 'typeorm';

@Crud({
  name: 'Khách hàng',
  controller: 'customers',
  model: {
    type: CustomerEntity,
  },
  query: {
    join: {
      addresses: {},
      'addresses.ward': {
        alias: 'ward',
      },
      'addresses.ward.district': {
        alias: 'district',
      },
      'addresses.ward.district.province': {
        alias: 'province',
      },
    },
  },
  routes: {
    include: ['deleteOneBase'],
  },
})
@CrudAuth({
  property: 'user',
  filter: (user: UserEntity) => ({
    userId: user.id,
  }),
})
@Auth()
export class CustomerController implements CrudController<CustomerEntity> {
  constructor(public readonly service: CustomerService) {}

  get base(): CrudController<CustomerEntity> {
    return this;
  }

  @Override()
  async updateOne(
    @Param('id') id: number,
    @Body() dto: CreateCustomerDto,
    @User() user: UserEntity,
  ): Promise<CustomerEntity> {
    const checkTel = await this.service.repo.findOne({
      where: {
        userId: dto.userId,
        tel: dto.tel,
        id: Not(id),
      },
    });
    if (checkTel)
      throw new BadRequestException('Đã tồn tại số điện thoại này cho CTV');
    const customer = await this.service.repo.findOne({ id: id });
    return await this.service.repo.save({
      ...customer,
      ...dto,
    });
  }

  @Override('createOneBase')
  @Post()
  @ApiOkResponse({ type: CustomerEntity })
  @ApiBody({ type: CreateCustomerDto })
  async createOne(
    @Body() dto: CreateCustomerDto,
    @User() user: UserEntity,
  ): Promise<CustomerEntity> {
    const phoneRegExp = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/;
    if (!dto.tel.match(phoneRegExp)) {
      throw new BadRequestException(
        `Số điện thoại ${dto.tel} không đúng định dạng`,
      );
    }
    return await this.service.addCustomer(dto, user);
  }

  @Post('/address')
  @ApiOperation({ summary: 'Thêm địa chỉ nhận hàng' })
  @ApiOkResponse({ type: CustomerEntity })
  @ApiBody({ type: CreateCustomerDto })
  async addAddress(
    @Body() dto: CreateCustomerDto,
    @User() user: UserEntity,
  ): Promise<CustomerEntity> {
    return this.service.addAddress(dto, user);
  }

  @Patch('/update-address/:id')
  @ApiOperation({ summary: 'Cập nhật địa chỉ nhận hàng' })
  @ApiOkResponse({ type: CustomerEntity })
  @ApiBody({ type: UpdateAddressCustomerDto })
  async updateAddress(
    @Param('id') id: number,
    @Body() dto: UpdateAddressCustomerDto,
    @User() user: UserEntity,
  ): Promise<CustomerEntity> {
    const address = await this.service.customerAddressRepo.findOne({ id });
    if (!address) throw new BadRequestException('Địa chỉ không tồn tại');
    await this.service.customerAddressRepo.save({
      ...address,
      wardId: dto.wardId,
      address: dto.address,
    });
    return await this.service.repo.findOne({
      where: { id: address.customerId },
      relations: ['addresses'],
    });
  }

  @Delete('/delete-address/:id')
  @ApiParam({ name: 'id', description: 'Id địa chỉ nhận hàng' })
  @ApiOperation({ summary: 'Xóa địa chỉ nhận hàng' })
  @ApiOkResponse({ type: CustomerEntity })
  async deleteAddress(
    @Param('id') id: number,
    @User() user: UserEntity,
  ): Promise<CustomerEntity> {
    const address = await this.service.customerAddressRepo.findOne({ id });
    if (!address) throw new BadRequestException('Địa chỉ không tồn tại');
    await this.service.customerAddressRepo.softDelete({ id: id });
    return await this.service.repo.findOne({
      where: { id: address.customerId },
      relations: ['addresses'],
    });
  }
}

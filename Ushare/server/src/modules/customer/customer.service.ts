import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { CustomerEntity } from 'entities/customer.entity';
import { UserEntity } from 'entities/user.entity';
import { CustomerAddressEntity } from '../../entities/customer-address.entity';
import { CreateCustomerDto } from './dto/create-customer.dto';

@Injectable()
export class CustomerService extends TypeOrmCrudService<CustomerEntity> {
  constructor(
    @InjectRepository(CustomerEntity)
    public repo: Repository<CustomerEntity>,
    @InjectRepository(CustomerAddressEntity)
    public customerAddressRepo: Repository<CustomerAddressEntity>,
  ) {
    super(repo);
  }
  /** Thêm mới khách hàng */
  async addCustomer(
    dto: CreateCustomerDto,
    user: UserEntity,
  ): Promise<CustomerEntity> {
    const customer = await this.repo.findOne({
      tel: dto.tel,
      userId: user.id,
    });
    if (customer) {
      throw new BadRequestException('Số điện thoại đã tồn tại');
    }
    dto.userId = user.id;
    const newCustomer = await this.repo.save(dto);
    const address = new CustomerAddressEntity();
    address.customerId = newCustomer.id;
    address.address = dto.address;
    address.wardId = dto.wardId;
    await this.customerAddressRepo.save(address);
    return await this.repo.findOne({
      where: {
        id: newCustomer.id,
      },
      relations: ['addresses'],
    });
  }

  /** Thêm địa chỉ khách hàng */
  async addAddress(
    dto: CreateCustomerDto,
    user: UserEntity,
  ): Promise<CustomerEntity> {
    const customer = await this.repo.findOne({
      where: {
        id: dto.customerId,
        userId: user.id,
      },
    });
    if (!customer) {
      throw new NotFoundException('Không tìm thấy khách hàng');
    }

    const newAddress = new CustomerAddressEntity();
    newAddress.customerId = dto.customerId;
    newAddress.address = dto.address;
    newAddress.wardId = dto.wardId;
    await this.customerAddressRepo.save(newAddress);

    return await this.repo.findOne({
      where: {
        id: dto.customerId,
      },
      relations: ['addresses'],
    });
  }
}

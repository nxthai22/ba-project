import { ApiProperty, PickType } from '@nestjs/swagger';
import { CustomerEntity } from 'entities/customer.entity';

export class CreateCustomerDto extends PickType(CustomerEntity, [
  'userId',
  'fullName',
  'tel',
]) {
  @ApiProperty({
    description: 'Id xã phường',
    required: true,
  })
  wardId: number;

  @ApiProperty({
    description: 'Địa chỉ cụ thể',
    required: false,
  })
  address?: string;

  @ApiProperty({
    description: 'Id khách hàng',
    required: false,
  })
  customerId?: number;
}
export class UpdateAddressCustomerDto {
  @ApiProperty({
    description: 'Id xã phường',
    required: true,
  })
  wardId: number;

  @ApiProperty({
    description: 'Địa chỉ cụ thể',
    required: true,
  })
  address?: string;
}

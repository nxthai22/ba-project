import { ApiProperty } from '@nestjs/swagger';

export class GetCustomersDto {
  @ApiProperty({
    description: 'Số khách hàng muốn lấy',
    required: false,
    default: 10,
  })
  pageSize?: number;

  @ApiProperty({
    description: 'Trang',
    required: false,
    default: 1,
  })
  page?: number;

  @ApiProperty({ description: 'Tên' })
  name?: number;

  @ApiProperty({ description: 'Số điện thoại' })
  tel?: string;
}

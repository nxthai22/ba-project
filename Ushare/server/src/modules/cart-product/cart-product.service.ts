import {
  BadRequestException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull, LessThan, MoreThan, Not, Repository } from 'typeorm';
import { CartProductEntity } from 'entities/cart-product.entity';
import { CreateUpdateCartProductDto } from './dto/create-update-cart-product.dto';
import { UserEntity } from 'entities/user.entity';
import { VoucherService } from '../voucher/voucher.service';
import { ProductService } from '../product/product.service';
import { VoucherDiscountType } from 'entities/voucher.entity';
import { throwErrorCartProduct } from 'interfaces';
import { join } from 'path';

@Injectable()
export class CartProductService extends TypeOrmCrudService<CartProductEntity> {
  constructor(
    @InjectRepository(CartProductEntity)
    public repo: Repository<CartProductEntity>,
    public voucherService: VoucherService,
    public productService: ProductService,
  ) {
    super(repo);
  }

  /** Thêm sản phẩm vào giỏ hàng */
  async calculateCartProduct(
    dtos: CreateUpdateCartProductDto[],
    user: UserEntity,
  ): Promise<CartProductEntity[]> {
    const canCheck = dtos.length === 1;
    const throwError: throwErrorCartProduct = {
      voucherCode: 0,
      voucherUshadeCode: 0,
      message: '',
      statusCode: HttpStatus.BAD_REQUEST,
    };

    const cartProducts = await Promise.all(
      dtos?.map(async (dto) => {
        const product = await this.productService.repo.findOne({
          id: dto.productId,
        });
        if (!product) throw new NotFoundException('Sản phẩm không tồn tại');
        const variant = await this.productService.productVariantRepo.findOne({
          id: dto.productVariantId,
        });
        const variantPrice = variant?.price || 0;

        const priceDiscount = await this.getPriceDiscount(
          canCheck,
          dto,
          dto.productId,
          product.price,
          variantPrice,
          user,
          throwError,
        );

        // Check quantity stock
        if (dto.checked) {
          const productStocks = await this.productService.productStockEntityRepo.find({
            productId: dto.productId,
          });
          productStocks?.map((stock) => {
            if (stock?.merchantAddressId === dto.merchantAddressId) {
              if (stock.quantity < dto.quantity) {
                throw new BadRequestException(`Sản phẩm đã hết hàng`)
              }
            }
          });
        }

        const commision =
          (Number(product.commission || 0) /
            Number(variant ? variant.price : product.price || 1)) *
          priceDiscount;
        const otherCartProduct = await this.repo.findOne({
          where: {
            productId: dto.productId,
            productVariantId: dto.productVariantId
              ? dto.productVariantId
              : IsNull(),
            userId: user.id,
            merchantAddressId: dto.merchantAddressId,
          },
        });
        let otherQuantity = 0;
        // Kiểm tra giỏ hàng đã tồn tại sản phẩm hay không?
        if (otherCartProduct) {
          otherQuantity = otherCartProduct.quantity || 0;
        }
        const newQuantity = !dto.id
          ? dto.quantity + otherQuantity
          : dto.quantity;
        const newCartProduct: CartProductEntity = {
          ...(dto.id && { id: dto.id }),
          ...(otherCartProduct && otherCartProduct),
          userId: user.id,
          productId: dto.productId,
          product: product,
          productVariantId: dto.productVariantId,
          productVariant: variant,
          quantity: newQuantity,
          voucherCode: dto.checked ? dto.voucherCode : null,
          checked: dto.checked,
          voucherUshareCode: dto.checked ? dto.voucherUshareCode : null,
          priceDiscount: Math.floor(priceDiscount * newQuantity),
          merchantAddressId: dto.merchantAddressId,
          commission: Math.floor(commision * newQuantity),
        };
        return newCartProduct;
      }),
    );

    if (
      !canCheck &&
      (throwError.voucherCode >= dtos.length ||
        throwError.voucherUshadeCode >= dtos.length)
    ) {
      switch (throwError.statusCode) {
        case HttpStatus.BAD_REQUEST:
          throw new BadRequestException(throwError.message);
        case HttpStatus.NOT_FOUND:
          throw new NotFoundException(throwError.message);

        default:
          throw new InternalServerErrorException(throwError.message);
      }
    }

    return cartProducts;
  }

  protected async getPriceDiscount(
    canCheck: boolean,
    dto: CreateUpdateCartProductDto,
    productId: number,
    productPrice: number = 0,
    variantPrice: number = 0,
    user: UserEntity,
    throwError: throwErrorCartProduct,
  ): Promise<number> {
    let priceDiscount = variantPrice || productPrice;
    if (!dto.checked) return priceDiscount;

    if (dto.voucherCode) {
      const voucherShop = await this.voucherService.repo.findOne({
        where: {
          code: dto.voucherCode,
          startDate: LessThan(new Date()),
          endDate: MoreThan(new Date()),
        },
      });
      if (!voucherShop) {
        const throwMessage = `Mã giảm giá ${dto.voucherCode} của shop không tồn tại hoặc đã hết hạn`;
        if (canCheck) {
          throw new NotFoundException(throwMessage);
        }
        throwError.message = throwMessage;
        throwError.statusCode = HttpStatus.NOT_FOUND;
      }
      if (!voucherShop) {
        throwError.voucherCode += 1;
      }
      if (voucherShop) {
        const voucherShopHistory =
          await this.voucherService.voucherHistoryRepo.findOne({
            where: {
              voucherId: voucherShop.id,
              tel: user.tel,
              orderId: Not(IsNull()),
            },
          });
        if (voucherShopHistory) {
          const throwMessage = `Mã giảm giá ${dto.voucherCode} đã được sử dụng`;
          if (canCheck) {
            throw new NotFoundException(throwMessage);
          }
          throwError.message = throwMessage;
          throwError.statusCode = HttpStatus.NOT_FOUND;
        }
        if (voucherShop.productIds?.includes(productId)) {
          priceDiscount -= Number(
            (voucherShop.discountType === VoucherDiscountType.AMOUNT
              ? Number(voucherShop.discountValue || 0)
              : (Number(voucherShop.discountValue || 0) / 100) *
              Number(productPrice || 0)) || 0,
          );
        }
      }
    }

    if (dto.voucherUshareCode) {
      const voucherUshare = await this.voucherService.repo.findOne({
        where: {
          code: dto.voucherUshareCode,
          startDate: LessThan(new Date()),
          endDate: MoreThan(new Date()),
        },
      });
      if (!voucherUshare) {
        const throwMessage = `Mã giảm giá ${dto.voucherUshareCode} của Ushare không tồn tại hoặc đã hết hạn`;
        if (canCheck) {
          throw new NotFoundException(throwMessage);
        }
        throwError.voucherUshadeCode += 1;
        throwError.message = throwMessage;
        throwError.statusCode = HttpStatus.NOT_FOUND;
      } else {
        const voucherUshareHistory =
          await this.voucherService.voucherHistoryRepo.findOne({
            where: {
              voucherId: voucherUshare.id,
              tel: user.tel,
              orderId: Not(IsNull()),
            },
          });
        if (voucherUshareHistory) {
          const throwMessage = `Mã giảm giá ${dto.voucherUshareCode} đã được sử dụng`;
          if (canCheck) {
            throw new BadRequestException(throwMessage);
          }
          throwError.voucherUshadeCode += 1;
          throwError.message = throwMessage;
          throwError.statusCode = HttpStatus.BAD_REQUEST;
        }
        priceDiscount -= Number(
          (voucherUshare.discountType === VoucherDiscountType.AMOUNT
            ? Number(voucherUshare.discountValue || 0)
            : (Number(voucherUshare.discountValue || 0) / 100) *
            productPrice) || 0,
        );
      }
    }

    return priceDiscount;
  }
}

import { ApiProperty, PickType } from '@nestjs/swagger';
import { CartProductEntity } from 'entities/cart-product.entity';

export class CreateUpdateCartProductDto extends PickType(CartProductEntity, [
  'productId',
  'productVariantId',
  'quantity',
  'merchantAddressId',
  'checked',
]) {
  @ApiProperty({
    description: 'ID giỏ hàng',
    type: Number,
    required: false,
  })
  id?: number;

  @ApiProperty({
    description: 'Mã ưu đãi của shop',
    type: String,
    required: false,
  })
  voucherCode?: string;

  @ApiProperty({
    description: 'Mã ưu đãi của Ushare',
    type: String,
    required: false,
  })
  voucherUshareCode?: string;
}

import { CartProductService } from './cart-product.service';
import { Crud } from 'decorators/crud.decorator';
import { CartProductEntity } from 'entities/cart-product.entity';
import { CrudAuth, CrudController, Override } from '@nestjsx/crud';
import { Auth } from 'decorators/auth.decorator';
import { CreateUpdateCartProductDto } from 'modules/cart-product/dto/create-update-cart-product.dto';
import { ApiBody, ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { User } from 'decorators/user.decorator';
import { UserEntity } from 'entities/user.entity';
import { Body, Delete, Param, Patch, Query } from '@nestjs/common';
import { ApiImplicitQuery } from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';
import { In } from 'typeorm';

@Crud({
  name: 'Sản phẩm trong giỏ hàng',
  controller: 'cart-product',
  model: {
    type: CartProductEntity,
  },
  routes: {
    include: ['deleteOneBase'],
  },
  dto: {
    create: CreateUpdateCartProductDto,
    update: CreateUpdateCartProductDto,
  },
  query: {
    join: {
      product: {},
      productVariant: {},
      merchantAddress: {},
    },
  },
})
@Auth()
@CrudAuth({
  property: 'user',
  filter: (user) => ({
    userId: user.id,
  }),
  persist: (user) => ({
    userId: user.id,
  }),
})
export class CartProductController
  implements CrudController<CartProductEntity>
{
  constructor(public readonly service: CartProductService) {}

  get base(): CrudController<CartProductEntity> {
    return this;
  }

  @Override()
  @ApiBody({
    type: CreateUpdateCartProductDto,
  })
  @ApiOkResponse({
    type: CartProductEntity,
  })
  async createOne(
    @Body() dto: CreateUpdateCartProductDto,
    @User() user: UserEntity,
  ): Promise<CartProductEntity> {
    const cartProducts = await this.service.calculateCartProduct(
      [
        {
          ...dto,
        },
      ],
      user,
    );
    return this.service.repo.save(cartProducts[0]);
  }

  @Override('updateOneBase')
  @ApiBody({
    type: CreateUpdateCartProductDto,
  })
  @ApiOkResponse({
    type: CartProductEntity,
  })
  async updateOne(
    @Param('id') id: number,
    @Body() dto: CreateUpdateCartProductDto,
    @User() user: UserEntity,
  ): Promise<CartProductEntity> {
    const cartProducts = await this.service.calculateCartProduct(
      [
        {
          ...dto,
          id: Number(id || 0),
        },
      ],
      user,
    );
    return await this.service.repo.save(cartProducts[0]);
  }

  @Patch('/bulk')
  @ApiOperation({
    summary: 'Cập nhật nhiều sản phẩm trong giỏ hàng',
  })
  @ApiBody({
    type: [CreateUpdateCartProductDto],
  })
  @ApiOkResponse({
    type: [CartProductEntity],
  })
  async updateMany(
    @Body() dtos: CreateUpdateCartProductDto[],
    @User() user: UserEntity,
  ): Promise<CartProductEntity[]> {
    const cartProducts = await this.service.calculateCartProduct(dtos, user);
    return await this.service.repo.save(cartProducts);
  }

  @Delete('/deletes')
  @ApiOperation({
    summary: 'Xóa nhiều sản phẩm trong giỏ hàng',
  })
  @ApiImplicitQuery({
    name: 'ids',
    type: [Number],
    isArray: true,
    description: 'Danh sách id giỏ hàng',
  })
  async deleteMany(@Query('ids') ids: number[], @User() user: UserEntity) {
    if (typeof ids === 'string') {
      ids = [ids];
    }
    const idDeletes = ids?.map((x) => Number(x || 0));
    await this.service.repo.delete({ id: In(idDeletes), userId: user.id });
  }
}

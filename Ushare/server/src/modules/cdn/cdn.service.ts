import { BadRequestException, Injectable } from '@nestjs/common';
import { FastifyRequest } from 'fastify';
import { extname, join } from 'path';
import { randomUUID } from 'crypto';
import { S3 } from 'aws-sdk';
import { ConfigService } from '@nestjs/config';
import {
  UploadVideoDto,
  UploadVideoResponseDto,
} from './dto/upload-video-response.dto';
import { UploadResponseDto } from './dto/upload-response.dto';
// import * as ffmpeg from 'fluent-ffmpeg';
import * as fs from 'fs';
import * as os from 'os';
import * as stream from 'stream';
import * as util from 'util';
@Injectable()
export class CdnService {
  constructor(private configService: ConfigService) {}

  async uploadFile(req: FastifyRequest) {
    //Check request is multipart
    // @ts-ignore
    if (!req.isMultipart()) {
      throw new BadRequestException('Request is not multipart');
    }
    const files = [];
    // @ts-ignore
    const parts = req.files();
    const s3 = new S3();
    try {
      for await (const part of parts) {
        const filename = randomUUID().toString() + extname(part.filename);
        const uploadResult = await s3
          .upload({
            Bucket: this.configService.get('BIZFLY_STORAGE_BUCKET_NAME'),
            Body: part.file,
            Key: filename,
            ContentType: part.mimetype,
            ACL: 'public-read',
          })
          .promise();
        files.push(
          this.configService.get('BIZFLY_STORAGE_ENDPOINT') + uploadResult.Key,
        );
      }
      const response = new UploadResponseDto();
      response.files = files;
      return Promise.resolve(response);
    } catch (e) {
      throw new BadRequestException(e);
    }
  }

  //Save files in directory
  // async handler(
  //   field: string,
  //   file: any,
  //   filename: string,
  //   encoding: string,
  //   mimetype: string,
  // ): Promise<void> {
  //   // const pipeline = util.promisify(stream.pipeline);
  //   const writeStream = fs.createWriteStream(
  //     join(
  //       __dirname,
  //       `../../../cdn/${randomUUID().toString() + extname(filename)}`,
  //     ),
  //   );
  //   try {
  //     await pipeline(file, writeStream);
  //   } catch (err) {
  //     console.error('Pipeline failed', err);
  //   }
  // }

  async uploadVideoFile(req: FastifyRequest) {
    //Check request is multipart
    // @ts-ignore
    if (!req.isMultipart()) {
      throw new BadRequestException('Request is not multipart');
    }
    const files: UploadVideoDto[] = [];
    // @ts-ignore
    const parts = req.files();
    const prefixUrl = this.configService.get('BIZFLY_STORAGE_ENDPOINT');
    const s3 = new S3();
    try {
      for await (const part of parts) {
        const uuid = randomUUID().toString();
        // get thumbnail of video
        const tmpVideoPath = join(
          os.tmpdir(),
          `${uuid}${extname(part.filename)}`,
        );
        const thumbnailFileName = `${uuid}.png`;
        const tmpThumbnailPath = join(os.tmpdir(), `${thumbnailFileName}`);
        const writeStream = fs.createWriteStream(tmpVideoPath);
        const pump = util.promisify(stream.pipeline);
        await pump(part.file, writeStream);
        writeStream.close();
        // MUST check truncated state after pump() function
        if (part.file.truncated) {
          // FILE SIZE EXCEED
          throw new BadRequestException('File quá lớn')
        }

        const uploadVideoResult = await s3
          .upload({
            Bucket: this.configService.get('BIZFLY_STORAGE_BUCKET_NAME'),
            Body: fs.createReadStream(tmpVideoPath),
            Key: `video/${uuid}${extname(part.filename)}`,
            ContentType: part.mimetype,
            ACL: 'public-read',
          })
          .promise();

        let ffmpegPromise = new Promise((resolve, reject) => {
          // ffmpeg(tmpVideoPath)
          //   .on('end', function () {
          //     console.log('Screenshots taken');
          //     resolve("Success!");
          //   })
          //   .on('error', function (err) {
          //     reject(err);
          //   })
          //   .screenshots({
          //     count: 1,
          //     filename: thumbnailFileName,
          //     folder: os.tmpdir()
          //   });
        });
        await ffmpegPromise.then().catch((err) => {
          throw new BadRequestException(err);
        });
        const uploadThumbnailResult = await s3
          .upload({
            Bucket: this.configService.get('BIZFLY_STORAGE_BUCKET_NAME'),
            Body: fs.createReadStream(tmpThumbnailPath),
            Key: `video/${thumbnailFileName}`,
            ContentType: 'image/png',
            ACL: 'public-read',
          })
          .promise();
        try {
          fs.unlinkSync(tmpVideoPath);
          fs.unlinkSync(tmpThumbnailPath);
        } catch (err) {
          console.error(err);
        }

        files.push({
          url: prefixUrl + uploadVideoResult.Key,
          thumbnail: prefixUrl + uploadThumbnailResult.Key,
        });
      }
      const response = new UploadVideoResponseDto();
      response.files = files;
      return Promise.resolve(response);
    } catch (e) {
      throw new BadRequestException(e);
    }
  }
}

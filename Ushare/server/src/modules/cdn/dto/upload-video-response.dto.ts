import { ApiProperty } from '@nestjs/swagger';


export class UploadVideoDto {
  @ApiProperty({
    description: 'Đường dẫn file video',
    type: String,
  })
  url: string;

  @ApiProperty({
    description: 'Đường dẫn file thumbnail',
    type: String,
  })
  thumbnail: string;
}

export class UploadVideoResponseDto {
  @ApiProperty({
    description: 'Files đã upload',
    type: [UploadVideoDto],
  })
  files: UploadVideoDto[];
}



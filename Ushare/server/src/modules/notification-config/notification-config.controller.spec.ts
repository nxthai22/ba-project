import { Test, TestingModule } from '@nestjs/testing';
import { NotificationConfigController } from './notification-config.controller';
import { NotificationConfigService } from './notification-config.service';

describe('NotificationConfigController', () => {
  let controller: NotificationConfigController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NotificationConfigController],
      providers: [NotificationConfigService],
    }).compile();

    controller = module.get<NotificationConfigController>(NotificationConfigController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

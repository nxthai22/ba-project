import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NotificationConfigEntity } from 'entities/notification-config.entity';
import { MerchantModule } from 'modules/merchant/merchant.module';
import { NotificationModule } from 'modules/notification/notification.module';
import { NotificationService } from 'modules/notification/notification.service';
import { UserModule } from 'modules/user/user.module';
import { NotificationConfigController } from './notification-config.controller';
import { NotificationConfigService } from './notification-config.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      NotificationConfigEntity,
    ]),
    NotificationModule,
    MerchantModule,
    UserModule],
  controllers: [NotificationConfigController],
  providers: [NotificationConfigService],
})
export class NotificationConfigModule { }

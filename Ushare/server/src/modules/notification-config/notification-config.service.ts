import { BadRequestException, ForbiddenException, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { NotificationConfigEntity, NotificationStatus, NotificationType } from 'entities/notification-config.entity';
import { RoleType } from 'entities/role.entity';
import { UserEntity } from 'entities/user.entity';
import { MerchantService } from 'modules/merchant/merchant.service';
import { NotificationService } from 'modules/notification/notification.service';
import { UserService } from 'modules/user/user.service';
import { Repository } from 'typeorm';
import { ApproveNotificationConfigDto } from './dto/approve-notification-config.dto';
import { CreateNotificationConfigDto } from './dto/create-notification-config.dto';

@Injectable()
export class NotificationConfigService extends TypeOrmCrudService<NotificationConfigEntity> {

  constructor(
    @InjectRepository(NotificationConfigEntity)
    public repo: Repository<NotificationConfigEntity>,
    private userService: UserService,
    private notificationService: NotificationService,
    public merchantService: MerchantService,
  ) {
    super(repo);
  }
  private readonly logger = new Logger(NotificationConfigService.name);

  async createNotification(dto: CreateNotificationConfigDto, user: UserEntity) {
    this.logger.log({
      method: 'createNotification',
      dto: dto,
      userId: user.id
    })
    // validate role
    if (user.role.type !== RoleType.ADMIN && user.role.type !== RoleType.MERCHANT) {
      throw new ForbiddenException('Tài khoản không có quyền thực hiện');
    }
    if (!dto.scheduledTime || dto.scheduledTime <= new Date()) {
      throw new BadRequestException('Thời gian gửi không được nhỏ hơn hiện tại');
    }
    // create new Notification
    const notification: NotificationConfigEntity = {
      ...dto,
      merchantId: user.merchantId,
      status: NotificationStatus.NEW
    }
    // allow admin approve when creating notification
    if (dto.status === NotificationStatus.APPROVED && user.role.type === RoleType.ADMIN) {
      notification.status = NotificationStatus.APPROVED;
    }
    const notiConfigEntity = await this.repo.save(notification);

    // Notify to Admin
    if (user.merchantId) {
      const query = this.userService.repo
        .createQueryBuilder('user')
        .select('"user".*')
        .leftJoin('role', 'role', '"role"."id" = "user"."roleId"')
        .where(
          '"role"."type" = :roleType',
          {
            roleType: RoleType.ADMIN,
          },
        );
      const adminUshare: UserEntity[] = (await query.execute()) as
        | UserEntity[]
        | [];
      const userIds = adminUshare.map((u) => u.id);
      const merchant = await this.merchantService.repo.findOne(user.merchantId);
      this.notificationService.sendNotificationToUsers({
        userIds,
        title: `${merchant?.name} đang chờ duyệt thông báo`,
        body: ``,
        type: NotificationType.OTHER,
        data: {
          notiConfigId: notiConfigEntity.id,
        },
      });
    }
    return notiConfigEntity;
  }

  async updateNotification(id: number, dto: CreateNotificationConfigDto, user: UserEntity) {
    this.logger.log({
      method: 'updateNotification',
      dto: dto,
      id,
      userId: user.id
    })
    // validate role
    if (user.role.type !== RoleType.ADMIN && user.role.type !== RoleType.MERCHANT) {
      throw new ForbiddenException('Tài khoản không có quyền thực hiện');
    }
    if (dto.scheduledTime <= new Date()) {
      throw new BadRequestException('Thời gian gửi không được nhỏ hơn hiện tại');
    }
    // check exist
    const notification = await this.repo.findOne(id);
    if (!notification) {
      throw new BadRequestException('Thông báo không tồn tại');
    }
    // check permission
    if (user.merchantId && user.merchantId !== notification.merchantId) {
      throw new BadRequestException('Thông báo không thuộc về bạn');
    }

    // update notification
    await this.repo.update(id, dto);
    return await this.repo.findOne(id);
  }

  async approveNotification(id: number, dto: ApproveNotificationConfigDto, user: UserEntity) {
    this.logger.log({
      method: 'approveNotification',
      id,
      userId: user.id
    })
    // check exist
    const notification = await this.repo.findOne(id);
    if (!notification) {
      throw new BadRequestException('Thông báo không tồn tại');
    }
    if (notification.status !== NotificationStatus.NEW) {
      throw new BadRequestException('Không thể duyệt thông báo đang ở trạng thái khác "Mới"');
    }
    // validate role
    if (user.role.type !== RoleType.ADMIN) {
      throw new ForbiddenException('Tài khoản không có quyền thực hiện');
    }

    // validate status
    if (dto.action !== NotificationStatus.APPROVED && dto.action !== NotificationStatus.REJECTED) {
      throw new BadRequestException('Không thể duyệt thông báo');
    }

    // validate time
    if (notification.scheduledTime < new Date()) {
      throw new BadRequestException('Đã quá hạn gửi thông báo');
    }

    // update notification
    await this.repo.update(id, { status: dto.action, rejecedReason: dto.rejectReason, updatedAt: () => '"updatedAt"' });


    // Send notification
    if (notification.merchantId) {
      const query = this.userService.repo.createQueryBuilder('user')
        .select('"user".*')
        .leftJoin(
          'role',
          'role',
          '"role"."id" = "user"."roleId"',
        )
        .where('"user"."merchantId" = :merchantId AND "role"."type" = :roleType', {
          merchantId: notification.merchantId,
          roleType: RoleType.MERCHANT
        });
      const adminMerchants: UserEntity[] = (await query.execute()) as UserEntity[] | [];
      const userIds = adminMerchants.map(u => u.id);
      let title = '';
      if (dto.action === NotificationStatus.APPROVED) {
        title = `Thông báo ${notification.title} đã được duyệt`;
      } else if (dto.action === NotificationStatus.REJECTED) {
        title = `Thông báo đã bị từ chối, lý do ${dto.rejectReason} `;
      }
      this.notificationService.sendNotificationToUsers({
        userIds,
        title,
        body: '',
        type: NotificationType.OTHER,
        data: {
          notiConfigId: id
        }
      })
    }

    return await this.repo.findOne(id);
  }
}
import { ApiProperty } from '@nestjs/swagger';

export class RegisterFcmTokenDto {
  @ApiProperty({
    description: 'FCM token',
    type: String,
    required: true,
  })
  fcmToken?: string;
}

import { PickType } from '@nestjs/swagger';
import { NotificationConfigEntity } from 'entities/notification-config.entity';

export class CreateNotificationConfigDto extends PickType(NotificationConfigEntity, [
  'title',
  'body',
  'content',
  'attachedFile',
  'receivedType',
  'scheduledTime',
  'type',
  'status',
]) { }

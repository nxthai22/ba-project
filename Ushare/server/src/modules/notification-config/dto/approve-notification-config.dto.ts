import { ApiProperty } from '@nestjs/swagger';
import { NotificationStatus } from 'entities/notification-config.entity';

export class ApproveNotificationConfigDto {
  @ApiProperty({
    description: 'Lý do từ chối',
    required: false,
    type: String,
  })
  rejectReason: string;

  @ApiProperty({
    description: 'Approve action',
    required: true,
    type: 'enum',
    enum: NotificationStatus,
  })
  action: NotificationStatus;
}

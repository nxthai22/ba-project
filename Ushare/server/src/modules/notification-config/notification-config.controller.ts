import { Body, Param, Patch, Post } from '@nestjs/common';
import { ApiBody, ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { CrudAuth, CrudController, CrudRequest, GetManyDefaultResponse, Override, ParsedBody, ParsedRequest } from '@nestjsx/crud';
import { Auth } from 'decorators/auth.decorator';
import { Crud } from 'decorators/crud.decorator';
import { User } from 'decorators/user.decorator';
import { NotificationConfigEntity } from 'entities/notification-config.entity';
import { NotificationTokenEntity } from 'entities/notification-token.entity';
import { RoleType } from 'entities/role.entity';
import { UserEntity } from 'entities/user.entity';
import { ApproveNotificationConfigDto } from './dto/approve-notification-config.dto';
import { CreateNotificationConfigDto } from './dto/create-notification-config.dto';
import { RegisterFcmTokenDto } from './dto/register-fcm-token.dto';
import { NotificationConfigService } from './notification-config.service';

@Crud({
  name: 'Cài đặt thông báo',
  controller: 'notification-configs',
  model: {
    type: NotificationConfigEntity,
  },
  routes: {
    include: ['deleteOneBase'],
    createOneBase: {
      decorators: [Auth()],
    },
    updateOneBase: {
      decorators: [Auth()],
    },
  },
  query: {
    join: {
      merchant: {},
    },
  },
})
@CrudAuth({
  property: 'user',
  filter: (user: UserEntity) => {
    if ([RoleType.MERCHANT, RoleType.ADMIN].includes(user?.role?.type)) {
      return {
        ...(user.merchantId && { merchantId: user.merchantId }),
      };
    }
  },
  persist: (user: UserEntity) => {
    if ([RoleType.MERCHANT, RoleType.ADMIN].includes(user?.role?.type)) {
      return {
        merchantId: user.merchantId,
      };
    }
  },
})
export class NotificationConfigController
  implements CrudController<NotificationConfigEntity>
{
  constructor(public readonly service: NotificationConfigService) { }

  get base(): CrudController<NotificationConfigEntity> {
    return this;
  }

  @Auth()
  @Override()
  async createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: CreateNotificationConfigDto,
    @User() user: UserEntity,
  ) {
    return this.service.createNotification(dto, user);
  }

  @Auth()
  @Override()
  async updateOne(
    @Param('id') id: number,
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: CreateNotificationConfigDto,
    @User() user: UserEntity,
  ) {
    return this.service.updateNotification(id, dto, user);
  }

  @Auth()
  @Patch('/:id/approval')
  @ApiOperation({
    summary: 'Phê duyệt thông báo',
  })
  @ApiOkResponse({
    type: NotificationConfigEntity,
  })
  async approveNotification(
    @ParsedRequest() req: CrudRequest,
    @Param('id') id: number,
    @Body() dto: ApproveNotificationConfigDto,
    @User() user: UserEntity,
  ) {
    return this.service.approveNotification(id, dto, user);
  }

  @Auth()
  @Override()
  async getMany(
    @ParsedRequest() req: CrudRequest,
    @User() user: UserEntity,
  ): Promise<GetManyDefaultResponse<NotificationConfigEntity> | NotificationConfigEntity[]> {
    return this.base.getManyBase(req);
  }

  @Auth()
  @Override()
  async getOne(@ParsedRequest() req: CrudRequest) {
    const response = await this.base.getOneBase(req);
    return response;
  }

  @Override()
  @Auth()
  async deleteOne(
    @Param('id') id: number,
    @ParsedRequest() req: CrudRequest,
  ): Promise<void | NotificationConfigEntity> {
    const notification = await this.service.repo.findOne({ id });
    return this.service.repo.softRemove(notification);
  }
}

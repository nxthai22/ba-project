import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ShippingAddressEntity } from 'entities/shipping-address.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ShippingAddressService extends TypeOrmCrudService<ShippingAddressEntity> {
  constructor(
    @InjectRepository(ShippingAddressEntity)
    public repo: Repository<ShippingAddressEntity>,
  ) {
    super(repo);
  }
}

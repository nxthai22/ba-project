import { CrudController } from '@nestjsx/crud';
import { Crud } from 'decorators/crud.decorator';
import { ShippingAddressEntity } from 'entities/shipping-address.entity';
import { ShippingAddressService } from './shipping-address.service';
import { Auth } from 'decorators/auth.decorator';

@Crud({
  controller: 'shipping-addresses',
  name: 'Địa chỉ giao hàng',
  model: {
    type: ShippingAddressEntity,
  },
})
@Auth()
export class ShippingAddressController
  implements CrudController<ShippingAddressEntity>
{
  constructor(public readonly service: ShippingAddressService) {}
}

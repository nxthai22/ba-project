import { Crud } from '../../decorators/crud.decorator';
import { Auth } from '../../decorators/auth.decorator';
import { CrudController } from '@nestjsx/crud';
import { BankEntity } from '../../entities/bank.entity';
import { BankService } from './bank.service';

@Crud({
  controller: 'bank',
  name: 'Ngân hàng',
  model: {
    type: BankEntity,
  },
  routes: {
    createOneBase: {
      decorators: [Auth()],
    },
    updateOneBase: {
      decorators: [Auth()],
    },
    deleteOneBase: {
      decorators: [Auth()],
    },
  },
})
export class BankController implements CrudController<BankEntity> {
  constructor(public readonly service: BankService) {}
}

import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BankEntity } from '../../entities/bank.entity';

@Injectable()
export class BankService extends TypeOrmCrudService<BankEntity> {
  constructor(
    @InjectRepository(BankEntity) public repo: Repository<BankEntity>,
  ) {
    super(repo);
  }
}

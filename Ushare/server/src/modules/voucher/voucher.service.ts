import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { OrderRevenueEntity } from 'entities/order-revenue.entity';
import { OrderEntity } from 'entities/order.entity';
import { UserEntity } from 'entities/user.entity';
import { VoucherHistoryEntity } from 'entities/voucher-history.entity';
import {
  VoucherDiscountType,
  VoucherEntity,
  VoucherType,
} from 'entities/voucher.entity';
import { OrderStatus } from 'enums';
import {
  In,
  IsNull,
  LessThanOrEqual,
  MoreThan,
  MoreThanOrEqual,
  Not,
  QueryRunner,
  Raw,
  Repository,
} from 'typeorm';
import { UserService } from '../user/user.service';
import { RoleType } from '../../entities/role.entity';
import { compareAsc } from 'date-fns';
import { CreateVoucherDto } from './dto/create-voucher.dto';
import { ProductEntity } from 'entities/product.entity';
import { CrudRequest, GetManyDefaultResponse, ParsedBody } from '@nestjsx/crud';
import {
  CreateOrderDto,
  CreateOrderProduct,
} from '../order/dto/create-order.dto';
import { OrderProductEntity } from '../../entities/order-product.entity';
import {
  DiscountValueType,
  FlashSaleDetailEntity,
} from '../../entities/flash-sale-detail.entity';
import { UpdateVoucherHistoryDto } from './dto/update-voucher.dto';
import { User } from 'decorators/user.decorator';
import { CartProductEntity } from 'entities/cart-product.entity';
import { FlashSaleService } from 'modules/flash-sale/flash-sale.service';
import { _ } from 'lodash';
import { da } from 'date-fns/locale';
import { MerchantAddressService } from 'modules/merchant-address/merchant-address.service';
import { isNull } from 'util';
import { CalculateVoucherDto } from './dto/caculate-voucher.dto';

@Injectable()
export class VoucherService extends TypeOrmCrudService<VoucherEntity> {
  constructor(
    @InjectRepository(VoucherEntity) public repo: Repository<VoucherEntity>,
    @InjectRepository(OrderRevenueEntity)
    public orderRevenueRepo: Repository<OrderRevenueEntity>,
    @InjectRepository(VoucherHistoryEntity)
    public voucherHistoryRepo: Repository<VoucherHistoryEntity>,
    @InjectRepository(OrderEntity)
    public orderRepo: Repository<OrderEntity>,
    @InjectRepository(ProductEntity)
    public productRepo: Repository<ProductEntity>,
    @InjectRepository(FlashSaleDetailEntity)
    public flashSaleDetailRepo: Repository<FlashSaleDetailEntity>,
    @InjectRepository(CartProductEntity)
    public cartProductRepo: Repository<CartProductEntity>,
    private userService: UserService,
    public flashSaleService: FlashSaleService,
    public merchantAddressService: MerchantAddressService,
  ) {
    super(repo);
  }

  /** Thêm thông tin voucher */
  async createVoucherNew(
    dto: CreateVoucherDto,
    user: UserEntity,
  ): Promise<VoucherEntity> {
    const userData = await this.userService.repo.findOne({
      where: {
        id: user,
      },
      relations: ['role'],
    });
    if (userData.role.type !== RoleType.MERCHANT) {
      throw new BadRequestException('Tài khoản không có quyền');
    }
    if (compareAsc(new Date(dto.startDate), new Date(dto.endDate)) > 0) {
      throw new BadRequestException('Ngày bắt đầu phải nhỏ hơn ngày kết thúc');
    }
    const voucherExist = await this.repo.findOne({
      where: {
        code: dto.code,
        merchantId: user.merchantId,
      },
    });
    if (voucherExist) {
      throw new BadRequestException('Mã voucher đã tồn tại trên hệ thống');
    }
    return await this.repo.save(dto);
  }

  /** Cập nhật thông tin voucher */
  async updateVoucher(
    dto: CreateVoucherDto,
    user: UserEntity,
  ): Promise<VoucherEntity> {
    const voucherExist = await this.repo.findOne({
      where: {
        code: dto.code,
        merchantId: user.merchantId,
        id: Not(In([dto.id])),
      },
    });
    if (voucherExist) {
      throw new BadRequestException('Mã voucher đã tồn tại trên hệ thống');
    }
    const voucher = await this.repo.findOne({
      id: dto.id,
    });
    if (compareAsc(new Date(), new Date(voucher.startDate)) >= 0) {
      throw new BadRequestException(
        'Vourcher đang trong thời gian được áp dụng',
      );
    }
    return await this.repo.save(dto);
  }

  /** Lấy danh sách voucher */
  async getVoucherForUser(
    req: CrudRequest,
    user: UserEntity,
  ): Promise<GetManyDefaultResponse<VoucherEntity>> {
    if (user.role.type === RoleType.PARTNER) {
      const merchantId = req.parsed.filter.find(
        (f) => f.field === 'merchantId',
      )?.value;
      const page = req.parsed.page;
      const limit = req.parsed.limit;
      const query = this.repo
        .createQueryBuilder('voucher')
        .leftJoinAndSelect(
          'voucher_history',
          'voucherHistory',
          '"voucher"."id" = "voucherHistory"."voucherId"',
        )
        .where('"voucherHistory"."orderId" IS NULL')
        .andWhere(
          ':date BETWEEN "voucher"."startDate" AND "voucher"."endDate"',
          {
            date: new Date(),
          },
        )
        .select('"voucher".*');
      if (!merchantId) {
        query.andWhere('"voucher"."merchantId" IS NULL');
      } else {
        query.andWhere('"voucher"."merchantId" = :merchantId', {
          merchantId: merchantId,
        });
      }
      const total = (await query.execute())?.length || 0;
      if (limit) {
        query.offset((page - 1) * limit).limit(limit);
      }
      const data = ((await query.execute()) as VoucherEntity[]) || [];
      return {
        total: total,
        data: data,
        count: data?.length || 0,
        page: page || 1,
        pageCount: Math.ceil(total / limit || total || 1),
      };
    }
    return {
      total: 0,
      data: [],
      count: 0,
      page: 1,
      pageCount: 0,
    };
  }

  /** Lưu thông tin sử dụng voucher */
  async updateVoucherHistory(
    dto: UpdateVoucherHistoryDto,
    queryRunner?: QueryRunner,
  ): Promise<VoucherHistoryEntity[]> {
    const histories: VoucherHistoryEntity[] = [];
    // Tạo mới đơn hàng
    if (
      dto.order?.status === OrderStatus.CREATED &&
      (dto.voucherCodes || dto.voucherIds)
    ) {
      const vouchers = await this.repo.find({
        where: {
          ...(dto?.voucherIds && dto?.voucherIds.length > 0
            ? { id: In([...dto.voucherIds]) }
            : { code: In([...dto.voucherCodes]) }),
          quantity: Raw((alias) => {
            return `${alias} > "VoucherEntity"."quantityUsed"`;
          }),
          endDate: MoreThan(new Date()),
          startDate: LessThanOrEqual(new Date()),
        },
      });
      const voucherHistories = await this.voucherHistoryRepo.find({
        where: {
          voucherId: In(vouchers.map((v) => v.id)),
          orderId: Not(IsNull()),
          tel: dto.order.tel,
        },
        relations: ['voucher'],
      });
      if (voucherHistories && voucherHistories?.length > 0) {
        throw new BadRequestException(
          `Mã giảm giá ${
            voucherHistories?.[0]?.voucher?.name || ''
          } đã được sử dụng`,
        );
      }
      vouchers?.map(async (voucher) => {
        voucher.quantityUsed += 1;
        const history = new VoucherHistoryEntity();
        history.voucherId = voucher.id;
        history.tel = dto.order.tel;
        history.orderId = dto.order.id;
        histories.push(history);
      });
      if (queryRunner) {
        await queryRunner.manager.save(VoucherEntity, vouchers);
        return await queryRunner.manager.save(VoucherHistoryEntity, histories);
      } else {
        await this.repo.save(vouchers);
        return await this.voucherHistoryRepo.save(histories);
      }
    }

    // Chuyển trạng thái đơn hàng
    if (
      [OrderStatus.CANCELLED, OrderStatus.MERCHANT_CANCELLED].includes(
        dto.order?.status,
      )
    ) {
      const voucherHistories = await this.voucherHistoryRepo.find({
        where: {
          orderId: dto.order.id,
        },
      });
      const vouchers = await this.repo.find({
        where: {
          id: In(voucherHistories.map((history) => history.voucherId)),
        },
      });
      vouchers?.map((voucher) => {
        voucher.quantityUsed -= 1;
        voucher.quantityUsed =
          voucher.quantityUsed < 0 ? 0 : voucher.quantityUsed;
        return voucher;
      });
      if (queryRunner) {
        await queryRunner.manager.save(VoucherEntity, vouchers);
        return await queryRunner.manager.remove(
          VoucherHistoryEntity,
          voucherHistories,
        );
      } else {
        await this.repo.save(vouchers);
        return await this.voucherHistoryRepo.remove(voucherHistories);
      }
    }
  }

  /** Tính mức giảm giá khi áp voucher */
  async calculateDiscountVoucher(
    dto: CalculateVoucherDto,
    userId: number,
    queryRunner?: QueryRunner,
    shouldApplyUshareVoucher?: boolean,
  ) {
    if (!dto.products || dto.products.length < 1) {
      throw new BadRequestException('Không có sản phẩm nào được chọn');
    }
    const productIds = dto.products.map(({ id }) => id);
    if (!dto.voucherCodes) dto.voucherCodes = [];
    const vouchers = await this.repo.find({
      where: {
        ...(dto?.voucherIds && dto?.voucherIds.length > 0
          ? { id: In([...dto.voucherIds]) }
          : { code: In([...dto.voucherCodes]) }),
        quantity: Raw((alias) => {
          return `${alias} > "VoucherEntity"."quantityUsed"`;
        }),
        endDate: MoreThan(new Date()),
        startDate: LessThanOrEqual(new Date()),
      },
    });
    if (dto?.voucherIds && dto?.voucherIds.length > 0) {
      const validVoucherIds = vouchers.map((v) => v.id);
      if (
        validVoucherIds &&
        !dto.voucherIds.every((vid) => validVoucherIds.includes(vid))
      ) {
        throw new BadRequestException('Ưu đãi bạn chọn đã hết hiệu lực');
      }
    } else if (dto?.voucherCodes && dto?.voucherCodes.length > 0) {
      const validVoucherCodes = vouchers.map((v) => v.code);
      if (
        validVoucherCodes &&
        !dto.voucherCodes.every((vid) => validVoucherCodes.includes(vid))
      ) {
        throw new BadRequestException('Ưu đãi bạn chọn đã hết hiệu lực');
      }
    }

    const voucherIds = vouchers.map(({ id }) => id);
    const voucherHistories = await this.voucherHistoryRepo.find({
      where: {
        id: In(voucherIds),
        tel: dto.tel,
        orderId: Not(IsNull()),
      },
    });

    if (voucherHistories.length && voucherHistories.length > 0) {
      const existedVoucherIds = voucherHistories.map(({ id }) => id);
      await this.removeVoucherCodeInCartProducts(
        userId,
        productIds,
        existedVoucherIds,
        queryRunner,
      );
    }

    if (vouchers.length === 0) {
      await this.removeVoucherCodeInCartProducts(
        userId,
        productIds,
        undefined,
        queryRunner,
      );
    }
    // Tính giảm giá cho đơn hàng
    return await this.calculateDiscountFlashSaleAndVoucher(
      dto.products,
      dto.voucherIds,
      dto.voucherCodes,
      dto.merchantAddressId,
      shouldApplyUshareVoucher,
    );
  }

  /*
   *   Caluate discount for an order or cart
   */
  async calculateDiscountFlashSaleAndVoucher(
    products: CreateOrderProduct[],
    voucherIds?: number[],
    voucherCodes?: string[],
    merchantAddressId?: number,
    shouldApplyUshareVoucher?: boolean,
  ) {
    const merchantAddress = await this.merchantAddressService.findOne({
      id: merchantAddressId,
    });
    if (!merchantAddress)
      throw new BadRequestException('Kho đặt hàng không tồn tại');

    const productIds = products.map(({ id }) => id);
    // find related Product and variants
    const productEntities = await this.productRepo.find({
      where: {
        id: In([...productIds]),
      },
      relations: ['variants'],
    });
    const orderProducts: OrderProductEntity[] = [];
    for (let i = 0; i < products.length; i++) {
      const prod = products[i];
      const productEntity = _.find(productEntities, (p) => p.id === prod.id);
      if (!productEntity)
        throw new BadRequestException(`Sản phẩm không tồn tại`);
      const orderProduct = new OrderProductEntity();
      orderProduct.productId = prod.id;
      orderProduct.product = productEntity;
      orderProduct.quantity = prod.quantity;
      // orderProduct.commission = productEntity.commission * prod.quantity || 0;
      orderProduct.voucherIds = [];
      orderProduct.commission = 0;
      orderProduct.discount = 0;
      orderProduct.discountVoucherProduct = 0;
      orderProduct.discountVoucherShop = 0;
      if (prod.variantId) {
        const variant = productEntity.variants?.find(
          (v) => v.id === prod.variantId,
        );
        if (variant) {
          orderProduct.variantId = prod.variantId;
          orderProduct.price = variant.price;
          orderProduct.variantTitle = variant.name;
          orderProduct.commission = variant.commission * prod.quantity || 0;
        }
      } else {
        orderProduct.price = productEntity.price;
        orderProduct.commission = productEntity.commission * prod.quantity || 0;
      }
      orderProducts.push(orderProduct);
    }
    // Get all related flashSale and apply flashSale price
    const now = new Date().toISOString();
    const query = this.flashSaleService.flashSaleDetailRepo
      .createQueryBuilder('flashSaleDetail')
      .select('"flashSaleDetail".*')
      .leftJoin(
        'flash_sale',
        'flashSale',
        '"flashSaleDetail"."flashSaleId" = "flashSale"."id"',
      )
      .where('"flashSale"."startTime" <= :now', { now })
      .andWhere('"flashSale"."endTime" >= :now', { now })
      .andWhere(
        '"flashSaleDetail"."quantity" > "flashSaleDetail"."usedQuantity"',
      )
      .andWhere('"flashSaleDetail"."productId" IN (:...productIds)', {
        productIds,
      });
    const flashSaleDetails = (await query.execute()) as
      | FlashSaleDetailEntity[]
      | [];
    if (flashSaleDetails) {
      // mapping order product with correcting flashsale
      for (let i = 0; i < orderProducts.length; i++) {
        const orderProduct = orderProducts[i];
        const flashSaleDetail = _.find(flashSaleDetails, (f) => {
          return (
            f.productId === orderProduct.productId &&
            (orderProduct.variantId
              ? f.productVariantId === orderProduct.variantId
              : true)
          );
        });
        if (flashSaleDetail) {
          orderProduct.flashSaleId = flashSaleDetail.flashSaleId;
          orderProduct.flashSaleDetailId = flashSaleDetail.id;
          orderProduct.price =
            Math.floor(
              flashSaleDetail?.discountValueType === DiscountValueType.AMOUNT
                ? Number(flashSaleDetail?.discountValue || 0)
                : (Number(flashSaleDetail?.discountValue || 0) / 100) *
                    orderProduct.price,
            ) || 0;
        }
      }
    }

    // Get all related vouchers
    let vouchers: VoucherEntity[] = await this.repo.find({
      where: {
        ...(voucherIds && voucherIds.length > 0
          ? { id: In([...voucherIds]) }
          : { code: In([...voucherCodes]) }),
        quantity: Raw((alias) => {
          return `${alias} > "VoucherEntity"."quantityUsed"`;
        }),
        endDate: MoreThan(new Date()),
        startDate: LessThanOrEqual(new Date()),
      },
      order: {
        merchantId: 'ASC', // will calculate voucher of Merchant first
      },
    });
    vouchers?.map((voucher) => {
      if (voucher.minProductNumber && voucher.minProductPrice) {
        const numProduct =
          orderProducts
            ?.filter((orderProd) => orderProd.price > voucher.minProductPrice)
            ?.reduce((total, curr) => {
              return total + Number(curr.quantity || 0);
            }, 0) || 0;
        if (numProduct < voucher.minProductNumber)
          throw new BadRequestException(
            `Đơn hàng chưa đủ điều kiện áp dụng mã giảm giá ${voucher.code}`,
          );
      }
    });
    // make sure voucher is only related with Merchant and Ushare
    vouchers = vouchers.filter(
      (v) => !v.merchantId || v.merchantId === merchantAddress.merchantId,
    );
    let numberVoucherPerMerchant = 0;
    let numberVoucherPerUshare = 0;
    let voucherMerchant;
    let voucherUshare;
    let voucherMerchantIsUsed = false;
    let voucherUshareIsUsed = false;
    for (let i = 0; i < vouchers.length; i++) {
      const voucher = vouchers[i];
      if (voucher.merchantId) {
        voucherMerchant = voucher;
        numberVoucherPerMerchant++;
      } else {
        voucherUshare = voucher;
        numberVoucherPerUshare++;
      }
      if (numberVoucherPerMerchant > 1 || numberVoucherPerUshare > 1) {
        throw new BadRequestException('Quá nhiều voucher được áp dụng');
      }
    }

    const totalPriceBeforeDiscount = _(orderProducts)
      .map((op) => op.price * op.quantity)
      .sum();
    let voucherMerchantDiscount = 0;
    let voucherUshareDiscount = 0;
    // let totalPrice = 0;
    // let totalCommission = 0;
    let orderPriceAfterApplyVoucherMerchant = totalPriceBeforeDiscount;
    const orderVoucherIds = [];
    // Apply voucher merchant
    if (voucherMerchant) {
      const minAmount = voucherMerchant.minAmount || 0;
      if (
        voucherMerchant.quantityUsed < voucherMerchant.quantity &&
        minAmount <= totalPriceBeforeDiscount
      ) {
        if (voucherMerchant.type === VoucherType.SHOP) {
          // Apply for order
          switch (voucherMerchant.discountType) {
            case 'percentage':
              voucherMerchantIsUsed = true;
              voucherMerchantDiscount = Math.round(
                (totalPriceBeforeDiscount * voucherMerchant.discountValue) /
                  100,
              );
              orderPriceAfterApplyVoucherMerchant =
                totalPriceBeforeDiscount - voucherMerchantDiscount;
              orderVoucherIds.push(voucherMerchant.id);
              let percentage = voucherMerchant.discountValue / 100;
              if (
                voucherMerchant.discountMaximum &&
                voucherMerchantDiscount > voucherMerchant.discountMaximum
              ) {
                // can not exceed discountMaximum
                voucherMerchantDiscount = voucherMerchant.discountMaximum;
                orderPriceAfterApplyVoucherMerchant =
                  totalPriceBeforeDiscount - voucherMerchantDiscount;
                percentage =
                  voucherMerchant.discountMaximum / totalPriceBeforeDiscount;
              }
              // update discount for orderProducts
              for (let j = 0; j < orderProducts.length; j++) {
                const op = orderProducts[j];
                const discount = Math.round(
                  op.price * op.quantity * percentage,
                );
                op.discount = discount;
                op.discountVoucherShop = discount;
              }
              break;
            case 'amount':
              voucherMerchantIsUsed = true;
              const maxDiscount =
                voucherMerchant.discountValue > totalPriceBeforeDiscount
                  ? totalPriceBeforeDiscount
                  : voucherMerchant.discountValue;
              voucherMerchantDiscount = maxDiscount;
              orderPriceAfterApplyVoucherMerchant =
                totalPriceBeforeDiscount - voucherMerchantDiscount;
              orderVoucherIds.push(voucherMerchant.id);
              // update discount for orderProducts
              for (let j = 0; j < orderProducts.length; j++) {
                const op = orderProducts[j];
                const discount = Math.round(
                  voucherMerchantDiscount *
                    ((op.price * op.quantity) / totalPriceBeforeDiscount),
                );
                op.discount = discount;
                op.discountVoucherShop = discount;
              }
              break;
            default:
              break;
          }
        } else if (voucherMerchant.type === VoucherType.PRODUCT) {
          const totalPriceOfItemCanApplyVoucher = _(orderProducts)
            .filter((op) =>
              _.includes(voucherMerchant.productIds, op.productId),
            )
            .map((op) => op.price * op.quantity)
            .sum();
          // const numberOfItemCanApplyVoucher = _(orderProducts).filter(op => _.includes(voucherMerchant.productIds, op.productId)).count();
          // Apply for each product
          for (let i = 0; i < orderProducts.length; i++) {
            const orderProduct = orderProducts[i];
            // Check applied productId
            if (
              _.includes(voucherMerchant.productIds, orderProduct.productId)
            ) {
              switch (voucherMerchant.discountType) {
                case 'percentage':
                  voucherMerchantIsUsed = true;
                  let discount = 0;
                  const estimatedDiscount =
                    (totalPriceOfItemCanApplyVoucher *
                      voucherMerchant.discountValue) /
                    100;
                  let percentage = voucherMerchant.discountValue / 100;
                  if (
                    voucherMerchant.discountMaximum &&
                    voucherMerchant.discountMaximum > estimatedDiscount
                  ) {
                    // can not exceed discountMaximum
                    percentage =
                      ((voucherMerchant.discountMaximum / estimatedDiscount) *
                        voucherMerchant.discountValue) /
                      100;
                    discount = Math.round(
                      orderProduct.price * orderProduct.quantity * percentage,
                    );
                    voucherMerchantDiscount = voucherMerchant.discountMaximum;
                  } else {
                    discount = Math.round(
                      (orderProduct.price *
                        orderProduct.quantity *
                        voucherMerchant.discountValue) /
                        100,
                    );
                    voucherMerchantDiscount += discount;
                  }
                  orderProduct.discount = discount;
                  orderProduct.discountVoucherProduct = discount;
                  orderPriceAfterApplyVoucherMerchant =
                    totalPriceBeforeDiscount - voucherMerchantDiscount;
                  // mark order_product is applied by voucher
                  orderProduct.voucherIds.push(voucherMerchant.id);
                  orderVoucherIds.push(voucherMerchant.id);
                  break;

                case 'amount':
                  voucherMerchantIsUsed = true;
                  const maxDiscountPerItem =
                    voucherMerchant.discountValue * orderProduct.quantity;
                  const discountItem = Math.round(
                    maxDiscountPerItem >
                      orderProduct.price * orderProduct.quantity
                      ? orderProduct.price * orderProduct.quantity
                      : maxDiscountPerItem,
                  );
                  orderProduct.discount = discountItem;
                  orderProduct.discountVoucherProduct = discountItem;
                  voucherMerchantDiscount += orderProduct.discount;
                  orderPriceAfterApplyVoucherMerchant =
                    totalPriceBeforeDiscount - voucherMerchantDiscount;
                  // mark order_product is applied by voucher
                  orderProduct.voucherIds.push(voucherMerchant.id);
                  orderVoucherIds.push(voucherMerchant.id);
                  break;
                default:
                  break;
              }
            }
          }
        }
      }
    }
    // Apply voucher Ushare
    if (voucherUshare && shouldApplyUshareVoucher) {
      const minAmount = voucherUshare.minAmount || 0;
      if (
        voucherUshare.quantityUsed < voucherUshare.quantity &&
        minAmount <= totalPriceBeforeDiscount
      ) {
        if (voucherUshare.type === VoucherType.SHOP) {
          // Apply for order
          switch (voucherUshare.discountType) {
            case 'percentage':
              voucherUshareIsUsed = true;
              voucherUshareDiscount =
                (orderPriceAfterApplyVoucherMerchant *
                  voucherUshare.discountValue) /
                100;
              orderVoucherIds.push(voucherUshare.id);
              let percentage = voucherUshare.discountValue / 100;
              if (
                voucherUshare.discountMaximum &&
                voucherUshare.discountMaximum > voucherUshareDiscount
              ) {
                // can not exceed discountMaximum
                voucherUshareDiscount = voucherUshare.discountMaximum;
                percentage =
                  voucherUshare.discountMaximum /
                  orderPriceAfterApplyVoucherMerchant;
              }
              // update discount for orderProducts
              for (let j = 0; j < orderProducts.length; j++) {
                const op = orderProducts[j];
                const previousDiscount = op.discount || 0;
                const discount = Math.round(
                  (op.price * op.quantity - previousDiscount) * percentage,
                );
                op.discount += discount;
                op.discountVoucherShop += discount;
              }
              break;
            case 'amount':
              voucherUshareIsUsed = true;
              const maxDiscount =
                voucherUshare.discountValue >
                orderPriceAfterApplyVoucherMerchant
                  ? orderPriceAfterApplyVoucherMerchant
                  : voucherUshare.discountValue;
              voucherUshareDiscount = maxDiscount;
              orderVoucherIds.push(voucherUshare.id);
              // update discount for orderProducts
              for (let j = 0; j < orderProducts.length; j++) {
                const op = orderProducts[j];
                const previousDiscount = op.discount || 0;
                const currentPrice = op.price * op.quantity - previousDiscount;
                const discount = Math.round(
                  voucherUshareDiscount *
                    (currentPrice / orderPriceAfterApplyVoucherMerchant),
                );
                op.discount += discount;
                op.discountVoucherShop += discount;
              }
              break;
            default:
              break;
          }
        } else if (voucherUshare.type === VoucherType.PRODUCT) {
          const totalPriceOfItemCanApplyVoucher = _(orderProducts)
            .filter((op) => _.includes(voucherUshare.productIds, op.productId))
            .map((op) => op.price * op.quantity)
            .sum();
          // Apply for each product
          for (let i = 0; i < orderProducts.length; i++) {
            const orderProduct = orderProducts[i];
            // Check applied productId
            if (_.includes(voucherUshare.productIds, orderProduct.productId)) {
              switch (voucherUshare.discountType) {
                case 'percentage':
                  voucherUshareIsUsed = true;
                  const estimatedDiscount =
                    (totalPriceOfItemCanApplyVoucher *
                      voucherUshare.discountValue) /
                    100;
                  const previousDiscount = orderProduct.discount || 0;
                  if (
                    voucherUshare.discountMaximum &&
                    voucherUshare.discountMaximum > estimatedDiscount
                  ) {
                    // can not exceed discountMaximum
                    const percentage =
                      ((voucherUshare.discountMaximum / estimatedDiscount) *
                        voucherUshare.discountValue) /
                      100;
                    const productDiscount = Math.round(
                      (orderProduct.price * orderProduct.quantity -
                        previousDiscount) *
                        percentage,
                    );
                    orderProduct.discount += productDiscount;
                    orderProduct.discountVoucherProduct += productDiscount;
                    voucherUshareDiscount = voucherUshare.discountMaximum;
                  } else {
                    const productDiscount = Math.round(
                      ((orderProduct.price * orderProduct.quantity -
                        previousDiscount) *
                        voucherUshare.discountValue) /
                        100,
                    );
                    orderProduct.discount += productDiscount;
                    orderProduct.discountVoucherProduct += productDiscount;
                    voucherUshareDiscount += productDiscount;
                    orderPriceAfterApplyVoucherMerchant =
                      totalPriceBeforeDiscount - voucherUshareDiscount;
                  }
                  // mark order_product is applied by voucher
                  orderProduct.voucherIds.push(voucherUshare.id);
                  orderVoucherIds.push(voucherUshare.id);
                  break;
                case 'amount':
                  voucherUshareIsUsed = true;
                  const maxDiscountPerItem =
                    voucherUshare.discountValue * orderProduct.quantity;

                  const discount = Math.round(
                    maxDiscountPerItem >
                      orderProduct.price * orderProduct.quantity
                      ? orderProduct.price * orderProduct.quantity
                      : maxDiscountPerItem,
                  );
                  orderProduct.discount += discount;
                  orderProduct.discountVoucherProduct += discount;
                  voucherUshareDiscount += discount;
                  // mark order_product is applied by voucher
                  orderProduct.voucherIds.push(voucherUshare.id);
                  orderVoucherIds.push(voucherUshare.id);
                  break;
                default:
                  break;
              }
            }
          }
        }
      }
    }

    let totalVoucherDiscount = voucherMerchantDiscount + voucherUshareDiscount;

    if (totalVoucherDiscount > totalPriceBeforeDiscount) {
      totalVoucherDiscount = totalPriceBeforeDiscount;
    }
    let totalPrice = totalPriceBeforeDiscount - totalVoucherDiscount;

    // Calculate commission
    let totalCommission = 0;
    orderProducts.forEach((orderProduct) => {
      const productDiscount = orderProduct.discount || 0;
      orderProduct.commission = Math.round(
        ((orderProduct.price * orderProduct.quantity - productDiscount) *
          orderProduct.product.commissionPercent) /
          100,
      );
      totalCommission += orderProduct.commission;
    });
    return {
      voucherMerchant: voucherMerchantIsUsed ? voucherMerchant : null,
      voucherUshare: voucherUshareIsUsed ? voucherUshare : null,
      orderVoucherIds,
      totalPrice: totalPrice ? Math.round(totalPrice) : 0,
      totalPriceBeforeDiscount: totalPriceBeforeDiscount
        ? Math.round(totalPriceBeforeDiscount)
        : 0,
      totalCommission: totalCommission ? Math.round(totalCommission) : 0,
      totalVoucherDiscount: totalVoucherDiscount
        ? Math.round(totalVoucherDiscount)
        : 0,
      orderProducts,
      voucherMerchantDiscount: voucherMerchantDiscount
        ? Math.round(voucherMerchantDiscount)
        : 0,
      voucherUshareDiscount: voucherUshareDiscount
        ? Math.round(voucherUshareDiscount)
        : 0,
    };
  }

  async postSave(id: number, user: UserEntity): Promise<VoucherHistoryEntity> {
    const voucher = await this.repo.findOne({
      id,
    });
    if (!voucher) throw new NotFoundException('Mã giảm giá không đúng');
    try {
      return await this.voucherHistoryRepo.save({
        userId: user.id,
        voucherId: id,
      });
    } catch (e) {
      if (e.code === '23505')
        throw new BadRequestException('Mã giảm giá đã được lưu');
      throw new BadRequestException();
    }
  }

  async getSaved(user: UserEntity): Promise<VoucherHistoryEntity[]> {
    return await this.voucherHistoryRepo.find({
      where: {
        userId: user.id,
        orderId: null,
        voucher: {
          endDate: MoreThanOrEqual(new Date()),
        },
      },
      relations: ['voucher'],
    });
  }

  async getExistVoucher(
    @ParsedBody() dto: CreateVoucherDto,
    @User() user: UserEntity,
    id?: number,
  ) {
    const existVoucher = await this.repo.find({
      where: [
        {
          merchantId: user?.merchantId,
          code: dto.code,
          startDate: LessThanOrEqual(dto.startDate),
          endDate: MoreThanOrEqual(dto.startDate),
          ...(id && { id: Not(id) }),
        },
        {
          merchantId: user?.merchantId,
          code: dto.code,
          startDate: LessThanOrEqual(dto.endDate),
          endDate: MoreThanOrEqual(dto.endDate),
          ...(id && { id: Not(id) }),
        },
        {
          merchantId: user?.merchantId,
          code: dto.code,
          startDate: LessThanOrEqual(dto.startDate),
          endDate: MoreThanOrEqual(dto.endDate),
          ...(id && { id: Not(id) }),
        },
        {
          merchantId: user?.merchantId,
          code: dto.code,
          startDate: MoreThanOrEqual(dto.startDate),
          endDate: LessThanOrEqual(dto.endDate),
          ...(id && { id: Not(id) }),
        },
      ],
    });
    if (existVoucher.length > 0)
      throw new BadRequestException(
        'Đã tồn tại mã giảm giá đang hoặc sắp diễn ra',
      );
  }

  // Xóa những voucher code đang có của user trong bảng cart_product
  protected async removeVoucherCodeInCartProducts(
    userId: number,
    productIds: number[],
    voucherIds?: number[],
    queryRunner?: QueryRunner,
  ) {
    let cartProducts = [];

    if (voucherIds && voucherIds.length > 0) {
      const vouchers = await this.repo.find({ id: In(voucherIds) });
      const voucherCodes = vouchers.map(({ code }) => code);

      cartProducts = await this.cartProductRepo.find({
        where: {
          userId,
          voucherCode: In(voucherCodes),
        },
      });
    } else {
      cartProducts = await this.cartProductRepo.find({
        where: {
          userId,
          productId: In(productIds),
        },
      });
    }

    if (cartProducts.length === 0) return;

    cartProducts.map((cardProduct) => ({
      ...cardProduct,
      voucherCode: null,
    }));

    if (queryRunner) {
      await queryRunner.manager.save(CartProductEntity, cartProducts);
    } else {
      await this.cartProductRepo.save(cartProducts);
    }
  }
}

import {
  CrudAuth,
  CrudController,
  CrudRequest,
  GetManyDefaultResponse,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { Crud } from 'decorators/crud.decorator';
import { VoucherEntity, VoucherType } from 'entities/voucher.entity';
import { VoucherService } from './voucher.service';
import { Auth } from 'decorators/auth.decorator';
import {
  ApiBody,
  ApiOkResponse,
  ApiOperation,
  ApiProperty,
} from '@nestjs/swagger';
import {
  BadRequestException,
  Body,
  Get,
  NotFoundException,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { User } from 'decorators/user.decorator';
import { UserEntity } from 'entities/user.entity';
import { RoleType } from 'entities/role.entity';
import { VoucherHistoryEntity } from 'entities/voucher-history.entity';
import { In, IsNull, LessThan, MoreThan } from 'typeorm';
import { isWithinInterval } from 'date-fns';
import { CreateVoucherDto } from 'modules/voucher/dto/create-voucher.dto';
import { FilterVoucherDto } from './dto/filter-voucher.dto';
import { ApiImplicitQuery } from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';
import {
  CalculateVoucherDto,
  CalculateVoucherResponse,
} from './dto/caculate-voucher.dto';

export class VoucherEntityResponse
  implements GetManyDefaultResponse<VoucherEntity>
{
  @ApiProperty({
    description: 'count',
  })
  count: number;
  @ApiProperty({
    description: 'data',
    type: [VoucherEntity],
  })
  data: VoucherEntity[];
  @ApiProperty({
    description: 'page',
  })
  page: number;
  @ApiProperty({
    description: 'pageCount',
  })
  pageCount: number;
  @ApiProperty({
    description: 'total',
  })
  total: number;
}

@Crud({
  name: 'Mã giảm giá',
  controller: 'vouchers',
  model: {
    type: VoucherEntity,
  },
  routes: {
    include: ['deleteOneBase'],
    createOneBase: {
      decorators: [Auth()],
    },
    updateOneBase: {
      decorators: [Auth()],
    },
  },
  query: {
    join: {
      voucherHistories: {},
    },
  },
})
@CrudAuth({
  property: 'user',
  filter: (user: UserEntity) => {
    if ([RoleType.ADMIN].includes(user?.role?.type)) {
      return {
        merchantId: user.merchantId,
      };
    }
  },
  persist: (user: UserEntity) => {
    if ([RoleType.MERCHANT, RoleType.ADMIN].includes(user?.role?.type)) {
      return {
        merchantId: user.merchantId,
      };
    }
  },
})
export class VoucherController implements CrudController<VoucherEntity> {
  constructor(public readonly service: VoucherService) {}

  get base(): CrudController<VoucherEntity> {
    return this;
  }

  @Auth()
  @Override()
  async createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: CreateVoucherDto,
    @User() user: UserEntity,
  ) {
    await this.service.getExistVoucher(dto, user);
    return this.base.createOneBase(req, dto as VoucherEntity);
  }

  @Auth()
  @Override()
  async updateOne(
    @Param('id') id: number,
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: CreateVoucherDto,
    @User() user: UserEntity,
  ) {
    await this.service.getExistVoucher(dto, user, id);
    return this.base.updateOneBase(req, dto as VoucherEntity);
  }

  @Auth()
  @Override()
  @ApiImplicitQuery({
    name: 'productIds',
    required: false,
    description: 'Danh sách Id sản phẩm',
  })
  async getMany(
    @ParsedRequest() req: CrudRequest,
    @User() user: UserEntity,
    @Query('productIds') productIds?: string,
  ): Promise<GetManyDefaultResponse<VoucherEntity> | any> {
    const page = Number(req.parsed.page || 1);
    const limit = Number(req.parsed.limit || 10);
    if ([RoleType.PARTNER].includes(user?.role.type)) {
      // req.parsed.join.push({
      //   field: 'voucherHistories',
      // });
      // req.parsed.search.$and.push({
      //   'voucherHistories.orderId': {
      //     $isnull: true,
      //   },
      // });
      req.parsed.search.$and.push({
        startDate: {
          $lte: new Date().toISOString(),
        },
      });
      req.parsed.search.$and.push({
        endDate: {
          $gte: new Date().toISOString(),
        },
      });
    }
    if (productIds) {
      const query = this.service.repo
        .createQueryBuilder('voucher')
        .select('voucher.*')
        .andWhere('"voucher"."deletedAt" is null')
        .andWhere('"voucher"."quantity" > "voucher"."quantityUsed"')
        .andWhere('"startDate" <= :startDate', {
          startDate: new Date().toISOString(),
        })
        .andWhere('"endDate" >= :endDate', {
          endDate: new Date().toISOString(),
        });
      const merchantIdFilter = req.parsed.filter.find(
        (x) => x.field === 'merchantId',
      );
      const merchantIdOperator = merchantIdFilter?.operator;
      const merchantIdValue = merchantIdFilter?.value;
      if (merchantIdOperator === 'isnull') {
        query.andWhere({
          merchantId: IsNull(),
        });
      }
      if (merchantIdOperator === 'eq') {
        query.andWhere('"merchantId" = :merchantId', {
          merchantId: merchantIdValue,
        });
      }
      let productList: number[] = productIds
        .trim()
        .split(',')
        .map((id) => Number(id));
      const queries = productList.map((pId) => {
        return `${Number(pId)} = ANY("productIds")`;
      });
      let productIdQuery = '(' + queries.join(' OR ') + ')';
      query.andWhere(
        `("type" = :typeShop OR ("type" = :typeProduct AND ${productIdQuery}))`,
        {
          typeShop: VoucherType.SHOP,
          typeProduct: VoucherType.PRODUCT,
        },
      );

      const total = (await query.execute())?.length | 0;
      let pageCount = 1;
      if (limit) {
        query.limit(limit).offset((page - 1) * limit);
        pageCount = Math.ceil(total / limit);
      }
      const data = (await query.execute()) as VoucherEntity[] | [];
      return {
        data: data,
        page: page,
        count: data?.length | 0,
        total: total,
        pageCount: pageCount,
      };
    }
    return await this.base.getManyBase(req);
  }

  @Get('/voucher-for-guest')
  @ApiOkResponse({
    type: VoucherEntityResponse,
  })
  @ApiOperation({
    summary: 'Danh sách mã giảm giá cho khách vãng lai/CTV chưa đăng nhập',
  })
  async getManyVoucherForGuest(
    @Query() dto?: FilterVoucherDto,
  ): Promise<GetManyDefaultResponse<VoucherEntity> | VoucherEntity[]> {
    const query = this.service.repo
      .createQueryBuilder('voucher')
      .select('voucher.*')
      .leftJoin(
        'voucher_history',
        'voucherHistories',
        '"voucherHistories"."voucherId" = "voucher"."id"',
      )
      .where('"voucherHistories"."orderId" is null')
      .andWhere('"voucher"."deletedAt" is null')
      .andWhere('"startDate" <= :startDate', {
        startDate: new Date().toISOString(),
      })
      .andWhere('"endDate" >= :endDate', {
        endDate: new Date().toISOString(),
      });
    if (dto.merchantId) {
      query.andWhere('"merchantId" = :merchantId', {
        merchantId: dto.merchantId,
      });
    } else {
      query.andWhere('"merchantId" IS NULL');
    }
    if (typeof dto.productIds === 'string') dto.productIds = [dto.productIds];
    let productList: number[] = dto.productIds?.reduce((arr, id) => {
      if (!arr.includes(Number(id || 0)) && Number(id) > 0)
        arr.push(Number(id));
      return arr;
    }, []);
    if (productList?.length > 0) {
      const queries = productList?.map((pId) => {
        return `${Number(pId)} = ANY("productIds")`;
      });
      let productIdQuery = '(' + queries.join(' OR ') + ')';
      query.andWhere(
        `("type" = :typeShop OR ("type" = :typeProduct AND ${productIdQuery}))`,
        {
          typeShop: VoucherType.SHOP,
          typeProduct: VoucherType.PRODUCT,
        },
      );
    }
    const total = (await query.execute())?.length | 0;
    let pageCount = 1;
    if (Number(dto.limit)) {
      query
        .limit(Number(dto.limit))
        .offset((Number(dto.page) - 1) * Number(dto.limit));
      pageCount = Math.ceil(total / Number(dto.limit));
    }
    const data = (await query.execute()) as VoucherEntity[] | [];
    return {
      data: data,
      page: Number(dto.page),
      count: data?.length | 0,
      total: total,
      pageCount: pageCount,
    };
  }

  @Override()
  async getOne(@ParsedRequest() req: CrudRequest) {
    const response = await this.base.getOneBase(req);
    if (response?.productIds) {
      response.products = await this.service.productRepo.find({
        id: In(response?.productIds),
      });
    }
    return response;
  }

  @Get('/byCode/:code')
  @ApiOperation({
    summary: 'Lấy chi tiết Voucher theo mã',
  })
  @ApiOkResponse({
    type: VoucherEntity,
  })
  async getOneByCode(@Param('code') code: string): Promise<VoucherEntity> {
    const response = await this.service.repo.findOne({
      where: {
        code,
        startDate: LessThan(new Date()),
        endDate: MoreThan(new Date()),
      },
    });
    if (!response)
      throw new NotFoundException(
        'Không tìm thấy Mã giảm giá hoặc thời hạn sử dụng của mã đã hết',
      );
    if (response?.productIds) {
      response.products = await this.service.productRepo.find({
        id: In(response?.productIds),
      });
    }
    return response;
  }

  @Get('/saved')
  @Auth()
  @ApiOperation({
    summary: 'Danh sách mã giảm giá đã lưu',
  })
  @ApiOkResponse({
    type: [VoucherHistoryEntity],
  })
  getSaved(@User() user: UserEntity): Promise<VoucherHistoryEntity[]> {
    return this.service.getSaved(user);
  }

  @Post('/calculate-discount')
  @ApiOperation({
    summary: 'Tính giảm giá cho đơn hàng',
  })
  @ApiBody({
    type: CalculateVoucherDto,
  })
  @ApiOkResponse({
    type: CalculateVoucherResponse,
  })
  async calculateDiscountOrder(
    @Body() dto: CalculateVoucherDto,
    @User() user: UserEntity,
  ) {
    const userId = dto?.pubId ? dto?.pubId : user ? user.id : 1;
    return await this.service.calculateDiscountVoucher(dto, userId, null, true);
  }

  @Post('save/:id')
  @Auth()
  @ApiOperation({
    summary: 'Lưu mã giảm giá',
  })
  @ApiOkResponse({
    type: VoucherHistoryEntity,
  })
  async postSave(
    @Param('id') id: number,
    @User() user: UserEntity,
  ): Promise<VoucherHistoryEntity> {
    return await this.service.postSave(id, user);
  }

  @Override()
  @Auth()
  async deleteOne(
    @Param('id') id: number,
    @ParsedRequest() req: CrudRequest,
  ): Promise<void | VoucherEntity> {
    const voucher = await this.service.repo.findOne({ id });
    if (
      isWithinInterval(new Date(), {
        start: voucher.startDate,
        end: voucher.endDate,
      }) ||
      voucher.endDate <= new Date()
    )
      throw new BadRequestException(
        'Thời gian phát Voucher đang diễn ra hoặc đã kết thúc! Không thể xoá!',
      );

    return this.service.repo.softRemove(voucher);
  }
}

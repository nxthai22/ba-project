import { Module } from '@nestjs/common';
import { VoucherService } from './voucher.service';
import { VoucherController } from './voucher.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VoucherEntity } from 'entities/voucher.entity';
import { OrderRevenueEntity } from 'entities/order-revenue.entity';
import { OrderEntity } from 'entities/order.entity';
import { VoucherHistoryEntity } from 'entities/voucher-history.entity';
import { UserModule } from '../user/user.module';
import { ProductEntity } from '../../entities/product.entity';
import { FlashSaleDetailEntity } from '../../entities/flash-sale-detail.entity';
import { CartProductEntity } from 'entities/cart-product.entity';
import { FlashSaleModule } from 'modules/flash-sale/flash-sale.module';
import { MerchantAddressModule } from 'modules/merchant-address/merchant-address.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      VoucherEntity,
      VoucherHistoryEntity,
      OrderRevenueEntity,
      OrderEntity,
      ProductEntity,
      FlashSaleDetailEntity,
      CartProductEntity,
    ]),
    UserModule,
    FlashSaleModule,
    MerchantAddressModule,
  ],
  controllers: [VoucherController],
  providers: [VoucherService],
  exports: [VoucherService],
})
export class VoucherModule {}

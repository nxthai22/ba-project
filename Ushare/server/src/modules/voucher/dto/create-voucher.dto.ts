import { PickType } from '@nestjs/swagger';
import { VoucherEntity } from 'entities/voucher.entity';

export class CreateVoucherDto extends PickType(VoucherEntity, [
  'id',
  'type',
  'name',
  'code',
  'startDate',
  'endDate',
  'productIds',
  'minAmount',
  'maxAmount',
  'quantity',
  'discountType',
  'discountValue',
  'discountMaximum',
  'description',
]) {}

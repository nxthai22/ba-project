import { ApiProperty, PickType } from '@nestjs/swagger';
import { VoucherEntity } from '../../../entities/voucher.entity';
import { OrderEntity } from '../../../entities/order.entity';
import { IsNumber, IsOptional, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { CreateOrderProduct } from '../../order/dto/create-order.dto';

export class CalculateVoucherDto extends PickType(OrderEntity, [
  'wardId',
  'note',
  'paymentType',
  'merchantAddressId',
]) {
  @ApiProperty({
    description: 'Tên KH',
    required: false,
    type: String,
  })
  fullname?: string;

  @ApiProperty({
    description: 'Số điện thoại KH',
    required: false,
    type: String,
  })
  tel?: string;

  @ApiProperty({
    description: 'Địa chỉ khách hàng',
    required: false,
    type: String,
  })
  shippingAddress?: string;

  @ValidateNested({ each: true })
  @Type(() => CreateOrderProduct)
  @ApiProperty({
    description: 'Sản phẩm trong đơn hàng',
    required: false,
    type: [CreateOrderProduct],
  })
  products: CreateOrderProduct[];

  @IsNumber()
  @IsOptional()
  @ApiProperty({
    description: 'ID người giới thiệu/Id CTV',
    type: Number,
    required: false,
  })
  pubId: number;

  @ApiProperty({
    description: 'Danh sách Id voucher',
    type: [Number],
    required: false,
  })
  voucherIds?: number[];

  @ApiProperty({
    description: 'Danh sách mã voucher',
    type: [String],
    required: false,
  })
  voucherCodes?: string[];

  @ApiProperty({
    description: 'Id kho hàng',
    type: Number,
    required: false,
  })
  merchantAddressId?: number;
}

export class CalculateVoucherResponse {
  @ApiProperty({
    description: 'Mã giảm giá của NCC',
    type: VoucherEntity,
    nullable: true,
    required: false,
  })
  voucherMerchant?: VoucherEntity;

  @ApiProperty({
    description: 'Mã giảm giá của sàn',
    type: VoucherEntity,
    nullable: true,
    required: false,
  })
  voucherUshare?: VoucherEntity;

  @ApiProperty({
    description: 'Tổng giá',
    type: Number,
    required: false,
  })
  totalPrice: number | 0;

  @ApiProperty({
    description: 'Tổng giá trước khi giảm',
    type: Number,
    required: false,
  })
  totalPriceBeforeDiscount: number | 0;

  @ApiProperty({
    description: 'Tổng hoa hồng',
    type: Number,
    required: false,
  })
  totalCommission: number | 0;

  @ApiProperty({
    description: 'Tổng giảm giá',
    type: Number,
    required: false,
  })
  totalVoucherDiscount: number | 0;

  @ApiProperty({
    description: 'Giảm giá của NCC',
    type: Number,
    required: false,
  })
  voucherMerchantDiscount: number | 0;

  @ApiProperty({
    description: 'Giảm giá của sàn',
    type: Number,
    required: false,
  })
  voucherUshareDiscount: number | 0;
}

import { ApiProperty } from '@nestjs/swagger';
import { OrderEntity } from 'entities/order.entity';
import { OrderStatus } from 'enums';

export class UpdateVoucherHistoryDto {
  @ApiProperty({
    description: 'Id voucher',
    type: [Number],
  })
  voucherIds?: number[];

  @ApiProperty({
    description: 'Mã voucher',
    type: [String],
  })
  voucherCodes?: string[];

  @ApiProperty({
    description: 'Đơn hàng',
    type: OrderEntity,
  })
  order?: OrderEntity;
}

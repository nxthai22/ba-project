import { ApiProperty } from '@nestjs/swagger';
import { OrderEntity } from '../../../entities/order.entity';

export class FilterVoucherDto {
  @ApiProperty({
    description: 'Id NCC',
    type: Number,
    required: false,
  })
  merchantId?: number;

  @ApiProperty({
    description: 'Danh sách Id sản phẩm',
    type: [Number],
    required: false,
  })
  productIds?: number[];

  @ApiProperty({
    type: Number,
    required: false,
  })
  page: number | 1;

  @ApiProperty({
    type: Number,
    required: false,
  })
  limit: number | 10;
}

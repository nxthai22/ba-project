import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateDepositDto {
  @IsString()
  @ApiProperty({
    description: 'Họ tên',
  })
  fullName: string;

  @IsString()
  @ApiProperty({
    description: 'Số điện thoại',
  })
  tel: string;
}

import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { SearchCriteria } from 'modules/base/base-search';

export enum SortType {
  TOP_SELL = 'top_sell',
  MAX_PRICE = 'max_price',
  MIN_PRICE = 'min_price',
  TOP_COMMISSION = 'top_commission',
}

export class ProductQueryFilterObj {
  @ApiProperty({
    description: 'provinceId',
    type: Number,
    required: false,
  })
  attributeId: number;
  @ApiProperty({
    description: 'Id giá trị thuộc tính',
    type: [Number],
    required: false,
  })
  attributeValueIds: number[];
  @ApiProperty({
    description: 'Index đơn vị của thuộc tính',
    type: [Number],
    required: false,
  })
  attributeUnitIndexs: number[];
}

export class ProductFilterDto extends SearchCriteria {
  @IsNotEmpty()
  @ApiProperty({
    description: 'Loại tìm kiếm',
    default: SortType.TOP_SELL,
    enum: SortType,
    required: false,
  })
  sortType: SortType;

  @ApiProperty({
    description: 'Id khu vực tìm kiếm',
    required: false,
  })
  provinceId?: number;

  @ApiProperty({
    description: 'Id danh mục',
    required: false,
  })
  categoryId?: number;

  @ApiProperty({
    description: 'Id NCC',
    required: false,
  })
  merchantId?: number;
}

/** Tìm kiếm top sản phẩm tìm kiếm nhiều nhất/xu hướng tìm kiếm */
export class TopProductSearch extends PickType(SearchCriteria, [
  'page',
  'limit',
]) {
  @ApiProperty({ description: 'Danh sách từ khóa tìm kiếm' })
  keywords: string[];
}

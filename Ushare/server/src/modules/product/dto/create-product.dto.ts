import { ApiProperty, OmitType, PickType } from '@nestjs/swagger';
import { ProductEntity } from 'entities/product.entity';
import { ProductCategoryAttributeValueEntity } from 'entities/product-category-attribute-value.entity';
import { ProductVariantEntity } from 'entities/product-variant.entity';
import { Column } from 'typeorm';

export class CreateProductVariantDto extends OmitType(ProductVariantEntity, [
  'product',
]) {}

export class CreateProductCategoryAttributeValueDto extends PickType(
  ProductCategoryAttributeValueEntity,
  ['productCategoryAttributeId'],
) {
  @ApiProperty({
    description: 'Đơn vị tính',
    required: false,
  })
  productCategoryAttributeUnitIndex?: number;

  @ApiProperty({
    description: 'ID các thuộc tính đã chọn',
    required: false,
  })
  productCategoryAttributeValueIds?: number[];

  @ApiProperty({
    description: ' Truyền lên nếu NCC tự nhập thuộc tính',
    required: false,
  })
  name?: string;
}

export class CreateProductDto extends PickType(ProductEntity, [
  'name',
  'price',
  'priceBeforeDiscount',
  'thumbnail',
  'images',
  'description',
  'productCategoryId',
  'groupType',
  'commissionPercent',
  'stock',
  'SKU',
  'tag',
  'height',
  'length',
  'width',
  'weight',
  'videoUrl',
  'videoProductThumbnailUrl',
  'videoProductUrl',
  'status',
]) {
  @ApiProperty({
    description: 'Danh sách giá trị thuộc tính của danh mục',
    type: [CreateProductCategoryAttributeValueDto],
    required: false,
  })
  productAttributes: CreateProductCategoryAttributeValueDto[];

  @ApiProperty({
    description: 'Danh sách biến thể',
    required: false,
    type: [CreateProductVariantDto],
  })
  variants?: CreateProductVariantDto[];
}

import {
  Body,
  ForbiddenException,
  Get,
  HttpCode,
  OnModuleInit,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import {
  ApiBody,
  ApiOkResponse,
  ApiOperation,
  ApiProperty,
} from '@nestjs/swagger';
import { ApiImplicitQuery } from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';
import {
  CrudController,
  CrudRequest,
  GetManyDefaultResponse,
  Override,
  ParsedRequest,
} from '@nestjsx/crud';
import { Auth } from 'decorators/auth.decorator';
import { Crud } from 'decorators/crud.decorator';
import { User } from 'decorators/user.decorator';
import { ConfigEntity, ConfigType } from 'entities/config.entity';
import { ProductEntity, ProductStatus } from 'entities/product.entity';
import { UserEntity } from 'entities/user.entity';
import { VoucherEntity } from 'entities/voucher.entity';
import { sortBy, groupBy } from 'lodash';
import { SearchCriteria, SearchResult } from 'modules/base/base-search';
import { In, IsNull, LessThan, LessThanOrEqual, MoreThan, Raw } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import {
  ProductFilterDto,
  ProductQueryFilterObj,
  TopProductSearch,
} from './dto/product-filter.dto';
import { ProductService } from './product.service';
import { ActiveStatus } from 'enums';
import { RoleType } from '../../entities/role.entity';
import { GroupBuyingService } from 'modules/group-buying/group-buying.service';
import { OptionalAuth } from 'decorators/optional-auth.decorator';
import { OrderService } from 'modules/order/order.service';
import { ModuleRef } from '@nestjs/core';
import { GroupBuyingCampaignEntity } from 'entities/group-buying-campaign.entity';

export enum FilterType {
  TOP_SEARCH = 'top_search',
  FOR_YOU = 'for_you',
  GROUP_BUYING = 'group_buying',
}

export class DetailProudct extends ProductEntity {
  @ApiProperty({
    description: 'Danh sách mã giảm giá',
    type: [VoucherEntity],
  })
  vouchers: VoucherEntity[];
  @ApiProperty({
    description: 'Cấu hình thưởng hoa hồng doanh thu',
    type: ConfigEntity,
  })
  configCommission: ConfigEntity;

  @ApiProperty({
    description: 'Hoa hông nhỏ nhất',
  })
  minCommission: number;

  @ApiProperty({
    description: 'Hoa hồng lớn nhất',
  })
  maxCommission: number;

  @ApiProperty({
    description: 'Chương trình mua chung',
  })
  campaign?: GroupBuyingCampaignEntity;
}

@Crud({
  controller: 'products',
  name: 'Sản phẩm',
  model: {
    type: ProductEntity,
  },
  query: {
    join: {
      variants: {},
      models: {},
      merchant: {},
      productCategory: {},
      'productCategory.parent': {
        alias: 'productCategory_parent',
      },
      'productCategory.productCategoryAttributes': {
        alias: 'productCategoryAttributes',
      },
      'productCategory.productCategoryAttributes.productCategoryAttributeValues':
        {
          alias: 'productCategoryAttributeValues',
        },
      productAttributes: {},
      stocks: {},
      'stocks.merchantAddress': {
        alias: 'stocksMerchantAddress',
      },
      'stocks.merchantAddress.ward': {
        alias: 'stocksMerchantAddressWard',
      },
      'stocks.merchantAddress.ward.district': {
        alias: 'stocksMerchantAddressWardDistrict',
      },
      'stocks.merchantAddress.ward.district.province': {
        alias: 'stocksMerchantAddressWardDistrictProvince',
      },
      groupBuyingProducts: {},
      'groupBuyingProducts.groupBuyingCampaign': {},
    },
  },
  routes: {
    include: ['deleteOneBase'],
  },
})
export class ProductController
  implements CrudController<ProductEntity>, OnModuleInit
{
  private orderService: OrderService;
  constructor(
    public readonly service: ProductService,
    private readonly groupBuyingService: GroupBuyingService,
    private moduleRef: ModuleRef,
  ) {}
  onModuleInit() {
    this.orderService = this.moduleRef.get(OrderService, { strict: false });
  }

  get base(): CrudController<ProductEntity> {
    return this;
  }

  @Override()
  @ApiImplicitQuery({
    name: 'type',
    enum: FilterType,
    required: false,
  })
  @ApiImplicitQuery({
    name: 'provinceId',
    type: Number,
    required: false,
  })
  @ApiImplicitQuery({
    name: 'attributeFilters',
    type: [String],
    required: false,
    isArray: true,
  })
  @ApiImplicitQuery({
    description: 'Id người dùng truy cập',
    name: 'userId',
    type: Number,
    required: false,
  })
  @HttpCode(200)
  async getMany(
    @ParsedRequest() req: CrudRequest,
    @Query('type') type?: FilterType,
    @Query('provinceId') provinceId?: number,
    @Query('attributeFilters') attributeFilters?: string,
    @Query('userId') userId?: number,
  ): Promise<GetManyDefaultResponse<ProductEntity>> {
    if (provinceId && Number(provinceId) > 0) {
      req.parsed.join.push({
        field: 'stocks',
      });
      req.parsed.join.push({
        field: 'stocks.merchantAddress',
      });
      req.parsed.join.push({
        field: 'stocks.merchantAddress.ward',
      });
      req.parsed.join.push({
        field: 'stocks.merchantAddress.ward.district',
      });
      req.parsed.join.push({
        field: 'stocks.merchantAddress.ward.district.province',
      });
      req.parsed.search.$and.push({
        'stocksMerchantAddressWardDistrictProvince.id': {
          $eq: provinceId,
        },
      });
    }
    // Kiểm tra sản phẩm hiển thị trên App + Web khách hàng
    if (userId) {
      const user = await this.service.userService.findOne({
        where: { id: userId },
        relations: ['role'],
      });
      if (user?.role?.type === RoleType.PARTNER)
        req.parsed.search.$and.push({
          status: {
            $ne: ProductStatus.DELETED,
          },
        });
    } else {
      req.parsed.search.$and.push({
        status: {
          $ne: ProductStatus.DELETED,
        },
      });
    }
    if (attributeFilters) {
      const objectFilters = JSON.parse(
        attributeFilters,
      ) as unknown as ProductQueryFilterObj[];
      const productIds: number[] = [];
      await Promise.all(
        objectFilters.map(async (attributeFilter) => {
          const productAttributes =
            await this.service.productAttributeRepo.find({
              select: ['productId'],
              where: {
                productCategoryAttributeValueIds: Raw((alias) => {
                  const queries = attributeFilter.attributeValueIds.map(
                    (attributeValueId) => {
                      return `${Number(
                        attributeValueId || 0,
                      )} = ANY("productCategoryAttributeValueIds")`;
                    },
                  );
                  return '(' + queries.join(' OR ') + ')';
                }),
                ...(attributeFilter?.attributeUnitIndexs && {
                  productCategoryAttributeUnitIndex: In(
                    attributeFilter.attributeUnitIndexs,
                  ),
                }),
              },
            });
          productIds.push(...productAttributes?.map((att) => att.productId));
        }),
      );
      if (productIds.length) {
        req.parsed.search.$and.push({
          id: {
            $in: productIds,
          },
        });
      } else {
        return {
          data: [],
          page: 0,
          pageCount: 0,
          total: 0,
          count: 0,
        };
      }
    }
    req.parsed.sort.push({
      field: 'updatedAt',
      order: 'ASC',
    });
    const existVariantsJoin = req.parsed.join.find(
      (join) => join.field === 'variants',
    );
    if (!existVariantsJoin)
      req.parsed.join.push({
        field: 'variants',
      });

    switch (type) {
      case FilterType.FOR_YOU:
      case FilterType.TOP_SEARCH:
        break;
      case FilterType.GROUP_BUYING:
        req.parsed.join.push(
          {
            field: 'groupBuyingProducts',
          },
          {
            field: 'groupBuyingProducts.groupBuyingCampaign',
          },
        );
        const now = new Date().toISOString();
        req.parsed.search.$and.push({
          'groupBuyingProducts.groupBuyingCampaign.startTime': {
            $lt: now,
          },
          'groupBuyingProducts.groupBuyingCampaign.endTime': {
            $gt: now,
          },
        });
        break;
      default:
        break;
    }

    const response = (await this.base.getManyBase(
      req,
    )) as unknown as GetManyDefaultResponse<ProductEntity>;
    if (response?.data.length > 0) {
      const currentFlashsale = await this.service.flashSaleService.repo.findOne(
        {
          where: {
            startTime: LessThanOrEqual(new Date()),
            endTime: MoreThan(new Date()),
          },
        },
      );
      if (currentFlashsale) {
        const flashsaleDetails = await this.service.flashSaleDetailRepo.find({
          flashSaleId: currentFlashsale.id,
        });
        response.data = response?.data.map((product) => {
          if (product?.variants?.length > 0) {
            let price = product.price;
            product.variants = product.variants.map((variant) => {
              const flashsaleDetail = flashsaleDetails.find(
                (flashsaleDetail) =>
                  flashsaleDetail.productVariantId === variant.id,
              );
              if (flashsaleDetail) {
                if (flashsaleDetail.discountValue < price) {
                  price = flashsaleDetail.discountValue;
                }
                product.rawPrice = variant.price;
                variant.price = flashsaleDetail.discountValue;
                variant.flashSale = currentFlashsale;
              }
              return variant;
            });
            product.price = price;
          } else {
            const flashsaleDetail = flashsaleDetails.find(
              (flashsaleDetail) => flashsaleDetail.productId === product.id,
            );
            if (flashsaleDetail) {
              product.rawPrice = product.price;
              product.price = flashsaleDetail.discountValue;
              product.flashSale = currentFlashsale;
            }
          }
          return product;
        });
      }

      // append groupBuying price
      if (type === FilterType.GROUP_BUYING) {
        const productIds = response?.data?.map(({ id }) => id);
        const campaigns =
          await this.groupBuyingService.findAllActiveCampaignOfProduct(
            productIds,
            {
              relations: [
                'groupBuyingProducts',
                'groupBuyingProducts.groupBuyingVariants',
              ],
            },
          );
        response.data = response?.data.map((product) => {
          campaigns?.forEach((campaign) => {
            campaign?.groupBuyingProducts?.forEach((groupBuyingProduct) => {
              product.groupBuyingPrice =
                this.groupBuyingService.findMinPriceOfGroupBuyingProduct(
                  groupBuyingProduct,
                );
              product.groupBuyingCommission = Math.round(
                (product?.commissionPercent * product.groupBuyingPrice) / 100 ||
                  0,
              );
              product.price = product.groupBuyingPrice;
              product.commission = product.groupBuyingCommission;
            });
          });
          return product;
        });
      }
    }
    return response;
  }

  @OptionalAuth()
  @Override()
  @ApiOkResponse({
    type: DetailProudct,
  })
  async getOne(@ParsedRequest() req: CrudRequest, @User() user: UserEntity) {
    const product = await this.base.getOneBase(req);

    const campaign = await this.groupBuyingService.findActiveCampaignOfProduct(
      product.id,
      {
        relations: [
          'groupBuyingProducts',
          'groupBuyingProducts.groupBuyingVariants',
        ],
      },
    );
    if (campaign) {
      const groupBuyingProduct = campaign.groupBuyingProducts[0];
      product.groupBuyingPrice =
        this.groupBuyingService.findMinPriceOfGroupBuyingProduct(
          groupBuyingProduct,
        );
      product.groupBuyingCommission = Math.round(
        (product?.commissionPercent * product.groupBuyingPrice) / 100 || 0,
      );
      product.groupBuyingStock =
        this.groupBuyingService.sumGroupBuyingStock(groupBuyingProduct);

      const [
        userJoinedGBProduct, // số lượng user đã tham gia vào chương trình mua chung sp
        groupJoined, // số nhóm mà user đã tham gia trong chương trình mua chung sp
        quantityBookedGB, // số lượng sản phẩm đã đặt trong chương trình mua chung sp
      ] = await Promise.all([
        this.groupBuyingService.countUserJoinedGBProduct(groupBuyingProduct.id),
        user
          ? this.orderService.countValidOrderOfGroupBuyingProduct(
              groupBuyingProduct.id,
              user.id,
            )
          : 0,
        user
          ? this.orderService.sumQuantityBookedOfGroupBuyingProduct(
              groupBuyingProduct.id,
              user.id,
            )
          : 0,
      ]);

      product.userJoinedGBProduct = userJoinedGBProduct;
      product.groupJoined = groupJoined;
      product.quantityBookedGB = quantityBookedGB;
      product.remainingQuantityGB =
        groupBuyingProduct.maxQuantityPerUser - quantityBookedGB;
    }

    const currentFlashsale = await this.service.flashSaleService.repo.findOne({
      where: {
        startTime: LessThanOrEqual(new Date()),
        endTime: MoreThan(new Date()),
      },
    });
    if (product?.variants?.length > 0) {
      const merchantAddresses = await this.service.merchantAddressRepo.find({
        where: {
          merchantId: product.merchantId,
          status: ActiveStatus.ACTIVE,
        },
        relations: ['ward', 'ward.district', 'ward.district.province'],
        order: {
          name: 'DESC',
        },
      });
      const productVariantIds = [];
      await Promise.all(
        product?.variants?.map(async (variant, variantIndex) => {
          const variantCommission =
            (product?.commissionPercent * variant?.price) / 100 || 0;
          product.variants[variantIndex].commission =
            Math.round(variantCommission);

          if (campaign) {
            const groupBuyingVariants =
              campaign.groupBuyingProducts[0].groupBuyingVariants;
            const groupBuyingVariant = groupBuyingVariants.find(
              (v) => v.productVariantId === variant.id,
            );
            if (groupBuyingVariant) {
              variant.groupBuyingPrice = groupBuyingVariant.groupPrice;
              variant.groupBuyingStock =
                groupBuyingVariant.maxQuantity -
                groupBuyingVariant.usedQuantity;
              variant.groupBuyingCommission = Math.round(
                (product?.commissionPercent * groupBuyingVariant.groupPrice) /
                  100 || 0,
              );
            }
          }

          if (variant?.stock) {
            const tmpStocks = [];
            const variantStockGrouped = groupBy(
              variant?.stock,
              'merchantAddressId',
            );
            await Promise.all(
              Object.keys(variantStockGrouped)?.map(
                async (merchantAddressId) => {
                  const merchantAddress = merchantAddresses.find(
                    (tmpMerchantAddress) =>
                      tmpMerchantAddress.id === Number(merchantAddressId),
                  );
                  if (merchantAddress)
                    tmpStocks.push({
                      ...variantStockGrouped[merchantAddressId][0],
                      merchantAddress,
                    });
                },
              ),
            ).then(() => {
              product.variants[variantIndex].stock = tmpStocks;
              productVariantIds.push(variant.id);
            });
          }
        }),
      ).then(async () => {
        if (currentFlashsale) {
          let price = product.price;
          const flashsaleDetails = await this.service.flashSaleDetailRepo.find({
            flashSaleId: currentFlashsale.id,
            productVariantId: In(productVariantIds),
          });
          product.variants = product.variants.map((variant) => {
            const flashsaleDetailVariant = flashsaleDetails.find(
              (flashsaleDetail) =>
                flashsaleDetail.productVariantId === variant.id,
            );
            if (flashsaleDetailVariant) {
              if (flashsaleDetailVariant.discountValue < price)
                price = flashsaleDetailVariant.discountValue;
              variant.price = flashsaleDetailVariant.discountValue;
              product.flashSale = currentFlashsale;
            }
            return variant;
          });
          product.price = price;
          product.flashSale = currentFlashsale;
        }
      });
    } else {
      if (currentFlashsale) {
        const flashsaleDetail = await this.service.flashSaleDetailRepo.findOne({
          flashSaleId: currentFlashsale.id,
          productId: product.id,
        });
        if (flashsaleDetail) {
          product.price = flashsaleDetail.discountValue;
          product.flashSale = currentFlashsale;
        }
      }
    }
    let productAttributesValuesIds = [];
    if (product?.productAttributes?.length > 0) {
      product?.productAttributes.map((productAttribute) => {
        if (productAttribute.productCategoryAttributeValueIds?.length > 0) {
          productAttributesValuesIds = [
            ...productAttributesValuesIds,
            ...productAttribute.productCategoryAttributeValueIds,
          ];
        }
      });
      const productAttributeValues =
        await this.service.productCategoryService.categoryAttributeValueRepo.find(
          {
            where: {
              id: In(productAttributesValuesIds),
            },
            relations: ['productCategoryAttribute'],
          },
        );

      product.productAttributes = product?.productAttributes.map(
        (productAttribute) => {
          const productAttributeValue = productAttributeValues.find(
            (productAttributeValue) =>
              productAttributeValue.id ===
              productAttribute.productCategoryAttributeValueIds[0],
          );
          return {
            ...productAttribute,
            productCategoryAttribute:
              productAttributeValue?.productCategoryAttribute,
            productCategoryAttributeValues:
              productAttribute.productCategoryAttributeValueIds.map(
                (productCategoryAttributeValueId) => {
                  return productAttributeValues.find(
                    (productAttributeValue) =>
                      productAttributeValue.id ===
                      productCategoryAttributeValueId,
                  );
                },
              ),
          };
        },
      );
    }
    const vouchers = await this.service.voucherRepo.find({
      where: [
        {
          merchantId: product.merchantId,
          type: 'shop',
          quantity: Raw(() => {
            return `"quantity" > "quantityUsed"`;
          }),
          startDate: LessThan(new Date()),
          endDate: MoreThan(new Date()),
        },
        {
          merchantId: product.merchantId,
          type: 'product',
          quantity: Raw(() => {
            return `"quantity" > "quantityUsed"`;
          }),
          productIds: Raw((columnAlias) => {
            return `${product.id} = ANY(${columnAlias})`;
          }),
          startDate: LessThan(new Date()),
          endDate: MoreThan(new Date()),
        },
        // {
        //   merchantId: IsNull(),
        //   type: 'shop',
        //   quantity: Raw(() => {
        //     return `"quantity" > "quantityUsed"`;
        //   }),
        //   startDate: LessThan(new Date()),
        //   endDate: MoreThan(new Date()),
        // },
        // {
        //   merchantId: IsNull(),
        //   type: 'product',
        //   quantity: Raw(() => {
        //     return `"quantity" > "quantityUsed"`;
        //   }),
        //   productIds: Raw((columnAlias) => {
        //     return `${product.id} = ANY(${columnAlias})`;
        //   }),
        //   startDate: LessThan(new Date()),
        //   endDate: MoreThan(new Date()),
        // },
      ],
    });
    const configCommission = await this.service.configService.findOne({
      where: {
        key: ConfigType.REVENUE,
        merchantId: product.merchantId,
      },
    });

    return {
      ...product,
      vouchers: [...vouchers],
      configCommission: {
        ...configCommission,
        value: sortBy(configCommission?.value, 'min'),
      },
      campaign,
      minCommission:
        product.groupBuyingCommission &&
        product.groupBuyingCommission < product.commission
          ? product.groupBuyingCommission
          : product.commission,
      maxCommission: Math.round(
        (product?.commissionPercent * product.maxPrice) / 100 || 0,
      ),
    };
  }

  @Get('/recommend')
  @ApiOperation({
    summary:
      'Lấy danh sách sản phẩm theo top tìm kiếm/Dành riêng cho bạn (Top tìm kiếm: keywords = [])',
  })
  @ApiOkResponse({ type: [ProductEntity] })
  @HttpCode(200)
  recommend(@Query() query: TopProductSearch): Promise<ProductEntity[]> {
    return this.service.getRecommend(query);
  }

  @Get('/orther')
  @ApiOperation({
    summary: 'Lấy danh sách sản phẩm cùng danh mục/cùng merchant',
  })
  @ApiOkResponse({ type: SearchResult })
  @HttpCode(200)
  getOrther(@Query() dto: ProductFilterDto): Promise<SearchResult> {
    return this.service.getOrther(dto);
  }

  @Override()
  @Auth()
  @ApiOperation({ summary: 'Thêm sản phẩm và biến thể' })
  @ApiBody({ type: CreateProductDto })
  @ApiOkResponse({
    type: ProductEntity,
  })
  @HttpCode(200)
  createOne(@Body() dto: CreateProductDto, @User() user: UserEntity) {
    // @ts-ignore
    delete dto?.configCommission;
    // @ts-ignore
    delete dto?.vouchers;

    if (!user.merchantId) {
      throw new ForbiddenException(
        'Người dùng không thuộc 1 Nhà phân phối nào nên không thể đăng sản phẩm!',
      );
    }
    return this.service.create(dto, user);
  }

  @Override()
  @Auth()
  @ApiOperation({ summary: 'Sửa sản phẩm và biến thể' })
  @ApiBody({ type: CreateProductDto })
  @HttpCode(200)
  @ApiOkResponse({
    type: ProductEntity,
  })
  updateOne(
    @Param('id') id: number,
    @Body() dto: CreateProductDto,
    @User() user: UserEntity,
  ) {
    return this.service.update(id, dto, user);
  }

  @Override()
  @Auth()
  @HttpCode(200)
  @ApiOperation({
    summary: 'Xóa sản phẩm',
  })
  deleteOne(@Param('id') id: number, @User() user: UserEntity) {
    if (!user.merchantId) {
      throw new ForbiddenException(
        'Người dùng không thuộc 1 Nhà phân phối nào nên không thể xóa sản phẩm!',
      );
    }
    return this.service.delete(id);
  }

  @Post('/searchProduct')
  @ApiOperation({
    summary:
      'Tìm kiếm danh sách sản phẩm (Tên, tag, khu vực, Danh mục): bán chạy, giá thấp nhất, giá cao nhất, hoa hồng',
  })
  @ApiOkResponse({
    type: SearchResult,
  })
  @HttpCode(200)
  async searchProduct(@Body() dto: ProductFilterDto): Promise<SearchResult> {
    return await this.service.searchProduct(dto);
  }
}

import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { ProductVariantEntity } from 'entities/product-variant.entity';
import { ProductEntity } from 'entities/product.entity';
import { SearchKeywordEntity } from 'entities/search-keyword.entity';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';
import { ProductAttributeEntity } from 'entities/product-attribute.entity';
import { ProductCategoryModule } from '../product-category/product-category.module';
import { ProductStockEntity } from 'entities/product-stock.entity';
import { VoucherEntity } from 'entities/voucher.entity';
import { ConfigModule } from '../config/config.module';
import { FlashSaleDetailEntity } from 'entities/flash-sale-detail.entity';
import { FlashSaleModule } from 'modules/flash-sale/flash-sale.module';
import { UserModule } from '../user/user.module';
import { GroupBuyingModule } from 'modules/group-buying/group-buying.module';
import { GroupBuyingProductEntity } from 'entities/group-buying-product.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ProductEntity,
      ProductVariantEntity,
      MerchantAddressEntity,
      SearchKeywordEntity,
      ProductAttributeEntity,
      ProductStockEntity,
      VoucherEntity,
      FlashSaleDetailEntity,
    ]),
    ProductCategoryModule,
    forwardRef(() => FlashSaleModule),
    ConfigModule,
    UserModule,
    GroupBuyingModule,
  ],
  controllers: [ProductController],
  providers: [ProductService],
  exports: [ProductService],
})
export class ProductModule {}

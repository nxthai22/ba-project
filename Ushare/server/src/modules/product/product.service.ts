import {
  BadRequestException,
  ForbiddenException,
  forwardRef,
  HttpStatus,
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { FlashSaleDetailEntity } from 'entities/flash-sale-detail.entity';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { OrderEntity } from 'entities/order.entity';
import { ProductAttributeEntity } from 'entities/product-attribute.entity';
import { ProductCategoryAttributeValueEntity } from 'entities/product-category-attribute-value.entity';
import { ProductStockEntity } from 'entities/product-stock.entity';
import { ProductVariantEntity } from 'entities/product-variant.entity';
import {
  ProductEntity,
  ProductGroupType,
  StockValue,
} from 'entities/product.entity';
import { SearchKeywordEntity } from 'entities/search-keyword.entity';
import { UserEntity } from 'entities/user.entity';
import { VoucherEntity } from 'entities/voucher.entity';
import { ErrorMessage, OrderStatus } from 'enums';
import { sortBy } from 'lodash';
import {
  PagingHelper,
  SearchCriteria,
  SearchResult,
} from 'modules/base/base-search';
import { PatienceDiffPlus } from 'modules/base/patienceDiff';
import {
  Connection,
  EntityManager,
  ILike,
  In,
  ObjectLiteral,
  Repository,
} from 'typeorm';
import { slugify } from 'utils';
import { ConfigService } from '../config/config.service';
import { ProductCategoryService } from '../product-category/product-category.service';
import {
  CreateProductCategoryAttributeValueDto,
  CreateProductDto,
  CreateProductVariantDto,
} from './dto/create-product.dto';
import {
  ProductFilterDto,
  SortType,
  TopProductSearch,
} from './dto/product-filter.dto';
import { FlashSaleService } from 'modules/flash-sale/flash-sale.service';
import { ControlType } from '../../entities/product-category-attribute.entity';
import { UserService } from '../user/user.service';
import { GroupBuyingService } from 'modules/group-buying/group-buying.service';

export class ProductSearchResult {
  @ApiProperty({
    description: 'Id sản phẩm',
  })
  id: number;

  @ApiProperty({
    description: 'Tên sản phẩm',
  })
  name: string;

  @ApiProperty({
    description: 'Giá thấp nhất',
  })
  price: number;

  @ApiProperty({
    description: 'Giá cao nhất',
  })
  maxPrice?: number;

  @ApiProperty({
    description: 'Hoa hồng',
  })
  commission?: number;

  @ApiProperty({
    description: 'Tổng số sản phẩm',
  })
  totalCount?: number;

  @ApiProperty({
    description: 'Danh sách thuộc tính',
  })
  attributes?: ProductGroupType[];

  @ApiProperty({
    description: 'Danh sách biến thể',
    default: [],
  })
  variants?: ProductVariantEntity[];
}

@Injectable()
export class ProductService extends TypeOrmCrudService<ProductEntity> {
  constructor(
    @InjectRepository(ProductEntity)
    public repo: Repository<ProductEntity>,
    @InjectRepository(ProductVariantEntity)
    public productVariantRepo: Repository<ProductVariantEntity>,
    @InjectRepository(MerchantAddressEntity)
    public merchantAddressRepo: Repository<MerchantAddressEntity>,
    @InjectRepository(ProductAttributeEntity)
    public productAttributeRepo: Repository<ProductAttributeEntity>,
    @InjectRepository(SearchKeywordEntity)
    public searchKeywordRepo: Repository<SearchKeywordEntity>,
    @InjectConnection() private connection: Connection,
    public productCategoryService: ProductCategoryService,
    @InjectRepository(ProductStockEntity)
    public productStockEntityRepo: Repository<ProductStockEntity>,
    @InjectRepository(ProductEntity)
    public productEntityRepo: Repository<ProductEntity>,
    @InjectRepository(VoucherEntity)
    public voucherRepo: Repository<VoucherEntity>,
    @InjectRepository(FlashSaleDetailEntity)
    public flashSaleDetailRepo: Repository<FlashSaleDetailEntity>,
    @Inject(forwardRef(() => FlashSaleService))
    public flashSaleService: FlashSaleService,
    public configService: ConfigService,
    public userService: UserService,
    public groupBuyingService: GroupBuyingService,
  ) {
    super(repo);
  }

  /** Tìm kiếm danh sách sản phẩm (Tên, tag, khu vực, Danh mục): bán chạy, giá thấp nhất, giá cao nhất, hoa hồng */
  async searchProduct(dto: ProductFilterDto): Promise<SearchResult> {
    // Thêm cụm từ tìm kiếm
    this.addSearchKeyword(dto.textSearch);
    // Thực hiện tìm kiếm
    const query = this.repo.createQueryBuilder('product');
    if (dto?.textSearch?.length > 0) {
      query.where(
        '(LOWER("product"."name") LIKE :textSearch OR LOWER("product"."tag") LIKE :textSearch)',
        {
          textSearch: '%' + dto.textSearch?.toLowerCase() + '%',
        },
      );
    }
    if (dto?.categoryId) {
      query.andWhere('"product"."productCategoryId" = :categoryId', {
        categoryId: dto.categoryId,
      });
    }
    if (dto?.provinceId) {
      const merchantAddressIds = (
        await this.merchantAddressRepo.find({
          where: {
            ward: {
              district: {
                provinceId: dto.provinceId,
              },
            },
          },
          relations: ['ward', 'ward.district'],
        })
      )?.map((m) => m.id);
      if (merchantAddressIds?.length > 0) {
        query.andWhere(
          '"product"."stock"::jsonb->"merchantAddressId" ?| array[:...merchantAddressIds]',
          {
            merchantAddressIds: merchantAddressIds,
          },
        );
      }
    }
    switch (dto?.sortType) {
      case SortType.TOP_SELL:
        query
          .leftJoin(
            'order_product',
            'orderProduct',
            '"orderProduct"."productId" = "product"."id"',
          )
          .leftJoin('order', 'order', '"order"."id" = "orderProduct"."orderId"')
          .andWhere('"order"."status" = :status', {
            status: OrderStatus.COMPLETED,
          })
          .orderBy('"totalCount"', 'DESC');
        break;
      case SortType.MAX_PRICE:
        query.orderBy('"product"."price"', 'DESC');
        break;
      case SortType.MIN_PRICE:
        query.orderBy('"product"."price"', 'ASC');
        break;
      case SortType.TOP_COMMISSION:
        query.orderBy('"product"."commission"', 'DESC');
        break;
    }
    query
      .groupBy('"product"."id"')
      .addGroupBy('"product"."name"')
      .addGroupBy('"product"."price"')
      .addGroupBy('"product"."maxPrice"')
      .addGroupBy('"product"."commission"')
      .select('"product"."id"')
      .addSelect('"product"."name"')
      .addSelect('"product"."price"')
      .addSelect('"product"."maxPrice"')
      .addSelect('"product"."commission"')
      .addSelect('COUNT("product"."id") as "totalCount"');
    const result = ((await query.execute()) as ProductSearchResult[]) || [];
    return PagingHelper.getPages(result, dto);
  }

  /** Thêm cụm từ tìm kiểm vào bảng search_keyword (nếu tỉ lệ giống > 80% thì update số lượt tìm kiếm, nếu < 80% thì thêm mới) */
  async addSearchKeyword(textSearch: string) {
    const keywordSearchs = await this.searchKeywordRepo
      .createQueryBuilder()
      .where('LENGTH("keyword") BETWEEN :min AND :max', {
        min: textSearch?.length - 5 ? 0 : textSearch?.length - 5,
        max: textSearch?.length + 5,
      })
      .getMany();
    const keywordSearch = keywordSearchs?.reduce(function (pre, curr) {
      if (pre && curr) {
        const preMatch = PatienceDiffPlus.calculateDiffPercent(
          pre?.keyword,
          textSearch,
          ' ',
        );
        const currMatch = PatienceDiffPlus.calculateDiffPercent(
          curr?.keyword,
          textSearch,
          ' ',
        );
        if (preMatch > currMatch) {
          return pre;
        } else {
          return curr;
        }
      } else {
        return null;
      }
    });
    if (
      keywordSearch &&
      PatienceDiffPlus.calculateDiffPercent(
        keywordSearch.keyword,
        textSearch,
        ' ',
      ) > 0.6
    ) {
      keywordSearch.amount += 1;
      await this.searchKeywordRepo.save(keywordSearch);
    } else {
      await this.searchKeywordRepo.save({
        keyword: textSearch,
        amount: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    }
  }

  /** Lấy danh sách sản phẩm và biến thể theo đối tác*/
  async getVariants(dto: ProductFilterDto): Promise<ProductSearchResult[]> {
    const products = await this.repo.find({
      where: {
        merchantId: dto?.merchantId,
      },
      relations: ['variants'],
    });

    if (products.length) {
      return products.map((product) => {
        const prodSearch = new ProductSearchResult();
        prodSearch.id = product.id;
        prodSearch.name = product.name;
        prodSearch.price = product.price;
        prodSearch.maxPrice = product.maxPrice;
        prodSearch.commission = product.commission;
        prodSearch.totalCount = product.variants?.length;
        prodSearch.variants = product.variants;
        return prodSearch;
      });
    }

    return [];
  }

  /** Lấy danh sách sản phẩm cùng danh mục/cùng merchant */
  async getOrther(dto: ProductFilterDto): Promise<SearchResult> {
    const where: ObjectLiteral = Object.assign(
      {},
      dto.categoryId && { productCategoryId: dto.categoryId },
      dto.merchantId && { merchantId: dto.merchantId },
    );

    const products = await this.repo.find({
      where,
      take: dto.limit,
      skip: (dto.page - 1) * dto.limit,
    });

    return {
      page: dto.page,
      limit: dto.limit,
      totalPage: Math.ceil(products.length / dto.limit),
      totalCount: products.length,
      data: products,
    };
  }

  /** Dành riêng cho bạn */
  async getRecommend(dto: TopProductSearch): Promise<ProductEntity[]> {
    let keywords = dto?.keywords?.length ? [...dto.keywords] : [];
    const take = dto.limit;
    const skip = (dto.page - 1) * take;

    if (!dto?.keywords?.length) {
      const topSearchKeywords = await this.searchKeywordRepo.find({
        order: {
          amount: 'DESC',
        },
        take,
        skip,
      });

      keywords = [...topSearchKeywords.map(({ keyword }) => keyword)];
    }

    const buildQuery = keywords.map((keyword) => ({
      name: ILike(`%${keyword}%`),
    }));

    if (!keywords.length) return [];

    return this.repo.find({ where: buildQuery, take, skip });
  }

  /** Lấy thông tin chi tiết sản phẩm */
  getDetail(prodId: number): Promise<ProductEntity> {
    return this.repo.findOne({
      where: {
        id: prodId,
      },
      relations: [
        'variants',
        'productCategory',
        'productAttributes',
        'productAttributes.productCategoryAttributeValue',
      ],
    });
  }

  /** Thêm sản phẩm và biến thể */
  async create(
    dto: CreateProductDto,
    user: UserEntity,
  ): Promise<ProductEntity> {
    const existProduct = await this.repo.findOne({
      where: {
        name: dto.name,
        merchantId: user.merchantId,
      },
    });

    if (existProduct) {
      throw new BadRequestException('Tên sản phẩm đã tồn tại');
    }

    // Use database transaction
    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const newProduct = {
        ...new ProductEntity(),
        ...this.initProductValue(dto),
        slug: slugify(dto.name),
        userId: user.id,
      };

      if (user.merchantId) {
        newProduct.merchantId = user.merchantId;
      }

      const product = await queryRunner.manager.save(ProductEntity, newProduct);

      // Cập nhật thông tin kho hàng
      const productStocks: ProductStockEntity[] = [];
      newProduct.stock?.map((st) => {
        productStocks.push({
          productId: newProduct.id,
          merchantAddressId: st.merchantAddressId,
          quantity: st.quantity || 0,
          product: null,
          merchantAddress: null,
        });
      });
      await queryRunner.manager.insert(ProductStockEntity, productStocks);

      if (dto.variants?.length > 0) {
        const variants = dto.variants.map((variant) => ({
          ...variant,
          productId: product.id,
        }));

        // Thêm biến thể mới
        await queryRunner.manager.insert(ProductVariantEntity, variants);
      }
      // Thêm thuộc tính danh mục sản phẩm
      if (dto.productAttributes?.length > 0) {
        await this.updateProductAttributes(
          product.id,
          user,
          dto.productAttributes,
          queryRunner.manager,
        );
      }
      await queryRunner.commitTransaction();

      return this.repo.findOne({ id: product.id });
    } catch (err) {
      await queryRunner.rollbackTransaction();

      switch (err.status) {
        case HttpStatus.BAD_REQUEST:
          throw new BadRequestException(err.message);
        case HttpStatus.NOT_FOUND:
          throw new NotFoundException(err.message);
        default:
          throw new InternalServerErrorException(err.message);
      }
    } finally {
      await queryRunner.release();
    }
  }

  /** Cập nhật sản phẩm */
  async update(
    id: number,
    dto: CreateProductDto,
    user: UserEntity,
  ): Promise<ProductEntity> {
    const product = await this.repo.findOne({ id });
    if (!product) {
      throw new BadRequestException('Không tìm thấy sản phẩm');
    }

    if (user.roleId !== 1 && user.merchantId != product.merchantId) {
      throw new ForbiddenException(
        'Không thể sửa sản phẩm không thuộc Nhà cung cấp',
      );
    }
    //TODO: Kiểm tra merchantId của sản phẩm phải bằng merchantId của user nếu user muốn sửa là quyền merchant

    // Use database transaction
    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const productAttributesDto = [...dto.productAttributes];

      delete dto.productAttributes;
      delete dto['vouchers'];
      delete dto['configCommission'];

      const updateProductParams = {
        ...this.initProductValue(dto),
        userId: user.id,
        slug: slugify(dto.name),
        id: Number(id),
      };
      await queryRunner.manager.save(ProductEntity, updateProductParams);

      const variantParams = dto.variants?.map((variant) => ({
        ...variant,
        productId: Number(id),
        commission: Math.floor(
          (Number(variant.price || 0) *
            Number(updateProductParams.commissionPercent || 0)) /
            100,
        ),
      }));
      const currentVariants = await this.productVariantRepo?.find({
        productId: id,
      });
      const deleteVariants = currentVariants?.reduce((arr, currVariant) => {
        if (!variantParams?.find((variant) => variant.id === currVariant.id)) {
          currVariant.deletedAt = new Date();
          arr.push(currVariant);
        }
        return arr;
      }, []);
      if (variantParams?.length > 0) {
        // update variants
        await queryRunner.manager.save(ProductVariantEntity, [
          ...variantParams,
          ...deleteVariants,
        ]);
        // await queryRunner.manager.save(ProductVariantEntity, deleteVariants);
      }

      if (dto.variants?.length > 0) {
        // Xoá kho
        const productStocks = await this.productStockEntityRepo.find({
          productId: id,
        });
        if (productStocks?.length > 0) {
          await queryRunner.manager.softDelete(
            ProductStockEntity,
            productStocks,
          );
        }

        const { affected } = await queryRunner.manager.update(
          ProductEntity,
          { id },
          { stock: [] },
        );

        if (!affected) {
          new InternalServerErrorException(ErrorMessage.UPDATE);
        }
      }

      await this.updateStock(
        id,
        updateProductParams.stock,
        queryRunner.manager,
      );

      // Cập nhật thuộc tính sản phẩm
      if (productAttributesDto?.length > 0) {
        await this.updateProductAttributes(
          id,
          user,
          productAttributesDto,
          queryRunner.manager,
        );
      }

      await queryRunner.commitTransaction();

      return product;
    } catch (err) {
      await queryRunner.rollbackTransaction();

      switch (err.status) {
        case HttpStatus.NOT_FOUND:
          throw new NotFoundException(err.message);
        case HttpStatus.FORBIDDEN:
          throw new ForbiddenException(err.message);
        default:
          throw new InternalServerErrorException(err.message);
      }
    } finally {
      await queryRunner.release();
    }
  }

  async delete(id: number): Promise<{ message: string }> {
    return await this.connection.transaction(async (manage) => {
      const product = await this.repo.findOne({ id });

      if (!product) {
        throw new BadRequestException('Không tìm thấy sản phẩm');
      }

      const currentVariants = await this.productVariantRepo.find({
        where: { productId: id },
      });

      await this.connection.transaction(async (manager) => {
        if (currentVariants.length) {
          await manager.softRemove(ProductVariantEntity, currentVariants);
        }

        await manager.softRemove(ProductEntity, product);
      });
      const productAttributes = await this.productAttributeRepo.find({
        productId: id,
      });
      await this.productAttributeRepo.softRemove(productAttributes);
      return Promise.resolve({ message: 'OK' });
    });
  }

  /** Cập nhật số lượng kho sau khi thay đổi trạng thái đơn hàng */
  async updateStockAfterChangeOrderStatus(
    order: OrderEntity,
    manager?: EntityManager,
  ): Promise<{ message: string }> {
    if (!order.orderProducts?.length) {
      throw new BadRequestException(
        'Không có sản phẩm nào để cập nhật số lượng kho',
      );
    }

    const productStockParams = [];
    const productParams = [];
    const productVariantParams = [];
    const flashSaleDetailParams = [];

    await Promise.all(
      order.orderProducts.map(async (orderProduct) => {
        // Cập nhật tồn kho trong bảng product-stock
        const productStock = await this.productStockEntityRepo.findOne({
          where: {
            productId: orderProduct.productId,
            merchantAddressId: order.merchantAddressId,
          },
        });

        if (productStock) {
          switch (order.status) {
            case OrderStatus.CONFIRMED:
              productStock.quantity =
                productStock.quantity - orderProduct.quantity;
              break;
            case OrderStatus.CANCELLED:
            case OrderStatus.RETURNED:
              productStock.quantity =
                productStock.quantity + orderProduct.quantity;
              break;
          }

          productStockParams.push(productStock);
        }
        // Cập nhật tồn kho cho biến thể
        const product = await this.repo.findOne({
          where: {
            id: orderProduct.productId,
          },
          relations: ['variants'],
        });

        if (product) {
          product.saleAmount =
            (product.saleAmount || 0) + orderProduct.quantity;

          product.stock?.map((stock) => {
            if (stock?.merchantAddressId === order.merchantAddressId) {
              switch (order.status) {
                case OrderStatus.CONFIRMED:
                  stock.quantity -= orderProduct.quantity;
                  break;
                case OrderStatus.CANCELLED:
                case OrderStatus.RETURNED:
                  stock.quantity += orderProduct.quantity;
                  break;
              }
            }
          });

          productParams.push(product);

          const flashSaleDetail = await this.flashSaleDetailRepo.findOne({
            id: orderProduct.flashSaleDetailId,
          });

          if (flashSaleDetail) {
            flashSaleDetailParams.push({
              ...flashSaleDetail,
              usedQuantity:
                flashSaleDetail.usedQuantity + orderProduct.quantity,
            });
          }
          const variant = product.variants?.find(
            (variant) => variant.id === orderProduct.variantId,
          );

          if (variant) {
            variant.stock?.map((st) => {
              if (st?.merchantAddressId === order.merchantAddressId) {
                switch (order.status) {
                  case OrderStatus.CONFIRMED:
                    st.quantity -= orderProduct.quantity;
                    break;
                  case OrderStatus.CANCELLED:
                  case OrderStatus.RETURNED:
                    st.quantity += orderProduct.quantity;
                    break;
                }
              }
            });

            productVariantParams.push(variant);
          }

          // Cập nhật số lượng sản phẩm đã dùng cho mua chung
          switch (order.status) {
            case OrderStatus.CONFIRMED:
              await this.groupBuyingService.updateProductUsedInCampaign(
                product.id,
                variant?.id,
                1,
                manager,
              );
              break;
            case OrderStatus.CANCELLED:
            case OrderStatus.RETURNED:
              await this.groupBuyingService.updateProductUsedInCampaign(
                product.id,
                variant?.id,
                -1,
                manager,
              );
              break;
          }
        }
      }),
    );

    if (manager) {
      await manager.save(ProductStockEntity, productStockParams);
      await manager.save(ProductEntity, productParams);
      await manager.save(ProductVariantEntity, productVariantParams);
      await manager.save(FlashSaleDetailEntity, flashSaleDetailParams);
    } else {
      await this.productStockEntityRepo.save(productStockParams);
      await this.productEntityRepo.save(productParams);
      await this.productVariantRepo.save(productVariantParams);
      await this.flashSaleDetailRepo.save(flashSaleDetailParams);
    }

    return Promise.resolve({ message: 'OK' });
  }

  protected async updateStock(
    productId: number,
    stocks: StockValue[],
    manager: EntityManager,
  ): Promise<void> {
    // Cập nhật lại tồn kho sản phẩm (product_stock)
    const productStocks = await this.productStockEntityRepo.find({
      productId,
    });

    if (productStocks && productStocks?.length > 0) {
      await manager.softDelete(ProductStockEntity, productStocks);
    }
    const productStockParams = stocks?.reduce((params, stock) => {
      const productStock = productStocks?.find(
        ({ merchantAddressId }) =>
          merchantAddressId === stock.merchantAddressId,
      );
      const productStockParam = productStock
        ? {
            ...productStock,
            deletedAt: null,
            quantity: stock.quantity || 0,
          }
        : {
            productId,
            merchantAddressId: stock.merchantAddressId,
            quantity: stock.quantity,
          };

      return [...params, productStockParam];
    }, []);
    await manager.save(ProductStockEntity, productStockParams);
  }

  protected initProductValue = (params: CreateProductDto): ProductEntity => {
    const getStock = this.getStocks(params.variants);
    return Object.keys(params)?.reduce((property: any, field) => {
      if (
        field === 'variants' ||
        field === 'price' ||
        field === 'stock' ||
        field === 'commission'
      ) {
        const variantsSortByPrice = params?.variants
          ? sortBy(params.variants, 'price')
          : [];
        const price =
          variantsSortByPrice?.[0] !== undefined
            ? variantsSortByPrice?.[0]?.price || 0
            : params?.price || 0;
        const maxPrice =
          variantsSortByPrice?.[params?.variants?.length - 1] !== undefined
            ? variantsSortByPrice?.[params?.variants?.length - 1]?.price || 0
            : params?.price || 0;
        const commission =
          Math.round((price * params.commissionPercent) / 100) || 0;
        const stock =
          !params.variants || params?.variants?.length < 1
            ? params?.stock
            : getStock;
        return { ...property, price, maxPrice, commission, stock };
      }
      return { ...property, [field]: params[field] };
    }, {});
  };

  protected getStocks(variants: CreateProductVariantDto[]): StockValue[] {
    if (!variants || variants?.length < 1) {
      return [];
    }
    const variantStocks: StockValue[] = variants?.reduce((preVar, currVar) => {
      if (preVar?.length < 1 || !preVar) {
        preVar = [];
      }
      if (currVar?.stock && Array.isArray(currVar.stock)) {
        preVar.push(...currVar.stock);
      }
      return preVar;
    }, []);
    return variantStocks?.reduce((stocks, vStock) => {
      const stock = stocks.find(
        ({ merchantAddressId }) =>
          merchantAddressId === vStock.merchantAddressId,
      );

      if (!stock)
        return [
          ...stocks,
          {
            merchantAddressId: vStock.merchantAddressId,
            quantity: vStock?.quantity || 0,
          },
        ];
      stock.quantity += vStock?.quantity || 0;

      return stocks;
    }, []);
  }

  protected async updateProductAttributes(
    productId: number,
    user: UserEntity,
    productAttributes: CreateProductCategoryAttributeValueDto[],
    manager: EntityManager,
  ): Promise<void> {
    // await manager.delete(ProductAttributeEntity, {
    //   productId,
    // });
    // const prodAttributes = await manager.find(ProductAttributeEntity, {
    //   productId,
    // });
    const updateProductAttributeEntities = await Promise.all(
      productAttributes.map(async (productAttribute) => {
        const productAttributeValue = { ...productAttribute };
        if (productAttributeValue.name) delete productAttributeValue.name;
        if (!productAttributeValue.productCategoryAttributeValueIds) {
          productAttributeValue.productCategoryAttributeValueIds =
            await this.productCategoryService.merchantAddAttributeValue(
              {
                ...(productAttribute as unknown as ProductCategoryAttributeValueEntity),
                merchantId: user.merchantId,
              },
              user,
              manager,
            );
        } else {
          if (
            typeof productAttributeValue.productCategoryAttributeValueIds ===
            'object'
          ) {
            const attrValue =
              await this.productCategoryService.categoryAttributeValueRepo.findOne(
                {
                  where: {
                    id: In(
                      productAttributeValue.productCategoryAttributeValueIds,
                    ),
                  },
                  relations: ['productCategoryAttribute'],
                },
              );
            if (!attrValue) {
              throw new NotFoundException('Không tìm thấy giá trị thuộc tính');
            }
            if (
              attrValue?.productCategoryAttribute?.controlType ===
                ControlType.TEXT &&
              attrValue.productCategoryAttribute.name !== productAttribute.name
            ) {
              await this.productCategoryService.categoryAttributeValueRepo.save(
                {
                  ...attrValue,
                  name: productAttribute.name,
                },
              );
            }
          } else {
            productAttributeValue.productCategoryAttributeValueIds = [
              productAttributeValue.productCategoryAttributeValueIds,
            ];
          }
        }
        // if (!productAttributeValue.productCategoryAttributeValueIds) {
        //   productAttributeValue.productCategoryAttributeValueIds =
        //     await this.productCategoryService.merchantAddAttributeValue(
        //       productAttributeValue as unknown as ProductCategoryAttributeValueEntity,
        //       user,
        //       manager,
        //     );
        // } else {
        //   if (
        //     typeof productAttributeValue.productCategoryAttributeValueIds ===
        //     'object'
        //   ) {
        //     console.log('1');
        //     if (
        //       productAttributeValue.productCategoryAttributeValueIds?.length > 0
        //     ) {
        //       const attrValue =
        //         await this.productCategoryService.categoryAttributeValueRepo.findOne(
        //           {
        //             id: In(
        //               productAttributeValue.productCategoryAttributeValueIds,
        //             ),
        //           },
        //         );
        //       if (!attrValue) {
        //         throw new NotFoundException(
        //           'Không tìm thấy giá trị thuộc tính',
        //         );
        //       } else {
        //         if (
        //           attrValue?.productCategoryAttribute?.controlType ===
        //             ControlType.TEXT &&
        //           attrValue.productCategoryAttribute.name !==
        //             productAttributeValue.name
        //         ) {
        //           await this.productCategoryService.categoryAttributeValueRepo.save(
        //             {
        //               ...attrValue,
        //               name: productAttribute.name,
        //             },
        //           );
        //         }
        //       }
        //     }
        //   } else {
        //     console.log('2');
        //     const attrValue =
        //       await this.productCategoryService.categoryAttributeValueRepo.findOne(
        //         { id: productAttributeValue.productCategoryAttributeValueIds },
        //       );
        //     productAttributeValue.productCategoryAttributeValueIds = [
        //       productAttributeValue.productCategoryAttributeValueIds,
        //     ];
        //     if (!attrValue) {
        //       throw new NotFoundException('Không tìm thấy giá trị thuộc tính');
        //     }
        //   }
        // }

        return {
          productId: Number(productId),
          productCategoryAttributeValueIds:
            productAttributeValue.productCategoryAttributeValueIds,
          productCategoryAttributeId:
            productAttributeValue.productCategoryAttributeId,
          productCategoryAttributeUnitIndex: null,
        };
      }),
    );
    await manager.delete(ProductAttributeEntity, { productId });
    await manager.save(ProductAttributeEntity, updateProductAttributeEntities);
  }

  async getProductIsInGroupBuying() {
    const query = this.groupBuyingService.groupBuyingProductRepo
      .createQueryBuilder('groupBuyingProduct')
      .leftJoin(
        'group_buying_campaign',
        'groupBuyingCampaign',
        '"groupBuyingCampaign"."id" = "groupBuyingProduct"."groupBuyingCampaignId"',
      )
      .where(
        '"groupBuyingCampaign"."startTime" < :now AND "groupBuyingCampaign"."endTime" > :now',
        {
          now: new Date(),
        },
      )
      .select('distinct ("groupBuyingProduct"."productId")');
    const productIdData = ((await query.execute()) as any[]) || [];
    const productIds = productIdData.map((item) => item.productId);
    return productIds;
  }
}

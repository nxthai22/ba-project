import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { FlashSaleDetailEntity } from 'entities/flash-sale-detail.entity';
import { FlashSaleEntity } from 'entities/flash-sale.entity';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { UserEntity } from 'entities/user.entity';
import { ProductService } from 'modules/product/product.service';
import { In, LessThanOrEqual, MoreThan, Repository } from 'typeorm';
import {
  FlashSaleDetailDto,
  FlashSaleFilterWithPaging,
} from './dto/flash-sale.dto';
import { FlashSaleGetNowDto } from 'modules/flash-sale/dto/flash-sale.get-now.dto';
import { GetManyDefaultResponse } from '@nestjsx/crud';
import { RoleType } from 'entities/role.entity';
import { endOfDay, startOfDay } from 'date-fns';
import { OrderProductEntity } from 'entities/order-product.entity';
import { ProductStatus } from '../../entities/product.entity';

@Injectable()
export class FlashSaleService extends TypeOrmCrudService<FlashSaleEntity> {
  constructor(
    @InjectRepository(FlashSaleEntity)
    public repo: Repository<FlashSaleEntity>,
    @InjectRepository(FlashSaleDetailEntity)
    public flashSaleDetailRepo: Repository<FlashSaleDetailEntity>,
    @InjectRepository(MerchantAddressEntity)
    public merchantAddressRepo: Repository<MerchantAddressEntity>,
    @InjectRepository(OrderProductEntity)
    public orderProductRepo: Repository<OrderProductEntity>,
    @Inject(forwardRef(() => ProductService))
    private productService: ProductService,
  ) {
    super(repo);
  }

  async getOneForEachRole(
    id: number,
    user: UserEntity,
  ): Promise<FlashSaleEntity> {
    const flashSale = await this.repo.findOne({ id });
    if (!flashSale) {
      throw new BadRequestException('Flash sale không tồn tại');
    }
    const query = this.flashSaleDetailRepo
      .createQueryBuilder('flashSaleDetail')
      .select('*')
      .where('"flashSaleId" = :flashSaleId', {
        flashSaleId: flashSale.id,
      });
    if (user?.merchantId) {
      query.andWhere('"flashSaleDetail"."merchantId" = :merchantId', {
        merchantId: user.merchantId,
      });
    }
    const data = (await query.execute()) as FlashSaleDetailEntity[] | [];
    flashSale.flashSaleDetail = data;
    return flashSale;
  }

  /** Thêm sản phẩm vào chương trình theo từng đối tác */
  async addProductToFlashSale(
    id: number,
    dto: FlashSaleDetailDto[],
    user: UserEntity,
  ): Promise<FlashSaleEntity> {
    const productIds = dto?.map(({ productId }) => productId);
    const productVariantIds = dto?.map(
      ({ productVariantId }) => productVariantId,
    );

    const uniqueProductIds = [...new Set(productIds)];
    const uniqueProductVariantIds = [...new Set(productVariantIds)];

    const variants = await this.productService.productVariantRepo.find({
      where: {
        id: In(uniqueProductVariantIds),
      },
      relations: ['product'],
    });
    const products = await this.productService.repo.find({
      where: {
        id: In(uniqueProductIds),
      },
      relations: ['stocks'],
    });

    const productMerchantIds = products.map(({ merchantId }) => merchantId);
    const productVariantMerchantIds = variants.map(
      ({ product }) => product.merchantId,
    );

    const uniqueMerchantIds = [
      ...new Set([...productMerchantIds, ...productVariantMerchantIds]),
    ];

    if (
      uniqueMerchantIds.length > 1 ||
      uniqueMerchantIds[0] !== user?.merchantId
    ) {
      throw new BadRequestException('Sản phẩm đã chọn không cùng nhà cung cấp');
    }

    const flashSale = await this.repo.findOne({ id });
    if (
      flashSale?.maxProductNumber > 0 &&
      productIds?.length > flashSale.maxProductNumber
    ) {
      throw new BadRequestException(
        'Số lượng sản phẩm tham gia vượt quá số lượng chương trình cho phép',
      );
    }
    const productsVariantImports: FlashSaleDetailEntity[] = [];
    await Promise.all(
      dto.map(async (element) => {
        const flashSaleDetail = new FlashSaleDetailEntity();
        flashSaleDetail.flashSaleId = id;
        flashSaleDetail.merchantId = user.merchantId;
        const product = products.find(
          (product) => product.id === element.productId,
        );
        if (product) {
          const maxQuantity = product?.stocks?.reduce(
            (total, stock) => total + stock.quantity,
            0,
          );
          if (!element.productVariantId) {
            flashSaleDetail.productId = element.productId;
            flashSaleDetail.productVariantId = null;
          } else {
            flashSaleDetail.productId = variants?.find(
              (x) => x.id === element.productVariantId,
            )?.product?.id;
            flashSaleDetail.productVariantId = element.productVariantId;
          }
          flashSaleDetail.discountValue = element?.discountValue || 0;
          flashSaleDetail.discountValueType = element.discountValueType;
          flashSaleDetail.quantity = element?.qty || 0;
          flashSaleDetail.usedQuantity = 0;
          if (element.qty > maxQuantity) {
            const product = await this.productService.repo.findOne({
              id: element.productId,
            });
            throw new BadRequestException(
              `Số lượng cho sản phẩm/biến thể ${product.name} vượt quá số lượng tồn kho hiện tại`,
            );
          }
          productsVariantImports.push(flashSaleDetail);
        } else {
          throw new BadRequestException(
            `Không tìm thấy Sản phẩm với ID: ${element?.productId}`,
          );
        }
      }),
    );
    await this.flashSaleDetailRepo.save(productsVariantImports);
    return await this.repo.findOne({
      where: {
        id: dto?.[0].flashSaleId,
      },
      relations: ['flashSaleDetail'],
    });
  }

  /** NCC cập nhật sản phẩm vào chương trình flash sale */
  async updateProductInFlashSaleByMerchantId(
    id: number,
    dto: FlashSaleDetailDto[],
    user: UserEntity,
  ): Promise<FlashSaleEntity> {
    if (!dto || dto?.length < 1) {
      throw new NotFoundException('Không có sản phẩm được cập nhật');
    }

    const flashSale = await this.repo.findOne({
      where: {
        id,
        startTime: MoreThan(new Date()),
      },
      relations: ['flashSaleDetail'],
    });
    if (!flashSale) {
      throw new BadRequestException(
        'Không thể thêm sản phẩm vào chương trình đã diễn ra/đã kết thúc',
      );
    }
    if (dto.length > flashSale.maxProductNumber) {
      throw new BadRequestException(
        'Vượt quá số sản phẩm tối đá được phép tham gia flash sale',
      );
    }
    flashSale.flashSaleDetail = flashSale.flashSaleDetail.reduce(
      (arr, curr) => {
        if (curr.merchantId === user.merchantId) {
          arr.push(curr);
        }
        return arr;
      },
      [],
    );
    await this.flashSaleDetailRepo.softRemove(flashSale.flashSaleDetail);
    return await this.addProductToFlashSale(id, dto, user);
  }

  /** Lấy danh sách chương trình flash sale của merchant */
  async getFlashSaleMerchant(
    req: FlashSaleFilterWithPaging,
    user: UserEntity,
  ): Promise<GetManyDefaultResponse<FlashSaleEntity>> {
    const page = Number(req.page | 1);
    const limit = Number(req.limit | 0);
    const query = this.flashSaleDetailRepo
      .createQueryBuilder('flashSaleDetail')
      .leftJoinAndSelect(
        'flash_sale',
        'flashSale',
        '"flashSale"."id" = "flashSaleDetail"."flashSaleId"',
      )
      .where('"flashSaleDetail"."merchantId" = :merchantId', {
        merchantId: user.merchantId,
      })
      .select('"flashSale".*')
      .distinct(true);
    if (req.fromDate && req.toDate && user?.role?.type === RoleType.MERCHANT) {
      query.andWhere('"flashSale"."startTime" >= :startDate', {
        startDate: startOfDay(new Date(req.fromDate)),
      });
      query.andWhere('"flashSale"."endTime" <= :endDate', {
        endDate: endOfDay(new Date(req.toDate)),
      });
    }
    const total = (await query.execute())?.length | 0;
    let pageCount = 1;
    if (limit) {
      query.limit(limit).offset((page - 1) * limit);
      pageCount = Math.ceil(total / limit);
    }
    const data = (await query.execute()) as FlashSaleEntity[] | [];
    await Promise.all(
      data.map(async (d) => {
        d.flashSaleDetail = await this.flashSaleDetailRepo.find({
          where: {
            flashSaleId: d.id,
            merchantId: user.merchantId,
          },
        });
      }),
    );
    return {
      data: data,
      page: page,
      count: data?.length | 0,
      total: total,
      pageCount: pageCount,
    };
  }

  /** Lấy danh sách sản phẩm trong chương trình flash sale theo merchant ID */
  async getProductInFlashSaleByMerchantId(
    id: number,
    user: UserEntity,
  ): Promise<FlashSaleEntity> {
    const flashSale = await this.repo.findOne({ id: id });
    const flashSaleDetails = await this.flashSaleDetailRepo.find({
      where: {
        merchantId: user.merchantId,
        flashSaleId: flashSale.id,
      },
    });
    flashSale.flashSaleDetail = flashSaleDetails;
    if (!flashSaleDetails.length) {
      throw new NotFoundException('Không có sản phẩm');
    }
    return flashSale;
  }

  /** Xóa chương trình flash sale */
  async deleteFlashSale(id: number, user: UserEntity) {
    const flashSale = await this.repo.findOne({
      where: {
        id: id,
        startTime: MoreThan(new Date()),
      },
      relations: ['flashSaleDetail'],
    });
    if (!flashSale) {
      throw new BadRequestException(
        'Không thể xóa chương trình đã diễn ra/đã kết thúc',
      );
    }
    await this.repo.softDelete({ id: id });
    await this.flashSaleDetailRepo.softDelete({
      flashSaleId: id,
    });
  }

  /** Lấy danh sách sản phẩm trong chương trình flash sale hiện tại */
  async getNow(dto: FlashSaleGetNowDto): Promise<FlashSaleEntity | any> {
    const flashsale = await this.repo.findOne({
      where: {
        startTime: LessThanOrEqual(new Date()),
        endTime: MoreThan(new Date()),
      },
    });
    if (flashsale) {
      const flashSaleDetailQuery =
        await this.flashSaleDetailRepo.createQueryBuilder('flashSaleDetail');
      flashSaleDetailQuery
        .leftJoinAndSelect('flashSaleDetail.product', 'product')
        .leftJoinAndSelect('product.stocks', 'product_stocks')
        .leftJoinAndSelect('product_stocks.merchantAddress', 'merchantAddress')
        .leftJoinAndSelect('merchantAddress.ward', 'merchantAddressWard')
        .leftJoinAndSelect(
          'merchantAddressWard.district',
          'merchantAddressWardDistrict',
        )
        .where('"flashSaleDetail"."flashSaleId" = :flashSaleId', {
          flashSaleId: flashsale.id,
        })
        .orderBy('flashSaleDetail.productId')
        .addOrderBy('flashSaleDetail.discountValue');
      if (dto.userId) {
        const user = await this.productService.userService.repo.findOne({
          where: { id: dto.userId },
          relations: ['role'],
        });
        if (user?.role?.type === RoleType.PARTNER) {
          flashSaleDetailQuery.andWhere('product.status <> :status', {
            status: ProductStatus.DELETED,
          });
        }
      } else {
        flashSaleDetailQuery.andWhere('product.status <> :status', {
          status: ProductStatus.DELETED,
        });
      }
      if (dto?.provinceId)
        flashSaleDetailQuery.andWhere(
          '"merchantAddressWardDistrict"."provinceId"=:provinceId',
          {
            provinceId: dto?.provinceId,
          },
        );
      if (dto?.productCategoryId)
        flashSaleDetailQuery.andWhere(
          '"product"."productCategoryId"=:productCategoryId',
          {
            productCategoryId: dto?.productCategoryId,
          },
        );
      const flashSaleDetails = await flashSaleDetailQuery.getMany();

      flashsale.flashSaleDetail = flashSaleDetails.reduce(
        (flashSaleDetails, flashSaleDetail: any) => {
          const flashSaleDetailIndex = flashSaleDetails.findIndex(
            ({ productId }) => productId === flashSaleDetail.productId,
          );
          if (flashSaleDetailIndex !== -1) {
            flashSaleDetails[flashSaleDetailIndex].usedQuantity =
              typeof flashSaleDetails[flashSaleDetailIndex].usedQuantity !==
              'number'
                ? flashSaleDetail.usedQuantity
                : flashSaleDetails[flashSaleDetailIndex].usedQuantity +
                  flashSaleDetail.usedQuantity;
            flashSaleDetails[flashSaleDetailIndex].saleAmount =
              flashSaleDetails[flashSaleDetailIndex].usedQuantity;
            return flashSaleDetails;
          }

          flashSaleDetail.saleAmount = flashSaleDetail.usedQuantity;

          return [...flashSaleDetails, flashSaleDetail];
        },
        [],
      );
      return flashsale;
    }
    return {};
  }
}

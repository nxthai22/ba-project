import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FlashSaleDetailEntity } from 'entities/flash-sale-detail.entity';
import { FlashSaleEntity } from 'entities/flash-sale.entity';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { ProductModule } from 'modules/product/product.module';
import { FlashSaleController } from './flash-sale.controller';
import { FlashSaleService } from './flash-sale.service';
import { OrderProductEntity } from 'entities/order-product.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      FlashSaleEntity,
      FlashSaleDetailEntity,
      MerchantAddressEntity,
      OrderProductEntity,
    ]),
    forwardRef(() => ProductModule),
  ],
  controllers: [FlashSaleController],
  providers: [FlashSaleService],
  exports: [FlashSaleService],
})
export class FlashSaleModule {}

import {
  BadRequestException,
  Body,
  Delete,
  Get,
  HttpCode,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import {
  ApiBody,
  ApiOkResponse,
  ApiOperation,
  ApiProperty,
} from '@nestjs/swagger';
import {
  CrudController,
  CrudRequest,
  GetManyDefaultResponse,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { Auth } from 'decorators/auth.decorator';
import { Crud } from 'decorators/crud.decorator';
import { User } from 'decorators/user.decorator';
import { FlashSaleEntity } from 'entities/flash-sale.entity';
import { UserEntity } from 'entities/user.entity';
import { CreateFlashSaleDto } from './dto/create-flash-sale.dto';
import {
  FlashSaleDetailDto,
  FlashSaleFilterWithPaging,
} from './dto/flash-sale.dto';
import { UpdateFlashSaleDto } from './dto/update-flash-sale.dto';
import { FlashSaleService } from './flash-sale.service';
import { FlashSaleGetNowDto } from './dto/flash-sale.get-now.dto';
import { Between, LessThanOrEqual, MoreThanOrEqual, Not } from 'typeorm';

export class FlashSaleMerchant
  implements GetManyDefaultResponse<FlashSaleEntity>
{
  @ApiProperty({
    description: 'count',
  })
  count: number;
  @ApiProperty({
    description: 'data',
    type: [FlashSaleEntity],
  })
  data: FlashSaleEntity[];
  @ApiProperty({
    description: 'page',
  })
  page: number;
  @ApiProperty({
    description: 'pageCount',
  })
  pageCount: number;
  @ApiProperty({
    description: 'total',
  })
  total: number;
}

@Crud({
  controller: 'flash-sale',
  name: 'Flash Sale',
  model: {
    type: FlashSaleEntity,
  },
  dto: {
    create: CreateFlashSaleDto,
    update: UpdateFlashSaleDto,
    replace: UpdateFlashSaleDto,
  },
  query: {
    join: {
      flashSaleDetail: {},
      'flashSaleDetail.product': {},
    },
  },
  // routes: {
  //   updateOneBase: {
  //     decorators: [Auth()],
  //   },
  //   deleteOneBase: {
  //     decorators: [Auth()],
  //   },
  //   createOneBase: {
  //     decorators: [Auth()],
  //   },
  // },
})
// @Auth()
export class FlashSaleController implements CrudController<FlashSaleEntity> {
  constructor(public readonly service: FlashSaleService) {}

  get base(): CrudController<FlashSaleEntity> {
    return this;
  }

  @Override()
  @Auth()
  @HttpCode(200)
  async getOne(
    @Param('id') id: number,
    @ParsedRequest() req: CrudRequest,
    @User() user: UserEntity,
  ) {
    return await this.service.getOneForEachRole(id, user);
  }

  @Override()
  @Auth()
  @HttpCode(200)
  async createOne(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: CreateFlashSaleDto,
    @User() user: UserEntity,
  ) {
    const existFlashsale = await this.service.find({
      where: [
        {
          startTime: LessThanOrEqual(dto.startTime),
          endTime: MoreThanOrEqual(dto.startTime),
        },
        {
          startTime: LessThanOrEqual(dto.endTime),
          endTime: MoreThanOrEqual(dto.endTime),
        },
        {
          startTime: LessThanOrEqual(dto.startTime),
          endTime: MoreThanOrEqual(dto.endTime),
        },
        {
          startTime: MoreThanOrEqual(dto.startTime),
          endTime: LessThanOrEqual(dto.endTime),
        },
      ],
    });
    if (existFlashsale.length > 0)
      throw new BadRequestException(
        'Đã có 1 chương trình Flash sale diễn ra trong khoảng thời gian đã chọn',
      );
    return this.base.createOneBase(req, dto);
  }

  @Override()
  @Auth()
  @HttpCode(200)
  async updateOne(
    @Param('id') id: number,
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: UpdateFlashSaleDto,
  ) {
    const existFlashsale = await this.service.find({
      where: [
        {
          startTime: LessThanOrEqual(dto.startTime),
          endTime: MoreThanOrEqual(dto.startTime),
          id: Not(id),
        },
        {
          startTime: LessThanOrEqual(dto.endTime),
          endTime: MoreThanOrEqual(dto.endTime),
          id: Not(id),
        },
        {
          startTime: LessThanOrEqual(dto.startTime),
          endTime: MoreThanOrEqual(dto.endTime),
          id: Not(id),
        },
        {
          startTime: MoreThanOrEqual(dto.startTime),
          endTime: LessThanOrEqual(dto.endTime),
          id: Not(id),
        },
      ],
    });
    if (existFlashsale.length > 0)
      throw new BadRequestException(
        'Đã có 1 chương trình Flash sale diễn ra trong khoảng thời gian đã chọn',
      );
    return this.base.updateOneBase(req, dto);
  }

  // @Post('/get-flash-sales')
  // @ApiOperation({
  //   summary: 'Lấy danh sách chương trình trong thời gian chọn',
  // })
  // @ApiOkResponse({
  //   type: [FlashSaleEntity],
  // })
  // async getFlashSales(
  //   @Body() dto: FlashSaleFilterDto,
  // ): Promise<FlashSaleEntity[]> {
  //   return this.service.getFlashSales(dto);
  // }

  // @Post('/get-product-flash-sales')
  // @ApiOperation({
  //   summary: 'Lấy danh sách sản phẩm trong chương trình Flash sale',
  // })
  // @ApiOkResponse({
  //   type: SearchResult,
  // })
  // async getProductInFlashSale(
  //   @Body() dto: FlashSaleFilterDto,
  // ): Promise<SearchResult> {
  //   return this.service.getProductInFlashSale(dto);
  // }

  @Get('/get-flash-sale-by-merchant')
  @ApiOperation({
    summary: 'Lấy danh sách flash sale theo merchant',
  })
  @ApiOkResponse({
    type: FlashSaleMerchant,
  })
  @Auth()
  async getFlashSaleMerchant(
    @Query() query: FlashSaleFilterWithPaging,
    @User() user: UserEntity,
  ): Promise<GetManyDefaultResponse<FlashSaleEntity>> {
    return await this.service.getFlashSaleMerchant(query, user);
  }

  @Get('/get-product-flash-sales-by-merchant-id/:id')
  @ApiOperation({
    summary: 'Lấy danh sách sản phẩm trong chương trình Flash sale theo NCC',
  })
  @ApiOkResponse({
    type: FlashSaleEntity,
  })
  @Auth()
  async getProductInFlashSaleByMerchantId(
    @User() user: UserEntity,
    @Param('id') id: number,
  ): Promise<FlashSaleEntity> {
    return this.service.getProductInFlashSaleByMerchantId(id, user);
  }

  @Auth()
  @Post('/add-product-flash-sales/:id')
  @ApiOperation({
    summary: 'Thêm sản phẩm vào chương trình theo từng đối tác',
  })
  @ApiBody({
    type: [FlashSaleDetailDto],
    description: 'Danh sách sản phẩm thêm vào chương trình',
  })
  @ApiOkResponse({
    type: FlashSaleEntity,
  })
  @Auth()
  async addProductToFlashSale(
    @Param('id') id: number,
    @Body() dto: FlashSaleDetailDto[],
    @User() user: UserEntity,
  ): Promise<FlashSaleEntity> {
    return this.service.addProductToFlashSale(id, dto, user);
  }

  @Patch('/update-product-flash-sale/:id')
  @ApiOperation({
    summary: 'Đối tác chỉnh sửa sản phẩm',
  })
  @ApiBody({ type: [FlashSaleDetailDto] })
  @ApiOkResponse({
    type: FlashSaleEntity,
  })
  @Auth()
  async updateProductInFlashSaleByMerchantId(
    @Param('id') id: number,
    @Body() dto: FlashSaleDetailDto[],
    @User() user: UserEntity,
  ): Promise<FlashSaleEntity> {
    return await this.service.updateProductInFlashSaleByMerchantId(
      id,
      dto,
      user,
    );
  }

  @Override()
  @ApiBody({ type: [FlashSaleDetailDto] })
  @Auth()
  async deleleOne(
    @ParsedRequest() req: CrudRequest,
    @Param('id') id: number,
    @User() user: UserEntity,
  ): Promise<void> {
    return await this.service.deleteFlashSale(id, user);
  }

  @Override()
  @Auth()
  async getMany(@ParsedRequest() req: CrudRequest, @User() user: UserEntity) {
    if (user?.roleId !== 1 && user?.merchantId) {
      if (req.parsed.join.find((join) => join.field === 'flashSaleDetail'))
        req.parsed.search.$and.push({
          'flashSaleDetail.merchantId': {
            $eq: user?.merchantId,
          },
        });
    }
    return await this.base.getManyBase(req);
  }

  @Get('now')
  @ApiOperation({
    summary: 'Lấy danh sách sản phẩm flash sale tại thời điểm hiện tại',
  })
  @ApiOkResponse({
    type: FlashSaleEntity,
  })
  async getNow(@Query() dto: FlashSaleGetNowDto): Promise<FlashSaleEntity> {
    return await this.service.getNow(dto);
  }
}

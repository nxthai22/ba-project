import { PickType } from '@nestjs/swagger';
import { FlashSaleEntity } from 'entities/flash-sale.entity';

export class CreateFlashSaleDto extends PickType(FlashSaleEntity, [
  'name',
  'startTime',
  'endTime',
  'maxQuantity',
  'maxProductNumber',
]) {}

import { ApiProperty } from '@nestjs/swagger';

export class FlashSaleGetNowDto {
  @ApiProperty({
    description: 'ID Tỉnh thành',
    type: Number,
    required: false,
  })
  provinceId?: number;

  @ApiProperty({
    description: 'ID chuyên mục',
    type: Number,
    required: false,
  })
  productCategoryId?: number;

  @ApiProperty({
    description: 'ID người dùng thao tác',
    type: Number,
    required: false,
  })
  userId?: number;
}

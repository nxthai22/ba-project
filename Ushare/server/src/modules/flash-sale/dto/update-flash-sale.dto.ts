import { IntersectionType, PickType } from '@nestjs/swagger';
import { FlashSaleEntity } from 'entities/flash-sale.entity';
import { CreateFlashSaleDto } from './create-flash-sale.dto';

export class UpdateFlashSaleDto extends IntersectionType(
  PickType(FlashSaleEntity, ['id']),
  CreateFlashSaleDto,
) {}

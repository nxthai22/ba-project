import { ApiProperty } from '@nestjs/swagger';
import { CommissionValueType } from 'entities/config.entity';
import { AttributeItemValue } from 'entities/product-variant.entity';
import { SearchCriteria } from 'modules/base/base-search';
import { IsNumber, Max, Min } from 'class-validator';
import { DiscountValueType } from '../../../entities/flash-sale-detail.entity';

/** Tìm kiếm */
export class FlashSaleFilterWithPaging {
  @ApiProperty({
    description: 'Tìm kiếm từ ngày',
    required: false,
  })
  fromDate?: Date;

  @ApiProperty({
    description: 'Tìm kiếm đến ngày',
    required: false,
  })
  toDate?: Date;

  @ApiProperty({
    description: 'Số lượng record/trang',
    type: Number,
    required: true,
    default: 10,
  })
  limit: number;

  @ApiProperty({
    description: 'Trang',
    type: Number,
    required: true,
    default: 1,
  })
  page: number;
}

export class FlashSaleFilterDto extends SearchCriteria {
  @ApiProperty({
    description: 'Ngày có chương trình Flash sale',
    default: new Date(),
  })
  date: Date;

  @ApiProperty({
    description: 'Tìm kiếm từ ngày',
  })
  startTime?: Date;

  @ApiProperty({
    description: 'Tìm kiếm đến ngày',
  })
  endTime?: Date;

  @ApiProperty({
    description: 'Id chương trình Flash sale',
  })
  flashSaleId?: number;

  @ApiProperty({
    description: 'Id danh mục sản phẩm',
  })
  productCategoryId?: number;

  @ApiProperty({
    description: 'Id tỉnh/thành phố',
  })
  provinceId?: number;
}

/** Thêm sản phẩm */
export class FlashSaleDetailDto {
  @ApiProperty({
    description: 'Id chương trình flash sale',
  })
  flashSaleId: number;

  @ApiProperty({
    description: 'Id sản phẩm thêm vào chương trình flash sale',
  })
  productId: number;

  @ApiProperty({
    description: 'Id biến thể thêm vào chương trình flash sale',
    required: false,
  })
  productVariantId?: number;

  @ApiProperty({
    description: 'Mức giảm giá',
  })
  discountValue: number;

  @ApiProperty({
    description: 'Loại giảm giá',
  })
  discountValueType: DiscountValueType;

  @IsNumber()
  @ApiProperty({
    description: 'Số lượng sản phẩm được dùng cho chương trình',
  })
  @Min(0, { message: 'Số lượng sản phẩm Khuyến mãi phải lớn hơn 0' })
  qty: number;
}

/** Sản phẩm trong chương trình FlashSale */
export class ProductFlashSale {
  @ApiProperty({
    description: 'Id sản phẩm',
  })
  productId: number;

  @ApiProperty({
    description: 'Tên sản phẩm',
  })
  productName: string;

  @ApiProperty({
    description: 'Giá sản phẩm',
  })
  productPrice: number;

  @ApiProperty({
    description: 'Giá biến thể sau giảm giá flash sale',
  })
  productPriceAfterDiscount: number;

  @ApiProperty({
    description: 'Ảnh sản phẩm',
  })
  productImages: string[];

  @ApiProperty({
    description: 'Danh sách biến thể',
  })
  variants: VariantFlashSale[];
}

/** Biến thể trong chương trình FlashSale */
export class VariantFlashSale {
  @ApiProperty({
    description: 'Id biến thể',
  })
  variantId: number;

  @ApiProperty({
    description: 'Tên biến thể',
  })
  variantName: string;

  @ApiProperty({
    description: 'Giá trị biến thể',
  })
  variantPrice: number;

  @ApiProperty({
    description: 'Ảnh biến thể',
  })
  variantImages: string[];

  @ApiProperty({
    description: 'Giá biến thể sau giảm giá flash sale',
  })
  variantPriceAfterDiscount: number;

  @ApiProperty({
    description: 'Thuộc tính biến thể',
  })
  variantAttributeValue: AttributeItemValue[];
}

import { ApiProperty, PickType } from '@nestjs/swagger';
import { OrderEntity } from 'entities/order.entity';

export class GhnItem {
  @ApiProperty({
    description: 'Tên của sản phẩm.',
    required: false,
  })
  name?: string;

  @ApiProperty({
    description: 'Mã của sản phẩm.',
    required: false,
  })
  code?: string;

  @ApiProperty({
    description: 'Số lượng của sản phẩm.',
    required: false,
  })
  quantity?: number;
}

export class GhnOrderDto {
  @ApiProperty({
    description: 'Tên người nhận hàng.',
  })
  to_name: string;

  @ApiProperty({
    description: 'Số điện thoại người nhận hàng.',
  })
  to_phone: string;

  @ApiProperty({
    description: 'Địa chỉ Shiper tới giao hàng.',
  })
  to_address: string;

  @ApiProperty({
    description: 'Phường của người nhận hàng.',
  })
  to_ward_code: string;

  @ApiProperty({
    description: 'Quận của người nhận hàng.',
  })
  to_district_id: number;

  @ApiProperty({
    description: 'Tiền thu hộ cho người gửi. Maximum :10.000.000',
    required: false,
  })
  cod_amount?: number;

  @ApiProperty({
    description:
      'Giá trị của đơn hàng ( Trường hợp mất hàng , bể hàng sẽ đền theo giá trị của đơn hàng). Tối đa 5.000.000',
    required: false,
  })
  insurance_value?: number;

  @ApiProperty({
    description: 'Khối lượng của đơn hàng (gram).',
    required: false,
  })
  weight?: number;

  @ApiProperty({
    description:
      'Mã loại dịch vụ cố định, giá trị : 1 hoặc 2 (Trong đó  1:Bay hoặc 2:Đi Bộ)',
  })
  service_type_id: number;

  @ApiProperty({
    description:
      'Mã người thanh toán phí dịch vụ.\n' +
      '\n' +
      '1: Người bán/Người gửi.\n' +
      '\n' +
      '2: Người mua/Người nhận.',
  })
  payment_type_id: number;

  @ApiProperty({
    description:
      'Ghi chú bắt buộc, Bao gồm: CHOTHUHANG, CHOXEMHANGKHONGTHU, KHONGCHOXEMHANG',
  })
  required_note: string;

  @ApiProperty({
    description: 'Mã đơn hàng riêng của khách hàng',
  })
  client_order_code: string;

  @ApiProperty({
    description:
      'Ghi chú bắt buộc, Bao gồm: CHOTHUHANG, CHOXEMHANGKHONGTHU, KHONGCHOXEMHANG',
    type: [GhnItem],
  })
  items: GhnItem[];

  @ApiProperty({
    description: 'Số điện thoại trả hàng khi không giao được.',
    required: false,
  })
  return_phone?: string;

  @ApiProperty({
    description: 'Địa chỉ trả hàng khi không giao được.',
    required: false,
  })
  return_address?: string;

  @ApiProperty({
    description: 'Quận của người nhận hàng trả.',
    required: false,
  })
  return_district_id?: number;

  @ApiProperty({
    description: 'Phường của người nhận hàng trả.',
    required: false,
  })
  return_ward_code?: string;

  @ApiProperty({
    description: 'Chiều dài gói hàng (cm)',
    required: false,
    maximum: 200,
  })
  length?: number;

  @ApiProperty({
    description: 'Chiều rộng gói hàng (cm)',
    required: false,
    maximum: 200,
  })
  width?: number;

  @ApiProperty({
    description: 'Chiều cao gói hàng (cm)',
    required: false,
    maximum: 200,
  })
  height?: number;

  @ApiProperty({
    description: 'Ghi chú đơn hàng',
    required: false,
  })
  note?: string;
}

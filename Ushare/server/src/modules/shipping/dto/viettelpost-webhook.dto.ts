import { ApiProperty } from '@nestjs/swagger';

export class VtpWebhookDto {
  DATA: VtpWebhookData;
  TOKEN: string;
}

export class VtpWebhookData {
  @ApiProperty({
    description: 'Mã vận đơn',
  })
  ORDER_NUMBER: string;
  @ApiProperty({
    description: 'Partner order number',
  })
  ORDER_REFERENCE: string;
  @ApiProperty({
    description: 'Ngày tháng trạng thái (dd/MM/yyyy H:m:s)',
  })
  ORDER_STATUSDATE: Date;
  @ApiProperty({
    description: 'Mã đơn hàng',
  })
  ORDER_STATUS: number;
  @ApiProperty({
    description: '',
  })
  STATUS_NAME: string;
  @ApiProperty({
    description: '',
  })
  LOCALION_CURRENTLY: string;
  @ApiProperty({
    description: 'Ghi chú đơn hàng',
  })
  NOTE: string;
  @ApiProperty({
    description:
      'Phí thu hộ (Số tiền hàng cần thu hộ - không bao gồm tiền cước)',
  })
  MONEY_COLLECTION: number;
  @ApiProperty({
    description:
      'Phí thu hộ (Số tiền hàng cần thu hộ - không bao gồm tiền cước)',
  })
  MONEY_FEECOD: number;
  @ApiProperty({
    description: 'Tổng tiền bao gồm VAT',
  })
  MONEY_TOTAL: number;
  @ApiProperty({
    description: 'Thời gian ước tính hoàn thiện',
  })
  EXPECTED_DELIVERY: string;
  @ApiProperty({
    description: 'Trọng lượng sản phẩm',
  })
  PRODUCT_WEIGHT: number;
  @ApiProperty({
    description: 'Dịch vụ đơn hàng',
  })
  ORDER_SERVICE: string;
}

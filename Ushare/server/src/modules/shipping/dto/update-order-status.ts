import {
  OrderShippingStatus,
  OrderStatus,
  ShippingPartner,
} from '../../../enums';

export const updateOrderStatus = (
  status,
  shippingPartner: ShippingPartner,
): any => {
  let statusResult;
  switch (shippingPartner) {
    case ShippingPartner.GHTK:
      switch (status) {
        case 1:
        case 2:
        case 7:
        case 8:
          statusResult = OrderStatus.READY_TO_PICK;
          break;
        case 3:
        case 12:
        case 123:
        case 127:
        case 128:
          statusResult = OrderStatus.PICKING;
          break;
        case 49:
        case 4:
        case 9:
        case 10:
        case 45:
        case 410:
          statusResult = OrderStatus.SHIPPING;
          break;
        case 5:
          statusResult = OrderStatus.SHIPPED;
          break;
        case 6:
        case 11:
          statusResult = OrderStatus.COMPLETED;
          break;
        case -1:
        case 13:
        case 20:
        case 21:
          statusResult = OrderStatus.CANCELLED;
          break;
      }
      break;
    case ShippingPartner.GHN:
      switch (status) {
        case 'ready_to_pick':
          statusResult = OrderStatus.READY_TO_PICK;
          break;
        case 'picking':
        case 'picked':
          statusResult = OrderStatus.PICKING;
          break;
        case 'storing':
        case 'transporting':
        case 'picked':
          statusResult = OrderStatus.SHIPPING;
          break;
        case 'money_collect_delivering':
          statusResult = OrderStatus.SHIPPED;
        case 'delivered':
          statusResult = OrderStatus.COMPLETED;
        case 'returned':
          statusResult = OrderStatus.RETURNED;
      }
      break;
    case ShippingPartner.VIETTELPOST:
      switch (status) {
        case 100:
        case 102:
        case 103:
        case 104:
        case -108:
          statusResult = OrderStatus.READY_TO_PICK;
          break;
        case 105:
          statusResult = OrderStatus.PICKING;
          break;
        case 200:
        case 202:
        case 300:
        case 320:
        case 400:
        case 500:
        case 506:
        case 570:
        case 508:
        case 509:
        case 550:
          statusResult = OrderStatus.SHIPPING;
          break;
        case 501:
          statusResult = OrderStatus.COMPLETED;
        case 504:
          statusResult = OrderStatus.RETURNED;
      }
      break;
  }
  return statusResult;
};

export const updateOrderShippingStatus = (
  status,
  partner: ShippingPartner,
): any => {
  let statusResult;
  switch (partner) {
    case ShippingPartner.GHTK:
      switch (status) {
        case -1:
          statusResult = OrderShippingStatus.CANCELLED;
          break;
        case 1:
        case 2:
        case 7:
        case 8:
        case 127:
        case 128:
        case 12:
          statusResult = OrderShippingStatus.READY_TO_PICK;
          break;
        case 3:
        case 123:
        case 4:
        case 10:
        case 410:
          statusResult = OrderShippingStatus.SHIPPING;
          break;
        case 49:
        case 9:
        case 11:
          statusResult = OrderShippingStatus.FAIL;
          break;
        case 5:
        case 45:
        case 6:
          statusResult = OrderShippingStatus.SHIPPED;
          break;
      }
      break;
    case ShippingPartner.GHN:
      switch (status) {
        case 'ready_to_pick':
        case 'picking':
          statusResult = OrderShippingStatus.READY_TO_PICK;
          break;
        case 'picked':
        case 'storing':
        case 'transporting':
        case 'sorting':
        case 'delivering':
        case 'money_collect_delivering':
        case 'exception':
          statusResult = OrderShippingStatus.SHIPPING;
          break;
        case 'delivered':
          statusResult = OrderStatus.SHIPPED;
          break;
        case 'delivery_fail':
        case 'cancel':
        case 'return_fail':
        case 'waiting_to_return':
        case 'returned':
        case 'return':
        case 'return_transporting':
        case 'return_sorting':
        case 'returning':
        case 'damage':
        case 'lost':
          statusResult = OrderShippingStatus.FAIL;
          break;
      }
      break;
  }
  return statusResult;
};

export const updateOrderShippingStatusName = (
  status,
  partner: ShippingPartner,
): any => {
  let statusName;
  switch (partner) {
    case ShippingPartner.GHTK:
      switch (status) {
        case -1:
          statusName = 'Huỷ đơn hàng (NCC huỷ đơn truyền sang GHTK)';
          break;
        case 1:
          statusName = 'Chưa tiếp nhận';
          break;
        case 2:
          statusName = 'Đã tiếp nhận';
          break;
        case 7:
          statusName = 'Không lấy được hàng';
          break;
        case 8:
          statusName = 'Hoãn lấy hàng';
          break;
        case 127:
          statusName =
            'Shipper (nhân viên lấy/giao hàng) báo không lấy được hàng';
          break;
        case 128:
          statusName = 'Shipper báo delay lấy hàng';
          break;
        case 12:
          statusName = 'Đã điều phối lấy hàng/Đang lấy hàng';
          break;
        case 3:
          statusName = 'Đã lấy hàng/Đã nhập kho';
          break;
        case 123:
          statusName = 'Shipper báo đã lấy hàng';
          break;
        case 4:
          statusName = 'Đã điều phối giao hàng/Đang giao hàng';
          break;
        case 10:
          statusName = 'Delay giao hàng';
          break;
        case 410:
          statusName = 'Shipper báo delay giao hàng';
          break;
        case 49:
          statusName = 'Shipper báo không giao được hàng';
          break;
        case 9:
          statusName = 'Không giao được hàng';
          break;
        case 11:
          statusName = 'Đã đối soát công nợ trả hàng (TH đơn thất bại)';
          break;
        case 5:
          statusName = 'Đã giao hàng/Chưa đối soát';
          break;
        case 45:
          statusName = 'Shipper báo đã giao hàng';
          break;
        case 6:
          statusName = 'Đã đối soát (TH đơn thành công)';
          break;
      }
      break;
    case ShippingPartner.GHN:
      switch (status) {
        case 'ready_to_pick':
          statusName = 'Mới tạo đơn hàng';
          break;
        case 'picking':
          statusName = 'Nhân viên đang lấy hàng';
          break;
        case 'picked':
          statusName = 'Nhân viên đã lấy hàng';
          break;
        case 'storing':
          statusName = 'Hàng đang nằm ở kho';
          break;
        case 'transporting':
          statusName = 'Đang luân chuyển hàng';
          break;
        case 'sorting':
          statusName = 'Đang phân loại hàng hóa';
          break;
        case 'delivering':
          statusName = 'Nhân viên đang giao cho người nhận';
          break;
        case 'money_collect_delivering':
          statusName = 'Nhân viên đang thu tiền người nhận';
          break;
        case 'exception':
          statusName = 'Đơn hàng ngoại lệ không nằm trong quy trình';
          break;
        case 'delivery_fail':
          statusName = 'Nhân viên giao hàng thất bại';
          break;
        case 'cancel':
          statusName = 'Hủy đơn hàng';
          break;
        case 'return_fail':
          statusName = 'Nhân viên trả hàng thất bại';
          break;
        case 'waiting_to_return':
          statusName = 'Chờ trả/hoàn';
          break;
        case 'returned':
          statusName = 'Trả/hoàn thành công';
          break;
        case 'return':
          statusName = 'Hoàn';
          break;
        case 'return_transporting':
          statusName = 'Đang luân chuyển hàng trả';
          break;
        case 'return_sorting':
          statusName = 'Đang phân loại hàng trả';
          break;
        case 'returning':
          statusName = 'Nhân viên đang đi trả hàng';
          break;
        case 'damage':
          statusName = 'Hàng bị hư hỏng';
          break;
        case 'lost':
          statusName = 'Hàng bị mất';
          break;
        case 'delivered':
          statusName = 'Nhân viên đã giao hàng thành công';
          break;
      }
      break;
  }
  return statusName;
};

export const updateOrderStatusByShippingStatus = (
  orderStatus,
  shippingStatus,
  partner: ShippingPartner,
): OrderStatus => {
  let status = orderStatus;
  switch (partner) {
    case ShippingPartner.GHTK:
      switch (shippingStatus) {
        case 5:
        case 45:
        case 6:
          status = OrderStatus.SHIPPED;
          break;
      }
      break;
    case ShippingPartner.GHN:
      switch (status) {
        case 'delivered':
          status = OrderStatus.SHIPPED;
          break;
      }
      break;
  }
  return status;
};

import { ApiProperty } from '@nestjs/swagger';

export enum GhnWebhook {
  Create = 'create',
  Switch_status = 'switch_status',
  Update_weight = 'update_weight',
  Update_cod = 'update_code',
  Update_fee = 'update_fee',
}

export enum GhnPaymentType {
  Shop = 1 /** Người bán/Người gửi */,
  Customer = 2 /** Người mua/Người nhận */,
}

export class GhnWebhookDto {
  cODAmount: number;
  cODTransferDate?: number;
  clientOrderCode: string;
  convertedWeight: number;
  description: string;
  fee: GhnWebhookFee;
  height: number;
  isPartialReturn: string;
  length: number;
  orderCode: string;
  paymentType: GhnPaymentType;
  reason: string;
  reasonCode: string;
  shipperName: string;
  shipperPhone: string;
  shopID: number;
  status: string;
  time: Date;
  totalFee: number;
  type: GhnWebhook;
  warehouse: string;
  weight: number;
  width: number;
}

export class GhnWebhookFee {
  @ApiProperty({
    description: 'Giá trị khuyến mãi.',
  })
  coupon: number;
  @ApiProperty({
    description: 'Phí khai giá hàng hóa',
  })
  insurance: number;
  @ApiProperty({
    description: 'Phí dịch vụ',
  })
  mainService: number;
  @ApiProperty({
    description: 'Phí giao lại hàng',
  })
  r2S: number;
  @ApiProperty({
    description: 'Phí hoàn hàng',
  })
  return: number;
  @ApiProperty({
    description: 'Phí gửi hàng tại bưu cục',
  })
  stationDO: number;
  @ApiProperty({
    description: 'Phí lấy hàng tại bưu cục.',
  })
  stationPU: number;
}

import { ApiProperty } from '@nestjs/swagger';
import { IsString, MaxLength } from 'class-validator';

export class GhtkOrder {
  @ApiProperty({
    description: 'Mã đơn hàng thuộc hệ thống của đối tác',
  })
  id: string;

  @ApiProperty({
    description: 'Tên người liên hệ lấy hàng hóa',
  })
  pick_name: string;

  @ApiProperty({
    description:
      'Số tiền CoD. Nếu bằng 0 thì không thu tiền CoD. Tính theo VNĐ',
  })
  pick_money: number;

  @ApiProperty({
    description:
      'Địa chỉ ngắn gọn để lấy nhận hàng hóa. Ví dụ: nhà số 5, tổ 3, ngách 11, ngõ 45',
  })
  pick_address: string;

  @ApiProperty({
    description: 'Tên tỉnh/thành phố nơi lấy hàng hóa',
  })
  pick_province: string;

  @ApiProperty({
    description: 'Tên quận/huyện nơi lấy hàng hóa',
  })
  pick_district: string;

  @ApiProperty({
    description: 'Tên phường/xã nơi lấy hàng hóa',
  })
  pick_ward: string;

  @ApiProperty({
    description: 'Tên đường/phố nơi lấy hàng hóa',
    required: false,
  })
  pick_street?: string;

  @ApiProperty({
    description: 'Số điện thoại liên hệ nơi lấy hàng hóa',
  })
  pick_tel: string;

  @ApiProperty({
    description: 'Email liên hệ nơi lấy hàng hóa',
    required: false,
  })
  pick_email?: string;

  @ApiProperty({
    description: 'Tên người nhận hàng',
  })
  name: string;

  @ApiProperty({
    description:
      'Địa chỉ chi tiết của người nhận hàng, ví dụ: Chung cư CT1, ngõ 58, đường Trần Bình',
  })
  address: string;

  @ApiProperty({
    description: 'Tên tỉnh/thành phố của người nhận hàng hóa',
  })
  province: string;

  @ApiProperty({
    description: 'Tên quận/huyện của người nhận hàng hóa',
  })
  district: string;

  @ApiProperty({
    description:
      'Tên phường/xã của người nhận hàng hóa (Bắt buộc khi không có đường/phố)',
  })
  ward: string;

  @ApiProperty({
    description:
      'Tên đường/phố của người nhận hàng hóa (Bắt buộc khi không có phường/xã)',
  })
  street?: string;

  @ApiProperty({
    description:
      'Tên thôn/ấp/xóm/tổ/… của người nhận hàng hóa. Nếu không có, vui lòng điền “Khác”',
  })
  hamlet?: string;

  @ApiProperty({
    description: 'Số điện thoại người nhận hàng hóa',
  })
  tel: string;

  @ApiProperty({
    description:
      'Ghi chú đơn hàng. Vd: Khối lượng tính cước tối đa: 1.00 kg. Ghi chú tối đa cho phép là 120 kí tự',
  })
  @IsString({})
  @MaxLength(50, {
    message: 'Ghi chú tối đa cho phép là 120 kí tự',
  })
  note?: string;

  @ApiProperty({
    description:
      'Giá trị đóng bảo hiểm, là căn cứ để tính phí bảo hiểm và bồi thường khi có sự cố.',
  })
  value?: number;

  @ApiProperty({
    description:
      'Nhận một trong hai giá trị gram và kilogram, mặc định là kilogram, đơn vị khối lượng của các sản phẩm có trong gói hàng',
  })
  weight_option?: string;

  @ApiProperty({
    description:
      'Freeship cho người nhận hàng. Nếu bằng 1 COD sẽ chỉ thu người nhận hàng số tiền bằng pick_money, nếu bằng 0 COD sẽ thu tiền người nhận số tiền bằng pick_money + phí ship của đơn hàng, giá trị mặc định bằng 0',
  })
  is_freeship?: number;

}

export class GhtkProduct {
  @ApiProperty({
    description: 'Tên hàng hóa',
  })
  name: string;

  @ApiProperty({
    description: 'Giá trị hàng hóa',
  })
  price: number;

  @ApiProperty({
    description: 'Khối lượng hàng hóa Tính theo đơn vị KG',
  })
  weight: number;

  @ApiProperty({
    description: 'Số lượng hàng hóa',
  })
  quantity: number;

  @ApiProperty({
    description: 'Mã sản phẩm được lấy từ api lấy danh sách thông tin sản phẩm',
  })
  product_code: number;
}

export class GhtkOrderDto {
  @ApiProperty({
    description: 'Thông tin đơn hàng gửi sang GHTK',
  })
  order: GhtkOrder;

  @ApiProperty({
    description: 'Danh sách sản phẩm',
  })
  products: GhtkProduct[];
}

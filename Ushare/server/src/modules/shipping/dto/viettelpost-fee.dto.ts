import { ApiProperty } from '@nestjs/swagger';

export enum ProductVtPType {
  Envelope = 'TH',
  Goods = 'HH',
}

export enum NationalVtPType {
  Inland = '1' /* Trong nước */,
  International = '0' /* Quốc tế */,
}

export class ViettelPostFeeDto {
  @ApiProperty({
    description: 'Trọng lượng sản phẩm',
  })
  PRODUCT_WEIGHT: number;
  @ApiProperty({
    description: 'Giá trị sản phẩm',
  })
  PRODUCT_PRICE: number;
  @ApiProperty({
    description:
      'Phí thu hộ (Số tiền hàng cần thu hộ - không bao gồm tiền cước)',
  })
  MONEY_COLLECTION: number;
  @ApiProperty({
    description:
      'Dịch vụ cộng thêm (theo hợp đồng). Dùng nhiều dịch vụ cộng thêm thì các dịch vụ cách nhau dấu “,”',
  })
  ORDER_SERVICE_ADD: string;
  @ApiProperty({
    description: 'Dịch vụ (theo hợp đồng)',
  })
  ORDER_SERVICE: string;
  @ApiProperty({
    description: 'ID tỉnh/thành phố gửi hàng',
  })
  SENDER_PROVINCE: number;
  @ApiProperty({
    description: 'ID quận/huyện gửi hàng',
  })
  SENDER_DISTRICT: number;
  @ApiProperty({
    description: 'ID tỉnh/thành phố nhận',
  })
  RECEIVER_PROVINCE: number;
  @ApiProperty({
    description: 'ID quận/huyện nhận',
  })
  RECEIVER_DISTRICT: number;
  @ApiProperty({
    description: 'Loại sản phẩm:',
  })
  PRODUCT_TYPE: string;
  @ApiProperty({
    description: 'Hình thức vận chuyển',
    type: 'enum',
    enum: NationalVtPType,
  })
  NATIONAL_TYPE: NationalVtPType;
  @ApiProperty({
    description: 'Chiều rộng/ width',
  })
  PRODUCT_WIDTH: number;
  @ApiProperty({
    description: 'Chiều cao/ height',
  })
  PRODUCT_HEIGHT: number;
  @ApiProperty({
    description: 'Chiều dài/ length',
  })
  PRODUCT_LENGTH: number;
}

export class ViettelPostFeeResponseDto {
  @ApiProperty({
    description: 'Tổng tiền ban đầu',
  })
  MONEY_TOTAL_OLD: number;
  @ApiProperty({
    description: 'Tổng tiền bao gồm VAT',
  })
  MONEY_TOTAL: number;
  @ApiProperty({
    description: 'Cước chính',
  })
  MONEY_TOTAL_FEE: number;
  @ApiProperty({
    description: 'Phụ phí (phụ phí xăng dầu, kết nối huyện...)',
  })
  MONEY_FEE: number;
  @ApiProperty({
    description:
      'Phí thu hộ (Số tiền hàng cần thu hộ - không bao gồm tiền cước)',
  })
  MONEY_COLLECTION_FEE: number;
  @ApiProperty({
    description: 'Phụ phí khác ( phụ phí đóng gói, phát sinh khác….)',
  })
  MONEY_OTHER_FEE: number;
  @ApiProperty({
    description: 'Phí gia tăng ( các dịch vụ cộng thêm khác có phát sinh)',
  })
  MONEY_VAS: number;
  @ApiProperty({
    description: 'Phí VAT',
  })
  MONEY_VAT: number;
  @ApiProperty({
    description: '',
  })
  KPI_HT: number;
}

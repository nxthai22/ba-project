import { ApiProperty } from '@nestjs/swagger';

export enum OrderPaymentType {
  Uncollect = 1 /** Không thu tiền */,
  Collect_fee_and_price = 2 /** Thu hộ tiền cước và tiền hàng*/,
  Collect_price = 3 /** Thu hộ tiền hàng */,
  Collect_fee = 4 /** Thu hộ tiền cước */,
}

export class ProductVtpOrder {
  @ApiProperty({
    description: 'tên sản phẩm',
  })
  PRODUCT_NAME: string;
  @ApiProperty({
    description: 'giá trị sản phẩm',
  })
  PRODUCT_PRICE: number;
  @ApiProperty({
    description: 'trọng lượng sản phẩm',
  })
  PRODUCT_WEIGHT: number;
  @ApiProperty({
    description: 'số lượng sản phẩm',
  })
  PRODUCT_QUANTITY: number;
}

export class ViettelPostOrder {
  @ApiProperty({
    description: 'Mã đơn hàng',
  })
  ORDER_NUMBER: string;
  @ApiProperty({
    description: 'Mã kho',
  })
  GROUPADDRESS_ID: number;
  @ApiProperty({
    description: 'Mã khách hàng',
  })
  CUS_ID: number;
  @ApiProperty({
    description: '\tDelivery date:dd/MM/yyyy H:m:s',
  })
  DELIVERY_DATE: string;
  @ApiProperty({
    description: 'Họ tên người gửi',
  })
  SENDER_FULLNAME: string;
  @ApiProperty({
    description: 'Địa chỉ người gửi',
  })
  SENDER_ADDRESS: string;
  @ApiProperty({
    description: 'Số điện thoại người gửi',
  })
  SENDER_PHONE: string;
  @ApiProperty({
    description: 'Email người gửi',
  })
  SENDER_EMAIL: string;
  @ApiProperty({
    description: 'ID xã/phường người gửi',
  })
  SENDER_WARD: number;
  @ApiProperty({
    description: 'ID quận/huyện người gửi',
  })
  SENDER_DISTRICT: number;
  @ApiProperty({
    description: 'ID tỉnh/thành phố người gửi',
  })
  SENDER_PROVINCE: number;
  @ApiProperty({
    description: 'Vĩ độ gửi hàng',
  })
  SENDER_LATITUDE: number;
  @ApiProperty({
    description: 'Kinh độ gửi hàng',
  })
  SENDER_LONGITUDE: number;
  @ApiProperty({
    description: 'Họ tên người nhận',
  })
  RECEIVER_FULLNAME: string;
  @ApiProperty({
    description: 'Địa chỉ người nhận',
  })
  RECEIVER_ADDRESS: string;
  @ApiProperty({
    description: 'Điện thoai người nhận',
  })
  RECEIVER_PHONE: string;
  @ApiProperty({
    description: 'Email người nhận',
  })
  RECEIVER_EMAIL: string;
  @ApiProperty({
    description: 'ID xã/phường người nhận',
  })
  RECEIVER_WARD: number;
  @ApiProperty({
    description: 'ID quận/huyện người nhận',
  })
  RECEIVER_DISTRICT: number;
  @ApiProperty({
    description: 'ID tỉnh/thành phố người nhận',
  })
  RECEIVER_PROVINCE: number;
  @ApiProperty({
    description: 'Vĩ độ nhận',
  })
  RECEIVER_LATITUDE: number;
  @ApiProperty({
    description: 'Kinh độ nhận',
  })
  RECEIVER_LONGITUDE: number;
  @ApiProperty({
    description: 'Tên sản phẩm',
  })
  PRODUCT_NAME: string;
  @ApiProperty({
    description: 'Mô tả sản phẩm',
  })
  PRODUCT_DESCRIPTION: string;
  @ApiProperty({
    description: 'Số lượng sản phẩm',
  })
  PRODUCT_QUANTITY: number;
  @ApiProperty({
    description: 'Giá trị sản phẩm',
  })
  PRODUCT_PRICE: number;
  @ApiProperty({
    description: 'Trọng lượng sản phẩm',
  })
  PRODUCT_WEIGHT: number;
  @ApiProperty({
    description: 'Chiều cao sản phẩm',
  })
  PRODUCT_LENGTH: number;
  @ApiProperty({
    description: 'Chiều rộng sản phẩm',
  })
  PRODUCT_WIDTH: number;
  @ApiProperty({
    description: 'Chiều cao sản phẩm',
  })
  PRODUCT_HEIGHT: number;
  @ApiProperty({
    description: 'Loại sản phẩm: + TH: Thư/ Envelope;+ HH: Hàng hóa/ Goods;',
  })
  PRODUCT_TYPE: string;
  @ApiProperty({
    description:
      'Loại vận đơn: 1: Không thu tiền,2: Thu hộ tiền cước và tiền hàng,3: Thu hộ tiền hàng,4: Thu hộ tiền cước',
    type: 'enum',
    enum: OrderPaymentType,
  })
  ORDER_PAYMENT: OrderPaymentType;
  @ApiProperty({
    description: 'Dịch vụ đơn hàng',
  })
  ORDER_SERVICE: string;
  @ApiProperty({
    description: 'Dịch vụ cộng thêm',
  })
  ORDER_SERVICE_ADD: string;
  @ApiProperty({
    description: 'Voucher',
  })
  ORDER_VOUCHER: string;
  @ApiProperty({
    description: 'Ghi chú đơn hàng',
  })
  ORDER_NOTE: string;
  @ApiProperty({
    description:
      'Phí thu hộ (Số tiền hàng cần thu hộ - không bao gồm tiền cước)',
  })
  MONEY_COLLECTION: number;
  @ApiProperty({
    description: 'Cước chính',
  })
  MONEY_TOTALFEE: number;
  @ApiProperty({
    description: 'Phụ phí thu hộ',
  })
  MONEY_FEECOD: number;
  @ApiProperty({
    description: 'Phí gia tăng ( các dịch vụ cộng thêm khác có phát sinh)',
  })
  MONEY_FEEVAS: number;
  @ApiProperty({
    description: 'Phí bảo hiểm (= 1% giá trị khai giá)',
  })
  MONEY_FEEINSURANCE: number;
  @ApiProperty({
    description: 'Phụ phí (phụ phí xăng dầu, kết nối huyện...)',
  })
  MONEY_FEE: number;
  @ApiProperty({
    description: 'Phụ phí khác ( phụ phí đóng gói, phát sinh khác….)',
  })
  MONEY_FEEOTHER: number;
  @ApiProperty({
    description: 'Phí VAT',
  })
  MONEY_TOTALVAT: number;
  @ApiProperty({
    description: 'Tổng tiền bao gồm VAT',
  })
  MONEY_TOTAL: number;
  @ApiProperty({
    description: 'Danh sách các sản phẩm trong đơn hàng',
    type: [ProductVtpOrder],
  })
  LIST_ITEM: ProductVtpOrder[];
}

export class ViettelPostOrderResponse {
  @ApiProperty({
    description: 'Mã đơn hàng',
  })
  ORDER_NUMBER: string;
  @ApiProperty({
    description:
      'Phí thu hộ (Số tiền hàng cần thu hộ - không bao gồm tiền cước)',
  })
  MONEY_COLLECTTION: number;
  @ApiProperty({
    description: 'Trọng lượng tính cước',
  })
  EXCHANGE_WEIGHT: number;
  @ApiProperty({
    description: 'Phụ phí khác ( phụ phí đóng gói, phát sinh khác….)',
  })
  MONEY_OTHER_FEE: number;
  @ApiProperty({
    description: 'Phụ phí (phụ phí xăng dầu, kết nối huyện...)',
  })
  MONEY_FEE: number;
  @ApiProperty({
    description:
      'Phí thu hộ (Số tiền hàng cần thu hộ - không bao gồm tiền cước)',
  })
  MONEY_COLLECTION_FEE: number;
  @ApiProperty({
    description: 'Phí VAT',
  })
  MONEY_FEE_VAT: number;
  @ApiProperty({
    description: 'Cước chính',
  })
  MONEY_TOTAL_FEE: number;
  @ApiProperty({
    description: 'Tổng tiền bao gồm VAT',
  })
  MONEY_TOTAL: number;
}

import { ApiProperty } from '@nestjs/swagger';

export class ViettelpostLoginDto {
  @ApiProperty({
    description: 'tài khoản',
    required: true,
  })
  USERNAME: string;
  @ApiProperty({
    description: 'mật khẩu',
    required: true,
  })
  PASSWORD: string;
}

export class ViettelpostLoginResponseDto {
  @ApiProperty({
    description: 'mã',
  })
  status: number;
  @ApiProperty({
    description: 'ID người dùng',
  })
  userid: number;
  @ApiProperty({
    description: 'Mã token',
  })
  token: string;
  @ApiProperty({
    description: 'Mã đối tác',
  })
  partner: number;
  @ApiProperty({
    description: 'Số điện thoại',
  })
  phone: string;
  @ApiProperty({
    description: 'Ngày hết hạn',
  })
  expired: string;
  @ApiProperty({
    description: '',
  })
  encrypted: string;
  @ApiProperty({
    description: 'Nguồn sử dụng: web/app',
  })
  source: string;
}

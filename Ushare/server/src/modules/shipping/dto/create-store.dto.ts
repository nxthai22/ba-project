export class CreateStoreGHNDto {
  district_id: number;
  ward_code: string;
  name: string;
  phone: string;
  address: string;
}

export class CreateStoreVtPDto {
  WARDS_ID: number;
  NAME: string;
  PHONE: string;
  ADDRESS: string;
}

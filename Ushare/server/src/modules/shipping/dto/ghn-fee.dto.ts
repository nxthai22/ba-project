import { ApiProperty, PickType } from '@nestjs/swagger';
import { OrderEntity } from 'entities/order.entity';

export class GhnFeeDto extends PickType(OrderEntity, [
  'width',
  'height',
  'length',
  'note',
]) {
  @ApiProperty({
    description: 'Mã quận/huyện người gửi hàng',
    type: Number,
  })
  from_district_id: number;

  @ApiProperty({
    description:
      'Mã loại dịch vụ cố định, giá trị :1 hoặc 2 (Trong đó 1: Bay or  2: Đi bộ)',
    type: Number,
  })
  service_type_id: number;

  @ApiProperty({
    description: 'Mã phường/xã người nhận hàng',
    type: Number,
  })
  to_ward_code: string;

  @ApiProperty({
    description: 'Mã quận/huyện người nhận hàng',
    type: Number,
  })
  to_district_id: number;

  @ApiProperty({
    description: 'Khối lượng của đơn hàng (gram).',
    type: Number,
  })
  weight: number;
}

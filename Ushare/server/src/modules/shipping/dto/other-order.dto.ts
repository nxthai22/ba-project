import { ApiProperty } from '@nestjs/swagger';
import { OrderStatus, ShippingPartner } from 'enums';

export class OtherOrder {
  @ApiProperty({
    description: 'Đối tác vận chuyển',
    enum: ShippingPartner,
    default: ShippingPartner.GHTK,
  })
  partner: ShippingPartner;

  @ApiProperty({
    description: 'Ghi chú của Merchant cho Đối tác',
  })
  note?: string;

  @ApiProperty({
    description: 'Mã đơn hàng',
  })
  id?: number;

  @ApiProperty({
    description: 'Mã đơn hàng đối tác',
    required: false,
  })
  partner_id?: string;

  @ApiProperty({
    description: 'Phí vận chuyển',
  })
  fee: number;

  @ApiProperty({
    description: 'Phí bảo hiểm',
    required: false,
  })
  insurance_fee?: number;

  @ApiProperty({
    description: 'Thời gian lấy hàng dự kiến',
    required: false,
  })
  estimated_pick_time?: string;

  @ApiProperty({
    description: 'Thời gian giao hàng dự kiến',
    required: false,
  })
  estimated_deliver_time?: string;

  @ApiProperty({
    description:
      'Trạng thái giao hàng (Tiếp nhận đơn hàng : Created;Lấy hàng: picking;Đang Giao: shipping;Đã giao thành công: shipped;Đã đối soát: complete;Hủy giao hàng: cancel)',
    required: false,
    enum: OrderStatus,
  })
  status?: OrderStatus;
}

export class GhtkWebhookDto {
  partner_id: string;
  label_id: string;
  status_id: number;
}

import { ApiProperty } from '@nestjs/swagger';

export class GhtkFeeDto {
  @ApiProperty({
    description:
      'Địa chỉ ngắn gọn để lấy nhận hàng hóa. Ví dụ: nhà số 5, tổ 3, ngách 11, ngõ 45',
    required: false,
  })
  pick_address?: string;

  @ApiProperty({
    description: 'Tên tỉnh/thành phố nơi lấy hàng hóa',
  })
  pick_province: string;

  @ApiProperty({
    description: 'Tên quận/huyện nơi lấy hàng hóa',
  })
  pick_district: string;

  @ApiProperty({
    description: 'Tên phường/xã nơi lấy hàng hóa',
    required: false,
  })
  pick_ward?: string;

  @ApiProperty({
    description: 'Tên đường/phố nơi lấy hàng hóa',
    required: false,
  })
  pick_street?: string;

  @ApiProperty({
    description:
      'Địa chỉ chi tiết của người nhận hàng, ví dụ: Chung cư CT1, ngõ 58, đường Trần Bình',
    required: false,
  })
  address?: string;

  @ApiProperty({
    description: 'Tên tỉnh/thành phố của người nhận hàng hóa',
  })
  province: string;

  @ApiProperty({
    description: 'Tên quận/huyện của người nhận hàng hóa',
    required: false,
  })
  district: string;

  @ApiProperty({
    description: 'Tên phường/xã của người nhận hàng hóa',
    required: false,
  })
  ward?: string;

  @ApiProperty({
    description: 'Tên đường/phố của người nhận hàng hóa',
    required: false,
  })
  street?: string;

  @ApiProperty({
    description:
      'Địa chỉ chi tiết của người nhận hàng, ví dụ: Chung cư CT1, ngõ 58, đường Trần Bình',
    required: false,
  })
  weight: number;

  @ApiProperty({
    description:
      'Giá trị thực của đơn hàng áp dụng để tính phí bảo hiểm, đơn vị sử dụng VNĐ',
    required: false,
  })
  value?: string;

  @ApiProperty({
    description:
      'Sử dụng phương thức vận chuyển xfast. Nhận 1 trong 2 giá trị xteam/none',
    required: false,
  })
  deliver_option?: 'xteam' | 'none';
}

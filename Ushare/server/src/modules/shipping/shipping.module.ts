import { Module } from '@nestjs/common';
import { ShippingService } from './shipping.service';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule as NestConfigModule } from "@nestjs/config";
import { ShippingController } from 'modules/shipping/shipping.controller';
import { OrderEntity } from 'entities/order.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderHistoryEntity } from 'entities/order-history.entity';
import { ProvinceEntity } from 'entities/province.entity';
import { DistrictEntity } from 'entities/district.entity';
import { WardEntity } from 'entities/ward.entity';
import { MerchantAddressEntity } from "../../entities/merchant-address.entity";
import { OrderProductEntity } from '../../entities/order-product.entity';
import { ConfigModule } from 'modules/config/config.module';

@Module({
  imports: [
    HttpModule,
    NestConfigModule,
    TypeOrmModule.forFeature([
      OrderEntity,
      OrderHistoryEntity,
      ProvinceEntity,
      DistrictEntity,
      WardEntity,
      MerchantAddressEntity,
      OrderProductEntity
    ]),
    ConfigModule
  ],
  controllers: [ShippingController],
  providers: [ShippingService],
  exports: [ShippingService],
})
export class ShippingModule {}

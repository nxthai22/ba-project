import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService as NestConfigService } from '@nestjs/config';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { DistrictEntity } from 'entities/district.entity';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import {
  OrderHistoryEntity,
  OrderHistoryType,
} from 'entities/order-history.entity';
import { OrderEntity, ShippingInfo } from 'entities/order.entity';
import { ProvinceEntity } from 'entities/province.entity';
import { WardEntity } from 'entities/ward.entity';
import {
  GHN_API,
  GHTK_API,
  OrderShippingStatus,
  OrderStatus,
  ShippingPartner,
  VIETTELPOST_API,
} from 'enums';
import * as OrderPaymentTypeEnum from 'enums/index';
import { GhnFeeDto } from 'modules/shipping/dto/ghn-fee.dto';
import { GhnOrderDto } from 'modules/shipping/dto/ghn-order.dto';
import { GhtkFeeDto } from 'modules/shipping/dto/ghtk-fee.dto';
import { GhtkOrderDto } from 'modules/shipping/dto/ghtk-order.dto';
import { GhtkWebhookDto } from 'modules/shipping/dto/ghtk-webhook.dto';
import { lastValueFrom } from 'rxjs';
import { Connection, In, IsNull, Repository } from 'typeorm';
import {
  ConfigEntity,
  ConfigType,
  ShippingInfoApiToken,
} from '../../entities/config.entity';
import { OrderProductEntity } from '../../entities/order-product.entity';
import { UserEntity } from '../../entities/user.entity';
import { ConfigService } from '../config/config.service';
import { CreateStoreVtPDto } from './dto/create-store.dto';
import { GhnWebhookDto } from './dto/ghn-webhook.dto';
import {
  updateOrderShippingStatus,
  updateOrderShippingStatusName,
  updateOrderStatus,
  updateOrderStatusByShippingStatus,
} from './dto/update-order-status';
import {
  NationalVtPType,
  ProductVtPType,
  ViettelPostFeeDto,
} from './dto/viettelpost-fee.dto';
import {
  ViettelpostLoginDto,
  ViettelpostLoginResponseDto,
} from './dto/viettelpost-login.dto';
import {
  OrderPaymentType,
  ProductVtpOrder,
  ViettelPostOrder,
} from './dto/viettelpost-order.dto';
import { VtpWebhookDto } from './dto/viettelpost-webhook.dto';
import { Cron, CronExpression } from '@nestjs/schedule';

@Injectable()
export class ShippingService {
  private ghtkToken = '01f9963DAD63e890F1Dc8d1B16324857e08C56E3';
  private ghnToken = '9d41b133-359c-11ec-b514-aeb9e8b0c5e3';
  private ghnShopId = '82752';
  private viettelPostToken = '';

  constructor(
    private nestConfigService: NestConfigService,
    private httpService: HttpService,
    @InjectRepository(OrderEntity)
    private orderRepo: Repository<OrderEntity>,
    @InjectRepository(OrderProductEntity)
    private orderProductRepo: Repository<OrderProductEntity>,
    @InjectRepository(ProvinceEntity)
    private provinceRepo: Repository<ProvinceEntity>,
    @InjectRepository(DistrictEntity)
    private districtRepo: Repository<DistrictEntity>,
    @InjectRepository(WardEntity)
    public wardRepo: Repository<WardEntity>,
    @InjectRepository(OrderHistoryEntity)
    private orderHistoryRepo: Repository<OrderHistoryEntity>,
    @InjectRepository(MerchantAddressEntity)
    private merchantAddressRepo: Repository<MerchantAddressEntity>,
    private configService: ConfigService,
    @InjectConnection() private connection: Connection,
  ) {}

  /** Lấy token từ viettel post */
  async updateTokenViettelPost(): Promise<ConfigEntity> {
    const config = (
      (await this.configService.repo.findOne({ key: ConfigType.SHIPPING_INFO }))
        ?.value as ShippingInfoApiToken[]
    )?.find((x) => x.partner === ShippingPartner.VIETTELPOST);

    const userDto: ViettelpostLoginDto = {
      USERNAME:
        config?.username || this.nestConfigService.get<string>('VtP_UserName'),
      PASSWORD:
        config?.password || this.nestConfigService.get<string>('VtP_Password'),
    };
    const response = await lastValueFrom(
      this.httpService.post(
        `${this.nestConfigService.get<string>('VtP_URL')}${
          VIETTELPOST_API.Login
        }`,
        userDto,
      ),
    );
    const result: ViettelpostLoginResponseDto = {
      status: response.data.status,
      userid: response.data.data.userid,
      partner: response.data.data.partner,
      token: response.data.data.token,
      phone: response.data.data.phone,
      expired: response.data.data.expired,
      encrypted: response.data.data.encrypted,
      source: response.data.data.source,
    };
    const shippingInfoConfig = await this.configService.repo.findOne({
      key: ConfigType.SHIPPING_INFO,
    });
    let shippingInfoVtPConfigValue = (
      shippingInfoConfig?.value as ShippingInfoApiToken[]
    )?.find((x) => x.partner === ShippingPartner.VIETTELPOST);
    if (!shippingInfoVtPConfigValue) {
      const config = new ShippingInfoApiToken();
      config.token = result.token;
      config.partner = ShippingPartner.VIETTELPOST;
      config.username = userDto.USERNAME;
      config.password = userDto.PASSWORD;
      shippingInfoConfig.value.push(config);
    } else {
      shippingInfoVtPConfigValue.token = result.token;
    }
    return await this.configService.repo.save(shippingInfoConfig);
  }

  /** Tạo đơn hàng sang GHTK */
  async createGhtkOrder(orderProductIds: number[]): Promise<ShippingInfo> {
    const orderProducts = await this.orderProductRepo.find({
      where: {
        id: In([...orderProductIds]),
      },
      relations: [
        'order',
        'order.ward',
        'order.ward.district',
        'order.ward.district.province',
        'product',
        'order.merchantAddress',
        'order.merchantAddress.ward',
        'order.merchantAddress.ward.district',
        'order.merchantAddress.ward.district.province',
      ],
    });
    if (orderProducts?.length < 1) {
      throw new NotFoundException('Không có sản phẩm nào được chọn');
    }
    const newOrder = new GhtkOrderDto();
    let pick_money = 0;
    if (
      orderProducts?.[0].order.paymentStatus ===
      OrderPaymentTypeEnum.OrderPaymentStatus.DEPOSITED
    ) {
      pick_money =
        orderProducts[0].order.total -
        Number(orderProducts[0].order.deposit || 0);
    } else if (
      orderProducts?.[0].order.paymentType ===
      OrderPaymentTypeEnum.OrderPaymentType.COD
    ) {
      pick_money = orderProducts[0].order.total;
    }
    newOrder.order = {
      id: String(orderProducts?.[0].order.id),
      pick_money: pick_money,
      pick_name: orderProducts?.[0].order.merchantAddress.name,
      pick_address: orderProducts?.[0].order.merchantAddress.address,
      pick_province:
        orderProducts?.[0].order.merchantAddress.ward.district.province.name,
      pick_district:
        orderProducts?.[0].order.merchantAddress.ward.district.name,
      pick_ward: orderProducts?.[0].order.merchantAddress.ward.name,
      pick_tel: orderProducts?.[0].order.merchantAddress.tel,
      name: orderProducts?.[0].order.fullname,
      address: orderProducts?.[0].order.shippingAddress,
      province: orderProducts?.[0].order.ward.district.province.name,
      district: orderProducts?.[0].order.ward.district.name,
      ward: orderProducts?.[0].order.ward.name,
      hamlet: 'Khác',
      tel: orderProducts?.[0].order.tel,
      value: orderProducts.reduce((total, prod) => {
        return total + Number(prod.price || 0) * Number(prod.quantity || 0);
      }, 0),
      note: orderProducts?.[0].order.note,
      weight_option: 'gram',
      is_freeship: 1,
    };
    newOrder.products = orderProducts.map((product) => ({
      name: product.product.name,
      price: product.price,
      weight: product.product.weight || 10,
      quantity: product.quantity || 1,
      product_code: product.product.id,
    }));
    try {
      const configGhtk = (
        (await this.configService.findOne({ key: ConfigType.SHIPPING_INFO }))
          ?.value as ShippingInfoApiToken[]
      )?.find((x) => x.partner === ShippingPartner.GHTK);
      const createOrderResponse = await lastValueFrom(
        this.httpService.post(
          `${this.nestConfigService.get<string>('GHTK_URL')}${
            GHTK_API.Create_Order
          }`,
          newOrder,
          {
            headers: {
              Token: configGhtk.token,
            },
          },
        ),
      );
      if (!createOrderResponse?.data?.success) {
        throw new BadRequestException({
          statusCode: 400,
          message: createOrderResponse?.data?.message,
        });
      }
      return {
        orderProductIds: orderProductIds,
        partner: ShippingPartner.GHTK,
        partner_id: createOrderResponse?.data?.order.label,
        fee: Number(createOrderResponse?.data?.order.fee),
        insurance_fee: Number(createOrderResponse?.data?.order.insurance_fee),
        estimated_pick_time:
          createOrderResponse?.data?.order.estimated_pick_time,
        estimated_deliver_time:
          createOrderResponse?.data?.order.estimated_deliver_time,
        status: OrderShippingStatus.READY_TO_PICK,
      };
    } catch (e) {
      throw new BadRequestException({
        statusCode: 400,
        message: e.response?.message,
      });
    }
  }

  /** Tạo đơn hàng sang GHN */
  async createGhnOrder(orderProductIds: number[]): Promise<ShippingInfo> {
    const orderProducts = await this.orderProductRepo.find({
      where: {
        id: In([...orderProductIds]),
      },
      relations: [
        'order',
        'order.ward',
        'order.ward.district',
        'order.ward.district.province',
        'product',
        'order.merchantAddress',
        'order.merchantAddress.ward',
        'order.merchantAddress.ward.district',
        'order.merchantAddress.ward.district.province',
      ],
    });
    if (orderProducts?.length < 1) {
      throw new NotFoundException('Không có sản phẩm nào được chọn');
    }
    const newOrder = new GhnOrderDto();
    let cod_amount = 0;

    if (
      orderProducts?.[0].order.paymentStatus ===
      OrderPaymentTypeEnum.OrderPaymentStatus.DEPOSITED
    ) {
      cod_amount =
        orderProducts[0].order.total -
        Number(orderProducts[0].order.deposit || 0);
    } else if (
      orderProducts?.[0].order.paymentType ===
      OrderPaymentTypeEnum.OrderPaymentType.COD
    ) {
      cod_amount = orderProducts[0].order.total;
    }
    newOrder.cod_amount = cod_amount;
    newOrder.to_name = orderProducts?.[0].order.fullname;
    newOrder.to_address = orderProducts?.[0].order.shippingAddress;
    newOrder.to_ward_code = orderProducts?.[0].order.ward.ghnCode;
    newOrder.to_district_id = orderProducts?.[0].order.ward.district.ghnId;
    newOrder.to_phone = orderProducts?.[0].order.tel;
    newOrder.return_phone = orderProducts?.[0].order.merchantAddress.tel;
    newOrder.return_address = orderProducts?.[0].order.merchantAddress.address;
    newOrder.return_district_id =
      orderProducts?.[0].order.merchantAddress.ward.district.ghnId;
    newOrder.return_ward_code =
      orderProducts?.[0].order.merchantAddress.ward.ghnCode;
    newOrder.to_phone = orderProducts?.[0].order.tel;
    newOrder.note = orderProducts?.[0].order.note;
    newOrder.length = orderProducts?.[0].order.length;
    newOrder.width = orderProducts?.[0].order.width;
    newOrder.height = orderProducts?.[0].order.height;
    newOrder.client_order_code = String(orderProducts?.[0].order.id);
    newOrder.weight =
      orderProducts?.reduce((weight, product) => {
        return weight + product.quantity * product.product.weight;
      }, 0) || 1000;
    newOrder.service_type_id = 2;
    newOrder.payment_type_id = 1;
    newOrder.required_note = 'CHOXEMHANGKHONGTHU';
    newOrder.items = orderProducts?.map((product) => ({
      name: product.product.name,
      quantity: product.quantity || 1,
      code: String(product.product.id),
    }));
    try {
      const configGhn = (
        (await this.configService.findOne({ key: ConfigType.SHIPPING_INFO }))
          ?.value as ShippingInfoApiToken[]
      )?.find((x) => x.partner === ShippingPartner.GHN);
      const createGhnOrderResponse = await lastValueFrom(
        this.httpService.post(
          `${this.nestConfigService.get<string>('GHN_URL')}${
            GHN_API.Create_Order
          }`,
          newOrder,
          {
            headers: {
              Token: configGhn.token,
              ShopId: configGhn.shopUshareId,
            },
          },
        ),
      );
      if (createGhnOrderResponse?.data.code !== 200) {
        throw new BadRequestException('Lỗi tạo đơn sang đơn vị vận chuyển');
      }
      /** Cập nhật địa chỉ lấy hàng */
      const updateSendAddressDto = {
        order_code: createGhnOrderResponse.data?.data.order_code,
        from_name: orderProducts?.[0].order.fullname,
        from_phone: orderProducts?.[0].order.tel,
        from_address: orderProducts?.[0].order.shippingAddress,
        from_ward_code:
          orderProducts?.[0].order.merchantAddress.ward.ghnCode.toString(),
        from_district_id:
          orderProducts?.[0].order.merchantAddress.ward.district.ghnId,
        status: OrderStatus.READY_TO_PICK,
      };
      const updateSendAddressResponse = await lastValueFrom(
        this.httpService.post(
          `${this.nestConfigService.get<string>('GHN_URL')}${
            GHN_API.Update_Order
          }`,
          updateSendAddressDto,
          {
            headers: {
              Token: configGhn.token,
              ShopId: configGhn.shopUshareId,
            },
          },
        ),
      );
      if (updateSendAddressResponse.data.code === 200) {
        const data: GhnFeeDto = {
          from_district_id: updateSendAddressDto.from_district_id,
          service_type_id: 2,
          to_district_id: newOrder.to_district_id,
          to_ward_code: newOrder.to_ward_code,
          weight: newOrder.weight,
          height: newOrder.height,
          length: newOrder.length,
          width: newOrder.width,
        };
        const feeResponse = await lastValueFrom(
          this.httpService.post(
            `${this.nestConfigService.get<string>('GHN_URL')}${
              GHN_API.Calculate_Fee
            }`,
            data,
            {
              headers: {
                Token: configGhn?.token,
                ShopId: configGhn.shopUshareId,
              },
            },
          ),
        );
        if (!feeResponse || feeResponse?.data.code !== 200) {
          await this.cancelGhnOrder(
            [updateSendAddressDto.order_code],
            configGhn,
          );
          return null;
        }
        return {
          orderProductIds: orderProductIds,
          partner: ShippingPartner.GHN,
          partner_id: createGhnOrderResponse?.data?.data?.order_code,
          fee: Number(feeResponse?.data.data.total),
          insurance_fee: Number(feeResponse?.data.data.insurance_fee),
          estimated_deliver_time:
            createGhnOrderResponse?.data?.data.expected_delivery_time,
          status: OrderShippingStatus.READY_TO_PICK,
        };
      } else {
        await this.cancelGhnOrder([updateSendAddressDto.order_code], configGhn);
        return null;
      }
    } catch (e) {
      throw new BadRequestException(
        e.response.data.code_message_value,
        e.response.data,
      );
    }
  }

  /** Hủy đơn đã tạo sang GHN */
  async cancelGhnOrder(
    order_code: string[],
    config?: ShippingInfoApiToken,
  ): Promise<boolean> {
    let configGhn = config;
    if (!configGhn) {
      configGhn = (
        (await this.configService.findOne({ key: ConfigType.SHIPPING_INFO }))
          ?.value as ShippingInfoApiToken[]
      )?.find((x) => x.partner === ShippingPartner.GHN);
    }
    const dataRequest = {
      order_codes: [...order_code],
    };
    const canceResponse = await lastValueFrom(
      this.httpService.post(
        `${this.nestConfigService.get<string>('GHN_URL')}${
          GHN_API.Cancel_Order
        }`,
        dataRequest,
        {
          headers: {
            Token: configGhn?.token,
            ShopId: configGhn.shopUshareId,
          },
        },
      ),
    );
    if (canceResponse?.data.code === 200) {
      return true;
    }
    return false;
  }

  /** Tạo đơn hàng vận chuyển sang ViettelPost */
  async createViettelPostOrder(
    orderProductIds: number[],
  ): Promise<ShippingInfo> {
    if (orderProductIds?.length < 1) {
      throw new BadRequestException('Không có sản phẩm được chọn');
    }
    const orderProducts = await this.orderProductRepo.find({
      where: {
        id: In([...orderProductIds]),
      },
      relations: [
        'order',
        'order.ward',
        'order.ward.district',
        'order.ward.district.province',
        'product',
        'order.merchantAddress',
        'order.merchantAddress.ward',
        'order.merchantAddress.ward.district',
        'order.merchantAddress.ward.district.province',
      ],
    });
    if (orderProducts?.length < 1) {
      throw new NotFoundException('Không có sản phẩm nào được chọn');
    }
    const orderProductVtps: ProductVtpOrder[] = orderProducts.map((prod) => {
      return {
        PRODUCT_NAME: prod.product.name,
        PRODUCT_PRICE: prod.price,
        PRODUCT_QUANTITY: prod.quantity,
        PRODUCT_WEIGHT: prod.product.weight,
      };
    });
    const configVtp = (
      (await this.configService.findOne({ key: ConfigType.SHIPPING_INFO }))
        ?.value as ShippingInfoApiToken[]
    )?.find((x) => x.partner === ShippingPartner.VIETTELPOST);
    if (!configVtp) {
      throw new NotFoundException(
        'Chưa cấu hình đơn vị vận chuyển Viettel post',
      );
    }
    let MONEY_COLLECTION = 0;

    if (
      orderProducts?.[0].order.paymentStatus ===
      OrderPaymentTypeEnum.OrderPaymentStatus.DEPOSITED
    ) {
      MONEY_COLLECTION =
        orderProducts[0].order.total -
        Number(orderProducts[0].order.deposit || 0);
    } else if (
      orderProducts?.[0].order.paymentType ===
      OrderPaymentTypeEnum.OrderPaymentType.COD
    ) {
      MONEY_COLLECTION = orderProducts[0].order.total;
    }
    const data: ViettelPostOrder = {
      ORDER_NUMBER: orderProducts?.[0]?.order.id.toString(),
      GROUPADDRESS_ID: Number(
        orderProducts?.[0]?.order?.merchantAddress?.vtpId || 0,
      ),
      CUS_ID: Number(configVtp?.shopUshareId || 0),
      DELIVERY_DATE: '',
      RECEIVER_FULLNAME: orderProducts?.[0]?.order.fullname,
      RECEIVER_PHONE: orderProducts?.[0]?.order.tel,
      RECEIVER_EMAIL: '',
      RECEIVER_ADDRESS: orderProducts?.[0]?.order.shippingAddress,
      RECEIVER_WARD: orderProducts?.[0]?.order.ward.vtpId,
      RECEIVER_DISTRICT: orderProducts?.[0]?.order.ward.district.vtpId,
      RECEIVER_PROVINCE: orderProducts?.[0]?.order.ward.district.province.vtpId,
      RECEIVER_LATITUDE: 0,
      RECEIVER_LONGITUDE: 0,
      SENDER_FULLNAME: orderProducts?.[0]?.order.merchantAddress.name,
      SENDER_PHONE: orderProducts?.[0]?.order.merchantAddress.tel,
      SENDER_EMAIL: '',
      SENDER_ADDRESS: orderProducts?.[0]?.order.merchantAddress.address,
      SENDER_WARD: orderProducts?.[0]?.order.merchantAddress.ward.vtpId,
      SENDER_DISTRICT:
        orderProducts?.[0]?.order.merchantAddress.ward.district.vtpId,
      SENDER_PROVINCE:
        orderProducts?.[0]?.order.merchantAddress.ward.district.province.vtpId,
      SENDER_LATITUDE: 0,
      SENDER_LONGITUDE: 0,
      PRODUCT_NAME: orderProducts?.[0]?.product?.name,
      PRODUCT_DESCRIPTION: '',
      PRODUCT_QUANTITY: orderProducts.reduce(function (total, prod) {
        return total + prod.quantity;
      }, 0),
      PRODUCT_PRICE: orderProducts?.[0]?.product.price || 0,
      PRODUCT_WEIGHT: orderProducts?.[0]?.product?.weight || 0,
      PRODUCT_LENGTH: orderProducts?.[0]?.product?.length || 0,
      PRODUCT_HEIGHT: orderProducts?.[0]?.product?.height || 0,
      PRODUCT_WIDTH: orderProducts?.[0]?.product?.width || 0,
      PRODUCT_TYPE: ProductVtPType.Goods,
      ORDER_PAYMENT: OrderPaymentType.Collect_fee_and_price,
      ORDER_SERVICE: 'VCN',
      ORDER_SERVICE_ADD: '',
      ORDER_NOTE: orderProducts?.[0]?.order.note,
      ORDER_VOUCHER: '',
      MONEY_COLLECTION: MONEY_COLLECTION,
      MONEY_FEE: 0,
      MONEY_FEECOD: 0,
      MONEY_FEEINSURANCE: 0,
      MONEY_FEEOTHER: 0,
      MONEY_FEEVAS: 0,
      MONEY_TOTAL: 0,
      MONEY_TOTALFEE: 0,
      MONEY_TOTALVAT: 0,
      LIST_ITEM: orderProductVtps,
    };
    try {
      let createVtpOrderResponse = await lastValueFrom(
        this.httpService.post(
          `${this.nestConfigService.get<string>('VtP_URL')}${
            VIETTELPOST_API.Create_Order
          }`,
          data,
          {
            headers: {
              token: configVtp?.token,
            },
          },
        ),
      );
      /** Lỗi token => update lại token */
      if (createVtpOrderResponse.data?.status !== 202) {
        const config = (
          (await this.updateTokenViettelPost())?.value as ShippingInfoApiToken[]
        )?.find((x) => x.partner === ShippingPartner.VIETTELPOST);
        createVtpOrderResponse = await lastValueFrom(
          this.httpService.post(
            `${this.nestConfigService.get<string>('VtP_URL')}${
              VIETTELPOST_API.Create_Order
            }`,
            data,
            {
              headers: {
                token: config.token,
              },
            },
          ),
        );
      }
      if (createVtpOrderResponse.data.status !== 200) {
        throw new BadRequestException('Lỗi tạo đơn sang đơn vị vận chuyển');
      }
      return {
        orderProductIds: orderProductIds,
        partner: ShippingPartner.VIETTELPOST,
        partner_id: createVtpOrderResponse?.data?.data?.ORDER_NUMBER,
        fee: Number(createVtpOrderResponse?.data?.data?.MONEY_TOTAL || 0),
        insurance_fee: 0,
        estimated_deliver_time: '',
        status: OrderShippingStatus.READY_TO_PICK,
      };
    } catch (e) {
      throw new BadRequestException(
        e.response.data.code_message_value,
        e.response.data,
      );
    }
  }

  /** Tính phí vận chuyển GHTK */
  async calculateGhtkFee(
    orderProducts: OrderProductEntity[],
    user: UserEntity,
  ): Promise<ShippingInfo> {
    const data: GhtkFeeDto = {
      address: orderProducts?.[0]?.order?.shippingAddress,
      district: orderProducts?.[0]?.order?.ward?.district?.name,
      province: orderProducts?.[0]?.order?.ward?.district?.province?.name,
      ward: orderProducts?.[0]?.order?.ward?.name,
      pick_address: orderProducts?.[0]?.order?.merchantAddress?.address,
      pick_ward: orderProducts?.[0]?.order?.merchantAddress?.ward?.name,
      pick_district:
        orderProducts?.[0]?.order?.merchantAddress?.ward?.district?.name,
      pick_province:
        orderProducts?.[0]?.order?.merchantAddress?.ward?.district?.province
          ?.name,
      weight: orderProducts.reduce((weight, product) => {
        return weight + product.quantity * product.product.weight;
      }, 0),
    };
    try {
      const configGhtk = (
        (await this.configService.findOne({ key: ConfigType.SHIPPING_INFO }))
          ?.value as ShippingInfoApiToken[]
      )?.find((x) => x.partner === ShippingPartner.GHTK);
      const feeResponse = await lastValueFrom(
        this.httpService.get(
          `${this.nestConfigService.get<string>('GHTK_URL')}${
            GHTK_API.Calculate_Fee
          }`,
          {
            headers: {
              Token: configGhtk.token,
            },
            data,
          },
        ),
      );
      if (!feeResponse?.data?.success) {
        new BadRequestException(feeResponse?.data?.message);
      }
      return {
        fee: feeResponse?.data.fee.fee,
        insurance_fee: feeResponse?.data.fee.insurance_fee,
        partner: ShippingPartner.GHTK,
        id: orderProducts?.[0]?.order.id,
      };
    } catch (e) {
      throw new BadRequestException(
        e.response?.data?.code_message_value,
        e.response?.data,
      );
    }
  }

  /** Tính phí vận chuyển GHN */
  async calculateGhnFee(
    orderProducts: OrderProductEntity[],
    user: UserEntity,
  ): Promise<ShippingInfo> {
    const data: GhnFeeDto = {
      from_district_id:
        orderProducts?.[0]?.order.merchantAddress.ward.district.ghnId,
      service_type_id: 2,
      to_district_id: orderProducts?.[0]?.order.ward.district.ghnId,
      to_ward_code: orderProducts?.[0]?.order.ward.ghnCode,
      weight:
        orderProducts.reduce((weight, product) => {
          return weight + product.quantity * product.product.weight;
        }, 0) || 10000,
      height: Number(orderProducts?.[0]?.order.height),
      length: Number(orderProducts?.[0]?.order.length),
      width: Number(orderProducts?.[0]?.order.width),
    };
    try {
      const configGhn = (
        (await this.configService.findOne({ key: ConfigType.SHIPPING_INFO }))
          ?.value as ShippingInfoApiToken[]
      )?.find((x) => x.partner === ShippingPartner.GHN);
      const feeResponse = await lastValueFrom(
        this.httpService.post(
          `${this.nestConfigService.get<string>('GHN_URL')}${
            GHN_API.Calculate_Fee
          }`,
          data,
          {
            headers: {
              Token: configGhn?.token,
              ShopId: configGhn.shopUshareId,
            },
          },
        ),
      );
      return {
        fee: feeResponse?.data.data.total,
        insurance_fee: feeResponse?.data.data.insurance_fee,
        partner: ShippingPartner.GHN,
        id: orderProducts?.[0]?.order.id,
      };
    } catch (e) {
      throw new BadRequestException(
        e.response.data.data.code_message_value,
        e.response.data.data,
      );
    }
  }

  /** Tính phí vận chuyển VTP */
  async calculateVtPFee(
    orderProducts: OrderProductEntity[],
    user: UserEntity,
  ): Promise<ShippingInfo> {
    const data: ViettelPostFeeDto = {
      PRODUCT_WEIGHT: orderProducts.reduce(function (total, prod) {
        return total + Number(prod.product.weight | 0);
      }, 0),
      PRODUCT_HEIGHT: 0,
      PRODUCT_LENGTH: 0,
      PRODUCT_WIDTH: 0,
      PRODUCT_PRICE: orderProducts.reduce(function (total, prod) {
        return total + Number(prod.price | 0);
      }, 0),
      PRODUCT_TYPE: ProductVtPType.Goods,
      MONEY_COLLECTION: 0,
      ORDER_SERVICE_ADD: '',
      ORDER_SERVICE: 'VCN',
      SENDER_PROVINCE:
        orderProducts?.[0].order?.merchantAddress?.ward?.district.province
          ?.vtpId,
      SENDER_DISTRICT:
        orderProducts?.[0].order?.merchantAddress?.ward?.district?.vtpId,
      RECEIVER_PROVINCE:
        orderProducts?.[0].order?.ward?.district?.province.vtpId,
      RECEIVER_DISTRICT: orderProducts?.[0].order?.ward?.district?.vtpId,
      NATIONAL_TYPE: NationalVtPType.Inland,
    };
    try {
      const configVtp = (
        (await this.configService.findOne({ key: ConfigType.SHIPPING_INFO }))
          ?.value as ShippingInfoApiToken[]
      )?.find((x) => x.partner === ShippingPartner.VIETTELPOST);
      if (!configVtp) {
        new NotFoundException('Chưa cấu hình đơn vị vận chuyển Viettel post');
      }
      let feeResponse = await lastValueFrom(
        this.httpService.post(
          `${this.nestConfigService.get<string>('VtP_URL')}${
            VIETTELPOST_API.Calculate_Fee
          }`,
          data,
          {
            headers: {
              token: configVtp.token,
            },
          },
        ),
      );
      if (feeResponse.data?.status === 204) {
        const config = (
          (await this.updateTokenViettelPost())?.value as ShippingInfoApiToken[]
        )?.find((x) => x.partner === ShippingPartner.VIETTELPOST);
        feeResponse = await lastValueFrom(
          this.httpService.post(
            `${this.nestConfigService.get<string>('VtP_URL')}${
              VIETTELPOST_API.Calculate_Fee
            }`,
            data,
            {
              headers: {
                token: config.token,
              },
            },
          ),
        );
      }
      return {
        fee: feeResponse?.data.data.MONEY_TOTAL_OLD,
        insurance_fee: 0,
        partner: ShippingPartner.VIETTELPOST,
        id: orderProducts?.[0]?.order?.id,
        orderProductIds: orderProducts.map(
          (orderProduct) => orderProduct.productId,
        ),
      };
    } catch (e) {
      throw new BadRequestException(
        e.response.data.code_message_value,
        e.response.data,
      );
    }
  }

  /** Thêm kho sang đối tác giao hàng */
  async createStore(
    merchantAddress: MerchantAddressEntity,
    shippingPartner: ShippingPartner,
  ): Promise<number> {
    let result: number = null;
    const shippingInfoConfig = (
      await this.configService.repo.findOne({
        where: {
          key: ConfigType.SHIPPING_INFO,
        },
      })
    )?.value as ShippingInfoApiToken[];
    if (!shippingInfoConfig) {
      throw new NotFoundException('Chưa cấu hình đơn vị vận chuyển');
    }
    switch (shippingPartner) {
      /** GHN */
      /** VIETTELPOST */
      case ShippingPartner.VIETTELPOST:
        const newStoreVtP: CreateStoreVtPDto = {
          NAME: merchantAddress.name,
          ADDRESS: merchantAddress.address,
          PHONE: merchantAddress.tel,
          WARDS_ID: merchantAddress.ward.vtpId,
        };
        let apiToken = shippingInfoConfig.find(
          (x) => x.partner === ShippingPartner.VIETTELPOST,
        ).token;
        try {
          if (!apiToken) {
            apiToken = (
              (await this.updateTokenViettelPost())
                ?.value as ShippingInfoApiToken[]
            )?.find((x) => x.partner === ShippingPartner.VIETTELPOST)?.token;
          }
          let resultVtP = await lastValueFrom(
            this.httpService.post(
              `${
                this.nestConfigService.get<string>('VtP_URL') +
                VIETTELPOST_API.Create_Store
              }`,
              newStoreVtP,
              {
                headers: {
                  token: apiToken,
                },
              },
            ),
          );
          if (resultVtP?.data.status !== 200) {
            apiToken = (
              (await this.updateTokenViettelPost())
                ?.value as ShippingInfoApiToken[]
            )?.find((x) => x.partner === ShippingPartner.VIETTELPOST)?.token;
            resultVtP = await lastValueFrom(
              this.httpService.post(
                `${
                  this.nestConfigService.get<string>('VtP_URL') +
                  VIETTELPOST_API.Create_Store
                }`,
                newStoreVtP,
                {
                  headers: {
                    token: apiToken,
                  },
                },
              ),
            );
          }
          if (resultVtP?.data?.status === 200 && resultVtP.data.data) {
            result = resultVtP.data.data?.[0]?.groupaddressId;
          }
          break;
        } catch {
          result = null;
        }
      /** GHTK */
    }
    return result;
  }

  /** Cập nhật trạng thái đơn hàng từ GHTK */
  async updateGhtkShipment(dto: GhtkWebhookDto) {
    const order = await this.orderRepo.findOne({
      id: Number(dto.partner_id),
    });

    if (!order) throw new NotFoundException();
    if (!order?.shippingInfos.find((x) => x.partner === ShippingPartner.GHTK))
      throw new BadRequestException('Mã vận đơn không đúng');

    const prevStatus = order.shippingInfos.find(
      (x) => x.partner === ShippingPartner.GHTK,
    )?.status;
    const status = updateOrderStatus(dto.status_id, ShippingPartner.GHTK);
    if (status != prevStatus) {
      order.shippingInfos.find(
        (x) => x.partner === ShippingPartner.GHTK,
      ).status = status;

      const history = new OrderHistoryEntity();
      history.orderId = order.id;
      history.status = status;
      history.type = OrderHistoryType.SHIPMENT;

      if ([OrderStatus.SHIPPED, OrderStatus.COMPLETED].includes(status)) {
        order.status = status;
        history.type = OrderHistoryType.ORDER;
      }

      await this.orderHistoryRepo.save(history);

      await this.orderRepo.save(order);
    }
    return Promise.resolve(dto);
  }

  /** Cập nhật trạng thái đơn hàng GHN */
  async updateGhnShipment(dto: GhnWebhookDto) {
    const order = await this.orderRepo.findOne({
      id: parseInt(dto.clientOrderCode),
    });
    if (!order) throw new NotFoundException();
    if (!order?.shippingInfos.find((x) => x.partner === ShippingPartner.GHN))
      throw new BadRequestException('Mã vận đơn không đúng');

    const prevStatus = order.shippingInfos.find(
      (x) => x.partner === ShippingPartner.GHN,
    )?.status;
    const status = updateOrderStatus(dto.status, ShippingPartner.GHN);
    if (status != prevStatus) {
      order.shippingInfos.find(
        (x) => x.partner === ShippingPartner.GHN,
      ).status = status;

      const history = new OrderHistoryEntity();
      history.orderId = order.id;
      history.status = status;
      history.type = OrderHistoryType.SHIPMENT;

      if ([OrderStatus.SHIPPED, OrderStatus.COMPLETED].includes(status)) {
        order.status = status;
        history.type = OrderHistoryType.ORDER;
      }

      await this.orderHistoryRepo.save(history);

      await this.orderRepo.save(order);
    }
  }

  /** Cập nhật trạng thái đơn hàng VIETTELPOST */
  async updateVtpShipment(dto: VtpWebhookDto) {
    const order = await this.orderRepo.findOne({
      id: parseInt(dto.DATA.ORDER_REFERENCE),
    });
    if (!order) throw new NotFoundException();
    if (!order?.shippingInfos.find((x) => x.partner === ShippingPartner.GHN))
      throw new BadRequestException('Mã vận đơn không đúng');

    const prevStatus = order.shippingInfos.find(
      (x) => x.partner === ShippingPartner.GHN,
    )?.status;
    const status = updateOrderStatus(
      dto.DATA.ORDER_STATUS,
      ShippingPartner.VIETTELPOST,
    );
    if (status != prevStatus) {
      order.shippingInfos.find(
        (x) => x.partner === ShippingPartner.VIETTELPOST,
      ).status = status;

      const history = new OrderHistoryEntity();
      history.orderId = order.id;
      history.status = status;
      history.type = OrderHistoryType.SHIPMENT;

      if ([OrderStatus.SHIPPED, OrderStatus.COMPLETED].includes(status)) {
        order.status = status;
        history.type = OrderHistoryType.ORDER;
      }

      await this.orderHistoryRepo.save(history);

      await this.orderRepo.save(order);
    }
  }

  /** Cập nhật trạng thái vận chuyển đơn hàng */
  @Cron(CronExpression.EVERY_5_MINUTES)
  async updateShippingStatus() {
    const orders = await this.orderRepo.find({
      where: {
        status: OrderStatus.SHIPPING,
      },
    });
    const configs = (
      await this.configService.repo.findOne({
        key: ConfigType.SHIPPING_INFO,
      })
    )?.value;
    const ordersUpdate = await Promise.all(
      orders?.map(async (order) => {
        await Promise.all(
          order?.shippingInfos?.map(async (shippingInfo) => {
            switch (shippingInfo?.partner) {
              case ShippingPartner.GHN:
                const configGHN = configs?.find(
                  (config) => config?.partner === ShippingPartner.GHN,
                );
                const ghnDto = {
                  order_code: shippingInfo.partner_id,
                };
                const ghnInfo = await lastValueFrom(
                  this.httpService.post(
                    `${this.nestConfigService.get<string>('GHN_URL')}${
                      GHN_API.Order_Detail
                    }`,
                    ghnDto,
                    {
                      headers: {
                        'Content-Type': 'application/json',
                        Token: configGHN?.token,
                      },
                    },
                  ),
                );
                if (Number(ghnInfo.data?.code) === 200) {
                  shippingInfo.status = updateOrderShippingStatus(
                    ghnInfo.data?.data?.status,
                    ShippingPartner.GHN,
                  );
                  shippingInfo.statusName = updateOrderShippingStatusName(
                    ghnInfo.data?.data?.status,
                    ShippingPartner.GHN,
                  );
                  const status = updateOrderStatusByShippingStatus(
                    order.status,
                    ghnInfo.data?.data?.status,
                    ShippingPartner.GHN,
                  );
                  if (
                    status === OrderStatus.SHIPPED &&
                    status !== order.status
                  ) {
                    order.status = status;
                    await this.orderHistoryRepo.save({
                      status: status,
                      userId: 1,
                      orderId: order.id,
                      type: OrderHistoryType.ORDER,
                    });
                  }
                }
                break;
              case ShippingPartner.GHTK:
                const configGHTK = configs?.find(
                  (config) => config?.partner === ShippingPartner.GHTK,
                );
                const ghtkInfo = await lastValueFrom(
                  this.httpService.get(
                    `${this.nestConfigService.get<string>('GHTK_URL')}${
                      GHTK_API.Order_Status
                    }/${shippingInfo.partner_id}`,
                    {
                      headers: {
                        Token: configGHTK?.token,
                      },
                    },
                  ),
                );
                if (ghtkInfo.data?.success) {
                  shippingInfo.status = updateOrderShippingStatus(
                    Number(ghtkInfo.data?.order?.status),
                    ShippingPartner.GHTK,
                  );
                  shippingInfo.statusName = updateOrderShippingStatusName(
                    Number(ghtkInfo.data?.order?.status),
                    ShippingPartner.GHTK,
                  );
                  const status = updateOrderStatusByShippingStatus(
                    order.status,
                    Number(ghtkInfo.data?.order?.status),
                    ShippingPartner.GHTK,
                  );
                  if (
                    status === OrderStatus.SHIPPED &&
                    status !== order.status
                  ) {
                    order.status = status;
                    await this.orderHistoryRepo.save({
                      status: status,
                      userId: 1,
                      orderId: order.id,
                      type: OrderHistoryType.ORDER,
                    });
                  }
                }
                break;
            }
          }),
        );
        return order;
      }),
    );
    await this.orderRepo.save(ordersUpdate);
  }

  async syncGhnProvince() {
    const ghnProvinces = await lastValueFrom(
      this.httpService.get(
        `${this.nestConfigService.get<string>(
          'GHN_URL',
        )}/shiip/public-api/master-data/province`,
        {
          headers: {
            Token: this.ghnToken,
            ShopId: this.ghnShopId,
          },
        },
      ),
    );
    const provinces = await this.provinceRepo.find();

    Promise.all(
      provinces.map(async (province) => {
        const existGhnProvince = ghnProvinces.data.data.find(
          (ghnProvince) => ghnProvince.ProvinceName === province.name,
        );
        if (existGhnProvince) {
          province.ghnId = existGhnProvince.ProvinceID;
          province.nameExtensions = existGhnProvince.NameExtension;
          await this.provinceRepo.save(province);
        }
      }),
    );
  }

  stripVN(input) {
    return input
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .replace(/đ/g, 'd')
      .replace(/Đ/g, 'D')
      .replace(/'/g, '')
      .toLowerCase();
    // .replace(/ /g, '')
  }

  async syncGhnDistrict() {
    const districts = await this.districtRepo.query(
      `SELECT "provinceId", "ghnId" FROM district WHERE "ghnId" IS NULL GROUP BY "provinceId", "ghnId"`,
    );
    return Promise.all(
      districts.map(async (district) => {
        const tmpProvince = await this.provinceRepo.findOne({
          id: district.provinceId,
        });
        const ghnDistricts = await lastValueFrom(
          this.httpService.get(
            `${this.nestConfigService.get<string>(
              'GHN_URL',
            )}/shiip/public-api/master-data/district`,
            {
              headers: {
                Token: this.ghnToken,
                ShopId: this.ghnShopId,
              },
              data: {
                province_Id: tmpProvince.ghnId,
              },
            },
          ),
        );
        const localDistricts = await this.districtRepo.find({
          provinceId: district.provinceId,
          ghnId: null,
        });
        ghnDistricts?.data?.data.map((ghnDistrict) => {
          localDistricts.map(async (district) => {
            console.log(this.stripVN(district.name));
            if (!district.ghnId)
              if (
                ghnDistrict?.NameExtension &&
                ghnDistrict?.NameExtension.includes(this.stripVN(district.name))
              ) {
                district.ghnId = ghnDistrict.DistrictID;
                await this.districtRepo.save(district);
              }
          });
        });
      }),
    );
  }

  async syncGhnWard() {
    // const wards = await this.districtRepo.query(
    //   `SELECT "districtId", "ghnCode" FROM ward WHERE id = 1 GROUP BY "districtId", "ghnCode"`,
    // );
    // const wards = await this.districtRepo.query(
    //   `SELECT "districtId", "ghnCode" FROM ward WHERE "ghnCode"  IS NULL  GROUP BY "districtId", "ghnCode"`,
    // );
    // return Promise.all(
    //   wards.map(async (ward) => {
    //     const tmpDistrict = await this.districtRepo.findOne({
    //       id: ward.districtId,
    //     });
    //     const ghnWards = await lastValueFrom(
    //       this.httpService.get(
    //         `${this.nestConfigService.get<string>(
    //           'GHN_URL',
    //         )}/shiip/public-api/master-data/ward?district_id`,
    //         {
    //           headers: {
    //             Token: this.ghnToken,
    //             ShopId: this.ghnShopId,
    //           },
    //           data: {
    //             district_id: tmpDistrict.ghnId,
    //           },
    //         },
    //       ),
    //     );
    //     const localWards = await this.wardRepo.find({
    //       districtId: ward.districtId,
    //       ghnCode: null,
    //     });
    //     Promise.all(
    //       ghnWards?.data?.data.map(async (ghnWard) => {
    //         Promise.all(
    //           localWards.map(async (ward) => {
    //             if (!ward.ghnCode) {
    //               if (ghnWard?.NameExtension) {
    //                 let exist = false;
    //                 ghnWard?.NameExtension.map((name: string) => {
    //                   console.log(name);
    //                   if (
    //                     name.toLowerCase() ===
    //                     this.stripVN(ward.name.trim()).toLowerCase()
    //                   )
    //                     exist = true;
    //                 });
    //
    //                 if (exist) {
    //                   console.log(ghnWard.WardCode);
    //                   ward.ghnCode = ghnWard.WardCode;
    //                   await this.wardRepo.save(ward);
    //                 }
    //               }
    //             }
    //           }),
    //         ).then();
    //       }),
    //     ).then();
    //   }),
    // );
    // const districts = await this.districtRepo.find();
    // return Promise.all(
    //   districts.map(async (district) => {
    //     const ghnWards = await lastValueFrom(
    //       this.httpService.get(
    //         `${this.nestConfigService.get<string>(
    //           'GHN_URL',
    //         )}/shiip/public-api/master-data/ward?district_id`,
    //         {
    //           headers: {
    //             Token: this.ghnToken,
    //             ShopId: this.ghnShopId,
    //           },
    //           data: {
    //             district_id: district.ghnId,
    //           },
    //         },
    //       ),
    //     );
    //     const localWards = await this.wardRepo.find({
    //       districtId: district.id,
    //       ghnCode: null,
    //     });
    //     if (ghnWards?.data?.data) {
    //       ghnWards?.data?.data?.map((ghnWard) => {
    //         localWards.map(async (ward) => {
    //           if (!ward.ghnCode)
    //             if (
    //               ghnWard?.NameExtension &&
    //               ghnWard?.NameExtension.includes(this.stripVN(ward.name))
    //             ) {
    //               ward.ghnCode = ghnWard.WardCode;
    //               await this.wardRepo.save(ward);
    //             }
    //         });
    //       });
    //     } else {
    //       console.log(district);
    //     }
    //   }),
    // );
  }

  async syncEShopProvince() {
    const provinces = await this.provinceRepo.find({
      where: {
        eShopId: IsNull(),
      },
    });
    const eShopProvinces = await lastValueFrom(
      this.httpService.get(
        `https://graphapi.mshopkeeper.vn/g1/api/v1/locations/bykindandparentid?kind=1&parentId=VN`,
        {
          headers: {
            Authorization:
              'Bearer cWUlEiFvSrnKwm9F6BES15uZmqV1YHSS6ZQuQZkZUiuaqKqdUlrD9f8NhL_ip2_u2um0QE-Aal9ACHlKFmpvcAtJGOdSrM1kgL2uoNFzrNxaV2WIFKdVA9b4HMC7uyU_TybNlREnvTN7qM4VOA0QbRxWDrnn05E6FmIe2GpTeuV2iFP5nSMYxd2qY1vNXvH1PUQRAbvb_j-qnLGVjUc0dPYNxsdI-QV0fNSuNIDR81jeh7a9nqqrjNcpBTfi1QKlICT4JobG6_EAwpJSuPpIvQ',
            Token: this.ghnToken,
            CompanyCode: 'ukgtech',
          },
        },
      ),
    );
    return Promise.all(
      provinces.map(async (province) => {
        eShopProvinces?.data?.Data.map((eShopProvince) => {
          if (
            this.stripVN(eShopProvince.Name) === this.stripVN(province.name)
          ) {
            province.eShopId = eShopProvince.Id;
          }
        });
        return await this.provinceRepo.save(province);
      }),
    );
  }

  async syncEShopDistrict() {
    const districts = await this.districtRepo.query(
      `SELECT "provinceId", "eShopId" FROM district WHERE "eShopId" IS NULL AND "deletedAt" IS NULL GROUP BY "provinceId", "eShopId" ORDER BY "provinceId" ASC LIMIT 1`,
    );
    return Promise.all(
      districts.map(async (district) => {
        const tmpProvince = await this.provinceRepo.findOne({
          id: district.provinceId,
        });

        try {
          const eShopDistricts = await lastValueFrom(
            this.httpService.get(
              `https://graphapi.mshopkeeper.vn/g1/api/v1/locations/bykindandparentid?kind=2&parentId=${tmpProvince.eShopId}`,
              {
                headers: {
                  Authorization:
                    'Bearer cWUlEiFvSrnKwm9F6BES15uZmqV1YHSS6ZQuQZkZUiuaqKqdUlrD9f8NhL_ip2_u2um0QE-Aal9ACHlKFmpvcAtJGOdSrM1kgL2uoNFzrNxaV2WIFKdVA9b4HMC7uyU_TybNlREnvTN7qM4VOA0QbRxWDrnn05E6FmIe2GpTeuV2iFP5nSMYxd2qY1vNXvH1PUQRAbvb_j-qnLGVjUc0dPYNxsdI-QV0fNSuNIDR81jeh7a9nqqrjNcpBTfi1QKlICT4JobG6_EAwpJSuPpIvQ',
                  Token: this.ghnToken,
                  CompanyCode: 'ukgtech',
                },
              },
            ),
          );
          const localDistricts = await this.districtRepo.find({
            provinceId: district.provinceId,
            eShopId: IsNull(),
          });
          console.log(localDistricts);
          if (eShopDistricts?.data?.Data) {
            console.log(eShopDistricts?.data?.Data);
            localDistricts.map(async (district) => {
              eShopDistricts?.data?.Data?.map(async (eShopDistrict) => {
                const eShopName = eShopDistrict?.Name.toLowerCase()
                  .replaceAll('quận ', '')
                  .replaceAll('huyện ', '')
                  .replaceAll('thành phố ', '')
                  .replaceAll('thị xã ', '');
                if (this.stripVN(eShopName) === this.stripVN(district.name)) {
                  console.log(district.name);
                  district.eShopId = eShopDistrict.Id;
                  await this.districtRepo.save(district);
                }
              });
            });
          } else {
            console.log(eShopDistricts?.data);
          }
        } catch (e) {
          console.log(e);
        }
      }),
    );
  }

  async syncEShopWard(district) {
    console.log('Start');
    const wards = await this.wardRepo.query(
      `SELECT "districtId", "eShopId" FROM ward WHERE "eShopId" IS NULL AND "deletedAt" IS NULL AND "districtId" > ${district} GROUP BY "districtId", "eShopId" ORDER BY "districtId" ASC LIMIT 5`,
    );
    return Promise.all(
      wards.map(async (ward) => {
        const tmpDistrict = await this.districtRepo.findOne({
          id: ward.districtId,
        });
        if (tmpDistrict) {
          console.log(ward.districtId);
          try {
            const eShopWards = await lastValueFrom(
              this.httpService.get(
                `https://graphapi.mshopkeeper.vn/g1/api/v1/locations/bykindandparentid?kind=3&parentId=${tmpDistrict.eShopId}`,
                {
                  headers: {
                    Authorization:
                      'Bearer cWUlEiFvSrnKwm9F6BES15uZmqV1YHSS6ZQuQZkZUiuaqKqdUlrD9f8NhL_ip2_u2um0QE-Aal9ACHlKFmpvcAtJGOdSrM1kgL2uoNFzrNxaV2WIFKdVA9b4HMC7uyU_TybNlREnvTN7qM4VOA0QbRxWDrnn05E6FmIe2GpTeuV2iFP5nSMYxd2qY1vNXvH1PUQRAbvb_j-qnLGVjUc0dPYNxsdI-QV0fNSuNIDR81jeh7a9nqqrjNcpBTfi1QKlICT4JobG6_EAwpJSuPpIvQ',
                    CompanyCode: 'ukgtech',
                  },
                },
              ),
            );
            const localWards = await this.wardRepo.find({
              districtId: ward.districtId,
              eShopId: IsNull(),
            });
            if (eShopWards?.data?.Data) {
              return Promise.all(
                localWards.map(async (localWard) => {
                  return Promise.all(
                    eShopWards?.data?.Data?.map(async (eShopWard) => {
                      const eShopName = eShopWard?.Name.toLowerCase()
                        .replaceAll('xã ', '')
                        .replaceAll('phường ', '')
                        // .replaceAll('thị xã ', '')
                        .replaceAll('thị trấn ', '');
                      if (
                        this.stripVN(eShopName).trim() ===
                        this.stripVN(localWard.name).trim()
                      ) {
                        console.log(localWard.name);
                        localWard.eShopId = eShopWard.Id;
                        await this.wardRepo.save(localWard);
                      }
                    }),
                  );
                }),
              );
            }
          } catch (e) {
            console.log('Lỗi eShop');
            console.log(e);
          }
        }
      }),
    );
  }

  async syncVtpProvince() {
    const vtpProvinces = await lastValueFrom(
      this.httpService.get(
        `${this.nestConfigService.get<string>(
          'VtP_URL',
        )}/v2/categories/listProvinceById?provinceId=-1`,
      ),
    );
    const provinces = await this.provinceRepo.find();

    Promise.all(
      provinces.map(async (province) => {
        const existVtpProvince = vtpProvinces.data.data.find(
          (vtpProvince) => vtpProvince.PROVINCE_NAME === province.name,
        );
        if (existVtpProvince) {
          province.vtpId = existVtpProvince.PROVINCE_ID;
          await this.provinceRepo.save(province);
        }
      }),
    );
  }

  async syncVtpDistrict() {
    const districts = await this.districtRepo.find();
    const vtpDistricts = await lastValueFrom(
      this.httpService.get(
        `${this.nestConfigService.get<string>(
          'VtP_URL',
        )}/v2/categories/listDistrict?provinceId=-1`,
      ),
    );
    console.log('vtpDistricts', vtpDistricts);
    Promise.all(
      districts.map(async (district) => {
        const exitsVtpDistrict = vtpDistricts.data.data.find(
          (vtpDistrict) =>
            vtpDistrict.DISTRICT_NAME ===
              ('Quận ' + district.name).toUpperCase() ||
            vtpDistrict.DISTRICT_NAME ===
              ('Huyện ' + district.name).toUpperCase(),
        );
        if (exitsVtpDistrict) {
          district.vtpId = exitsVtpDistrict.DISTRICT_ID;
          await this.districtRepo.save(district);
        }
      }),
    );
  }

  async syncVtpWard() {
    const wards = await this.wardRepo.find();
    const vtpWards = await lastValueFrom(
      this.httpService.get(
        `${this.nestConfigService.get<string>(
          'VtP_URL',
        )}/v2/categories/listWards?districtId=-1`,
      ),
    );
    Promise.all(
      wards.map(async (ward) => {
        const exitsVtpWard = vtpWards.data.data.find(
          (vtpWard) =>
            vtpWard.WARDS_NAME === ('Xã ' + ward.name).toUpperCase() ||
            vtpWard.WARDS_NAME === ('Thị trấn ' + ward.name).toUpperCase() ||
            vtpWard.WARDS_NAME === ('Phường ' + ward.name).toUpperCase(),
        );
        if (exitsVtpWard) {
          ward.vtpId = exitsVtpWard.WARDS_ID;
          await this.wardRepo.save(ward);
        }
      }),
    );
  }
}

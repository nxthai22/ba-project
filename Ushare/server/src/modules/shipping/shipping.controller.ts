import { ApiTags } from '@nestjs/swagger';
import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { GhtkWebhookDto } from 'modules/shipping/dto/ghtk-webhook.dto';
import { ShippingService } from 'modules/shipping/shipping.service';
import { VtpWebhookDto } from './dto/viettelpost-webhook.dto';
import { GhnWebhookDto } from './dto/ghn-webhook.dto';

@ApiTags('Shipping')
@Controller('shipping')
export class ShippingController {
  constructor(public readonly service: ShippingService) {}

  @Post('/ghtk/update-shipment')
  @HttpCode(200)
  async updateGhtkShipment(@Body() dto: GhtkWebhookDto) {
    return await this.service.updateGhtkShipment(dto);
  }

  @Post('/ghn/update-shipment')
  @HttpCode(200)
  async updateGhnShipment(@Body() dto: GhnWebhookDto) {
    return await this.service.updateGhnShipment(dto);
  }

  @Post('/vtp/update-shipment')
  @HttpCode(200)
  async updateVtpShipment(@Body() dto: VtpWebhookDto) {
    return await this.service.updateVtpShipment(dto);
  }

  //
  // @Get('/ghn/syncGhnProvince')
  // @HttpCode(200)
  // async syncGhnProvince() {
  //   return this.service.syncGhnProvince();
  // }
  //
  // @Get('/ghn/syncGhnDistrict')
  // @HttpCode(200)
  // async syncGhnDistrict() {
  //   return this.service.syncGhnDistrict();
  // }
  //
  // @Get('/ghn/syncGhnWard')
  // @HttpCode(200)
  // async syncGhnWard() {
  //   return this.service.syncGhnWard();
  // }

  // @Get('/ghn/sync-eshop-province')
  // @HttpCode(200)
  // async syncGhnProvince() {
  //   return this.service.syncEShopProvince();
  // }
  //
  // @Get('/ghn/sync-eshop-district')
  // @HttpCode(200)
  // async syncGhnDistrict() {
  //   return this.service.syncEShopDistrict();
  // }
  //
  // @Get('/ghn/sync-eshop-ward/:district')
  // @HttpCode(200)
  // @ApiQuery({
  //   name:'district'
  // })
  // async syncGhnWard(@Query('district') district) {
  //   return this.service.syncEShopWard(district);
  // }
  // @Get('/viettelpost/sync-viettelpost-ward')
  // @HttpCode(200)
  // async syncVtpWard() {
  //   return this.service.syncVtpWard();
  // }
  // @Get('/viettelpost/sync-viettelpost-district')
  // @HttpCode(200)
  // async syncVtpDistrict() {
  //   return this.service.syncVtpDistrict();
  // }
  // @Get('/viettelpost/sync-viettelpost-province')
  // @HttpCode(200)
  // async syncVtpProvince() {
  //   return this.service.syncVtpProvince();
  // }
}

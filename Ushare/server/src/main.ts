process
  .on('unhandledRejection', (reason, p) => {
    console.error(reason, 'Unhandled Rejection at Promise', p);
  })
  .on('uncaughtException', (err) => {
    console.error(err, 'Uncaught Exception thrown');
    process.exit(1);
  });

import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { CrudConfigService } from '@nestjsx/crud';
import { setup } from './core/setup';

CrudConfigService.load({
  query: {
    limit: 10,
    maxLimit: 1000,
    cache: 2000,
    alwaysPaginate: true,
  },
  routes: {
    exclude: ['deleteOneBase'],
  },
});

import { AppModule } from 'app.module';
import { utilities, WinstonModule } from 'nest-winston';
import * as winston from 'winston';
import 'winston-daily-rotate-file';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
    {
      // logger: WinstonModule.createLogger({
      //   level: 'info',
      //   transports: [
      //     new winston.transports.Console({
      //       format: winston.format.combine(
      //         winston.format.timestamp(),
      //         winston.format.ms(),
      //         utilities.format.nestLike('Ushare', { prettyPrint: true }),
      //       ),
      //     }),
      //     new winston.transports.DailyRotateFile({
      //       filename: 'application-%DATE%.log',
      //       datePattern: 'YYYY-MM-DD-HH',
      //       dirname: 'logs',
      //       zippedArchive: true,
      //       maxSize: '20m',
      //       maxFiles: '3d',
      //     }),
      //   ],
      // }),
      logger: ['error'],
    },
  );

  await setup(app);
  await app.listen(process.env.PORT || 4000, '0.0.0.0');
  console.log(`Application is running on: ${await app.getUrl()}`);
}

bootstrap();

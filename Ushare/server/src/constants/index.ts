export const ACCOUNTANT_ROLE_ID = 4;
export const CLIENT_DOMAIN =
  process.env.NODE_ENV === 'development'
    ? 'https://dev.ushare.com.vn'
    : 'https://ushare.com.vn';

import { ApiProperty } from '@nestjs/swagger';
import { hash } from 'bcrypt';
import {
  IsNumber,
  IsNumberString,
  IsOptional,
  Matches,
  MinLength,
} from 'class-validator';
import { BaseEntity } from 'entities/base.entity';
import { MerchantEntity } from 'entities/merchant.entity';
import { ProductEntity } from 'entities/product.entity';
import { RoleEntity, RoleType } from 'entities/role.entity';
import { WardEntity } from 'entities/ward.entity';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  Tree,
  TreeChildren,
  TreeParent,
} from 'typeorm';
import { CommissionValueType } from './config.entity';
import { CustomerEntity } from './customer.entity';
import { RevenueStatisticEntity } from './revenue-statistic.entity';
import { UserTransactionEntity } from './user-transaction.entity';
import { UserWalletHistoryEntity } from './user-wallet-history.entity';
import { UserWalletEntity } from './user-wallet.entity';
import { VoucherHistoryEntity } from 'entities/voucher-history.entity';
import { DeviceTokenEntity } from 'entities/device-token.entity';
import { BankEntity } from './bank.entity';
import { CartProductEntity } from 'entities/cart-product.entity';

export enum UserStatus {
  ACTIVE = 'active',
  INACTIVE = 'inactive',
  BANNED = 'banned',
  DELETED = 'deleted',
}

export enum UserGender {
  MALE = 'male',
  FEMALE = 'female',
  OTHER = 'other',
}

@Entity('user')
@Tree('materialized-path')
export class UserEntity extends BaseEntity {
  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Email',
    required: false,
  })
  email?: string;

  @Column()
  @IsOptional()
  @ApiProperty({
    description: 'Mật khẩu',
  })
  @MinLength(6, { message: 'Mật khẩu cần có độ dài >= 6 ký tự!' })
  password?: string;

  @IsOptional()
  @Column({
    default: [],
    type: 'jsonb',
    nullable: true,
  })
  @ApiProperty({
    description: 'Lưu các mật khẩu cũ',
    required: false,
  })
  passwordHistory: string[];

  @Column()
  @ApiProperty({
    description: 'Tên',
  })
  fullName: string;

  @ApiProperty({
    description: 'Số điện thoại',
  })
  // @Length(10, 12)
  @IsNumberString()
  @Column({
    unique: true,
  })
  // @Matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g, {
  //   message: 'Số điện thoại không đúng định dạng',
  // })
  tel?: string;

  @Column({
    type: 'timestamp with time zone',
    nullable: true,
  })
  @ApiProperty({
    description: 'Đăng nhập cuối',
    required: false,
  })
  lastLogin?: Date;
  //
  // @Column({
  //   nullable: true,
  // })
  // @ApiProperty({
  //   description: 'Link Facebook',
  //   required: false,
  // })
  // facebook?: string;

  @Column({
    nullable: true,
    default: 8,
  })
  @ApiProperty({
    description: 'Role ID',
    required: false,
  })
  roleId?: number;

  @ManyToOne(() => RoleEntity, (role) => role.users, {
    nullable: true,
  })
  role: RoleEntity;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'ID cha',
    required: false,
  })
  parentId?: number;

  @ManyToOne(() => UserEntity, (parent) => parent.id, {
    nullable: true,
  })
  parent?: UserEntity;

  // @OneToMany(() => UserEntity, (child) => child.parent, {
  //   nullable: true,
  // })
  // @ApiProperty({
  //   description: 'ID con',
  // })
  // childs?: UserEntity[];

  // @Column({
  //   nullable: true,
  // })
  // @ApiProperty({
  //   description: 'ID người giới thiệu',
  //   required: false,
  // })
  // referralId?: number;
  //
  // // @ApiProperty({
  // //   description: 'Người giới thiệu',
  // //   type: UserEntity,
  // // })
  // @ManyToOne(() => UserEntity, (referral) => referral.id, {
  //   nullable: true,
  // })
  // referral?: UserEntity;

  @ApiProperty({
    description: 'Danh sách cấp dưới ',
    required: false,
    type: [UserEntity],
  })
  @TreeChildren()
  referralChildren?: UserEntity[];

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Id cấp trên',
    required: false,
    type: Number,
  })
  referralParentId?: number;

  // @ApiProperty({
  //   description: 'Cấp trên',
  //   required: false,
  //   type: UserEntity,
  // })
  @TreeParent()
  referralParent?: UserEntity;

  @Column({
    nullable: true,
    select: false,
  })
  @ApiProperty({
    description: 'Session sau khi gửi xác nhận SĐT với Firebase',
    required: false,
  })
  sessionVerifyCode?: string;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'ID Nhà cung cấp',
    required: false,
  })
  merchantId?: number;

  // @ApiProperty({
  //   description: 'Nhà cung cấp',
  //   required: false,
  // type: MerchantEntity,
  // })
  // @ManyToOne(() => MerchantEntity, (merchant) => merchant.id)
  // merchant?: MerchantEntity;

  @OneToMany(() => ProductEntity, (product) => product.id)
  products?: ProductEntity;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Số CMTND/CCCD',
    required: false,
  })
  nationalId?: string;

  @Column({
    nullable: true,
    type: 'timestamp with time zone',
  })
  @ApiProperty({
    description: 'Số CMTND/CCCD',
    required: false,
  })
  nationalIssueDate?: Date;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Nơi cấp',
    required: false,
  })
  nationalIssueBy?: string;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Địa chỉ',
    required: false,
  })
  address?: string;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Phường/Xã',
    required: false,
  })
  wardId?: number;

  @ManyToOne(() => WardEntity, (ward) => ward.id)
  ward?: WardEntity;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Số tài khoản',
    required: false,
  })
  bankNumber?: string;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Chủ tài khoản',
    required: false,
  })
  bankAccountName?: string;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Chi nhánh ngân hàng',
    required: false,
  })
  bankBranch?: string;

  @Column({
    nullable: true,
    type: 'timestamp with time zone',
  })
  @ApiProperty({
    description: 'Ngày sinh',
    required: false,
  })
  dob?: Date;

  @Column({
    type: 'enum',
    enum: UserGender,
    default: UserGender.OTHER,
  })
  @ApiProperty({
    description: 'Giới tính',
    enum: UserGender,
  })
  gender: UserGender;

  @Column({
    type: 'enum',
    enum: UserStatus,
    default: UserStatus.INACTIVE,
  })
  @ApiProperty({
    description: 'Trạng thái',
    enum: UserStatus,
  })
  status: UserStatus;

  @Column({
    type: 'timestamp with time zone',
    nullable: true,
  })
  @ApiProperty({
    description:
      'Ngày bắt đầu tính hoa hồng cho người giới thiệu (ngày hoàn thành đơn đầu tiên)',
    required: false,
  })
  referralBonusStart?: Date;

  @Column({
    type: 'timestamp with time zone',
    nullable: true,
  })
  @ApiProperty({
    description: 'Thời hạn kết thúc nhận Bonus cho người giới thiệu',
    required: false,
  })
  referralBonusExpire?: Date;

  @IsOptional()
  @IsNumber()
  @Column({
    nullable: true,
    type: 'float4',
  })
  @ApiProperty({
    description: 'Giá trị hoa hồng/thưởng được áp dụng',
    type: Number,
    required: false,
  })
  valueBonus?: number;

  @Column({
    nullable: true,
    enum: CommissionValueType,
  })
  @ApiProperty({
    description: 'Loại giá trị hoa hồng/thưởng được áp dụng',
    enum: CommissionValueType,
  })
  valueBonusType?: CommissionValueType;

  @OneToMany(() => RevenueStatisticEntity, (reStatistic) => reStatistic.userId)
  revenueStatistic: RevenueStatisticEntity[];

  @ApiProperty({
    description: 'Ví của Thành viên',
    type: [UserWalletEntity],
  })
  @OneToMany(() => UserWalletEntity, (wallet) => wallet.user)
  wallets: UserWalletEntity[];

  @OneToMany(
    () => UserWalletHistoryEntity,
    (walletHistory) => walletHistory.user,
  )
  walletHistories: UserWalletHistoryEntity[];

  @OneToMany(
    () => UserTransactionEntity,
    (walletTransaction) => walletTransaction.user,
  )
  walletTransactions: UserTransactionEntity[];

  @OneToMany(() => CustomerEntity, (customer) => customer.user)
  customers: CustomerEntity[];

  // @OneToMany(() => UserMerchantEntity, ({ leader }) => leader)
  // inferiors: UserMerchantEntity[];

  @ApiProperty({
    description: 'Danh sách cấp trên',
    type: [UserEntity],
  })
  @ManyToMany((type) => UserEntity, (u) => u.leaders)
  @JoinTable({
    name: 'user_leader',
    joinColumn: {
      name: 'userId',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'leaderId',
      referencedColumnName: 'id',
    },
  })
  leaders: UserEntity[];

  @ApiProperty({
    description: 'Danh sách NCC',
    type: [MerchantEntity],
  })
  @ManyToMany((type) => MerchantEntity, (u) => u.users)
  @JoinTable({
    name: 'user_merchant',
    joinColumn: {
      name: 'userId',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'merchantId',
      referencedColumnName: 'id',
    },
  })
  merchants: MerchantEntity[];

  mpath?: string;

  @OneToMany(() => VoucherHistoryEntity, (history) => history.voucher)
  voucherHistories: VoucherHistoryEntity[];

  @ManyToOne(() => DeviceTokenEntity, ({ user }) => user)
  tokens: DeviceTokenEntity[];

  @IsOptional()
  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Id ngân hàng',
    type: Number,
    required: false,
  })
  bankId?: number;

  @ManyToOne(() => BankEntity, (bank) => bank.users)
  bank: BankEntity;

  @OneToMany(() => CartProductEntity, ({ user }) => user)
  cartProducts: CartProductEntity[];

  @ApiProperty({ description: 'Ảnh đại diện', required: false })
  @Column({ nullable: true })
  avatar?: string;

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    if (this.password) this.password = await hash(this.password, 10);
  }

  isUshareRole(): boolean {
    return [RoleType.ADMIN, RoleType.ACCOUNTANT].indexOf(this.role.type) !== -1;
  }

  isMerchantRole(): boolean {
    return (
      [RoleType.MERCHANT, RoleType.MERCHANT_ACCOUNTANT].indexOf(
        this.role.type,
      ) !== -1
    );
  }

  /**
   * Kiểm tra user là admin ushare
   * @returns boolean
   */
  isAdmin(): boolean {
    return this.role.type === RoleType.ADMIN;
  }

  /**
   * Kiểm tra user là nhà cung cấp/đối tác
   * @returns boolean
   */
  isMerchant(): boolean {
    return this.role.type === RoleType.MERCHANT;
  }

  /**
   * Kiểm tra user là trưởng nhóm cộng tác viên
   * @returns boolean
   */
  isLeader(): boolean {
    return this.role.type === RoleType.LEADER;
  }

  /**
   * Kiểm tra user là cộng tác viên
   * @returns boolean
   */
  isPartner(): boolean {
    return this.role.type === RoleType.PARTNER;
  }
}

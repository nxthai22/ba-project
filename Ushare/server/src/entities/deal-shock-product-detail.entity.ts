import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { DealShockProductEntity } from './deal-shock-product.entity';
import { ApiProperty } from '@nestjs/swagger';
import { ProductEntity } from './product.entity';

export enum DiscountValueType {
  AMOUNT = 'amount', // Tính theo số lượng
  PERCENT = 'percent', // Tính theo phần trăm
}

// @Entity('deal-shock-product-detail')
export class DealShockProductDetailEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    required: true,
    description: 'Id sản phẩm chính tham gia Deal sốc',
  })
  dealShockProductId: number;

  @ApiProperty({ description: 'Thông tin sản phẩm chính' })
  @ManyToOne(
    () => DealShockProductEntity,
    (prod) => prod.dealShockProductDetails,
  )
  dealShockProduct: DealShockProductEntity;

  @Column()
  @ApiProperty({
    required: true,
    description: 'Id sản phẩm mua kèm sản phẩm chính',
  })
  productId: number;

  @ApiProperty({ description: 'Thông tin sản phẩm mua kèm' })
  @ManyToOne(() => ProductEntity, (prod) => prod.id)
  product: ProductEntity;

  @Column()
  @ApiProperty({
    required: false,
    description: 'Giới hạn đặt hàng',
  })
  limit?:number;

  @Column()
  @ApiProperty({
    description: 'Mức giảm giá',
  })
  discountValue?: number;

  @Column()
  @ApiProperty({
    description: 'Loại giảm giá',
    type: 'enum',
    enum: DiscountValueType,
  })
  discountValueType?: DiscountValueType;
}
import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  Unique,
} from 'typeorm';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ActiveStatus, ShippingPartner } from 'enums';
import { ConfigEntity } from './config.entity';
import { BaseEntity } from 'entities/base.entity';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { RevenueStatisticEntity } from './revenue-statistic.entity';
import { WardEntity } from './ward.entity';
import { UserEntity } from 'entities/user.entity';
import { BankEntity } from './bank.entity';

export enum MerchantCommissionType {
  AMOUNT = 'amount',
  PERCENTAGE = 'percentage',
}

export class MerchantShippingApiToken {
  @ApiProperty({
    description: 'Đối tác',
    enum: ShippingPartner,
  })
  partner: ShippingPartner;

  @ApiProperty({
    description: 'Token',
    required: false,
  })
  token?: string;

  @ApiProperty({
    description: 'Shop ID',
    required: false,
  })
  shopId?: string;

  @ApiProperty({
    description: 'Shop Address ID',
    required: false,
  })
  shopAddressId?: string;

  @ApiProperty({
    description: 'Tên đăng nhập',
    required: false,
  })
  username?: string;

  @ApiProperty({
    description: 'Mật khẩu',
    required: false,
  })
  password?: string;

  @ApiProperty({
    description: 'API Key',
    required: false,
  })
  apiKey?: string;

  @ApiProperty({
    description: 'API Secret',
    required: false,
  })
  apiSecret?: string;

  @ApiProperty({
    description: 'Domain',
    required: false,
  })
  domain?: string;
}

@Entity('merchant')
@Unique(['name'])
@Unique(['code'])
export class MerchantEntity extends BaseEntity {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'Tên đối tác', required: true })
  @Column()
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'Mã nhà cung cấp', required: true })
  @Column({
    nullable: true,
  })
  code: string;

  @ApiProperty({ description: 'Địa chỉ', required: false })
  @Column({
    nullable: true,
  })
  address: string;

  @ApiProperty({ description: 'Số điện thoại', required: false })
  @Column({
    nullable: true,
  })
  tel: string;

  @ApiProperty({ description: 'ID của Xã/Phường', required: false })
  @Column({
    nullable: true,
  })
  wardId: number;

  @ManyToOne(() => WardEntity, ({ id }) => id)
  ward?: WardEntity;

  @ApiProperty({
    description: 'Trạng thái',
    enum: [ActiveStatus.INACTIVE, ActiveStatus.ACTIVE],
    required: false,
  })
  @Column({
    type: 'enum',
    enum: ActiveStatus,
    default: ActiveStatus.ACTIVE,
  })
  status: ActiveStatus;

  @ApiProperty({
    description: 'Id ngân hàng',
    required: false,
  })
  @Column({
    nullable: true,
  })
  bankId?: number;

  @ManyToOne(() => BankEntity, (bank) => bank.merchants)
  bank?: BankEntity;

  @ApiProperty({
    description: 'Chủ TK ngân hàng',
    required: false,
  })
  @Column({
    nullable: true,
  })
  bankAccount?: string;

  @ApiProperty({
    description: 'STK ngân hàng',
    required: false,
  })
  @Column({
    nullable: true,
  })
  bankNumber?: string;

  @ApiProperty({
    description: 'Chi nhánh ngân hàng',
    required: false,
  })
  @Column({
    nullable: true,
  })
  bankBranch?: string;

  @IsString()
  @ApiProperty({ description: 'Website', required: false })
  @Column({ nullable: true })
  website?: string;

  @ApiProperty({ description: 'Hoa hồng', required: false })
  @OneToMany(() => ConfigEntity, ({ merchantId }) => merchantId)
  commission?: ConfigEntity[];

  @ApiProperty({ description: 'Kho hàng', required: false })
  @OneToMany((type) => MerchantAddressEntity, ({ merchant }) => merchant)
  addresses?: MerchantAddressEntity[];

  @ApiProperty({ description: 'Thống kê doanh thu', required: false })
  @OneToMany(() => RevenueStatisticEntity, ({ merchantId }) => merchantId)
  revenueStatistic?: RevenueStatisticEntity[];

  @ManyToMany((type) => UserEntity, (c) => c.merchants)
  users?: UserEntity[];
}

import { Column, Entity, OneToMany, Unique } from "typeorm";
import { BaseEntity } from 'entities/base.entity';
import { UserEntity } from './user.entity';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export enum RoleType {
  ADMIN = 'admin', // Quyền admin
  MERCHANT = 'merchant', // Quyền NCC,
  LEADER = 'leader', // Quyền Trưởng nhóm
  OTHER = 'other', // Quyền khác
  PARTNER = 'partner', // Quyền CTV
  ACCOUNTANT = 'accountant', // Quyền kế toán ushare
  MERCHANT_ACCOUNTANT = 'merchant_accountant', // Quyền kế toán NCC
  MANAGEMENT_PARTNER = 'management_partner', // Quyền quản lý CTV
}

@Entity('role')
export class RoleEntity extends BaseEntity {
  @ApiProperty({
    description: 'Tên nhóm',
  })
  @IsNotEmpty({
    message: 'Tên nhóm không được bỏ trống',
  })
  @IsString()
  @Transform(({ value }) => value.trim())
  @Column()
  name: string;

  @ApiProperty({
    description: 'Hiển thị cho nhà cung cấp',
    type: Boolean,
  })
  @Column({
    type: 'boolean',
    nullable: true,
    default: false
  })
  viewForMerchant?: boolean;

  @ApiProperty({
    description: 'Quyền',
  })
  @Column({
    type: 'jsonb',
    default: [''],
  })
  permissions: string[];

  @ApiProperty({
    description: 'Loại quyền',
    type: 'enum',
    enum: RoleType,
  })
  @Column({
    type: 'enum',
    enum: RoleType,
    default: RoleType.OTHER,
    nullable: true,
  })
  type?: RoleType;

  @OneToMany(() => UserEntity, (user) => user.role)
  public users: UserEntity[];
}

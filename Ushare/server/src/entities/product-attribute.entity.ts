import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from 'entities/base.entity';
import { ProductEntity } from 'entities/product.entity';
import { ApiProperty } from '@nestjs/swagger';
import { ProductCategoryAttributeEntity } from 'entities/product-category-attribute.entity';
import { ProductCategoryAttributeValueEntity } from 'entities/product-category-attribute-value.entity';

@Entity('product_attribute')
export class ProductAttributeEntity extends BaseEntity {
  @ApiProperty({
    description: 'Id sản phẩm',
  })
  @Column()
  productId: number;

  @ManyToOne(() => ProductEntity, ({ productAttributes }) => productAttributes)
  product: ProductEntity;

  // @ApiProperty({
  //   description: 'ID giá trị thuộc tính danh mục',
  //   required: false,
  // })
  // @Column({
  //   nullable: true,
  //   type: 'json',
  // })
  // productCategoryAttributeValueId: number | number[];
  @Column({
    type: 'int',
    array: true,
    nullable: true,
  })
  @ApiProperty({
    description: 'ID giá trị thuộc tính danh mục',
    type: [Number],
    required: false,
  })
  productCategoryAttributeValueIds?: number[];

  @ApiProperty({
    description: 'Diá trị thuộc tính danh mục',
    type: [ProductCategoryAttributeValueEntity],
    required: false,
  })
  @OneToMany(
    () => ProductCategoryAttributeValueEntity,
    (attributeValue) => attributeValue.id,
  )
  productCategoryAttributeValues?: ProductCategoryAttributeValueEntity[];

  @ApiProperty({
    description: 'ID giá trị thuộc tính danh mục',
    required: false,
  })
  @Column({
    nullable: true,
  })
  productCategoryAttributeId: number;

  @ApiProperty({
    description: 'Giá trị thuộc tính danh mục',
    required: false,
    type: ProductCategoryAttributeEntity,
  })
  @ManyToOne(
    () => ProductCategoryAttributeEntity,
    (attributeValue) => attributeValue.id,
  )
  productCategoryAttribute: ProductCategoryAttributeEntity;

  @ApiProperty({
    description: 'Đơn vị tính',
    required: false,
  })
  @Column({
    nullable: true,
  })
  productCategoryAttributeUnitIndex: number;
}

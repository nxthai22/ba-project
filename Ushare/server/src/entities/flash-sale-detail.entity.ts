import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { CommissionValueType } from './config.entity';
import { FlashSaleEntity } from './flash-sale.entity';
import { ProductVariantEntity } from './product-variant.entity';
import { ProductEntity } from './product.entity';

export enum DiscountValueType {
  AMOUNT = 'amount', // Tính theo số lượng
  PERCENT = 'percent', // Tính theo phần trăm
}
@Entity('flash_sale_detail')
export class FlashSaleDetailEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Id chương trình Flash Sale',
  })
  flashSaleId: number;

  @ManyToOne(() => FlashSaleEntity, (fs) => fs.flashSaleDetail)
  flashSale: FlashSaleEntity;

  @Column()
  @ApiProperty({
    description: 'Id đối tác tham gia Flash Sale',
  })
  merchantId: number;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Id biến thể sản phẩm trong Flash Sale',
  })
  productVariantId?: number;

  @ApiProperty({
    description: 'Thông tin sản phẩm trong Flash Sale',
  })
  @ManyToOne(() => ProductVariantEntity, (variant) => variant.id)
  productVariant: ProductVariantEntity;

  @Column()
  @ApiProperty({
    description: 'Id sản phẩm trong Flash Sale',
  })
  productId: number;

  @ApiProperty({
    description: 'Thông tin sản phẩm trong Flash Sale',
  })
  @ManyToOne(() => ProductEntity, (prod) => prod.id)
  product: ProductEntity;

  @Column()
  @ApiProperty({
    description: 'Mức giảm giá',
  })
  discountValue: number;

  @Column()
  @ApiProperty({
    description: 'Loại giảm giá',
    type: 'enum',
    enum: DiscountValueType,
  })
  discountValueType: DiscountValueType;

  @Column()
  @ApiProperty({
    description: 'Số lượng sản phẩm cho chương trình',
  })
  quantity: number | 0;

  @Column()
  @ApiProperty({
    description: 'Số sản phẩm đã được bán',
  })
  usedQuantity: number | 0;
}

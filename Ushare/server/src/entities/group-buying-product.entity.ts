import { ApiProperty } from '@nestjs/swagger';
import { isNotEmpty, IsNotEmpty, IsNumber, Max, Min } from 'class-validator';
import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { GroupBuyingCampaignEntity } from './group-buying-campaign.entity';
import { GroupBuyingVariantEntity } from './group-buying-variant.entity';
import { GroupEntity } from './group.entity';
import { ProductEntity } from './product.entity';

@Entity('group_buying_product')
export class GroupBuyingProductEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Id chương trình mua chung',
  })
  groupBuyingCampaignId: number;

  @ApiProperty({
    description: 'Cấu hình mua chung',
  })
  @ManyToOne(() => GroupBuyingCampaignEntity, (gb) => gb.groupBuyingProducts)
  groupBuyingCampaign: GroupBuyingCampaignEntity;

  @Column()
  @ApiProperty({
    description: 'Id sản phẩm',
  })
  productId: number;

  @ApiProperty({
    description: 'Thông tin sản phẩm',
    type: () => ProductEntity,
  })
  @ManyToOne(() => ProductEntity, (prod) => prod.groupBuyingProducts)
  product: ProductEntity;

  @IsNumber()
  @Min(1)
  @Column()
  @ApiProperty({
    description: 'Số lượng CTV tối thiểu trên 1 nhóm',
  })
  minMemberNumber: number;

  @Column()
  @ApiProperty({
    description: 'Số lượng sản phẩm tối đa / CTV',
  })
  maxQuantityPerUser: number;

  @IsNumber()
  @Min(1)
  @Column()
  @ApiProperty({
    description: 'Thời gian ghép đơn',
  })
  timeToTeamUp: number;

  @ApiProperty({
    description: 'Danh sách biến thể sản phẩm trong chương trình',
    type: () => [GroupBuyingVariantEntity],
  })
  @OneToMany(() => GroupBuyingVariantEntity, (gbv) => gbv.groupBuyingProduct, {
    cascade: ['insert', 'recover', 'soft-remove', 'update'],
  })
  groupBuyingVariants?: GroupBuyingVariantEntity[];

  @Column({ nullable: true })
  @ApiProperty({
    description: 'Số nhóm chờ ghép tối đa/CTV',
  })
  maxGroupPerUser?: number;

  // In case product has no variant
  @Column({ nullable: true })
  @ApiProperty({
    description: 'Giá mua chung',
  })
  groupPrice?: number;

  @Column({ nullable: true })
  @ApiProperty({
    description: 'Số lượng sản phẩm tối đa cho chương trình',
  })
  maxQuantity?: number;

  @Column({ nullable: true, default: 0 })
  @ApiProperty({
    description: 'Số lượng sản phẩm đã dùng cho chương trình',
  })
  usedQuantity?: number;

  @ApiProperty({
    description: 'Danh sách nhóm',
    type: () => [GroupEntity],
  })
  @OneToMany(() => GroupEntity, (g) => g.groupBuyingProduct)
  groups?: GroupEntity[];

  @Column({
    default: 0,
  })
  @ApiProperty({
    description: 'Mức giảm giá mua chung của sản phẩm',
  })
  priceDiscount: number;
}

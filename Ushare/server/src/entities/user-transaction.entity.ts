import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, Min } from 'class-validator';
import { TransactionStatus, TransactionType, WalletType } from 'enums';
import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { MIN_AMOUNT } from 'config/wallet';
import { BaseEntity } from './base.entity';
import { UserEntity } from './user.entity';
import { UserWalletHistoryEntity } from './user-wallet-history.entity';
import { UserWalletEntity } from './user-wallet.entity';
import { BankEntity } from './bank.entity';

@Entity('user_transaction')
export class UserTransactionEntity extends BaseEntity {
  @Column()
  @ApiProperty({ description: 'Id người dùng', required: true })
  userId: number;

  @ManyToOne(() => UserEntity, (user) => user.walletTransactions)
  user: UserEntity;

  @Column()
  @ApiProperty({ description: 'Id ví người dùng', required: true })
  walletId: number;

  @ManyToOne(() => UserWalletEntity, (wallet) => wallet.transactions)
  wallet: UserWalletEntity;

  @Column({ nullable: true })
  @ApiProperty({
    description: 'Id quản trị xác nhận giao dịch',
    required: false,
  })
  confirmId: number;

  @IsNotEmpty()
  @Min(MIN_AMOUNT)
  @Column()
  @ApiProperty({ description: 'Số tiền giao dịch', required: true })
  amount: number;

  @IsNotEmpty()
  @IsEnum(TransactionStatus)
  @Column({ type: 'enum', enum: TransactionStatus })
  @ApiProperty({
    description: 'Trạng thái giao dịch',
    required: true,
    enum: TransactionStatus,
  })
  status: TransactionStatus;

  @ApiProperty({ description: 'Id ngân hàng', type: Number, required: false })
  @Column()
  bankId: number;

  @ApiProperty({ description: 'Số tài khoản', required: false })
  @Column()
  bankNumber: string;

  @ApiProperty({ description: 'Chủ tài khoản', required: false })
  @Column()
  bankAccountName: string;

  @Column({ type: 'enum', enum: WalletType })
  @ApiProperty({
    description: 'Loại ví',
    required: true,
    enum: WalletType,
  })
  walletType: WalletType;

  @Column({ type: 'enum', enum: TransactionType })
  @ApiProperty({
    description: 'Loại giao dịch',
    required: true,
    enum: TransactionType,
  })
  transactionType: TransactionType;

  @Column({ nullable: true })
  @ApiProperty({ description: 'Ghi chú', required: false })
  note?: string;

  @ApiProperty({ description: 'Thông tin tên ngân hàng' })
  @ManyToOne(() => BankEntity, ({ id }) => id)
  bank?: BankEntity;

  @OneToMany(
    () => UserWalletHistoryEntity,
    (walletHistory) => walletHistory.transaction,
  )
  histories: UserWalletHistoryEntity[];
}

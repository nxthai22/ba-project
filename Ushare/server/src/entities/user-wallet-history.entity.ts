import { ApiProperty } from '@nestjs/swagger';
import { TransactionType, WalletType } from 'enums';
import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { UserTransactionEntity } from './user-transaction.entity';
import { UserWalletEntity } from './user-wallet.entity';
import { UserEntity } from './user.entity';

@Entity('user_wallet_history')
export class UserWalletHistoryEntity extends BaseEntity {
  @Column()
  @ApiProperty({ description: 'Id người dùng', required: true })
  userId: number;

  @Column()
  @ApiProperty({ description: 'Id ví người dùng', required: true })
  walletId: number;

  @Column({ nullable: true })
  @ApiProperty({ description: 'Id giao dịch', required: true })
  transactionId: number;

  @Column()
  @ApiProperty({
    description: 'Loại ví',
    required: true,
    type: WalletType,
  })
  walletType: WalletType;

  @Column({ type: 'enum', enum: TransactionType, nullable: true })
  @ApiProperty({
    description: 'Loại giao dịch',
    enum: TransactionType,
  })
  transactionType: TransactionType;

  @Column()
  @ApiProperty({ description: 'Số tiền giao dịch', required: true })
  amount: number;

  @Column({ nullable: true })
  @ApiProperty({ description: 'Ghi chú', required: false })
  note?: string;

  @ManyToOne(() => UserEntity, (user) => user.walletHistories)
  user: UserEntity;

  @ManyToOne(() => UserWalletEntity, (wallet) => wallet.histories)
  wallet: UserWalletEntity;

  @ManyToOne(
    () => UserTransactionEntity,
    (userTransaction) => userTransaction.histories,
  )
  transaction: UserWalletEntity;
}

import { Column, Entity, ManyToOne } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { OrderEntity } from 'entities/order.entity';
import { BaseEntity } from './base.entity';
import { OrderStatus } from 'enums';
import { UserEntity } from 'entities/user.entity';

export enum OrderHistoryType {
  ORDER = 'order',
  SHIPMENT = 'shipment',
}

@Entity('order_history')
export class OrderHistoryEntity extends BaseEntity {
  @ApiProperty({
    description: 'ID Người dùng',
    required: false,
    type: Number,
  })
  @Column({
    nullable: true,
  })
  userId: number;

  @ApiProperty({
    description: 'Người dùng',
    required: false,
    type: UserEntity,
  })
  @ManyToOne(() => UserEntity, (user) => user.id)
  user: UserEntity;

  @ApiProperty({
    description: 'ID Đơn hàng',
    type: Number,
  })
  @Column()
  orderId: number;

  @ManyToOne(() => OrderEntity, (order) => order.id)
  order: OrderEntity;

  @ApiProperty({
    description: 'Trạng thái',
    enum: OrderStatus,
  })
  @Column({
    type: 'enum',
    enum: OrderStatus,
    default: OrderStatus.CREATED,
  })
  status: OrderStatus;

  @ApiProperty({
    description: 'Loại',
    enum: OrderHistoryType,
  })
  @Column({
    type: 'enum',
    enum: OrderHistoryType,
    default: OrderHistoryType.ORDER,
  })
  type: OrderHistoryType;
}

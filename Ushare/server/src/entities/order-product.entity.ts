import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from 'entities/base.entity';
import { OrderEntity } from 'entities/order.entity';
import { ProductEntity } from 'entities/product.entity';
import { ApiProperty } from '@nestjs/swagger';
import { ProductVariantEntity } from './product-variant.entity';
import { FlashSaleDetailEntity } from './flash-sale-detail.entity';
import { FlashSaleEntity } from './flash-sale.entity';

@Entity('order_product')
export class OrderProductEntity extends BaseEntity {
  @ApiProperty({
    description: 'Mã đơn hàng',
  })
  @Column()
  orderId?: number;

  @ManyToOne(() => OrderEntity, (order) => order.id)
  order?: OrderEntity;

  @ApiProperty({
    description: 'ID Sản phẩm',
  })
  @Column()
  productId?: number;

  @ApiProperty({
    description: 'ID Flash sale',
    required: false,
  })
  @Column({ nullable: true })
  flashSaleId?: number;

  @ApiProperty({
    description: 'ID Flash sale detail',
    required: false,
  })
  @Column({ nullable: true })
  flashSaleDetailId?: number;

  @ApiProperty({
    description: 'Sản phẩm',
    type: ProductEntity,
    required: false,
  })
  @ManyToOne(() => ProductEntity, (product) => product.id)
  product?: ProductEntity;

  @ApiProperty({
    description: 'Tên biến thể (Tên các thuộc tính)',
    required: false,
  })
  @Column({
    nullable: true,
  })
  variantTitle?: string;

  @ApiProperty({
    description: 'ID biến thể',
  })
  @Column({
    nullable: true,
  })
  variantId?: number;

  @ApiProperty({
    description: 'ID biến thể',
    type: ProductVariantEntity,
  })
  @ManyToOne(() => ProductVariantEntity, (variant) => variant.id)
  variant?: ProductVariantEntity;

  @ApiProperty({
    description: 'Số lượng',
    required: false,
  })
  @Column({
    default: 0,
  })
  quantity?: number;

  @ApiProperty({
    description: 'Giá',
    required: false,
  })
  @Column({
    default: 0,
  })
  price?: number;

  @ApiProperty({
    description: 'Giảm giá',
    required: false,
  })
  @Column({
    default: 0,
  })
  discount?: number;

  @Column({
    default: 0,
  })
  @ApiProperty({
    description: 'Giảm giá sản phẩm',
    required: false,
    type: Number,
  })
  discountVoucherProduct?: number;

  @Column({
    default: 0,
  })
  @ApiProperty({
    description: 'Giảm giá shop',
    required: false,
    type: Number,
  })
  discountVoucherShop?: number;

  @ApiProperty({
    description: 'Hoa hồng',
    required: false,
  })
  @Column({
    default: 0,
  })
  commission?: number;

  @Column({
    type: 'int',
    array: true,
    nullable: true,
  })
  @ApiProperty({
    description: 'Danh sách voucher được áp dụng',
    type: [Number],
    required: false,
  })
  voucherIds?: number[];

  @ManyToOne(() => FlashSaleEntity, ({ id }) => id)
  flashSale?: FlashSaleEntity;

  @ManyToOne(() => FlashSaleDetailEntity, ({ id }) => id)
  flashSaleDetail?: FlashSaleDetailEntity;
}

import { BaseEntity } from 'entities/base.entity';
import { Column, Entity, ManyToOne, Unique } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { UserEntity } from 'entities/user.entity';
import { ProductEntity } from 'entities/product.entity';
import { ProductVariantEntity } from 'entities/product-variant.entity';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';

@Entity('cart_product')
@Unique(['userId', 'productId', 'productVariantId', 'merchantAddressId'])
export class CartProductEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Người dùng',
    type: Number,
  })
  userId: number;

  @ManyToOne(() => UserEntity, ({ cartProducts }) => cartProducts)
  user?: UserEntity;

  @ApiProperty({
    description: 'ID Sản phẩm',
    type: Number,
  })
  @Column()
  productId: number;

  @ApiProperty({
    description: 'Sản phẩm',
    type: () => ProductEntity,
    required: false,
  })
  @ManyToOne(() => ProductEntity, ({ cartProducts }) => cartProducts)
  product?: ProductEntity;

  @ApiProperty({
    description: 'ID Biến thể của Sản phẩm',
    type: Number,
    required: false,
  })
  @Column({
    nullable: true,
  })
  productVariantId?: number;

  @ApiProperty({
    description: 'Biến thể sản phẩm',
    type: () => ProductVariantEntity,
    required: false,
  })
  @ManyToOne(() => ProductVariantEntity, ({ cartProducts }) => cartProducts)
  productVariant?: ProductVariantEntity;

  @ApiProperty({
    description: 'ID Kho hàng',
    type: Number,
  })
  @Column()
  merchantAddressId: number;

  @ManyToOne(() => MerchantAddressEntity, ({ id }) => id)
  merchantAddress?: MerchantAddressEntity;

  @ApiProperty({
    description: 'Số lượng',
    type: Number,
  })
  @Column()
  quantity: number;

  @ApiProperty({
    description: 'Sản phẩm được chọn',
    type: Boolean,
  })
  @Column({
    type: 'boolean',
    nullable: true,
  })
  checked: boolean;

  @ApiProperty({
    description: 'Giá sau khi áp dụng mã giảm giá thành công',
    type: Number,
  })
  @Column({
    nullable: true,
  })
  priceDiscount?: number;

  @ApiProperty({
    description: 'Mã ưu đãi của shop',
    type: String,
    required: false,
  })
  @Column({
    nullable: true,
  })
  voucherCode?: string;

  @ApiProperty({
    description: 'Mã ưu đãi của Ushare',
    type: String,
    required: false,
  })
  @Column({
    nullable: true,
  })
  voucherUshareCode?: string;

  @ApiProperty({
    description: 'Hoa hồng cho CTV',
    type: Number,
    required: false,
  })
  @Column({
    nullable: true,
    type: 'float',
  })
  commission?: number;
}

import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from 'entities/base.entity';
import { WardEntity } from 'entities/ward.entity';
import { UserEntity } from 'entities/user.entity';

@Entity('shipping_address')
export class ShippingAddressEntity extends BaseEntity {
  @ManyToOne(() => UserEntity, (user) => user.id)
  user: number;

  @Column()
  address: string;

  @Column()
  tel: string;

  @ManyToOne(() => WardEntity, (ward) => ward.id)
  ward: WardEntity;
}

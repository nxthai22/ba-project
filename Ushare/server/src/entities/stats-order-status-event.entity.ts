import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('stats_order_status_event_hyper', { synchronize: false })
export class StatsOrderStatusEventEntity {
  @PrimaryColumn()
  time: Date;

  @PrimaryColumn()
  orderId: number;

  @PrimaryColumn()
  status: string;

  @Column()
  merchantId?: number;

  @Column()
  userId?: number;
}

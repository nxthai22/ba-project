import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from 'entities/base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { ProductCategoryAttributeEntity } from './product-category-attribute.entity';

@Entity('product_category_attribute_value')
export class ProductCategoryAttributeValueEntity extends BaseEntity {
  @ApiProperty({
    description: 'Tên giá trị',
  })
  @Column({
    nullable: true,
  })
  name: string;

  @ApiProperty({
    description: 'Đối tác',
    required: false,
  })
  @Column({
    nullable: true,
    type: Number,
  })
  merchantId?: number;

  @ApiProperty({
    description: 'Id thuộc tính danh mục sản phẩm',
    required: false,
  })
  @Column({
    nullable: true,
  })
  productCategoryAttributeId?: number;

  @ApiProperty({
    description: 'Thuộc tính danh mục sản phẩm',
    required: false,
    type: () => ProductCategoryAttributeEntity,
  })
  @ManyToOne(
    () => ProductCategoryAttributeEntity,
    (attribute) => attribute.productCategoryAttributeValues,
    { onDelete: 'CASCADE' },
  )
  productCategoryAttribute: ProductCategoryAttributeEntity;
}

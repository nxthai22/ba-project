import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from 'entities/base.entity';
import { Column, Entity } from 'typeorm';

@Entity('notification_token')
export class NotificationTokenEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'FCM token',
    type: String,
    required: true,
  })
  fcmToken: string;

  @ApiProperty({
    description: 'ID Người dùng',
    required: false,
    type: Number,
  })
  @Column({
    nullable: true,
  })
  userId: number;

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  @ApiProperty({
    description: 'Topics',
    type: [String],
    required: false,
  })
  topics?: string[];
}

import { Column, Entity, ManyToOne, Unique } from "typeorm";
import { BaseEntity } from 'entities/base.entity';
import { ProductVariantEntity } from 'entities/product-variant.entity';
import { ProductEntity } from "entities/product.entity";

@Entity('product_model')
@Unique(['productId', 'variantOptionIndex'])
export class ProductModelEntity extends BaseEntity {
  @Column()
  productId: number;

  @ManyToOne(() => ProductEntity, (product) => product.id)
  product: ProductEntity;

  @Column({
    type: 'jsonb',
  })
  variantOptionIndex: number[];

  @Column({
    default: 1,
  })
  status: number;

  @Column({
    default: 0,
  })
  stock: number;

  @Column({
    default: 0,
  })
  price: number;

  @Column({
    default: 0,
  })
  priceBeforeDiscount: number;
}

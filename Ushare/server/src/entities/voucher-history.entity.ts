import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, ManyToOne, Unique } from 'typeorm';
import { BaseEntity } from './base.entity';
import { OrderEntity } from './order.entity';
import { VoucherEntity } from './voucher.entity';

@Entity('voucher_history')
@Unique(['tel', 'voucherId'])
export class VoucherHistoryEntity extends BaseEntity {
  @Column({
    nullable: true,
    type: String,
  })
  @ApiProperty({
    description: 'SĐT Người đặt hàng',
    type: String,
  })
  tel?: string;

  @Column()
  @ApiProperty({
    description: 'Id voucher',
  })
  voucherId: number;

  @ManyToOne(() => VoucherEntity, (voucher) => voucher.voucherHistories)
  voucher?: VoucherEntity;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Id đơn hàng',
  })
  orderId?: number;

  @ManyToOne(() => OrderEntity, (order) => order.voucherHistories)
  order?: OrderEntity;
}

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { GroupBuyingProductEntity } from './group-buying-product.entity';

export enum GroupBuyingCampaignStatus {
  ACTIVE = 'ACTIVE',
  MERCHANT_END = 'MERCHANT_END',
  EXPIRE_END = 'EXPIRE_END',
}

@Entity('group_buying_campaign')
export class GroupBuyingCampaignEntity extends BaseEntity {
  @IsNotEmpty()
  @Column()
  @ApiProperty({
    description: 'Tên chương trình mua chung',
  })
  name: string;

  @Column({
    type: 'timestamp with time zone',
  })
  @ApiProperty({
    description: 'Thời gian bắt đầu chương trình mua chung',
    type: String,
  })
  startTime: Date;

  @Column({
    type: 'timestamp with time zone',
  })
  @ApiProperty({
    description: 'Thời gian kết thúc chương trình mua chung',
    type: String,
  })
  endTime: Date;

  @ApiProperty({
    description: 'Trạng thái chiến dịch mua chung',
    enum: GroupBuyingCampaignStatus,
  })
  @Column({
    type: 'enum',
    enum: GroupBuyingCampaignStatus,
    default: GroupBuyingCampaignStatus.ACTIVE,
  })
  status?: GroupBuyingCampaignStatus;

  @Column({
    default: 0,
  })
  @ApiProperty({
    description: 'Số người đã đặt hàng',
    required: false,
  })
  participantNumber?: number | 0;

  @Column({
    default: 0,
  })
  @ApiProperty({
    description: 'Số lượng sản phẩm được tham gia',
    required: false,
  })
  productNumber?: number;

  @Column()
  @ApiProperty({
    description: 'Id đối tác tham gia',
  })
  merchantId: number;

  @ApiProperty({
    description: 'Danh sách sản phẩm trong chương trình',
    type: () => [GroupBuyingProductEntity],
  })
  @OneToMany(() => GroupBuyingProductEntity, (gbp) => gbp.groupBuyingCampaign, {
    cascade: true,
  })
  groupBuyingProducts?: GroupBuyingProductEntity[];
}

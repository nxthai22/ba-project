import { Column, Entity, Index } from 'typeorm';
import { BaseEntity } from './base.entity';

export enum CustomerType {
  ushare = 'ushare',
  merchant = 'merchant',
  collaborator = 'collaborator',
}

@Entity('stats_customer')
@Index('UNQ_stats_customer_tel', ['tel'], {
  unique: true,
  where: `"type" = '${CustomerType.ushare}'`,
})
@Index('UNQ_stats_customer_merchantId_tel', ['merchantId', 'tel'], {
  unique: true,
  where: `"type" = '${CustomerType.merchant}'`,
})
@Index('UNQ_stats_customer_userId_tel', ['userId', 'tel'], {
  unique: true,
  where: `"type" = '${CustomerType.collaborator}'`,
})
export class StatsCustomerEntity extends BaseEntity {
  @Column({ nullable: true })
  merchantId: number;

  @Column({ nullable: true })
  userId: number;

  @Column()
  tel: string;

  @Column({
    type: 'enum',
    enum: CustomerType,
    default: CustomerType.ushare,
  })
  type: CustomerType;
}

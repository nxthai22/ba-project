import { Column, Entity, ManyToOne } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { OrderEntity } from 'entities/order.entity';
import { BaseEntity } from './base.entity';
import { OrderPaymentStatus, OrderStatus } from 'enums';
import { UserEntity } from 'entities/user.entity';

@Entity('order_payment_history')
export class OrderPaymentHistoryEntity extends BaseEntity {
  @ApiProperty({
    description: 'ID Người dùng',
    required: false,
    type: Number,
  })
  @Column({
    nullable: true,
  })
  userId: number;

  @ApiProperty({
    description: 'Người dùng',
    required: false,
    type: UserEntity,
  })
  @ManyToOne(() => UserEntity, (user) => user.id)
  user?: UserEntity;

  @ApiProperty({
    description: 'ID Đơn hàng',
    type: Number,
  })
  @Column()
  orderId: number;

  @ManyToOne(() => OrderEntity, (order) => order.id)
  order?: OrderEntity;

  @ApiProperty({
    description: 'Trạng thái',
    enum: OrderPaymentStatus,
  })
  @Column({
    type: 'enum',
    enum: OrderPaymentStatus,
  })
  paymentStatus: OrderPaymentStatus;
}

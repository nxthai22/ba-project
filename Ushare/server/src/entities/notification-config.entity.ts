import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from 'entities/base.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { MerchantEntity } from './merchant.entity';

export enum NotificationType {
  ORDER = 'order', //Đơn hàng
  OFFER = 'offer', //Ưu đãi

  BONUS = 'bonus', //Thưởng
  PRODUCT = 'product',  //Sản phẩm 
  HIGHLIGHT = 'highlight', //Nổi bật
  OTHER = 'other' // Khác
}

export enum NotificationReceivedType {
  COLLABORATOR = 'collaborator', // CTV
  FOLLOWED_COLLABORATOR = 'followed_collaborator', // CTV đang theo dõi
  STAFF = 'staff', // Nhân viên
  USHARE_STAFT = 'ushare_staff', //Tất cả đối tác
  ALL = 'all', // Tất cả
}

export enum NotificationStatus {
  NEW = 'new', // Mới
  APPROVED = 'approved', // Đã duyệt
  REJECTED = 'rejected', // Từ chối
  SENT = 'sent', // Đã gửi
  FAILED = 'failed', // Lỗi
}

@Entity('notification_config')
export class NotificationConfigEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Tiêu đề',
    type: String,
    required: true,
  })
  title: string;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Nội dung ngắn',
    type: String,
    required: false,
  })
  body?: string;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Nội dung',
    type: String,
    required: false,
  })
  content?: string;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'ID nhà cung cấp',
    type: Number,
    required: false,
  })
  merchantId?: number;

  @Column({
    type: 'timestamp with time zone',
  })
  @ApiProperty({
    description: 'Thời gian gửi',
    required: true,
  })
  scheduledTime: Date;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'File đính kèm',
    type: String,
    required: false,
  })
  attachedFile?: string;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Image',
    type: String,
    required: false,
  })
  image?: string;

  @Column({
    type: 'enum',
    enum: NotificationType,
    default: NotificationType.OTHER,
  })
  @ApiProperty({
    description: 'Loại',
    enum: NotificationType,
    required: true
  })
  type: NotificationType;

  @Column({
    type: 'enum',
    enum: NotificationReceivedType,
    default: NotificationReceivedType.FOLLOWED_COLLABORATOR,
  })
  @ApiProperty({
    description: 'Loại người nhận',
    enum: NotificationReceivedType,
    required: true
  })
  receivedType: NotificationReceivedType;

  @Column({
    type: 'enum',
    enum: NotificationStatus,
    default: NotificationStatus.NEW,
  })
  @ApiProperty({
    description: 'Loại người nhận',
    enum: NotificationStatus,
    required: false
  })
  status: NotificationStatus;

  @ApiProperty({ description: 'Nhà cung cấp', type: () => MerchantEntity })
  @ManyToOne(() => MerchantEntity, ({ id }) => id)
  merchant?: MerchantEntity;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Lý do từ chối',
    type: String,
    required: false,
  })
  rejecedReason?: string;
}

import { ApiProperty } from "@nestjs/swagger";
import { Column, Entity, ManyToOne, OneToMany } from "typeorm";
import { BaseEntity } from "./base.entity";
import { MerchantEntity } from "./merchant.entity";
import { OrderRevenueEntity } from "./order-revenue.entity";
import { UserEntity } from "./user.entity";

@Entity('revenue_statistic')
export class RevenueStatisticEntity extends BaseEntity {
    @Column()
    @ApiProperty({
        description: 'Id người dùng'
    })
    userId: number;

    @ManyToOne(() => UserEntity, (us) => us.revenueStatistic)
    user: UserEntity;

    @Column()
    @ApiProperty({
        description: 'Id đối tác'
    })
    merchantId?: number;

    @ManyToOne(() => MerchantEntity, (mer) => mer.revenueStatistic)
    merchant: MerchantEntity;

    @Column({
        type: 'float'
    })
    @ApiProperty({
        description: 'Tổng doanh thu hoàn thành trong tháng'
    })
    totalRevenueCompleted: number;

    @Column({
        type: 'float',
        nullable: true
    })
    @ApiProperty({
        description: 'Tổng hoa hồng hoàn thành trong tháng'
    })
    totalCommissionCompleted: number;

    @Column({
        type: 'float',
        nullable: true
    })
    @ApiProperty({
        description: 'Tổng hoa hồng theo sản phẩm trong tháng'
    })
    totalProductCommission: number;
    
    @Column()
    @ApiProperty({
        description: 'Tháng thống kê'
    })
    date: Date;
}
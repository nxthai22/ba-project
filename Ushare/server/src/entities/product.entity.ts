import { AfterLoad, Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { ProductCategoryEntity } from 'entities/product-category.entity';
import { UserEntity } from 'entities/user.entity';
import { ProductVariantEntity } from 'entities/product-variant.entity';
import { ProductAttributeEntity } from 'entities/product-attribute.entity';
import { BaseEntity } from 'entities/base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { ProductModelEntity } from 'entities/product-model.entity';
import { MerchantEntity } from 'entities/merchant.entity';
import { IsNotEmpty, IsNumber, IsString, Max, Min } from 'class-validator';
import { ProductStockEntity } from 'entities/product-stock.entity';
import { CartProductEntity } from 'entities/cart-product.entity';
import { MerchantAddressEntity } from './merchant-address.entity';
import { GroupBuyingProductEntity } from './group-buying-product.entity';

export enum ProductStatus {
  ACTIVE = 'active', // Đang kinh doanh
  INACTIVE = 'inactive', // Tạm hết hàng
  DELETED = 'deleted', // Ngừng kinh doanh
}

export class ProductGroupTypeValue {
  @ApiProperty({
    description: 'Tên biến thể',
    type: String,
  })
  field: string;
}

/** Nhóm Biến thể sản phẩm */
export class ProductGroupType {
  @ApiProperty({ description: 'Tên nhóm Biến thể' })
  name: string;

  @ApiProperty({
    description: 'Biến thể',
    type: [ProductGroupTypeValue],
  })
  value: ProductGroupTypeValue[];
}

/** Giá trị tồn kho */
export class StockValue {
  @ApiProperty({ description: 'Id kho hàng' })
  merchantAddressId: number;

  @ApiProperty({ description: 'Số lượng kho' })
  quantity: number;

  @ApiProperty({
    description: 'Thông tin địa chỉ kho hàng',
    required: false,
    type: () => MerchantAddressEntity,
  })
  merchantAddress?: MerchantAddressEntity;
}

@Entity('product')
export class ProductEntity extends BaseEntity {
  @ApiProperty({ description: 'Id sản phẩm eShop', required: false })
  @Column({ nullable: true })
  productEShopId?: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'Tên sản phẩm' })
  @Column()
  name: string;

  @ApiProperty({ description: 'Đường dẫn sản phẩm' })
  @Column({
    nullable: true,
  })
  slug?: string;

  @ApiProperty({ description: 'Mô tả', required: false })
  @Column({ nullable: true })
  description?: string;

  @ApiProperty({ description: 'Mô tả ngắn', required: false })
  @Column({ nullable: true })
  shortDescription?: string;

  @ApiProperty({ description: 'Ảnh bìa sản phẩm', required: false })
  @Column({ nullable: true, type: 'jsonb', default: [] })
  thumbnail?: string[];

  @ApiProperty({ description: 'Danh sách ảnh', required: false })
  @Column({ nullable: true, type: 'jsonb', default: [] })
  images?: string[];

  @ApiProperty({ description: 'Giá sản phẩm/Giá thấp nhất của sản phẩm' })
  @Column({ default: 0 })
  price?: number;

  @ApiProperty({ description: 'Giá chưa khuyến mãi' })
  rawPrice?: number;

  @ApiProperty({ description: 'Giá cao nhất sản phẩm' })
  @Column({ default: 0 })
  maxPrice?: number;

  @IsNumber()
  @ApiProperty({ description: 'Giá trước khi giảm' })
  @Column({ default: 0 })
  priceBeforeDiscount?: number;

  @ApiProperty({ description: 'ID Người đăng' })
  @Column()
  userId: number;

  @ManyToOne(() => UserEntity, ({ id }) => id)
  user: UserEntity;

  @ApiProperty({ description: 'Khối lượng (ĐVT: gram)' })
  @Column({ default: 0 })
  weight?: number;

  @ApiProperty({ description: 'Chiều cao', required: false })
  @Column({ default: 0 })
  height?: number;

  @ApiProperty({ description: 'Chiều dài', required: false })
  @Column({ default: 0 })
  length?: number;

  @ApiProperty({ description: 'Chiều rộng', required: false })
  @Column({ default: 0 })
  width?: number;

  @ApiProperty({ description: 'ID Đối tác' })
  @Column({ nullable: true })
  merchantId?: number;

  @ApiProperty({ description: 'Video youtube' })
  @Column({ nullable: true })
  videoUrl?: string;

  @ApiProperty({ description: 'Video thumbnail sản phẩm' })
  @Column({ nullable: true })
  videoProductThumbnailUrl?: string;

  @ApiProperty({ description: 'Video sản phẩm' })
  @Column({ nullable: true })
  videoProductUrl?: string;

  @ApiProperty({ description: 'Đối tác' })
  @ManyToOne(() => MerchantEntity, ({ id }) => id)
  merchant: MerchantEntity;

  @ApiProperty({ description: 'ID Sản phẩm của Merchant' })
  @Column({ nullable: true })
  merchantProductId?: number;

  @ApiProperty({ description: 'ID Danh mục sản phẩm' })
  @Column({ nullable: true })
  productCategoryId?: number;

  @ApiProperty({ description: 'Số lượng sản phẩm đã bán' })
  @Column({ nullable: true, default: 0 })
  saleAmount?: number;

  @ApiProperty({ description: 'Trạng thái', enum: ProductStatus })
  @Column({
    nullable: true,
    default: ProductStatus.ACTIVE,
    type: 'enum',
    enum: ProductStatus,
  })
  status?: ProductStatus;

  @ApiProperty({
    description: 'Tên nhóm biến thể',
    required: false,
    type: [ProductGroupType],
  })
  @Column({ type: 'jsonb', nullable: true })
  groupType?: ProductGroupType[];

  @ApiProperty({ description: 'Hoa hồng sản phẩm từ NCC' })
  @Column({ default: 0, nullable: true, type: 'int' })
  commission?: number;

  @ApiProperty({ description: 'Hoa hồng theo % sản phẩm từ NCC' })
  @Column({ default: 0, nullable: true, type: 'float' })
  @Min(0, { message: 'Hoa hồng phải lớn hơn 0%' })
  @Max(100, { message: 'Hoa hồng phải nhỏ hơn 100%' })
  commissionPercent?: number;

  @ApiProperty({
    description: 'Danh sách tồn kho',
    required: false,
    type: [StockValue],
  })
  @Column({ type: 'jsonb', nullable: true })
  stock?: StockValue[];

  @ApiProperty({ description: 'Mã Sku', required: false })
  @Column({ nullable: true })
  SKU?: string;

  @ApiProperty({ description: 'Tag sản phẩm', required: false })
  @Column({ nullable: true })
  tag?: string;

  @ApiProperty({ description: 'Danh mục', type: () => ProductCategoryEntity })
  @ManyToOne(() => ProductCategoryEntity, ({ products }) => products)
  productCategory?: ProductCategoryEntity;

  @ApiProperty({
    description: 'Danh sách thuộc tính',
    type: () => [ProductAttributeEntity],
  })
  @OneToMany(() => ProductAttributeEntity, ({ product }) => product, {
    cascade: true,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  productAttributes?: ProductAttributeEntity[];

  @ApiProperty({
    description: 'Biến thể',
    required: false,
    type: () => [ProductVariantEntity],
  })
  @OneToMany(() => ProductVariantEntity, ({ product }) => product)
  variants?: ProductVariantEntity[];

  @OneToMany(() => ProductModelEntity, ({ product }) => product)
  models?: ProductModelEntity[];

  @ApiProperty({
    description: 'Kho',
    required: false,
    type: [ProductStockEntity],
  })
  @OneToMany(() => ProductStockEntity, ({ product }) => product)
  stocks?: ProductStockEntity[];

  @ApiProperty({
    description: 'Kho',
    required: false,
  })
  @OneToMany(() => CartProductEntity, ({ product }) => product)
  cartProducts?: CartProductEntity[];

  @ApiProperty({
    description: 'Flash sale',
    required: false,
  })
  flashSale?: any;

  @ApiProperty({
    description: 'Giá mua chung',
    required: false,
  })
  groupBuyingPrice?: number;

  @ApiProperty({
    description: 'Số lượng sản phẩm mua chung còn lại',
    required: false,
  })
  groupBuyingStock?: number;

  @ApiProperty({
    description:
      'Số lượng user đã tham gia mua chung sản phẩm trong chương trình',
    required: false,
  })
  userJoinedGBProduct?: number;

  @ApiProperty({
    description: 'Hoa hồng mua chung',
    required: false,
  })
  groupBuyingCommission?: number;

  @ApiProperty({
    description: 'Số lượng sản phẩm mua chung còn lại của user',
    required: false,
  })
  remainingQuantityGB?: number;

  @ApiProperty({
    description: 'số nhóm mà user đã tham gia trong chương trình mua chung sp',
    required: false,
  })
  groupJoined?: number;

  @ApiProperty({
    description: 'số lượng sp user đã mua trong chương trình mua chung sp',
    required: false,
  })
  quantityBookedGB?: number;

  @OneToMany(() => GroupBuyingProductEntity, (gbp) => gbp.product)
  groupBuyingProducts?: GroupBuyingProductEntity[];

  @AfterLoad()
  getCDNLink() {
    if (this.images)
      this.images = this.images.map((image) => {
        if (!image) return '';
        if (!image.includes('http') && !image.includes('https'))
          return 'https://ushare.hn.ss.bfcplatform.vn/' + image;
        return image;
      });
  }
}

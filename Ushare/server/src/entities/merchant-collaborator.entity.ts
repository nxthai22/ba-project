import { Column, Entity, Index } from 'typeorm';
import { BaseEntity } from './base.entity';

@Entity('merchant_collaborator')
@Index('UNQ_userId', ['userId'], {
  unique: true,
  where: `"merchantId" is null`,
})
@Index('UNQ_merchantId_userId', ['merchantId', 'userId'], {
  unique: true,
  where: `"merchantId" notnull`,
})
export class MerchantCollaboratorEntity extends BaseEntity {
  @Column({ nullable: true })
  merchantId: number;

  @Column()
  userId: number;
}

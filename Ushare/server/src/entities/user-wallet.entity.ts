import { ApiProperty } from '@nestjs/swagger';
import { WalletType } from 'enums';
import { Column, Entity, ManyToOne, OneToMany, Unique } from 'typeorm';
import { BaseEntity } from './base.entity';
import { UserEntity } from './user.entity';
import { UserWalletHistoryEntity } from './user-wallet-history.entity';
import { UserTransactionEntity } from './user-transaction.entity';

@Entity('user_wallet')
@Unique(['userId', 'type'])
export class UserWalletEntity extends BaseEntity {
  @Column()
  @ApiProperty({ description: 'Id người dùng', required: true })
  userId: number;

  @Column({ type: 'enum', enum: WalletType })
  @ApiProperty({ description: 'Loại ví', required: true, enum: WalletType })
  type: WalletType;

  @Column({ nullable: true, default: 0 })
  @ApiProperty({ description: 'Số tiền trong ví', required: false })
  amount?: number;

  @ManyToOne(() => UserEntity, (user) => user.wallets)
  user: UserEntity;

  @OneToMany(
    () => UserWalletHistoryEntity,
    (walletHistory) => walletHistory.wallet,
  )
  histories: UserWalletHistoryEntity[];

  @OneToMany(
    () => UserTransactionEntity,
    (userTransaction) => userTransaction.wallet,
  )
  transactions: UserTransactionEntity[];
}

import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { CustomerEntity } from './customer.entity';
import { WardEntity } from './ward.entity';

@Entity('customer-address')
export class CustomerAddressEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Id của khách hàng',
    required: true,
  })
  customerId: number;

  @ManyToOne(() => CustomerEntity, (cus) => cus.addresses, {
    onDelete: 'CASCADE',
  })
  customer: CustomerEntity;

  @Column()
  @ApiProperty({
    description: 'Địa chỉ cụ thể',
    required: true,
  })
  address: string;

  @Column()
  @ApiProperty({
    description: 'Id của phường/xã',
    required: true,
  })
  wardId: number;

  @ManyToOne(() => WardEntity, (ward) => ward.id)
  ward: WardEntity;
}

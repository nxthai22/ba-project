import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity('stats_product_event', { synchronize: false })
export class StatsProductEventEntity {
  @PrimaryColumn()
  time: Date;

  @PrimaryColumn()
  productId: number;

  @PrimaryColumn()
  event: string;

  @Column()
  merchantId: number;
}

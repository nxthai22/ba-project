import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity } from 'typeorm';
import { BaseEntity } from './base.entity';

@Entity('search_keyword')
export class SearchKeywordEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Cụm từ tìm kiếm',
  })
  keyword: string;

  @Column()
  @ApiProperty({
    description: 'Số lần được tìm kiếm',
  })
  amount: number | 0;
}

import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from 'entities/base.entity';
import { Column, Entity, ManyToOne, OneToOne } from 'typeorm';
import { NotificationConfigEntity, NotificationType } from './notification-config.entity';

export enum NotificationTrigger {
  AUTO = 'auto',
  SCHEDULED = 'scheduled'
}

@Entity('notification')
export class NotificationEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Tiêu đề',
    type: String,
    required: true,
  })
  title: string;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Nội dung ngắn',
    type: String,
    required: false,
  })
  body?: string;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Nội dung',
    type: String,
    required: false,
  })
  content?: string;

  @Column({
    nullable: true,
    unique: true
  })
  @ApiProperty({
    description: 'ID notification config',
    type: Number,
    required: false,
  })
  notificationConfigId?: number;

  @Column({
    type: 'enum',
    enum: NotificationType,
    default: NotificationType.OTHER,
  })
  @ApiProperty({
    description: 'Loại',
    enum: NotificationType,
    required: true
  })
  type: NotificationType;

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  @ApiProperty({
    description: 'Data',
    type: Object,
    required: false,
  })
  data?: Object;

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  @ApiProperty({
    description: 'Danh sách User nhận riêng',
    type: String,
    required: false,
  })
  userIds?: number[];

  @Column({
    type: 'enum',
    enum: NotificationTrigger,
    default: NotificationTrigger.AUTO,
  })
  @ApiProperty({
    description: 'Kiểu kích hoạt thông báo',
    enum: NotificationTrigger,
    required: false
  })
  trigger?: NotificationTrigger;

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  @ApiProperty({
    description: 'Data',
    type: [Object],
    required: false,
  })
  log: Object;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'File đính kèm',
    type: String,
    required: false,
  })
  attachedFile?: string;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Image',
    type: String,
    required: false,
  })
  image?: string;

  @ApiProperty({ description: 'Notification config', type: () => NotificationConfigEntity })
  @OneToOne(() => NotificationConfigEntity, (notiConfig) => notiConfig.id)
  notificationConfig?: NotificationConfigEntity;
}

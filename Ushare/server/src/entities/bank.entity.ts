import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { UserEntity } from './user.entity';
import { MerchantEntity } from './merchant.entity';

@Entity('bank')
export class BankEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Tên đầy đủ của ngân hàng',
  })
  name: string;

  @Column()
  @ApiProperty({
    description: 'Tên viết tắt',
  })
  shortName: string;

  @OneToMany(() => UserEntity, (user) => user.bank)
  users: UserEntity[];

  @OneToMany(() => MerchantEntity, (merchant) => merchant.bank)
  merchants: MerchantEntity[];
}

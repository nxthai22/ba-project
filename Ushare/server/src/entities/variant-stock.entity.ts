import { Column, Entity, ManyToOne } from "typeorm";
import { BaseEntity } from "./base.entity";
import { ApiProperty } from "@nestjs/swagger";
import { ProductEntity } from "./product.entity";
import { MerchantAddressEntity } from "./merchant-address.entity";
import { ProductVariantEntity } from "./product-variant.entity";

// @Entity('variant_stock')
export class VariantStockEntity extends BaseEntity {
  @ApiProperty({
    description: 'ID biến thể',
    type: Number,
  })
  @Column()
  variantId: number;

  @ManyToOne(()=>ProductVariantEntity,(variant)=>variant.id)
  variant: ProductVariantEntity;

  @ApiProperty({
    description: 'Số lượng tồn kho',
    type: Number,
  })
  @Column()
  quantity: number;

  @ApiProperty({
    description: 'ID Kho',
    type: Number,
  })
  @Column()
  merchantAddressId: number;

  @ApiProperty({
    description: 'Kho',
    type: MerchantAddressEntity,
  })
  @ManyToOne(() => MerchantAddressEntity, ({ id }) => id)
  merchantAddress: MerchantAddressEntity;
}
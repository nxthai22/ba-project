import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from 'entities/base.entity';
import { ProductCategoryEntity } from 'entities/product-category.entity';
import { ApiProperty } from '@nestjs/swagger';
import { ProductCategoryAttributeValueEntity } from './product-category-attribute-value.entity';

export enum ControlType {
  TEXT = 'text',
  MULTI_SELECT = 'multi_select',
  SELECT = 'select',
}

@Entity('product_category_attribute')
export class ProductCategoryAttributeEntity extends BaseEntity {
  @ApiProperty({
    description: 'Tên thuộc tính',
  })
  @Column({
    nullable: true,
  })
  name: string;

  @ApiProperty({
    description: 'Loại control',
    type: 'enum',
    enum: ControlType,
    default: ControlType.TEXT,
  })
  @Column({
    type: 'enum',
    enum: ControlType,
    default: ControlType.TEXT,
  })
  controlType: ControlType;

  @ApiProperty({
    description: 'Đối tác có thể tự thêm',
    type: Boolean,
  })
  @Column({
    type: 'boolean',
    default: true,
  })
  canAdd: boolean;

  @ApiProperty({
    description: 'Đơn vị',
    default: [],
    type: [String],
    required: false,
  })
  @Column({
    type: 'jsonb',
    nullable: true,
  })
  units: string[] | [];

  @ApiProperty({
    description: 'Id danh mục sản phẩm',
    required: false,
  })
  @Column({
    nullable: true,
  })
  productCategoryId: number;

  @ManyToOne(() => ProductCategoryEntity, ({ id }) => id)
  productCategory: ProductCategoryEntity;

  @ApiProperty({
    description: 'Danh sách giá trị của thuộc tính',
    type: [ProductCategoryAttributeValueEntity] || [],
  })
  @OneToMany(
    () => ProductCategoryAttributeValueEntity,
    (value) => value.productCategoryAttribute,
  )
  productCategoryAttributeValues: ProductCategoryAttributeValueEntity[] | [];
}

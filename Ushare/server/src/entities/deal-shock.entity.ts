import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { DealShockProductEntity } from './deal-shock-product.entity';

export enum DealShockType {
  COMBO = 'combo', // Mua kèm deal shock
  GIFT = 'gift', // Mua để nhận quà
}

// @Entity('deal-shock')
export class DealShockEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    required: true,
    description: 'Tên chương trình',
    type: String,
  })
  name: string;

  @Column()
  @ApiProperty({
    required: true,
    description: 'Loại chương trình',
    type: 'enum',
    enum: DealShockType,
    default: DealShockType.COMBO,
  })
  type: DealShockType;

  @Column()
  @ApiProperty({
    required: true,
    description: 'Thời gian bắt đầu',
    type: Date,
  })
  startTime: Date;

  @Column()
  @ApiProperty({
    required: true,
    description: 'Thời gian kết thúc',
    type: Date,
  })
  endTime: Date;

  @Column()
  @ApiProperty({
    required: false,
    description: 'Giới hạn sản phẩm mua kèm',
    type: Number,
    default: 0,
  })
  maxProductNumber?: number;

  @Column()
  @ApiProperty({
    required: false,
    description: 'Giá trị đơn hàng tối thiểu để nhận quà',
    type: Number,
    default: 0,
  })
  minPriceOrder?: number;

  @Column()
  @ApiProperty({
    required: false,
    description: 'Số quà được nhận',
    type: Number,
    default: 0,
    maximum: 50,
  })
  giftNumber?: number;

  @OneToMany(() => DealShockProductEntity, (prod) => prod.dealShock)
  dealShockProducts: DealShockProductEntity[];
}

import { OrderStatus } from 'enums';
import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';

@Entity('stats_order', { synchronize: false })
export class StatsOrderEntity {
  @PrimaryColumn()
  orderId: number;

  @PrimaryColumn()
  createdAt?: Date;

  @Column({
    type: 'enum',
    enum: OrderStatus,
    default: OrderStatus.CREATED,
  })
  status: OrderStatus;

  @Column()
  merchantId: number;

  @Column()
  merchantAddressId: number;

  @Column()
  userId: number;

  @Column()
  revenue?: number;

  @Column()
  commission?: number;

  @ManyToOne(
    () => MerchantAddressEntity,
    (merchantAddress) => merchantAddress.id,
  )
  merchantAddress?: MerchantAddressEntity;
}

import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from 'entities/base.entity';
import { UserEntity } from 'entities/user.entity';
import { WardEntity } from 'entities/ward.entity';
import { OrderProductEntity } from 'entities/order-product.entity';
import { ApiProperty } from '@nestjs/swagger';
import { MerchantEntity } from 'entities/merchant.entity';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import {
  OrderPaymentStatus,
  OrderPaymentType,
  OrderShippingStatus,
  OrderStatus,
  ShippingPartner,
} from 'enums';
import { OrderHistoryEntity } from 'entities/order-history.entity';
import { IsNotEmpty, IsNumberString, IsString } from 'class-validator';
import { Transform } from 'class-transformer';
import { OrderRevenueEntity } from './order-revenue.entity';
import { VoucherHistoryEntity } from './voucher-history.entity';
import { nanoid } from 'nanoid';
import { format } from 'date-fns';
import { GroupEntity } from './group.entity';
import { OrderPaymentHistoryEntity } from './order-deposit-history.entity';

export const randomCodeNano = () => {
  const date = format(new Date(), 'yyMMdd');
  const id = nanoid(8).toUpperCase();
  return date + id;
};

export class ShipperInfo {
  @ApiProperty({
    description: 'Tên Đối tác',
    required: false,
    type: String,
  })
  partnerName?: string;

  @ApiProperty({
    description: 'Tên người vận chuyển',
    required: false,
    type: String,
  })
  fullName?: string;

  @ApiProperty({
    description: 'Số điện thoại người vận chuyển',
    required: false,
    type: String,
  })
  tel?: string;
}

export class ShippingInfo {
  @ApiProperty({
    description: 'Id sản phẩm vận đơn',
  })
  orderProductIds?: number[] | [];

  @ApiProperty({
    description: 'Đối tác vận chuyển',
    enum: ShippingPartner,
  })
  partner: ShippingPartner;

  @ApiProperty({
    description: 'Ghi chú của Merchant cho Đối tác',
  })
  note?: string;

  @ApiProperty({
    description: 'Mã đơn hàng',
    required: false,
  })
  id?: number;

  @ApiProperty({
    description: 'Mã đơn hàng đối tác Vận chuyển',
    required: false,
  })
  partner_id?: string;

  @ApiProperty({
    description: 'Phí vận chuyển',
  })
  fee: number;

  @ApiProperty({
    description: 'Phí bảo hiểm',
    required: false,
  })
  insurance_fee?: number;

  @ApiProperty({
    description: 'Thời gian lấy hàng dự kiến',
    required: false,
  })
  estimated_pick_time?: string;

  @ApiProperty({
    description: 'Thời gian giao hàng dự kiến',
    required: false,
  })
  estimated_deliver_time?: string;

  @ApiProperty({
    description: 'Trạng thái giao hàng',
    required: false,
    enum: OrderShippingStatus,
  })
  status?: OrderShippingStatus;

  @ApiProperty({
    description: 'Tên chi tiết trạng thái giao hàng',
    required: false,
    type: String,
  })
  statusName?: string;

  @ApiProperty({
    description: 'Thông tin Người giao hàng',
    required: false,
    type: ShipperInfo,
  })
  shipperInfo?: ShipperInfo;
}

export const ORDER_STATUS_INVALID = [
  OrderStatus.CANCELLED,
  OrderStatus.MERCHANT_CANCELLED,
  OrderStatus.FAILED,
  OrderStatus.REFUNDED,
  OrderStatus.RETURNED,
  OrderStatus.CANCELLED,
];

@Entity('order')
export class OrderEntity extends BaseEntity {
  // @ApiProperty({
  //   description: 'Id đơn hàng eShop',
  // })
  // @Column({
  //   nullable: true,
  // })
  // eShopOrderId?: string;
  //
  // @ApiProperty({
  //   description: 'Mã đơn hàng eShop',
  // })
  // @Column({
  //   nullable: true,
  // })
  // eShopOrderNo?: string;
  @ApiProperty({
    description: 'Mã đơn hàng',
    type: String,
  })
  @Column({
    nullable: true,
  })
  code?: string;

  @ApiProperty({
    description: 'Mã đơn hàng import',
    type: String,
  })
  @Column({
    nullable: true,
  })
  importCode?: string;

  @ApiProperty({
    description: 'Trạng thái đơn hàng',
    enum: OrderStatus,
  })
  @Column({
    type: 'enum',
    enum: OrderStatus,
    default: OrderStatus.CREATED,
  })
  status?: OrderStatus;

  @ApiProperty({
    description: 'ID Đối tác',
    required: false,
  })
  @Column({
    nullable: true,
  })
  merchantId?: number;

  @ApiProperty({
    description: 'Đối tác',
  })
  @ManyToOne(() => MerchantEntity, (merchant) => merchant.id)
  merchant?: MerchantEntity;

  @ApiProperty({
    description: 'ID Địa chỉ lấy hàng Đối tác',
    required: false,
  })
  @Column({
    nullable: true,
  })
  merchantAddressId?: number;

  @ApiProperty({
    description: 'Địa chỉ lấy hàng Đối tác',
  })
  @ManyToOne(
    () => MerchantAddressEntity,
    (merchantAddress) => merchantAddress.id,
  )
  merchantAddress?: MerchantAddressEntity;

  @ApiProperty({
    description: 'ID Người dùng đặt hàng',
    required: false,
  })
  @Column({
    nullable: true,
  })
  userId?: number;

  @ApiProperty({
    description: 'Người dùng đặt hàng',
  })
  @ManyToOne(() => UserEntity, (user) => user.id)
  user?: UserEntity;

  @ApiProperty({
    description: 'Họ tên',
  })
  @IsNotEmpty({
    message: 'Họ tên không được bỏ trống',
  })
  @IsString({ message: ' ' })
  @Transform(({ value }) => value.trim())
  @Column()
  fullname: string;

  @IsNotEmpty({
    message: 'Địa chỉ không được bỏ trống',
  })
  @IsString({ message: ' ' })
  @Transform(({ value }) => value.trim())
  @ApiProperty({
    description: 'Địa chỉ',
  })
  @Column()
  shippingAddress: string;

  @ApiProperty({
    description: 'ID Phường/Xã',
    required: false,
  })
  @Column({
    nullable: true,
  })
  wardId?: number;

  @ApiProperty({
    description: 'Phường/Xã',
  })
  @ManyToOne(() => WardEntity, (ward) => ward.id)
  ward?: WardEntity;

  @IsNotEmpty({
    message: 'Số điện thoại không được bỏ trống',
  })
  @IsNumberString(() => IsNumberString, { message: ' ' })
  @Transform(({ value }) => value.trim())
  @ApiProperty({
    description: 'Số điện thoại',
  })
  @Column()
  tel: string;

  @ApiProperty({
    description: 'Phí ship',
    required: false,
  })
  @Column({
    nullable: true,
    default: 0,
  })
  shippingFee?: number;

  @ApiProperty({
    description: 'Giảm giá Phí ship',
    required: false,
  })
  @Column({
    nullable: true,
    default: 0,
  })
  shippingDiscount?: number;

  @ApiProperty({
    description: 'Tổng tiền',
    required: false,
  })
  @Column({
    nullable: true,
    default: 0,
  })
  total?: number;

  @ApiProperty({
    description: 'Giảm giá',
    required: false,
  })
  @Column({
    default: 0,
  })
  discount?: number;

  @ApiProperty({
    description: 'Phương thức thanh toán',
    required: false,
    enum: OrderPaymentType,
    default: OrderPaymentType.COD,
  })
  @Column({
    type: 'enum',
    enum: OrderPaymentType,
    default: OrderPaymentType.COD,
  })
  paymentType?: OrderPaymentType;

  @ApiProperty({
    description: 'Trạng thái thanh toán',
    required: false,
    enum: OrderPaymentStatus,
  })
  @Column({
    type: 'enum',
    enum: OrderPaymentStatus,
    nullable: true,
  })
  paymentStatus?: OrderPaymentStatus;

  @ApiProperty({
    description: 'Đơn vị vận chuyển',
    required: false,
  })
  @Column({
    nullable: true,
    type: 'enum',
    enum: ShippingPartner,
  })
  shippingPartner?: ShippingPartner;

  @ApiProperty({
    description: 'Thông tin vận chuyển',
    required: false,
    type: [ShippingInfo],
  })
  @Column({
    nullable: true,
    type: 'jsonb',
    default: [],
  })
  shippingInfos?: ShippingInfo[];

  @ApiProperty({
    description: 'Thông tin vận chuyển (view)',
    required: false,
    type: ShippingInfo,
  })
  shippingInfo?: ShippingInfo;

  @ApiProperty({
    description: 'ID Đơn hàng của Merchant ',
    required: false,
  })
  @Column({
    nullable: true,
  })
  merchantOrderId?: number;

  @ApiProperty({
    description: 'Danh sách sản phẩm',
    required: false,
    type: [OrderProductEntity],
  })
  @OneToMany(() => OrderProductEntity, (orderProduct) => orderProduct.order)
  orderProducts?: OrderProductEntity[];

  @ApiProperty({
    description: 'Lịch sử đơn hàng',
    required: false,
    type: [OrderHistoryEntity],
  })
  @OneToMany(() => OrderHistoryEntity, (orderHistory) => orderHistory.order)
  histories?: OrderHistoryEntity[];

  @ApiProperty({
    description: 'Chiều cao (ĐVT: cm)',
    required: false,
  })
  @Column({
    default: 0,
  })
  height?: number;

  @ApiProperty({
    description: 'Chiều dài (ĐVT: cm)',
    required: false,
  })
  @Column({
    default: 0,
  })
  length?: number;

  @ApiProperty({
    description: 'Chiều rộng (ĐVT: cm)',
    required: false,
  })
  @Column({
    default: 0,
  })
  width?: number;

  @ApiProperty({
    description: 'Ghi chú của KH/CTV',
    required: false,
  })
  @Column({
    nullable: true,
  })
  note?: string;

  @ApiProperty({
    description: 'Lý do huỷ',
    required: false,
  })
  @Column({
    nullable: true,
  })
  cancel_reason?: string;

  @ApiProperty({
    description: 'Cấu hình',
    required: false,
  })
  @Column({
    nullable: true,
  })
  configCommissionId?: number;

  @ApiProperty({
    description: 'Tiền đặt cọc/trả trước',
    required: false,
  })
  @Column({
    nullable: true,
  })
  deposit?: number;

  @ApiProperty({
    description: 'Hoa hồng',
    required: false,
  })
  @Column({
    default: 0,
  })
  commission?: number;

  @ApiProperty({
    description: 'Giảm giá của voucher Shop',
    required: false,
  })
  @Column({
    default: 0,
  })
  voucherMerchantDiscount?: number;

  @ApiProperty({
    description: 'Giảm giá của voucher Ushare',
    required: false,
  })
  @Column({
    default: 0,
  })
  voucherUshareDiscount?: number;

  // @ApiProperty({
  //   description: 'Id người xác nhận',
  //   required: false,
  // })
  // @Column({
  //   nullable: true,
  // })
  // depositedById?: number;
  //
  // @ManyToOne(() => UserEntity, (user) => user.id)
  // depositedBy: UserEntity[];

  @OneToMany(() => OrderRevenueEntity, (orderRevenue) => orderRevenue.orderId)
  revenue?: OrderRevenueEntity[];

  @OneToMany(() => VoucherHistoryEntity, (voucher) => voucher.order)
  voucherHistories?: VoucherHistoryEntity[];

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Id nhóm mua chung',
  })
  groupId?: number;

  @ApiProperty({
    description: 'Nhóm mua chung',
    type: GroupEntity,
  })
  @ManyToOne(() => GroupEntity, (g) => g.orders)
  group?: GroupEntity;

  @ApiProperty({
    description: 'Lịch sử thanh toán',
    required: false,
    type: [OrderPaymentHistoryEntity],
  })
  @OneToMany(
    () => OrderPaymentHistoryEntity,
    (paymentHistory) => paymentHistory.order,
  )
  paymentHistories?: OrderPaymentHistoryEntity[];
}

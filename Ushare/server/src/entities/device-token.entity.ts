import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from 'entities/base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { UserEntity } from 'entities/user.entity';

@Entity('device_token')
export class DeviceTokenEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Firebase Messaging token',
  })
  token: string;

  @Column()
  @ApiProperty({
    description: 'User ID',
  })
  userId: number;

  @ManyToOne(() => UserEntity, ({ tokens }) => tokens)
  user: UserEntity;
}

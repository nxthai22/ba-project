import { ApiProperty, PickType } from '@nestjs/swagger';
import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { GroupBuyingProductEntity } from './group-buying-product.entity';
import { OrderEntity } from './order.entity';
import { UserEntity } from './user.entity';

export enum GroupStatus {
  WAITING = 'WAITING',
  ACCEPTED = 'ACCEPTED',
  MERCHANT_END = 'MERCHANT_END',
  EXPIRE_END = 'EXPIRE_END',
}

@Entity('group')
export class GroupEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Tên nhóm',
  })
  name: string;

  @ApiProperty({
    description: 'ID Người dùng trưởng nhóm',
    required: true,
    type: Number,
  })
  @Column({
    nullable: false,
  })
  userId: number;

  @ApiProperty({
    description: 'Người dùng trưởng nhóm',
    required: false,
    type: () => UserEntity,
  })
  @ManyToOne(() => UserEntity, (user) => user.id)
  user?: UserEntity;

  @Column()
  @ApiProperty({
    description: 'Id sản phẩm chương trình mua chung',
  })
  groupBuyingProductId: number;

  @ApiProperty({
    description: 'Cấu hình sản phẩm mua chung',
    type: () => [GroupBuyingProductEntity],
  })
  @ManyToOne(() => GroupBuyingProductEntity, (gbp) => gbp.groups)
  groupBuyingProduct?: GroupBuyingProductEntity;

  @Column()
  @ApiProperty({
    description: 'Refer link',
    required: true,
  })
  ref: string;

  @Column()
  @ApiProperty({
    description: 'Số lượng người còn lại để đủ nhóm',
  })
  remains: number;

  @ApiProperty({
    description: 'Trạng thái nhóm mua chung',
    enum: GroupStatus,
  })
  @Column({
    type: 'enum',
    enum: GroupStatus,
    default: GroupStatus.WAITING,
  })
  status?: GroupStatus;

  @ApiProperty({
    description: 'Đơn hàng',
    type: () =>
      class GBOrderDto extends PickType(OrderEntity, [
        'paymentStatus',
        'user',
      ]) {},
  })
  @OneToMany(() => OrderEntity, (o) => o.group)
  orders?: OrderEntity[];

  @ApiProperty({
    description: 'user đã tham gia nhóm ?',
    type: Boolean,
  })
  isJoined?: boolean;
}

import {
  AfterLoad,
  Column,
  Entity,
  ManyToMany,
  OneToMany,
  Unique,
} from 'typeorm';
import { BaseEntity } from 'entities/base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { VoucherHistoryEntity } from './voucher-history.entity';
import { ProductEntity } from 'entities/product.entity';

export enum VoucherType {
  SHOP = 'shop',
  PRODUCT = 'product',
}

export enum VoucherDiscountType {
  PERCENT = 'percentage',
  AMOUNT = 'amount',
}

@Entity('voucher')
export class VoucherEntity extends BaseEntity {
  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Id đối tác',
    required: false,
  })
  merchantId?: number;

  @Column({
    type: 'enum',
    enum: VoucherType,
    default: VoucherType.SHOP,
    nullable: true,
  })
  @ApiProperty({
    description: 'Loại voucher',
    type: 'enum',
    enum: VoucherType,
  })
  type?: VoucherType;

  @Column()
  @ApiProperty({
    description: 'Tên voucher',
  })
  name: string;

  @Column({
    type: String,
  })
  @ApiProperty({
    description: 'Mã voucher',
  })
  code: string;

  @Column({
    type: 'timestamp with time zone',
  })
  @ApiProperty({
    description: 'Thời gian bắt đầu áp dụng voucher',
  })
  startDate: Date;

  @Column({
    type: 'timestamp with time zone',
  })
  @ApiProperty({
    description: 'Thời gian kết thúc áp dụng voucher',
  })
  endDate: Date;

  @Column({
    type: 'int',
    array: true,
    nullable: true,
  })
  @ApiProperty({
    description: 'Danh sách sản phẩm được áp dụng',
    type: [Number],
    required: false,
  })
  productIds?: number[];

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  @ApiProperty({
    description: 'Danh sách người dùng được áp dụng',
    required: false,
  })
  userIds?: number[];

  @Column({ default: 0 })
  @ApiProperty({
    description: 'Số lượng voucher',
    required: false,
  })
  quantity?: number;

  @Column({ default: 0 })
  @ApiProperty({
    description: 'Số lượng voucher đã sử dụng',
    required: false,
  })
  quantityUsed?: number;

  @Column({ default: 0 })
  @ApiProperty({
    description: 'Giá trị nhỏ nhất đơn hàng',
    required: false,
  })
  minAmount?: number;

  @Column({ default: 0 })
  @ApiProperty({
    description: 'Giá trị lớn nhất đơn hàng',
    required: false,
  })
  maxAmount?: number;

  @Column({
    enum: VoucherDiscountType,
    type: 'enum',
    default: VoucherDiscountType.PERCENT,
  })
  @ApiProperty({
    description: 'Loại giảm',
    enum: VoucherDiscountType,
    type: 'enum',
  })
  discountType: VoucherDiscountType;

  @Column()
  @ApiProperty({
    description: 'Mức giảm',
  })
  discountValue: number;

  @Column({ default: 0 })
  @ApiProperty({
    description: 'Mức giảm tối đa',
    required: false,
  })
  discountMaximum: number;

  @Column({ nullable: true })
  @ApiProperty({
    description: 'Mô tả ',
    required: false,
    type: String,
  })
  description?: string;

  @OneToMany(() => VoucherHistoryEntity, (history) => history.voucher)
  voucherHistories?: VoucherHistoryEntity[];

  @ApiProperty({
    description: 'Danh sách Sản phẩm áp dụng',
    type: [ProductEntity],
    required: false,
  })
  @ManyToMany(() => ProductEntity, (p) => p)
  products?: ProductEntity[];

  @ApiProperty({
    description: 'Tiêu đề ngắn mã giảm giá',
    type: String,
    required: false,
  })
  @Column({
    nullable: true,
  })
  subTitle?: string;

  @Column({
    default: 0,
  })
  @ApiProperty({
    description: 'Số lượng sản phẩm tối thiểu cho 1 đơn hàng',
    required: false,
  })
  minProductNumber?: number;

  @Column({
    default: 0,
  })
  @ApiProperty({
    description: 'Giá tối thiểu cho mỗi sản phẩm',
    required: false,
  })
  minProductPrice?: number;

  @AfterLoad()
  async createSubTitle() {
    switch (this.discountType) {
      case VoucherDiscountType.PERCENT:
        this.subTitle = 'Giảm ' + this.discountValue + '%';
        break;
      case VoucherDiscountType.AMOUNT:
        this.subTitle = 'Giảm đ' + Math.floor(this.discountValue / 1000) + 'k';
        break;
    }
  }
}

import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { GroupBuyingProductEntity } from './group-buying-product.entity';
import { ProductVariantEntity } from './product-variant.entity';

@Entity('group_buying_variant')
export class GroupBuyingVariantEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Id sản phẩm trong chương trình mua chung',
  })
  groupBuyingProductId: number;

  @ManyToOne(() => GroupBuyingProductEntity, (gbp) => gbp.groupBuyingVariants, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  groupBuyingProduct: GroupBuyingProductEntity;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Id biến thể sản phẩm',
  })
  productVariantId: number;

  @ApiProperty({
    description: 'Thông tin sản phẩm',
  })
  @ManyToOne(() => ProductVariantEntity, (variant) => variant.id)
  productVariant: ProductVariantEntity;

  @Column()
  @ApiProperty({
    description: 'Giá mua chung',
  })
  groupPrice: number;

  @Column()
  @ApiProperty({
    description: 'Số lượng sản phẩm tối đa cho chương trình',
  })
  maxQuantity: number;

  @Column({
    default: 0,
  })
  @ApiProperty({
    description: 'Số lượng sản phẩm đã dùng cho chương trình',
  })
  usedQuantity: number;
}

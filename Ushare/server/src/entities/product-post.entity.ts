import { AfterLoad, Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from 'entities/base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { UserEntity } from 'entities/user.entity';
import { ProductEntity } from 'entities/product.entity';

export enum ProductPostMediaType {
  IMAGE = 'image',
  VIDEO = 'video',
}

export enum ProductPostStatus {
  PUBLISHED = 'published', // Đã xuất bản
  DRAFT = 'draf', // Bản nháp
}

export class ProductPostMedia {
  @Column({
    type: 'enum',
    enum: ProductPostMediaType,
    default: ProductPostMediaType.IMAGE,
  })
  @ApiProperty({
    enum: ProductPostMediaType,
  })
  type: ProductPostMediaType;

  @Column()
  @ApiProperty({
    description: 'Đường dẫn file',
  })
  url: string;
}

@Entity('product_post')
export class ProductPostEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Người tạo',
    required: false,
  })
  createdById?: number;

  @ManyToOne(() => UserEntity, (createdBy) => createdBy.id)
  createdBy?: number;

  @Column()
  @ApiProperty({
    description: 'Người sửa cuối',
    required: false,
  })
  modifiedById?: number;

  @ManyToOne(() => UserEntity, (modifiedBy) => modifiedBy.id)
  modifiedBy?: number;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'ID Sản phẩm',
    required: false,
  })
  productId?: number;

  @ApiProperty({
    description: 'Thông tin Sản phẩm',
    required: false,
  })
  @ManyToOne(() => ProductEntity, (product) => product.id)
  product?: ProductEntity;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Tiêu đề',
    required: false,
  })
  title?: string;

  @Column()
  @ApiProperty({
    description: 'Nội dung',
  })
  content: string;

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  @ApiProperty({
    description: 'Ảnh/video',
    type: [String],
    required: false,
  })
  media?: string[];

  @Column({
    type: 'enum',
    enum: ProductPostStatus,
    default: ProductPostStatus.DRAFT,
  })
  @ApiProperty({
    description: 'Trạng thái',
    required: false,
    enum: ProductPostStatus,
  })
  status: ProductPostStatus;

  @AfterLoad()
  getCDNLink() {
    if (this.media)
      this.media = this.media.map((image) => {
        if (!image) return '';
        if (!image.includes('http') && !image.includes('https'))
          return 'https://ushare.hn.ss.bfcplatform.vn/' + image;
        return image;
      });
  }
}

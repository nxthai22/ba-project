import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { DealShockEntity } from './deal-shock.entity';
import { ProductEntity } from './product.entity';
import { DealShockProductDetailEntity } from './deal-shock-product-detail.entity';

// @Entity('deal-shock-product')
export class DealShockProductEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    required: true,
    description: 'Id chương trình Deal sốc',
  })
  dealShockId: number;

  @ApiProperty({ description: 'Thông tin Deal sốc' })
  @ManyToOne(() => DealShockEntity, (dealShock) => dealShock.dealShockProducts)
  dealShock: DealShockEntity;

  @Column()
  @ApiProperty({
    required: true,
    description: 'Id sản phẩm chính',
  })
  productId: number;

  @ApiProperty({ description: 'Thông tin sản phẩm chính' })
  @ManyToOne(() => ProductEntity, (prod) => prod.id)
  product: ProductEntity;

  @ApiProperty({ description: 'Thông tin chi tiết sản phẩm mua kèm' })
  @OneToMany(
    () => DealShockProductDetailEntity,
    (detail) => detail.dealShockProduct,
  )
  dealShockProductDetails: DealShockProductDetailEntity[];
}

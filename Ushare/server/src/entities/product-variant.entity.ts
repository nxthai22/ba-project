import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from 'entities/base.entity';
import { ProductEntity, StockValue } from 'entities/product.entity';
import { ApiProperty } from '@nestjs/swagger';
import { CartProductEntity } from 'entities/cart-product.entity';
import { FlashSaleEntity } from 'entities/flash-sale.entity';

export class AttributeItemValue {
  @ApiProperty({ description: 'Id thuộc tính sản phẩm' })
  productAttributeId: number;

  @ApiProperty({ description: 'Giá trị của thuộc tính' })
  value: string;
}

@Entity('product_variant')
export class ProductVariantEntity extends BaseEntity {
  @ApiProperty({
    description: 'Tên biến thể "Thuộc tính 1,Thuộc tính 2"',
  })
  @Column()
  name: string;

  @ApiProperty({
    description: 'Index biến thể',
    required: false,
  })
  @Column({ default: 0 })
  groupTypeIndex?: number;

  @ApiProperty({
    description: 'Index giá trị biến thể',
    required: false,
  })
  @Column({ default: 0 })
  valueTypeIndex?: number;

  @ApiProperty({
    description: 'Id sản phẩm',
    required: false,
  })
  @Column()
  productId?: number;

  @ApiProperty({
    description: 'Sản phẩm',
    required: false,
    type: () => ProductEntity,
  })
  @ManyToOne(() => ProductEntity, ({ id }) => id)
  product: ProductEntity;

  @ApiProperty({
    description: 'Hình ảnh biến thể',
    required: false,
    type: [String],
  })
  @Column({
    type: 'jsonb',
    default: [],
  })
  images?: string[];

  @ApiProperty({
    description: 'Giá biến thể sản phẩm',
  })
  @Column({ default: 0 })
  price?: number;

  @ApiProperty({
    description: 'Hoa hồng biến thể sản phẩm từ NCC',
    required: false,
  })
  @Column({ default: 0 })
  commission?: number;

  @ApiProperty({
    description: 'Danh sách tồn kho',
    required: false,
    type: () => [StockValue],
  })
  @Column({
    type: 'jsonb',
    nullable: true,
  })
  stock?: StockValue[];

  @ApiProperty({
    description: 'Mã Sku',
    required: false,
  })
  @Column({ nullable: true })
  SKU?: string;

  @ApiProperty({
    description: 'Tag biến thể sản phẩm',
    required: false,
  })
  @Column({ nullable: true })
  tag?: string;

  @OneToMany(() => CartProductEntity, ({ productVariant }) => productVariant)
  cartProducts: CartProductEntity[];

  @ApiProperty({
    description: 'Flash sale',
    required: false,
  })
  flashSale?: any;

  @ApiProperty({
    description: 'Giá mua chung',
    required: false,
  })
  groupBuyingPrice?: number;

  @ApiProperty({
    description: 'Số lượng sản phẩm mua chung còn lại',
    required: false,
  })
  groupBuyingStock?: number;

  @ApiProperty({
    description: 'Hoa hồng biến thể sản phẩm mua chung',
    required: false,
  })
  groupBuyingCommission?: number;
}

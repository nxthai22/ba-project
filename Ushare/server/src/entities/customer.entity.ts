import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Column, Entity, ManyToOne, OneToMany, Unique } from 'typeorm';
import { BaseEntity } from './base.entity';
import { UserEntity } from './user.entity';
import { CustomerAddressEntity } from './customer-address.entity';

@Entity('customer')
@Unique(['userId', 'tel'])
export class CustomerEntity extends BaseEntity {
  @Column()
  @ApiProperty({ description: 'Id người dùng', required: false })
  userId: number;

  @ManyToOne(() => UserEntity, (user) => user.customers, { nullable: true })
  user: UserEntity;

  @Column()
  @ApiProperty({ description: 'Tên' })
  fullName: string;

  @Column()
  @ApiProperty({ description: 'Số điện thoại' })
  tel: string;

  @OneToMany(() => CustomerAddressEntity, (add) => add.customer)
  addresses: CustomerAddressEntity[];
}

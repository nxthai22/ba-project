import { BaseEntity } from 'entities/base.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { ProductEntity } from 'entities/product.entity';
import { MerchantAddressEntity } from 'entities/merchant-address.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity('product_stock')
export class ProductStockEntity extends BaseEntity {
  @ApiProperty({
    description: 'ID Sản phẩm',
    type: Number,
  })
  @Column()
  productId: number;

  @ManyToOne(() => ProductEntity, ({ stocks }) => stocks)
  product: ProductEntity;

  @ApiProperty({
    description: 'Số lượng tồn kho',
    type: Number,
  })
  @Column()
  quantity: number;

  @ApiProperty({
    description: 'ID Kho',
    type: Number,
  })
  @Column()
  merchantAddressId: number;

  @ApiProperty({
    description: 'Kho',
    type: MerchantAddressEntity,
  })
  @ManyToOne(() => MerchantAddressEntity, ({ id }) => id)
  merchantAddress: MerchantAddressEntity;
}

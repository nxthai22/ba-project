import { Column, Entity, ManyToOne, OneToMany, Unique } from 'typeorm';
import { BaseEntity } from 'entities/base.entity';
import { ApiProperty } from '@nestjs/swagger';
import { WardEntity } from 'entities/ward.entity';
import {
  MerchantEntity,
  MerchantShippingApiToken,
} from 'entities/merchant.entity';
import { IsNotEmpty, IsNumber } from 'class-validator';
import { ProvinceEntity } from './province.entity';
import { DistrictEntity } from './district.entity';
import { ActiveStatus } from 'enums';
import { ProductStockEntity } from 'entities/product-stock.entity';
import { CartProductEntity } from 'entities/cart-product.entity';

@Entity('merchant_address')
@Unique(['name', 'code', 'merchantId'])
export class MerchantAddressEntity extends BaseEntity {
  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ description: 'ID của NCC' })
  @Column()
  merchantId?: number;

  @ApiProperty({ description: 'Nhà cung cấp', type: () => MerchantEntity })
  @ManyToOne(() => MerchantEntity, (merchant) => merchant.id)
  merchant?: MerchantEntity;

  @ApiProperty({ description: 'Địa chỉ' })
  @Column()
  address: string;

  @ApiProperty({ description: 'Tên kho hàng' })
  @Column()
  name: string;

  @ApiProperty({ description: 'Mã' })
  @Column({
    nullable: true,
  })
  code?: string;

  @ApiProperty({ description: 'Số điện thoại' })
  @Column()
  tel: string;

  @IsNotEmpty()
  @ApiProperty({
    description: 'Trạng thái',
    enum: ActiveStatus,
  })
  @Column({
    type: 'enum',
    enum: ActiveStatus,
    default: ActiveStatus.ACTIVE,
  })
  status: ActiveStatus;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({
    description: 'ID của Xã/Phường',
    required: true,
  })
  @Column()
  wardId: number;

  @ApiProperty({
    description: 'Xã phường',
    type: WardEntity,
  })
  @ManyToOne(() => WardEntity, ({ id }) => id)
  ward?: WardEntity;

  // @OneToMany(() => ProductStockEntity, ({ merchantAddress }) => merchantAddress)
  // stocks: ProductStockEntity[];

  // @ApiProperty({
  //   description: 'Shipping API Token',
  //   required: false,
  //   type: [MerchantShippingApiToken],
  // })
  // @Column({ type: 'jsonb', nullable: true })
  // shippingApiTokens: MerchantShippingApiToken[];

  @ApiProperty({
    description: 'ShopId địa chỉ của GHN',
    required: false,
    type: Number,
  })
  @Column({ nullable: true })
  ghnId?: number;

  @ApiProperty({
    description: 'ShopId địa chỉ của GHTK',
    required: false,
    type: Number,
  })
  @Column({ nullable: true })
  ghtkId?: number;

  @ApiProperty({
    description: 'ShopId địa chỉ của VIETTELPOST',
    required: false,
    type: Number,
  })
  @Column({ nullable: true })
  vtpId?: number;
}

import { ApiProperty } from '@nestjs/swagger';
import { OrderStatus } from 'enums';
import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { OrderEntity } from './order.entity';

@Entity('order_revenue')
export class OrderRevenueEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Id đơn hàng',
    required: true,
  })
  orderId: number;

  @ManyToOne(() => OrderEntity, (od) => od.revenue)
  order: OrderEntity;

  @Column()
  @ApiProperty({
    description: 'Trạng thái đơn hàng',
    required: true,
    type: OrderStatus,
  })
  status: OrderStatus;

  @Column()
  @ApiProperty({
    description: 'Doanh thu đơn hàng',
    required: true,
  })
  revenue: number | 0;

  @Column({
    nullable: true,
  })
  @ApiProperty({
    description: 'Hoa hồng theo từng sản phẩm',
    required: true,
  })
  productCommission: number | 0;

  @Column()
  @ApiProperty({
    description: 'Id đối tác',
    required: true,
  })
  merchantId?: number;

  @Column()
  @ApiProperty({
    description: 'Tháng tính doanh thu',
  })
  month?: Date;

  @Column()
  @ApiProperty({
    description: 'Id CTV',
    required: true,
  })
  userId?: number;

  // @ManyToOne(() => UserEntity, (us) => us.orderRevenue)
  // userOrderRevenue: UserEntity;
}

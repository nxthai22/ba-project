import {
  AfterLoad,
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  OneToMany,
  Tree,
  TreeChildren,
  TreeParent,
  Unique,
} from 'typeorm';
import { BaseEntity } from 'entities/base.entity';
import { ProductEntity } from 'entities/product.entity';
import { ApiProperty } from '@nestjs/swagger';
import { ProductCategoryAttributeEntity } from './product-category-attribute.entity';
import { slugify } from 'utils';

export enum ProductCategoryState {
  ACTIVE = 'active',
  INACTIVE = 'inActive',
}

@Entity('product_category')
@Unique(['name', 'code'])
@Tree('materialized-path')
export class ProductCategoryEntity extends BaseEntity {
  @ApiProperty({ description: 'Tên' })
  @Column()
  name: string;

  @ApiProperty({ description: 'Đường dẫn danh mục', required: false })
  @Column({
    nullable: true,
  })
  slug?: string;

  @ApiProperty({ description: 'Mã' })
  @Column({
    nullable: true,
  })
  code: string;

  @ApiProperty({ description: 'Mô tả', required: false })
  @Column({ nullable: true })
  description?: string;

  // @ApiProperty({ description: "Vị trí" })
  // @Column({ default: 0 })
  // position?: string;

  @ApiProperty({ description: 'Màu sắc', required: false })
  @Column({ default: '#000000', nullable: true })
  color?: string;

  @ApiProperty({
    description: 'Trạng thái',
    type: 'enum',
    enum: ProductCategoryState,
  })
  @Column({
    type: 'enum',
    enum: ProductCategoryState,
    default: ProductCategoryState.ACTIVE,
  })
  state?: string;

  @ApiProperty({ description: 'Ảnh', required: false })
  @Column({ nullable: true })
  image?: string;

  @OneToMany(() => ProductEntity, ({ productCategory }) => productCategory)
  products: ProductEntity[];

  @ApiProperty({ description: 'ID danh mục của Merchant', required: false })
  @Column({ nullable: true })
  merchantCategoryId?: number;

  @ApiProperty({
    description: 'Danh sách thuộc tính',
    type: [ProductCategoryAttributeEntity],
  })
  @OneToMany(
    () => ProductCategoryAttributeEntity,
    (attribute) => attribute.productCategory,
  )
  productCategoryAttributes: ProductCategoryAttributeEntity[];

  @ApiProperty({
    description: 'Danh sách cấp dưới ',
    required: false,
    type: [ProductCategoryEntity],
  })
  @TreeChildren()
  childrens?: ProductCategoryEntity[];

  @ApiProperty({ description: 'Danh mục cha', required: false })
  @Column({ nullable: true })
  parentId?: number;

  @ApiProperty({
    description: 'Danh mục cha',
    type: ProductCategoryEntity,
    required: false,
  })
  // @ManyToOne(() => ProductCategoryEntity, ({ parent }) => parent)
  @TreeParent()
  parent: ProductCategoryEntity;

  mpath?: string;

  @BeforeInsert()
  @BeforeUpdate()
  async createSlug() {
    if (this.name) this.slug = slugify(this.name);
  }

  @AfterLoad()
  getCDNLink() {
    if (
      this.image &&
      !this.image?.includes('http') &&
      !this.image?.includes('https')
    ) {
      this.image = 'https://ushare.hn.ss.bfcplatform.vn/' + this.image;
    }
  }
}

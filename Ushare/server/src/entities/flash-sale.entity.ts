import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { FlashSaleDetailEntity } from './flash-sale-detail.entity';

@Entity('flash_sale')
export class FlashSaleEntity extends BaseEntity {
  @Column()
  @ApiProperty({
    description: 'Tên chương trình Flash Sale',
  })
  name: string;

  @Column({
    type: 'timestamp with time zone',
  })
  @ApiProperty({
    description: 'Thời gian bắt đầu chương trình Flash Sale',
    type: String,
  })
  startTime: Date;

  @Column({
    type: 'timestamp with time zone',
  })
  @ApiProperty({
    description: 'Thời gian kết thúc chương trình Flash Sale',
    type: String,
  })
  endTime: Date;

  @Column({
    default: 0,
  })
  @ApiProperty({
    description: 'Số lượng khuyến mãi 1 NCC được phép bán ra',
    required: false,
  })
  maxQuantity: number;

  @Column({
    default: 0,
  })
  @ApiProperty({
    description: 'Số lượng sản phẩm tối đa được tham gia của 1 NCC',
    required: false,
  })
  maxProductNumber?: number;

  @ApiProperty({
    description: 'Danh sách sản phẩm trong chương trình',
    type: () => [FlashSaleDetailEntity],
  })
  @OneToMany(() => FlashSaleDetailEntity, (detail) => detail.flashSale)
  flashSaleDetail?: FlashSaleDetailEntity[];
}

import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from 'entities/base.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { NotificationEntity } from './notification.entity';
import { UserEntity } from './user.entity';

@Entity('notification_user')
export class NotificationUserEntity extends BaseEntity {

  @Column({
    nullable: false,
  })
  @ApiProperty({
    description: 'ID Thông báo',
    type: Number,
    required: true,
  })
  notificationId: number;

  @Column({
    nullable: false,
  })
  @ApiProperty({
    description: 'ID user',
    type: Number,
    required: true,
  })
  userId?: number;

  @ApiProperty({
    description: 'Đã đọc',
    type: Boolean,
  })
  @Column({
    type: 'boolean',
    nullable: false,
    default: false
  })
  read: boolean;

  @ApiProperty({ description: 'Thông báo', type: () => NotificationEntity })
  @ManyToOne(() => NotificationEntity, (notification) => notification.id)
  notification?: NotificationEntity;

  @ApiProperty({ description: 'User', type: () => UserEntity })
  @ManyToOne(() => UserEntity, (user) => user.id)
  user?: UserEntity;
}

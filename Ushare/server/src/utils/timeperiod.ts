import * as dateFns from 'date-fns';
import {
  eachDayOfInterval,
  eachMonthOfInterval,
  eachQuarterOfInterval,
  eachWeekOfInterval,
  eachYearOfInterval,
} from 'date-fns';
import { TimePeriod } from 'enums';

export enum TimePeriodBucket {
  day = '1 day',
  week = '1 week',
  month = '1 month',
  quarter = '3 month',
  year = '1 year',
}

enum TimePeriodBucketFun {
  day = 'time_bucket',
  week = 'time_bucket',
  month = 'timescaledb_experimental.time_bucket_ng',
  quarter = 'timescaledb_experimental.time_bucket_ng',
  year = 'timescaledb_experimental.time_bucket_ng',
}

const timeIntervalFn = {
  day: eachDayOfInterval,
  week: (interval) => {
    return eachWeekOfInterval(interval, { weekStartsOn: 1 });
  },
  month: eachMonthOfInterval,
  quarter: eachQuarterOfInterval,
  year: eachYearOfInterval,
};

export const bucketFn = (type: TimePeriod, startTime: Date, column: string) => {
  const fn = TimePeriodBucketFun[type] || TimePeriodBucketFun.day;
  const bucketTime = TimePeriodBucket[type] || TimePeriodBucket.day;
  switch (fn) {
    case 'time_bucket':
      return `time_bucket('${bucketTime}', ${column}, TIMESTAMPTZ '${startTime.toISOString()}')`;
    case 'timescaledb_experimental.time_bucket_ng':
      return `timescaledb_experimental.time_bucket_ng('${bucketTime}', ${column}, timezone => 'Asia/Ho_Chi_Minh')`;
  }
};

export const eachTimeInterval = (type: TimePeriod) => {
  return timeIntervalFn[type] || timeIntervalFn.day;
};

export const formatTimePeriod = (
  startTime: Date,
  endTime: Date,
  type: TimePeriod,
) => {
  switch (type) {
    case TimePeriod.week:
      return [
        dateFns.startOfWeek(startTime, { weekStartsOn: 1 }),
        dateFns.endOfWeek(endTime, { weekStartsOn: 1 }),
      ];
    case TimePeriod.month:
      return [dateFns.startOfMonth(startTime), dateFns.endOfMonth(endTime)];
    case TimePeriod.quarter:
      return [dateFns.startOfQuarter(startTime), dateFns.endOfQuarter(endTime)];
    case TimePeriod.year:
      return [dateFns.startOfYear(startTime), dateFns.endOfYear(endTime)];
    default:
      return [dateFns.startOfDay(startTime), dateFns.endOfDay(endTime)];
  }
};

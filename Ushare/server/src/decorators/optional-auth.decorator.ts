import { applyDecorators, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { OptionalAuthGuard } from 'guards/optional-auth.guard';

/**
 * dùng cho public api. nếu truyền bearer token thì attach userEntity. nếu không truyền thì KHÔNG báo lỗi unauthorized.
 */
export function OptionalAuth() {
  return applyDecorators(ApiBearerAuth(), UseGuards(OptionalAuthGuard));
}

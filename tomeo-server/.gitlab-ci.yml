cache:
  key: ${CI_PROJECT_NAME}
  paths:
    - node_modules/
    - .yarn

variables:
  CONTAINER_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  CONTAINER_NAME: ushare-server

stages:
  - install-dependencies
  - build
  - release-staging
  - deploy-staging
  - release-prod
  - deploy-prod

install-dependencies: 
  image: node:16.11-alpine
  stage: install-dependencies
  before_script:
    - rm -rf ./node_modules/.cache
  script:
    - ls -l
    - yarn config set cache-folder .yarn
    - yarn install --prefer-offline --pure-lockfile --cache-folder .yarn &> /dev/null
  only:
    refs:
      - develop
      - staging
      - main
    changes:
      - package.json
  tags:
    - docker
  allow_failure: false
  when: always

build:
  image: node:16.11-alpine
  stage: build
  dependencies:
    - install-dependencies
  script:
    - yarn build
  artifacts:
    paths:
      - dist/
  tags:
    - docker
  only:
    - develop
    - staging
    - main
  allow_failure: false
  when: always

.release:
  image: docker:20.10-git
  stage: .release
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
  services:
    - docker:20.10-dind
  dependencies:
    - build
  script:
    - docker build --platform linux/amd64 -t ${CONTAINER_IMAGE} -f ${ENV}.Dockerfile .
    - docker tag ${CONTAINER_IMAGE} $CI_REGISTRY_IMAGE:${ENV}
    - docker login -u ${CI_REGISTRY_USER} -p $CI_REGISTRY_PASSWORD ${CI_REGISTRY}
    - docker push $CI_REGISTRY_IMAGE:${ENV}
  tags:
    - docker
  allow_failure: false


release-staging:
  extends: .release
  stage: release-staging
  variables:
    ENV: staging
  only:
    - staging
  when: always

release-prod:
  extends: .release
  stage: release-prod
  variables:
    ENV: prod
  only:
    - main
  when: always


.deploy:
  image: alpine:latest
  stage: .deploy
  variables:
    GIT_STRATEGY: none
  before_script:
    - 'command -v ssh-agent >/dev/null || ( apk add --update openssh )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan -p $SSH_SERVER_PORT $SSH_SERVER_IP >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - >
      ssh $SSH_USER@$SSH_SERVER_IP -p $SSH_SERVER_PORT "
      mkdir -p /root/ushare-root;
      cd /root/ushare-root;

      cat <<EOF | tee /root/ushare-root/docker-compose.yml

      services:
        ${CONTAINER_NAME}:
          image: $CI_REGISTRY_IMAGE:${ENV}
          container_name: ${CONTAINER_NAME}
          ports:
            - 4005:4000
          restart: unless-stopped
      EOF

      docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY};
      docker-compose pull ${CONTAINER_NAME};
      docker stop ${CONTAINER_NAME};
      docker-compose up -d ${CONTAINER_NAME};
      docker image prune -f;"

deploy-staging:
  extends: .deploy
  variables:
    ENV: staging
    SSH_PRIVATE_KEY: ${SSH_PRIVATE_KEY_STG}
    SSH_USER: ${SSH_USER_STG}
    SSH_SERVER_IP: ${SSH_SERVER_IP_STG}
    SSH_SERVER_PORT: ${SSH_SERVER_PORT_STG}
  stage: deploy-staging
  only:
    - staging
  dependencies:
    - release-staging
  when: always

deploy-prod:
  extends: .deploy
  variables:
    ENV: prod
    SSH_PRIVATE_KEY: ${SSH_PRIVATE_KEY_PROD}
    SSH_USER: ${SSH_USER_PROD}
    SSH_SERVER_IP: ${SSH_SERVER_IP_PROD}
    SSH_SERVER_PORT: ${SSH_SERVER_PORT_PROD}
  stage: deploy-prod
  only:
    - main
  dependencies:
    - release-prod
  when: always

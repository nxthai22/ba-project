import { gql } from 'apollo-angular';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export interface Scalars {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
}

export interface Address {
  address?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  countryId?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['Float']>;
  district?: Maybe<Scalars['String']>;
  districtId?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  longitude?: Maybe<Scalars['Float']>;
  owner?: Maybe<User>;
  ownerId?: Maybe<Scalars['String']>;
  province?: Maybe<Scalars['String']>;
  provinceId?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['Float']>;
  ward?: Maybe<Scalars['String']>;
  wardId?: Maybe<Scalars['String']>;
}

export interface AddressZone {
  code?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['Float']>;
  english?: Maybe<Scalars['String']>;
  fullname?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  level?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  parent?: Maybe<AddressZone>;
  updatedAt?: Maybe<Scalars['Float']>;
}

export interface AddressZoneFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  parentId?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['Int']>;
  updatedAt?: InputMaybe<Scalars['Float']>;
}

export interface AddressZoneResponse {
  count: Scalars['Int'];
  total: Scalars['Int'];
  zones?: Maybe<Array<AddressZone>>;
}

export interface AppIncidentCancelAbortArgs {
  incidentId: Scalars['String'];
}

export interface AppIncidentCancelAcceptArgs {
  incidentId: Scalars['String'];
}

export interface AppIncidentCancelArgs {
  cancelReasonId: Scalars['String'];
  incidentId: Scalars['String'];
  note?: InputMaybe<Scalars['String']>;
}

export interface AppIncidentCancelDenyArgs {
  incidentId: Scalars['String'];
  note?: InputMaybe<Scalars['String']>;
}

export interface AppIncidentCheckInArgs {
  checkInAt?: InputMaybe<Scalars['Float']>;
  incidentId: Scalars['String'];
  latitude?: InputMaybe<Scalars['Float']>;
  longitude?: InputMaybe<Scalars['Float']>;
}

export interface AppIncidentCloseArgs {
  appointmentAt?: InputMaybe<Scalars['Float']>;
  checkOutAt?: InputMaybe<Scalars['Float']>;
  componentFee?: InputMaybe<Scalars['Float']>;
  distanceMoving?: InputMaybe<Scalars['Float']>;
  expireReason?: InputMaybe<Scalars['String']>;
  incidentId: Scalars['String'];
  latitude?: InputMaybe<Scalars['Float']>;
  longitude?: InputMaybe<Scalars['Float']>;
  measurement?: InputMaybe<Scalars['String']>;
  note?: InputMaybe<Scalars['String']>;
  productFee?: InputMaybe<Scalars['Float']>;
  productImageIds?: InputMaybe<Array<Scalars['String']>>;
  productManufactureAt?: InputMaybe<Scalars['Float']>;
  productPurchaseAt?: InputMaybe<Scalars['Float']>;
  productSerial?: InputMaybe<Scalars['String']>;
  productTypeId?: InputMaybe<Scalars['String']>;
  reportImageIds?: InputMaybe<Array<Scalars['String']>>;
  serviceFee?: InputMaybe<Scalars['Float']>;
  supportResult?: InputMaybe<Scalars['String']>;
  troubleAnalysisId?: InputMaybe<Scalars['String']>;
  troubleGroupId?: InputMaybe<Scalars['String']>;
  troubleReasonId?: InputMaybe<Scalars['String']>;
  warrantyTypeId?: InputMaybe<Scalars['String']>;
}

export interface AppIncidentFilterArgs {
  createdAt?: InputMaybe<Scalars['Float']>;
  groupId?: InputMaybe<Scalars['String']>;
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  progressCode?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
  technicianId?: InputMaybe<Scalars['String']>;
}

export interface AppIncidentMoveToWarrantyArgs {
  incidentId: Scalars['String'];
  items?: InputMaybe<Array<WarrantyItemArgs>>;
  productManufactureAt?: InputMaybe<Scalars['Float']>;
  productPurchaseAt?: InputMaybe<Scalars['Float']>;
  productSerial?: InputMaybe<Scalars['String']>;
  productTypeId?: InputMaybe<Scalars['String']>;
  warrantyTypeId?: InputMaybe<Scalars['String']>;
}

export interface AppIncidentStatisticItem {
  code?: Maybe<Scalars['Int']>;
  count?: Maybe<Scalars['Int']>;
  progress?: Maybe<Scalars['String']>;
}

export interface AppIncidentStatisticResponse {
  count?: Maybe<Scalars['Int']>;
  items?: Maybe<Array<AppIncidentStatisticItem>>;
  total?: Maybe<Scalars['Int']>;
}

export interface AppIncidentTransferAcceptArgs {
  incidentId: Scalars['String'];
}

export interface AppIncidentTransferArgs {
  incidentId: Scalars['String'];
  note?: InputMaybe<Scalars['String']>;
  technicianId: Scalars['String'];
}

export interface AppIncidentTransferCancelArgs {
  incidentId: Scalars['String'];
}

export interface AppIncidentTransferRejectArgs {
  incidentId: Scalars['String'];
}

export interface AppIncidentUpdateArgs {
  appointmentAt?: InputMaybe<Scalars['Float']>;
  componentFee?: InputMaybe<Scalars['Float']>;
  distanceMoving?: InputMaybe<Scalars['Float']>;
  expireReason?: InputMaybe<Scalars['String']>;
  incidentId: Scalars['String'];
  measurement?: InputMaybe<Scalars['String']>;
  note?: InputMaybe<Scalars['String']>;
  productFee?: InputMaybe<Scalars['Float']>;
  productImageIds?: InputMaybe<Array<Scalars['String']>>;
  productManufactureAt?: InputMaybe<Scalars['Float']>;
  productPurchaseAt?: InputMaybe<Scalars['Float']>;
  productSerial?: InputMaybe<Scalars['String']>;
  productTypeId?: InputMaybe<Scalars['String']>;
  reportImageIds?: InputMaybe<Array<Scalars['String']>>;
  serviceFee?: InputMaybe<Scalars['Float']>;
  supportResult?: InputMaybe<Scalars['String']>;
  troubleAnalysisId?: InputMaybe<Scalars['String']>;
  troubleGroupId?: InputMaybe<Scalars['String']>;
  troubleReasonId?: InputMaybe<Scalars['String']>;
  warrantyTypeId?: InputMaybe<Scalars['String']>;
}

export interface AppInstallationArgs {
  acceptanceImageIds?: InputMaybe<Array<Scalars['String']>>;
  affairWarrantyTypeId?: InputMaybe<Scalars['String']>;
  appointmentAt?: InputMaybe<Scalars['Float']>;
  installationAt?: InputMaybe<Scalars['Float']>;
  note?: InputMaybe<Scalars['String']>;
  orderTypeId?: InputMaybe<Scalars['String']>;
  receiptImageIds?: InputMaybe<Array<Scalars['String']>>;
  resolvedAt?: InputMaybe<Scalars['Float']>;
  resultId?: InputMaybe<Scalars['String']>;
  supportResultId?: InputMaybe<Scalars['String']>;
}

export interface AppInstallationFilterArgs {
  createdAt?: InputMaybe<Scalars['Float']>;
  installationAt?: InputMaybe<Scalars['Float']>;
  page?: InputMaybe<Scalars['Int']>;
  resultId?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['Int']>;
}

export interface AppNotificationFilterArgs {
  isRead?: InputMaybe<Scalars['Boolean']>;
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
}

export interface AppNotificationUpdateArgs {
  isRead?: InputMaybe<Scalars['Boolean']>;
  noticationId: Scalars['String'];
}

export interface AppProductComponentFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Float']>;
  productTypeId?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['Float']>;
}

export interface AppScheduleFilterArgs {
  fromDate: Scalars['Float'];
  toDate: Scalars['Float'];
}

export interface AppUserFilterArgs {
  addressZoneId?: InputMaybe<Scalars['String']>;
  fullname?: InputMaybe<Scalars['String']>;
  groupId?: InputMaybe<Scalars['String']>;
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  roleCode?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
  statusCode?: InputMaybe<Scalars['Int']>;
}

export interface AppUserUpdateArgs {
  isActive?: InputMaybe<Scalars['Boolean']>;
  isLeader?: InputMaybe<Scalars['Boolean']>;
  isTechnician?: InputMaybe<Scalars['Boolean']>;
  leaderGroupIds?: InputMaybe<Array<Scalars['String']>>;
  technicianGroupId?: InputMaybe<Scalars['String']>;
  userId: Scalars['String'];
  workingZoneId?: InputMaybe<Scalars['String']>;
}

export interface Bank {
  code?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  en_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  vn_name?: Maybe<Scalars['String']>;
}

export interface BankAccount {
  accountHolder?: Maybe<Scalars['String']>;
  accountNumber?: Maybe<Scalars['String']>;
  bankBranch?: Maybe<Scalars['String']>;
  bankName?: Maybe<Scalars['String']>;
  cardNumber?: Maybe<Scalars['String']>;
  id: Scalars['String'];
}

export interface BankFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Float']>;
  size?: InputMaybe<Scalars['Float']>;
}

export interface BankResponse {
  banks?: Maybe<Array<Bank>>;
  count: Scalars['Int'];
  total: Scalars['Int'];
}

export interface BitrixGetLeadArgs {
  delay: Scalars['Float'];
  from?: InputMaybe<Scalars['Float']>;
  preName: Scalars['String'];
  start: Scalars['Int'];
  to?: InputMaybe<Scalars['Float']>;
}

export interface BitrixLead {
  BP_PUBLISHED?: Maybe<Scalars['String']>;
  CODE?: Maybe<Scalars['String']>;
  CREATED_BY?: Maybe<Scalars['String']>;
  DATE_CREATE?: Maybe<Scalars['String']>;
  IBLOCK_ID?: Maybe<Scalars['String']>;
  IBLOCK_SECTION_ID?: Maybe<Scalars['String']>;
  ID?: Maybe<Scalars['String']>;
  NAME?: Maybe<Scalars['String']>;
  PROPERTY_356?: Maybe<Scalars['String']>;
  PROPERTY_359?: Maybe<Scalars['String']>;
  PROPERTY_360?: Maybe<Scalars['String']>;
  PROPERTY_361?: Maybe<Scalars['String']>;
  PROPERTY_362?: Maybe<Scalars['String']>;
  PROPERTY_369?: Maybe<Scalars['String']>;
  PROPERTY_370?: Maybe<Scalars['String']>;
  PROPERTY_497?: Maybe<Scalars['String']>;
  bitrixCreatedAt?: Maybe<Scalars['Float']>;
  createdAt: Scalars['Float'];
  eco248LeadId?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  no: Scalars['Float'];
  syncToBitrixEco248?: Maybe<Scalars['Float']>;
  updatedAt: Scalars['Float'];
}

export interface BitrixStopTriggerResponse {
  timeoutNames?: Maybe<Array<Scalars['String']>>;
  total: Scalars['Int'];
}

export interface BitrixTriggerResponse {
  timeouts?: Maybe<Array<TimeOutObj>>;
  total: Scalars['Int'];
}

export interface BusinessRole {
  address?: Maybe<Address>;
  agreementUrl?: Maybe<Scalars['String']>;
  bankAccount?: Maybe<BankAccount>;
  code?: Maybe<Scalars['String']>;
  contentUrl?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Organization>;
  status: Scalars['String'];
}

export interface BusinessRoleResponse {
  businessRoles?: Maybe<Array<BusinessRole>>;
  count: Scalars['Int'];
  total: Scalars['Int'];
}

export interface Carrier {
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
}

export interface CarrierFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Float']>;
  size?: InputMaybe<Scalars['Float']>;
}

export interface CarrierResponse {
  carriers?: Maybe<Array<Carrier>>;
  count: Scalars['Int'];
  total: Scalars['Int'];
}

export interface Complain {
  complainHandler?: Maybe<CrmUser>;
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  incident?: Maybe<Incident>;
  measurement?: Maybe<Scalars['String']>;
  progress: Scalars['String'];
  updatedAt: Scalars['Float'];
  vendors?: Maybe<Array<ComplainVendor>>;
}

export interface ComplainArgs {
  complainId: Scalars['String'];
  measurement?: InputMaybe<Scalars['String']>;
}

export interface ComplainCloseArgs {
  complainId: Scalars['String'];
}

export interface ComplainComment {
  comment?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  sender?: Maybe<CrmUser>;
  senderDepartment?: Maybe<Department>;
  updatedAt: Scalars['Float'];
  vendor?: Maybe<ComplainVendor>;
}

export interface ComplainCommentArgs {
  comment: Scalars['String'];
  complainId: Scalars['String'];
  vendorId: Scalars['String'];
}

export interface ComplainDepartmentFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
}

export interface ComplainFilterArgs {
  appointmentFromAt?: InputMaybe<Scalars['Float']>;
  appointmentToAt?: InputMaybe<Scalars['Float']>;
  contactResultId?: InputMaybe<Scalars['String']>;
  departmentId?: InputMaybe<Scalars['String']>;
  keyword?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  phone?: InputMaybe<Scalars['String']>;
  priorityId?: InputMaybe<Scalars['String']>;
  productName?: InputMaybe<Scalars['String']>;
  progressCode?: InputMaybe<Scalars['Int']>;
  requestGroupId?: InputMaybe<Scalars['String']>;
  requestTypeId?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['Int']>;
  sourceId?: InputMaybe<Scalars['String']>;
}

export interface ComplainMoveToVendorArgs {
  complainId: Scalars['String'];
  departmentId: Scalars['String'];
  note?: InputMaybe<Scalars['String']>;
}

export interface ComplainResponse {
  complains?: Maybe<Array<Complain>>;
  count: Scalars['Int'];
  total: Scalars['Int'];
}

export interface ComplainVendor {
  comments?: Maybe<Array<ComplainComment>>;
  complain?: Maybe<Complain>;
  createdAt: Scalars['Float'];
  department?: Maybe<Department>;
  id: Scalars['String'];
  note?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface Configuration {
  businessRoles?: Maybe<Array<BusinessRole>>;
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  menu?: Maybe<Array<ConfigurationMenu>>;
  updatedAt: Scalars['Float'];
}

export interface ConfigurationMenu {
  businessRole: BusinessRole;
  createdAt: Scalars['Float'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  items?: Maybe<Array<ConfigurationMenuItem>>;
  updatedAt: Scalars['Float'];
}

export interface ConfigurationMenuItem {
  childs?: Maybe<Array<ConfigurationMenuItem>>;
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  metadata?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  paren?: Maybe<ConfigurationMenuItem>;
  updatedAt: Scalars['Float'];
}

export interface CrmAddress {
  address?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  countryId?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  district?: Maybe<Scalars['String']>;
  districtId?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  latitude?: Maybe<Scalars['Float']>;
  longitude?: Maybe<Scalars['Float']>;
  province?: Maybe<Scalars['String']>;
  provinceId?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
  ward?: Maybe<Scalars['String']>;
  wardId?: Maybe<Scalars['String']>;
}

export interface CrmCustomer {
  address?: Maybe<CrmAddress>;
  createdAt: Scalars['Float'];
  dealCount?: Maybe<Scalars['Int']>;
  id: Scalars['String'];
  incidentHistory?: Maybe<Array<Incident>>;
  name?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  phones?: Maybe<Array<Scalars['String']>>;
  productHistory?: Maybe<Array<CrmProductType>>;
  recognizedAt?: Maybe<Scalars['Float']>;
  revenue?: Maybe<Scalars['Float']>;
  totalRevenue?: Maybe<Scalars['Float']>;
  updatedAt: Scalars['Float'];
}

export interface CrmCustomerResponse {
  count: Scalars['Int'];
  customers?: Maybe<Array<CrmCustomer>>;
  total: Scalars['Int'];
}

export interface CrmNotification {
  content?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  image?: Maybe<Scalars['String']>;
  isRead: Scalars['Boolean'];
  metadata?: Maybe<Scalars['String']>;
  receivers?: Maybe<Array<CrmUser>>;
  title?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface CrmNotificationFilterArgs {
  isRead?: InputMaybe<Scalars['Boolean']>;
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
}

export interface CrmNotificationResponse {
  count: Scalars['Int'];
  notifications?: Maybe<Array<CrmNotification>>;
  total: Scalars['Int'];
}

export interface CrmPermissionAction {
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  service?: Maybe<CrmPermissionService>;
  updatedAt: Scalars['Float'];
}

export interface CrmPermissionPolicy {
  actions?: Maybe<Array<CrmPermissionAction>>;
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface CrmPermissionRole {
  createdAt: Scalars['Float'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  items?: Maybe<Array<CrmPermissionPolicy>>;
  name?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface CrmPermissionService {
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface CrmProductBrand {
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface CrmProductCategory {
  brand?: Maybe<CrmProductBrand>;
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  factoryWarrantyMonths?: Maybe<Scalars['Float']>;
  id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
  warrantyMonths?: Maybe<Scalars['Float']>;
}

export interface CrmProductComponent {
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  description?: Maybe<Scalars['String']>;
  hasWarranty: Scalars['Boolean'];
  id: Scalars['String'];
  imageUrls?: Maybe<Array<Scalars['String']>>;
  images?: Maybe<Array<File>>;
  internalPrice?: Maybe<Scalars['Float']>;
  listedPrice?: Maybe<Scalars['Float']>;
  model?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  productBrand?: Maybe<CrmProductBrand>;
  productType?: Maybe<CrmProductType>;
  productTypes?: Maybe<Array<CrmProductType>>;
  quantity?: Maybe<Scalars['Int']>;
  status?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
  warrantyMonths?: Maybe<Scalars['Float']>;
}

export interface CrmProductType {
  category?: Maybe<CrmProductCategory>;
  code?: Maybe<Scalars['String']>;
  components?: Maybe<Array<CrmProductComponent>>;
  createdAt: Scalars['Float'];
  description?: Maybe<Scalars['String']>;
  factoryWarrantyMonths?: Maybe<Scalars['Float']>;
  id: Scalars['String'];
  listedPrice?: Maybe<Scalars['Float']>;
  model?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  productImageUrls?: Maybe<Array<Scalars['String']>>;
  status: Scalars['String'];
  updatedAt: Scalars['Float'];
  warrantyMonths?: Maybe<Scalars['Float']>;
}

export interface CrmUser {
  address?: Maybe<CrmAddress>;
  avatarUrl?: Maybe<Scalars['String']>;
  businessRoles?: Maybe<Array<BusinessRole>>;
  configuration?: Maybe<Configuration>;
  createdAt: Scalars['Float'];
  department?: Maybe<Department>;
  departments?: Maybe<Array<Department>>;
  email?: Maybe<Scalars['String']>;
  employeeNo?: Maybe<Scalars['String']>;
  fullname?: Maybe<Scalars['String']>;
  groups?: Maybe<Array<CrmUserGroup>>;
  id: Scalars['String'];
  isLeader: Scalars['Boolean'];
  leaderGroupIds?: Maybe<Array<Scalars['String']>>;
  leaderGroups?: Maybe<Array<CrmUserGroup>>;
  pendingIncidentCount: Scalars['Int'];
  phone?: Maybe<Scalars['String']>;
  phoneExt?: Maybe<Scalars['Int']>;
  phones?: Maybe<Array<Scalars['String']>>;
  sessionBusinessRole?: Maybe<BusinessRole>;
  sessionMultiBusinessRoles?: Maybe<Array<BusinessRole>>;
  status: Scalars['String'];
  technicianGroup?: Maybe<CrmUserGroup>;
  technicianGroupId?: Maybe<Scalars['String']>;
  unreadNotificationCount?: Maybe<Scalars['Int']>;
  updatedAt: Scalars['Float'];
  username?: Maybe<Scalars['String']>;
  workingAddress?: Maybe<CrmAddress>;
}

export interface CrmUserArgs {
  address?: InputMaybe<Scalars['String']>;
  addressZoneId?: InputMaybe<Scalars['String']>;
  businessRoleId?: InputMaybe<Scalars['String']>;
  departmentId?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
  employeeNo?: InputMaybe<Scalars['String']>;
  fullname?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  phones?: InputMaybe<Array<Scalars['String']>>;
  statusCode?: InputMaybe<Scalars['Int']>;
}

export interface CrmUserFilterArgs {
  businessRoleId?: InputMaybe<Scalars['String']>;
  employeeNo?: InputMaybe<Scalars['String']>;
  fullname?: InputMaybe<Scalars['String']>;
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
  statusCode?: InputMaybe<Scalars['Int']>;
}

export interface CrmUserGroup {
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  leader?: Maybe<CrmUser>;
  name?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface CrmUserLoginArgs {
  email: Scalars['String'];
  organizationId: Scalars['String'];
  password: Scalars['String'];
}

export interface CrmUserLoginResponse {
  accessToken?: Maybe<Scalars['String']>;
  loggedInTime?: Maybe<Scalars['Float']>;
  refreshToken?: Maybe<Scalars['String']>;
  user?: Maybe<CrmUser>;
}

export interface CrmUserResponse {
  count: Scalars['Int'];
  total: Scalars['Int'];
  users?: Maybe<Array<CrmUser>>;
}

export interface CrmUserUpdateArgs {
  address?: InputMaybe<Scalars['String']>;
  addressZoneId?: InputMaybe<Scalars['String']>;
  businessRoleId?: InputMaybe<Scalars['String']>;
  departmentId?: InputMaybe<Scalars['String']>;
  employeeNo?: InputMaybe<Scalars['String']>;
  fullname?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  phones?: InputMaybe<Array<Scalars['String']>>;
  statusCode?: InputMaybe<Scalars['Int']>;
  userId: Scalars['String'];
}

export interface CustomerFilterArgs {
  addressZoneId?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['Float']>;
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
}

export interface CustomerFindOneArgs {
  phone?: InputMaybe<Scalars['String']>;
}

export interface CustomerHistoryFilterArgs {
  customerId: Scalars['String'];
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
}

export interface Deal {
  address?: Maybe<CrmAddress>;
  adminNote?: Maybe<Scalars['String']>;
  closeDealAt?: Maybe<Scalars['Float']>;
  collaboratorDiscount?: Maybe<Scalars['Float']>;
  createdAt: Scalars['Float'];
  customerCharge?: Maybe<Scalars['Float']>;
  dealResult?: Maybe<OptionItem>;
  ecoDeposit?: Maybe<Scalars['Float']>;
  giveFilterCount?: Maybe<Scalars['Int']>;
  id: Scalars['String'];
  isGiveFilter?: Maybe<Scalars['Boolean']>;
  karofiDeposit?: Maybe<Scalars['Float']>;
  ktcReason?: Maybe<OptionItem>;
  lead?: Maybe<Lead>;
  marketingProgram?: Maybe<OptionItem>;
  no: Scalars['Float'];
  note?: Maybe<Scalars['String']>;
  orderId?: Maybe<Scalars['String']>;
  products?: Maybe<Array<DealProduct>>;
  promotions?: Maybe<Array<OptionItem>>;
  reason?: Maybe<Scalars['String']>;
  repaymentAmount?: Maybe<Scalars['Float']>;
  revenue?: Maybe<Scalars['Float']>;
  serviceFee?: Maybe<Scalars['Float']>;
  totalListedPrice: Scalars['Float'];
  totalPrice: Scalars['Float'];
  updatedAt: Scalars['Float'];
  warehouseExportAt?: Maybe<Scalars['Float']>;
}

export interface DealArgs {
  adminNote?: InputMaybe<Scalars['String']>;
  closeDealAt?: InputMaybe<Scalars['Float']>;
  collaboratorDiscount?: InputMaybe<Scalars['Float']>;
  customerCharge?: InputMaybe<Scalars['Float']>;
  dealKtcReasonId?: InputMaybe<Scalars['String']>;
  dealProducts?: InputMaybe<Array<DealProductArgs>>;
  dealResultId?: InputMaybe<Scalars['String']>;
  ecoDeposit?: InputMaybe<Scalars['Float']>;
  isGiveFilter?: InputMaybe<Scalars['Boolean']>;
  karofiDeposit?: InputMaybe<Scalars['Float']>;
  maketingProgramId?: InputMaybe<Scalars['String']>;
  orderId?: InputMaybe<Scalars['String']>;
  promotionIds?: InputMaybe<Array<Scalars['String']>>;
  repaymentAmount?: InputMaybe<Scalars['Float']>;
  revenue?: InputMaybe<Scalars['Float']>;
  serviceFee?: InputMaybe<Scalars['Float']>;
  warehouseExportAt?: InputMaybe<Scalars['Float']>;
}

export interface DealFilterArgs {
  exportDay?: InputMaybe<Scalars['Float']>;
  keyword?: InputMaybe<Scalars['String']>;
  orderId?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  productTypeId?: InputMaybe<Scalars['String']>;
  resultId?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['Int']>;
}

export interface DealProduct {
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  price?: Maybe<Scalars['Float']>;
  productType?: Maybe<CrmProductType>;
  quantity?: Maybe<Scalars['Float']>;
  serial?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface DealProductArgs {
  code?: InputMaybe<Scalars['String']>;
  price: Scalars['Int'];
  productTypeId: Scalars['String'];
  quantity: Scalars['Int'];
  serial?: InputMaybe<Scalars['String']>;
}

export interface DealResponse {
  count: Scalars['Int'];
  deals?: Maybe<Array<Deal>>;
  total: Scalars['Int'];
}

export interface Delivery {
  accountantNote?: Maybe<Scalars['String']>;
  billOfLading?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  deal?: Maybe<Deal>;
  deliveryAt?: Maybe<Scalars['Float']>;
  id: Scalars['String'];
  no: Scalars['Float'];
  note?: Maybe<Scalars['String']>;
  shippingFee?: Maybe<Scalars['Float']>;
  shippingMethod?: Maybe<OptionItem>;
  shippingStatus?: Maybe<OptionItem>;
  shippingVendor?: Maybe<OptionItem>;
  updatedAt: Scalars['Float'];
  warehouse?: Maybe<Warehouse>;
  warehouseExportAt?: Maybe<Scalars['Float']>;
  warehouseLinkedAmount?: Maybe<Scalars['Float']>;
}

export interface DeliveryArgs {
  accountantNote?: InputMaybe<Scalars['String']>;
  billOfLading?: InputMaybe<Scalars['String']>;
  deliveryAt?: InputMaybe<Scalars['Float']>;
  note?: InputMaybe<Scalars['String']>;
  shippingFee?: InputMaybe<Scalars['Float']>;
  shippingMethodId?: InputMaybe<Scalars['String']>;
  shippingStatusId?: InputMaybe<Scalars['String']>;
  shippingVendorId?: InputMaybe<Scalars['String']>;
  warehouseExportAt?: InputMaybe<Scalars['Float']>;
  warehouseId?: InputMaybe<Scalars['String']>;
  warehouseLinkedAmount?: InputMaybe<Scalars['Float']>;
}

export interface DeliveryFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
}

export interface DeliveryResponse {
  count: Scalars['Int'];
  deliveries?: Maybe<Array<Delivery>>;
  total: Scalars['Int'];
}

export interface Department {
  businessRoleId?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  members?: Maybe<Array<CrmUser>>;
  name?: Maybe<Scalars['String']>;
  parent?: Maybe<Department>;
  status: Scalars['String'];
  updatedAt: Scalars['Float'];
}

export interface DepartmentAddMemberArgs {
  departmentId: Scalars['String'];
  memberIds: Array<Scalars['String']>;
}

export interface DepartmentArgs {
  businessRoleId?: InputMaybe<Scalars['String']>;
  memberIds?: InputMaybe<Array<Scalars['String']>>;
  name: Scalars['String'];
  parentId?: InputMaybe<Scalars['String']>;
}

export interface DepartmentFilterArgs {
  businessRoleId?: InputMaybe<Scalars['String']>;
  keyword?: InputMaybe<Scalars['String']>;
  memberId?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Float']>;
  parentId?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['Float']>;
  statusCode?: InputMaybe<Scalars['Int']>;
}

export interface DepartmentRemoveMemberArgs {
  departmentId: Scalars['String'];
  memberIds: Array<Scalars['String']>;
}

export interface DepartmentResponse {
  count: Scalars['Int'];
  departments?: Maybe<Array<Department>>;
  total: Scalars['Int'];
}

export interface DepartmentUpdateArgs {
  businessRoleId?: InputMaybe<Scalars['String']>;
  departmentId: Scalars['String'];
  memberIds?: InputMaybe<Array<Scalars['String']>>;
  name: Scalars['String'];
  parentId?: InputMaybe<Scalars['String']>;
  statusCode?: InputMaybe<Scalars['Int']>;
}

export interface Eco248Lead {
  ASSIGNED_BY_ID?: Maybe<Scalars['String']>;
  PHONE?: Maybe<Scalars['String']>;
  SOURCE_ID?: Maybe<Scalars['String']>;
  STATUS_ID?: Maybe<Scalars['String']>;
  TITLE?: Maybe<Scalars['String']>;
  UF_CRM_1606325864?: Maybe<Scalars['String']>;
  UF_CRM_1610013827?: Maybe<Scalars['String']>;
  UF_CRM_1607052064996?: Maybe<Scalars['String']>;
  UF_CRM_1607066687948?: Maybe<Scalars['String']>;
  UF_CRM_1608865188845?: Maybe<Scalars['String']>;
  bitrixCreatedAt?: Maybe<Scalars['Float']>;
  bitrixLeadId?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  eco248Id?: Maybe<Scalars['String']>;
  eco248Response?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  no: Scalars['Float'];
  syncToBitrixEco248?: Maybe<Scalars['Float']>;
  updatedAt: Scalars['Float'];
}

export interface Eco248LeadArgs {
  ASSIGNED_BY_ID?: InputMaybe<Scalars['String']>;
  PHONE?: InputMaybe<Scalars['String']>;
  SOURCE_ID?: InputMaybe<Scalars['String']>;
  STATUS_ID?: InputMaybe<Scalars['String']>;
  TITLE?: InputMaybe<Scalars['String']>;
  UF_CRM_1606325864?: InputMaybe<Scalars['String']>;
  UF_CRM_1610013827?: InputMaybe<Scalars['String']>;
  UF_CRM_1607052064996?: InputMaybe<Scalars['String']>;
  UF_CRM_1607066687948?: InputMaybe<Scalars['String']>;
  UF_CRM_1608865188845?: InputMaybe<Scalars['String']>;
}

export interface File {
  createdAt: Scalars['Float'];
  encoding?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  location?: Maybe<Scalars['String']>;
  mimetype?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  size?: Maybe<Scalars['Int']>;
  updatedAt: Scalars['Float'];
}

export interface ImportResponse {
  filename?: Maybe<Scalars['String']>;
  itemCount?: Maybe<Scalars['Int']>;
  itemImportedCount?: Maybe<Scalars['Int']>;
  itemUpdatedCount?: Maybe<Scalars['Int']>;
  logs?: Maybe<Array<Scalars['String']>>;
  typeCount?: Maybe<Scalars['Int']>;
  typeImportedCount?: Maybe<Scalars['Int']>;
  typeUpdatedCount?: Maybe<Scalars['Int']>;
}

export interface Incident {
  address?: Maybe<CrmAddress>;
  appointmentAt?: Maybe<Scalars['Float']>;
  cancelReason?: Maybe<OptionItem>;
  checkInAt?: Maybe<Scalars['Float']>;
  checkInLatitude?: Maybe<Scalars['Float']>;
  checkInLongitude?: Maybe<Scalars['Float']>;
  checkOutAt?: Maybe<Scalars['Float']>;
  checkOutLatitude?: Maybe<Scalars['Float']>;
  checkOutLongitude?: Maybe<Scalars['Float']>;
  componentFee?: Maybe<Scalars['Float']>;
  contactResult?: Maybe<OptionItem>;
  createdAt: Scalars['Float'];
  customer?: Maybe<CrmCustomer>;
  description?: Maybe<Scalars['String']>;
  distanceMoving?: Maybe<Scalars['Float']>;
  expireReason?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  internal?: Maybe<IncidentInternal>;
  measurement?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  no: Scalars['Float'];
  note?: Maybe<Scalars['String']>;
  partner?: Maybe<OptionItem>;
  phone?: Maybe<Scalars['String']>;
  phones?: Maybe<Array<Scalars['String']>>;
  priority?: Maybe<OptionItem>;
  productBrand?: Maybe<CrmProductBrand>;
  productCategory?: Maybe<CrmProductCategory>;
  productFee?: Maybe<Scalars['Float']>;
  productImageUrls?: Maybe<Array<Scalars['String']>>;
  productManufactureAt?: Maybe<Scalars['Float']>;
  productName?: Maybe<Scalars['String']>;
  productPurchaseAt?: Maybe<Scalars['Float']>;
  productSerial?: Maybe<Scalars['String']>;
  productType?: Maybe<CrmProductType>;
  progress?: Maybe<Scalars['String']>;
  progressCode: Scalars['Int'];
  reason?: Maybe<Scalars['String']>;
  receivedAt?: Maybe<Scalars['Float']>;
  reportImageUrls?: Maybe<Array<Scalars['String']>>;
  requestGroup?: Maybe<OptionItem>;
  requestType?: Maybe<OptionItem>;
  requesterCode?: Maybe<Scalars['String']>;
  requesterPhone?: Maybe<Scalars['String']>;
  resolveMethod?: Maybe<Scalars['String']>;
  serviceFee?: Maybe<Scalars['Float']>;
  sla: Scalars['Float'];
  source?: Maybe<OptionItem>;
  supportResult?: Maybe<Scalars['String']>;
  supportType?: Maybe<Scalars['String']>;
  technician?: Maybe<CrmUser>;
  transferFrom?: Maybe<CrmUser>;
  transferTo?: Maybe<CrmUser>;
  troubleAnalysis?: Maybe<OptionItem>;
  troubleGroup?: Maybe<OptionItem>;
  troubleReason?: Maybe<OptionItem>;
  updatedAt: Scalars['Float'];
  warrantyReceipts?: Maybe<Array<Warranty>>;
  warrantyType?: Maybe<OptionItem>;
}

export interface IncidentArgs {
  address?: InputMaybe<Scalars['String']>;
  addressZoneId?: InputMaybe<Scalars['String']>;
  appointmentAt?: InputMaybe<Scalars['Float']>;
  contactResultId?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  internalArgs?: InputMaybe<IncidentInternalArgs>;
  name?: InputMaybe<Scalars['String']>;
  partnerId?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  phones?: InputMaybe<Array<Scalars['String']>>;
  priorityId?: InputMaybe<Scalars['String']>;
  productBrandId?: InputMaybe<Scalars['String']>;
  productCategoryId?: InputMaybe<Scalars['String']>;
  productImageIds?: InputMaybe<Array<Scalars['String']>>;
  productName?: InputMaybe<Scalars['String']>;
  productPurchaseAt?: InputMaybe<Scalars['Float']>;
  productSerial?: InputMaybe<Scalars['String']>;
  productTypeId?: InputMaybe<Scalars['String']>;
  requestGroupId?: InputMaybe<Scalars['String']>;
  requestTypeId?: InputMaybe<Scalars['String']>;
  requesterCode?: InputMaybe<Scalars['String']>;
  requesterPhone?: InputMaybe<Scalars['String']>;
  sourceId?: InputMaybe<Scalars['String']>;
  supportResult?: InputMaybe<Scalars['String']>;
  technicianId?: InputMaybe<Scalars['String']>;
}

export interface IncidentFilterArgs {
  appointmentFromAt?: InputMaybe<Scalars['Float']>;
  appointmentToAt?: InputMaybe<Scalars['Float']>;
  contactResultId?: InputMaybe<Scalars['String']>;
  keyword?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  phone?: InputMaybe<Scalars['String']>;
  priorityId?: InputMaybe<Scalars['String']>;
  productName?: InputMaybe<Scalars['String']>;
  progressCode?: InputMaybe<Scalars['Int']>;
  requestGroupId?: InputMaybe<Scalars['String']>;
  requestTypeId?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['Int']>;
  sourceId?: InputMaybe<Scalars['String']>;
  technicianId?: InputMaybe<Scalars['String']>;
  typeCode?: InputMaybe<Scalars['Int']>;
}

export interface IncidentImportResponse {
  imported: Scalars['Int'];
  logs?: Maybe<Array<Scalars['String']>>;
  total: Scalars['Int'];
}

export interface IncidentInternal {
  complain?: Maybe<Complain>;
  complainHandler?: Maybe<CrmUser>;
  createdAt: Scalars['Float'];
  customerCareNote?: Maybe<Scalars['String']>;
  customerNote?: Maybe<Scalars['String']>;
  customerRequest?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  incident?: Maybe<Incident>;
  lead?: Maybe<Lead>;
  orderType?: Maybe<OptionItem>;
  reason?: Maybe<OptionItem>;
  response?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface IncidentInternalArgs {
  complainHandlerId?: InputMaybe<Scalars['String']>;
  customerCareNote?: InputMaybe<Scalars['String']>;
  customerRequest?: InputMaybe<Scalars['String']>;
  orderTypeId?: InputMaybe<Scalars['String']>;
  reasonId?: InputMaybe<Scalars['String']>;
  response?: InputMaybe<Scalars['String']>;
}

export interface IncidentResponse {
  count: Scalars['Int'];
  incidents?: Maybe<Array<Incident>>;
  total: Scalars['Int'];
}

export interface IncidentTemplateResponse {
  fileUrl?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
}

export interface Installation {
  acceptance?: Maybe<Scalars['String']>;
  acceptanceImageUrls?: Maybe<Array<Scalars['String']>>;
  acceptanceImages?: Maybe<Array<File>>;
  affairWarrantyType?: Maybe<OptionItem>;
  appointmentAt?: Maybe<Scalars['Float']>;
  createdAt: Scalars['Float'];
  deal?: Maybe<Deal>;
  delivery?: Maybe<Delivery>;
  id: Scalars['String'];
  installationAt?: Maybe<Scalars['Float']>;
  ktcReason?: Maybe<OptionItem>;
  lastReplaceFilterAt?: Maybe<Scalars['Float']>;
  no: Scalars['Float'];
  note?: Maybe<Scalars['String']>;
  purchaseAt?: Maybe<Scalars['Float']>;
  receiptImageUrls?: Maybe<Array<Scalars['String']>>;
  receiptImages?: Maybe<Array<File>>;
  remindAt?: Maybe<Scalars['Float']>;
  resolvedAt?: Maybe<Scalars['Float']>;
  result?: Maybe<OptionItem>;
  schedule?: Maybe<Schedule>;
  supportResult?: Maybe<OptionItem>;
  technician?: Maybe<CrmUser>;
  updatedAt: Scalars['Float'];
  workingShift?: Maybe<WorkingShift>;
}

export interface InstallationArgs {
  affairWarrantyTypeId?: InputMaybe<Scalars['String']>;
  appointmentAt?: InputMaybe<Scalars['Float']>;
  giveFilterCount?: InputMaybe<Scalars['Int']>;
  installationAt?: InputMaybe<Scalars['Float']>;
  installationKtcReasonId?: InputMaybe<Scalars['String']>;
  installationResultId?: InputMaybe<Scalars['String']>;
  lastReplaceFilterAt?: InputMaybe<Scalars['Float']>;
  note?: InputMaybe<Scalars['String']>;
  purchaseAt?: InputMaybe<Scalars['Float']>;
  remindAt?: InputMaybe<Scalars['Float']>;
  serviceFee?: InputMaybe<Scalars['Float']>;
  technicianId?: InputMaybe<Scalars['String']>;
  workingShiftId?: InputMaybe<Scalars['String']>;
}

export interface InstallationFilterArgs {
  appointmentAt?: InputMaybe<Scalars['Float']>;
  installationAt?: InputMaybe<Scalars['Float']>;
  keyword?: InputMaybe<Scalars['String']>;
  maxRevenue?: InputMaybe<Scalars['Float']>;
  minRevenue?: InputMaybe<Scalars['Float']>;
  page?: InputMaybe<Scalars['Int']>;
  resultId?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['Int']>;
  technicianId?: InputMaybe<Scalars['String']>;
}

export interface InstallationResponse {
  count: Scalars['Int'];
  installations?: Maybe<Array<Installation>>;
  total: Scalars['Int'];
}

export interface Lead {
  accessId?: Maybe<Scalars['String']>;
  address?: Maybe<CrmAddress>;
  collaborator?: Maybe<CrmUser>;
  content?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  customer?: Maybe<CrmCustomer>;
  id: Scalars['String'];
  ktcReason?: Maybe<OptionItem>;
  leadResult?: Maybe<OptionItem>;
  name?: Maybe<Scalars['String']>;
  no: Scalars['Float'];
  note?: Maybe<Scalars['String']>;
  orderType?: Maybe<OptionItem>;
  other?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  processedAt?: Maybe<Scalars['Float']>;
  promotion?: Maybe<Scalars['String']>;
  reason?: Maybe<Scalars['String']>;
  sale?: Maybe<CrmUser>;
  source?: Maybe<OptionItem>;
  storyboard?: Maybe<LeadStoryBoard>;
  updatedAt: Scalars['Float'];
}

export interface LeadArgs {
  accessId?: InputMaybe<Scalars['String']>;
  address?: InputMaybe<Scalars['String']>;
  addressZoneId?: InputMaybe<Scalars['String']>;
  collaboratorId?: InputMaybe<Scalars['String']>;
  content?: InputMaybe<Scalars['String']>;
  ktcReasonId?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  note?: InputMaybe<Scalars['String']>;
  orderTypeId?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  processedAt?: InputMaybe<Scalars['Float']>;
  resultId?: InputMaybe<Scalars['String']>;
  saleId?: InputMaybe<Scalars['String']>;
  sourceId?: InputMaybe<Scalars['String']>;
}

export interface LeadAssignFilterArgs {
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
  sourceCode?: InputMaybe<Scalars['Int']>;
}

export interface LeadAssignHistory {
  createdAt: Scalars['Float'];
  fileName?: Maybe<Scalars['String']>;
  fileUrl?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  reason?: Maybe<Scalars['String']>;
  source?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface LeadAssignHistoryResponse {
  count: Scalars['Int'];
  histories?: Maybe<Array<LeadAssignHistory>>;
  total: Scalars['Int'];
}

export interface LeadFilterArgs {
  collaboratorId?: InputMaybe<Scalars['String']>;
  createdDay?: InputMaybe<Scalars['Float']>;
  keyword?: InputMaybe<Scalars['String']>;
  orderTypeId?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  processedDay?: InputMaybe<Scalars['Float']>;
  resultId?: InputMaybe<Scalars['String']>;
  saleId?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['Int']>;
  sourceId?: InputMaybe<Scalars['String']>;
}

export interface LeadResponse {
  count: Scalars['Int'];
  leads?: Maybe<Array<Lead>>;
  total: Scalars['Int'];
}

export interface LeadStoryBoard {
  linkedDeals?: Maybe<Array<Deal>>;
  linkedDeliveries?: Maybe<Array<Delivery>>;
  linkedInstallations?: Maybe<Array<Installation>>;
}

export interface Mutation {
  appIncidentCancel: Incident;
  appIncidentCancelAbort: Incident;
  appIncidentCancelAccept: Incident;
  appIncidentCancelReject: Incident;
  appIncidentCheckIn: Incident;
  appIncidentClose: Incident;
  appIncidentMoveToWarranty: Incident;
  appIncidentTransfer: Incident;
  appIncidentTransferAbort: Incident;
  appIncidentTransferAccept: Incident;
  appIncidentTransferReject: Incident;
  appIncidentUpdate: Incident;
  appIncidentUploadImage: File;
  appInstallationUpdate: Installation;
  appInstallationUploadImage: File;
  appInstallationUploadReceipt: File;
  appNotificationGetDetail: Notification;
  appNotificationRemove: Notification;
  appNotificationUpdate: Notification;
  bitrixAddLead: Eco248Lead;
  crmAppUserUpdate: CrmUser;
  crmComplainAddComment: Complain;
  crmComplainClose: Complain;
  crmComplainMoveToVendor: Complain;
  crmComplainUpdate: Complain;
  crmDealMakeProfile: User;
  crmDealMoveToDelivery: Delivery;
  crmDealMoveToInstallation: Installation;
  crmDealRemove: Deal;
  crmDealUpdate: Deal;
  crmDeliveryMoveToInstallation: Installation;
  crmDeliveryRemove: Delivery;
  crmDeliveryUpdate: Delivery;
  crmDepartmentAddMembers: Department;
  crmDepartmentCreate: Department;
  crmDepartmentRemove: Scalars['Boolean'];
  crmDepartmentRemoveMembers: Department;
  crmDepartmentUpdate: Department;
  crmIncidentCreate: Incident;
  crmIncidentImport: IncidentImportResponse;
  crmIncidentSend: Incident;
  crmIncidentUpdate: Incident;
  crmIncidentUploadImage: File;
  crmInstallationRemove: Installation;
  crmInstallationUpdate: Installation;
  crmLeadAdd: Lead;
  crmLeadAssignWithExcelFile: LeadResponse;
  crmLeadAssignWithGooleLink: LeadResponse;
  crmLeadImport: LeadResponse;
  crmLeadMoveToDeal: Deal;
  crmLeadRemove: Lead;
  crmLeadUpdate: Lead;
  crmNotificationGetDetail: Scalars['Boolean'];
  crmOptionImport: ImportResponse;
  crmOptionItemAdd: OptionItem;
  crmOptionItemRemove: OptionItem;
  crmOptionItemUpdate: OptionItem;
  crmPermissionActionAdd: CrmPermissionAction;
  crmPermissionActionRemove: CrmPermissionAction;
  crmPermissionPolicyAdd: CrmPermissionPolicy;
  crmPermissionPolicyRemove: CrmPermissionPolicy;
  crmPermissionRoleAdd: CrmPermissionRole;
  crmPermissionRoleRemove: CrmPermissionPolicy;
  crmPermissionServiceAdd: CrmPermissionService;
  crmPermissionServiceRemove: CrmPermissionService;
  crmProductBrandAdd: CrmProductBrand;
  crmProductBrandRemove: Scalars['Boolean'];
  crmProductCategoryAdd: CrmProductCategory;
  crmProductCategoryRemove: Scalars['Boolean'];
  crmProductComponentCreate: CrmProductComponent;
  crmProductComponentImport: ProductComponentResponse;
  crmProductComponentRemove: CrmProductComponent;
  crmProductComponentUpdate: CrmProductComponent;
  crmProductComponentUploadImage: File;
  crmProductTypeAdd: CrmProductType;
  crmProductTypeImport: ProductTypeResponse;
  crmProductTypeRemove: Scalars['Boolean'];
  crmProductTypeUpdate: CrmProductType;
  crmProductUploadImage: File;
  crmSLAAddressUpdate: SlaAddress;
  crmSLAGroupAdd: SlaGroup;
  crmSLAGroupRemove: SlaGroup;
  crmSLAGroupUpdate: SlaGroup;
  crmSLATimeUpdate: SlaTime;
  crmSLATroubleAdd: OptionItem;
  crmSLATroubleUpdate: OptionItem;
  crmSLAZoneAdd: SlaZone;
  crmSLAZoneRemove: SlaZone;
  crmSLAZoneUpdate: SlaZone;
  crmScheduleAdd?: Maybe<Schedule>;
  crmScheduleWorkingShiftAdd: WorkingShift;
  crmScheduleWorkingShiftRemove: Scalars['Boolean'];
  crmUserChagePassword: CrmUser;
  crmUserCreate: CrmUser;
  crmUserLogin: CrmUserLoginResponse;
  crmUserLogout?: Maybe<CrmUser>;
  crmUserRefreshToken: CrmUserResponse;
  crmUserUpdate: CrmUser;
  crmUserUploadAvatar: CrmUser;
  crmWarehouseAdd: Warehouse;
  crmWarehouseExport: WarehouseReceipt;
  crmWarehouseImport: WarehouseReceipt;
  crmWarehouseRecall: WarehouseReceipt;
  crmWarehouseRemove: Scalars['Boolean'];
  crmWarehouseTransfer: WarehouseReceipt;
  crmWarehouseUploadImage: File;
  crmWarrantyUpdate: Warranty;
  iamNotificationSubscribe: NotificationSubscribeResponse;
  identityChangePassword: UserResponse;
  identityDichvu3tLogin: UserResponse;
  identityDichvu3tPhoneChallenge?: Maybe<OtpResponse>;
  identityDichvu3tRegister: OtpResponse;
  identityEcoLogin: UserResponse;
  identityEcoPhoneChallenge?: Maybe<OtpResponse>;
  identityEcoRegister: OtpResponse;
  identityKarofiLogin: UserResponse;
  identityKarofiPhoneChallenge?: Maybe<OtpResponse>;
  identityKarofiRegister: OtpResponse;
  identityLoginWithBusinessRole: UserResponse;
  identityLogout?: Maybe<User>;
  identityProfileUpdate?: Maybe<User>;
  identityRefreshToken?: Maybe<UserResponse>;
  identityRegisterWithBusinessRole: UserResponse;
  identitySetPassword?: Maybe<UserResponse>;
  identityUploadAvatar: User;
  identityVerifyForgotPassword?: Maybe<UserResponse>;
  identityVerifyOtp?: Maybe<UserResponse>;
  storageUploadFile: File;
}


export interface MutationAppIncidentCancelArgs {
  arguments: AppIncidentCancelArgs;
}


export interface MutationAppIncidentCancelAbortArgs {
  arguments: AppIncidentCancelAbortArgs;
}


export interface MutationAppIncidentCancelAcceptArgs {
  arguments: AppIncidentCancelAcceptArgs;
}


export interface MutationAppIncidentCancelRejectArgs {
  arguments: AppIncidentCancelDenyArgs;
}


export interface MutationAppIncidentCheckInArgs {
  arguments: AppIncidentCheckInArgs;
}


export interface MutationAppIncidentCloseArgs {
  arguments: AppIncidentCloseArgs;
}


export interface MutationAppIncidentMoveToWarrantyArgs {
  agruments: AppIncidentMoveToWarrantyArgs;
}


export interface MutationAppIncidentTransferArgs {
  arguments: AppIncidentTransferArgs;
}


export interface MutationAppIncidentTransferAbortArgs {
  arguments: AppIncidentTransferCancelArgs;
}


export interface MutationAppIncidentTransferAcceptArgs {
  arguments: AppIncidentTransferAcceptArgs;
}


export interface MutationAppIncidentTransferRejectArgs {
  arguments: AppIncidentTransferRejectArgs;
}


export interface MutationAppIncidentUpdateArgs {
  arguments: AppIncidentUpdateArgs;
}


export interface MutationAppIncidentUploadImageArgs {
  file: Scalars['Upload'];
}


export interface MutationAppInstallationUpdateArgs {
  arguments: AppInstallationArgs;
  id: Scalars['String'];
}


export interface MutationAppInstallationUploadImageArgs {
  file: Scalars['Upload'];
}


export interface MutationAppInstallationUploadReceiptArgs {
  file: Scalars['Upload'];
}


export interface MutationAppNotificationGetDetailArgs {
  id: Scalars['String'];
}


export interface MutationAppNotificationRemoveArgs {
  id: Scalars['String'];
}


export interface MutationAppNotificationUpdateArgs {
  arguments: AppNotificationUpdateArgs;
}


export interface MutationBitrixAddLeadArgs {
  arguments: Eco248LeadArgs;
}


export interface MutationCrmAppUserUpdateArgs {
  arguments: AppUserUpdateArgs;
}


export interface MutationCrmComplainAddCommentArgs {
  arguments?: InputMaybe<ComplainCommentArgs>;
}


export interface MutationCrmComplainCloseArgs {
  arguments?: InputMaybe<ComplainCloseArgs>;
}


export interface MutationCrmComplainMoveToVendorArgs {
  arguments?: InputMaybe<ComplainMoveToVendorArgs>;
}


export interface MutationCrmComplainUpdateArgs {
  arguments: ComplainArgs;
}


export interface MutationCrmDealMakeProfileArgs {
  id: Scalars['String'];
}


export interface MutationCrmDealMoveToDeliveryArgs {
  arguments: DealArgs;
  dealId: Scalars['String'];
}


export interface MutationCrmDealMoveToInstallationArgs {
  arguments: DealArgs;
  dealId: Scalars['String'];
}


export interface MutationCrmDealRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmDealUpdateArgs {
  arguments: DealArgs;
  dealId: Scalars['String'];
}


export interface MutationCrmDeliveryMoveToInstallationArgs {
  arguments: DeliveryArgs;
  id: Scalars['String'];
}


export interface MutationCrmDeliveryRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmDeliveryUpdateArgs {
  arguments: DeliveryArgs;
  id: Scalars['String'];
}


export interface MutationCrmDepartmentAddMembersArgs {
  arguments: DepartmentAddMemberArgs;
}


export interface MutationCrmDepartmentCreateArgs {
  arguments: DepartmentArgs;
}


export interface MutationCrmDepartmentRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmDepartmentRemoveMembersArgs {
  arguments: DepartmentRemoveMemberArgs;
}


export interface MutationCrmDepartmentUpdateArgs {
  arguments: DepartmentUpdateArgs;
}


export interface MutationCrmIncidentCreateArgs {
  arguments: IncidentArgs;
}


export interface MutationCrmIncidentImportArgs {
  file: Scalars['Upload'];
}


export interface MutationCrmIncidentSendArgs {
  arguments: IncidentArgs;
  id: Scalars['String'];
}


export interface MutationCrmIncidentUpdateArgs {
  arguments: IncidentArgs;
  id: Scalars['String'];
}


export interface MutationCrmIncidentUploadImageArgs {
  file: Scalars['Upload'];
}


export interface MutationCrmInstallationRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmInstallationUpdateArgs {
  arguments: InstallationArgs;
  id: Scalars['String'];
}


export interface MutationCrmLeadAddArgs {
  arguments: LeadArgs;
}


export interface MutationCrmLeadAssignWithExcelFileArgs {
  file: Scalars['Upload'];
  saleIds: Array<Scalars['String']>;
}


export interface MutationCrmLeadAssignWithGooleLinkArgs {
  saleIds: Array<Scalars['String']>;
  sheetLink: Scalars['String'];
}


export interface MutationCrmLeadImportArgs {
  file: Scalars['Upload'];
}


export interface MutationCrmLeadMoveToDealArgs {
  arguments: LeadArgs;
  lealId: Scalars['String'];
}


export interface MutationCrmLeadRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmLeadUpdateArgs {
  arguments: LeadArgs;
  id: Scalars['String'];
}


export interface MutationCrmNotificationGetDetailArgs {
  id: Scalars['String'];
}


export interface MutationCrmOptionImportArgs {
  file: Scalars['Upload'];
}


export interface MutationCrmOptionItemAddArgs {
  arguments: OptionItemArgs;
}


export interface MutationCrmOptionItemRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmOptionItemUpdateArgs {
  arguments: OptionItemUpdateArgs;
}


export interface MutationCrmPermissionActionAddArgs {
  arguments: PermissionActionArgs;
}


export interface MutationCrmPermissionActionRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmPermissionPolicyAddArgs {
  arguments: PermissionPolicyArgs;
}


export interface MutationCrmPermissionPolicyRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmPermissionRoleAddArgs {
  arguments: PermissionRoleArgs;
}


export interface MutationCrmPermissionRoleRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmPermissionServiceAddArgs {
  arguments: PermissionServiceArgs;
}


export interface MutationCrmPermissionServiceRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmProductBrandAddArgs {
  arguments: ProductBrandArgs;
}


export interface MutationCrmProductBrandRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmProductCategoryAddArgs {
  arguments: ProductCategoryArgs;
}


export interface MutationCrmProductCategoryRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmProductComponentCreateArgs {
  arguments: ProductComponentArgs;
}


export interface MutationCrmProductComponentImportArgs {
  file: Scalars['Upload'];
}


export interface MutationCrmProductComponentRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmProductComponentUpdateArgs {
  arguments: ProductComponentArgs;
  id: Scalars['String'];
}


export interface MutationCrmProductComponentUploadImageArgs {
  file: Scalars['Upload'];
}


export interface MutationCrmProductTypeAddArgs {
  arguments: ProductTypeArgs;
}


export interface MutationCrmProductTypeImportArgs {
  file: Scalars['Upload'];
}


export interface MutationCrmProductTypeRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmProductTypeUpdateArgs {
  arguments: ProductTypeUpdateArgs;
}


export interface MutationCrmProductUploadImageArgs {
  file: Scalars['Upload'];
}


export interface MutationCrmSlaAddressUpdateArgs {
  argument: SlaAddressUpdateArgs;
}


export interface MutationCrmSlaGroupAddArgs {
  arguments: SlaGroupArgs;
}


export interface MutationCrmSlaGroupRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmSlaGroupUpdateArgs {
  arguments: SlaGroupUpdateArgs;
}


export interface MutationCrmSlaTimeUpdateArgs {
  argument: SlaTimeUpdateArgs;
}


export interface MutationCrmSlaTroubleAddArgs {
  arguments: SlaTroubleArgs;
}


export interface MutationCrmSlaTroubleUpdateArgs {
  arguments: SlaTroubleUpdateArgs;
}


export interface MutationCrmSlaZoneAddArgs {
  arguments: SlaZoneArgs;
}


export interface MutationCrmSlaZoneRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmSlaZoneUpdateArgs {
  arguments: SlaZoneUpdateArgs;
}


export interface MutationCrmScheduleAddArgs {
  arguments: ScheduleArgs;
}


export interface MutationCrmScheduleWorkingShiftAddArgs {
  arguments: WorkingShiftArgs;
}


export interface MutationCrmScheduleWorkingShiftRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmUserChagePasswordArgs {
  newPassword: Scalars['String'];
  oldPassword: Scalars['String'];
}


export interface MutationCrmUserCreateArgs {
  arguments: CrmUserArgs;
}


export interface MutationCrmUserLoginArgs {
  credential: CrmUserLoginArgs;
}


export interface MutationCrmUserRefreshTokenArgs {
  refreshToken: Scalars['String'];
}


export interface MutationCrmUserUpdateArgs {
  arguments: CrmUserUpdateArgs;
}


export interface MutationCrmUserUploadAvatarArgs {
  avatar: Scalars['Upload'];
}


export interface MutationCrmWarehouseAddArgs {
  arguments: WarehouseArgs;
}


export interface MutationCrmWarehouseExportArgs {
  arguments: WarehouseExportArgs;
}


export interface MutationCrmWarehouseImportArgs {
  arguments: WarehouseImportArgs;
}


export interface MutationCrmWarehouseRecallArgs {
  arguments: WarehouseRecallArgs;
}


export interface MutationCrmWarehouseRemoveArgs {
  id: Scalars['String'];
}


export interface MutationCrmWarehouseTransferArgs {
  arguments: WarehouseTransferArgs;
}


export interface MutationCrmWarehouseUploadImageArgs {
  file: Scalars['Upload'];
}


export interface MutationCrmWarrantyUpdateArgs {
  arguments?: InputMaybe<WarrantyUpdateArgs>;
  id?: InputMaybe<Scalars['String']>;
}


export interface MutationIamNotificationSubscribeArgs {
  deviceToken: Scalars['String'];
}


export interface MutationIdentityChangePasswordArgs {
  arguments: UserPasswordArgs;
}


export interface MutationIdentityDichvu3tLoginArgs {
  credential: UserLoginArgs;
}


export interface MutationIdentityDichvu3tPhoneChallengeArgs {
  phone: Scalars['String'];
}


export interface MutationIdentityDichvu3tRegisterArgs {
  arguments: UserRegisterArgs;
}


export interface MutationIdentityEcoLoginArgs {
  credential: UserLoginArgs;
}


export interface MutationIdentityEcoPhoneChallengeArgs {
  phone: Scalars['String'];
}


export interface MutationIdentityEcoRegisterArgs {
  arguments: UserRegisterArgs;
}


export interface MutationIdentityKarofiLoginArgs {
  credential: UserLoginArgs;
}


export interface MutationIdentityKarofiPhoneChallengeArgs {
  phone: Scalars['String'];
}


export interface MutationIdentityKarofiRegisterArgs {
  arguments: UserRegisterArgs;
}


export interface MutationIdentityLoginWithBusinessRoleArgs {
  businessRoleId: Scalars['String'];
}


export interface MutationIdentityProfileUpdateArgs {
  arguments: UserInfoArgs;
}


export interface MutationIdentityRefreshTokenArgs {
  refreshToken: Scalars['String'];
}


export interface MutationIdentityRegisterWithBusinessRoleArgs {
  businessRoleId: Scalars['String'];
  registerInfo: RegisterBusinessRoleArgs;
}


export interface MutationIdentitySetPasswordArgs {
  password: Scalars['String'];
}


export interface MutationIdentityUploadAvatarArgs {
  avatar: Scalars['Upload'];
}


export interface MutationIdentityVerifyForgotPasswordArgs {
  otp: Scalars['Float'];
  session: Scalars['String'];
}


export interface MutationIdentityVerifyOtpArgs {
  otp: Scalars['String'];
  session: Scalars['String'];
}


export interface MutationStorageUploadFileArgs {
  file: Scalars['Upload'];
}

export interface Notification {
  content?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['Float']>;
  id: Scalars['String'];
  image?: Maybe<Scalars['String']>;
  isRead: Scalars['Boolean'];
  metadata?: Maybe<Scalars['String']>;
  receiver?: Maybe<User>;
  sender?: Maybe<User>;
  title?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['Float']>;
}

export interface NotificationResponse {
  count: Scalars['Int'];
  notifications?: Maybe<Array<Notification>>;
  total: Scalars['Int'];
}

export interface NotificationSubscribeResponse {
  deviceToken: Scalars['String'];
  updatedAt: Scalars['Float'];
  userId: Scalars['String'];
}

export interface Option {
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface OptionFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  optionId: Scalars['String'];
  parentId?: InputMaybe<Scalars['String']>;
}

export interface OptionItem {
  children?: Maybe<Array<OptionItem>>;
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  description?: Maybe<Scalars['String']>;
  editable?: Maybe<Scalars['Boolean']>;
  hexColor?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  isCancel?: Maybe<Scalars['Boolean']>;
  isFinal?: Maybe<Scalars['Boolean']>;
  name?: Maybe<Scalars['String']>;
  option?: Maybe<Option>;
  order: Scalars['Int'];
  organizationName?: Maybe<Scalars['String']>;
  parent?: Maybe<OptionItem>;
  slaGroup?: Maybe<SlaGroup>;
  status: Scalars['String'];
  statusCode: Scalars['Int'];
  updatedAt: Scalars['Float'];
}

export interface OptionItemArgs {
  description?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  optionId: Scalars['String'];
  order: Scalars['Int'];
  parentId?: InputMaybe<Scalars['String']>;
}

export interface OptionItemFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  parentId?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['Int']>;
}

export interface OptionItemResponse {
  count?: Maybe<Scalars['Int']>;
  items?: Maybe<Array<OptionItem>>;
  total?: Maybe<Scalars['Int']>;
}

export interface OptionItemUpdateArgs {
  description?: InputMaybe<Scalars['String']>;
  itemId: Scalars['String'];
  name: Scalars['String'];
  order: Scalars['Int'];
}

export interface OptionResponse {
  count?: Maybe<Scalars['Int']>;
  options?: Maybe<Array<Option>>;
  total?: Maybe<Scalars['Int']>;
}

export interface Organization {
  address?: Maybe<Address>;
  area?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  distributorChanel?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  pdaCode?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  region?: Maybe<Scalars['String']>;
  regionCode?: Maybe<Scalars['String']>;
  salesOffice?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
}

export interface OrganizationResponse {
  count: Scalars['Int'];
  organizations?: Maybe<Array<Organization>>;
  total: Scalars['Int'];
}

export interface OtpResponse {
  organization: Scalars['String'];
  session: Scalars['String'];
  target: Scalars['String'];
}

export interface PermissionActionArgs {
  code: Scalars['String'];
  description?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  serviceId: Scalars['String'];
}

export interface PermissionActionResponse {
  actions?: Maybe<Array<CrmPermissionAction>>;
  count: Scalars['Int'];
  total: Scalars['Int'];
}

export interface PermissionPolicyArgs {
  actionIds: Array<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  resourceIds?: InputMaybe<Array<Scalars['String']>>;
}

export interface PermissionPolicyResponse {
  count: Scalars['Int'];
  policies?: Maybe<Array<CrmPermissionPolicy>>;
  total: Scalars['Int'];
}

export interface PermissionRoleArgs {
  description?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  policyIds: Array<Scalars['String']>;
}

export interface PermissionRoleResponse {
  count: Scalars['Int'];
  roles?: Maybe<Array<CrmPermissionRole>>;
  total: Scalars['Int'];
}

export interface PermissionServiceArgs {
  code: Scalars['String'];
  description?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
}

export interface PermissionServiceResponse {
  count: Scalars['Int'];
  services?: Maybe<Array<CrmPermissionService>>;
  total: Scalars['Int'];
}

export interface ProductBrandArgs {
  code?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
}

export interface ProductBrandFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Float']>;
  size?: InputMaybe<Scalars['Float']>;
}

export interface ProductBrandResponse {
  brands?: Maybe<Array<CrmProductBrand>>;
  count: Scalars['Int'];
  total: Scalars['Int'];
}

export interface ProductCategoryArgs {
  brandId: Scalars['String'];
  code?: InputMaybe<Scalars['String']>;
  factoryWarrantyMonths?: InputMaybe<Scalars['Float']>;
  name: Scalars['String'];
  warrantyMonths?: InputMaybe<Scalars['Float']>;
}

export interface ProductCategoryFilterArgs {
  brandId?: InputMaybe<Scalars['String']>;
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Float']>;
  size?: InputMaybe<Scalars['Float']>;
}

export interface ProductCategoryResponse {
  categories?: Maybe<Array<CrmProductCategory>>;
  count: Scalars['Int'];
  total: Scalars['Int'];
}

export interface ProductComponentArgs {
  code?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  hasWarranty: Scalars['Boolean'];
  imageIds?: InputMaybe<Array<Scalars['String']>>;
  imageUrls?: InputMaybe<Array<Scalars['String']>>;
  internalPrice?: InputMaybe<Scalars['Float']>;
  listedPrice?: InputMaybe<Scalars['Float']>;
  model?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  price?: InputMaybe<Scalars['Float']>;
  productBrandId?: InputMaybe<Scalars['String']>;
  productTypeIds?: InputMaybe<Array<Scalars['String']>>;
  quantity?: InputMaybe<Scalars['Int']>;
  statusCode: Scalars['Int'];
  warrantyMonths?: InputMaybe<Scalars['Int']>;
}

export interface ProductComponentFilterArgs {
  hasWarranty?: InputMaybe<Scalars['Boolean']>;
  internalPrice?: InputMaybe<Scalars['Float']>;
  keyword?: InputMaybe<Scalars['String']>;
  listedPrice?: InputMaybe<Scalars['Float']>;
  page?: InputMaybe<Scalars['Float']>;
  productBrandId?: InputMaybe<Scalars['String']>;
  productCategoryId?: InputMaybe<Scalars['String']>;
  productTypeId?: InputMaybe<Scalars['String']>;
  quantity?: InputMaybe<Scalars['Float']>;
  size?: InputMaybe<Scalars['Float']>;
  statusCode?: InputMaybe<Scalars['Int']>;
}

export interface ProductComponentResponse {
  components?: Maybe<Array<CrmProductComponent>>;
  count: Scalars['Int'];
  total: Scalars['Int'];
}

export interface ProductTypeArgs {
  brandName: Scalars['String'];
  categoryName?: InputMaybe<Scalars['String']>;
  componentIds?: InputMaybe<Array<Scalars['String']>>;
  name: Scalars['String'];
}

export interface ProductTypeFilterArgs {
  brandId?: InputMaybe<Scalars['String']>;
  categoryId?: InputMaybe<Scalars['String']>;
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Float']>;
  size?: InputMaybe<Scalars['Float']>;
  statusCode?: InputMaybe<Scalars['Float']>;
}

export interface ProductTypeResponse {
  count: Scalars['Int'];
  total: Scalars['Int'];
  types?: Maybe<Array<CrmProductType>>;
}

export interface ProductTypeUpdateArgs {
  brandName: Scalars['String'];
  categoryName?: InputMaybe<Scalars['String']>;
  componentIds?: InputMaybe<Array<Scalars['String']>>;
  name: Scalars['String'];
  productTypeId: Scalars['String'];
  status?: InputMaybe<Scalars['String']>;
}

export interface Query {
  addressGetCountries: AddressZoneResponse;
  addressGetDistricts: AddressZoneResponse;
  addressGetProvinces: AddressZoneResponse;
  addressGetWards: AddressZoneResponse;
  addressZoneGetList: AddressZoneResponse;
  appBusinessRoleAvailableList: BusinessRoleResponse;
  appIncidentCancelReasonGetList: OptionItemResponse;
  appIncidentExpireReasonGetList: OptionItemResponse;
  appIncidentGetDetail: Incident;
  appIncidentGetList: IncidentResponse;
  appIncidentRequestGroupGetList: OptionItemResponse;
  appIncidentStatistic: AppIncidentStatisticResponse;
  appIncidentTroubleAnalysisGetList: OptionItemResponse;
  appIncidentTroubleGroupGetList: OptionItemResponse;
  appIncidentTroubleReasonGetList: OptionItemResponse;
  appInstallationGetDetail?: Maybe<Installation>;
  appInstallationGetList: InstallationResponse;
  appNotificationGetList: NotificationResponse;
  appProductComponentGetList: ProductComponentResponse;
  appProductTypeGetList: ProductTypeResponse;
  appScheduleGetList: ScheduleResponse;
  appUserFindTechnicianByPhone?: Maybe<CrmUser>;
  appUserGetTechnicianList?: Maybe<CrmUserResponse>;
  appUserGroupForTechnician: UserGroupResponse;
  appUserGroupMemberList: CrmUserResponse;
  appWarrantyProgressGetList: OptionItemResponse;
  appWarrantyTypeGetList: OptionItemResponse;
  bitrixGetLeadsStopAll: BitrixStopTriggerResponse;
  bitrixGetLeadsTrigger: BitrixTriggerResponse;
  crmAppBusinessRoleAvailableList: BusinessRoleResponse;
  crmAppUserDetail: CrmUser;
  crmAppUserGetList: CrmUserResponse;
  crmComplainDepartmentList: DepartmentResponse;
  crmComplainGetDetail: Complain;
  crmComplainGetList: ComplainResponse;
  crmCustomerFindOne?: Maybe<CrmCustomer>;
  crmCustomerGetList: CrmCustomerResponse;
  crmCustomerHistory: DealResponse;
  crmDealGetByNo?: Maybe<Deal>;
  crmDealGetDetail?: Maybe<Deal>;
  crmDealGetList: DealResponse;
  crmDealHistory?: Maybe<Lead>;
  crmDealKtcReasonGetList: OptionItemResponse;
  crmDealMarketingGetList: OptionItemResponse;
  crmDealPromotionGetList: OptionItemResponse;
  crmDealResultGetList: OptionItemResponse;
  crmDeliveryGetDetail?: Maybe<Delivery>;
  crmDeliveryGetList: DeliveryResponse;
  crmDeliveryHistory?: Maybe<Lead>;
  crmDeliveryShippingMethodGetList: OptionItemResponse;
  crmDeliveryShippingStatusGetList: OptionItemResponse;
  crmDeliveryShippingVendorGetList: OptionItemResponse;
  crmDepartmentDetail: Department;
  crmDepartmentGetList: DepartmentResponse;
  crmIncidentCancelReasonGetList: OptionItemResponse;
  crmIncidentContactResultGetList: OptionItemResponse;
  crmIncidentExpireReasonGetList: OptionItemResponse;
  crmIncidentExportTemplateGet: IncidentTemplateResponse;
  crmIncidentGetDetail: Incident;
  crmIncidentGetList: IncidentResponse;
  crmIncidentImportTemplateGet: IncidentTemplateResponse;
  crmIncidentInternalDepartmentGetList: OptionItemResponse;
  crmIncidentInternalReasonGetList: OptionItemResponse;
  crmIncidentPartnerGetList: OptionItemResponse;
  crmIncidentPriorityGetList: OptionItemResponse;
  crmIncidentRequestGroupGetList: OptionItemResponse;
  crmIncidentRequestTypeGetList: OptionItemResponse;
  crmIncidentResultGetList: OptionItemResponse;
  crmIncidentSourceGetList: OptionItemResponse;
  crmIncidentTroubleAnalysisGetList: OptionItemResponse;
  crmIncidentTroubleGroupGetList: OptionItemResponse;
  crmIncidentTroubleReasonGetList: OptionItemResponse;
  crmInstallationGetDetail?: Maybe<Installation>;
  crmInstallationGetList: InstallationResponse;
  crmInstallationHistory?: Maybe<Lead>;
  crmInstallationKtcReasonGetList: OptionItemResponse;
  crmInstallationResultGetList: OptionItemResponse;
  crmInstallationSupportResultGetList: OptionItemResponse;
  crmLeadAssignGetList: LeadAssignHistoryResponse;
  crmLeadGetDetail?: Maybe<Lead>;
  crmLeadGetList: LeadResponse;
  crmLeadKtcReasonGetList: OptionItemResponse;
  crmLeadOrderTypeGetList: OptionItemResponse;
  crmLeadResultGetList: OptionItemResponse;
  crmLeadSourceGetList: OptionItemResponse;
  crmNotificationGetList: CrmNotificationResponse;
  crmOptionGetList: OptionResponse;
  crmOptionItemGetList: OptionItemResponse;
  crmPermissionActionGetList: PermissionActionResponse;
  crmPermissionPolicyGetList: PermissionPolicyResponse;
  crmPermissionRoleGetList: PermissionRoleResponse;
  crmPermissionServiceGetList: PermissionServiceResponse;
  crmProductBrandGetList: ProductBrandResponse;
  crmProductCategoryGetList: ProductCategoryResponse;
  crmProductComponentGetList: ProductComponentResponse;
  crmProductTypeGetList: ProductTypeResponse;
  crmSLAAddressGetList: SlaAddressResponse;
  crmSLAGroupGetList: SlaGroupResponse;
  crmSLATimeGetList: SlaTimeResponse;
  crmSLATroubleGetList: SlaTroubleResponse;
  crmSLAZoneGetList: SlaZoneResponse;
  crmScheduleGetList: ScheduleResponse;
  crmScheduleWorkingShiftGetList: WorkingShiftResponse;
  crmUserAvailableBusinessRoles: BusinessRoleResponse;
  crmUserDetail: CrmUser;
  crmUserGetCollaboratorList?: Maybe<CrmUserResponse>;
  crmUserGetComplainHandlerList?: Maybe<CrmUserResponse>;
  crmUserGetComplainResolverList?: Maybe<CrmUserResponse>;
  crmUserGetList: CrmUserResponse;
  crmUserGetSaleList?: Maybe<CrmUserResponse>;
  crmUserGetTechnicianList?: Maybe<CrmUserResponse>;
  crmUserGroupForTechnician: UserGroupResponse;
  crmUserGroupGetList: UserGroupResponse;
  crmUserProfile?: Maybe<CrmUser>;
  crmWarehouseDeliveryList: WarehouseDeliveryResponse;
  crmWarehouseGetList: WarehouseResponse;
  crmWarehouseProductList: WarehouseProductResponse;
  crmWarehouseTypeGetList: OptionItemResponse;
  crmWarrantyGetList: WarrantyResponse;
  crmWarrantyProgressGetList: OptionItemResponse;
  crmWarrantyTypeGetList: OptionItemResponse;
  identityBankGetList: BankResponse;
  identityCarrierGetList: CarrierResponse;
  identityProfile: User;
  organizationGetByCode: Organization;
  organizationGetByPhone: OrganizationResponse;
  organizationGetGroup: OrganizationResponse;
  organizationGetList: OrganizationResponse;
}


export interface QueryAddressGetCountriesArgs {
  filter?: InputMaybe<AddressZoneFilterArgs>;
  keyword?: InputMaybe<Scalars['String']>;
}


export interface QueryAddressGetDistrictsArgs {
  filter?: InputMaybe<AddressZoneFilterArgs>;
  keyword?: InputMaybe<Scalars['String']>;
  provinceId?: InputMaybe<Scalars['String']>;
}


export interface QueryAddressGetProvincesArgs {
  countryId?: InputMaybe<Scalars['String']>;
  filter?: InputMaybe<AddressZoneFilterArgs>;
  keyword?: InputMaybe<Scalars['String']>;
}


export interface QueryAddressGetWardsArgs {
  districtId: Scalars['String'];
  filter?: InputMaybe<AddressZoneFilterArgs>;
  keyword?: InputMaybe<Scalars['String']>;
}


export interface QueryAddressZoneGetListArgs {
  filter?: InputMaybe<AddressZoneFilterArgs>;
}


export interface QueryAppIncidentGetDetailArgs {
  id?: InputMaybe<Scalars['String']>;
}


export interface QueryAppIncidentGetListArgs {
  filter?: InputMaybe<AppIncidentFilterArgs>;
}


export interface QueryAppIncidentTroubleReasonGetListArgs {
  filter?: InputMaybe<OptionItemFilterArgs>;
}


export interface QueryAppInstallationGetDetailArgs {
  id: Scalars['String'];
}


export interface QueryAppInstallationGetListArgs {
  filter?: InputMaybe<AppInstallationFilterArgs>;
}


export interface QueryAppNotificationGetListArgs {
  filter?: InputMaybe<AppNotificationFilterArgs>;
}


export interface QueryAppProductComponentGetListArgs {
  filter?: InputMaybe<AppProductComponentFilterArgs>;
}


export interface QueryAppProductTypeGetListArgs {
  filter?: InputMaybe<ProductTypeFilterArgs>;
}


export interface QueryAppScheduleGetListArgs {
  filter: AppScheduleFilterArgs;
}


export interface QueryAppUserFindTechnicianByPhoneArgs {
  phone: Scalars['String'];
}


export interface QueryAppUserGetTechnicianListArgs {
  filter?: InputMaybe<UserFilterArgs>;
}


export interface QueryAppUserGroupForTechnicianArgs {
  filter?: InputMaybe<UserGroupFilterArgs>;
}


export interface QueryAppUserGroupMemberListArgs {
  filter?: InputMaybe<UserGroupMemberFilterArgs>;
}


export interface QueryAppWarrantyTypeGetListArgs {
  typeId?: InputMaybe<Scalars['String']>;
}


export interface QueryBitrixGetLeadsTriggerArgs {
  params: BitrixGetLeadArgs;
}


export interface QueryCrmAppUserDetailArgs {
  id: Scalars['String'];
}


export interface QueryCrmAppUserGetListArgs {
  filter?: InputMaybe<AppUserFilterArgs>;
}


export interface QueryCrmComplainDepartmentListArgs {
  filter?: InputMaybe<ComplainDepartmentFilterArgs>;
}


export interface QueryCrmComplainGetDetailArgs {
  id?: InputMaybe<Scalars['String']>;
}


export interface QueryCrmComplainGetListArgs {
  filter?: InputMaybe<ComplainFilterArgs>;
}


export interface QueryCrmCustomerFindOneArgs {
  filter: CustomerFindOneArgs;
}


export interface QueryCrmCustomerGetListArgs {
  filter?: InputMaybe<CustomerFilterArgs>;
}


export interface QueryCrmCustomerHistoryArgs {
  arguments: CustomerHistoryFilterArgs;
}


export interface QueryCrmDealGetByNoArgs {
  no: Scalars['Float'];
}


export interface QueryCrmDealGetDetailArgs {
  id: Scalars['String'];
}


export interface QueryCrmDealGetListArgs {
  filter?: InputMaybe<DealFilterArgs>;
}


export interface QueryCrmDealHistoryArgs {
  id: Scalars['String'];
}


export interface QueryCrmDeliveryGetDetailArgs {
  id: Scalars['String'];
}


export interface QueryCrmDeliveryGetListArgs {
  filter?: InputMaybe<DeliveryFilterArgs>;
}


export interface QueryCrmDeliveryHistoryArgs {
  id: Scalars['String'];
}


export interface QueryCrmDepartmentDetailArgs {
  id?: InputMaybe<Scalars['String']>;
}


export interface QueryCrmDepartmentGetListArgs {
  filter?: InputMaybe<DepartmentFilterArgs>;
}


export interface QueryCrmIncidentGetDetailArgs {
  id?: InputMaybe<Scalars['String']>;
}


export interface QueryCrmIncidentGetListArgs {
  filter?: InputMaybe<IncidentFilterArgs>;
}


export interface QueryCrmIncidentInternalReasonGetListArgs {
  orderTypeId?: InputMaybe<Scalars['String']>;
}


export interface QueryCrmIncidentTroubleReasonGetListArgs {
  filter?: InputMaybe<OptionItemFilterArgs>;
}


export interface QueryCrmInstallationGetDetailArgs {
  id: Scalars['String'];
}


export interface QueryCrmInstallationGetListArgs {
  filter?: InputMaybe<InstallationFilterArgs>;
}


export interface QueryCrmInstallationHistoryArgs {
  id: Scalars['String'];
}


export interface QueryCrmLeadAssignGetListArgs {
  filter?: InputMaybe<LeadAssignFilterArgs>;
}


export interface QueryCrmLeadGetDetailArgs {
  id: Scalars['String'];
}


export interface QueryCrmLeadGetListArgs {
  filter?: InputMaybe<LeadFilterArgs>;
}


export interface QueryCrmLeadOrderTypeGetListArgs {
  parentId?: InputMaybe<Scalars['String']>;
}


export interface QueryCrmNotificationGetListArgs {
  filter?: InputMaybe<CrmNotificationFilterArgs>;
}


export interface QueryCrmOptionItemGetListArgs {
  filter: OptionFilterArgs;
}


export interface QueryCrmPermissionActionGetListArgs {
  serviceId: Scalars['String'];
}


export interface QueryCrmProductBrandGetListArgs {
  filter?: InputMaybe<ProductBrandFilterArgs>;
}


export interface QueryCrmProductCategoryGetListArgs {
  filter?: InputMaybe<ProductCategoryFilterArgs>;
}


export interface QueryCrmProductComponentGetListArgs {
  filter?: InputMaybe<ProductComponentFilterArgs>;
}


export interface QueryCrmProductTypeGetListArgs {
  filter?: InputMaybe<ProductTypeFilterArgs>;
}


export interface QueryCrmSlaAddressGetListArgs {
  filter?: InputMaybe<SlaAddressFilterArgs>;
}


export interface QueryCrmSlaGroupGetListArgs {
  filter?: InputMaybe<SlaGroupFilterArgs>;
}


export interface QueryCrmSlaTimeGetListArgs {
  filter?: InputMaybe<SlaTimeFilterArgs>;
}


export interface QueryCrmSlaTroubleGetListArgs {
  filter?: InputMaybe<SlaTroubleFilterArgs>;
}


export interface QueryCrmSlaZoneGetListArgs {
  filter?: InputMaybe<SlaZoneFilterArgs>;
}


export interface QueryCrmScheduleGetListArgs {
  filter: ScheduleFilterArgs;
}


export interface QueryCrmScheduleWorkingShiftGetListArgs {
  filter?: InputMaybe<WorkingShiftFilterArgs>;
}


export interface QueryCrmUserDetailArgs {
  id: Scalars['String'];
}


export interface QueryCrmUserGetCollaboratorListArgs {
  filter?: InputMaybe<UserFilterArgs>;
}


export interface QueryCrmUserGetComplainHandlerListArgs {
  filter?: InputMaybe<UserFilterArgs>;
}


export interface QueryCrmUserGetComplainResolverListArgs {
  filter?: InputMaybe<UserFilterArgs>;
}


export interface QueryCrmUserGetListArgs {
  filter?: InputMaybe<CrmUserFilterArgs>;
}


export interface QueryCrmUserGetSaleListArgs {
  filter?: InputMaybe<UserFilterArgs>;
}


export interface QueryCrmUserGetTechnicianListArgs {
  filter?: InputMaybe<UserFilterArgs>;
}


export interface QueryCrmUserGroupForTechnicianArgs {
  filter?: InputMaybe<UserGroupFilterArgs>;
}


export interface QueryCrmUserGroupGetListArgs {
  filter?: InputMaybe<UserGroupFilterArgs>;
}


export interface QueryCrmWarehouseDeliveryListArgs {
  filter: WarehouseDeliveryFilterArgs;
}


export interface QueryCrmWarehouseGetListArgs {
  filter?: InputMaybe<WarehouseFilterArgs>;
}


export interface QueryCrmWarehouseProductListArgs {
  filter: WarehouseProductFilterArgs;
}


export interface QueryCrmWarrantyGetListArgs {
  filter?: InputMaybe<WarrantyFilterArgs>;
}


export interface QueryCrmWarrantyTypeGetListArgs {
  typeId?: InputMaybe<Scalars['String']>;
}


export interface QueryIdentityBankGetListArgs {
  filter?: InputMaybe<BankFilterArgs>;
}


export interface QueryIdentityCarrierGetListArgs {
  filter?: InputMaybe<CarrierFilterArgs>;
}


export interface QueryOrganizationGetByCodeArgs {
  code: Scalars['String'];
}


export interface QueryOrganizationGetByPhoneArgs {
  phone: Scalars['String'];
}

export interface RegisterBusinessRoleArgs {
  address?: InputMaybe<Scalars['String']>;
  addressZoneId?: InputMaybe<Scalars['String']>;
  bankAccountHolder?: InputMaybe<Scalars['String']>;
  bankAccountNumber?: InputMaybe<Scalars['String']>;
  bankBranch?: InputMaybe<Scalars['String']>;
  bankCardNumber?: InputMaybe<Scalars['String']>;
  bankId?: InputMaybe<Scalars['String']>;
  carrierId?: InputMaybe<Scalars['String']>;
  dateOfBirth?: InputMaybe<Scalars['Float']>;
  email?: InputMaybe<Scalars['String']>;
  fullname?: InputMaybe<Scalars['String']>;
  identityCardNo?: InputMaybe<Scalars['String']>;
  secondPhone?: InputMaybe<Scalars['String']>;
}

export interface SlaAddress {
  country?: Maybe<Scalars['String']>;
  countryId?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  district?: Maybe<Scalars['String']>;
  districtId?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  province?: Maybe<Scalars['String']>;
  provinceId?: Maybe<Scalars['String']>;
  slaZone?: Maybe<SlaZone>;
  updatedAt: Scalars['Float'];
  ward?: Maybe<Scalars['String']>;
  wardId?: Maybe<Scalars['String']>;
}

export interface SlaAddressFilterArgs {
  addressZoneId?: InputMaybe<Scalars['String']>;
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
  slaZoneId?: InputMaybe<Scalars['String']>;
}

export interface SlaAddressResponse {
  addresses?: Maybe<Array<SlaAddress>>;
  count: Scalars['Int'];
  total: Scalars['Int'];
}

export interface SlaAddressUpdateArgs {
  slaAddressId: Scalars['String'];
  slaZoneId: Scalars['String'];
}

export interface SlaGroup {
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  name: Scalars['String'];
  updatedAt: Scalars['Float'];
}

export interface SlaGroupArgs {
  code?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
}

export interface SlaGroupFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
}

export interface SlaGroupResponse {
  count: Scalars['Int'];
  groups?: Maybe<Array<SlaGroup>>;
  total: Scalars['Int'];
}

export interface SlaGroupUpdateArgs {
  code?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  zoneId: Scalars['String'];
}

export interface SlaTime {
  createdAt: Scalars['Float'];
  group?: Maybe<SlaGroup>;
  groupId: Scalars['String'];
  hours: Scalars['Int'];
  id: Scalars['String'];
  updatedAt: Scalars['Float'];
  zone?: Maybe<SlaZone>;
  zoneId: Scalars['String'];
}

export interface SlaTimeFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
}

export interface SlaTimeResponse {
  count: Scalars['Int'];
  times?: Maybe<Array<SlaTime>>;
  total: Scalars['Int'];
}

export interface SlaTimeUpdateArgs {
  hours: Scalars['Int'];
  slaTimeId: Scalars['String'];
}

export interface SlaTroubleArgs {
  slaGroup: Scalars['String'];
  troubleGroup: Scalars['String'];
  troubleReason: Scalars['String'];
}

export interface SlaTroubleFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
  slaGroupId?: InputMaybe<Scalars['String']>;
  statusCode?: InputMaybe<Scalars['Int']>;
  troubleGroupId?: InputMaybe<Scalars['String']>;
  troubleReasonId?: InputMaybe<Scalars['String']>;
}

export interface SlaTroubleResponse {
  count: Scalars['Int'];
  total: Scalars['Int'];
  troubles?: Maybe<Array<OptionItem>>;
}

export interface SlaTroubleUpdateArgs {
  slaGroup: Scalars['String'];
  statusCode?: InputMaybe<Scalars['Int']>;
  troubleGroup: Scalars['String'];
  troubleReason: Scalars['String'];
  troubleReasonId: Scalars['String'];
}

export interface SlaZone {
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  name: Scalars['String'];
  updatedAt: Scalars['Float'];
}

export interface SlaZoneArgs {
  code?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
}

export interface SlaZoneFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
}

export interface SlaZoneResponse {
  count: Scalars['Int'];
  total: Scalars['Int'];
  zones?: Maybe<Array<SlaZone>>;
}

export interface SlaZoneUpdateArgs {
  code?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  zoneId: Scalars['String'];
}

export interface SchedueDay {
  count?: Maybe<Scalars['Int']>;
  date?: Maybe<Scalars['Float']>;
  dayOfWeek?: Maybe<Scalars['Int']>;
  localDate?: Maybe<Scalars['String']>;
  scheduleWorkingShifts?: Maybe<Array<ScheduleWorkingShift>>;
  total?: Maybe<Scalars['Int']>;
  utcDate?: Maybe<Scalars['String']>;
}

export interface Schedule {
  appointment?: Maybe<Scalars['Float']>;
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  updatedAt: Scalars['Float'];
  user?: Maybe<CrmUser>;
  workingShift?: Maybe<WorkingShift>;
}

export interface ScheduleArgs {
  appointmentAt?: InputMaybe<Scalars['Float']>;
  dealId: Scalars['String'];
  installationAt: Scalars['Float'];
  ktcReasonId?: InputMaybe<Scalars['String']>;
  lastReplaceFilterAt?: InputMaybe<Scalars['Float']>;
  note?: InputMaybe<Scalars['String']>;
  remindAt?: InputMaybe<Scalars['Float']>;
  resolvedAt?: InputMaybe<Scalars['Float']>;
  resultId?: InputMaybe<Scalars['String']>;
  serviceFee?: InputMaybe<Scalars['Float']>;
  technicianId: Scalars['String'];
  workingShiftId: Scalars['String'];
}

export interface ScheduleFilterArgs {
  fromDate: Scalars['Float'];
  technicianId?: InputMaybe<Scalars['String']>;
  toDate: Scalars['Float'];
}

export interface ScheduleResponse {
  count: Scalars['Int'];
  scheduleDays?: Maybe<Array<SchedueDay>>;
  total: Scalars['Int'];
}

export interface ScheduleWorkingShift {
  installation?: Maybe<Installation>;
  reservedUsers?: Maybe<Scalars['Int']>;
  totalUsers?: Maybe<Scalars['Int']>;
  workingShift: WorkingShift;
}

export interface TimeOutObj {
  delay: Scalars['Int'];
  name: Scalars['String'];
}

export interface User {
  address?: Maybe<Address>;
  avatar?: Maybe<File>;
  bankAccounts?: Maybe<Array<BankAccount>>;
  businessRoles?: Maybe<Array<BusinessRole>>;
  carriers?: Maybe<Array<Carrier>>;
  configuration?: Maybe<Configuration>;
  createdAt?: Maybe<Scalars['Float']>;
  dateOfBirth?: Maybe<Scalars['Float']>;
  email?: Maybe<Scalars['String']>;
  extendId?: Maybe<Scalars['String']>;
  fullname?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  phone?: Maybe<Scalars['String']>;
  phones?: Maybe<Array<Scalars['String']>>;
  sessionBusinessRole?: Maybe<BusinessRole>;
  sessionMultiBusinessRoles?: Maybe<Array<BusinessRole>>;
  unreadNotificationCount?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['Float']>;
  username?: Maybe<Scalars['String']>;
}

export interface UserFilterArgs {
  addressZoneId?: InputMaybe<Scalars['String']>;
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
  sortByAddressZoneId?: InputMaybe<Scalars['String']>;
}

export interface UserGroupFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Float']>;
  size?: InputMaybe<Scalars['Float']>;
}

export interface UserGroupMemberFilterArgs {
  groupId?: InputMaybe<Scalars['String']>;
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Float']>;
  size?: InputMaybe<Scalars['Float']>;
}

export interface UserGroupResponse {
  count: Scalars['Int'];
  groups?: Maybe<Array<CrmUserGroup>>;
  total: Scalars['Int'];
}

export interface UserInfoArgs {
  address?: InputMaybe<Scalars['String']>;
  addressZoneId?: InputMaybe<Scalars['String']>;
  bankAccountHolder?: InputMaybe<Scalars['String']>;
  bankAccountNumber?: InputMaybe<Scalars['String']>;
  bankBranch?: InputMaybe<Scalars['String']>;
  bankCardNumber?: InputMaybe<Scalars['String']>;
  bankId?: InputMaybe<Scalars['String']>;
  carrierId?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  fullname?: InputMaybe<Scalars['String']>;
  phones?: InputMaybe<Array<Scalars['String']>>;
}

export interface UserLoginArgs {
  password: Scalars['String'];
  phone: Scalars['String'];
}

export interface UserPasswordArgs {
  newPassword: Scalars['String'];
  oldPassword?: InputMaybe<Scalars['String']>;
}

export interface UserRegisterArgs {
  name?: InputMaybe<Scalars['String']>;
  password: Scalars['String'];
  phone: Scalars['String'];
}

export interface UserResponse {
  accessToken?: Maybe<Scalars['String']>;
  avaiableBusinessRoles?: Maybe<Array<BusinessRole>>;
  loggedInTime?: Maybe<Scalars['Float']>;
  refreshToken?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
}

export interface Warehouse {
  address?: Maybe<CrmAddress>;
  createdAt: Scalars['Float'];
  createdBy?: Maybe<CrmUser>;
  deliveries?: Maybe<Array<Delivery>>;
  id: Scalars['String'];
  inventory?: Maybe<Scalars['Float']>;
  name?: Maybe<Scalars['String']>;
  products?: Maybe<Array<WarehouseProduct>>;
  status?: Maybe<Scalars['String']>;
  type?: Maybe<WarehouseType>;
  updatedAt: Scalars['Float'];
}

export interface WarehouseArgs {
  address?: InputMaybe<Scalars['String']>;
  addressZoneId?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  statusCode: Scalars['Int'];
  warehouseTypeId: Scalars['String'];
}

export interface WarehouseDeliveryFilterArgs {
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
  warehouseId: Scalars['String'];
}

export interface WarehouseDeliveryResponse {
  count: Scalars['Int'];
  deliveries?: Maybe<Array<Delivery>>;
  total: Scalars['Int'];
}

export interface WarehouseExportArgs {
  deliveryId: Scalars['String'];
  warehouseId: Scalars['String'];
}

export interface WarehouseFilterArgs {
  address?: InputMaybe<Scalars['String']>;
  ctvId?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
  typeId?: InputMaybe<Scalars['String']>;
}

export interface WarehouseImportArgs {
  appointmentAt?: InputMaybe<Scalars['Float']>;
  products: Array<WarehouseProductArgs>;
  warehouseId: Scalars['String'];
}

export interface WarehouseProduct {
  createdAt: Scalars['Float'];
  exportReceipts?: Maybe<Array<WarehouseReceipt>>;
  id: Scalars['String'];
  importReceipts?: Maybe<Array<WarehouseReceipt>>;
  inventory: Scalars['Float'];
  productType?: Maybe<CrmProductType>;
  recallReceipts?: Maybe<Array<WarehouseReceipt>>;
  transferReceipts?: Maybe<Array<WarehouseReceipt>>;
  updatedAt: Scalars['Float'];
  warehouse?: Maybe<Warehouse>;
}

export interface WarehouseProductArgs {
  imageIds?: InputMaybe<Array<Scalars['String']>>;
  quantity?: InputMaybe<Scalars['Int']>;
  typeId: Scalars['String'];
}

export interface WarehouseProductFilterArgs {
  page?: InputMaybe<Scalars['Int']>;
  size?: InputMaybe<Scalars['Int']>;
  warehouseId: Scalars['String'];
}

export interface WarehouseProductResponse {
  count: Scalars['Int'];
  products?: Maybe<Array<WarehouseProduct>>;
  total: Scalars['Int'];
}

export interface WarehouseRecallArgs {
  deliveryId: Scalars['String'];
  warehouseId: Scalars['String'];
}

export interface WarehouseReceipt {
  createdAt: Scalars['Float'];
  delivery?: Maybe<Delivery>;
  exportWarehouse?: Maybe<Warehouse>;
  id: Scalars['String'];
  importWarehouse?: Maybe<Warehouse>;
  processedAt?: Maybe<Scalars['Float']>;
  receiptItems?: Maybe<Array<WarehouseReceiptItem>>;
  type?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface WarehouseReceiptItem {
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  imageUrls?: Maybe<Array<Scalars['String']>>;
  productType?: Maybe<CrmProductType>;
  quantity?: Maybe<Scalars['Int']>;
  updatedAt: Scalars['Float'];
}

export interface WarehouseResponse {
  count: Scalars['Int'];
  total: Scalars['Int'];
  warehouses?: Maybe<Array<Warehouse>>;
}

export interface WarehouseTransferArgs {
  exportWarehouseId: Scalars['String'];
  importWarehouseId: Scalars['String'];
  products: Array<WarehouseProductArgs>;
  transferAt?: InputMaybe<Scalars['Float']>;
}

export interface WarehouseType {
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface Warranty {
  addressZone?: Maybe<Scalars['String']>;
  condition?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  customer?: Maybe<CrmCustomer>;
  customerId?: Maybe<Scalars['String']>;
  delayReason?: Maybe<Scalars['String']>;
  expectedResolveAt?: Maybe<Scalars['Float']>;
  id: Scalars['String'];
  incident?: Maybe<Incident>;
  measurement?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  receiveAt?: Maybe<Scalars['Float']>;
  repareDateCount?: Maybe<Scalars['Float']>;
  requestAt?: Maybe<Scalars['Float']>;
  requestCount?: Maybe<Scalars['Float']>;
  requestItems?: Maybe<Array<WarrantyItem>>;
  resolveAt?: Maybe<Scalars['Float']>;
  resolveMethod?: Maybe<Scalars['String']>;
  resolveOption?: Maybe<Scalars['String']>;
  resolveTime?: Maybe<Scalars['Float']>;
  responseCount?: Maybe<Scalars['Float']>;
  responseItems?: Maybe<Array<WarrantyItem>>;
  result?: Maybe<OptionItem>;
  technicianReceiveAt?: Maybe<Scalars['Float']>;
  technicianResolveAt?: Maybe<Scalars['Float']>;
  updatedAt: Scalars['Float'];
  warehouseReceiveAt?: Maybe<Scalars['Float']>;
  warehouseResolveAt?: Maybe<Scalars['Float']>;
  warrantyImageUrls?: Maybe<Array<Scalars['String']>>;
  warrantyType?: Maybe<OptionItem>;
}

export interface WarrantyFilterArgs {
  email?: InputMaybe<Scalars['String']>;
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  phone?: InputMaybe<Scalars['String']>;
  serial?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['Int']>;
  troubleAnalysisId?: InputMaybe<Scalars['String']>;
  troubleReasonId?: InputMaybe<Scalars['String']>;
}

export interface WarrantyItem {
  component?: Maybe<CrmProductComponent>;
  count?: Maybe<Scalars['Float']>;
  createdAt: Scalars['Float'];
  id: Scalars['String'];
  measurement?: Maybe<Scalars['String']>;
  no: Scalars['Float'];
  price?: Maybe<Scalars['Float']>;
  progressId?: Maybe<Scalars['String']>;
  totalPrice?: Maybe<Scalars['Float']>;
  updatedAt: Scalars['Float'];
}

export interface WarrantyItemArgs {
  componentId: Scalars['String'];
  count?: InputMaybe<Scalars['Int']>;
  measurement?: InputMaybe<Scalars['String']>;
  progressId?: InputMaybe<Scalars['String']>;
}

export interface WarrantyResponse {
  count: Scalars['Int'];
  total: Scalars['Int'];
  warranties?: Maybe<Array<Warranty>>;
}

export interface WarrantyUpdateArgs {
  items?: InputMaybe<Array<WarrantyItemArgs>>;
}

export interface WorkingShift {
  code?: Maybe<Scalars['String']>;
  createdAt: Scalars['Float'];
  hourEnd: Scalars['Int'];
  hourStart: Scalars['Int'];
  id: Scalars['String'];
  minuteEnd: Scalars['Int'];
  minuteStart: Scalars['Int'];
  name?: Maybe<Scalars['String']>;
  updatedAt: Scalars['Float'];
}

export interface WorkingShiftArgs {
  code?: InputMaybe<Scalars['String']>;
  hourEnd: Scalars['Int'];
  hourStart: Scalars['Int'];
  minuteEnd: Scalars['Int'];
  minuteStart: Scalars['Int'];
  name: Scalars['String'];
}

export interface WorkingShiftFilterArgs {
  keyword?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Float']>;
  size?: InputMaybe<Scalars['Float']>;
}

export interface WorkingShiftResponse {
  count: Scalars['Int'];
  total: Scalars['Int'];
  workingShifts?: Maybe<Array<WorkingShift>>;
}

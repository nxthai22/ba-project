import { gql } from 'apollo-angular';

export const AuthQuery = {
  LOGIN: gql`mutation crmUserLogin($credential: CrmUserLoginArgs!) {
              crmUserLogin(credential: $credential) {
                accessToken
                refreshToken
                user {
                  id
                  phone
                  email
                  fullname
                  avatarUrl
                  department{
                    id
                    name
                  }
                  departments {
                    id
                    name
                  }
                  status
                }
              }
            }`
};

import { Component, OnInit } from '@angular/core';
import { LoginDto } from '../dto/login.dto';
import { GlobalVariable } from '../../../common/global-variable';
import { CommonService } from '../../../common/common.service';
import { CookieService } from 'ngx-cookie-service';
import { FormBuilder, Validators } from '@angular/forms';
import { Mutation } from '../../../../commons/types';
import { Apollo } from 'apollo-angular';
import { map } from 'rxjs/operators';
import { AuthQuery } from '../graphql/graphql_query';
import { nameof } from 'ts-simple-nameof';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  dto: LoginDto = new LoginDto();
  userForm: any;

  constructor(
    private globalVariable: GlobalVariable,
    private common: CommonService,
    private cookie: CookieService,
    private fb: FormBuilder,
    private apollo: Apollo
  ) {
  }

  async ngOnInit() {
    this.userForm = this.fb.group({
      tel: ['', Validators.required],
      password: ['', Validators.required]
    });
    const abc = await this.apollo.mutate<Mutation>({
      mutation: AuthQuery.LOGIN,
      variables: {
        credential: {
          email: 'cvgqkn1@smarthiz.com',
          password: 'Nothing@123',
          organizationId: 'a9e98734-e5d2-4851-84f3-5f1c9a9cd2a7'
        }
      }
    }).pipe(map(res => res.data?.crmUserLogin)).toPromise();
  }

  async submitForm() {
    if (!this.common.validateForm(this.userForm)) return;

    // this.globalVariable.setIsLoading(true);
    // AuthService.authControllerLogin({
    //   body: this.dto,
    // })
    //   .then((response) => {
    //     this.globalVariable.setIsLoading(false);
    //     this.cookie.set('token', String(response.token), { path: '/' });
    //     window.location.href = '/dashboard';
    //   })
    //   .catch((err) => {
    //     this.globalVariable.setIsLoading(false);
    //     this.common.alertError('', err);
    //   });
  }
}

export interface DataResult<T> {
  data: T[];
  count: number;
  total: number;
  page: number;
  pageCount: number;
}

export interface INavData {
  name: string;
  url?: string;
  icon: string;
  visible?: boolean;
  key?: string;
  children?: INavData[];
}

export interface Auth {
  username: string;
  tel?: string;
  fullName?: string;
  permissions: string[];
  roleId: number;
  type: string;
  iss?: string;
  sub?: string;
  aud?: string[] | string;
  exp?: number;
  nbf?: number;
  iat?: number;
  jti?: string;
}

// const { codegen: generateService } = require('apollo-angular');

// generateService({
//   methodNameMode: 'operationId',
//   // remoteUrl: 'http://10.20.4.157:4000/docs/json',
//   remoteUrl: 'http://localhost:5005/docs/json',
//   outputDir: './src/app/services',
//   strictNullChecks: true,
//   useCustomerRequestInstance: true,
//   modelMode: 'interface',
//   extendDefinitionFile: './services/customerDefinition.ts',
//   extendGenericType: ['JsonResult'],
//   sharedServiceOptions: true,
//   useStaticMethod: true,
//   openApi: '3.0.0',
// }).then(() => {
//   // eslint-disable-next-line no-console
//   console.log('OK');
// });
module.exports = {
  overwrite: true,
  schema: 'https://stg-crm-api.karofi.com/graphql',
  // documents: './src/graphql/*.graphql',
  generates: {
    'src/commons/types.ts': {
      plugins: [
        'typescript',
        'typescript-operations',
        'typescript-apollo-angular'
      ],
      config: {
        scalars: { ID: 'string' | 'number' },
        skipTypename: true,
        declarationKind:{
          union: 'type',
          type: 'interface',
          input: 'interface',
          scalar: 'interface',
          arguments: 'interface',
          interface: 'interface'
        },
        enumsAsTypes: true
      }
    }
  }
};
